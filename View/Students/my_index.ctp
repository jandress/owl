<div class="tm_3_em container-fluid">
  <h1>Students enrolled in <span>Digital Writing</span></h1>
    <?php
      echo $this->BootstrapForm->select('selectoptions', $selectoptions);
    ?>
    <table class="table table-striped table-bordered table-hover">
      <tr>
        <th class="span1" style = 'text-align:center;' ><?php echo $this->BootstrapPaginator->sort('id');?></th>
        <th class="span5"><?php echo $this->BootstrapPaginator->sort('first_name');?></th>
        <th class="span5"><?php echo $this->BootstrapPaginator->sort('last_name');?></th>
        <th class="span5"><?php echo $this->BootstrapPaginator->sort('username');?></th>
        <th class="actions span4" style = 'text-align:center;'>Actions</th>
      </tr>
    <?php
    // echo $this->BootstrapForm->hidden('LandingUser.name');
    // echo $this->BootstrapForm->hidden('LandingUser.id');
    foreach ($users as $User):
      $pid = $User['User']['id'] ;
       ?>
      <tr>
        <td style = 'text-align:center;'><?php echo h($User['User']['id']); ?>&nbsp;</td>
        <td><?php echo h($User['User']['first_name']); ?>&nbsp;</td>

        <td><?php echo h($User['User']['last_name']); ?>&nbsp;</td>
        <td><?php echo h($User['User']['username']); ?>&nbsp;</td>

        <td class="actions" style = 'text-align:center;'>
          <?php echo $this->Html->link(__('View'), array('action' => 'view', $User['User']['id'])); ?><span> | </span>
          <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $User['User']['id'])); ?><span> | </span>
          <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $User['User']['id']), null, __('Are you sure you want to delete # %s?', $User['User']['id'])); ?>
        </td>
      </tr>
    <?php endforeach; ?>
    </table>
</div>
