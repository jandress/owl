<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo __($title_for_layout); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
	<?php if ($isIE): ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<?php endif ?>

    <!-- Bootstrap core CSS -->
    <?php if (file_exists("css/stylesheet/".$settings['stylesheet'])): ?>
            <link href="/css/stylesheet/<?php echo $settings['stylesheet']; ?>" rel="stylesheet" title = "theme">
    <?php else: ?>
            <link href="/css/stylesheet/bootstrap.min.css" rel="stylesheet"  title = "theme">    
    <?php endif; ?>


<link href="/css/layout_aids.css" rel="stylesheet"  title = "theme">    
<link href="/css/style.css" rel="stylesheet"  title = "theme">    
    <link href="/css/icons.css" rel="stylesheet">  
  <style type="text/css">

.hidden {
  display: none;
  visibility: hidden;
}

.visible-phone {
  display: none !important;
}

.visible-tablet {
  display: none !important;
}

.hidden-desktop {
  display: none !important;
}

.visible-desktop {
  display: inherit !important;
}

@media (min-width: 768px) and (max-width: 979px) {
  .hidden-desktop {
    display: inherit !important;
  }
  .visible-desktop {
    display: none !important ;
  }
  .visible-tablet {
    display: inherit !important;
  }
  .hidden-tablet {
    display: none !important;
  }
}

@media (max-width: 767px) {
  .hidden-desktop {
    display: inherit !important;
  }
  .visible-desktop {
    display: none !important;
  }
  .visible-phone {
    display: inherit !important;
  }
  .hidden-phone {
    display: none !important;
  }
}




img {
	width: auto;
	height: auto;
	max-width: 100%;
	vertical-align: middle;
	border: 0;
	-ms-interpolation-mode: bicubic;
}

  </style>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
    <![endif]-->
	<script type="text/javascript" src = '/js/jquery.js'></script>	
	<script type="text/javascript" src="/js/ajax_helper.js"></script>	
	<script type="text/javascript" src='/quizmodules/js/quiz_helper.js'></script>	
	<?php echo $this->element('ga_js'); ?>	
		
  </head>
  <body>	
	<a class="featured iframe"></a>   			
    <div id="wrap">
      <div class="">
	
	<?php echo($this->element('navigation/top_nav')); ?>		
		
		


    <div class="container tm_2_em">		
			<div class = "clearfix" style="line-height:1px">&nbsp;</div>
			<div id = "home_content_area" class="well" >
				<?php $pathToBannerAssets = '/banners/home/'; ?>
				<div class="well hidden-phone " style="background-color:#330033; color:#ffffff; overflow-x:hidden; min-height:470px; max-height:470px; overflow-y:hidden;">
					<div  style="width:500px; margin-left:auto !important; margin-right:auto;" class = "bm_1_em">
						<img src = '/img/yellow_text.png' alt = "Welcome To The Online Writing Lab"/>			
						<?php if (empty($isIE)): ?>
							<div id = "arrows_container">
								<div id="arrows">
									<div id="left_arrow">

									</div>
									<div id="right_arrow">

									</div>										
								</div>
							</div>							
						<?php endif ?>
					</div>
					<section class="ccontainer">
				
					    <div id="carousel" class="panels-backface-invisible">

							<?php


		


							// $landingpage = $landingpage['Landingtier'][0];
							 $limit = empty($isIE) ? 4 : 1;
							//debug($landingpage['Landingtier'][0]['Landingtout']);

							 foreach ($landingpage['Landingtier'][0]['Landingtout'] as $key => $value):
							 //foreach ($landingpage['Landingtout'] as $key => $value):

							 	if($key<$limit):

							 ?>

	<figure id="figure_<?php echo $key?>">
		<a href = "#">
			<div class="imageholder" style = "width:70%; margin:auto">
				<img alt="Sherlock Owl" src="/owls/badge_large_<?php echo $value['image_url'];?>"/>
			</div>
			<br/>
			<div style="width:600px; margin-left:-135px !important;">
				<small>
					<?php 
						echo $value['copy'];							
					?>													
				</small>
			</div>
		</a>
	</figure>



							<?php

							endif;
							 endforeach ?>
					    </div>
				  </section>
				</div>
				
				
				
				
				
				<div class="hero-unit home_hero_unit visible-phone" style="text-align:center; background: #DDD url('<?php echo $pathToBannerAssets ?>gradient_strip.jpg') repeat-x; padding:14px; " >
					<div style="width:90%; margin:auto">
						<img style = 'margin-top:24px; margin-bottom:20px;' src="<?php echo $pathToBannerAssets ?>iphone_banner.png" alt = ""/>						
					</div>
				</div>
				
				
				
				
				
				
	
				
				
				
				
				
				
				
				
				
				
				
			

			<div style = "width:100%;" class ="" >

					<?php 
					$variable = $landingpage['Landingtier'][0]['Landingtout'];
					//debug($variable);
					foreach ($variable as $key => $value): 
						// $i = 'badge_small_'.str_replace(".jpg",'',$value['image_url']).'.png';
						// $j = 'badge_med_'.str_replace(".jpg",'',$value['image_url']).'.png';
						$base = str_replace("/owls/badge_med_",'',$value['image_url']);
						$base = str_replace(".png", '', $base);
						$i = 'badge_small_'.$base.'.png';
						$j = 'badge_med_'.$base.'.png';

						?>														
						<div class="condensed_landing_tout med" style="text-align:center !important; float:left; margin-top:4px" >
							<div class="">
								<img src ='/owls/<?php echo $j; ?>' alt = "<?php echo $value['cta_url']; ?>" class = "med"/>										
								<div class="clearfix" style="line-height:1px;">&nbsp;</div>									
								<br/>
								<small class="med">
									<?php  echo $value['copy']; ?>
								</small>									
								<div class="clearfix" style="line-height:1px;">&nbsp;</div>																	
								<div class = "tm_2_em bm_2_em" style="width:80%; margin:auto;">
								<a href = "<?php echo $value['cta_url']; ?>" target = "<?php echo str_replace(".jpg",'',$value['cta_target']); ?>" class = "btn btn-large btn-success btn-block"><?php echo $value['cta_label']; ?></a>										
								</div>
								<div class="clearfix" style="line-height:1px;">&nbsp;</div>																										
							</div>		
						</div>

						<div class="condensed_landing_tout small" style="text-align:center !important; width:10%; float:left; margin-top:4px" >
							<a id = "/owls/badge_med_<?php echo str_replace(".jpg",'',$value['image_url']); ?>"  class = "condensed_landing_tout_link" href = "<?php echo $value['cta_url']; ?>" target = "<?php echo $value['cta_target']; ?>">	
								<img src ='/owls/<?php echo $i; ?>' alt = "<?php echo $value['cta_url']; ?>" class = "small" />	
								<div class="clearfix" style="line-height:1px;">&nbsp;</div>									
								<small class="small">
									<?php 
										echo $value['head']; 
										//echo $key; 											
									?>
								</small>							
							</a>
							<div class="clearfix" style="line-height:1px;">&nbsp;</div>																	
						</div>
						
					<?php endforeach ?>						
			<div class = "clearfix">&nbsp;</div>
		</div>












			</div>
			<div class = "clearfix tm_4_em">&nbsp;</div>			
      	</div><!--/row-->
      </div>

    </div>

	<?php 
	$background_image ='armillarywatermark_dark.jpg';
	
//	debug($landingpage['Landingtout']);
	
	
	 ?>
	<div class="fullscreen-image">
	  <img alt="" src="/img/backgrounds/<?php echo $background_image; ?>">
	</div>
	
	
	<script type="text/javascript">
		

	
	var testclips = [
		<?php

		$ar = $landingpage['Landingtier'][0]['Landingtout'];
		 foreach ($ar as $key => $value): ?>
			<?php 
				// /owls/badge_med_hipster.png
				//  /owls/badge_med_
				$base = str_replace("/owls/badge_med_",'',$value['image_url']);
				$base = str_replace(".png", '', $base);
				//$large = '/owls/badge_large_'.str_replace(".jpg",'.png',$value['image_url']);
				// $small = '/owls/badge_small_'.str_replace(".jpg",'.png',$value['image_url']);
				// $med = '/owls/badge_med_'.str_replace(".jpg",'.png',$value['image_url']);
				$small = '/owls/badge_small_'.$base.'.png';
				$med = '/owls/badge_med_'.$base.'.png';
				$cta_url = $value['cta_url'];
				echo '[ "'.$small.'", "'.$med.'" ]';
				if($key<count($ar)-1){
					echo ',';
				}
		?>
		<?php endforeach ?>
	];

	var ctas = [
		<?php foreach ($ar as $key => $value): ?>
			<?php 
				$cta_url = $value['cta_url'];
				echo "'".$cta_url."'";
				if($key<count($ar)-1){
					echo ',';
				}
		?>
		<?php endforeach ?>
	];
	var copy = [
		<?php foreach ($ar as $key => $value): ?>
			<?php 
				$cta_copy = $value['copy'];
				echo "'".addslashes($cta_copy)."'";
				if($key<count($ar)-1){
					echo ',';
				}
		?>
		<?php endforeach ?>
	];
	
	var ids = [
		<?php foreach ($ar as $key => $value): ?>
			<?php 
				$cta_url = str_replace(".jpg",'',$value['image_url']);
				echo "'/owls/badge_med_".$cta_url."'";
				if($key<count($ar)-1){
					echo ',';
				}
		?>
		<?php endforeach ?>
	];
	</script>
	
		
	<?php if (empty($isIE)): ?>
		<script type="text/javascript" src="/js/cons_hp_utils.js"></script>	
		<script type="text/javascript" src="/js/cons_hp.js"></script>
	<?php else:?>
		<script type="text/javascript" src="/js/cons_hp_ie9.js"></script>		
	<?php endif; ?>
	
	<script src="/js/bootstrap.min.js"></script>

	

	






  </body>
</html>