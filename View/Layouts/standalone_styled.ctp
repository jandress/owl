<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
    <title><?php echo $title_for_layout; ?></title>
    <?php if (file_exists("css/stylesheet/".$settings['stylesheet'])): ?>
            <link href="/css/stylesheet/<?php echo $settings['stylesheet']; ?>?nc=<?php echo rand()?>" rel="stylesheet">
    <?php else: ?>
            <link href="/css/stylesheet/bootstrap.min.css" rel="stylesheet">
    <?php endif ?>
    <!-- Custom styles for this template -->
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/layout_aids.css" rel="stylesheet">
    <link href="/Quizmodules/css/pnotify.custom.min.css" rel="stylesheet">
    <link href="/css/icons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/PagekwikTools/wyswyg/bootstrap3-wysihtml5.min.css">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="//js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/js/ie-emulation-modes-warning.js"></script>
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/PagekwikTools/js/ajax_slider.js"></script>
    <script type="text/javascript" src='/quizmodules/js/quiz_helper.js'></script>
    <!--wyswyg editor -->
    <script src="/PagekwikTools/wyswyg/wysihtml5-0.3.0.min.js"></script>
    <script src="/PagekwikTools/wyswyg/handlebars/handlebars.runtime.min.js"></script>
    <script src="/PagekwikTools/wyswyg/bootstrap3-wysihtml5.min.js"></script>
    <!--end wyswyg editor -->
    <?php  /* <link rel="stylesheet" type="text/css" href="/css/freelancer.css"></link> */ ?>
    <?php echo $this->element('navigation/top_nav',array('admin'=>false)); ?>
    <script type="text/javascript">
            var main_content_ajax_helper = ajax_slider();
    </script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <a class="featured iframe"></a>
    <?php echo $this->element('navigation/top_nav',array('admin'=>false)); ?>
    <div class = "container tm_4_em">
        <?php echo $content_for_layout; ?>
    </div>
    <div class = "clear clearfix tm_1_em" >&nbsp;</div>
    <?php echo $this->element('global/footer') ?>
    <!-- Event Modal -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-body">
                        <p>Loading...</p>
                    </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Event modal -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/Quizmodules/js/pnotify.custom.min.js"></script>
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
    <script src="/Quizmodules/js/quiz_notification.js"></script>
    <script type="text/javascript" src="/Audiomanager/soundmanager/soundmanager2.js"></script>
    <script type="text/javascript" src="/Audiomanager/soundmanager/inlineplayer.js"></script>
    <script type="text/javascript" charset="utf-8" src = '/PagekwikTools/js/zeroclipboard/ZeroClipboard.js' ></script>
    <script src="/js/siteJsWrapper.js"></script>
    <script type="text/javascript" charset="utf-8" src = '/Quizmodules/js/quiz.js' ></script>
    <?php foreach ($additional_scripts as $key => $value): ?>
      <script type="text/javascript" src='<?php echo $value ?>.js'></script>
    <?php endforeach; ?>
    <?php
        echo $this->Session->flash('flash', array(
            'element' => 'flash',
            'params' => array('plugin' => 'PagekwikTools')
        ));
        // this flash should probably be included in the sites, rather than an a plugin.
        // also the site flash currently resides in the quiz plugin. that needs to be relocated to somewhere universal.
    ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            onPageLoad();
        });
    </script>
    <?php  echo $this->Js->writeBuffer();  ?>
  </body>
</html>