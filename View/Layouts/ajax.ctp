<?php echo $content_for_layout; ?>
<?php foreach ($additional_scripts as $key => $value): ?>
    <script type="text/javascript" src='<?php echo $value ?>.js'></script>
<?php endforeach; ?>
<?php echo $this->fetch('bottom_script') ?>
<?php 
	echo $this->Js->writeBuffer(); 
	echo $this->Session->flash('flash', array( 'element' => 'flash', 'params' => array('plugin' => 'PagekwikTools') ));
?>