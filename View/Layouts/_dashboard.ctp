  <!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="/favicon.ico">
      <title><?php echo $title_for_layout; ?></title>

      <!-- Bootstrap core CSS -->
      <?php if (file_exists("css/stylesheet/".$settings['stylesheet'])): ?>
              <link href="/css/stylesheet/<?php echo $settings['stylesheet']; ?>" rel="stylesheet" title = "theme">
      <?php else: ?>
              <link href="/css/stylesheet/bootstrap.min.css" rel="stylesheet"  title = "theme">
      <?php endif;
      //debug(file_exists("css/stylesheet/".$settings['stylesheet']));exit;
      ?>
      <!-- Custom styles for this template -->
      <link href="/css/style.css" rel="stylesheet">
      <link href="/css/dashboard.css" rel="stylesheet">
      <link href="/css/layout_aids.css" rel="stylesheet">
      <link href="/css/icons.css" rel="stylesheet">
      <link href="/css/calendar.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="/PagekwikTools/jquery-ui/jquery-ui.min.css">
      <link href="/Quizmodules/css/pnotify.custom.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="/PagekwikTools/wysihtml5/bootstrap3-wysihtml5.min.css">
      <!--CSS -->
      <script src="/js/jquery.js"></script>
      <script src="/js/bootstrap.js"></script>
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="/js/ie10-viewport-bug-workaround.js"></script>


      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

      <style type="text/css">

              .navbar-default h1, .navbar-default h2, .navbar-default h3, .navbar-default h4, .navbar-default h5, .navbar-default h6,
              .navbar-inverse h1, .navbar-inverse h2, .navbar-inverse h3, .navbar-inverse h4, .navbar-inverse h5, .navbar-inverse h6,
              .navbar-default p, .navbar-inverse p
               {
                  margin-top: 0px;
                  margin-bottom: 2px;
                  padding: 6px 6px 6px 6px;
                  padding-left: 15px;
                  color:#ffffff !important;
              }

              .dropdown-menu h1, .dropdown-menu h2, .dropdown-menu h3, .dropdown-menu h4, .dropdown-menu h5, .dropdown-menu h6,
              .dropdown-menu p
               {
                  color:#666666 !important;
              }

              .navbar-default p, .navbar-inverse p{
                  padding-top: 0px !important;
              }
              .admin_content .pagination{
                  margin: 0 !important;
                  margin-top: 10px !important;
              }
              .admin_content table{
                  margin: 0 !important;
                  margin-top: 10px !important;
              }
              .clearfix{
                  line-height: 1px !important;
                  height:1px !important;
                  max-height:1px !important;
                  float:none;
                  clear: both;
              }

              .draggable{
                  position: absolute;
                  background-color: #ececec !important;
              }

              .navbar{
                  min-height:50px;

              }
              .navbar-nav>li>a {
                   padding-top: 16px;
                   padding-bottom: 10px;
              }
              .subbar{
                  height:auto !important;
                  color:#ffffff;
              }
      </style>
    </head>
    <body>
      <?php echo $this->element('navigation/top_nav_full_width'); ?>
      <div class="container-fluid">
        <div class="row" >
              <?php if ($layoutpos == "a"): ?>
                  <div id = "sidebar" class="col-sm-3 col-md-2 sidebar well">
                    <?php  echo($this->element('navigation/admin_sidenav'));  ?>
                  </div>
                  <div class="content admin_content pr_0 pl_0  layout_a col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2" id = "content">


                      <?php echo $content_for_layout; ?>


                  </div>
              <?php else: ?>
                  <div id = "sidebar" class="col-sm-3 col-md-2 sidebar well hidden">
                      <div class= "bm_2_em tm_10_px" style="width:100%; text-align:center;" >
                          <img data-src="holder.js/240x140" class="img-circle" alt="Avatar Picture" src="/owls/badge_med_general.png" data-holder-rendered="true" style="width: 140px; height: 140px;">
                      </div>
                      <div id = "sidenav_content"></div>
                      <div class="bm_1em">&nbsp;</div>
                  </div>
                  <div class="content admin_content pr_0 pl_0  col-sm-12 col-md-12 layout_b" id = "content">


                    <?php echo $content_for_layout; ?>

                    
                  </div>
          <?php endif ?>
          <div class = "clearfix bm_1_em">&nbsp;</div>
        </div>
      <div class = "clear clearfix bm_5_em" >&nbsp;</div>
      </div>
      <div style = "height:0px;">&nbsp;</div>

      <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation"></div>

      <script type="text/javascript" src = '/PagekwikTools/js/ckeditor/ckeditor.js'></script>
      <script src="/Quizmodules/js/quiz_helper.js"></script>
      <script src="/PagekwikTools/js/ajax_slider.js"></script>

      <script src="/Quizmodules/js/pnotify.custom.min.js"></script>
      <script src="/Quizmodules/js/quiz_notification.js"></script>
      <script type="text/javascript" src="/Audiomanager/soundmanager/soundmanager2.js"></script>
      <script type="text/javascript" src="/Audiomanager/soundmanager/inlineplayer.js"></script>
      <script type="text/javascript" charset="utf-8" src = '/PagekwikTools/js/zeroclipboard/ZeroClipboard.js' ></script>
      <script src="/js/siteJsWrapper.js"></script>
      <script type="text/javascript" charset="utf-8" src = '/Quizmodules/js/quiz.js' ></script>
      <script src="/js/course_select.js"></script>



      <?php
     /// debug($additional_scripts);
   //   debug($this->additional_scripts);
          echo $this->Js->writeBuffer();
          echo $this->Session->flash('flash', array(
              'element' => 'flash',
              'params' => array('plugin' => 'PagekwikTools')
          ));
          // this flash should probably be included in the sites, rather than an a plugin.
          // also the site flash currently resides in the quiz plugin. that needs to be relocated to somewhere universal.
      ?>
      <script type="text/javascript">
          $( document ).ready(function() {  onPageLoad(); });
      </script>
      <?php echo $this->element('misc_ui/layout_toggle_button_dashboard'); ?>

      <?php foreach ($additional_scripts as $key => $value): ?>
        <script type="text/javascript" src='<?php echo $value ?>.js'></script>
      <?php endforeach; ?>

      <!-- Event Modal -->
      <div id="myModal" class="modal fade">
          <div class="modal-dialog">
              <div class="modal-content">
                      <div class="modal-body">
                          <p>Loading...</p>
                      </div>
              </div>
              <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
      </div>
      <!-- Event modal -->



    
    </body>
  </html>
