<?php $alias = 'avatars'; ?>

<div class="">
	<div class="navbar-inverse">
		<div class = 'pull-left'>
			<h2 ><?php echo __( __('Avatars'));?></h2>
			<p><?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')));?></p>
		</div>
		<div class = ''>
			<div class="pull-right col-lg-6 tm_2_em">



	
			</div>
		</div>
		<div class = 'clear'></div>
	</div>



	<div class="admin_padding">
		
		<?php echo $this->BootstrapPaginator->pagination(); ?>
			<a href = '/my/avatars/add/' class = '  btn btn-default btn-sm pull-right tm_1_em bm_1_em' style=""><div class="glyphicon glyphicon-plus-sign">&nbsp;</div>New Avatar&nbsp;</a>
		<table class="table table-striped table-bordered table-hover">
		<tr>
			<th class = "span1" style = "text-align:center" ><?php echo $this->BootstrapPaginator->sort('id');?></th>
			<th><?php echo $this->BootstrapPaginator->sort('name');?></th>
			<th><?php echo $this->BootstrapPaginator->sort('filepath');?></th>
			<th> </th>
		</tr>
		
		<?php foreach ($avatars as $avatar): ?>
		
		<tr>
			<td class = "" style = "text-align:center" ><?php echo h($avatar['Avatar']['id']); ?></td>
			<td>
				<?php echo h($avatar['Avatar']['name']); ?>
			</td>
			<td><?php echo h($avatar['Avatar']['filepath']); ?>&nbsp;</td>

				<td>

					<ul style = "text-align:center; list-style:none; padding:0" >
			            <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								Actions
							</a>
							<ul class="dropdown-menu" role="menu" style = "text-decoration:none;">

								<li>
									<a class = "" target = '_blank' href="/<?php echo $alias ?>/view/<?php echo $avatar['Avatar']['id']; ?>/">View</a>
							    </li>

								<li>
									<a class = "
	" href="/my/<?php echo $alias ?>/edit/<?php echo $avatar['Avatar']['id']; ?>/">Edit</a>
							    </li>

								<li>
									<?php  echo $this->Form->postLink( __('Delete'), '/my/avatars/delete/'.$avatar['Avatar']['id'] , array($avatar['Avatar']['id']) ,__('Are you sure you want to delete avatar: %s?', $avatar['Avatar']['name']) ); ?>
							    </li>	

				

							</ul>
			            </li>
		         	</ul>
				</td>



		</tr>
		<?php endforeach; ?>
		</table>
		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
</div>
