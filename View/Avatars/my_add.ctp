
<div class="navbar-inverse">
	<div class = 'pull-left'>
		<h2>
			<?php echo __('Add Avatar'); ?>
		</h2>
	</div>
	<div class = 'clear'></div>
</div>







<div class="admin_padding tm_1_em">
<?php echo $this->Form->create('Avatar'); ?>
	<fieldset>
	<?php
		echo $this->BootstrapForm->input('name');
		echo $this->BootstrapForm->input('filepath',array('options'=>$fileOptions));
	?>
	</fieldset>
<?php echo $this->Form->submit('Submit',array('class'=>'btn tm_1_em pull-right')); ?>
<?php echo $this->Form->end(); ?>
</div>
