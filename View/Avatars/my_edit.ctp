<div class="navbar-inverse">
	<div class = 'pull-left'>
		<h2>
			<?php echo __('Add Avatar'); ?>
		</h2>
	</div>
	<div class = 'clear'></div>
</div>




<div class = "admin_padding tm_2_em col-md-10">
	<?php echo $this->BootstrapForm->create('Avatar', array('class' => ''));?>
	<fieldset>
		<?php
			echo $this->BootstrapForm->input('name',array('class'=>'col-md-12'));
			echo '<div class = "clearfix bm_2_em">&nbsp;</div>';
			echo $this->BootstrapForm->input('filepath', $fileOptions);
			echo $this->BootstrapForm->hidden('id');
		?>
		<div class = "clear bm_1_em"></div>
		<?php echo $this->BootstrapForm->submit(__('Submit'),array('div'=>'pull-right'));?>
	</fieldset>
	<?php echo $this->BootstrapForm->end();?>
</div>
