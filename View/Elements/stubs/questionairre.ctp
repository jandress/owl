<div class = "container">
    <h2>Please Rate you skill level and interest in the following subject.</h2>
    <table class = "table table-striped table-border tm_4_em">
        <tr>
            <td class = "col-sm-6"></td>
            <td >Skill / experience / mastery</td>
            <td style = "text-align:center">interest</td>
        <?php
          $variable = array(
            "How many Chords do you know?",
            "Scales",
            "Diads",
            "Triads",            
            "Intervals",
            "Arpeggios",
            "Quads"                        
          );
          $select_options = array(
            'options'=>array(
              '1'=>"1",
              "2"=> "2",
              "3"=> "3",
              "4"=> "4",
              "5"=> "5"
            )
            
          );
          foreach ($variable as $key => $value): ?>
            <tr>
                <td>
                    <?php echo $value; ?>
                </td>
                <td  style = "text-align:center">
                  <? 
                    echo $this->BootstrapForm->select(
                      null,
                      $select_options
                    ); 
                  ?>
                </td>
                <td style = "text-align:center">
                  <span class = "glyphicon glyphicon-star-empty">&nbsp;</span>
                  <span class = "glyphicon glyphicon-star-empty">&nbsp;</span>
                  <span class = "glyphicon glyphicon-star-empty">&nbsp;</span>
                  <span class = "glyphicon glyphicon-star-empty">&nbsp;</span>
                  <span class = "glyphicon glyphicon-star-empty">&nbsp;</span>
                </td>
          </tr>
          <?php endforeach; ?>
    </table>

    <a class = "btn btn-default pull-right col-sm-6 tm_2_em" >Submit</a>


</div>










