<div class = "container">
    <h2 style = " font-size:3em; letter-spacing:1px">Practice</h2>
    <table class = "tm_2_em table table-striped table-border">
      <thead>
        <tr style = "font-weight:bold; font-size:1.25em; color:#ececec">
            <td class = "col-sm-6">Area</td>
            <td >Title</td>
            <td >Esitmated Time</td>
            <td style = "text-align:center">&nbsp;</td>
        </tr>
      </thead>            
        <?php
          $variable = array(
            array('subject'=>'Scales','title'=>"E Major Position 2, Play Then Reverse"),
            array('subject'=>'Scales / Chords','title'=>"Diads"),
            array('subject'=>'Scales / Chords','title'=>"Triads"),
            array('subject'=>'Intervals','title'=>"Intervals"),
            array('subject'=>'Arpeggios','title'=>"Arpeggios")
          );
          $select_options = array(
            'options'=>array(
              '1'=>"1",
              "2"=> "2",
              "3"=> "3",
              "4"=> "4",
              "5"=> "5"
            )
          );
          foreach ($variable as $key => $value): ?>
          
            <tr>
              
                <td>
                  <a href = "#t"><?php echo $value['subject']; ?></a>
                </td>
                <td  style = "text-align:right" >
                  <?php echo $value['title']; 
                      // echo $this->BootstrapForm->select(
                      //   null,
                      //   $select_options
                      // ); 
                    ?>
                </td>
                <td style = "text-align:right">
                  5:00
                </td>
                <td style = "text-align:right">






<div class="col-sm-3 pull-right">

      <div class="dropdown pull-right lm_10_px rm_10_px">
          <a class="dropdown-toggle btn btn-default btn-sm" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
            Edit &nbsp;<b class="caret">&nbsp;</b>
          </a>
          <ul class="dropdown-menu " role="menu" aria-labelledby="dLabel">
              <li>
                <a href="/my/activities/edit/2/" class="  ">Swap For Another Activity</a>
              </li>
              <li>
                <a href="/my/activities/edit/2/" class="  ">Remove Activity</a>
              </li>              
              <li>
                <a href="/my/activities/edit/2/" class="  ">Change Time Alotment</a>
              </li>                            

          </ul>
    </div>

</div>


















                </td>
          </tr>


          </a>

          <?php endforeach; ?>
    </table>

                    <div style = "padding:60px;" class = "col-sm-12">
                      <a class = "btn btn-block btn-success pull-right " style = "font-size:2em; font-weight:bold; letter-spacing:2px" >Get Started</a>
                    </div>


    <div>
            <ul>
              <li>Figure out how to implement this ui into the Profile</li>
              <li>Create PracticeSessionModel with a function called "getNextActivity() and getActivity(i)"</li>
              <li>Everything should work off of Session Vars</li>
              <li>Not sure how time keeping should work.  Also not sure if it is important or not beyond giving a basic estimate</li>
              <li> 
                getNextActivity() should call getActivity(i) and be able to return skipped activities or Session Complete or false 
              </li>
              <li> You also need to figure out how to represent porogress in a practice session in the screen UI.   Maybe include a session dropdown menu.
            </ul>
    </div>
</div>






