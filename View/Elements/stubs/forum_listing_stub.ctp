
<?php
    $badges = array(
        "badge_small_general.png",
        "badge_small_grad.png",
        "badge_small_greek.png",
        "badge_small_hipster.png",
        "badge_small_judge.png",
        "badge_small_nerd.png",
        "badge_small_sherlock.png",
        "badge_small_twain.png",
        "badge_small_world.png",
        "badge_small_zombie.png"
    );
 ?>




<div class="forum_entry ">


  <div class="entry_container">
    <div style="width:120px">
      <div class="forum_voter_arrows">
          <a href="/my/forumentry/2"><div class="arrow up">&nbsp;</div></a>
          <div class="rank_number">
            <?php echo $recursion; ?>
          </div>
          <a href="/my/forumentry/2"><div class="arrow">&nbsp;</div></a>
      </div>
      <div class="avatar pull-left" style="margin-top:5px;">
        <img src="/owls/small/<?php echo $badges[rand(1,8)] ?>" alt="" />
      </div>
    </div>

    <!--  -->
    <div class="col-sm-10 forum_entry_field">
      <div class=" col-sm-10 pl_0" >
        <div class = "r5" style="border:1px solid #e3e3e3; padding:10px; ">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <ul>
          <li>1 Comment</li>
          <li>Share</li>
          <li>Submitted yesterday by <a href="/my/forumentry/2">lorem ipsum,</a></li>
          <li>6 Comments</li>
          <li><a href = "">Reply</a></li>
        </ul>
      </div>
    </div>
  </div>

<div class="clear clearfix tm_3_px">&nbsp;</div>

  <?php 
  // $recursion = 4;
  if ($recursion > 0): ?>
      <div class=" child_comment ">
        <?php
          for ($i=0; $i < 2; $i++) {
             echo $this->element('stubs/forum_listing_stub', array('recursion'=>($recursion-1)));
             echo '<div class="clear clearfix tm_3_px">&nbsp;</div>';
          }
       ?>
      </div>
  <?php endif; ?>

</div>
<div class="clear clearfix">&nbsp;</div>
