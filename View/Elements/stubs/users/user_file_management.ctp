
<div class="">
    <div id="waveform_container">
        <canvas id="waveFormBackground" style="border: 1px; height: 140px; position: absolute; background-color: rgb(255, 255, 255); width: 1110px;" class="gs_0 " width="1110" height="145"></canvas>
        <canvas id="waveFormBox" style="border: 1px; height: 140px; position: relative; width: 1110px;" class="gs_0 " width="1110" height="145"></canvas>
    </div>
</div>




<div class="">
    <div class="nav navbar navbar-primary">
        <div class="btn-group">
            <div>

                <a id="step_back_btn" class="btn">
                    <span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span>
                </a>
                
                <a id="back_btn" class="btn">
                    <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
                </a>
                <a id="play_btn" class="btn">
                    <span id="playpause" class="glyphicon glyphicon-play" aria-hidden="true"></span>
                </a>
                <a id="forward_btn" class="btn">
                    <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
                </a>
                <a id="step_forward_btn" class="btn">
                    <span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span>
                </a>
                <a id="stop_btn" class="btn">
                    <span id="playpause" class="glyphicon glyphicon-stop" aria-hidden="true"></span>
                </a>												
            </div>		
        </div>	
        <span id="counter">00:00</span>
        <div id="songLabel" style="color:#ffffff" class="pull-right tm_10_px"></div>
        <div class="clear clearfix bm_3_px">&nbsp;</div>
    </div>	
</div>










<script>





var filepath = "/users/be0fbff5ded65f6524a26955bb72965cb6823249/";
//  console.log(filepath);

// Waveform
var waveFormBoxCanvas = document.getElementById("waveFormBox");
var waveFormBackgroundCanvas = document.getElementById("waveFormBackground");

var waveFormBoxContext = waveFormBoxCanvas.getContext("2d");
var waveFormBackgroundContext = waveFormBackgroundCanvas.getContext("2d");
//---------------
var duration = 0;
waveFormBoxContext.fillStyle = "#333333";
waveFormBackgroundContext.fillStyle = "#333333";

var boxWidth = $("#waveform_container").width();
var boxHeight = $("#waveform_container").height();
$("#waveFormBox").width(boxWidth)
$("#waveFormBackground").width(boxWidth)


waveFormBoxContext.canvas.width  = waveFormBackgroundContext.canvas.width = boxWidth;
waveFormBoxContext.canvas.height  = waveFormBackgroundContext.canvas.height = boxHeight;

// end Waveform

//player stuff
window.AudioContext = window.AudioContext || window.webkitAudioContext;
const audioContext = new AudioContext();
let currentBuffer = null;







var timer = setInterval(function(){
    if(player.currentTime == 0) return;
    function millisToMinutesAndSeconds(millis) {
      var minutes = Math.floor(millis / 60000);
      var seconds = ((millis % 60000) / 1000).toFixed(0);
      return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    }
    var per = player.currentTime / player.duration
    $("#counter").html(millisToMinutesAndSeconds(player.currentTime*1000)+" / "+millisToMinutesAndSeconds(duration*1000))
    waveFormBackgroundContext.clearRect(0, 0, boxWidth, boxHeight);
    waveFormBackgroundContext.fillStyle = "#999999";
    waveFormBackgroundContext.beginPath();
    waveFormBackgroundContext.fillRect(0, 0, boxWidth*per, boxHeight);

}, 1000)


const visualizeAudio = url => {
    fetch(url)
    .then(response => response.arrayBuffer())
    .then(arrayBuffer => audioContext.decodeAudioData(arrayBuffer))
    .then(audioBuffer => visualize(audioBuffer));
};
const visualize = audioBuffer =>{
    B = filterData(audioBuffer)
    var j=0			
    var K 
    waveFormBoxContext.clearRect(0, 0, boxWidth, boxHeight);
    duration = player.duration;
    waveFormBoxContext.beginPath();
    for (var i = 0; i < B.length; i++) {
        // waveFormBoxContext.moveTo(j, boxHeight/2);
        K = B[i]
        k = (boxHeight/2) - (boxHeight/2)*K
        waveFormBoxContext.moveTo(j, k);
        // waveFormBoxContext.moveTo(j, boxHeight/2);				
        k2 = (boxHeight/2) + (boxHeight/2)*K
        waveFormBoxContext.lineTo(j, k2);
        j+=3
    }
    waveFormBoxContext.stroke();			
}


const filterData = audioBuffer => {
  const rawData = audioBuffer.getChannelData(0); // We only need to work with one channel of data
   // const rawData = normalizeData(audioBuffer.getChannelData(0))
  // console.log(rawData)
  const samples = boxWidth; // Number of samples we want to have in our final data set
  const blockSize = Math.floor(rawData.length / samples); // the number of samples in each subdivision
  const filteredData = [];
  for (let i = 0; i < samples; i++) {
    let blockStart = blockSize * i; // the location of the first sample in the block
    let sum = 0;
    for (let j = 0; j < blockSize; j++) {
      sum = sum + Math.abs(rawData[blockStart + j]) // find the sum of all the samples in the block
    }
    filteredData.push(sum / blockSize); // divide the sum by the block size to get the average
  }
  return filteredData;
}






 var player = new Audio(); 


$(window).ready(function(){

        $(".tracklist").click(function(e){

            var src = filepath+"/"+$(e.currentTarget).attr("href").replace("#","");
            console.log(src);
            $("#songLabel").html($(e.currentTarget).attr("href").replace("#",""))   
            console.log(filepath)
            console.log(filepath)
            console.log(src)
            player.src = src
            player.play();
            visualizeAudio(src)			
            return false
        });


        $(".songlist_link").click(function(e){

            var src = filepath+$(e.currentTarget).attr("href").replace("#","");
            $("#songLabel").html($(e.currentTarget).attr("href").replace("#",""))
            player.src = src
            player.play();
            visualizeAudio(src)			
            return false
        });


        $("#play_btn").click(function(){
            console.log(player)
            if($("#playpause").hasClass("glyphicon-play")){
                $("#playpause").removeClass("glyphicon-play").addClass("glyphicon-pause");	
                // player =  new Audio("./mp3s/"+track).play()
                player.play();

            }else{
                $("#playpause").removeClass("glyphicon-pause").addClass("glyphicon-play");	
                player.pause();
            }	
        })

        $("#waveform_container").click(function(e){
            const rect = waveFormBackgroundCanvas.getBoundingClientRect()
            const x = event.clientX - rect.left
            const y = event.clientY - rect.top
            player.currentTime = (x  / boxWidth )*player.duration
            return false
        });


        $("#stop_btn").click(function(e){
            player.pause();
            player.currentTime = 0;
            clearInterval(timer);
            console.log("stop")

        });





})





























</script>    <div>
	<div class="admin_file_listing_toolbar navbar navbar-default " style="z-index:10">
	<div class="admin_file_listing_container">
		<div class="container-fluid bm_4_px">		
			

		
			
			<a class="btn btn-sm btn-default" href="/my/userassets/"><span class="glyphicon glyphicon-home"></span></a>
			

						
			
			
			 

			<div class="upload_toolbar_link btn-group  pull-right">
				
				

					<a class="btn btn-sm btn-default fb_ajax_link" href="/my/userassets/addir/">
						<span class="icon-folder-close ">&nbsp;</span>
						 New Directory.
					</a>

					<a class="btn btn-sm btn-default fb_ajax_link" href="/my/userassets/upload/">
						<span class="icon-upload ">&nbsp;</span>
						Upload New File.
					</a>

							</div>

		</div>		
	
	</div>
	<div class="clear">&nbsp;</div>			
</div>
	
	

<table id="projects" class="table table-striped table-bordered table-hover">
	<thead>
		<tr class="gb_28">
			<th class="col-md-1">&nbsp;</th>
			<th class="span9">Name</th>
			<th class="actions col-md-2" style="text-align:center">Options</th>
		</tr>
	</thead>			
	<tbody>
			<tr id="0" class="altrow">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
				</span> &nbsp;<span class="glyphicon glyphicon-folder-close"></span>&nbsp; <span class="file_listing_label"><a class="fb_ajax_link" href="/my/userassets/collaborations">collaborations</a>				</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">

									<li>	
										<a target="_blank" href="/playtrack/collaborations">Open File in Standalone Player.collaborations</a>
									</li>			           


									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/collaborations">View</a>
									</li>			                	

									
									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/collaborations">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/collaborations">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/collaborations">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/collaborations">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/collaborations">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;collaborations\&quot;?');" class="" href="/assets/rmdir/collaborations">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
			<tr id="1">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
				</span> &nbsp;<span class="glyphicon glyphicon-folder-close"></span>&nbsp; <span class="file_listing_label"><a class="fb_ajax_link" href="/my/userassets/images">images</a>				</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">

									<li>	
										<a target="_blank" href="/playtrack/images">Open File in Standalone Player.images</a>
									</li>			           


									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/images">View</a>
									</li>			                	

									
									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/images">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/images">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/images">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/images">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/images">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;images\&quot;?');" class="" href="/assets/rmdir/images">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
			<tr id="2" class="altrow">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
				</span> &nbsp;<span class="glyphicon glyphicon-folder-close"></span>&nbsp; <span class="file_listing_label"><a class="fb_ajax_link" href="/my/userassets/jams">jams</a>				</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">

									<li>	
										<a target="_blank" href="/playtrack/jams">Open File in Standalone Player.jams</a>
									</li>			           


									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/jams">View</a>
									</li>			                	

									
									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/jams">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/jams">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/jams">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/jams">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/jams">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;jams\&quot;?');" class="" href="/assets/rmdir/jams">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
			<tr id="3">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
				</span> &nbsp;<span class="glyphicon glyphicon-folder-close"></span>&nbsp; <span class="file_listing_label"><a class="fb_ajax_link" href="/my/userassets/private">private</a>				</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">

									<li>	
										<a target="_blank" href="/playtrack/private">Open File in Standalone Player.private</a>
									</li>			           


									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/private">View</a>
									</li>			                	

									
									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/private">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/private">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/private">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/private">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/private">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;private\&quot;?');" class="" href="/assets/rmdir/private">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
			<tr id="4" class="altrow">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
				</span> &nbsp;<span class="glyphicon glyphicon-folder-close"></span>&nbsp; <span class="file_listing_label"><a class="fb_ajax_link" href="/my/userassets/public">public</a>				</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">

									<li>	
										<a target="_blank" href="/playtrack/public">Open File in Standalone Player.public</a>
									</li>			           


									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/public">View</a>
									</li>			                	

									
									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/public">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/public">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/public">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/public">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/public">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;public\&quot;?');" class="" href="/assets/rmdir/public">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
			<tr id="5">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
				



							<a href="#sky_lake.jpg" class="tracklist">
								sky_lake.jpg							</a>
										</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">

									<li>	
										<a target="_blank" href="/playtrack/sky_lake.jpg">Open File in Standalone Player.sky_lake.jpg</a>
									</li>			           


									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/sky_lake.jpg">View</a>
									</li>			                	

									
									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/sky_lake.jpg">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/sky_lake.jpg">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/sky_lake.jpg">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/sky_lake.jpg">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/sky_lake.jpg">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;sky_lake.jpg\&quot;?');" class="" href="/assets/rmdir/sky_lake.jpg">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
			<tr id="6" class="altrow">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
				</span> &nbsp;<span class="glyphicon glyphicon-folder-close"></span>&nbsp; <span class="file_listing_label"><a class="fb_ajax_link" href="/my/userassets/tracks">tracks</a>				</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">

									<li>	
										<a target="_blank" href="/playtrack/tracks">Open File in Standalone Player.tracks</a>
									</li>			           


									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/tracks">View</a>
									</li>			                	

									
									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/tracks">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/tracks">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/tracks">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/tracks">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/tracks">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;tracks\&quot;?');" class="" href="/assets/rmdir/tracks">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
			<tr id="7">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
				</span> &nbsp;<span class="glyphicon glyphicon-folder-close"></span>&nbsp; <span class="file_listing_label"><a class="fb_ajax_link" href="/my/userassets/uploads">uploads</a>				</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">

									<li>	
										<a target="_blank" href="/playtrack/uploads">Open File in Standalone Player.uploads</a>
									</li>			           


									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/uploads">View</a>
									</li>			                	

									
									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/uploads">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/uploads">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/uploads">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/uploads">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/uploads">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;uploads\&quot;?');" class="" href="/assets/rmdir/uploads">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
		</tbody>
</table>	
</div>



