<div class = "container_fluid">







    <div id="player_screen" class = "container-fluid">



    <div class = "nav navbar">
            <div class="btn-group" >
                <div>
                    <a id = "step_back_btn" class = "btn">
                        <span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span>
                    </a>
                    
                    <a id = "back_btn" class = "btn">
                        <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
                    </a>
                    <a id = "play_btn" class = "btn">
                        <span id="playpause" class="glyphicon glyphicon-play" aria-hidden="true"></span>
                    </a>
                    <a id = "forward_btn" class = "btn">
                        <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
                    </a>
                    <a id = "step_forward_btn" class = "btn">
                        <span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span>
                    </a>
                    <a id = "stop_btn" class = "btn">
                        <span id="playpause" class="glyphicon glyphicon-stop" aria-hidden="true"></span>
                    </a>												
                </div>		
                <div class = "clear clearfix bm_3_px">&nbsp;</div>
            </div>	
            <span id = "counter">00:00</span>
            <div class = "clear clearfix bm_3_px">&nbsp;</div>
        </div>



        <?php
                    $filepath = "users/".$current_user['User']['user_dir']."/jams/";
                    $initial_file = '/users/be0fbff5ded65f6524a26955bb72965cb6823249/jams/g.mp3';
?>
            
        
        <div class = "clear clearfix">&nbsp;</div>	
        <div id = "waveform_container">
            <canvas id = "waveFormBackground" style = "border: 1px; height:140px; position:absolute; background-color:#ffffff " class = "gs_0 "></canvas>
            <canvas id = "waveFormBox" style = "border: 1px; height:140px; position:relative; " class = "gs_0 "></canvas>
        </div>


        

        </div>
    </div><!-- </end container div> -->


    </div>    









<div class = "container-fluid tm_1_em" style = "padding:30px; padding-top:20px">


    <div style="width:120px">
            <div class="forum_voter_arrows">
                <a href="#"><div class="arrow up">&nbsp;</div></a>
                <div class="rank_number">
                    137
                </div>
                <a href="#"><div class="arrow">&nbsp;</div></a>
            </div>

            <div  style = "width:80px"  class="avatar pull-left">
                <img style = "width:100%" src="/users/be0fbff5ded65f6524a26955bb72965cb6823249/images/cat.png" alt="" />
            </div>
    </div>

    <div class="col-sm-10 forum_entry_field">
        <h3>Lorem ipsum dolor sit amet</h3>
        <div class="">
        <h6 class="gt_2" style="display:inline">Submitted yesterday by</h6> <h6 class="gt_15" style="display:inline">lorem ipsum</h6><h6 class="gt_15" style="display:inline">, 6 Comments</h6>
        </div>
    </div>

    <div class = "clear clearfix tm__em">&nbsp</div>



    <div class="tm_1_em col-sm-10 pl_0">
            <div class="r5" style="border:1px solid #e3e3e3; padding:10px;">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class = "clear clearfix tm_1_em">&nbsp</div>            
            <span style ="margin-top:25px;">

              <span >Share</span>
            </span>
          </div>



</div>








<div class="forum_entry">
        <?php
            $badges = array(
                "badge_small_general.png",
                "badge_small_grad.png",
                "badge_small_greek.png",
                "badge_small_hipster.png",
                "badge_small_judge.png",
                "badge_small_nerd.png",
                "badge_small_sherlock.png",
                "badge_small_twain.png",
                "badge_small_world.png",
                "badge_small_zombie.png"
            );
         ?>

       
        <div class="clear clearfix">&nbsp;</div><hr/>
        <!-- Text Edit Area -->
        <div class="col-sm-6">
          <textarea rows="1" cols="1" name="text" data-event-action="comment" data-type="link"></textarea>
          <a class ="btn btn-primary pull-right" href="#">Submit</a>
        </div>
        <div class="clear clearfix ">&nbsp;</div>


        <div class="tm_5_em">
          <?php
              for ($i=0; $i < 2; $i++) {
                $recursion = rand(1,8);
                echo $this->element('stubs/forum_listing_stub', array('recursion'=>$recursion,'badges'=>$badges));
              }
           ?>
        </div>

</div>






























        <script type="text/javascript">



            var filepath = "/<?php echo $filepath;?>";

            // Waveform
            var waveFormBoxCanvas = document.getElementById("waveFormBox");
            var waveFormBackgroundCanvas = document.getElementById("waveFormBackground");

            var waveFormBoxContext = waveFormBoxCanvas.getContext("2d");
            var waveFormBackgroundContext = waveFormBackgroundCanvas.getContext("2d");
            //---------------
            var duration = 0;
            waveFormBoxContext.fillStyle = "#000000";
            waveFormBackgroundContext.fillStyle = "#000000";

            var boxWidth = $("#player_screen").width();
            var boxHeight = $("#waveFormBox").height();
            $("#waveFormBox").width(boxWidth)
            $("#waveFormBackground").width(boxWidth)
            waveFormBoxContext.canvas.width  = waveFormBackgroundContext.canvas.width = boxWidth;
            waveFormBoxContext.canvas.height  = waveFormBackgroundContext.canvas.height = boxHeight;
            // end Waveform

            //console.log(boxWidth,waveFormBoxContext.canvas.width, $("#waveFormBox").width())
            window.AudioContext = window.AudioContext || window.webkitAudioContext;
            const audioContext = new AudioContext();
            let currentBuffer = null;

            var timer = setInterval(function(){
                if(player.currentTime == 0) return;
                function millisToMinutesAndSeconds(millis) {
                var minutes = Math.floor(millis / 60000);
                var seconds = ((millis % 60000) / 1000).toFixed(0);
                return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
                }
                var per = player.currentTime / player.duration
                $("#counter").html(millisToMinutesAndSeconds(player.currentTime*1000)+" / "+millisToMinutesAndSeconds(duration*1000))
                waveFormBackgroundContext.clearRect(0, 0, boxWidth, boxHeight);
                waveFormBackgroundContext.fillStyle = "#999999";
                waveFormBackgroundContext.beginPath();
                waveFormBackgroundContext.fillRect(0, 0, boxWidth*per, boxHeight);
            }, 1000)


            const visualizeAudio = url => {
                fetch(url)
                .then(response => response.arrayBuffer())
                .then(arrayBuffer => audioContext.decodeAudioData(arrayBuffer))
                .then(audioBuffer => visualize(audioBuffer));
            };
            const visualize = audioBuffer =>{
                B = filterData(audioBuffer)
                var j=0			
                var K 
                waveFormBoxContext.clearRect(0, 0, boxWidth, boxHeight);
                duration = player.duration;
                waveFormBoxContext.beginPath();
                for (var i = 0; i < B.length; i++) {
                    // waveFormBoxContext.moveTo(j, boxHeight/2);
                    K = B[i]
                    k = (boxHeight/2) - (boxHeight/2)*K
                    waveFormBoxContext.moveTo(j, k);
                    // waveFormBoxContext.moveTo(j, boxHeight/2);				
                    k2 = (boxHeight/2) + (boxHeight/2)*K
                    waveFormBoxContext.lineTo(j, k2);
                    j+=3
                }
                waveFormBoxContext.stroke();			
            }
            

            const filterData = audioBuffer => {
            const rawData = audioBuffer.getChannelData(0); // We only need to work with one channel of data
            // const rawData = normalizeData(audioBuffer.getChannelData(0))
            // console.log(rawData)
            const samples = boxWidth; // Number of samples we want to have in our final data set
            const blockSize = Math.floor(rawData.length / samples); // the number of samples in each subdivision
            const filteredData = [];
            for (let i = 0; i < samples; i++) {
                let blockStart = blockSize * i; // the location of the first sample in the block
                let sum = 0;
                for (let j = 0; j < blockSize; j++) {
                sum = sum + Math.abs(rawData[blockStart + j]) // find the sum of all the samples in the block
                }
                filteredData.push(sum / blockSize); // divide the sum by the block size to get the average
            }
            return filteredData;
            }

            var player = new Audio("<?php echo $initial_file; ?>");
            player.src = "<?php echo $initial_file; ?>";
            visualizeAudio(player.src )
            player.play();

            




            $("#play_btn").click(function(){
                console.log(player)
                if($("#playpause").hasClass("glyphicon-play")){
                    $("#playpause").removeClass("glyphicon-play").addClass("glyphicon-pause");	
                    // player =  new Audio("./mp3s/"+track).play()
                    player.play();

                }else{
                    $("#playpause").removeClass("glyphicon-pause").addClass("glyphicon-play");	
                    player.pause();
                }	
            })


            $(".tracklist").click(function(e){
                var src = filepath+$(e.currentTarget).attr("href");
                $("#songLabel").html($(e.currentTarget).attr("href"))
                player.src = src
                player.play();
                visualizeAudio(src)			
                return false
            });
            $(".songlist_link").click(function(e){
                var src = filepath+$(e.currentTarget).attr("href").replace("#","");
                $("#songLabel").html($(e.currentTarget).attr("href").replace("#",""))
                player.src = src
                player.play();
                visualizeAudio(src)			
                return false
            });

            $("#waveform_container").click(function(e){
                const rect = waveFormBackgroundCanvas.getBoundingClientRect()
                const x = event.clientX - rect.left
                const y = event.clientY - rect.top
                player.currentTime = (x  / boxWidth )*player.duration
                return false
            });


            $("#stop_btn").click(function(e){
                player.pause();
                player.currentTime = 0;
                clearInterval(timer);
                console.log("stop")

            });

            

            function adjust(){
                var a = $("#image_banner").height();
                var b = $("#banner_image").height()
                var c = -((b-a)/1.5)
                console.log(a, b,c)
                // $("#image_banner").css("margin-top")
                $('#banner_image').css('margin-top',c+"px");

            }


            $("document").ready(function(){
                adjust();
                // document.querySelector('#banner_image').style.cssText = 'position: absolute; top: 50%; transform: translateY(-50%);';

            })



            window.onresize = function() {
                // console.log("rs")
                adjust();

            }
            

            // visualizeAudio("./mp3s/ecoomic_systems.wav")

    /*

    AudioBuffer {length: 5061147, duration: 114.76523809523809, sampleRate: 44100, numberOfChannels: 1}
    duration
    : 
    114.76523809523809
    length
    : 
    5061147
    numberOfChannels
    : 
    1
    sampleRate
    : 
    44100
    [[Prototype]]
    : 
    AudioBuffer





    AudioBuffer {length: 18313627, duration: 415.27498866213153, sampleRate: 44100, numberOfChannels: 2}
    duration
    : 
    415.27498866213153
    length
    : 
    18313627
    numberOfChannels
    : 
    2
    sampleRate
    : 
    44100
    [[Prototype]]
    : 
    AudioBuffer


    */



        </script>