<div class="forum_entry">
        <?php
            $badges = array(
                "badge_small_general.png",
                "badge_small_grad.png",
                "badge_small_greek.png",
                "badge_small_hipster.png",
                "badge_small_judge.png",
                "badge_small_nerd.png",
                "badge_small_sherlock.png",
                "badge_small_twain.png",
                "badge_small_world.png",
                "badge_small_zombie.png"
            );
         ?>

        <div style="width:120px">
          <div class="forum_voter_arrows">
              <a href="#"><div class="arrow up">&nbsp;</div></a>
              <div class="rank_number">
                  137
              </div>
              <a href="#"><div class="arrow">&nbsp;</div></a>
          </div>
          <div class="avatar pull-left">
            <img src="/owls/small/<?php echo $badges[rand(1,8)] ?>" alt="" />
          </div>
        </div>




        <div class="col-sm-10 forum_entry_field">
          <h3>Lorem ipsum dolor sit amet</h3>
          <div class="">
            <h6 class="gt_2" style="display:inline">Submitted yesterday by</h6> <h6 class="gt_15" style="display:inline">lorem ipsum</h6><h6 class="gt_15" style="display:inline">, 6 Comments</h6>
          </div>
          <div class="tm_1_em col-sm-10 pl_0" >
            <div class = "r5" style="border:1px solid #e3e3e3; padding:10px;">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <ul>
              <li>1 Comment</li>
              <li>Share</li>
            </ul>
          </div>
        </div>


        <div class="clear clearfix">&nbsp;</div><hr/>
        <!-- Text Edit Area -->
        <div class="col-sm-6">
          <textarea rows="1" cols="1" name="text" data-event-action="comment" data-type="link"></textarea>
          <a class ="btn btn-primary pull-right" href="#">Submit</a>
        </div>
        <div class="clear clearfix ">&nbsp;</div>


        <div class="tm_5_em">
          <?php
              for ($i=0; $i < 2; $i++) {
                $recursion = rand(1,8);
                echo $this->element('stubs/forum_listing_stub', array('recursion'=>$recursion,'badges'=>$badges));
              }
           ?>
        </div>

</div>
