<div class="forum_listing">
  <?php
      $badges = array(
          "badge_small_general.png",
          "badge_small_grad.png",
          "badge_small_greek.png",
          "badge_small_hipster.png",
          "badge_small_judge.png",
          "badge_small_nerd.png",
          "badge_small_sherlock.png",
          "badge_small_twain.png",
          "badge_small_world.png",
          "badge_small_general.png",
          "badge_small_grad.png",
          "badge_small_greek.png",
          "badge_small_hipster.png",
          "badge_small_judge.png",
          "badge_small_nerd.png",
          "badge_small_sherlock.png",
          "badge_small_twain.png",
          "badge_small_world.png",          
          "badge_small_zombie.png"
      );
      if(!isset($limit)){
        $limit = 10;
      }
      for ($i=0; $i < $limit; $i++) {
        echo $this->element('stubs/forum_title_row', array('recursion'=>1,'badges'=>$badges));

      }
   ?>
</div>
