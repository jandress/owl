
<table class="table table-striped table-bordered table-hover tm_2_em">
	<tbody>
		<tr>
			<th><a href="/my/quizzes/sort:id/direction:asc">Class</a></th>
			<th><a href="/my/quizzes/sort:name/direction:asc">Lesson Name</a></th>
			<th><a href="/my/quizzes/sort:Description/direction:asc">Description</a></th>
	    	<th><a href="/my/quizzes/sort:Description/direction:asc">Coverage Period</a></th>
		</tr>





		<?php

			$a = array(
				"class"=>"Chords",
				"name"=>"Lesson Name",
				"description"=>"class",
				"time"=>"time required"
			);
			$b = array(
				"class"=>"Scales",
				"name"=>"Lesson Name",
				"description"=>"class",
				"time"=>"time required"
			);
			$c = array(
				"class"=>"Intervals",
				"name"=>"Perfect Consonant Intervals",
				"description"=>"class",
				"time"=>"time required"
			);
			$d = array(
				"class"=>"Rhythm / Picking",
				"name"=>"Make it talk",
				"description"=>"class",
				"time"=>"time required"
			);
			$e = array(
				"class"=>"Vocabulary",
				"name"=>"BB King licks",
				"description"=>"class",
				"time"=>"time required"
			);

		
			$ar = array(
				$a,$b,$c,$d,$e
			);
			foreach ($ar as $key => $value){
				echo $this->element('stubs/current_lesson_row', array("tst"=>$value) ); 
			}
		
		
		?>
		
	</tbody>
</table>

