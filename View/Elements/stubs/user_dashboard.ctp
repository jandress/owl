<div class = "container-fluid">

    <div id = "" class = " col-md-2">
        <?php echo $this->element("stubs/users/user_icon"); ?>
        <?php echo $this->element("stubs/users/user_dashboard_nav"); ?>
    </div>    

    <div class = "col-md-8">
        <div  class = "">
            <h3>Activity</h3>
            <div class = "clear clearfix tm_1_em">&nbsp;</div>
            <div class = "tm_1_em">
            <?php echo $this->element("stubs/notification_rows");?>
            </div>
        </div>     
    </div>
    <div class = "col-md-2">
        <?php  echo $this->element("stubs/users/file_pie"); ?>
    </div>
</div>
