<div class="messages_blurb">
  <h2>Messages</h2>
  <?php if (count($messages)): ?>
    <ul>
      <?php foreach ($messages as $key => $value): ?>
        <li>
          <a href="/my/messages/read/<?php echo $value['Message']['id'] ?>">
            <div class="well" style="padding:2px; padding-left:20px;">
              <h4><?php echo $value['Message']['subject'] ?></h4>
              <p><?php echo $this->Text->truncate($value['Message']['message'],100,array("ellipsis")); ?></p>
            </div>
          </a>
        </li>
      <?php endforeach; ?>
    </ul>
  <?php endif; ?>
</div>
