<?php
$nl = $value["Navlink"];
$nid = $nl["id"];
$k = "row_".$key%2;
//$level = $level+1;
//$row_id = $value["Navlink"]["id"].'_'.$level;
$new_address = $address.'_'.$key;
$id = $new_address.'n'.$nid;
$level = count(explode('_',$address));

?>

<tr id = "SM<?php echo $id; ?>" class = "parent<?php echo $address; ?> level_<?php echo $level ?> sitemap_row">
<td>
    <div class = "pt_6_px">
    <?php if (count($value["ChildNavlink"])): ?>
    <a style = "float:left" href = "<?php echo $id ?>" class = "btn expand_button nav_toggle_buttons btn-xs btn-default" id = "expand_<?php echo $id ?>" > <i class="glyphicon glyphicon-plus"></i></a>
    <a style = "float:left; display:none;" href = "<?php echo $id ?>" class = "collapse_btn btn nav_toggle_buttons btn-xs btn-default"  id = "collapse_<?php echo $id ?>" >
      <i class="glyphicon glyphicon-minus"></i>
    </a>

    <?php endif ?>


    <h5 style = "float:left; margin-left:1em; margin-top:3px;">
      <?php if (strtolower($nl["class"])=="url"): ?>
              <a  href = "<?php echo $nl["foreign_id"] ?>" target = "_blank"><?php echo $value["Navlink"]["name"] ?></a>
      <?php else: ?>
        <?php if (!empty($nl["class"])): ?>
          <a href = "/<?php echo strtolower(Inflector::pluralize($nl["class"])) ?>/view/<?php echo $nl["foreign_id"] ?>/" target = "_blank"><?php echo $value["Navlink"]["name"] ?></a>
        <?php else: ?>
          <span class = "disabled"><?php echo $value["Navlink"]["name"] ?></span>
        <?php endif ?>
      <?php endif; ?>
    </h5>



  </div>

  <?php
  //     dashboards/navlinks/recursive_sitemap_navlink

  // isAdmin is set by the isAdmin() function in AppController
   if (!empty($editable)): ?>
    <?php echo $this->element('dashboards/navlinks/navlink_edit_ui',array('nl'=>$nl,'value'=>$value)); ?>
    <?php else: ?>
  <?php endif ?>
  </div>
</td>
</tr>
