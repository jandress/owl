
<div class="container-fluid">




		<div class="admin_padding">


			<?php echo $this->BootstrapForm->create('Navlink', array('class' => 'form-horizontal'));?>

			<?php



				echo $this->BootstrapForm->hidden('parent_id', array(
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);

				echo $this->BootstrapForm->input('name', array(
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);
			?>


				<table>
					<tr>
						<th></th>
						<th></th>
						<th></th>
					</tr>
					<tr>
						<td>
							<?php echo $this->BootstrapForm->input('in_next', array()); ?>
						</td>
						<td>
							<?php
									echo $this->BootstrapForm->input('in_nav', array());
							 ?>
						</td>
						<td>

							<div class="control-group">
								<label class="control-label" for="ownership">Link Target</label>
								<div class="controls">
									<?php
									$options=array(
										'_blank'=>'New Window'
										,'_self'=>'This window'
									);
									echo $this->BootstrapForm->select('window', $options);
									?>
								</div>
							</div>


						</td>
					</tr>
				</table>


			<div class="tm_3_em bm_5_em">
				<?php echo $this->BootstrapForm->input('avatar',$fileOptions); ?>
			</div>



			<?php
				$nl = $this->data['Navlink'];
				echo $this->BootstrapForm->input('class',$classOptions);
			?>

			<div id="classAdmin">
        <?php
        	if(!empty($foreign_id_options)){
        		echo $this->BootstrapForm->input('Navlink.foreign_id',array('label'=>'Content','options'=>$foreign_id_options  ));
        	}else{
        		if (empty($label)) {
        			$label = 'Id';
        		}
        		echo $this->BootstrapForm->input('Navlink.foreign_id',array('type'=>'textArea','label'=>$label,'class'=>'col-md-10'));
        	}
        ?>
			</div>

			<div class="clear"></div>

			<?php echo $this->BootstrapForm->hidden('id'); ?>

			<div class="pull_right tm_1_em">
				<?php echo $this->BootstrapForm->submit(__('Submit'),array('class'=>array('btn pull-right')));?>
			</div>

			<?php echo $this->BootstrapForm->end();?>

		</div>

		<div class="clear clearfix">
			&nbsp;
		</div>

</div>





<script type="text/javascript" >
		function addModuleToPage(val){
			var targetDiv = "#classAdmin";
			$(targetDiv).html('<div class="icon-refresh offset3">&nbsp;</div>');
			$.ajax({
				url: '/navlinks/getfid/'+val,
				context: $(targetDiv),
				success: function(data){
					$(this).html(data);
					//$(data).appendTo(targetDiv);
				}
			});
		}
		function updateAvatar(val){
			// $(targetDiv).html('<div class="icon-refresh offset3">&nbsp;</div>');
			$.ajax({
				url: '/navlinks/getavatar/'+val,
				success: function(data){
					$("#NavlinkAvatar").val(data);
					//$(data).appendTo(targetDiv);
				}
			});
		}

		function deleteModule(div_id,thingie_id){}


		$("#NavlinkClass").change(function(e){
			addModuleToPage(e.target.value);
		});
		$("#NavlinkParentId").change(function(e){
			updateAvatar(e.target.value);
		});
</script>
