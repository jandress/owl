<div class="container-fluid tm_1_em">
  <div class="row">
    <div class="col-sm-9">
      <h1>Teacher Dashboard</h1>
        <div class="container-fluid" style="padding:2px; padding-left:10px;">
            <a class = "btn btn-default pull-right" href="/my/courses/add/">New Course</a>
            <?php echo $this->element("dashboards/classes_listing",array("current_user"=>$current_user)); ?>
        </div>
    </div>
    <div class="col-sm-3">
      <?php echo $this->element("dashboards/assignments_blurb",array("current_user"=>$current_user)); ?>
      <?php echo $this->element('dashboards/messages_blurbs'); ?>
    </div>
  </div>
  <div class="clear clearfix">&nbsp;</div>
  <div class="row">
    <div class="col-sm-9">
      <div class="container-fluid">
        <div class="well">
        </div>
        <?php echo $this->element('dashboards/notifications'); ?>
      </div>
    </div>
  </div>
</div>
