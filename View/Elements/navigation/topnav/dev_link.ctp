<li id="admin_link" class="dropdown " >
	<a href="#" class="dropdown-toggle"	 data-toggle="dropdown">&nbsp;Developer</a>
	<div class="dropdown-menu" style="min-width:280px;" role="menu">
			<ul class="nav nav-list  dashboardSideNavL">
				<li class=""><a class="admin_ajax_link ajax_link" href="/my/userassets/">File Manager</a></li>
				<li role="presentation" class="divider"></li>
				<li class=""><a class="admin_ajax_link ajax_link" href="/my/practicesessions/">Practice Sesssion</a></li>
				<li class=""><a class="admin_ajax_link ajax_link" href="/my/practiceplans/">Practice Plans</a></li>


				<li role="presentation" class="divider"></li>
				<li class=""><a class="admin_ajax_link ajax_link" href="/my/activities/">Activities</a></li>				

				<li class=""><a class="admin_ajax_link ajax_link" href="/my/vocab/">Vocabulary</a></li>

				<li class=""><a class="admin_ajax_link ajax_link" href="/my/intervals/">Intervals</a></li>
				<li role="presentation" class="divider"></li>
				<li class=""><a class="admin_ajax_link ajax_link" href="/my/settings/">Settings</a></li>
				<li class=""><a class="admin_ajax_link ajax_link" href="/my/dynamicroutes/">Routes</a></li>
				

				<li role="presentation" class="divider"></li>
				<li>The Trash</li>
				<li role="presentation" class="divider"></li>
					<li class=""><a class="admin_ajax_link ajax_link" href="/my/posts/">Posts</a></li>
					<li class=""><a class="admin_ajax_link ajax_link" href="/my/users/">Users</a></li>
					<li class=""><a class="admin_ajax_link ajax_link" href="/my/u/">User Page</a></li>
					<li class=""><a class="admin_ajax_link ajax_link" href="/my/stub/">Stubs</a></li>
					<li class=""><a class="admin_ajax_link ajax_link" href="/my/courses/">Courses</a></li>

					
					
				<li role="presentation" class="divider"></li>
					<?php if (!empty($current_user)):  ?>
    	          <li><a class=" " href="/logout/">Logout</a></li>  
        	    <?php else: ?>
            	  <li><a class=" " href="/my/dashboard/">Login</a></li>   
            	<?php endif ?>



			</ul>        				
	</div>
</li>