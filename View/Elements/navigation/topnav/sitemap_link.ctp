<?php
  $var = array(
    array(
      'name'=>'Practice',
      'link'=>'/my/practicesessions/'
    ),
  );
?>
<li class="dropdown" >
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">&nbsp; Improve</a>
    <div class="dropdown-menu dropdown-menu-right" style = "min-width:280px;" role="menu">
      <div style = "max-height:800px; overflow-y:auto;">
            <ul class = "nav" >            
              <?php foreach ($var as $key => $value): ?>
                  <li><a class=" " href="<?php echo $value['link'] ?>"><?php echo $value['name'] ?></a></li>
              <?php endforeach; ?>
              <li>
                <?php
                $nData = $this->TreeNav->children($leftNavlinks, !empty($is_manager),$selected_nav_parents); ?>
                <?php echo $nData['nav'];  ?>
              </li>
            </ul> 
      </div>
    </div>
</li>