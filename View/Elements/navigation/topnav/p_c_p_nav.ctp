<li class="dropdown" >
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">&nbsp; Collaborate</a>
    <div class="dropdown-menu dropdown-menu-right" style = "min-width:280px;" role="menu">
        <div style = "max-height:800px; overflow-y:auto;">
            <ul class = "nav">
                <?php
                
                
                foreach ($current_user['Collaboration'] as $key => $value): ?>
                    <li class="treenav_link level_2 noliststyle">
                        <a  href="/my/collaborations/view/<?php echo $value['id'] ?>" target="_blank">
                            <?php echo $value['name'] ?>
                        </a>
                    </li>
                <?php endforeach; ?>
				<li role="presentation" class="divider bm_0_px"></li>

                <li class="treenav_link level_2 noliststyle">
                        <a href="/my/collaborations/" target="_blank">
                            Administer Your Collaborations
                        </a>
                    </li>
                
        </div>
    </div>
</li>


