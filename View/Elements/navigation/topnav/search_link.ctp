<li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-search"></i>&nbsp;Search</a>
        <ul class="dropdown-menu" role="menu">
      <li>
          <div class="tm_1_em pl_1_em pr_1_em col-md-12 bm_1_em" >
            <?php
              echo $this->Form->create('Post', array('url' => '/posts/search/'));
              echo $this->Form->input('q', array('label'=>'' ,'style'=>'width:100%'));
              echo '<div class = "clear clearfix tm_10_px">&nbsp;</div>';
              echo $this->Form->submit(__('Search'), array('div' => false, 'class' => 'btn btn-block btn-sm'));
              echo $this->Form->end();
            ?>
          </div>
      </li>
        </ul>
    </li>
