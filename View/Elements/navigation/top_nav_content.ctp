


<div class="navbar-header">

  <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>

  <a class="tm_5_px bm_0px pl_30_px navbar-brand" href="/">
    Sonosphere
  </a>
</div>


<?php

  // $url = '/';
  // if(
  //   strstr($_SERVER['HTTP_HOST'],'eslwowstg.excelsior.edu') ||
  //   strstr($_SERVER['HTTP_HOST'],'owlstg.excelsior.edu')
  // ){
  //     $url = '/';
  // }
$url = '/';

if(!empty($current_user)): ?>
<div id="navbar" class="navbar-collapse collapse ">
  <ul class="nav navbar-nav">
    <?php echo $this->element('navigation/topnav/p_c_p_nav') ?>
    <?php echo $this->element('navigation/topnav/sitemap_link') ?>
    <?php echo $this->element('navigation/topnav/admin_link') ?>
    <?php echo $this->element('navigation/topnav/dev_link') ?>

    <li>
      <div class = "pl_30_px">&nbsp;</div>
    </li>
  </ul>
</div>

<?php else: ?>
  <div id="navbar" class="navbar-collapse collapse ">
  <ul class="nav navbar-nav">
    <li><a href="/login/">Login</a></li>
  </ul>
</div>
<?php endif; ?>


