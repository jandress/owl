<?php
  // debug(strstr($_SERVER['HTTP_HOST'],'dev'));
	$t = explode ('.',$_SERVER['HTTP_HOST']);
	$y ='';
	foreach ($t as $key => $value) {
	  # code...
		$g = $value == 'eslwow' ? 'owl' : $value;
	  $y .= $g;
		if($key < count($t)-1){
	  		$y .= '.';			
		}
	}
?>

<li>	
	<a class="pull-left" href="/">
		<i class="icon-white icon-home"></i>&nbsp;ESL-WOW Home
	</a>	
	
</li>
<li>	
	<a class="pull-left" href="http://<?php echo $y; ?>">
		<i class="icon-white icon-home"></i>&nbsp; Home
	</a>	
</li>
<li>
	<a class="pull-left" href="/sitemap/">
		<i class="icon-white icon-list"></i>&nbsp;Sitemap
	</a>
</li>
<?php
/*
<li>
	<a class="pull-left" href="http://<?php echo str_replace('eslwow','owl',$_SERVER['HTTP_HOST']); ?>/additional_resources/">
		<i class="icon-white icon-plus-sign"></i>&nbsp;Additional Resources
	</a>
</li>
*/
?>
<li class="dropdown">
	<a data-toggle="dropdown" class="dropdown-toggle" role="button" id="drop2" href="#">
		<i class="icon-white icon-plus-sign"></i>&nbsp;Additional Resources
	</a>
	<ul aria-labelledby="drop2" role="menu" class="dropdown-menu">
		<?php foreach ($additional_elements as $key => $value): ?>
			<li role="presentation">
				<?php if ($value['Navlink']['class']=='url'): ?>
						<a href="<?php echo $value['Navlink']['class'] ?>" tabindex="-1" role="menuitem"><?php echo($value['Navlink']['name']); ?></a>
				<?php else: ?>
						<a href="/<?php echo strtolower(Inflector::pluralize($value['Navlink']['class'])) ?>/view/<?php echo $value['Navlink']['foreign_id'] ?>" tabindex="-1" role="menuitem"><?php echo($value['Navlink']['name']); ?></a>					
				<?php endif ?>
			</li>			
		<?php endforeach ?>		
	</ul>
</li>

<li class="dropdown ">
	<a data-toggle="dropdown" class="dropdown-toggle" role="button" id="drop2" href="#">
		<i class="icon-white icon-search"></i>&nbsp;Search
	</a>
	<ul aria-labelledby="drop2" role="menu" class="dropdown-menu">
			<li role="presentation" class = "span3">
				<div class="pr_2_em tm_1_em" >
					<?php
						echo $this->Form->create('Post', array('url' => '/search/'));
						echo $this->Form->input('q', array('label'=>'','style'=>'width:95%'));
						echo $this->Form->submit(__('Search'), array('div' => false, 'class' => 'btn btn-block'));
						echo $this->Form->end();
					?>
				</div>
			</li>			
	</ul>
</li>

<?php if (empty($current_user)): 
	echo '';
	?>
<?php else: ?>
	
	<li>
		<a class="pull-left" href="/my/dashboard/">
			<i class="icon-white icon-wrench"></i>&nbsp;Dashboard
		</a>		
	</li>	
	<li>		
		<a class="pull-left" href="/logout/">
			<i class="icon-white  icon-ban-circle"></i>&nbsp;Logout
		</a>						
	</li>	
<?php endif ?>