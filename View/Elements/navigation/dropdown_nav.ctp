<ul class="dropdown-menu dropdown-menu-right" role="menu">
  <?php

  if(!empty($dropdown_data)):
   foreach ($dropdown_data as $key => $value): ?>
        <li role="presentation">
            <?php if (strtolower($value['Navlink']['class'])=='url'): ?>
            <a href="<?php echo $value['Navlink']['foreign_id'] ?>" tabindex="-1" role="menuitem"><?php echo($value['Navlink']['name']); ?></a>
            <?php else: ?>
            <a href="/<?php echo strtolower(Inflector::pluralize($value['Navlink']['class'])) ?>/view/<?php echo $value['Navlink']['foreign_id'] ?>" tabindex="-1" role="menuitem"><?php echo($value['Navlink']['name']); ?></a>
            <?php endif ?>
      </li>
  <?php endforeach;
  		endif;
   ?>
</ul>
