<div class= "col-sm-3 col-md-2" style = "position:fixed; top:39px; z-index:111; left:-5px; padding-top:1px; padding-bottom:1px !important; padding:0px !important">
	<a id = "format_toggle" class = 'btn-block btn tm_10' href="" style = 'padding-bottom:1px; padding-top:8px'>
		<span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
	</a>
</div>


<script type="text/javascript">
	$("#format_toggle").mouseover(function(e){});

	function relocateNavToTop(){
		$("#sidebar").addClass('hidden');
		$("#admin_link").removeClass('hidden');
		$("#admin_link_content").html($("#sidenav_content").html());
		$("#sidenav_content").html('');
	}

	function relocateNavToSide(){
		$("#admin_link").addClass('hidden');
		$("#sidebar").removeClass('hidden');
		$("#sidenav_content").html($("#admin_link_content").html());
		$("#admin_link_content").html('');
	}

	$("#format_toggle").click(function(e){
		console.log('click')
		var a = 'col-sm-9 col-md-10 col-sm-offset-3 col-md-offset-2 layout_a';
		var b = 'col-sm-12 col-md-12 layout_b';
		var _sidebar = "#sidebar";
		var _content = "#content";
		var _position = 'b';
		//--
		if($("#content").hasClass('layout_a')){
		//	$(_sidebar).hide();
			$(_content).removeClass(a);
			$(_content).addClass(b);
			_position = 'b';
			relocateNavToTop();

		}else{
			relocateNavToSide();

			$(_sidebar).removeClass('hidden');
			$(_content).removeClass(b);
			$(_content).addClass(a);
			_position = 'a';
		}
		$.ajax({
            url:'/layout/'+_position,
          //  data:$("form").serialize(),
            success: function(response){},
            error: function(response){
            }
        });
        //console.log(_position);
		return false
	});
</script>
