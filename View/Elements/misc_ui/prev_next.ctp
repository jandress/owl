<?php
	$hidden = '';
	$style = 'btn-success';
 if ($layoutpos == "a"): 
	$hidden = 'hidden';
?>

		
<?php endif ?>

<div class = 'btn-group pull-right tm_3_em <?php echo $hidden; ?> pr_10_px' id = "prev_next" >
    <?php $nData = $this->TreeNav->children($leftNavlinks, !empty($is_manager),$selected_nav_parents); ?>
    <a href="<?php echo $nData['prev'] ?>" class = "btn btn-sm <?php echo $style; ?>">
        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
    </a>
   <a href="<?php echo $nData['next'] ?>" class = "btn btn-sm <?php echo $style; ?>">
        <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
    </a>
</div>