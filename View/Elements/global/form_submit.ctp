<?php
	$g = 'btn btn-primary pull-right';
	$class = empty($class) ? $g : $class; 
	$label = empty($label) ? 'Save changes' : $label; 
?>
<?php echo $this->Form->submit($label, array(
    'div' => false,
    'class' => $class,
)); ?>
<div class = "clearfix">&nbsp;</div>