<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>

<div class="clearfix">&nbsp;</div>

<div>
	<div class="">
		<div style="width:45%; margin:auto !important;">
			<img src = '/owls/badge_large_sherlock.png' alt = "Sherlock Owl"/>			
			<h1>Oops!!! <?php echo $name; ?></h1>
			<p class="error">
				<strong><?php echo __d('cake', 'Error'); ?>: </strong>
				<?php printf(
					__d('cake', 'The requested address %s was not found on this server.'),
					"<strong>'{$url}'</strong>"
				);
				echo $content_for_layout;
				 ?>
				
			</p>
			<?php
			if (Configure::read('debug') > 0 ):
				echo $this->element('exception_stack_trace');
			endif;
			?>
		</div>
	</div>
	<hr/>	
	<?php echo $this->element('recursive_sitemap_navlinks',array('is_manager'=>true,'navlinks'=>$navlinks)); ?>
</div>
