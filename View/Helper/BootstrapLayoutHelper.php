<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
App::uses('String', 'Utility');
class BootstrapLayoutHelper extends AppHelper {
	
	
	
	public function sidebarNav($data = null, $params = null){
		// make this into an object param model
		// pass in tout
		return $this->_View->element('bslayout/sidebar_nav');
	}
	
	public function topNav($data = null, $params = null){
		// make this into an object param model
		// pass in tout
		return $this->_View->element('bslayout/top_nav');
	}
	public function heroUnit($data = null, $params = null){
		// make this into an object param model
		// pass in tout
		return $this->_View->element('bslayout/hero_unit');
	}

	public function toutSet($model, $data=null, $maxPerRow = 3, $startingIndex = 0,$endingIndex=200){
		// make this into an object param model
		//unpack($data)
		$g = '';
		for ($i = $startingIndex; $i < count($data)-2; $i+= $maxPerRow) { 
			$g .= $this->_View->element('bslayout/fluid_tout_row',array('controller' => 'posts','tout'=>'bslayout/standard_tout','controller','model'=>$model,'data'=>$data, 'trunc'=>300, 'maxPerRow'=>$maxPerRow));
		}
		return $g;
	}
	
	
	public function navbar($content = null, $params = null){
		$s = '<div class="navbar">
				<div class="navbar-inner">
					'.$content.'
				</div>
		</div>';
		return $s;
	}

	public function iconButton($text, $icon = null, $params = null){
		// the side to put the icon on
		$side = 'left';
		$icon = empty($icon) ? 'icon-arrow-left' : $icon;
		$attributes = '';
		if(!empty($params)){
			$side = empty($params['side']) ? $icon : $params['side']; 			
			if(!empty($params['href'])){
				$attributes .= ' href = "'.$params['href'].'" ';
			}
			if(!empty($params['onclick'])){
				$attributes .= ' onclick="'.$params['onclick'].'" ';
			}			
		}
		if (strtolower($side) =='right') {
			//icon on right side
			$string = '<a '.$attributes.' class="btn btn-default btn-sm">
				'.$text.'
				&nbsp;<span class="'.$icon.'">&nbsp;</span>				
			</a>';
		}else{
			//icon on right side
			$string = '<a '.$attributes.' class="btn btn-default btn-sm ">
				<span class="'.$icon.'">&nbsp;</span>&nbsp;
				'.$text.'
			</a>';			
		}
		
		return $string;		
	}
	



	
}
/*
<div class="upload_toolbar_link btn-group bm_2_em pull-right">
			<a alt="Back" onclick="back()" class="btn btn-small">
				<span class="icon-arrow-left">&nbsp;</span>
				Back
			</a>
							<a alt="Upload file" onclick="newFile(&quot;img/&quot;)" class="btn btn-small">
					<span class="icon-upload ">&nbsp;</span>
					Upload New File.
				</a>
					</div>
					*/