<?php

class RecursiveNavHelper extends AppHelper {


	public $prefix = 'my';
	public $first = false;
	
	function __construct() {
		$this->view = ClassRegistry::getObject('view');
	}

	function drawNav($data, $pageId = "",$options=null){
		return $this->draw($data,1,$options);
	}

	function draw($links, $level=1, $options = null){
		$ulClass = empty($options['ulClass']) ? '' : $options['ulClass'];
		$str = '<ul class="nav nav-list '.$ulClass.' dashboardSideNavL'.$level.'">';
		$str .= '<li><a href="/my/stub/">Stubs</a></li>';
		
		if(isset($links)) {
		//	pr($links);
			// first level nav items
			foreach($links as $key => $item) {
				$class = '';
				//	pr($item);exit;
				$tier1on_id = '';
				$selected_first = false;
				// set url to value or null
				$param_name = (isset($this->params['url']['url'])) ?  Inflector::camelize($this->params['url']['url']) : null;
				//	pr($param_name);
				// assemble link url
				$item_name="";
				if(isset($item['url'])){
					$item_name = Inflector::camelize($item['url']);
					$link_url = '/'.$this->prefix.'/'.$item['url'].'/';
				}else{
					$class = '';
				}
				if(!empty($item['class'])){

					$class .= ' '.$item['class'].' ';
				}
				//alias name (controller ),
				$alias_name = isset($this->viewVars['controller']) ? Inflector::camelize($this->viewVars['controller']) : false;
				// is in ancestry of selction?
				if(($item_name == $param_name) || ($alias_name == $item_name)) {
					$selected_first = true;
					$tier1on_id = (($alias_name == $item_name) ? ' id="tier'.$level.'onDaddy"' : ' id="tier'.$level.'on"');
				}
				//start assembling link string
				$str .= '<li class = "'.$class.'">'."\n";
				if(isset($item['label'])) {
					$link_label = $item['label'];
				}
				if( isset($item['head'])){
					$str .= '<h4 class="light grey_text3">'.$item['head'].'</h4>';
					$this->not_first = true;
				}
				if( isset($item['disabled']) ) {
					$str .= "<span class = 'disabled' >$link_label</span>";
				}else{
					if(isset($item['url'])){
						$str .= "<a href=\"$link_url\">$link_label</a>";
					}
					if( isset($item['javascript']) ) {
						$jsc = $item['javascript'];
						$str .= '<a onclick="'.$jsc.'" >'.$link_label.'</a>';
					}
				}
				if( isset($item['child_links']) ) {
					$str .=  $this->draw($item['child_links'], $level+1);
				}
				$str .= "</li>\n";
	        }
	    }
		$str .= '</ul>';

		return $str;
 	}


	function drawNavFlat($data, $pageId = "",$options=null){
		$ulClass = empty($options['ulClass']) ? '' : $options['ulClass'];
		$str = '<ul class="nav nav-list '.$ulClass.' dashboardSideNavL">';
		$str .= $this->drawFlat($data,1,$options);
		$str .= '</ul>';
		return $str;
	}




	function drawFlat($links, $level=1, $options = null){
		$aClass = empty($options['aClass']) ? '' : $options['aClass'];
		//debug($ulClass);
		$str = '';
		if(isset($links)) {
		//	pr($links);
			// first level nav items
			// debug($links); exit;
			foreach($links as $key => $item) {
				$class = '';
				//	pr($item);exit;
				$tier1on_id = '';
				$selected_first = false;
				// set url to value or null
				$param_name = (isset($this->params['url']['url'])) ?  Inflector::camelize($this->params['url']['url']) : null;
				//	pr($param_name);
				// assemble link url
				$item_name="";
				if(isset($item['url'])){
					$item_name = Inflector::camelize($item['url']);
					if (substr ( $item['url'], 0 ,1 )=="/") {
						$link_url = $item['url'].'/';
					}else{
						$link_url = '/'.$this->prefix.'/'.$item['url'].'/';
					}

				}else{
					$class = '';
				}
				//alias name (controller ),
				$alias_name = isset($this->viewVars['controller']) ? Inflector::camelize($this->viewVars['controller']) : false;
				// is in ancestry of selction?
				if(($item_name == $param_name) || ($alias_name == $item_name)) {
					$selected_first = true;
					$tier1on_id = (($alias_name == $item_name) ? ' id="tier'.$level.'onDaddy"' : ' id="tier'.$level.'on"');
				}
				//start assembling link string
				$str .= '<li class = "'.$class.'">'."\n";
				if(isset($item['label'])) {
					$link_label = $item['label'];
				}
				if( isset($item['head'])){
					$str .= '<h4 class="light grey_text3">'.$item['head'].'</h4>';
					$this->not_first = true;
				}
				if( isset($item['disabled']) ) {
					$str .= "<span class = 'disabled' >$link_label</span>";
				}else{
					if (isset($aClass)) {
						# code...
					}
					if(isset($item['url'])){
						/// this should probably be relocated.   i just need it to work now.
						$c = '';
						if(isset($item['class'])){
							$c = ' '.$item['class'].' ';
						}

						$str .= "<a class =\"".$c.$aClass."\" href=\"$link_url\">$link_label</a>";
					}
					if( isset($item['javascript']) ) {
						$jsc = $item['javascript'];
						$str .= '<a class = "'.$aClass.'"" onclick="'.$jsc.'" >'.$link_label.'</a>';
					}
				}
				if( isset($item['child_links']) ) {
					$str .=  $this->drawFlat($item['child_links'], $level+1, $options);
				}
				$str .= "</li>\n";
	        }
	    }
		return $str;
 	}

 	private function drawLinkArray(){}
}
