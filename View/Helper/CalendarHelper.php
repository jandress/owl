<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
App::uses('String', 'Utility');

class CalendarHelper extends AppHelper {

/*
30 days has September,
April, June, and November.
When short February's done
All the rest have 31...

Thirty days hath September,
April, June, and November,
all the rest have thirty-one.
February has twenty-eight,
but leap year coming one in four
February then has one day more.


Determination of the anchor day for the century,
calculation of the anchor day for the year from the one for the century,
 selection of the closest date out of those that always fall on the doomsday,
  e.g., 4/4 and 6/6, and count of the number of days (modulo 7) between that date and the date in question to arrive at the day of the week.

*/

public $monthArr = array('January', 'February', 'March','April', 'May', 'June', 'July','August', 'September', 'October','November', 'December');
public $dayArr = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
// The Doomsday dates for each month
public $doomsdayArrayLeapYear = [4,1,7,4,2,6,4,1,5,3,7,5];
public $doomsdayArrayNotLeapYear = [3,7,7,4,2,6,4,1,5,3,7,5];
public $daysWith30Days = array(8, 3, 5, 10);

/*




determine previous month?

year = 74
divide last 2 digits of year by 12
    74/12 = 6.16
get the difference between the years last 2 digits and the previous result * 12:
    74 - (6*12)  = 2
divide previous results by 4.
  2/4 = 0   ?
add all the results plus the anchor day value
  6+2+0+3 = 11
take the modulous of 7 from the previous day to get Doomsday
  11 % 7 = 4
Is it a leap year? False
Add or subtract the previous result from the doomsday number to find this dates day. 4+(21-5) %7 = 6

$today = 10/2/20
*/



public function getNumberOfDaysInMonth($month, $year){
  if ($month==1) {
    // is feburary.
      if($this->isLeapYear($year)){
        return 29;
      } else{
        return 28;
      }
  }
  //not feb
  if (in_array( $month, $this->daysWith30Days )) {
    return 30;
  }
  return 31;
}


public function isLeapYear($year){
  $isLeapYear = false;
  if($year % 100 == 0){
    $isLeapYear = false;
    if ($year % 400 == 0) {
      $isLeapYear = true;
    }
  }else{
    if ($year % 4 == 0) {
      $isLeapYear = true;
    }
  }
  return $isLeapYear;
}





public function getDayOfWeekForDate($day, $month, $year){
    $a = strval($year);
    $b = strlen($a);
    if( $b == 4 ){
      $c = substr($a, 2,2);
    } else {
      $c = substr($a, 0,2);
    }
    $d = floor($c / 12); // 6
    $e = $c -  ( $d * 12 ); // 2  // ok these are either 12 becausea of the month oer the year.
    $f = floor($e / 4); // 0
    // 6+2+0+3 = 11
    $g = $d + $e +$f+ 2;
    $h = $g % 7 ; // doomsday.   4 $this->dow[$h]; = thursday
    // the doomsday for december is the 5th, so if you want to find out what weekday the 21s is on ou
    $doomsDayForDecember = 5;  /// where do you get the
    // Add or subtract the previous result from the doomsday number to find this dates day. 4+(21-5) % 7 = 6
    $i = $h + ( $day - 5 ) % 7; // 5 is the doomsday for December.  21 is the day we are looking for
    $doomsdayDay=0;
    if($this->isLeapYear($year)){
      $doomsdayDay = $this->doomsdayArrayLeapYear[$month];
    }else{
      $doomsdayDay = $this->doomsdayArrayNotLeapYear[$month];
    }
    if($day < $doomsdayDay){
        $dayIndex = $h  - ($doomsdayDay - $day);
    } else if($day > $doomsdayDay){
        $dayIndex = $h  + ($day - $doomsdayDay);
        if($dayIndex >= 7){
            $dayIndex = $dayIndex % 7;
        }
    } else {
        $dayIndex = $h ;
    }
    if($dayIndex < 0) $dayIndex = 7 + $dayIndex;
    // echo $this->dayArr[$dayIndex];
    return $dayIndex;
}


  private function getEventForDay($events,$day, $month, $year){
    $t = array();
    if(!count($events)){
      $events = array();
    }
    foreach ($events as $key => $value) {
      if(
        intval($value['year']) == intval($year) &&
        intval($value['month'])  == intval($month) &&
        intval($value['day']) == intval($day)
      ){
        array_push($t, $value);
      }
    }
    return $t;
  }


  public function dayNameFromDate($id){
    return $this->dayArr[$this->getDayOfWeekForDate($day, $month, $year)];
  }
  public function monthName($id){
    return $this->monthArr[$id-1];
  }

  public function serializeDays($day, $month, $year, $events){
      // echo $this->monthArr[$month];
      $prevMonth = $month == 0 ? 11 : $month - 1;
      $a = $this->getDayOfWeekForDate($day, $month, $year);
      $b =  $this ->getNumberOfDaysInMonth($prevMonth, $year);
      $c =  $this ->getNumberOfDaysInMonth($month, $year);
      $ar = array();
      $dayAr = array();
      $e = array();
      // assemble array of daya for the days on this calenday
      // cycle through previous months days....
      ///come back and fix this for previous year on Jan 1st / Dec 31st

      for ($i = ($b - $a ); $i < $b ; $i++) {
        $this->getEventForDay($events,$day, $month, $year);
        $dayAr = array(
          'month'=>$prevMonth,
          'day'=>$i+1,
          'events' => $this->getEventForDay($events, $i+1, $prevMonth, $year)
        );
        array_push($ar,$dayAr);
      }
      // cycle through current months days....
      $e = array();
      for ($i=1; $i <= $c; $i++) {
        $dayAr = array(
          'month'=>$month,
          'day'=>$i,
          'events' => $this->getEventForDay($events, $i, $month, $year)
        );
        array_push($ar,$dayAr);
      }
      $i = 0;
      $t = true;
      $e = array();
      // cycle through next months days to complete the week.
      while ($t) {
        if(( count($ar)%7 )==0){
          $t = false;
          continue;
        }
        $i++;
        $dayAr = array(
          'month'=>$prevMonth,
          'day'=>$i,
          'events' => $this->getEventForDay($events, $i, $prevMonth, $year)
        );
        array_push($ar,$dayAr);
      }
      return $ar;
  }




  public function printMonth($day, $month, $year, $events = array() ) {?>
	   <div class="cal-context">
       <div id="" class="" style="width: 100%;">
          <div class="cal-row-fluid cal-row-head">
           <div class="cal-cell1">Sunday</div>
           <div class="cal-cell1">Monday</div>
           <div class="cal-cell1">Tuesday</div>
           <div class="cal-cell1">Wednesday</div>
           <div class="cal-cell1">Thursday</div>
           <div class="cal-cell1">Friday</div>
           <div class="cal-cell1">Saturday</div>
         </div>
          <div class="cal-month-box">
          <?php
            $p = 0;
            //debug($events);

            $h = $this->serializeDays($day, $month, $year, $events);
            for ($i=0; $i < count($h) ; $i++) {
              $p++;
              $class = "";
              if ($p==1) {
                echo '<div class="cal-row-fluid cal-before-eventlist">';
                $class .'cal-day-weekend';
              }
              if ($p==1 || $p==7 ) { $class .= 'cal-day-weekend '; }
              ?>
              <div class="cal-cell1 cal-cell <?php echo $class ?> " data-cal-row="-day6">
                <div class="cal-month-day cal-day-inmonth">
                  <span class="pull-right" data-cal-date="2013-03-01" data-cal-view="day" data-toggle="tooltip" title="" data-original-title=""><?php echo $h[$i]['day'] ?></span>
                  <?php

                  if (count($h[$i]['events'])): ?>
                    <div class="events-list" data-cal-start="1363158000000" data-cal-end="1363244400000">
                      <?php foreach ($h[$i]['events'] as $key => $value): ?>
                        <a href="#" data-event-id="293" data-event-class="event-warning" class="pull-left event event-warning" data-toggle="tooltip" title="" data-original-title="This is warning class event with very long title to check how it fits to evet in day view"></a>
                      <?php endforeach; ?>
                    </div>
                  <?php endif; ?>
                </div>
              </div>
              <?php
              if ($p==7) {
                $p = 0;
                echo '</div>';
              }
            }
          ?>
          </div>
       </div>
     </div>
     <?php
  }
} ?>

<?
/*
Doomsday Table


Jan 3 or 4. 3 if not, 4 if is a ly
Feb 7 or 1
march 7
april 4
may 2
june 6
july 4
August 1
September 5
october 3
nov 7
dec 5

*/


//
// public $months = array(
//   array(
//     'name'=>"January",
//     'number_of_days'=>"31"
//   ),
//   array(
//     'name'=>"Feburary",
//     'number_of_days'=>"31"
//   ),
//   array(
//     'name'=>"March",
//     'number_of_days'=>"31"
//   ),
//   array(
//     'name'=>"April",
//     'number_of_days'=>"31"
//   ),
//   array(
//     'name'=>"May",
//     'number_of_days'=>"31"
//   ),
//   array(
//     'name'=>"June",
//     'number_of_days'=>"31"
//   ),
//   array(
//     'name'=>"July",
//     'number_of_days'=>"31"
//   ),
//   array(
//     'name'=>"August",
//     'number_of_days'=>"31"
//   ),
//   array(
//     'name'=>"September",
//     'number_of_days'=>"31"
//   ),
//   array(
//     'name'=>"October",
//     'number_of_days'=>"31"
//   ),
//   array(
//     'name'=>"November",
//     'number_of_days'=>"31"
//   ),
//   array(
//     'name'=>"December",
//     'number_of_days'=>"31"
//   )
// );

?>
