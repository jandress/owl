<?php

	App::uses('AppHelper', 'View/Helper','Helper', 'View');
	class HabtmTableHelper extends Helper {

		public function printTable($list,$habtm_ids,$controller,$name_attr,$id_base,$disabled=null, $editable = false, $repositionable = false){
			if(!empty($list)):
					$g = 0;
					?> <table><?php
					foreach($list as $k => $v):
						$g++;
						$styling = ($g %2)==1 ? "altrow" : "row";
						$match = in_array($k,$habtm_ids) ? true : false;
						$check_string = '';
						if($match === true){ 	$check_string = 'checked="checked"'; }
						if(!empty($disabled)){ 	$check_string .= ' disabled="disabled" '; } ?>
						<tr class="altrow">
							<td>
								<input type="checkbox" name="<?php echo($name_attr) ?>" value="<?php echo Sanitize::html($k); ?>" id="<?php echo($id_base.$k); ?>" <?php echo($check_string); ?> />
							</td>
							<td>
								<label for="<?php echo($id_base.$k); ?>"><?php echo Sanitize::html($v) ?></label>
							</td>
							<td>
								<?php
									if (!empty($editable)) {
										if ($editable) {
											?><a href ="<?php echo('/my/'.$controller.'/edit/'.$k.'/') ?>">Edit</a><?php
										}
									}
								?>
							</td>
							<td>
								<?php
									if (!empty($repositionable)) {
											// put repositioning arrows here...
											// echo $this->element('tables/reposition_arrows',array('reorderable'=>true));
											echo "put up and down arrows here";
									}
								?>
							</td>
						</tr>
					<?php
					endforeach;
					?>
					</table>
					<?php
			endif;
		}



			public function printClasses($list,$habtm_ids,$controller,$name_attr,$id_base,$disabled=null, $editable = false, $repositionable = false){
				foreach($list as $k => $v):
					$check_string = '';
					$match = in_array($k,$habtm_ids) ? true : false;
					if($match === true){ 	$check_string = 'checked="checked"'; }
					if(!empty($disabled)){ 	$check_string .= ' disabled="disabled" '; } ?>
				<div class="col-md-4 tm_2_em">
			      <div class="well" style="">
							<div class="row">
								<img class="col-md-3" src="/owls/badge_small_sherlock.png" alt="">
								<h4 class="display:inline; padding-top:10px;" ><?php echo Sanitize::html($v) ?></h4>
										<input type="checkbox" name="<?php echo($name_attr) ?>" value="<?php echo Sanitize::html($k); ?>" id="<?php echo($id_base.$k); ?>" <?php echo($check_string); ?> />
									</td>
									<td>
							</div>
			      </div>
			    </div>
				<?php
				endforeach;
			}
	}
?>
