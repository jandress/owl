Class field should get a list of suitable plugins from polymorhic
when it's select value changes, you should query to get a list from that plugins model.

<div class="row-fluid">
	<div class="span9">
		<?php echo $this->BootstrapForm->create('Contentmodule', array('class' => 'form-horizontal'));?>
			<fieldset>
				<legend><?php echo __('My Add %s', __('Contentmodule')); ?></legend>
				<?php
				echo $this->BootstrapForm->input('class', array(
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('foreign_id', array(
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);
				?>
				<?php echo $this->BootstrapForm->submit(__('Submit'));?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__( __('Contentmodules')), array('action' => 'index'));?></li>
		</ul>
		</div>
	</div>
</div>