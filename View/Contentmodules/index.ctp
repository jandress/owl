<h2><?php echo __( __('Contentmodules'));?></h2>
<p>
	<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
</p>

<table class="table">
	<tr>
		<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
		<th><?php echo $this->BootstrapPaginator->sort('class');?></th>
		<th><?php echo $this->BootstrapPaginator->sort('foreign_id');?></th>
		<th><?php echo $this->BootstrapPaginator->sort('created');?></th>
		<th><?php echo $this->BootstrapPaginator->sort('modified');?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
<?php foreach ($contentmodules as $contentmodule): ?>
	<tr>
		<td><?php echo h($contentmodule['Contentmodule']['id']); ?>&nbsp;</td>
		<td><?php echo h($contentmodule['Contentmodule']['class']); ?>&nbsp;</td>
		<td><?php echo h($contentmodule['Contentmodule']['foreign_id']); ?>&nbsp;</td>
		<td><?php echo h($contentmodule['Contentmodule']['created']); ?>&nbsp;</td>
		<td><?php echo h($contentmodule['Contentmodule']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $contentmodule['Contentmodule']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $contentmodule['Contentmodule']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $contentmodule['Contentmodule']['id']), null, __('Are you sure you want to delete # %s?', $contentmodule['Contentmodule']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>

<?php echo $this->BootstrapPaginator->pagination(); ?>