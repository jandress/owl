<div class="row-fluid">
	<div class="navbar-inverse">
		<h2>SITE SETTINGS</h2>
	</div>
	<div class="admin_padding">
		<?php echo $this->BootstrapForm->create('Setting', array('class' => 'form-horizontal'));?>
			<fieldset>

				
				<?php 

				//	echo $this->BootstrapForm->input('pushcode',array('label'=>"Push database content to Production?"));
 				//	echo $this->BootstrapForm->input('pushdb',array('label'=>"Push Code and Media Files to Production?"));
				 ?>

<hr/>
Please don't change anything below this line unless you know what you are doing. 
<hr/>
				<?php
				// echo $this->Form->input('google_analyics', array(
				// 	'required' => 'required',
				// 	'value'=>$this->data['Setting']['google_analyics'],
				// 	'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;'
				// 	,'class'=>'span6 bm_1_em'
				// 	)
				// );
				echo $this->BootstrapForm->input('default_layout', array('options'=>$layout_options, 'value'=>$this->data['Setting']['default_layout']));
				echo $this->BootstrapForm->input('stylesheet', array('options'=>$stylesheet_options, 'value'=>$this->data['Setting']['stylesheet']));
				?>
				<div class = "clearfix bm_1_em">&nbsp;</div>
				<a  class = "tm_4_px btn btn-info btn-sm" href="/my/theme">View Themes</a>

				<?php				
				echo '<div class = "clearfix tm_1_em">&nbsp;</div>';
				//--------------------------------------------------------------------------------
				echo $this->BootstrapForm->input('sitemap_parent_id',array(
					'label'=>'Sidenav Parent Navlink ID'
					,'options'=>$navlink_options
					,'value'=>$this->data['Setting']['sitemap_parent_id']
				));
				
				echo '<small>Warning.  Carelessly Changing this could break the Left Nav.</small><div class = "clearfix tm_1_em">&nbsp;</div>';
				echo $this->BootstrapForm->input('additional_parent_id',array(
					'label'=>'Additional Resources Menu Navlink ID'
					,'options'=>$navlink_options
					,'value'=>$this->data['Setting']['additional_parent_id']						
				));					
				echo '<small>Warning.  Carelessly Changing this could break the Top Nav.</small><div class = "clearfix tm_1_em">&nbsp;</div>';					
				?>
				<div class = "clearfix bm_2_em">&nbsp;</div>
				<div class = "clearfix ">&nbsp;</div>

				<div class = "clearfix bm_2_em">&nbsp;</div>
				<div class = "clearfix">&nbsp;</div>
				<?php echo $this->BootstrapForm->submit(__('Submit'));?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>