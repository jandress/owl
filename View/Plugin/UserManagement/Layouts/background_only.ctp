<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
    <title><?php echo $title_for_layout; ?></title>
    <!-- Bootstrap core CSS -->
    <link href="/css/stylesheet/bootstrap.min.css" rel="stylesheet">
    <?php if (file_exists("css/stylesheet/".$settings['stylesheet'])): ?>
            <link href="/css/stylesheet/<?php echo $settings['stylesheet']; ?>" rel="stylesheet">
    <?php else: ?>
    <?php endif; ?>

    <link href="/css/icons.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/layout_aids.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/js/ie-emulation-modes-warning.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Custom styles for this template -->
    <style media="screen">

        body, html, .body_background_color{
          background-color:#000000;
        }
        .logo{
          background: url("/img/layout_set_logo_purple.png") no-repeat scroll 0 0 transparent;
          width: 250px;
          height:100px;
          margin:auto;
        }
        h1{
          font-size:42px;
        }
    </style>
  </head>
  <body>

    <div class="container-fluid">
      <?php echo $content_for_layout; ?>
    </div>
    <div class="fullscreen-image">


    </div>
    <?php $this->Js->buffer("console.log($('.hasTooltip').tooltip({trigger:'hover', html:'false' }));"); ?>    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
