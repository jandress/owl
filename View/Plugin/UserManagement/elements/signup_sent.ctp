<div style="padding:200px; padding-top:20px" class="container tm_2_em">
  <div style="padding:20px" class="panel panel-default  full_width_fields">
      <div class="logo h_centered hasTooltip" data-placement="right" data-original-title="Password">&nbsp;</div>
      <div class="tm_2_em">
        <div class = "clearfix">&nbsp;</div>
        <a class="h_centered" href = "/" >
        	<div class="alert alert-success centered">
        		An email with instructions has been sent to the email address we have on file for you.
        	</div>
        </a>
        <?php
        	$a = Configure::read('debug');
        	$b = !empty($link);
        	if($a && $b){
        		echo '<strong>'.$link.'</strong>';
        	}
        ?>
        	</strong>
        <div class = "clearfix">&nbsp;</div>

      <div class="clear clearfix tm_2_em">&nbsp;</div>
      </div>
  </div>
  <div class="clear clearfix tm_2_em">&nbsp;</div>
</div>
