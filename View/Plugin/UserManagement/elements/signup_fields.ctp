<div class="container tm_2_em">
  <div style="padding:20px" class="panel panel-default  full_width_fields">
      <div class="logo h_centered hasTooltip" data-placement="right" data-original-title="Password">&nbsp;</div>
      <h4 style="text-align:center" style="margin:15px">Please sign Up</h4>
      <div style="width:75%; margin-left:12% ">
        <fieldset>
          <?php
            echo $this->UserManagementForm->createForm('User','/signup/');
            echo $this->UserManagementForm->printFields($userDatafields);
            echo $this->UserManagementForm->printFields($passwordfields);
            $ar = array(
              'options'=>array(
                '1'=>'Student',
                '2'=>'Teacher',
                '3'=>'Administrator',
                '4'=>'Developer'
              )
            );
            echo $this->BootstrapForm->input('Group', $ar);
          ?>
        </fieldset>
        <div class="tm_2_em">&nbsp;</div>
        <button type="submit" class="btn btn-primary btn-large pull-right offset2">Submit</button>
        <div class="tm_2_em">&nbsp;</div>
        <?php
          echo $this->Form->end();
          echo $this->Session->flash();
        ?>
        <div class="clear clearfix tm_2_em">&nbsp;</div>
        </div>
        <div class="clear clearfix">&nbsp;</div>
    </div>
</div>
