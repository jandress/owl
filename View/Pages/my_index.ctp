<?php echo $this->BootstrapForm->create('Landingpage', array('class' => ''));?>
<div class="row-fluid">
		<h2><?php echo __( __('Pages'));?></h2>
		<div class="navbar">
			<div class="navbar-inner">
				<div class='span9' style = 'padding-top:8px'><p><?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></p></div>
				<div class="nav span3 pull-right">
					<a href = '/my/pages/add/' class = 'btn pull-right'><div class="icon-plus">&nbsp;</div>&nbsp;New Page</a>
				</div>				
			</div>
		</div>
		<table class="table table-striped table-bordered table-hover">
			<tr>				
				<th class="span1" style = 'text-align:center;' ><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th class="span5"><?php echo $this->BootstrapPaginator->sort('name');?></th>
				<th class="actions span4" style = 'text-align:center;'>Actions</th>								
			</tr>
		<?php 
		echo $this->BootstrapForm->hidden('Landingpage.name');
		echo $this->BootstrapForm->hidden('Landingpage.id');		
		foreach ($pages as $page):
			$pid = $page['Page']['id'] ;
			 ?>
			<tr>
				<td style = 'text-align:center;'><?php echo h($page['Page']['id']); ?>&nbsp;</td>		
				<td><?php echo h($page['Page']['name']); ?>&nbsp;</td>
				<td class="actions" style = 'text-align:center;'>
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $page['Page']['id'])); ?><span> | </span>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $page['Page']['id'])); ?><span> | </span>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $page['Page']['id']), null, __('Are you sure you want to delete # %s?', $page['Page']['id'])); ?>
				</td>	
			</tr>
		<?php endforeach; ?>
		</table>
		<?php echo $this->BootstrapPaginator->pagination(); ?>
</div>
<?php echo $this->BootstrapForm->end();?>