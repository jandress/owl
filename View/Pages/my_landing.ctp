<?php echo $this->BootstrapForm->create('Landingpage', array('class' => ''));?>
<div class="row-fluid">
	<h2>Choose a page for the home page</h2>
	<div class="navbar">
		<div class="navbar-inner">
			<div class='span9' style = 'padding-top:8px'><p><?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></p></div>
			<div class="nav span2 pull-right">

			</div>
		</div>
	</div>
	<table class="table table-striped table-bordered table-hover">
		<tr>
			<th class="span2" style = 'text-align:center;'>Home Page</th>				
			<th class=""><?php echo $this->BootstrapPaginator->sort('name');?></th>
			<th class="span1" style = 'text-align:center;'><?php echo $this->BootstrapPaginator->sort('id');?></th>				
		</tr>
		<?php 
		echo $this->BootstrapForm->hidden('Landingpage.name');
		echo $this->BootstrapForm->hidden('Landingpage.id');		
		foreach ($pages as $page):
			$pid = $page['Page']['id'] ;
			 ?>
			<tr>
				<th style = 'text-align:center;'>
					<input name="data[Landingpage][page_id]" type="radio" value="<?php echo $pid; ?>" <?php if($pid == $this->data['Landingpage']['page_id']){echo 'checked = "true"';} ?> id="PageId <?php echo $pid; ?>" />				
				</th>				
				<td><?php echo h($page['Page']['name']); ?>&nbsp;</td>
				<td style = 'text-align:center;'><?php echo h($page['Page']['id']); ?>&nbsp;</td>				
			</tr>
		<?php endforeach; ?>
	</table>
	<?php echo $this->BootstrapForm->submit(__('Submit'),array('div'=>'pull-right'));?>		
	<?php echo $this->BootstrapPaginator->pagination(); ?>
</div>
<?php echo $this->BootstrapForm->end();?>