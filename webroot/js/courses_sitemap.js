function closeChildNavLinks(d) {
	var b = d.split("/");
	var c = b[b.length-1];
	var address = c.split("n")[0];
	var g = $(".sitemap_row");
	for(var i = 0; i < g.length; i++){
		var row_id = $(g[i]).attr('id');
		var t = row_id.split("n")[0];
		var b = "SM"+address
		if(t!=b){
			if(t.substring(0,b.length)==b){
				$("#"+row_id).remove();
			}
		}
		var x;
	}
	$("#expand_"+c).css('display','block');	
	$("#collapse_"+c).css('display','none');
	return false;
};



function getChildNavLinks( d ) {
	
	var b = d.split("/")
	console.log("b", b[b.length-1]);
	var k = b[b.length-1];
	b.pop();
	var j = b.join("/");


	// child_id should be the navlink id.   change this to reflect.
	var child_id = k.split("n")[1]
	var address = k.split("n")[0]
	console.log('j:',j, ',k:',k, ',b[b.length-1]:', b[b.length-1])
	console.log("child_id ",child_id, "address ", address);
	var url = j+"/"+"/"+child_id+"/"+address;
	var div = $("#SM"+k); // #SM0_0n2
	$('<tr id= "sitemapchildren_'+address+'" ><td id = "sitemap_loader" class="centered"><div class = "bm_2_em"><em><h4>Loading...</h4></em><img src="/img/loader.gif" alt = ""/></div></td></tr>').insertAfter(div);
	$.ajax({
		url: url,
		success: function(data){
			// insert after div? where is this getting inserted?
			//console.log(data);
			$("<tr> "+data+" </tr>").insertAfter(div);
			$("#sitemap_loader").remove();
			var b = d.split("/");
			var c = b[b.length-1];
			$("#expand_"+c).css('display','none');
			$("#collapse_"+c).css('display','block');
			initSitemap()
		}
	});
};

function initSitemap(){
	$(".expand_button").unbind('click');
	$(".collapse_btn").unbind('click');
	$(".expand_button").click(function(e){
		var g = $(e.currentTarget).attr('href');
		getChildNavLinks(g);
		return false;
	});
	$(".collapse_btn").click(function(e){
		var g = $(e.currentTarget).attr('href');
		closeChildNavLinks(g);
		return false;
	});
}


initSitemap()