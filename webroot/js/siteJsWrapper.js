

 var inlinePlayer = null;
 function initSound(){
 	soundManager.setup({
      debugMode: false,// disable or enable debug output
      preferFlash: false,// use HTML5 audio for MP3/MP4, if available
      useFlashBlock: true,
      url: '/soundmanager/',// path to directory containing SM2 SWF
      flashVersion: 9// optional: enable MPEG-4/AAC support (requires flash 9)
    });
    soundManager.onready(function() {
      inlinePlayer = new InlinePlayer();
    });
 }



 function onPageLoad(){
	try {
		CKEDITOR.replaceAll( 'ckeditor' );
	}catch (e) {}
	try {
    	$('.wyswyg').wysihtml5(
                {
                "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                  "emphasis": true, //Italics, bold, etc. Default true
                  "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                  "html": true, //Button which allows you to edit the generated HTML. Default false
                  "link": true, //Button to insert a link. Default true
                  "image": true, //Button to insert an image. Default true,
                  "color": true //Button to change color of font
                }
        );
	}catch (e) {}


	$('body').on('hidden.bs.modal', '.modal', function () {
	  $(this).removeData('bs.modal');
	  //$("#modal-content").remove();
	});
    $('#general_modal').modal();
    //---------------------
	try {
    	$('.fancybox').fancybox();
	}
	catch (e) {

  }

	try {
   		initSound();
	}
	catch (e) {}
	try {
   		main_content_ajax_helper.applyAjaxListenerToLinks('.ajax_link');
      main_content_ajax_helper.applyAjaxListenerToLinks('.ajax_link_2');
      //		applyAjaxListenerToLinks: function(_targetButton,_targetDiv,_ldrString){

  }
	catch (e) {}

}



// function openmodal(_href){
//   return false;
// };


// var main_content_ajax_helper = ajax_slider();
//main_content_ajax_helper.init("#content",'.ajax_link');
// main_content_ajax_helper.init("#ajax_container_2",'.ajax_link_2');

// main_content_ajax_helper.onLoaded = onPageLoad;
