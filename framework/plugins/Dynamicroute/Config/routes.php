<?php

  Router::connect('/my/dynamicroutes', array('plugin'=>'Dynamicroute','controller' => 'Dynamicroute', 'action' => 'admin_index'));  
  Router::connect('/my/dynamicroutes/edit/*', array('plugin'=>'Dynamicroute','controller' => 'Dynamicroute', 'action' => 'admin_edit'));  
  Router::connect('/my/dynamicroutes/add/*', array('plugin'=>'Dynamicroute','controller' => 'Dynamicroute', 'action' => 'admin_add'));  
  Router::connect('/my/dynamicroutes/delete/*', array('plugin'=>'Dynamicroute','controller' => 'Dynamicroute', 'action' => 'admin_delete'));  
  Router::connect('/my/dynamicroutes/tablecontent/*', array('plugin'=>'Dynamicroute','controller' => 'Dynamicroute', 'action' => 'tablecontent'));  
  Router::connect('/my/dynamicroutes/content/*', array('plugin'=>'Dynamicroute','controller' => 'Dynamicroute', 'action' => 'content'));  

  App::uses('ClassRegistry', 'Utility');
  /**
   * Initialize model and perform find
   */
  $dr = ClassRegistry::init('Dynamicroute'); 
  $cmsdata = $dr->find('all'); 
  /**
   * Iterate over results and define routes
   */
  foreach ($cmsdata as $cmsrow) {
 // 	debug($cmsrow);
    $fid = $cmsrow['Dynamicroute']['foreignid'];
    switch($cmsrow['Dynamicroute']['class']){
      case("Quiz"):
        $controller = 'Quizzes';
        $plugin = 'Quizmodules';
      break;
      case("Landingpage"):
        $controller = 'Landingpages';
        $plugin = 'Landingpages';
      break;
      case("Post"):
        $controller = 'Posts';
        $plugin = 'Post';
      break;            
    }

    $fid = $cmsrow['Dynamicroute']['foreignid'];
		Router::connect('/'.$cmsrow['Dynamicroute']['slug'], array('plugin'=>$plugin, 'controller' => $controller, 'action' => 'view',$fid));	
    //Router::connect('/', array('plugin'=>'Post','controller' => $cmsrow['Dynamicroute']['class'], 'action' => 'view',$cmsrow['Dynamicroute']['foreignid']));
  }
?>