<?php

	App::uses('AppController', 'Controller');

	class DynamicrouteController extends DynamicrouteAppController {

		var $uses = array('Dynamicroute.Dynamicroute','Post.Post', 'Landingpages.Landingpage','Quizmodules.Quiz');
		public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator','Quizmodules.Breadcrumbs');



		var $classoptions = array(
			'options'=>array(
				'Post'=>'Posts',
				'Landingpage'=>'Landing Page',
				'Quiz'=>'Quiz'
			)
		);

		//beadcrumbs
		var $aebc = array(
				array(
				'label'=>'Route  Index',
				'attributes'=>array(
					'href'=>'/my/dynamicroutes/',
					'class'=>"ajax_link",
				),
			),
			array(
				'label'=>'Editing Route',
				'attributes'=>array(
					'class'=>"ajax_link",
				),
			)
		);


		//beadcrumbs
		var $aabc = array(
				array(
				'label'=>'Route Index',
				'attributes'=>array(
					'href'=>'/my/dynamicroutes/',
					'class'=>"ajax_link",
				),
			),
			array(
				'label'=>'New Route',
				'attributes'=>array(
					'href'=>'/my/dynamicroutes/add',
					'class'=>"ajax_link",
				),
			)
		);



		public function admin_index(){
			if ($this->request->is('post') || $this->request->is('put')) {
					$this->Dynamicroute->updateAll(array('home'=>0));
					$this->Dynamicroute->updateAll(array('home'=>1),array('id'=>$this->request->data['Dynamicroute']['home']));
			}
			$this->Dynamicroute->recursive = 0;
			$this->set('dynamicroutes', $this->paginate());
		}


		public function admin_edit($id){
			$this->set('breadcrumbs' , $this->aebc );
			$this->set('classoptions',$this->classoptions);
			$this->aebc[1]['attributes']['href']='/my/dynamicroutes/edit/'.$id;

			$this->Dynamicroute->id = $id;
			if (!$this->Dynamicroute->exists()) {
				throw new NotFoundException(__('Invalid %s', __('post')));
			}
			if ($this->request->is('post') || $this->request->is('put')) {
				if ($this->Dynamicroute->save($this->request->data)) {
					$this->Session->setFlash(
						__('The %s has been saved', __('post')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);

					$t = '/my/dynamicroutes/content/'.$id;
					if($this->checkAjax()){
						echo '{"href":"'.$t.'"}';
						exit;
					}else{
						$this->redirect($t);
					}
				} else {
					$this->Session->setFlash(
						__('The %s could not be saved. Please, try again.', __('post')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-error'
						)
					);
				}
			} else {
				$this->request->data = $this->Dynamicroute->read(null, $id);
				$this->tablecontent($this->request->data['Dynamicroute']['class']);
				$this->title("Editing Dynamicroute: ".$this->request->data['Dynamicroute']['slug']	);
			}
		}// end edit


		public function admin_add(){
			$this->set('classoptions',$this->classoptions);
			$this->set('breadcrumbs' , $this->aabc );
			if ($this->request->is('post') || $this->request->is('put')) {
				$b = $this->Dynamicroute->create();
				$t = $this->Dynamicroute->save($this->request->data);
				if($t) {
					$this->Session->setFlash(
						__('The %s has been saved', __('post')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);
					$t = '/my/dynamicroutes/content/'.$this->Dynamicroute->getLastInsertId();
					if($this->checkAjax()){
						echo '{"href":"'.$t.'"}';
						exit;
					}else{
						$this->redirect($t);
					}
				} else {
					$this->Session->setFlash(
						__('The %s could not be saved. Please, try again.', __('post')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-error'
						)
					);
				}
			} else {
				$this->request->data = $this->Dynamicroute->create();
				$this->title("New Dynamic Route");
			}
		}  // end edit


		public function admin_delete($id){
			$this->set('breadcrumbs' , $this->aebc );
			$this->set('classoptions',$this->classoptions);
			$this->Dynamicroute->id = $id;
			$t = '/my/dynamicroutes/';

			if (!$this->Dynamicroute->exists()) {
				throw new NotFoundException(__('Invalid %s', __('route')));
			}



			if ($this->request->is('post') || $this->request->is('put')) {
				if ($this->Dynamicroute->delete($this->request->data)) {
					$this->Session->setFlash(
						__('The %s has been deleted', __('post')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);

					if($this->checkAjax()){
						echo '{"href":"'.$t.'"}';
						exit;
					}else{
						$this->redirect($t);
					}
				} else {
					$this->Session->setFlash(
						__('The %s could not be deleted. Please, try again.', __('post')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-error'
						)
					);
				}
			}
			$this->redirect($t);
		}// end edit












		public function content($id){
			$this->set('classoptions',$this->classoptions);
			$this->Dynamicroute->id = $id;
			if (!$this->Dynamicroute->exists()) {
				throw new NotFoundException(__('Invalid %s', __('post')));
			}
			if ($this->request->is('post') || $this->request->is('put')) {
				if ($this->Dynamicroute->save($this->request->data)) {
					$this->Session->setFlash(
						__('The %s has been saved', __('post')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);

					$t = '/my/dynamicroutes/';
					if($this->checkAjax()){
						echo '{"href":"'.$t.'"}';
						exit;

					}else{
						$this->redirect($t);
					}
				} else {
					$this->Session->setFlash(
						__('The %s could not be saved. Please, try again.', __('post')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-error'
						)
					);
				}
			} else {
				$this->request->data = $this->Dynamicroute->read(null, $id);
				$this->tablecontent($this->request->data['Dynamicroute']['class']);
				$this->title("Editing Dynamicroute: ".$this->request->data['Dynamicroute']['slug']	);
			}
		}// end edit
















		function tablecontent($string='Post'){
	 			//--------------------------
				$this->set('is_paginating',true);
			if(strpos($this->params->url,'page')!=false){
				$this->set('is_paginating',false);
			}
	 		$boundModel = ClassRegistry::init($string);
	 		$this->paginate = array('limit'=>10);
	 		$data = $this->paginate($string);

	 			//--------------------------
	 		$this->set("display_field", $boundModel->displayField);
	 		$this->set("model",$string);
	 		$this->set("data",$data);

		}


	}
