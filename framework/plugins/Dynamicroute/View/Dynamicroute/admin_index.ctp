<div>
	<div class="navbar-inverse">



		<div class = "navbar-inverse_left admin_padding">
			<a href = '/my/dynamicroutes/add/' class = 'tm_1_em btn btn-sm btn-default pull-right'><div class="glyphicon glyphicon-plus-sign">&nbsp;</div>&nbsp;New Route</a>
			<h2>
				Dynamic Route Management
			</h2>
			<p>
				<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
			</p>
		</div>

		<div class = "clear"></div>
	</div>



<div class="admin_padding">

	<?php echo $this->BootstrapPaginator->pagination(); ?>


	<table class="table table-striped table-bordered table-hover">
		<tr>
			<th class = 'span1' style = 'text-align:center'>
				<?php echo $this->BootstrapPaginator->sort('id');?>
			</th>
			<th class = 'span4'>
				<?php echo $this->BootstrapPaginator->sort('slug');?>
			</th>

			<th class = 'span1'>
				<?php echo $this->BootstrapPaginator->sort('active');?>
			</th>
			<th class = 'span1'>

			</th>
		</tr>
	<?php foreach ($dynamicroutes as $dynamicroute): ?>
		<tr>
			<td style = 'text-align:center'><?php echo h($dynamicroute['Dynamicroute']['id']); ?>&nbsp;</td>
			<td><?php echo h($dynamicroute['Dynamicroute']['slug']); ?>&nbsp;</td>
			<td><?php echo h($dynamicroute['Dynamicroute']['active']); ?>&nbsp;</td>



			<td class="actions" style = "text-align:center">
				<ul style = "list-style:none; padding:0" >
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	              	Actions
	              </a>
	                <ul class="dropdown-menu  dropdown-menu-right" role="menu" style = "text-decoration:none;">
	                    <li >
							<a href="/<?php echo $dynamicroute['Dynamicroute']['slug']; ?>" target = "_blank" >View</a>
	                    </li>
						<li >
							<a class="ajax_link" href="/my/dynamicroutes/edit/<?php echo $dynamicroute['Dynamicroute']['id']; ?>">Edit</a>
	                    </li>
						<li>
							<?php echo $this->Form->postLink(__('Delete'), '/my/dynamicroutes/delete/'.$dynamicroute['Dynamicroute']['id'], null, __('Are you sure you want to delete # %s?', $dynamicroute['Dynamicroute']['id'])); ?>
	                    </li>
	                </ul>
	            </li>
	          </ul>
			</td>




		</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->BootstrapPaginator->pagination(); ?>
	<div class = "clearfix">&nbsp;</div>
</div>
</div>
