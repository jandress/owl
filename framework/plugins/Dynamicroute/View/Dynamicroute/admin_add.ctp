<div>
	<div class="navbar-inverse">
		<h1><?php  echo __($title_for_layout); ?></h1>		
	</div>
	<div class="admin_subsub"> 
		<div class="admin_padding tm_10_px bm_12_px">
			<?php  echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
		</div>
	</div>	
	<div class = "admin_padding tm_2_em">
		<?php echo $this->BootstrapForm->create('Dynamicroute', array('class' => ''));?>
			<div class = "lm_20_px tm_1_em">
				<?php 
					echo $this->BootstrapForm->input('Dynamicroute.active',array('label'=>'Active'));				
				 ?>
			</div>
			<?php
				echo $this->BootstrapForm->input('slug',array('label'=>'Url','class'=>'col-md-12'));
				echo '<div class="clear clearfix">&nbsp;</div>';
				echo '<div class="clear clearfix tm_1_em">&nbsp;</div>';
				echo $this->BootstrapForm->input('class',$classoptions);
				echo $this->BootstrapForm->hidden('id');
				echo '<div class="clear clearfix tm_1_em">&nbsp;</div>';
			?>
			<hr/>
			<div class="clear clearfix bm_3_em">&nbsp;</div>
			<?php //echo $this->BootstrapForm->submit(__('Submit'),array('div'=>'pull-right'));?>
			<a href = '<?php echo $here; ?>' class='btn btn-default ajax_submit tm_1_em pull-right'>Next ></a>				

		<?php echo $this->BootstrapForm->end();?>		
	</div>
</div>
