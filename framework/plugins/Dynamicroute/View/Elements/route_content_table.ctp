
<table id = "fidtable" class=" table table-striped table-bordered table-hover">
	<tbody>
	<tr>
		<th class="span1" style="text-align:center">
			<a href="/Dynamicroute/Dynamicroute/admin_index/sort:id/direction:asc">Id</a>			
		</th>
		<th class="span4">
			<a href="/Dynamicroute/Dynamicroute/admin_index/sort:slug/direction:asc"><?php echo $display_field ?></a>			
		</th>
	</tr>
	<?php 
	foreach ($data as $key => $value): ?>
	<tr id = "<?php echo($value[$model]['id']); ?>">
		<td style="text-align:center" >
			<?php echo($value[$model]['id']); ?>	
		</td>
		<td>
			<?php echo($value[$model][$display_field]); ?>
		</td>
	</tr>
	<?php endforeach ?>
	</tbody>
</table>

<div  class="pagination">
	<?php  
		echo $this->Paginator->prev();
		echo "&nbsp;";
		echo $this->Paginator->numbers(array('separator'=>''));
		echo "&nbsp;";
		echo $this->Paginator->next();
	?>
</div>



<script type="text/javascript">
	$("#fidtable tr").click(function(e){
		$("tr").removeClass('selectedtr');
		$(e.currentTarget).addClass('selectedtr');
		$("#DynamicrouteForeignid").val($(e.currentTarget).attr('id'));
	});
</script>