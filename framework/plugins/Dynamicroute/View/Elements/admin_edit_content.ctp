
<div>
	<div class="navbar-inverse">
		<h1><?php  echo __($title_for_layout); ?></h1>		
	</div>
	<div class = "admin_padding tm_2_em">
		<?php echo $this->BootstrapForm->create('Dynamicroute', array('class' => ''));?>
			<?php echo $this->BootstrapForm->hidden('id'); ?>
			<?php echo $this->BootstrapForm->hidden('foreignid'); ?>

			<div id ="route_table_holder">
				<?php  echo $this->element('route_content_table'); ?>
			</div>


			<div class="clear clearfix bm_3_em">&nbsp;</div>
			<?php //echo $this->BootstrapForm->submit(__('Submit'),array('div'=>'pull-right'));?>
			<a href = '<?php echo $here; ?>' class='btn btn-default ajax_submit tm_1_em pull-right'>Submit</a>				
			<?php echo $this->BootstrapForm->end();?>		
	</div>
</div>




<script type="text/javascript">
	function initAjax(){
		$('.pagination a').unbind('click');
		$('#DynamicrouteClass').unbind('click');	

		$('.pagination a').click(function(e){
			var thisHref = $(this).attr('href');
			$('#route_table_holder').fadeTo(300, 0).load(thisHref, function() {
				$(this).fadeTo(200, 1);
				initAjax();				
			});
			return false;
		});
		$("#DynamicrouteClass").change(function(e){
			$.ajax({
				url: '/my/dynamicroutes/tablecontent/'+$(e.target).val(),
				success: function(data){
					$("#route_table_holder").empty();
					$("#route_table_holder").html(data);  
					initAjax();
				}
			});
		});		
	}
	initAjax();
</script>

