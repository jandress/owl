<?php
App::uses('AppModel', 'Model');
/**
 * LandingTout Model
 *
 * @property Landingpages $Landingpages
 */
class Landingtout extends AppModel {
/**
 * Use database config
 *
 * @var string
 */
//	public $useDbConfig = 'landings';
var $actsAs = array('Containable','PagekwikTools.Flatlist');
/**
 * Use table
 *
 * @var mixed False or table name
 */
	//public $useTable = 'cows';
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'head';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	public $belongsTo = array(
		'Landingtier' => array(
			'className' => 'Landingtier',
			'foreignKey' => 'landingtier_id',
			'conditions' => '',
			//'fields' => ''
		//	'order' => ''
		)
	);
}
