<?php
App::uses('AppModel', 'Model');
/**
 * Landingpage Model
 *
 * @property Landingtier $Landingtier
 */
class Landingpage extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';
	var $actsAs = array('Containable');

	private function _breadcrumbsIndexPage($ar= null){
			// /my/quizzes/edit/7
			return array(
					'label'=> 'Langing Pages Index',//$ar['Quiz']['name'],
					'attributes'=>array(
						'href'=>'/my/landingpages/',
						'class'=>"",
					),
				);
	}



	public function breadcrumbsIndexPage($ar = null){
			return array($this->_breadcrumbsIndexPage());
	}


	private function _breadcrumbsAddPage($ar){
			// /my/quizzes/edit/7
			return array(
					'label'=> 'test',//$ar['Quiz']['name'],
					'attributes'=>array(
						'href'=>'',
						'class'=>"ajax_link",
					),
				);
		}

	private function _breadcrumbsEditPage($ar){
			// /my/quizzes/edit/7
			return array(
					'label'=> 'test',//$ar['Quiz']['name'],
					'attributes'=>array(
						'href'=>'',
						'class'=>"",
					),
				);
		}

	public $hasMany = array(
		'Landingtier' => array(
			'className' => 'Landingtier',
			'foreignKey' => 'landingpage_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => 'order',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


	public $belongsTo = array(
		'Dynamicroute' => array(
			'className' => 'Dynamicroute',
			'foreignKey' => 'dynamicroute_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
