<?php
App::uses('AppModel', 'Model');
/**
 * Landingtier Model
 *
 * @property Landingpage $Landingpage
 * @property LandingTout $LandingTout
 */
class Landingtier extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';
	var $actsAs = array('Containable','PagekwikTools.Flatlist');

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */

	public function delete ($id = null , $cascade = true ){
		$boundModel = ClassRegistry::init('Landingtout');
		$boundModel->deleteAll(array('Landingtout.landingtier_id'=>$id));
		return parent::delete ($id, $cascade);
	}
	
	
	public $tier_types = array(
		''=>'',
		'responsive_banner'=>"Banner",
		'hero_small'=>"Touts",
	);

	public $belongsTo = array(
		'Landingpage' => array(
			'className' => 'Landingpage',
			'foreignKey' => 'landingpage_id',
			//'conditions' => '',
			//'fields' => '',
			//'order' => ''
		)
	);

	public $hasMany = array(
		'Landingtout' => array(
			'className' => 'Landingtout',
			'foreignKey' => 'landingtier_id',
			'dependent' => false,
			// 'conditions' => '',
			// 'fields' => '',
			'order' => 'Landingtout.order',
			// 'limit' => '',
			// 'offset' => '',
			// 'exclusive' => '',
			// 'finderQuery' => '',
			// 'counterQuery' => ''
		)
	);

}
