<?php 
  $multiple = count($tier_data['Landingtout']) > 1;
?>

<div class="bs-example" data-example-id="simple-carousel">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
            <?php foreach ($tier_data['Landingtout'] as $key => $value): 
            $active = $key == 0 ? 'active' : '';
            ?>
              <li data-target="#carousel-example-generic" data-slide-to="<?php echo $key; ?>" class="<?php echo $active; ?>"></li>
            <?php endforeach; ?>  
    
      </ol>
      <div class="carousel-inner" role="listbox">

      <?php foreach ($tier_data['Landingtout'] as $key => $value): 
          $active = $key == 0 ? 'active' : '';
        ?>

        <div class="item <?php echo $active; ?>">
          <div class="carousel-caption">
            <h1><?php echo  $value['head'] ?></h1>
            <p class="lead"><?php echo  $value['copy'] ?></p>
            <a class="btn btn-large btn-primary" href="<?php echo $value['cta_url'] ?>"><?php echo $value['cta_label'] ?></a>
          </div>
          <img data-src="holder.js/900x500/auto/#777:#555/text:First slide" alt="First slide [900x500]" src="<?php echo  $value['image_url']; ?>" data-holder-rendered="true">
        </div>
      <?php endforeach; ?>
      </div>
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>

<div class = "clear clearfix">&nbsp;</div>