<div class="container marketing">
	<div class="row">
		<?php
		foreach ($tier_data['Landingtout'] as $key => $value): 
		//	debug($value);
	//	 foreach (array(1,2,3,4,5) as $key => $value): 
		 	//$active = $key == 0 ? 'active' : '';
		?>
		<div class="col-lg-4">
		  <img class="img-circle" src="<?php echo $value['image_url'];?>" alt="<?php echo $value['head'];?> image" style="width: 140px; height: 140px;">
		  <h2>
		  	<?php echo $value['head']; ?>
		  </h2>
		  <p>
		  	<?php echo $value['copy']; ?>
		  </p>
		  <p>
		  	<a class="btn btn-success" target="<?php echo $value['cta_target']; ?>"  href="<?php echo $value['cta_url']; ?>" role="button">
		  		<?php echo $value['cta_label']; ?> »
		  	</a>
		  </p>
			<div class = "clear bm_2_em"></div>		  
		</div>

		<?php endforeach; ?>
	</div>
</div>