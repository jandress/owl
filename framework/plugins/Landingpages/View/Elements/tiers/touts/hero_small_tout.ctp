	<div class="col-lg-4">
	  <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" style="width: 140px; height: 140px;">
	  <h2><?php echo $value['head']; ?></h2>
	  <p><?php echo $value['copy']; ?></p>
	  <p><a class="btn btn-default" target="<?php echo $value['cta_target']; ?>"  href="<?php echo $value['cta_url']; ?>" role="button"><?php echo $value['cta_label']; ?> »</a></p>
	</div>