<div class="item <?php echo $active ?>">
  <img src="/img/banners/section_banners/writing_process/background.jpg" alt="First slide">
  <div class="container">
    <div class="carousel-caption">
      <h1>
      	<?php if(!empty($value['head'])){ echo $value['head']; } ?>
      </h1>
      <p>
      	<?php if(!empty($value['copy'])){ echo $value['copy']; } ?>
      </p>
      <p>

      <?php if(
      	!empty($value['cta_label']) &&
		!empty($value['cta_url']) &&
		!empty($value['cta_target'])					
      ):?>
      	<a class="btn btn-lg btn-primary" target = "<?php echo $value['cta_target']; ?>" href="<?php echo $value['cta_url']; ?>" role="button"><?php echo $value['cta_label']; ?></a></p>
		<?php endif; ?>   	
    </div>
  </div>
</div>