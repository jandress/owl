<?php
 //foreach (array(1,2,3,4,5) as $key => $value): 
foreach ($this->data['Landingtout'] as $key => $value): 
 	$active = $key == 0 ? 'active' : '';
?>
<div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">Your education is complete. <span class="text-muted">The Student is now the master.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive" src="/img/fpo/500x500_fpo.png" alt="Generic placeholder image">
        </div>
      </div>

<hr class="featurette-divider">      
<?php endforeach; ?>