<?php if (!empty($landingtiers)): ?>


	

<table class="table table-striped table-bordered table-hover tm_1_em">
		<tbody>
			<tr>
				<th class="span1" style="text-align:center">id</th>
				<th class="span4"><a href="/my/landingpages/sort:title/direction:asc">Title</a></th>
				<th class="span1"><a href="/my/landingpages/sort:layout/direction:asc">Tier Type</a></th>
				<th class="actions span3" style="text-align:center">Actions</th>
				<?php if (count($landingtiers)>1): ?>
					<th class="actions span1" style="text-align:center"></th>					
				<?php endif ?>
			</tr>

			<?php foreach ($landingtiers as $key => $value): 
			?>
				<tr>
					<td style="text-align:center">
						<?php echo $value['Landingtier']['id']; ?>&nbsp;
					</td>
					<td>
						<?php echo $value['Landingtier']['title']; ?>
						&nbsp;
					</td>
					<td>
						<?php echo $value['Landingtier']['type'] ?>
						&nbsp;
					</td>

					<td class="actions" style="text-align:center">
						<ul style="list-style:none; padding:0">
			            <li class="dropdown">
			              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
			              	Actions
			              </a>
			                <ul class="dropdown-menu  dropdown-menu-right" role="menu" style="text-decoration:none;">
								<li>	
									<a href="/my/landingpages/tiers/edit/<?php echo $value['Landingtier']['id'] ?>" target="_blank">Edit tier</a>
			                    </li>
								<li>	
						<?php

									
							echo $this->Form->postLink( __('Delete'), '/my/landingtiers/delete/'.$value['Landingtier']['id'] , array($value['Landingtier']['id']) ,__('Are you sure you want to delete this tier and all associated (child) content?', $value['Landingtier']['id']) );									
									
						?>



								</li>
			                </ul>
			            </li>
			          </ul>	


					</td>
					
					<?php if (count($landingtiers)>1): ?>
						<td style = "text-align:center;" >
							<span class = "btn-group-vertical repos_btns">
								<a href = "/landingtier/moveup/<?php echo $value['Landingtier']['id']; ?>" class = 'repos_btn btn btn-default btn-xs'><span class = "glyphicon glyphicon-arrow-up"></span></a>
								<a  href = "/landingtier/movedown/<?php echo $value['Landingtier']['id']; ?>" class = 'repos_btn btn btn-default btn-xs'><span class = "glyphicon glyphicon-arrow-down"></span></a>
							</span>
						</td>
					<?php endif ?>
				</tr>					
			<?php endforeach ?>

		</tbody>
</table>
<?php echo $this->BootstrapPaginator->pagination(); ?>
<?php endif ?>