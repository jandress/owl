<div class="admin_padding">
	<?php echo $this->BootstrapForm->create('Landingtout', array('class' => 'form-horizontal'));?>
		<fieldset>
			<?php
			$c = array('class'=>'col-md-12');
			echo $this->BootstrapForm->hidden('landingtier_id' );
			echo $this->BootstrapForm->input('id' , $c);
			echo $this->BootstrapForm->input('head' , $c);
			echo $this->BootstrapForm->input('copy' , array('class'=>'col-md-12 ckeditor'));
			echo $this->BootstrapForm->input('cta_label' , $c);
			echo $this->BootstrapForm->input('cta_url' , $c);
			echo $this->BootstrapForm->input('cta_target',$cta_target_options);
			echo $this->BootstrapForm->input('image_url' , $c);
			?>
			<div class = "clear tm_1_em bm_1_em"></div>
			<?php echo $this->BootstrapForm->submit(__('Submit'));?>
		</fieldset>
	<?php echo $this->BootstrapForm->end();?>
</div>
