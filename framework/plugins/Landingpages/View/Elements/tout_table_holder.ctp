


<div class = "tm_1_em" id = "landing_tout_table">
	<?php

	 if (!empty($landingTouts)): ?>
		<?php echo $this->element('landing_tout_table') ?>	
	<?php else: ?>	

	
	<?php endif ?>

</div>
<script type="text/javascript">
	function bindRepos(){
		$(".repos_btns a").unbind('click');
		$(".repos_btns a").click(function(e){
			e.preventDefault();
			var _url = $(e.currentTarget).attr("href");
			$("#landing_tout_table").empty();
			$.ajax({
				url: _url,
				context: $("#landing_tout_table "),
				success: function(data){
					//console.log('success: '+data);
					//return;
					$(this).empty();
					$(this).html(data);  
					bindRepos();
				}
			});	
			return false;
		});
	}
	bindRepos();
</script>