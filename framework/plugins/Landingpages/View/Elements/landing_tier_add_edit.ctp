<?php echo $this->BootstrapForm->create('Landingtier', array('class' => '_form-horizontal'));?>
<div class="tm_1_em">
	<fieldset>
		<?php	
			echo $this->BootstrapForm->input('id',array('class'=>'col-md-12'));
			echo $this->BootstrapForm->hidden('landingpage_id');
			echo $this->BootstrapForm->input('title',array('class'=>'col-md-12','label'=>'Title'));
			//echo $this->BootstrapForm->input('cmsname',array('class'=>'col-md-6','label'=>'Cms Label'));
				if($this->data['Landingtier']['type'] =='responsive_banner'){
					$disp = "block";
				}else{
					$disp = "none";
				}			
			?>

			
			<div class = "clearfix bm_1_em">&nbsp;</div><?php
			echo $this->BootstrapForm->input('type',array('options'=>$tier_types));
			?>&nbsp;
			<div id ='LandingtierCopyHolder' class = "" style = "display:<?php echo $disp; ?>">
				<?php echo $this->BootstrapForm->input('copy',array('options'=>$banner_options['options'],'class'=>'','label'=>'Available Banners'));?>
			</div>			
			<div class = "clearfix">&nbsp;</div>			

			<?php echo $this->BootstrapForm->submit('Submit',array('class'=>'btn pull-right'));?>
			<?php echo $this->BootstrapForm->end();?>			
		<div class = "clearfix bm_1_em">&nbsp;</div>					
	</fieldset>
	

	<?php if (!empty($this->data['Landingtier']['id'])):
			if($this->data['Landingtier']['type'] =='responsive_banner'){
				$disp = "none";			
			}else{
				$disp = "block";
			}			
		 ?>
		<div id="landing_tout_table_holder" style = "display:<?php echo $disp; ?>" >
			<hr/>
			<h3 style="display:inline">
				Touts
			</h3>			
			<?php
				echo $this->element('new_tout_btn');	
				echo $this->element('tout_table_holder'); 
			?>		
		</div>
	<?php endif ?>





</div>





<div class = "clearfix">&nbsp;</div>

<script type="text/javascript">
	$(function () {
	  $('[data-toggle="popover"]').popover()

	})
	$("#LandingtierType").change(function(e){
		switch($(e.target).val().toLowerCase()){
			case('responsive_banner'):			
				$("#LandingtierCopyHolder").css('display','block');	
				$("#landing_tout_table_holder").css('display','none');					
			break;
			default:
				$("#LandingtierCopyHolder").css('display','none');
				$("#landing_tout_table_holder").css('display','block');									
			break;			
		}
		
	});
</script>