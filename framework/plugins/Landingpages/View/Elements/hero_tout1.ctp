<div class="span4 hero-tout">
	<div class="pl_2_em pr_2_em pb_2_em pt_1_em">
		<div style='height:150px'>
			<h2>
				<?php echo $data['title']; ?>
			</h2>
		   	<p>
				<?php echo $data['copy']; ?>
			</p>
		</div>
	   <p><a class="btn <?php echo $button_color; ?>"  target ="<?php echo $data['cta_target']; ?>" href="<?php echo $data['href']; ?>">View details &raquo;</a></p>		
	</div>
</div>
