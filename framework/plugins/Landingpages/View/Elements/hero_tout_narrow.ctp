<div class="span3 hero-tout white_hero_tout" style = 'overflow-x:hidden'>

	<div class="pl_2_em pr_2_em pb_2_em pt_1_em ">
	
		<div >
			<div class="">
				<?php if (strlen($data['head'])<20): ?>
					<h2 style = 'text-align:center; '>
						<?php echo $data['head']; ?>
					</h2>
				<?php else: ?>
					<h3 style = 'text-align:center; '>
						<?php echo $data['head']; ?>
					</h3>
				<?php endif ?>
			</div>

				<?php $image_size = '45%' ?>


				<?php $fle =  "/img/avatars/".$data['image_url']; ?>
				<div class="" style='text-align:center;'>
					<?php if(file_exists("../webroot/img/avatars/".$data['image_url']) && $fle != "/img/avatars/"): ?>
						<img style='width:<?php echo $image_size; ?>' src="<?php echo $fle; ?>" class="img-circle" alt = "<?php echo $data['head']; ?>"/>					
					<?php else: ?>
						<img style='width:<?php echo $image_size; ?>' src="/img/avatars/default.jpg" class="img-circle" alt = "<?php echo $data['head']; ?>"/>
					<?php endif ?>
				</div>



			<div class="tm_1_em">
			   	<small>
					<?php echo $data['copy']; ?>
				</small>
			</div>
			
		</div>
				
		<div class="pr_2_em pl_2_em">
			<?php if (
				!empty($data['cta_label'])&&
				!empty($data['cta_url'])				
			): ?>
		   <p><a class="btn btn-block tm_1_em <?php echo $button_color; ?>"  target ="<?php echo $data['cta_target']; ?>" href="<?php echo $data['cta_url']; ?>"><?php echo $data['cta_label'] ?></a></p>					
			<?php else: ?>
				<div class = "clearfix">&nbsp;</div>
			<?php endif ?>
		</div>		
		
	</div>
</div>
<div class="top_nav_phone_spacer visible-phone">&nbsp;</div>