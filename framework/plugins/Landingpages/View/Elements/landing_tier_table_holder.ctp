<div id = "landing_tier_table">
	<?php echo $this->element('landing_tier_table'); ?>				
</div>

<script type="text/javascript">
	function bindRepos(){
		$(".repos_btns a").unbind('click');
		$(".repos_btns a").click(function(e){
			e.preventDefault();
			var _url = $(e.currentTarget).attr("href");
			$("#landing_tier_table").empty();
			$.ajax({
				url: _url,
				context: $("#landing_tier_table"),
				success: function(data){
					//console.log('success: '+data);
					//return;
					$(this).empty();
					$(this).html(data);  
					bindRepos();
				}
			});	
			return false;
		});
	}
	bindRepos();
</script>