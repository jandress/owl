<?php echo $this->BootstrapForm->create('Landingpage', array('class' => '_form-horizontal'));?>
<div class="tm_1_em">
	<fieldset>
		<?php	
			echo $this->BootstrapForm->hidden('id',array('class'=>'col-md-12'));
			echo $this->BootstrapForm->input('title',array('class'=>'col-md-12','label'=>'Title'));
			echo $this->BootstrapForm->hidden('cmsname',array('class'=>'col-md-6','label'=>'Cms Label'));
			echo $this->BootstrapForm->hidden('home');
			// <button type="button" class="btn btn-sm btn-default btn-info" data-toggle="popover" title="Cms-only title" data-content="This optional field is to differentiate different pages with the same name in the cms. If you leave it blank, the title will be used on the index page and others.">?</button>
			?>&nbsp;

			<?php
			echo $this->BootstrapForm->hidden('layout', $layout_options);			
		?>

	</fieldset>
	<?php echo $this->BootstrapForm->submit('Submit',array('class'=>'btn btn-sm pull-right tm_1_em'));?>
	<?php echo $this->BootstrapForm->end();?>
	<div class = "clearfix bm_6_em">&nbsp;</div>					
	<?php if (!empty($this->data)): ?>
		<h3 style="display:inline">
			Tiers
		</h3>	
		<?php echo $this->element('new_tier_btn') ?>
		<?php echo $this->element('landing_tier_table_holder'); ?>				
	<?php endif ?>
</div>







	







<div class = "clearfix">&nbsp;</div>




<script type="text/javascript">
$(function () {
  $('[data-toggle="popover"]').popover()
})



<?php $this->Js->buffer('var tierslider = ajax_slider();
tierslider.init("#ajax_container",".ajax_link");            
main_content_ajax_helper.onLoaded = function(){
	alert("yes!");
};') ?>



// $('.repos_btns a').click(function(e){
// 	alert( $(e.target).attr('href') );
// 	return false;
// })
</script>

