
<?php if (!empty($landingTouts)): ?>
	


<table class="table table-striped table-bordered table-hover">
		<tr>
		<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
		<th><?php echo $this->BootstrapPaginator->sort('head');?></th>
		<th><?php echo $this->BootstrapPaginator->sort('cta_label');?></th>
		<th><?php echo $this->BootstrapPaginator->sort('cta_url');?></th>
		<th><?php echo $this->BootstrapPaginator->sort('image_url');?></th>
		<th style="text-align:center" class="actions"><?php echo __('Actions');?></th>






<?php if (count($landingTouts)>1): ?>
			<th class="actions span1" style="text-align:center"></th>					
		<?php endif ?>



	</tr>

				<?php foreach ($landingTouts as $key => $landingTout): ?>

	<tr>
		<td><?php echo h($landingTout['Landingtout']['id']); ?>&nbsp;</td>
		<td><?php echo h($landingTout['Landingtout']['head']); ?>&nbsp;</td>
		<td><?php echo h($landingTout['Landingtout']['cta_label']); ?>&nbsp;</td>
		<td><?php echo h($landingTout['Landingtout']['cta_url']); ?>&nbsp;</td>
		<td><?php echo h($landingTout['Landingtout']['image_url']); ?>&nbsp;</td>
		<!--  -->
		<td class="actions" style = "text-align:center">
			<ul style = "list-style:none; padding:0" >
		    <li class="dropdown">
		      <a href="#" class="dropdown-toggle" data-toggle="dropdown" >
		      	Actions
		      </a>
		        <ul class="dropdown-menu  dropdown-menu-right" role="menu" style = "text-decoration:none;">
		            <li>	
						<a href="/my/landingpages/touts/edit/<?php echo $landingTout['Landingtout']['id']; ?>">Edit</a>
		            </li>
		            <li>	
						<?php
							echo $this->Form->postLink( __('Delete'), '/my/landingtouts/delete/'.$landingTout['Landingtout']['id'] , array($landingTout['Landingtout']['id']) ,__('Are you sure you want to delete # %s?', $landingTout['Landingtout']['id']) );
				?>
				
				
				
		            </li>                        
		        </ul>
		    </li>
		  </ul>	
		</td>



			<?php if (count($landingTouts)>1): ?>
				<td style = "text-align:center;" >
					<span class = "btn-group-vertical repos_btns">
						<a href = "/landingtouts/moveup/<?php echo $landingTout['Landingtout']['id']; ?>" class = 'repos_btn btn btn-default btn-xs'><span class = "glyphicon glyphicon-arrow-up"></span></a>
						<a  href = "/landingtouts/movedown/<?php echo $landingTout['Landingtout']['id']; ?>" class = 'repos_btn btn btn-default btn-xs'><span class = "glyphicon glyphicon-arrow-down"></span></a>
					</span>
				</td>
			<?php endif ?>
		</tr>				




		<!--  -->
	</tr>
<?php endforeach; ?>
</table>
<div class = "tm_1_em">
	<p><?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
	</p>
</div>		
<?php echo $this->BootstrapPaginator->pagination(); ?>


<?php endif ?>