<div class="navbar-inverse">

		<div class="col-md-6">
			<h2><?php echo __('Edit Tier'); ?></h2>
			<p>
				Editing tier <em><?php echo($this->data['Landingtier']['title']); ?></em> on landing page <em><?php echo($this->data['Landingpage']['title']); ?></em>.
			</p>
		</div>
		<div class="col-md-6 tm_2_em">
			<a target = "_blank" class='btn btn-default pull-right btn-sm' href='/my/landingpages/view/<?php echo $this->data['Landingtier']['landingpage_id'] ?>'>View Page </a>		
		</div>

		<div class = "clearfix">&nbsp;</div>			
</div>
<div class="admin_subsub "> 
	<div class="admin_padding tm_10_px bm_12_px">
			<?php  echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
		<div class = "clearfix">&nbsp;</div>				
	</div>
</div>

<div class="admin_padding tm_1_em">
	<?php echo $this->element('landing_tier_add_edit'); ?>			
</div>

