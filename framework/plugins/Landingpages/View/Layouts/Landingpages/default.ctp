<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo __($title_for_layout); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<?php if ($isIE): ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<?php endif ?>

    <!-- CSS -->
  	<?php echo($this->AssetCompress->css('common', array('raw' => $concat_assets))."\n"); ?>
	<script type="text/javascript" src = '/js/jquery.js'></script>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="/landingpages/js/html5shiv.js"></script>
    <![endif]-->
	<?php echo $this->element('/fluid_theme/background_css'); ?>	
  </head>


  <body>
    <!-- Part 1: Wrap all page content here -->
    <div id="wrap">
      <!-- Begin page content -->
	<?php echo($this->element('navigation/top_nav')); ?>		
	
	
	<div class = "clearfix tm_10_px hidden-desktop" style = "height:1px;" >&nbsp;</div>
	<div class = "clearfix tm_60_px visible-desktop">&nbsp;</div>	
	
	
      <div class="container">
        <div class="content">
		  <div class="container">
			<h1>Landing Page Layout does not exist</h2>
		  </div>
      	</div><!--/row-->
		<div class = "clearfix tm_2_em hidden-phone">&nbsp;</div>
		<div class = "clearfix tm_5_px visible-phone" style='height:5px;' >&nbsp;</div>
      </div>
	<div class = "clearfix">&nbsp;</div>
    </div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
	<?php $background_image ='armillarywatermark_dark.jpg' ?>
	<div class="fullscreen-image">
	  <img alt="" src="/img/backgrounds/<?php echo $background_image; ?>">
	</div>
	
	
	
	<?php echo $this->element('global/footer');?>	




  </body>
</html>
