<div class="navbar-inverse">
	<h1><?php echo __('Add %s', __('Landingpage')); ?></h1>
	<div class = "clearfix">&nbsp;</div>			
</div>
<div class="admin_subsub "> 
	<div class="admin_padding tm_10_px bm_12_px">
		<?php  echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
	</div>
</div>
<div class="admin_padding">
	<?php echo $this->element('landing_add_edit'); ?>			
</div>
<div class = "clearfix">&nbsp;</div>