<div class="navbar-inverse">
	<div class="col-md-6">
		<h1><?php echo __('Editing %s', __('Landingpage')); ?></h1>
		<h3>	
			<?php echo($this->data['Landingpage']['title']); ?>
		</h3>
	</div>	

	<div class="col-md-6 tm_2_em">
		<a target = "_blank" class='btn btn-default pull-right btn-sm' href='/my/landingpages/view/<?php echo $this->data['Landingpage']['id'] ?>'>View Page </a>	
	</div>	
	<div class = "clearfix bm_1_em">&nbsp;</div>			
</div>

<div class="admin_subsub "> 
	<div class="admin_padding tm_10_px bm_12_px bm_12_px">
		<?php  echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
	</div>
</div>

<div class="admin_padding tm_1_em">
		<?php echo $this->element('landing_add_edit'); ?>			
</div>


