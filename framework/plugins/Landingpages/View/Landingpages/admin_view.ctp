<div class="row-fluid">
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('')), array('controller' => '', 'action' => 'add'));?> </li>
		</ul>
	</div>
	<div class="span9">
		<h2><?php  echo __('Landingpage');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($landingpage['Landingpage']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('H1'); ?></dt>
			<dd>
				<?php echo h($landingpage['Landingpage']['h1']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('H2'); ?></dt>
			<dd>
				<?php echo h($landingpage['Landingpage']['h2']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Copy'); ?></dt>
			<dd>
				<?php echo h($landingpage['Landingpage']['copy']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Layout'); ?></dt>
			<dd>
				<?php echo h($landingpage['Landingpage']['layout']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Cta Label'); ?></dt>
			<dd>
				<?php echo h($landingpage['Landingpage']['cta_label']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Cta Url'); ?></dt>
			<dd>
				<?php echo h($landingpage['Landingpage']['cta_url']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Landingpage')), array('action' => 'edit', $landingpage['Landingpage']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Landingpage')), array('action' => 'delete', $landingpage['Landingpage']['id']), null, __('Are you sure you want to delete # %s?', $landingpage['Landingpage']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__( __('Landingpages')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Landingpage')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__( __('Landing Touts')), array('controller' => 'landing_touts', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Landing Tout')), array('controller' => 'landing_touts', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Landing Touts')); ?></h3>
	<?php if (!empty($landingpage['Landingtout'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Landingpages Id'); ?></th>
				<th><?php echo __('Head'); ?></th>
				<th><?php echo __('Cta Label'); ?></th>
				<th><?php echo __('Cta Url'); ?></th>
				<th><?php echo __('Copy'); ?></th>
				<th><?php echo __('Image Url'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($landingpage['Landingtout'] as $landingTout): ?>
			<tr>
				<td><?php echo $landingTout['id'];?></td>
				<td><?php echo $landingTout['landingpage_id'];?></td>
				<td><?php echo $landingTout['head'];?></td>
				<td><?php echo $landingTout['cta_label'];?></td>
				<td><?php echo $landingTout['cta_url'];?></td>
				<td><?php echo $landingTout['copy'];?></td>
				<td><?php echo $landingTout['image_url'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'landing_touts', 'action' => 'view', $landingTout['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'landing_touts', 'action' => 'edit', $landingTout['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'landing_touts', 'action' => 'delete', $landingTout['id']), null, __('Are you sure you want to delete # %s?', $landingTout['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
</div>
