<div>
	<div class="navbar-inverse">
		<h2>Landingpages</h2>
		<div class = "navbar-inverse_left">
			<div class="nav span3 pull-right admin_padding">
				<a href = '/my/landingpages/add/' class = 'btn btn-sm btn-default pull-right'>
					<div class="glyphicon glyphicon-plus-sign">&nbsp;</div>&nbsp;New Landing Page
				</a>
			</div>
			<p>
				<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
			</p>
		</div>

		<div class = "clear tm_1_em"></div>
	</div>

<div class="admin_padding">
	<?php echo $this->BootstrapPaginator->pagination(); ?>
	<table class="table table-striped table-bordered table-hover">
		<tr>

			<th class = 'span1' style = 'text-align:center'><?php echo $this->BootstrapPaginator->sort('id');?></th>
			<th class = 'span4'><?php echo $this->BootstrapPaginator->sort('title');?></th>
			<th class = 'span1'><?php echo $this->BootstrapPaginator->sort('layout');?></th>
			<th class="actions span3" style = 'text-align:center'><?php echo __('Actions');?></th>
		</tr>
	<?php foreach ($landingpages as $landingpage): ?>
		<tr>
			<td style = 'text-align:center'><?php echo h($landingpage['Landingpage']['id']); ?>&nbsp;</td>
			<td><?php echo h($landingpage['Landingpage']['title']); ?>&nbsp;</td>
			<td><?php echo h($landingpage['Landingpage']['layout']); ?>&nbsp;</td>
			<td class="actions" style = "text-align:center">
				<ul style = "list-style:none; padding:0" >
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	              	Actions
	              </a>
	                <ul class="dropdown-menu  dropdown-menu-right" role="menu" style = "text-decoration:none;">
	                    <li>
							<a href="/my/landingpages/view/<?php echo $landingpage['Landingpage']['id']; ?>" target = "_blank" >View</a>
	                    </li>
						<li>
							<a class="" href="/my/landingpages/edit/<?php echo $landingpage['Landingpage']['id']; ?>">Edit</a>
	                    </li>
						<li>
							<?php


echo $this->Form->postLink( __('Delete'), '/my/landingpages/delete/'.$landingpage['Landingpage']['id'] , array($landingpage['Landingpage']['id']) ,__('Are you sure you want to delete # %s?', $landingpage['Landingpage']['id']) );
//echo $this->Form->postLink(__('Delete'), '/my/landingpages/delete/'.$landingpage['Landingpage']['id'], null, __('whats up with this? %s?', $landingpage['Landingpage']['id']));
///echo $this->Form->postLink(__("Delete This Link Only"),"/my/navlinks/remove/".$nl["id"],array("data"=>array("id"=>$nl["id"])),__("Are you sure you want to delete this link? ".  $childrenCount." children will be moved up one level. This could make your sitemap messy if you don\"t know what to expect."));
 							?>

							</li>
	                </ul>
	            </li>
	          </ul>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->BootstrapPaginator->pagination(); ?>
	<div class = "clearfix">&nbsp;</div>
</div>
</div>
