



<div class="navbar-inverse">
	<h1><?php echo __('Adding new %s', __('Tout')); ?></h1>
	<p>Adding a new tout to the Landing tier <em><?php echo $this->data['Landingtier']['title']; ?></em> on the landing page <em><?php echo $this->data['Landingpage']['title']; ?></em>.</p>	
	<div class = "clearfix">&nbsp;</div>			
</div>

<div class="admin_subsub "> 
	<div class="admin_padding tm_10_px bm_12_px">
		<?php  echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
	</div>
</div>


<div class="admin_padding">
	<?php echo $this->element('tout_edit'); ?>
</div>

<div class = "clearfix">&nbsp;</div>
