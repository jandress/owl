<div class="row-fluid">
	<div class="span9">
		<h2><?php echo __( __('Landing Touts'));?></h2>

		<p>
			<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
		</p>

		<table class="table">
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('landingpage_id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('head');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('cta_label');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('cta_url');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('copy');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('image_url');?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($landingTouts as $landingTout): ?>
			<tr>
				<td><?php echo h($landingTout['Landingtout']['id']); ?>&nbsp;</td>
				<td><?php echo h($landingTout['Landingtout']['landingpage_id']); ?>&nbsp;</td>
				<td><?php echo h($landingTout['Landingtout']['head']); ?>&nbsp;</td>
				<td><?php echo h($landingTout['Landingtout']['cta_label']); ?>&nbsp;</td>
				<td><?php echo h($landingTout['Landingtout']['cta_url']); ?>&nbsp;</td>
				<td><?php echo h($landingTout['Landingtout']['copy']); ?>&nbsp;</td>
				<td><?php echo h($landingTout['Landingtout']['image_url']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $landingTout['Landingtout']['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $landingTout['Landingtout']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $landingTout['Landingtout']['id']), null, __('Are you sure you want to delete # %s?', $landingTout['Landingtout']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>

		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('New %s', __('Landing Tout')), array('action' => 'add')); ?></li>
		</ul>
		</div>
	</div>
</div>