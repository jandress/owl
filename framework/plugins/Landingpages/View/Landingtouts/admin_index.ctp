<div class="navbar-inverse">
	<h2><?php echo __( __('Landing Touts Index'));?></h2>
	<p>Listing touts for the Landing tier <em><?php echo $this->data['Landingtier']['title']; ?></em> on the landing page <em><?php echo $this->data['Landingpage']['title']; ?></em></p>
	<div class = "clearfix">&nbsp;</div>			
</div>
<div class="admin_subsub "> 
	<div class="admin_padding tm_10_px bm_12_px">
		<?php echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
	</div>
</div>
<div class = "clear bm_1_em"></div>


<div class="admin_padding">		
			<a class = "btn btn-sm btn-default pull-right" href="/my/landingpages/touts/add/<?php echo $this->data['Landingtier']['id']; ?>"> New Tout</a>
			<div class = "clear clearfix">&nbsp;</div>
			<?php echo $this->element('tout_table_holder') ?>			

</div>

<div class = "clearfix">&nbsp;</div>
