<div class="row-fluid">
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('')), array('controller' => '', 'action' => 'add'));?> </li>
		</ul>
	</div>
	<div class="span9">
		<h2><?php  echo __('Landing Tout');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($landingTout['Landingtout']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Landingpages Id'); ?></dt>
			<dd>
				<?php echo h($landingTout['Landingtout']['landingpage_id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Head'); ?></dt>
			<dd>
				<?php echo h($landingTout['Landingtout']['head']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Cta Label'); ?></dt>
			<dd>
				<?php echo h($landingTout['Landingtout']['cta_label']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Cta Url'); ?></dt>
			<dd>
				<?php echo h($landingTout['Landingtout']['cta_url']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Copy'); ?></dt>
			<dd>
				<?php echo h($landingTout['Landingtout']['copy']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Image Url'); ?></dt>
			<dd>
				<?php echo h($landingTout['Landingtout']['image_url']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Landing Tout')), array('action' => 'edit', $landingTout['Landingtout']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Landing Tout')), array('action' => 'delete', $landingTout['Landingtout']['id']), null, __('Are you sure you want to delete # %s?', $landingTout['Landingtout']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__( __('Landing Touts')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Landing Tout')), array('action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

