<?php
App::uses('AppController', 'Controller');

class LandingpagesController extends AppController {

	public $layout = 'frontend';
	public $defaultRedirect = '/my/landingpages/';
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator','Quizmodules.Breadcrumbs');
	public $components = array('Session','Landingpages.Fileutil','Auth','Quizmodules.QAdminbreadcrumb','Paginator');
	public $uses = array('Landingpages.Landingpage','Landingpages.Landingtier','Landingpages.Landingtout');

	var $layout_options = array(
		'options'=>array(
			'condensed'=>'Condensed',
			'hero'=>'Hero Classic',
			'hero_b'=>'Hero Updated',
		)
	);

	var $applicationLayoutDirectory;
	var $pluginLayoutDirectory;

	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('index','view');
		$this->applicationLayoutDirectory = APP.'View/Layouts/Landingpages/';
		$this->pluginLayoutDirectory = dirname(__FILE__).'/../View/Layouts/Landingpages/';
		//	$this->cleanResponsiveBannerPaths();
	}



	public function index() {
		//what landing page has been set in the database
		//$this->Landingpage->recursive = 4;
		$this->Landingpage->contain('Landingtier.Landingtout');
		$d = $this->Landingpage->find('first',array('conditions'=>array('home'=>1)));
		if(empty($d)){
			$this->Landingpage->contain('Landingtier.Landingtout');
			$d = $this->Landingpage->find('first');
		}
		$file_exists = $this->layoutExists($d['Landingpage']['layout']);
		if(!empty($d['Landingpage']['layout']) && $file_exists){
			$this->layout = $d['Landingpage']['layout'];
		}else{
			$this->layout = 'hero';
		}
		$this->layout = 'Landingpages/'.$this->layout;
		$this->data = $d;
		$this->set('landingpage', $d);
	}




	function cleanResponsiveBannerPaths(){
		$this->Landingtier->contain();
		$b = $this->Landingtier->find('all',array('conditions'=>array('type'=>'responsive_banner')));
		foreach ($b as $key => $value) {
//			$value['Landingtier']['copy']
			$a = explode('img/banners/section_banners/',$value['Landingtier']['copy']);
			if(isset($a[1])){
				$c = $a[1];
				$_t =explode('/index.php', $c);
				if(isset($_t[0])){
					$b[$key]['Landingtier']['copy'] = $_t[0];
				}
			}
		}
		debug($this->Landingtier->saveAll($b));
		exit;
	}


	public function view($id = null) {
	$this->Landingpage->contain('Landingtier.Landingtout');
		$d = $this->Landingpage->find('first',array('conditions'=>array('id'=>$id)));
//		$this->cleanResponsiveBannerPaths();exit;
		// /img/banners/section_banners
//		debug($this->Fileutil->getDirectoryContents('./img/banners/section_banners')); exit;
		$file_exists = $this->layoutExists($d['Landingpage']['layout']);
		if(!empty($d['Landingpage']['layout']) && $file_exists){
			$this->layout = $d['Landingpage']['layout'];
		}else{
			$this->layout = 'hero';
		}
		$this->layout = 'Landingpages/'.$this->layout;
		$this->data = $d;
		$this->set('landingpage', $d);
		// $this->Landingpage->id = $id;
		// if (!$this->Landingpage->exists()) {
		// 	throw new NotFoundException(__('Invalid %s', __('landingpage')));
		// }
		// $this->data = $d = $this->Landingpage->find('first', array('conditions'=>array('Landingpage.id'=>$id)));


		// $this->set('title_for_layout', $this->data['Landingpage']['title']);

		// $file_exists = $this->layoutExists($d['Landingpage']['layout']);

		// if(!empty($d['Landingpage']['layout']) && $file_exists){
		// 	$this->layout = 'Landingpages/'.$d['Landingpage']['layout'];
		// }else{
		// 	$this->layout = 'Landingpages/default';
		// }
		// //		$this->layout = 'Landingpages/'.$this->layout;
		// $this->set('landingpage', $d);
	}


	private function layoutExists($file){
	 	return file_exists($this->applicationLayoutDirectory.$file.'.ctp')  ||
		file_exists($this->pluginLayoutDirectory.$file.'.ctp');
	}


	public function admin_index() {
		if ($this->request->is('post') || $this->request->is('put')) {
				$this->Landingpage->updateAll(array('home'=>0));
				$this->Landingpage->updateAll(array('home'=>1),array('id'=>$this->request->data['Landingpage']['home']));
		}
		$this->Landingpage->recursive = 0;
		$this->set('landingpages', $this->paginate());
	}


	private function getLayouts(){
		if(file_exists($this->applicationLayoutDirectory)){
			$n = $this->applicationLayoutDirectory;
		}else{
			//		$n = dirname(__FILE__).'/../View/Layouts';
			$n = $this->pluginLayoutDirectory;
		}
		return array('hero'=>'hero');//$this->Fileutil->getFileListingForOptions($n,array('fileNameAsKeys'=>true,'removeFileExtension'=>true));
	}


	private function banners(){
		$n = './banners/';
		$d = $this->Fileutil->getFileListingForOptions($n,array('fileNameAsKeys'=>true));;
		return $d;
	}

	public function admin_add() {
		//$this->set('layout_options',$layout_options);
		 $layout_options = $this->getLayouts();
		$this->set('breadcrumbs' , $this->Landingpage->breadcrumbsIndexPage());
		$this->set('layout_options',array('options'=>$layout_options));
		$this->set('banners_options',$this->banners());
		if ($this->request->is('post')) {
			$d = $this->data;
			if(empty($d['Landingpage']['cmsname'])){
				$d['Landingpage']['cmsname'] = $d['Landingpage']['title'];
				$this->data = $d;
			}

			$this->Landingpage->create();
			$saved = $this->Landingpage->save($this->request->data);
			if($saved) {
				$this->Session->setFlash(
					__('The %s has been saved', __('landingpage')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->defaultRedirect = '/my/landingpages/edit/'.$this->Landingpage->getLastInsertId();
				if($this->checkAjax()){
					echo '{"href":"'.$this->defaultRedirect.'"}';
					exit;
				}else{
					$this->redirect($this->defaultRedirect);
				}
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('landingpage')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
	}






	public function admin_edit($id = null) {
		$this->Landingpage->id = $id;
		$this->set('landing_page_id', $id );
		$this->set('cta_target_options',array('_self'=>'_self','_blank'=>'_blank') );
		$this->set('breadcrumbs' , $this->Landingpage->breadcrumbsIndexPage());
		$layout_options = $this->getLayouts();
		$this->set('layout_options',array('options'=>$layout_options));

		$this->tier_types= $this->Landingtier->tier_types;
		$this->paginate=array(
			'conditions'=>array(
				'Landingtier.landingpage_id'=>$id
			),
			'order'=>'Landingtier.order'

		);
		$this->set('landingtiers', $this->Paginator->paginate('Landingtier' ));
		$this->set('tier_types', $this->tier_types);
		// $this->set('landing_page_id', $id);



		if (!$this->Landingpage->exists()) {
			throw new NotFoundException(__('Invalid %s', __('landingpage')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			//debug($this->data); exit;
			//$this->Landingpage->deleteAll(array('landingpage_id'=>$id));
			$q = $this->request->data;
			if(empty($q['Landingpage'])){
				$q['Landingpage'] = array();
			}
			if ($this->Landingpage->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('landingpage')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				//$this->redirect($this->defaultRedirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('landingpage')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}

				$this->defaultRedirect = '/my/landingpages/tiers/'.$id;
				if($this->checkAjax()){
					echo '{"href":"'.$this->defaultRedirect.'"}';
					exit;
				}else{
					$this->redirect($this->defaultRedirect);
				}




		} else {

		}
	 	$this->request->data = $this->Landingpage->read(null, $id);

	// 	//------
	// 	$image_url_options=$this->Fileutil->getFileListingForOptions(IMAGES, array('fileNameAsKeys'=>true));
	// 	$this->set('image_url_options',$image_url_options );
 		//$layout_options = $this->getLayouts();
 		//debug($g); exit;
		//$g['value'] = $this->request->data['Landingpage']['layout'];
		//$this->set('layout_options',$layout_options);
		//$this->set('layout_options',array('options'=>$layout_options));
	// 	$this->set('banners_options',$this->banners());
	}


	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Landingpage->id = $id;
		if (!$this->Landingpage->exists()) {
			throw new NotFoundException(__('Invalid %s', __('landingpage')));
		}
		if ($this->Landingpage->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('landingpage')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->defaultRedirect);
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('landingpage')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->defaultRedirect);
	}


	public function toutrow($id, $lpid = ''){
		$this->handleAjax();
		$this->set('key',$id);
		$this->set('landingpage_id',$lpid);
		$this->set('cta_target_options',array('_self'=>'_self','_blank'=>'_blank') );

		$image_url_options=$this->Fileutil->getFileListingForOptions(IMAGES.'avatars', array('fileNameAsKeys'=>true));
		$this->set('image_url_options',$image_url_options );
	}

}
