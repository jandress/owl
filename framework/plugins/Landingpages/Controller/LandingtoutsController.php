<?php
App::uses('AppController', 'Controller');
/**
 * Landingtouts Controller
 *
 * @property Landingtout $Landingtout
 */
class LandingtoutsController extends AppController {

/**
 *  Layout
 *
 * @var string
 */
//	public $layout = 'frontend';

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator','Quizmodules.Breadcrumbs');


/**
 * Components
 *
 * @var array
 */
	public $components = array('Session','Quizmodules.QAdminbreadcrumb');
	public $uses = array('Landingpages.Landingtout','Landingpages.Landingtier','Landingpages.Landingpage');
/**
 * index method
 *
 * @return void
 */

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('cta_target_options',array('options'=>array('_self'=>'_self','_blank'=>'_blank')) );
	}


	public function admin_index($id = null) {
		$this->set('breadcrumbs' , $this->Landingpage->breadcrumbsIndexPage());
		$this->Landingtier->id = $id;
		$this->data = $this->Landingtier->read();

		$this->Landingtout->recursive = 0;
		if(!empty($id)){
			$this->paginate=array('order'=>'Landingtout.order','conditions' => array(
				'Landingtout.landingtier_id' => $id
			));
			$this->set('landingTouts', $this->paginate());
		}else{
			$this->set('landingTouts', $this->paginate());
		}
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {

		$this->Landingtout->id = $id;
		if (!$this->Landingtout->exists()) {
			throw new NotFoundException(__('Invalid %s', __('landing tout')));
		}
		$this->set('landingtout', $this->Landingtout->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add($a = null, $b = null) {

		if ($this->request->is('post')) {
			$this->Landingtout->create();
			$saved = $this->Landingtout->save($this->request->data);
			if ($saved) {
				$this->Session->setFlash(
					__('The %s has been saved', __('landing tout')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				// debug($saved);
				// exit;
				// $this->redirect(array('action' => 'index'));
			//	$this->defaultRedirect = '/my/landingpages/edit/'.$saved['Landingtout']['landingpage_id'];
				$this->defaultRedirect = '/my/landingpages/tiers/edit/'.$saved['Landingtout']['landingtier_id'];
				if($this->checkAjax()){
					echo '{"href":"'.$this->defaultRedirect.'"}';
					exit;
				}else{
					$this->redirect($this->defaultRedirect);
				}
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('landing tout')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$d = $this->Landingtout->create();
		$this->Landingtier->id = $a;
		$this->Landingtier->recursive = 2;
		$c = $this->Landingtier->read();


		$d['Landingtout']['landingtier_id'] = $a;
		$c['Landingtout'] = $d['Landingtout'];
		$data  = $this->data = $c;

		// Landingtier
		// Landingpage
		
			$bc = $this->Landingpage->breadcrumbsIndexPage();
			$bc[1] = array(
				'label'=>'Edit '.$data['Landingpage']['title'],
				'attributes' => array('href' => '/my/landingpages/edit/'.$data['Landingpage']['id'])
			);
			$bc[2] = array(
				'label'=>'Edit '.$data['Landingtier']['title'],
				'attributes' => array('href' => '/my/landingpages/tiers/edit/'.$data['Landingtier']['id'])
			);
			$this->set('breadcrumbs' , $bc);
		
		
	}



	public function moveup($id){
		$g = $this->Landingtout->find('first',array('conditions'=>array('Landingtout.id'=>$id)));
		$b = $this->Landingtout->moveup($id);
		if(!empty($id)){
			$this->paginate = array('order'=>'Landingtout.order','conditions'=>array('landingtier_id'=>$g['Landingtout']['landingtier_id']));
		}
		$this->set('landingTouts', $this->paginate());
	}
	public function movedown($id = null){
		$g = $this->Landingtout->find('first',array('conditions'=>array('Landingtout.id'=>$id)));
		$b = $this->Landingtout->movedown($id);
		if(!empty($id)){
			$this->paginate = array('order'=>'Landingtout.order','conditions'=>array('landingtier_id'=>$g['Landingtout']['landingtier_id']));
		}
		$this->set('landingTouts', $this->paginate());
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Landingtout->id = $id;
		if (!$this->Landingtout->exists()) {
			throw new NotFoundException(__('Invalid %s', __('landing tout')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->Landingtout->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('landing tout')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
// dsafsadffsdaf
// saved
//go to landing tier page with the tout listing
				$this->Landingtier->contain();
				$r = $this->Landingtier->find('first',array(
					'conditions'=> array(
						'Landingtier.id'=>$this->request->data['Landingtout']['landingtier_id']
					)
					//,'fields'->array()
					
				));

					



				$this->defaultRedirect = '/my/landingpages/tiers/edit/'.$r['Landingtier']['id'];
//				$this->defaultRedirect = '/my/landingpages/touts/'.$this->request->data['Landingtout']['landingtier_id'];
				
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('landing tout')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
				$this->defaultRedirect = '/my/landingpages/touts/'.$this->request->data['Landingtout']['landingtier_id'];
			}
			if($this->checkAjax()){
				echo '{"href":"'.$this->defaultRedirect.'"}';
				exit;
			}else{
				$this->redirect($this->defaultRedirect);
			}


		} else {
			$this->Landingtout->recursive = 3;
			$this->data = $this->request->data = $this->Landingtout->read(null, $id);
			$this->setBreadCrumbs($this->data);			
			
//			debug($this->data); exit;
			


		}


		//debug($this->data); exit;
		//$landingpages = $this->Landingtout->Landingpage->find('list');
		//$this->set(compact('landingpages'));
	}
	

	private function setBreadCrumbs($data){
		$bc = $this->Landingpage->breadcrumbsIndexPage();
		$bc[1] = array(
			'label'=>'Edit '.$data['Landingtier']['Landingpage']['title'],
			'attributes' => array('href' => '/my/landingpages/edit/'.$data['Landingtier']['Landingpage']['id'])
		);
		$bc[2] = array(
			'label'=>'Edit '.$data['Landingtier']['title'],
			'attributes' => array('href' => '/my/landingpages/tiers/edit/'.$data['Landingtier']['id'])
		);
		$this->set('breadcrumbs' , $bc);
	}
	

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Landingtout->id = $id;
		$d = $this->Landingtout->read();
		if (!$this->Landingtout->exists()) {
			throw new NotFoundException(__('Invalid %s', __('landing tout')));
		}
		if ($this->Landingtout->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('landing tout')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect('/my/landingpages/tiers/edit/'.$d['Landingtout']['landingtier_id']);
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('landing tout')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect('/my/landingpages/tiers/edit/'.$d['Landingtout']['landingtier_id']);
	}
}
