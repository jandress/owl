<?php
	App::uses('AppController', 'Controller');


	class LandingtiersController extends AppController {

		public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator','Quizmodules.Breadcrumbs');
		public $components = array('Session','Landingpages.Fileutil','Auth','Quizmodules.QAdminbreadcrumb','Paginator');
		public $uses = array('Landingpages.Landingtier','Landingpages.Landingpage','Landingpages.LandingTout');
		public $tier_types ;
		var $applicationLayoutDirectory;
		var $pluginLayoutDirectory;

		public function beforeFilter(){
			parent::beforeFilter();
			$this->tier_types= $this->Landingtier->tier_types;
			$h = $this->Fileutil->getDirectories('./banners',array('fileNameAsKeys'=>1));
			array_unshift($h,'');
			$this->set('banner_options', array('options'=>$h));
			// $this->Auth->allow('index','view');
			// $this->applicationLayoutDirectory = APP.'View/Layouts/Landingpages/';
			// $this->pluginLayoutDirectory = dirname(__FILE__).'/../View/Layouts/Landingpages/';
		}


		public function admin_view($id = null) {
			$this->layout = 'standalone_styled';
			$this->Landingtier->id = $id;
			$this->data = $this->Landingtier->read();
			// if (!$this->Landingpage->exists()) {
			// 	throw new NotFoundException(__('Invalid %s', __('landingpage')));
			// }
			// $this->data = $d = $this->Landingpage->find('first', array('conditions'=>array('Landingpage.id'=>$id)));
			// $this->set('title_for_layout', $this->data['Landingpage']['h1']);
			// $file_exists = $this->layoutExists($d['Landingpage']['layout']);
			// if(!empty($d['Landingpage']['layout']) && $file_exists){
			// 	$this->layout = 'Landingpages/'.$d['Landingpage']['layout'];
			// }else{
			// 	$this->layout = 'Landingpages/default';
			// }
			// //		$this->layout = 'Landingpages/'.$this->layout;
			// $this->set('landingpage', $d);
		}

		public function admin_index($id=null) {
			if ($this->request->is('post') || $this->request->is('put')) {
					$this->Landingpage->updateAll(array('home'=>0));
					$this->Landingpage->updateAll(array('home'=>1),array('id'=>$this->request->data['Landingpage']['home']));
			}
			$this->Landingpage->id = $id;
			$this->data = $this->Landingpage->read();
			$lp = $this->data['Landingpage'];
			$bc = $this->Landingpage->breadcrumbsIndexPage();
			$bc[1]=array(
				'label'=> $lp['title'],
				'attributes'=> array(
					'href'=>'/my/landingpages/edit/'.$lp['id']
				)
			);
			$this->set('breadcrumbs' , $bc);
			if(!empty($id)){
				$this->paginate = array('order'=>'order','conditions'=>array('landingpage_id'=>$id));
			}
			$this->set('landingtiers', $this->paginate());
			$this->set('tier_types', $this->tier_types);
			$this->set('landing_page_id', $id);
		}
		
		

		public function admin_add($landingpage_id) {
			$this->set('breadcrumbs' , $this->Landingpage->breadcrumbsIndexPage());
			$this->set('tier_types', $this->tier_types);
			if ($this->request->is('post')) {
				$this->Landingtier->create();
				$g = $this->Landingtier->save($this->request->data);
				if ($g) {
					$this->Session->setFlash(
						__('The %s has been saved', __('landingpage')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);
					if( $this->request->data['Landingtier']['type']=='responsive_banner'){
						$this->defaultRedirect = '/my/landingpages/edit/'.$g['Landingtier']['landingpage_id'].'/';
					}else{
						$this->defaultRedirect = '/my/landingpages/touts/add/'.$this->Landingtier->getLastInsertId();
					}
				} else {
					$this->Session->setFlash(
						__('The %s could not be saved. Please, try again.', __('landingpage')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-error'
						)
					);
					$this->defaultRedirect = '/my/landingpages/touts/add/'.$this->Landingtier->getLastInsertId();
				}
				if($this->checkAjax()){
						echo '{"href":"'.$this->defaultRedirect.'"}';
						exit;
					}else{
						$this->redirect($this->defaultRedirect);
					}
			} else {
				$g = $this->Landingtier->create();
				$g['Landingtier']['landingpage_id'] = $landingpage_id;
				$this->Landingpage->id = $landingpage_id;
				$b = $this->Landingpage->read();
				$g['Landingpage'] = $b['Landingpage'];
				$this->data = $g;		
			}
			$bc = $this->Landingpage->breadcrumbsIndexPage();
			$bc[1] = array(
				'label'=>'Edit '.$this->data['Landingpage']['title'],
				'attributes' => array('href' => '/my/landingpages/edit/'.$landingpage_id)
			);
			$this->set('breadcrumbs' , $bc);			
		}




		public function moveup($id = null){
			$this->Landingtier->contain();
			$g = $this->Landingtier->find('first',array('conditions'=>array('id'=>$id)));
			$b = $this->Landingtier->moveup($id);
			if(!empty($id)){
				$this->paginate = array('order'=>'order','conditions'=>array('Landingtier.landingpage_id'=>$g['Landingtier']['landingpage_id']));
			}
			$this->set('landingtiers', $this->paginate());
		}


		public function movedown($id = null){
			$this->Landingtier->contain();
			$g = $this->Landingtier->find('first',array('conditions'=>array('id'=>$id)));
			$b = $this->Landingtier->movedown($id);
			if(!empty($id)){
				$this->paginate = array('order'=>'order','conditions'=>array('Landingtier.landingpage_id'=>$g['Landingtier']['landingpage_id']));
			}
			$this->set('landingtiers', $this->paginate());
		}


		public function admin_edit($id = null) {
			$this->set('tier_types', $this->tier_types);
			if ($this->request->is('post') || $this->request->is('put')) {
//				debug($this->request->data); exit;
				if ($this->Landingtier->save($this->request->data)) {
					$this->Session->setFlash(
						__('The %s has been saved', __('landingpage')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);
					
					
					if( $this->request->data['Landingtier']['type']=='responsive_banner'){
						$this->defaultRedirect = '/my/landingpages/edit/'.$this->request->data['Landingtier']['landingpage_id'].'/';
					}else{
						$this->defaultRedirect = '/my/landingpages/touts/add/'.$this->Landingtier->getLastInsertId();
					}
					
					//$this->defaultRedirect = '/my/landingpages/touts/add/'.$this->Landingtier->getLastInsertId();
//					$this->defaultRedirect = '/my/landingpages/touts/'.$id;
				} else {
					$this->Session->setFlash(
						__('The %s could not be saved. Please, try again.', __('landingpage')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-error'
						)
					);
					$this->defaultRedirect = '/my/landingpages/touts/'.$id;
				}
				if($this->checkAjax()){
						echo '{"href":"'.$this->defaultRedirect.'"}';
						exit;
					}else{
						$this->redirect($this->defaultRedirect);
					}
			} else {
				$this->Landingtier->id = $id;
				$this->data = $this->Landingtier->find('first',array('conditions'=>array('Landingtier.id'=>$id)));
				if(!empty($id)){
					$this->paginate=array('order'=>'Landingtout.order','conditions' => array(
						'Landingtout.landingtier_id' => $id
					));
					$this->set('landingTouts', $this->Paginator->paginate('Landingtout'));
				}
				$bc = $this->Landingpage->breadcrumbsIndexPage();
				$bc[1] = array(
					'label'=>'Edit '.$this->data['Landingpage']['title'],
					'attributes' => array('href' => '/my/landingpages/edit/'.$this->data['Landingpage']['id'])
				);
				$this->set('breadcrumbs' , $bc);
			}
		}


		public function admin_delete($id = null) {
			if (!$this->request->is('post')) {
				//throw new MethodNotAllowedException();
			}
			$this->Landingtier->id = $id;
			$d = $this->Landingtier->read();
			if (!$this->Landingtier->exists()) {
				throw new NotFoundException(__('Invalid %s', __('landingpage')));
			}
			if ($this->Landingtier->delete($id)) {
				$this->Session->setFlash(
					__('The %s was deleted', __('Landingtier')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('/my/landingpages/edit/'.$d['Landingtier']['landingpage_id']);
			}
			$this->Session->setFlash(
				__('The %s was not deleted', __('Landingtier')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-error'
				)
			);
			$this->redirect($this->defaultRedirect);
		}
	}
