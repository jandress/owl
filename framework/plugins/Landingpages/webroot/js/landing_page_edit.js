var landing_page_edit = function() {       
	var function_object = {					
		
		addRowWithRowCount: function(_url,element){
			function_object.addRow(_url+'/key:'+$(element).length);
		},

		addRow: function(_url){
			$.ajax({
				url: _url,
				success: function(data){
					$('#landing_tout_table').append(data);					
					// //console.log(data);
				}
			});
		},
		
		removeRow:function(rowId){
			//console.log("removeRow: "+($('.landing_tout_row').length)+",  "+ ($('#'+rowId)));
			if($('.landing_tout_row').length>=1){
				$('#'+rowId).remove();						
			}else{
				//alert('Fortune Cookie says: Unlike in life, this quiz can have no questions without answers. Just edit the remaining field.');
			}
		}
		
	}
	return function_object;
}; 
var lp_editor = landing_page_edit();
