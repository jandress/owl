<?php
App::uses('AppModel', 'Model');
/**
 * Page Model
 *
 * @property Post $Post
 */
class Pageimpression extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
//    public $useDbConfig = 'owl';
    public $useDbConfig = 'analytics';

	//id
	//ip
	//time
	//username
	//requestobject
	public function recordImpression($user,$url,$ip,$referrer=null){
		$this->create();
		$d = array();
		$d['ip'] = $ip;
		$d['url'] = $url;
		$d['username'] = $user;
		$d['fullurl'] = Router::url( $this->here, true ); 
		$q = array(
			'HTTP_USER_AGENT'=>$_SERVER['HTTP_USER_AGENT'],
			'REQUEST_URI'=>$_SERVER['REQUEST_URI'],			
			'HTTP_HOST'=>$_SERVER['HTTP_HOST'],			
			'REQUEST_TIME'=>$_SERVER['REQUEST_TIME']
		);
		$d['referrer'] = $referrer;
		$d['useragent'] = $_SERVER['HTTP_USER_AGENT'];
		$d['requestobject'] = json_encode($q);
		$date = new DateTime();
		$d['time']=$date->format('Y-m-d H:i:s');		
		$this->save($d);
	}


}


/*
HTTP_USER_AGENT
REQUEST_URI

array(
	'REDIRECT_REDIRECT_STATUS' => '200',
	'REDIRECT_STATUS' => '200',
	'HTTP_HOST' => 'owl',
	'HTTP_DNT' => '1',
	'HTTP_COOKIE' => 'CAKEPHP=6792bc8da2f1ee494c5e5f205a91b166',
	'HTTP_CONNECTION' => 'keep-alive',
	'HTTP_ACCEPT' => 'text/html,application/xhtml+xml,application/xml;q=0.9,;q=0.8',
	'HTTP_USER_AGENT' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.73.11 (KHTML, like Gecko) Version/7.0.1 Safari/537.73.11',
	'HTTP_ACCEPT_LANGUAGE' => 'en-us',
	'HTTP_ACCEPT_ENCODING' => 'gzip, deflate',
	'HTTP_CACHE_CONTROL' => 'max-age=0',
	'PATH' => '/usr/bin:/bin:/usr/sbin:/sbin',
	'SERVER_SIGNATURE' => '',
	'SERVER_SOFTWARE' => 'Apache/2.2.24 (Unix) DAV/2 PHP/5.4.17 mod_ssl/2.2.24 OpenSSL/0.9.8y',
	'SERVER_NAME' => 'owl',
	'SERVER_ADDR' => '127.0.0.1',
	'SERVER_PORT' => '80',
	'REMOTE_ADDR' => '127.0.0.1',
	'DOCUMENT_ROOT' => '/Users/jonandress/Sites/excelsior_owl_eslwow/trunk/sites/owl',
	'SERVER_ADMIN' => 'you@example.com',
	'SCRIPT_FILENAME' => '/Users/jonandress/Sites/excelsior_owl_eslwow/trunk/sites/owl/webroot/index.php',
	'REMOTE_PORT' => '56823',
	'REDIRECT_URL' => '/webroot/sitemap/',
	'GATEWAY_INTERFACE' => 'CGI/1.1',
	'SERVER_PROTOCOL' => 'HTTP/1.1',
	'REQUEST_METHOD' => 'GET',
	'QUERY_STRING' => '',
	'REQUEST_URI' => '/sitemap/',
	'SCRIPT_NAME' => '/webroot/index.php',
	'PHP_SELF' => '/webroot/index.php',
	'REQUEST_TIME_FLOAT' => (float) 1390637172.153,
	'REQUEST_TIME' => (int) 1390637172,
	'argv' => array(),
	'argc' => (int) 0
)
*/