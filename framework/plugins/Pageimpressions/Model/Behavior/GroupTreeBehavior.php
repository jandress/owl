<?php 
App::import('Behavior','Tree'); 
class GroupTreeBehavior extends TreeBehavior {
    function generatetreegrouped(&$Model, $conditions = null, $keyPath = null, $valuePath = null, $groupPath = null, $recursive = null) {
		//unpacking settings and setting recurse level
        $overrideRecursive = $recursive;
        extract($this->settings[$Model->alias]);
        if (!is_null($overrideRecursive)) {
            $recursive = $overrideRecursive;
        }
        //------------------------------------------------------------------------------------------------
		//set default fields if $displayField is set and  $keypath, $valuepath & groupath are all null
        if ($keyPath == null && $valuePath == null && $groupPath == null && $Model->hasField($Model->displayField)) {
            $fields = array($Model->primaryKey, $Model->displayField, 'parent_id', $left, $right);
        } else {
            $fields = null;
        }
		// i dont know what '{n}.' does or why the string '{n}.Page.id' would be useful.
        if ($keyPath == null) {
			//'{n}.Page.id' 
            $keyPath = '{n}.' . $Model->alias . '.' . $Model->primaryKey;
        }

        if ($valuePath == null) {
			//{n}.Page.link_label
            $valuePath = '{n}.' . $Model->alias . '.' . $Model->displayField;
        }
        
        if ($groupPath == null) {
			//{n}.Page.parent_id
            $groupPath = '{n}.' . $Model->alias . '.parent_id';
        }
        //------------------------------------------------------------------------------------------------
		// return results ordered by left
        $order = $Model->alias . '.' . $left . ' asc';
        $results = $Model->find('all', compact('conditions', 'fields', 'order', 'recursive'));
        $stack = array();
        //------------------------------------------------------------------------------------------------

		// loop over all the results and 
        foreach ($results as $i => $result) {
			//$stack gets filled with $right values of the tree structure.
			// this conditional says while:
			// $stack hai and the last $stack[key] is less than the current $result['Page']['right']
			// its basically remove all items from $stack that are less than $result['Page']['right'].
			// making sure that the array is only items that are reater than $result['Page']['right']..
            while ($stack && ($stack[count($stack) - 1] < $result[$Model->alias][$right])) {
                array_pop($stack);
            }
            $stack[] = $result[$Model->alias][$right];
        }
        if (empty($results)) {
            return array();
        }
		//Set::combine($data, $path1 = null, $path2 = null, $groupPath = null)
		// Set::combine Creates an associative array using a $path1 as the path to build its keys, and optionally $path2 as path to get the values. 
		//If $path2 is not specified, all values will be initialized to null (useful for Set::merge). 		
				
		//$keyPath, $valuePath, $groupPath map to keys in the $results array. See below.
		$r = Set::combine($results, $keyPath, $valuePath, $groupPath);
        return $r;
    }	

}
/*


$keyPath = {n}.Page.id;
$valuePath = {n}.Page.link_label;
$groupPath = {n}.Page.parent_id;


$results = 
array(
		[114] => Array
		    (
		        [Page] => Array
		            (
		                [id] => 152
		                [link_label] => Abstract
		                [parent_id] => 22
		                [lft] => 231
		                [rght] => 232
		            )

		    )
		[115] => Array
		    (
		        [Page] => Array
		            (
		                [id] => 153
		                [link_label] => Congratulations!
		                [parent_id] => 22
		                [lft] => 233
		                [rght] => 234
		            )

		    )
)







*/

?>