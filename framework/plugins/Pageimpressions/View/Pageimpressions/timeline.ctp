
<div class="well wb">
	<h1>Analytics</h1>
	<div class="name">
		<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
		
	</div>
</div>
<?php //$this->Js->buffer($this->element('')); 
echo '';
?>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
	$(function () {
	        $('#container').highcharts({
	            title: {
	                text: 'Site Traffic',
	                x: -20 //center
	            },

	            subtitle: {
	                text: '<?php echo $this->Time->nice($dateFrom); ?> - <?php echo $this->Time->nice($dateTo); ?>, <?php echo $numberOfDays; ?> Days',
	                x: -20
	            },
	            xAxis: {
	                categories: [
						<?php
						 foreach ($increments as $key => $value): ?>
							<?php							
echo "'".$this->Time->nice($value)."'"; 
								if($key<count($increments)-1){
									echo ",";
								}
							?>
						<?php endforeach;
						?>
					]
	            },
	            yAxis: {
	                title: {
	                    text: 'Hits'
	                },
	                plotLines: [{
	                    value: 0,
	                    width: 1,
	                    color: '#808080'
	                }]
	            },
	            tooltip: {
	                valueSuffix: ''
	            },
	            legend: {
	                layout: 'vertical',
	                align: 'right',
	                verticalAlign: 'middle',
	                borderWidth: 0
	            },
	            series: [{
	                name: 'Page Hits',
	                data: [
											<?php
											 foreach ($values as $key => $value): ?>
												<?php							
													echo $value; 
													if($key<count($increments)-1){
														echo ",";
													}
												?>
											<?php endforeach;
											?>
					]
	            }
				]
	        });
	    });

</script>