
<div class="well wb">
	<h1>Analytics</h1>
	<hr/>
	<?php
	echo $this->element('analytics/timeline_chart');
	?>
	<hr/>	
	<table class="table table-striped table-bordered table-hover">
		<tr>				
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
			<?php foreach ($table_content as $key => $value): ?>
		<tr>				
				<td>
					<?php if (empty($value['url'])): ?>
						<b><?php echo $value['label']; ?></b> 							
					<?php else: ?>
					<b><a href = "<?php echo $value['url']; ?>"><?php echo $value['label']; ?></a></b> 							
					<?php endif ?>
				</td>
				<td>
					<?php echo $value['value']; ?>
				</td>
		</tr>				
			<?php endforeach ?>
	</table>
</div>