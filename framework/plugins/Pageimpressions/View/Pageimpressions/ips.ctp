<div class="well wb">
	<?php echo $this->BootstrapForm->create('Landingpage', array('class' => ''));?>
	<div class="row-fluid">
			<h2><?php echo __( __('IPs Visiting'));?></h2>
			<div class="navbar">
				<div class="navbar-inner">
					<div class='span9' style = 'padding-top:8px'><p><?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></p></div>
					<div class="nav span3 pull-right">

					</div>				
				</div>
			</div>
		<table class="table table-striped table-bordered table-hover" style='font: 75% "Lucida Grande", "Trebuchet MS", Verdana, sans-serif;'>
				<tr>				
					<th>
						<?php echo $this->BootstrapPaginator->sort('ip');?>
													|	
						<?php echo $this->BootstrapPaginator->sort('username');?>						
					</th>											
					<th>
						<?php echo $this->BootstrapPaginator->sort('useragent');?>


					</th>				
					<th>
						<?php echo $this->BootstrapPaginator->sort('referrer');?>											
					</th>				
					<th><?php echo $this->BootstrapPaginator->sort('time');?></th>		
				</tr>
			<?php 
			foreach ($data as $page):
				 ?>
				<tr>
					<td>
						<a href = "/pageimpressions/ip/<?php echo h($page['Pageimpression']['ip']); ?>"><?php echo h($page['Pageimpression']['ip']); ?>&nbsp;</a>  |	
						<?php echo h($page['Pageimpression']['username']); ?>												
					</td>						
					<td>
						<?php echo ($page['Pageimpression']['useragent']); ?>
					</td>				
					
					<td>
						<?php echo h($page['Pageimpression']['referrer']); ?>						
					</td>
					<td><?php echo $this->Time->nice($page['Pageimpression']['time']); ?>&nbsp;</td>
				</tr>
			<?php endforeach; ?>
			</table>
			<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
	<?php echo $this->BootstrapForm->end();?>	
</div>