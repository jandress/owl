<?php

	class FlatlistBehavior extends ModelBehavior {


		public function moveup($model,$id){
			$siblings = $this->getSiblings($model,$id);
			// //look in the array for this item id, get it's order number and the previous item's order number, swap them.
			// check to see if they are the same, if they are, then increment the previous one.  set the values.
			$prev = 0;
			$swapped = false;
			$currentOrder = 0;
			foreach ($siblings as $key => $value) {
				$currentOrder = $value[$model->alias]['order'];
				if($value[$model->alias]['id']==$id){
					$activeVal = $value[$model->alias]['order'];
					$prevval = $siblings[$prev][$model->alias]['order'];
					if($activeVal==$prevval){
						$prevval =$prevval+1;
					}
					$siblings[$prev][$model->alias]['order'] = $activeVal;
					$siblings[$key][$model->alias]['order'] = $prevval;
					$swapped = true;
					$prev = $key;
					continue;
				}
				$prev = $key;
			}
			return $model->saveAll($siblings);
		}


		public function movedown($model,$id){
			$siblings = $this->getSiblings($model,$id);
			$activeKey = 0;
			$isNext = false;
			foreach ($siblings as $key => $value) {
				if($value[$model->alias]['id']==$id){
					//$value['Landingtier']['id'] is the current one. mark the key and the value, and set the flag to perform
					//operations on the next item.
					$activeVal = $value[$model->alias]['order'];
					$activeKey = $key;
					$isNext = true;
					continue;
				}
				if($isNext){
					// is the next one, perform the swap of their order vlues in the data array
					$isNext = false; //prevent it from happening to the rest.
					$nextVal = $value[$model->alias]['order'];
					$siblings[$activeKey][$model->alias]['order'] = $nextVal;
					if($activeVal == $nextVal){
						$activeVal = $activeVal+1;
					}
					$siblings[$key][$model->alias]['order'] = $activeVal;
					$swapped = true;
					continue;
				}
			}
			return $model->saveAll($siblings);
		}

		private function getSiblings($model,$id){
			// get this record,
			//then get it's first belongsTo association
			// and get those associated records via it's foreignKey (define in the belongsTo array in the model)
			$model->contain();
			$g = $model->find('first',array('conditions'=>array('id'=>$id)));
			if(!empty($model->belongsTo)){
				$first_key = reset($model->belongsTo); // gets the first item in the belongsTo array
				$foreign_key = $first_key['foreignKey']; //
				$foreign_key_value = $g[$model->alias][$foreign_key];
				$model->contain(); // not sure why you did this.
				// conditions to get the siblings of this record
				$conditions = array( 'order'=>'order','conditions'=>array($foreign_key=>$g[$model->alias][$foreign_key]));
				$siblings = $model->find('all',$conditions);				
			}else{
				$conditions = array( 'order'=>'order');
				$siblings = $model->find('all',$conditions);
			}
			// clean up any bad 'order' values.
			return $this->serialize($siblings,$model->alias);
		}



		private function serialize($siblings,$alias){
			$ind = 0;
			foreach ($siblings as $key => $value) {
				$ind = $ind+1;
				$siblings[$key][$alias]['order'] = $ind;
			}
			return $siblings;
		}


	}
