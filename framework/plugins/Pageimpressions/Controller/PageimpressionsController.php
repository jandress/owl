<?php

	App::uses('AppController', 'Controller');
 	App::uses('CakeTime', 'Utility');
	class PageimpressionsController extends AppController {

		public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator','Time');
		public $components = array('Session');
		public $uses = array('Pageimpressions.Pageimpression','Setting','Navlink');
		public $paginate = array(
			'limit'=> 100,					
			 'maxLimit' => 200
		);





		public function index(){
		///	debug(CakeTime::nice(strtotime('today midnight')));exit;
			$this->setDashboardLayout();
			$total = $this->Pageimpression->find('count');
			$totaltoday = $this->Pageimpression->find('count',
									array(
											'conditions'=>array( 
												'Pageimpression.time >'=>CakeTime::format('today midnight', "%Y-%m-%d %H:%M:%S")
												)
										)
														);				
			$uniqueVisitorsToday = $this->Pageimpression->find('count',
				array(
						'fields'=>'DISTINCT ip',
						'conditions'=>array(
							'Pageimpression.time >'=>CakeTime::format('today midnight', "%Y-%m-%d %H:%M:%S")
						)
					)
				);
					
				$totalUniqueVisitors = $this->Pageimpression->find('all', array(
				    'fields'=>'DISTINCT ip'
				));
				
				$table_content = array(
					array(
						'label'=>'Total Hits to Date',
						'value'=>$total,
						'url'=>'/my/pageimpressions/listing'
					),
					array(
						'label'=>'Total Hits Today',
						'value'=>$totaltoday,
						'url'=>'/my/pageimpressions/today'						
					),
					array(						
						'label'=>'Total Unique ips',
						'value'=>count($totalUniqueVisitors),
						'url'=>'/my/pageimpressions/ips'												
					),
					array(						
						'label'=>'Unique ips today',
						'url'=>'/my/pageimpressions/ips/today',																		
						'value'=>$uniqueVisitorsToday
					)
				);
			$this->set(compact('table_content'));
			$this->timeline();
		}
		
		
		public function my_timeline(){}
		public function timeline(){
			$t = $this->Pageimpression->find('first',
				array(
					'order' => array( 'time' => 'asc'),
					'fields'=>array('time')
				)										
			);	
			$dateFrom =strtotime($t['Pageimpression']['time']);
			$dateTo  = (strtotime("now"));
			$datediff = $dateTo - $dateFrom;
		    $oneHour = 60*60;
			$numberOfDays = floor($datediff/(60*60*24));
			$numberOfHours = $numberOfDays*24;
			$increment = floor($numberOfHours/10)*$oneHour;
			$increments = array();
			$t = $dateFrom;
			for ($i=0; $i <= 10; $i++) { 
				array_push($increments, $t);
				$t += $increment;
			}
			$values =array();
			for ($i=1; $i < count($increments); $i++) {
				$frm = CakeTime::format($increments[$i-1], "%Y-%m-%d %H:%M:%S");
				$to = CakeTime::format($increments[$i], "%Y-%m-%d %H:%M:%S");
				$val = $this->Pageimpression->find('count',
					array(
							'conditions'=>array(
								'Pageimpression.time >'=>$frm,
								'Pageimpression.time <'=>$to,								
							)
						)
					);	
					array_push($values, $val);	
			}			
			$this->set(compact(array('dateTo','dateFrom','numberOfDays','numberOfHours','increments','values')));
		}
			
		public function listing(){
			$this->my_listing();
			$this->handleAjax();			
		}
		public function my_listing(){
				$this->paginate = array(
					'conditions'=> array(),
					'limit'=> 100,					
					 'maxLimit' => 200
				);
			$d = $this->paginate();
			$this->set('data', $d);
			$this->setDashboardLayout();
			$this->handleAjax();			
		}
		public function today(){
			$this->my_today();
		}
		public function my_today(){
			$this->paginate = array(
				'conditions'=>array( 
					'Pageimpression.time >'=>CakeTime::format('today midnight', "%Y-%m-%d %H:%M:%S")
				),
				'limit'=> 100,					
				'maxLimit' => 200
			);
			$d = $this->paginate();
			$this->set('data', $d);
			$this->setDashboardLayout();
		}
		
		public function ip($ip) {
			$this->paginate = array(
				'conditions'=> array(
					'Pageimpression.ip' => $ip
				),
				'order' => array(
					'id' => 'asc'
				),				
				'limit'=> 100,					
				'maxLimit' => 200
			);
			$data = $this->paginate();
			$this->set(compact(array('data','ip')));
			$this->setDashboardLayout();
		}
		public function my_ips() {}
		
		public function ips($beginningDate=null,$endingDate=null) {
			if (!empty($beginningDate)) {
				if (!empty($endingDate)) {
					//put a range here.
					$this->paginate = array(
						'order' => array( 'time' => 'desc','ip' => 'asc'),
						array(
								'group' => array('Pageimpression.ip'),
								'conditions' => array( 
									'Pageimpression.time >' => $beginningDate,
									'Pageimpression.time <' => $endingDate,
									)
							)				
							,
							'limit'=> 200,					
							 'maxLimit' => 200							
					);
				}else{
					// one day only.
					$this->paginate = array(
						'order' => array(
							'id' => 'asc'
						),
						'limit'=> 200,					
						 'maxLimit' => 200						
					);					
				}				
			}else{
				$this->paginate = array(
					'fields'=>array(
						'ip','fullurl','useragent','username','time','referrer'
					),
					'group' => array('Pageimpression.ip'),
					'order' => array(
						'id' => 'asc'
					),
					'limit'=> 200,					
					 'maxLimit' => 200									
				);	
			}
			$d = $this->paginate();
			$this->set('data', $d);
			$this->setDashboardLayout();
		}


	}