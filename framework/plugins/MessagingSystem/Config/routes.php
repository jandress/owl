<?php
	Router::connect('/my/messages/', array('plugin'=>'MessagingSystem','controller' => 'MessagingSystem', 'action' => 'index')); 
	Router::connect('/my/messages/saved/*', array('plugin'=>'MessagingSystem','controller' => 'MessagingSystem', 'action' => 'saved')); 
	Router::connect('/my/messages/inbox/*', array('plugin'=>'MessagingSystem','controller' => 'MessagingSystem', 'action' => 'inbox')); 
	Router::connect('/my/messages/sentlist/*', array('plugin'=>'MessagingSystem','controller' => 'MessagingSystem', 'action' => 'sentlist'));
	Router::connect('/my/messages/sentlist/*', array('plugin'=>'MessagingSystem','controller' => 'MessagingSystem', 'action' => 'sentlist'));
	Router::connect('/my/messages/write/*', array('plugin'=>'MessagingSystem','controller' => 'MessagingSystem', 'action' => 'write'));
	Router::connect('/my/messages/read/*', array('plugin'=>'MessagingSystem','controller' => 'MessagingSystem', 'action' => 'read'));
	Router::connect('/my/messages/reply/*', array('plugin'=>'MessagingSystem','controller' => 'MessagingSystem', 'action' => 'reply'));
	Router::connect('/my/messages/delete/*', array('plugin'=>'MessagingSystem','controller' => 'MessagingSystem', 'action' => 'delete'));
?>