<?php

class MessagingSystemAppController extends AppController {

	var $components = array('RequestHandler','Session','Auth','Acl');	

	
	public function beforeFilter(){
		parent::beforeFilter();
		$this->setDashboardLayout();								
		$this->set('controller',"MessagingSystem");
		$this->set('model',"Message");		
		if ($this->request->is('ajax')) {
		    $this->disableCache();
			$this->layout = "ajax";
			$this->set('is_ajax',true);					
		}else{
			$this->set('is_ajax',false);								
		}		
	}

}

