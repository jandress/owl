<?php

class MessagingSystemController extends MessagingSystemAppController {

	var $name = 'Messages';
	var $helpers = array("Form","Html","Js" ,"Session", 'Paginator' => array('className' => 'TwitterBootstrap.BootstrapPaginator'));
	var $uses = array('User','MessagingSystem.Message');
	var $paginationLimit = 10;

	public $paginate = array(
        'limit' => 5
		,'tag' => 'li'
		,'update'=>'#ajax_container'
		,'evalScripts' => true
    );

	public function index(){
		$this->inbox();
	}

	public function inbox(){
		//  pr($this->current_user);
		$reciver_id = $this->current_user['User']['id'];
		$paginate = array();
		$this->paginate['conditions'] = array('rec_id' =>$reciver_id,'read'=>false);
		$this->data=$this->paginate('Message');
		// debug($this->current_user);
		// debug($reciver_id);
		// debug($this->data);
		//pr($this->Message->find('all'));
		//$this->Message->find('all',array('conditions'=>array('rec_id' =>$reciver_id) ));
		$new = $this->Message->find('count',array('conditions'=>array('rec_id' =>$reciver_id,'read'=>false)));
		if($new==0){
			$titleString = "No New Messages";
		}
		if($new==1){
			$titleString = "1 New Message";
		}
		if($new > 1){
			$titleString = $new." New Messages";
		}
		$this->title($titleString);
		$this->set('data',$this->data);

		$this->set('controller','messages');
		$this->set('model','Message');
		$this->set('displayField','title');
	}


	public function write(){
		$this->title('Message From');
		$this->data = array(
			'Message'=> array(
				'subject'=> '',
	            'sender_id' => $this->current_user['User']['id'],
	            'rec_id' => 1
			),
			'User'=>$this->current_user
		);
	}

	public function sentlist(){
		$reciver_id = $this->current_user['User']['id'];
		$paginate = array();
		$this->paginate['conditions'] = array('sender_id' =>$reciver_id,);
		$this->data=$this->paginate('Message');
		$this->set('data',$this->data);
		//
		$this->set('controller','messages');
		$this->set('model','Message');
		$this->set('displayField','title');
	}

	public function saved(){
		$reciver_id = $this->current_user['User']['id'];

		$paginate = array();
		$this->paginate['conditions'] = array('rec_id' =>$reciver_id,'read'=>1);
		$this->data=$this->paginate('Message');
		$this->set('data',$this->data);
		//
		$this->set('controller','messages');
		$this->set('model','Message');
		$this->set('displayField','title');
	}

	public function read($index){
		$this->Message->id = $index;
		$this->data = $this->Message->read();
		$g = $this->data;
		$g['Message']['read'] = true;
		$this->Message->save($g);
		$this->title($this->data['Message']['subject']);
		//~enhancement -> maybe create a helper AppController to add inline js to an array that can then be
		// printed via $this->script->printInline('identifier')?
		$this->set('script_element','js/message_reply');
		$this->set('message',$this->data['Message']);
		$this->set('from',$this->data['User']);
	}

	public function reply($index){

		$this->Message->id = $index;
		$d = $this->Message->read();
		$this->data = array(
			'Message'=> array(
				'subject'=> 'Re: '.$d['Message']['subject'],
	            'sender_id' => $this->current_user['User']['id'],
	            'rec_id' => $d['Message']['sender_id']
			),
			'User'=>$this->current_user['User']
		);
		$this->Message->User = $this->current_user['User'];
		$this->set('replyingTo',$d);
	}



	public function delete($id) {
		$this->Message->id = $id;
		$data = $this->Message->read();
		// pr($data);
		if(!empty($data)) {
			if($this->Message->delete($data['Message'])){
				$this->Session->setFlash('Message Deleted','alert');
				$this->redirect('/my/messages/');
			}
		}
	}
	public function submit() {
///		$this->set('script_element','js/message_submit');
//		pr($this->data); exit;
		if(!empty($this->data)) {
			if($this->Message->Create($this->data) && $this->Message->validates()) {
				if($this->Message->save($this->data)){
					$this->Session->setFlash('Message Saved','session_flash');
				}
				$this->redirect('/my/messages/');
				return;
			} else {
				$this->Session->setFlash(__('validation error'));
			}
		}else{
			echo('data is empty ');
		}
		$this->title("Edit Message: <span class = 'error-message' >Validation Error</span>");

	}





























}
