<div id="ajax_container_2" class="">
	<?php echo($this->element('messaging_top_nav')); ?>
	<div class="well">
		<div class="white_well">
			<div class="ajax_links pull-right ">
			  	<a class="btn" href = "/my/messages/reply/<?php echo $message['id']; ?>/" ><i class="icon-share-alt"></i>&nbsp;&nbsp;Reply</a>
			</div>
			<h1><?php echo $title_for_layout?></h1>
			<div id="#reply_content">
				<h3><span class = "dark_gray">From:</span> <?php echo ucfirst($from['first_name']).'  '.ucfirst($from['last_name']) ;?></h3>
				<h4><span class = "dark_gray">Sent On:</span> <?php echo $message['date']?></h4>
				<hr/>
				<p class="tm_2em"><?php echo $message['message'];?></p>
			</div>
		</div>
	</div>
</div>
