<div class="well">
	<div class="white_well">
			<h1>New Message</h1>
			<h3>Subject</h3>
			<?php
			 	echo($this->Form->create(array('action'=>'./submit/')));
				echo($this->Form->hidden('Message.id'));
				echo($this->Form->hidden('Message.sender_id'));			
				echo($this->Form->hidden('Message.rec_id'));						
				echo($this->Form->input('Message.subject',array('label'=>'','class'=>'span12')));
			?>                
			<h3>Message</h3>
			<?php
				echo($this->Form->input('Message.message',array('label'=>'','class'=>'span12')));
			?>	
		    <?php echo $this->Form->submit('Send', array(
		        'div' => false,
		        'class' => 'btn btn-large btn-primary pull-right tm_1px span3',
		    )); ?>
		<div class = "clearfix">&nbsp;</div>	
	</div>
</div>