<? $reorderable = isset($reorderable) ? $reorderable : false; ?>
<table class="table table-striped table-bordered table-hover">
	<thead id="cart_header">
		<tr>
			<th>&nbsp;</th>
			<th class = "">
				Name
			</th>

			<th class = "">
				Subject
			</th>
			<th class = "span2">
				Date
			</th>
			<th class = "span1">
				&nbsp;
			</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if (!empty($data)){
	 		foreach ($data as $key => $value):
				echo $this->element('message_table_row',array('reorderable'=>$reorderable,'value'=>$value));
			endforeach;
		}else{
			for ($i=0; $i < 5; $i++) {
				echo('<th>&nbsp;</th>');
			}
		} ?>
  	</tbody>
</table>
<?php
if(count($data)){
	echo $this->Paginator->pagination(array('div'=>'pagination ajax_links'));
	//$this->Js->buffer("");
}
?>
