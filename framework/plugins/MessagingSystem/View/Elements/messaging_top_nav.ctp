<?php if (empty($is_ajax)): ?>
	<div class="navbar bm_5px">
		<div class="navbar-inner">
			  <div class="btn-group " >
			    <a class="btn active pull-left ajax_link_2" href = '/my/messages/inbox/' ><i class="icon-inbox"></i>&nbsp;&nbsp;Unread Messages</a>
			    <a class="btn pull-left ajax_link_2" href = '/my/messages/saved/' ><i class="icon-folder-open"></i>&nbsp;&nbsp;Read Messages</a>
			    <a class="btn pull-left ajax_link_2" href = '/my/messages/sentlist/'><i class="icon-share-alt"></i>&nbsp;&nbsp;Sent Messages</a>
				<a class="btn pull-right ajax_link_2" href = '/my/messages/write/'><i class="icon-pencil"></i>&nbsp;&nbsp;Write Administrators!</a>
			  </div>
			<div class = "clearfix">&nbsp;</div>
		</div>
	</div>
<?php endif ?>
