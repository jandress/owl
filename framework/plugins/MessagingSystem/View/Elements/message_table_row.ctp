<?php
	if($value['Message']['read']){
		$read = " read ";
	}else{
		$read = "  ";
	}
	$vSpacing = 'tm_2em';
	$id = $value['Message']['id'];
?>
<tr class="altrow ajax_links">
	<td>
		&nbsp;
	</td>
	<td>
		<?php
			$user = $value['User'];
			echo ucfirst($user['first_name']).'  '.ucfirst($user['last_name']);
		?>
	</td>
	<td>
		<?php
			$max = 100;
			echo '<strong class ="row_subject">'.$value['Message']['subject'].'</strong><br/>';
			$string = '<span class ="row_excerpt">'.$value['Message']['message'].'</span>';
			if(strlen($string)<100){
				echo $string;
			}else{
				echo substr($string,0,strpos($string," ",$max)).'...';
			}
			echo('<em class ="blue continued_link"><br/><a class="ajax_link_2" href="/my/messages/read/'.$id.'/">(Continued)</a></em>');
		?>
	</td>
	<td>
		<?php echo $value['Message']['date']; ?>
	</td>
	<td>
		<div class="h_centered_content">
			<a  class="btn btn-primary btn bm_3px ajax_link_2" href="/my/messages/read/<?php echo $id; ?>/"><i class="icon-white icon-folder-open"></i></a>
			<a onclick= "return confirm('Are you sure you want to delete this message? You can\'t undo that.'); " class="btn btn-danger btn ajax_link_2" href="/my/messages/delete/<?php echo $id; ?>/"><i class="icon-white icon-trash"></i></a>
		</div>
	</td>
</tr>
