<?php

App::uses('MessagingSystemAppModel', 'MessagingSystem.Model');
App::uses('CakeEmail', 'Network/Email');

class Message extends MessagingSystemAppModel {

	var $welcomeSubject="Welcome to Konark Market!";
	var $welcomeFrom = "Jon Andress, Konark Market";
	var $welcomeMessage = "Welcome to KonarkMarket! You’ve just joined a community of hundreds of thousands of bookworms who are finding new inspiration daily! At KonarkMarket, you can find books - from the popular to the unique, top publishers to independent authors, all curated just for you. 
Talk to us and each other! Come in, browse us, share us and be a part of the digital revolution! Leave comments and suggestions, and share your story!
We are thrilled that you have joined us! Congratulations! Welcome to the family!";
	
	var $useDbConfig = 'konark_messaging';	
	public $belongsTo = array(
	      'User' => array(
	          'className'    => 'User',
	          'foreignKey'   => 'sender_id'
	      )
	  );


	public function sendWelcomeMessage($userId) {
			$this->data = array(
				'Message'=> array(
					'subject'=> $this->welcomeSubject,
		            'sender_id' => 1,
		            'rec_id' => $userId,
					'message'=>$this->welcomeMessage
				),
				//'User'=>$this->current_user
			);	
			$this->save($this->data);
	}
	
	public function sendPublisherWelcomeMessage($userId){
			$this->data = array(
				'Message'=> array(
					'subject'=> 'Welcome Publisher!',
		            'sender_id' => 1,
		            'rec_id' => $userId,
					'message'=>$this->welcomeMessage
				),
				//'User'=>$this->current_user
			);	
			$this->save($this->data);
	}
	
	
	public function sendPurchaseMessage($user){
			$purchaseSubject = "Thanks for shopping with Konark Market!";
			$purchaseMessage = "Thanks for shopping with Konark Market! We will email you soon with the link to download your pdf.";
			$adminPurchaseMessage = "Thanks for shopping with Konark Market! We will email you soon with the link to download your pdf.";						
			$this->data = array(
				'Message'=> array(
					'subject'=> $purchaseSubject,
		            'sender_id' => 1,
		            'rec_id' => $user['id'],
					'message'=>$purchaseMessage
				),
				//'User'=>$this->current_user
			);
			$this->save($this->data);			
			$this->sendEmail($user['email_address'], $purchaseSubject, $adminPurchaseMessage);
			$this->sendEmail('jon@konarkpublishers.com','konarkMarket purchase made',$adminPurchaseMessage);			
	}
	



	public function sendEmail($to,$subject,$message){
		//~security:  remove log statements from Tickets		
		$email = new CakeEmail();
		$email->from(array('noreply@konarkmarket.com' => 'Konark Market '));
		$email->to($to);
		$email->subject($subject);
		$email->send($message);
		error_log('New Ticket Email To: '.$to.' Subject: '."\n".$subject.' Message: '.$message);
	}
	
	
	
}

/*

public $validate = array(
	'sender_id' => array(
		'numeric' => array(
			'rule' => array('numeric'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
	),
	'rec_id' => array(
		'numeric' => array(
			'rule' => array('numeric'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
	),
	'read' => array(
		'boolean' => array(
			'rule' => array('boolean'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
	),
	'subject' => array(
		'notempty' => array(
			'rule' => array('notempty'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
	),
);

*/