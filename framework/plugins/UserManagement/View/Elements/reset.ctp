<h4>Enter Your new Password</h4>		
<?php
	echo($this->Form->create());
?>
<div class="well ">
	<div class="h_centered_content">
		<?php                                                                
			$controller = "users";
			echo($this->Form->hidden('User.id'));				
		    echo($this->Form->input('password',array('type'=>'password','label'=>'','placeholder'=>'Password','class'=>'span3')));
		    echo($this->Form->input('confirmPassword',array('type'=>'password','label'=>'','placeholder'=>'Confirm Password','class'=>'span3')));			
			echo $this->Session->flash('auth');
			echo $this->Session->flash();
		?>		
	</div>
</div>
<?php
	echo $this->Form->submit('Send', array(
		'div'=>'padding_sides_50',
		'class' => 'btn btn-large btn-block btn-primary',
	)); 
?>
<?php echo($this->Form->end());?>
<div class = "clearfix">&nbsp;</div>