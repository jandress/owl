<div class="control-group">
	<div class="input-prepend input-append">
		<a class="btn add-on hasTooltip" data-placement="left" data-original-title="User Name" >
			<i class="icon-user"></i>
		</a>
		<?php echo($this->Form->input('username',array('label'=>'','div'=>null,'placeholder' => "User Name", 'class'=>'standalone_login_module_field'))); ?>
		<a data-original-title="Forgot your username?" data-placement="right" href="" class="add-on hasTooltip btn" title="Forgot your username?">
			<i data-placement="left" class="icon-question-sign" data-original-title="User Name" title="Forgot your username?">&nbsp;</i>						
		</a>										
	</div>
</div>