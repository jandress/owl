<h4 class = "bm_0">Forgot Your Password?</h4>		
<div class="alert alert-info tm_1em">
	Simply enter your username or email address, enter the verification code you see on the next page, and we will email instructions to you on how to reset your password.
</div>

<?php echo($this->Form->create('UserManagement.User',array('class'=>'form-inline'))); ?>		
<div class="well grey_background">
	<?php echo $this->element('username_field'); ?>		
</div><!-- well grey_background -->
<?php
	echo $this->Form->submit('Send Request', array(
		'div' => array('class'=>'padding_right_50 padding_left_50	'),
		'class' => 'btn btn-primary btn-block btn-large',
	)); 
	echo($this->Form->end());
?>			
<div class = "clearfix">&nbsp;</div>		

<?php $this->Js->buffer("console.log($('.hasTooltip').tooltip({trigger:'hover', html:'false' }));"); ?>