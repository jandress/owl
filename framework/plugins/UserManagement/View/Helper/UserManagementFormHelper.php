<?php 

	App::uses('AppHelper', 'View/Helper','Helper', 'View');
	
	class UserManagementFormHelper extends Helper {

		public $helpers = array('TwitterBootstrap.BootstrapForm');
		
		public function createForm($formName,$url=null){
			if(empty($url)){
				return $this->BootstrapForm->create($formName, array('class' => 'form-horizontal'));				
			}else{
				return $this->BootstrapForm->create($formName, array('class' => 'form-horizontal','url'=>$url));				
			}
		}
		
		public function printFields($fields){
			$str = '';
			foreach ($fields as $key => $v) {
				$str.= $this->BootstrapForm->input($key,$v);
			}	
			return $str;
		}
		
		public function printFieldsSimple($fields){
			$ar = array();
			foreach ($fields as $key => $v) {
				$ar[$v] = array();
			}	
			return $this->printFields($ar);
		}
	
		public function printSubmit($class = 'btn btn-primary btn-large pull-right offset2'){
			return $this->BootstrapForm->submit('Submit', array(
		        'div' => false,
		        'class' => $class,
		    ));
		}
		
		public function printAlert($level,$strong,$normal=''){
			return '<div class="alert alert-'.$level.'">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>'.$strong.'</strong> '.$normal.'
			</div>';
		}
		
		public function printSavedAlert($saved, $asset){
			if($saved){
				return $this->printAlert('success',$asset.' Saved');
			}else{
				return $this->printAlert('error',$asset.' Not Saved');				
			}
		}
				
		
	}
?>