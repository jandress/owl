<?php debug("here") ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $title_for_layout; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- fav and touch icons -->
	<?php echo($this->AssetCompress->css("owl.css", array('raw' => $concat_assets))."\n"); ?>
  </head>

  <body>
    <div class="container main-wrapper">
	<?php echo($this->element('navigation/top_nav')); ?>
      <!-- Main hero unit for a primary marketing message or call to action -->


	<div class="full_bleed ">
      <div class="row-fluid">
        <div class="span12 well">
			<?php
				echo $content_for_layout;
			?>
        </div><!--/span-->
      </div><!--/row-->
	</div>
		<?php echo $this->element('global/footer');?>
    </div> <!-- /container -->
	<?php
		// javascript and css combination sets are definied in /config/asset_compress.ini
		// and set by AppController->setCombinedPageScript() & AppController->setCombinedPageScript();
		// echo($this->AssetCompress->script($page_script, array('raw' => $concat_assets))."\n");
		// echo("\n");
		//  		echo($this->element('global/modal'));
 ?>

  </body>
</html>
