<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $title_for_layout; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	<?php echo($this->AssetCompress->css($page_style, array('raw' => $concat_assets))."\n"); ?>
  </head>
  <body>
    <div class="fluid-container">
	
	
		<div class="tm_2em h_centered_content">
			<div style = "width:350px" class="standalone-unit white_background standalone_login_module tm_1em h_centered">
				<a alt = 'home' href = "/">
					<div class="logo h_centered hasTooltip" data-placement="right" data-original-title="Password">&nbsp;</div>
				</a>
				<?php echo $content_for_layout; ?>						
			</div>		
		</div>	
    </div><!--/.fluid-container-->
	<?php
		echo($this->AssetCompress->script($page_script, array('raw' => $concat_assets))."\n");			  				    									  				   
		echo $this->Js->writeBuffer();				
	?>
  </body>
</html>
