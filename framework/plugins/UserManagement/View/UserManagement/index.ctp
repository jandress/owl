<div class="">





	<div class="navbar-inverse">
		<div class = 'pull-left'>
			<h2 ><?php echo __( __('Users'));?></h2>
			<p><?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')));?></p>
		</div>
		<div class = ''>

			<div class="pull-right col-lg-6 tm_2_em">



				<?php echo $this->Form->create('User', array('url' => '/my/users/search/')); ?>
				<div class="input-group input-group-sm col-lg-6  pull-right">
					<input name="data[User][q]" class="form-control bm_1_px search-query" placeholder="Search" type="text" id="UserQ" aria-describedby="sizing-addon3">
					<span class="input-group-btn">
							<input class="btn btn-default" type="button" type="submit" value="Search">
					</span>
				</div>
				<?php echo $this->Form->end(); ?>



						<ul style="list-style:none; padding:0;" class = "pull-right rm_1_em">
							    <li class="dropdown">
						       <a data-toggle="dropdown" class="dropdown-toggle btn btn-sm btn-default" role="button" id="drop3" href="#">Actions
						        	<b class="caret"></b>
						        </a>

						        <ul aria-labelledby="drop3" role="menu" class="dropdown-menu">
						            <li role="presentation">
										<a class = "" href = "/my/users/add">New User</a>
									</li>

						            <li role="presentation">
										<a  class = "" href = "/my/profile">My Profile</a>
									</li>
						        </ul>
							        </li>
							</ul>

			</div>
		</div>
		<div class = 'clear'></div>
	</div>




	<div class = "admin_padding">
		<table class="table table-striped table-bordered table-hover">
		<tr>
			<th style = "text-align:center"><?php echo $this->BootstrapPaginator->sort('id');?></th>

			<th><?php echo $this->BootstrapPaginator->sort('last_name','Real Name');?></th>
			<th><?php echo $this->BootstrapPaginator->sort('email_address');?></th>
			<th><?php echo $this->BootstrapPaginator->sort('username');?></th>
			<th><?php echo $this->BootstrapPaginator->sort('group');?></th>
			<th class="actions"  style = "text-align:center"><?php echo __('Actions');?></th>
		</tr>
	<?php foreach ($users as $user): ?>
		<tr>
			<td style = "text-align:center"><?php echo h($user['User']['id']); ?>&nbsp;</td>

			<td><?php echo h($user['User']['last_name']); ?>, <?php echo h($user['User']['first_name']); ?>&nbsp;</td>
			<td><?php
			 echo h($user['User']['email_address']);

			?>&nbsp;</td>
			<td><?php echo h($user['User']['username']); ?>&nbsp;</td>


			<td><?php


			 echo h($user['Group']['name']); ?>&nbsp;</td>


			<td class="actions" style = "text-align:center">
				<a class =""  href="/my/users/edit/<?php echo $user['User']['id']; ?>">Edit</a><span> | </span>
				<?php

				 if ($userId ==$user['User']['id']): ?>
					<b class = "gt_20">
						Delete
					</b>
				<?php else: ?>
				<?php echo $this->Form->postLink(__('Delete'), array('plugin'=>'','action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', 		$user['User']['id'])); ?>
				<?php endif ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>

	</div>
	<?php echo $this->BootstrapPaginator->pagination(); ?>
</div>
