<div class="row-fluid">
		<div class="navbar-inverse">
			<div class = "navbar-inverse_left">
				<h2  class = "pull_left">
				 	Edit User
				</h2>
			</div>
			<a href = '/my/password/<?php echo $this->data['User']['id']; ?>'  class = "btn btn-sm btn-default pull-right" >Change Password</a>
			<div class = "clear"></div>
		</div>
		<div class = "admin_padding">
			<?php 					
			if (!empty($save_message)): ?>
				<?php echo $this->UserManagementForm->printSavedAlert($save_message=='saved', 'Profile') ?>
				<div class = "clearfix">&nbsp;</div>
			<?php endif ?>			
			<?php 
			
			echo('<h2>'.$this->data['User']['username'].'</h2>');			

				$userDatafields['User.username']['hidden'] = true;
				$userDatafields['User.username']['label'] = '';
				echo $this->UserManagementForm->createForm('User');
				echo $this->UserManagementForm->printFields($userDatafields);
				if($isAdmin){
					echo $this->BootstrapForm->input('group_id',$roles);							
				}
				echo $this->UserManagementForm->printSubmit();									
				echo '<div class = "clearfix">&nbsp;</div>';
			?>                           
			<?php 
				echo $this->BootstrapForm->end();
				echo $this->Session->flash();
			?>			
		</div>
</div>

<?php $this->Js->buffer($this->element('role_js')); ?>