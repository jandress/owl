<div class="row-fluid">
		<div class="navbar-inverse">
			<div class = "navbar-inverse_left">
				<h2  class = "pull_left">
					<?php echo __('Change Password'); ?>
				</h2>
			</div>
			<div class = "clear"></div>
		</div>
		<div class= "admin_padding">
			<?php
			if (!empty($save_message)): ?>
				<?php echo $this->UserManagementForm->printSavedAlert($save_message=='saved', 'Password') ?>
				<div class = "clearfix">&nbsp;</div>
			<?php endif ?>
			<fieldset>
			<?php 
				echo $this->UserManagementForm->createForm('User');
				echo $this->UserManagementForm->printFields(array('User.id' => array()));
				echo $this->UserManagementForm->printFields($passwordfields);
				echo $this->UserManagementForm->printSubmit();									
				echo '<div class = "clearfix">&nbsp;</div>';
			?>                           
			</fieldset>
			<?php 
				echo $this->BootstrapForm->end();
			?>						
		</div>
</div>
