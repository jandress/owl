<div class="row-fluid">
		<?php 
		if (!empty($save_message)): ?>
			<?php echo $this->UserManagementForm->printSavedAlert($save_message=='saved', 'Profile') ?>
			<div class = "clearfix">&nbsp;</div>
		<?php endif ?>			
		<div class="bm_1em">
			<legend><?php echo __('My Profile'); ?>
				<a href = '/my/password/' class = "btn pull-right">Change Password</a>
			</legend>	
		</div>
		<fieldset>
			<?php 
				$userDatafields['User.username']['disabled'] = 'disabled';
				echo $this->UserManagementForm->createForm('User');
				echo $this->UserManagementForm->printFields($userDatafields);
				//echo $this->UserManagementForm->printFields($passwordfields);
				echo $this->UserManagementForm->printSubmit();									
				echo '<div class = "clearfix">&nbsp;</div>';
			?>                           
		</fieldset>
		<?php 
			echo $this->BootstrapForm->end();
			echo $this->Session->flash();
		?>			
</div>
<div class = "clearfix">&nbsp;</div>

