<div class=" ">

		<div class="navbar-inverse">
			<div class = "navbar-inverse_left">
				<h2  class = "pull_left">
					New User
				</h2>
			</div>
			<a href = '/my/users/' class = "btn btn-default btn-sm pull-right ajax_link" >User Listing</a>	


			<div class = "clear"></div>
		</div>



	<div class = "admin_padding">

			<?php 
			if (!empty($save_message)): ?>
				<?php echo $this->UserManagementForm->printSavedAlert($save_message=='saved', 'Profile') ?>
				<div class = "clearfix">&nbsp;</div>
			<?php endif ?>			
			<fieldset>
				<?php 
//						$userDatafields['User.username']['disabled'] = 'disabled';
					echo $this->UserManagementForm->createForm('User');
					echo $this->UserManagementForm->printFields($userDatafields);
					echo $this->UserManagementForm->printFields($passwordfields);
					if($isAdmin){
						echo $this->BootstrapForm->input('group_id',$roles);							
					}					
					echo $this->UserManagementForm->printSubmit();									
					echo '<div class = "clearfix">&nbsp;</div>';
				?>                           
			</fieldset>
			<?php 
				echo $this->BootstrapForm->end();
				echo $this->Session->flash();
			?>			
	</div>
</div>
