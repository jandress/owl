<div class="">
		<div class="navbar-inverse">
			<div class = "navbar-inverse_left">
				<h2  class = "pull_left"><?php echo __('My Profile'); ?></h2>
			</div>
			<a href = '/my/password/' class = "btn btn-sm btn-default pull-right ajax_link">Change Password</a>
			<div class = "clear"></div>
		</div>
		<div class = "admin_padding">
		<?php 
		if (!empty($save_message)): ?>
			<?php echo $this->UserManagementForm->printSavedAlert($save_message=='saved', 'Profile') ?>
			<div class = "clearfix">&nbsp;</div>
		<?php endif ?>			
			<?php 

				$userDatafields['User.username']['disabled'] = 'disabled';
				echo $this->UserManagementForm->createForm('User');
				echo $this->UserManagementForm->printFields($userDatafields);
				//echo $this->UserManagementForm->printFields($passwordfields);
				echo $this->UserManagementForm->printSubmit();									
				echo '<div class = "clearfix">&nbsp;</div>';
			?>                           
			<?php 
				echo $this->BootstrapForm->end();
				echo $this->Session->flash();
			?>			
		</div>


</div>
