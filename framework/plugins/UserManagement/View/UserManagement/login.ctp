<div class ="tm_1_em pl_4_em">
	<div class="panel panel-default">
<div class="panel-body">
	<div class="">
			<?php echo($this->Form->create('User', array('url'=>'/login/','class'=>'form-inline'))); ?>
   			<h4 class="">Please sign in!</h4>
			<div class = "clearfix">&nbsp;</div>
			<?php
				$params = array('params'=>array('class'=>'alert alert-error'));
				$g = $this->Session->flash('auth',$params);
				$g = empty($g) ? $this->Session->flash('flash',$params) : $g;
				if(!empty($g)){ echo $g;}
			 ?>
			<div class="control-group tm_1_px">
				<div class="input-prepend input-append">
					<a class="btn add-on hasTooltip" data-placement="left" data-original-title="User Name" >
						<i class="icon-user"></i>
					</a>
					<?php echo($this->Form->input('username',array('label'=>'','div'=>null,'placeholder' => "User Name", 'class'=>'standalone_login_module_field'))); ?>
					<a data-original-title="" data-placement="right" href="" class="add-on btn" title="">
					</a>
				</div>
			</div>
			<div class="control-group">
				<div class="input-prepend input-append">
					<a class="btn add-on hasTooltip" data-placement="left" data-original-title="Password">
						<i class="icon-lock">&nbsp;</i>
					</a>
					<?php echo($this->Form->input('password',array('label'=>'','div'=>null,'placeholder' => "Password", 'class'=>'standalone_login_module_field'))); ?>
					<a data-original-title="Forgot your password?" data-placement="right" href = "/iforgot/" class="hasTooltip add-on btn" title="Forgot your password?">
						<i data-placement="left" class="icon-question-sign" data-original-title="User Name" title="Forgot your password?">&nbsp;</i>
					</a>
				</div>
			</div>
			<div class = "clearfix">&nbsp;</div>
			<div class="pr_2_em pl_2_em">
				<?php
					echo $this->Form->submit('Log in', array(
						'div' => array('class'=>'submit_container'),
						'class'=>'btn-block btn-large btn btn-success',
					));
					echo($this->Form->end());
				?>
			</div>
		<div class = "clearfix">&nbsp;</div>
	</div><!-- well grey_background -->
  </div>
</div>
<?php $this->Js->buffer("console.log($('.hasTooltip').tooltip({trigger:'hover', html:'false' }));"); ?>
</div>
