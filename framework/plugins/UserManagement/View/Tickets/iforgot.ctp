<div class="tm_2em h_centered_content">
	<div style = "width:350px" class="standalone-unit white_background standalone_login_module tm_1em h_centered">
		<a href = "http://konark_market"><div class="logo h_centered hasTooltip" data-placement="right" data-original-title="Password">&nbsp;</div></a>
		<a class="h_centered" href = "/" >
			<h4>Forgot Your Password?</h4>
		</a>
		<div class="well grey_background">
			<p>Simply enter your username or email address, enter the verification code you see on the next page, and we will email instructions to you on how to reset your password.</p>
			<div class="h_centered_content">
				<?php                                                                
					$controller = "users";
					echo($this->Form->create(array('class'=>'form-inline'))); ?>
					<div class="control-group tm_2em">
						<div class="input-prepend input-append">
							<a class="btn add-on hasTooltip" data-placement="left" data-original-title="User Name" >
								<i class="icon-user"></i>
							</a>
							<?php echo($this->Form->input('username',array('label'=>'','div'=>null,'placeholder' => "User Name", 'class'=>'standalone_login_module_field'))); ?>
						</div>
					</div>
					<?php 
				    $options = array(
					    'label' => 'Submit',
					    'div' => array(
							'class' => 'btn primary-btn',
					    )
				    );
					?><div class="padding_left_25 padding_right_25"><?php
						echo $this->Form->submit('Send password reset', array(
							'div' => false,
							'class' => 'tm_1em btn btn-primary btn-large btn-block',
						)); 
						echo($this->Form->end());					
					?></div>				
			</div>
			</form>		
		</div><!-- well grey_background -->
	</div>
</div>
<?php $this->Js->buffer("console.log($('.hasTooltip').tooltip({trigger:'hover', html:'false' }));"); ?>