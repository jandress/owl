<div class = "clearfix bm_1em">&nbsp;</div>
<a class="h_centered" href = "/" >
	<div class="alert">
		An email with instructions has been sent to the email address we have on file for you.
	</div>
</a>
<?php if (!empty($link)): ?>
	<strong>	
		<?php echo $link; ?>
	</strong>				
<?php endif ?>
<div class = "bm_1em">&nbsp;</div>
<div class = "clearfix">&nbsp;</div>