

	<div class="navbar-inverse admin_padding">
			<?php echo $this->Html->link(__('New %s', __('Group')), array('action' => 'add'),array('class'=>'btn btn-default btn-sm pull-right tm_2_em')); ?>
	<h2>
		Groups
	</h2>
	<p>
		<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
	</p>
	</div>




<div class="row-fluid">
	<div class="admin_padding">

		<table class="table table-striped table-bordered table-hover">
			<tr>
				<th style = "text-align:center;">
					Default
				</th>
				<th  style = "text-align:center;"><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('name');?></th>

				<th class="actions"  style = "text-align:center;"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($groups as $group): ?>
			<tr>
				<td style = 'text-align:center;'>
					<input name="data[Landingpage][home]" type="radio" value="1" checked="true" id="Landingpage1">
				</td>
				<td  style = "text-align:center;"><?php echo h($group['Group']['id']); ?>&nbsp;</td>
				<td><?php echo h($group['Group']['name']); ?>&nbsp;</td>

				<td class="actions"  style = "text-align:center;">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $group['Group']['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $group['Group']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $group['Group']['id']), null, __('Are you sure you want to delete # %s?', $group['Group']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>

</div>
