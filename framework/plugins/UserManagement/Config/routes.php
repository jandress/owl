<?php
	// Router::connect('/resetpassword', array('plugin'=>'UserManagement','controller' => 'tickets', 'action' => 'index'));
	// Router::connect('/resetpassword/', array('plugin'=>'UserManagement','controller' => 'tickets', 'action' => 'index'));
	// Router::connect('/reset/sent', array('plugin'=>'UserManagement','controller' => 'tickets', 'action' => 'lost_password_reset_sent'));
	// Router::connect('/reset/sent/', array('plugin'=>'UserManagement','controller' => 'tickets', 'action' => 'lost_password_reset_sent'));
	// Router::connect('/iforgot/', array('plugin'=>'UserManagement','controller' => 'Tickets', 'action' => 'iforgot'));
	// Router::connect('/iforgot', array('plugin'=>'UserManagement','controller' => 'Tickets', 'action' => 'iforgot'));
	// Router::connect('/resetpassword/*', array('plugin'=>'UserManagement','controller' => 'tickets', 'action' => 'resetpassword'));
	// Router::connect('/my/users/dummy/*', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'dummy'));




	Router::connect('/users/welcome', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'welcome'));
	Router::connect('/welcome', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'welcome'));





	Router::connect('/my/users/search/*', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'search'));


	Router::connect('/users/login', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'login'));
	Router::connect('/my/users/login', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'login'));
	Router::connect('/login', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'login'));
	Router::connect('/logout', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'logout'));

	Router::connect('/activate/*', array('plugin'=>'UserManagement','controller' => 'tickets', 'action' => 'activate_account'));

	Router::connect('/signup/*', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'signup'));
	Router::connect('/signup', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'signup'));


	Router::connect('/my/profile', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'profile'));
	Router::connect('/my/profile/*', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'profile'));

	Router::connect('/my/password', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'password'));
	Router::connect('/my/password/*', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'password'));

	Router::connect('/my/users/edit/*', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'admin_edit'));
	Router::connect('/my/users/add/*', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'add'));
	Router::connect('/my/users/role/*', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'role'));
	Router::connect('/my/users/*', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'index'));
	Router::connect('/confirmsignup/', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'confirmsignup'));



	Router::connect('/UserManagement/delete/*', array('plugin'=>'UserManagement','controller' => 'UserManagement', 'action' => 'delete'));

	Router::connect('/my/acls', array('plugin'=>'UserManagement','controller' => 'AclManagement', 'action' => 'index'));
	Router::connect('/my/groups', array('plugin'=>'UserManagement','controller' => 'Groups', 'action' => 'admin_index'));
	// Router::connect('/resendinvite/', array('controller' => 'tickets', 'action' => 'resendinvite'));
	// Router::connect('/resendinvite', array('controller' => 'tickets', 'action' => 'resendinvite'));
