<?php

App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('Security', 'Utility');
App::uses('Auth', 'Component');


class UserManagementController extends UserManagementAppController {

	var $uses = array('MessagingSystem.Message','UserManagement.Ticket','UserManagement.User','UserManagement.Group','Publisher','UserManagement.Message');
	var $helpers = array('Html',"Session" , "Js",'TwitterBootstrap.BootstrapHtml',  'TwitterBootstrap.BootstrapForm','UserManagement.UserManagementForm' );
	public $components = array('Acl', 'Session','Security','Auth','UserManagement','RequestHandler','Email');

	var $userDatafields = array(
		'User.id' => array(),
		'User.username' => array(),
		'User.email_address' => array(),
		'User.first_name' => array(),
		'User.last_name' => array(),
		'User.last_name' => array(),
	);

		var $passwordfields = array(
			'User.password'=> array(),
			'User.confirmPassword' => array('type'=>'password')
		);

		var $default_redirect = '/my/dashboard/';

    function beforeFilter() {
					parent::beforeFilter();
					$this->Security->blackHoleCallback = 'forceSSL';
					$this->set('page_style',"owl.css");
					$this->set('page_script',"common.js");
					$this->Security->requireSecure('login', 'checkout','signup','my_index','admin_edit','profile','password');
					$this->Auth->allow(array("login",'logout',"signup","my_delete","lost_recovery_login","signup","publisher_signup"));
					$this->set('model','User');
					$this->set('controller','users');
					$this->Auth->userModel = 'User';
					$this->Auth->fields = array('username' => 'username', 'password' => 'password'); //have to put both, even if we're just changing one
					$this->Auth->loginAction = //array('controller' => 'users', 'action' => 'login');
					$this->Auth->logoutRedirect = '/login/';
    }


	function forceSSL(){
		//$this->redirect('https://' . $_SERVER['SERVER_NAME'] . $this->here);
	}




	public function add(){
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->data = Sanitize::clean($this->data);
			//		$d = $this->User->save($this->data);
			$d = $this->User->saveAndHashPassword($this->data);
			if($d){
				$this->data =$this->User->read();
				$this->current_user = $this->data['User'];
				$this->set('save_message','saved');
				$this->redirect('/my/users/');
			} else {
				$this->set('save_message' ,'not saved');
			}
		}else{
			// $this->data = array(
			// 	'User'=>$this->current_user
			// );
		}
		$defaultGroup = 1;
		$r = array(
			'options'=>$this->Group->find('list'),
			'value'=>$defaultGroup
		);
		$this->set('roles' ,$r);
		$this->set('passwordfields' ,$this->passwordfields);
		$this->set('userDatafields' ,$this->userDatafields);
		$this->setDashboardLayout();
		$this->handleAjax();
	}





	public function delete($id){
	///	$this->checkPermissions();
		// dependant on setCurrentUser() in appController
		if ($this->request->is('post') || $this->request->is('put')) {
			// $this->data = Sanitize::clean($this->data);
			$this->User->id = $id;
			$user = $this->User->read();
			$un = $user['User']['username'];

			$d = $this->User->delete($id);
			$aro = $this->Aro->find('first',array('conditions'=>array('alias'=>$un)));
			//you had to restructure this because it appears that when users are created, aro's are not always created.  That seems problematic.
			if(!empty($aro)){
				$d = $this->Aro->delete($aro['Aro']['id']);
			}
			if($d){
				$this->data =$this->User->read();
			}
			$this->set('save_message','User Deleted');
		}else{

		}
		$this->redirect('/my/users/');
	}





	public function login() {
		$this->layout='background_only';
		if(isset($this->current_user)){
			//$this->redirect('/my/dashboard/');
		}
		$this->set('element','login_forms');
		if(!empty($this->data)) {
			$this->data = Sanitize::clean($this->data, array('encode' => false));
			$g = $this->Auth->login();
			if($g){
				// Check for User Status...
				$n = $this->Auth->user();
				switch($n['status']){
					case (1):
						// valid login
						//	$this->redirect( $this->default_redirect );
						return $this->redirect($this->Auth->redirect( ));//$this->redirect($this->Auth->redirectUrl());
					break;
					default:
					case (0):
						// stuff
					case (2):
						// account disabled
						$this->set('element','login_failed');
					break;
				}
			}else{
	       		$this->Session->setFlash(__('Invalid username or password, try again'));
			}
		}
	}



		public function confirmsignup(){
			$this->layout='background_only';
			//This is activation code.

			$user = $this->current_user['User'];
			$this->Message->sendWelcomeMessage($user['id']);
			$guid = uniqid($user['username'],true);
			$this->Ticket->newSignupTicket($guid,$user['email_address'],$user['id']);
			$this->set('element' ,'user_signup_sent');
			$this->set('successful' , true);

			// $this->onUserSignup();
			if(Configure::read('debug')){
				echo $this->set('link','<a href = "/activate/'.$guid.'">Click Me</a>');
			}
		}





	public function signup(){
			$this->layout='background_only';
			$title_for_layout = "Signup";
			$this->set('title_for_layout',$title_for_layout);
			$this->set('passwordfields',true);
			$this->set('element' ,'');
			$this->set('userDatafields' ,$this->userDatafields);
			$this->set('passwordfields' ,$this->passwordfields);
			//---
			if(!empty($this->data) ) {
				$this->data = Sanitize::clean($this->data);
				$b = $this->data;
				$b['User']['ip_address']=$_SERVER['REMOTE_ADDR'];
				$b['User']['group_id'] = $this->data['User']['Group'];


				$d = $this->User->saveAndHashPassword($b);
				$this->data = Sanitize::clean($this->data, array('encode' => false));
				$g = $this->Auth->login();

				if($d){

					$user = $d['User'];
					$this->redirect('/my/courses ');
					// $this->current_user = $user;
			//		$t = $this->UserManagement->createUser($user['username']);
					// you should bounce to the course selection page. then from there to the email sent page. thouse should be 3 different pages.
					// debug($this->request);
					// debug($t);





				}
			}
	}






	public function password($id=null){
		$this->setDashboardLayout();
		$this->set('userDatafields' ,$this->userDatafields);
		$this->set('passwordfields' ,$this->passwordfields);
		if ($this->request->is('post') || $this->request->is('put')) {

			$this->User->id = $id;
			$user = $this->User->read();
			// this->data
			// debug($this->data);
			 //	debug($user);
				if($this->data['User']['confirmPassword']==$this->data['User']['password']){
					$user['User']['confirmPassword'] = $user['User']['password'] = $this->data['User']['password'];
				}
				$this->data = Sanitize::clean($user);
				if($this->User->saveAndHashPassword($this->data)){
					$this->Session->setFlash("Password Updated");
					$this->set('save_message','saved');
				}else{
					$this->Session->setFlash("Password not Updated");
					$this->set('save_message','not saved');
				}
		}else{
		}
	}


	public function profile(){
		// debug($this->current_user); exit;
		
		// dependant on setCurrentUser() in appController
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->data = Sanitize::clean($this->data);
			$d = $this->User->save($this->data);
			if($d){
				$this->data =$this->User->read();
				$this->current_user = $this->data['User'];
				$this->set('save_message','saved');
			} else {
				$this->set('save_message','not saved');
			}
		}else{
			$this->data = $this->current_user;
			// array(
			// 	'User'=>$this->current_user
			// );
		}
		$this->set('data',$this->data);
		//$this->data = $this->current_user['User'];
		$this->set('passwordfields' ,$this->passwordfields);
		$this->set('userDatafields' ,$this->userDatafields);
		$this->setDashboardLayout();
	}









	public function admin_edit($id){
	//	$this->checkPermissions();
		// dependant on setCurrentUser() in appController

		if ($this->request->is('post') || $this->request->is('put')) {
			// $this->data = Sanitize::clean($this->data);
			//debug($this->data);
			//exit;
			$d = $this->User->save($this->data['User']);
			//debug($d);
			if($d){
				$this->data =$this->User->read();
				//debug($this->data);
				$this->current_user = $this->data['User'];
				$this->set('save_message','saved');
			} else {
				$this->set('save_message','not saved');
			}
		}else{
			$this->User->id = $id;
			$this->data = $this->User->read();
		}

		$this->set('passwordfields' ,$this->passwordfields);
		$this->set('userDatafields' ,$this->userDatafields);
		//debug($this->data);
		$r = array(
			'options'=>$this->Group->find('list'),
			'value'=>$this->data['Group']['id']
		);
		$this->set('roles' ,$r);
		$this->setDashboardLayout();
		$this->handleAjax();
	}









	public function logout(){
		$this->redirect($this->Auth->logout());
	}

	//~security admin access only.
	public function my_index() {
		//my_users is admin only.  listing of publishers.  may be a good idea to
		$this->setDashboardLayout();
		$this->set('data', $this->paginate('User'));
		$this->set('controller','Users');
		$this->set('model','User');
		$this->set('display_field','username');
	}

	public function index(){
		// $this->checkPermissions();
			// debug('');
			// debug('index and edit should be admin only. lock it down. For other users, ssers nav, should not say users, but my profile. then profile is just edit and change password. change password has a link back to profile and thats it.  Verify Password needs implementation.');
			//my_users is admin only.  listing of publishers.  may be a good idea to
			$this->setDashboardLayout();
			$this->paginate=array(
				'conditions'=>array(
					'User.username !='=>'admin'
				)
			);

			$userId = $this->current_user['User']['id'];
			$this->set('userId',$userId);
			//$this->User->contain('Aro');
			$u = $this->paginate('User');
			$this->set('users', $u);
			$this->set('controller','Users');
			$this->set('model','User');
			$this->set('display_field','username');
			$this->handleAjax();
	}

	function welcome() {
		$this->layout='background_only';
	}



}
