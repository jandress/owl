<?php
	App::uses('CakeEmail', 'Network/Email');
	class SystemMailerComponent extends Component {
		
		public function contactAdmin(){
			$email = new CakeEmail();
			$email->from(array('me@example.com' => 'My Site'));
			$email->to('jon@konarkpublishers.com');
			$email->subject('About');
			$email->send('My message');			
		}
		
	
	}
?>