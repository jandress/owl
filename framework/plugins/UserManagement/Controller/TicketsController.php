<?php

	App::uses('CakeEmail', 'Network/Email');
	App::uses('Security', 'Utility');

	class TicketsController extends UserManagementAppController {

		var $name = 'Tickets';
		var $components = array('Session','Auth','UserManagement.SystemMailer','UserManagement','Acl');
		public $helpers = array('Html',"Session","Form", "Js",'Paginator' => array('className' => 'TwitterBootstrap.BootstrapPaginator','Time'));
		var $uses = array('UserManagement.User','UserManagement.Ticket');

		public $paginate = array(
	        'limit' => 20,
	        'order' => array(
	            'Ticket.date_issued' => 'asc'
	        )
	    );


		public function beforeFilter(){
			parent::beforeFilter();
			$this->Auth->allow('iforgot','resetpassword','activate_account','lost_recovery_login','lost_password_reset_sent');
		}

		/*****************************************************************
			Password reset
		******************************************************************/

		public function iforgot(){
			// $this->layout="background_only";
			// if data empty, serv the forms. else  create a password ticket and send the email.
			if(empty($this->data)) {
				$this->set('element','iforgot');
			}else{
				// $this->data has username only
				// get the user based on username from the ticket,
				$this->set('element','iforgot_sent');
				$g = $this->User->find('first',array('conditions'=>array('User.username'=>$this->data['User']['username'])));
				$user = $g['User'];
				// create ticket data ticket data (id, guid , email, type , date_issued, visited, user_id)
				$guid = uniqid($user['username'],true);
				// 	$data = array( 'guid' => $guid , 'email' => $email , 'type' => $type );
				$this->Ticket->newPasswordTicket($guid,$user['email_address'],$user['id']);
				$this->redirect('/reset/sent/');
			}
		}

		public function resetpassword($guid){
			// change this to "resetpassword"
			//  password reset.  perform approprite action (and set appropriate element) for whether or not passwords match..
			//  check password
			$this->layout="small_standalone";
			// $this->set('element','reset');
			$this->set('element','reset');



			if(empty($this->data)) {

				// has no password data...just serve password Fields
				// find current ticket,  im not sure if this next part is redundant or not...
				$this->data = $this->Ticket->find('first',array('conditions'=>array('Ticket.guid'=>$guid)));
				$this->Ticket->id = $this->data['Ticket']['id'];
				$this->data = $this->Ticket->read();
				//if this ticket has been already used once by the user, it is invalid?  Maybe this is good policy, maybe not.
				// if the ticket has been visited, serve invalid.  else save it as visited.
				if($this->data['Ticket']['visited']){
					$this->set('element','reset_invalid');
				}else{
					$this->set('element','reset');
					// maybe even delete instead of "visited"
					$this->Ticket->save(
						array('visited'=>true)
					);
				}
				$uid =$this->data['Ticket']['user_id'];
				$n = $this->User->find("first",array('conditions'=>array('User.id'=>$uid)));
				//This was that wierd shit where the password hash was getting output to the field
				$n['User']['password'] = '';
				$this->data = $n;
			}else{
					$u = $this->data['User'];
					$a = $u['password'];
					$b = $u['confirmPassword'];
					//	pr($this->data);exit;

						$this->User->id = $u['id'];
						$user = $this->User->read();
						$user['User']['password'] = $a;
						$user['User']['confirmPassword'] = $u['confirmPassword'];
						$this->data = $user;
					//	pr($this->data);
						if($this->User->saveAndHashPassword($this->data)){
							$this->set('element','reset_sent');
						}
						$this->set('link','link');

			}
		}

		public function lost_password_reset_sent(){
			$this->layout='small_standalone';
		//	pr($this->current_user);
		}
		/******************************************************************
	  		Account Signup / Activation....
		***************************************************************** */
		public function my_index(){
			$this->setDashboardLayout();
			// $data = $this->Ticket->getAuthorizationRequests();
			$this->paginate['conditions']=array(
				'Ticket.type'=> 1
			);
		 	$this->set('data', $this->paginate('Ticket'));
			$this->set('rowStyle','tickets/ticket_row');
			$this->set('controller','ticket');
			$this->set('model','Ticket');
			$this->set('displayField','title');
			$this->set('title_for_layout','Authorization Requests');
			$this->set('ticket_types' , array('Signup','Forgotten Password','Authorization Request'));
		}

		public function my_authorization($guid){
			$this->setDashboardLayout();
			$this->data = $this->Ticket->find('all',array('conditions'=>array('guid'=>$guid)));
			$this->set('data',$this->data);
		}

		public function activate_account($guid){
			// initiate and creates
			 $this->layout = "background_only";
			//has data, get ticket data. set element, save as visited, set user status to '1' ( activated )
			// if it doesnt have data nothing is required because the empty fields serve up
			$this->Ticket->contain(array('User'=>array('fields'=>array('id','status'))));
			$this->data = $this->Ticket->find('first',array('conditions'=>array('Ticket.guid'=>$guid)));
			if(!$this->data){
				$this->set('element','reset_invalid');
				return;
			}
			$this->set('hide_try_again_link',true);
			// if the ticket has already been visited, it is invalid.  Otherwise serve account activated view elements
			if($this->data['Ticket']['visited']){
				$this->set('element','reset_invalid');
			}else{
				$l = $this->data['Ticket'];
				$l['visited'] = 1;
				// save as visited, serve account activated view elements
				$this->Ticket->save(array('Ticket'=>$l));
				//save User.status as 1.




				//debug( $this->data['User'] );
				//exit;

				$this->User->id = $this->data['User'];
				// $this->User->read();

				$this->User->status = 1;

				$d = $this->data['User'];
				$d['status']=1;
				$s = $this->User->save(array('User'=>$d));
				$u = $this->Auth->login($this->data['User']);
				if( $s && $u){
					$this->redirect('/my/dashboard/');
				}
			}
		}

		public function resendinvite(){
			$this->layout="background_only";
			//	debug($this->data);
			if(!empty($this->data)) {
				$data = $this->User->find("first",array('conditions'=>array(
			    	"User.username"=>$this->data['User']['username']
			    )));
				$this->Ticket->newSignupTicket(uniqid($data['User']['username'],true),$data['User']['email_address'],$data['User']['id']);
				$this->set('element','resend_invite_sent');
			}else{
				$this->set('element','resend_invite');
			}
		}

		private function passwordsMatch($a,$b){
			return $a==$b;
		}

		//~security note!!
		public function expunge(){
			// for shell
			/// will clear out all tickets older than 1020 minutes
			$d = $this->Ticket->find('all',array('fields'=>array('id','date_issued')));
	        App::uses('CakeTime', 'Utility');
			foreach ($d as $key => $value) {
				$timeLimit = "1020 minutes";
				if(
					! CakeTime::wasWithinLast($timeLimit,$value['Ticket']['date_issued'] )
					){
					echo "expired";
				}
				echo'<br/>';
				echo CakeTime::timeAgoInWords($value['Ticket']['date_issued']);
			}
			// SELECT DATE(`yourtimestamp`) FROM thetablename WHERE DATE(`yourtimestam`) < CURDATE() - INTERVAL 2 DAY;
			exit;
		}

	}
?>
