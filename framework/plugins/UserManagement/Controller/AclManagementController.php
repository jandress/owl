<?php

App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('Security', 'Utility');
App::uses('Auth', 'Component');


class AclManagementController extends UserManagementAppController {

	var $uses = array('Aro','Aco');
	var $helpers = array('Html',"Session" , "Js",'TwitterBootstrap.BootstrapHtml',  'TwitterBootstrap.BootstrapForm','UserManagement.UserManagementForm','Tree' ); 

	public $components = array('UserManagement.UserManagement','Acl', 'Session','Security','Auth','RequestHandler','Email');				

	var $roles= array(
		'students'=>'students',
		'staff'=>'staff',
		'administrators'=>'administrators',
	);
	

	var $default_redirect = '/my/dashboard/';

	
    function beforeFilter() {}							

	function forceSSL(){
		//	$this->redirect('https://' . $_SERVER['SERVER_NAME'] . $this->here);
	}

	public function index() {
			//debug($this->Aro->find('all'));
		// $this->Aco->recursive = 2;
		// $this->Aco->displayField = 'alias';
		// debug(Router::getPaths());
		  // $dr = ClassRegistry::init('Dynamicroute'); 
		  // $cmsdata = $dr->find('all'); 
		  // debug($cmsdata);
		//debug(App::objects('UserManagement.Controller', null, true));




$routes = Router::$routes;
$plugins = array('plugins'=>array());

foreach ($routes as $key => $value) {
	//debug($value);
	// does this plugin exist. 
	if (!empty($value->defaults['plugin'])) {
		if(empty($plugins['plugins'][$value->defaults['plugin']]   ) ){
			$plugins['plugins'][$value->defaults['plugin']] = array('controllers'=>array());
		}
		
		if(empty($plugins['plugins'][$value->defaults['plugin']]['controllers'][$value->defaults['controller']]   ) ){
			if(empty($plugins['plugins'][$value->defaults['plugin']][$value->defaults['controller'] ]  ) ){
				$plugins['plugins'][$value->defaults['plugin']]['controllers'][$value->defaults['controller']] = array('actions'=>array()); 
			}		
		}		

		if(empty( $plugins['plugins'][$value->defaults['plugin']]['controllers'][$value->defaults['controller']]['actions'][$value->defaults['action']]  ) ){

				 array_push($plugins['plugins'][$value->defaults['plugin']]['controllers'][$value->defaults['controller']]['actions'],$value->defaults['action']);
		}	

		//$g[$value->defaults['plugin']];
	}
}

debug($plugins);




		debug($this->Aco->find('all',array()));
		debug($this->Aro->find('all',array()));
		//debug($this->Aco->children());
			exit;

						
	}


	private function createAros(){
		$variable = $this->User->find('all');
		$aro = new Aro();		
		foreach ($variable as $key => $value) {
			$ar = array(
				'foreign_id' => $value['User']['id'],
				'alias'=>$value['User']['username'],
				'model'=>'User',
				'parent_id'=>2
			);
	        $aro->create();	
	        $aro->save($ar);
		}
	}
		
	


}
