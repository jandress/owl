<?php
App::uses('AppModel', 'Model');
App::uses('CakeEmail', 'Network/Email');


class Ticket extends UserManagementAppModel {

	var $SIGNUP = 0;
	var $FORGOTTEN_PASSWORD = 1;
	var $AUTHORIZATION_REQUEST = 2;
	var $base_url='http://konark_market/';
	var $belongsTo = 'User';
	var $actsAs = array('Containable');

	//	ALTER TABLE tickets RENAME COLUMN type_human_readable to name;

	public function newPasswordTicket($guid,$email,$userId){
		$this->data = array( 'guid' => $guid , 'email' => $email , 'type' => $this->FORGOTTEN_PASSWORD,'user_id'=>$userId );	
		$this->save($this->data);		
		//$message = $this->base_url.'resetpassword/'.$guid.'/';
		$message = FULL_BASE_URL.'/resetpassword/'.$guid.'/ ';		
		$this->sendEmail( $email, 'Password Reset', $message );
	}	

	public function newSignupTicket($guid,$email,$userId){
		$this->data = array( 'guid' => $guid , 'email' => $email , 'type' => $this->SIGNUP,'user_id'=>$userId );	
		$this->save($this->data);
		$message = FULL_BASE_URL.'/activate/'.$guid.'/'."\n";		
	//	$message = 'Thank you for your interest in KonarkMarket!  We\'re opening the doors the the next few days and will send you an email as soon as we\'re ready!';
		$this->sendEmail($email, 'Activate your KonarkMarket account!',$message);		
	}

	public function newAuthorizationRequestTicket($guid,$email,$userId,$message){
		$this->data = array( 'guid' => $guid , 'email' => $email , 'type' => $this->AUTHORIZATION_REQUEST,'user_id'=>$userId );	
		$this->save($this->data);
		$message .= FULL_BASE_URL.'/tickets/authorize/'.$guid.'/'."\n";		
		$this->sendEmail($email, 'Authorization Request: ',$message);		
	}

	private function sendEmail($to,$subject,$message){
		//~security:  remove log statements from Tickets		
		$email = new CakeEmail();
		$email->from(array('noreply@konarkmarket.com' => 'Konark Market '));
		$email->to($to);
		$email->subject($subject);
		$email->send($message);
		if( Configure::read('debug') == 2){
			error_log('New Ticket Email To: '.$to.' Subject: '."\n".$subject.' Message: '.$message);			
		}
	}


	public function getAuthorizationRequests(){
		return $this->find('all', array( 'conditions' => array('type'=>1)));
	}

}
// id 	int(255) 			No 	None 	AUTO_INCREMENT 	Change Change 	Drop Drop 	Show more actions More
// 2 	guid 	text 	latin1_swedish_ci 		No 	None 		Change Change 	Drop Drop 	Show more actions More
// 3 	email 	text 	latin1_swedish_ci 		No 	None 		Change Change 	Drop Drop 	Show more actions More
// 4 	type 	int(3) 			No 	0 		Change Change 	Drop Drop 	Show more actions More
// 5 	date_issued 	timestamp