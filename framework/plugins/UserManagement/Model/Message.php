<?php

//App::uses('UserManagementAppModel', 'UserManagement.Model');
App::uses('AppModel', 'Model');

class Message extends UserManagementAppModel {

	var $welcomeSubject="Welcome to Konark Market!";
	var $welcomeFrom = "Jon Andress, Konark Market";
	var $welcomeMessage = "Please excuse our dust. We have a bit of construction going on right now. So you may notice a bug or two. If you do notice something, please be a pal and let us know about it.  Anyone reporting a previously unreported bug will recieve store credit.  Thanks, and happy reading.  Jon ";

	var $useDbConfig = 'konark_messaging';	
	
	public $belongsTo = array(
	      'User' => array(
	          'className'    => 'User',
	          'foreignKey'   => 'sender_id'
	      )
	  );


	public function sendWelcomeMessage($userId) {
			$this->data = array(
				'Message'=> array(
					'subject'=> $this->welcomeSubject,
		            'sender_id' => 3,
		            'rec_id' => $userId,
					'message'=>$this->welcomeMessage
				),
				//'User'=>$this->current_user
			);	
			$this->save($this->data);
	}
	
	public function sendPublisherWelcomeMessage($userId){
			$this->data = array(
				'Message'=> array(
					'subject'=> 'Welcome Publisher!',
		            'sender_id' => 3,
		            'rec_id' => $userId,
					'message'=>$this->welcomeMessage
				),
				//'User'=>$this->current_user
			);	
			$this->save($this->data);
	}
	

	
	
	
}

/*

public $validate = array(
	'sender_id' => array(
		'numeric' => array(
			'rule' => array('numeric'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
	),
	'rec_id' => array(
		'numeric' => array(
			'rule' => array('numeric'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
	),
	'read' => array(
		'boolean' => array(
			'rule' => array('boolean'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
	),
	'subject' => array(
		'notempty' => array(
			'rule' => array('notempty'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
	),
);

*/