<?php
App::uses('AppModel', 'Model');
// App::uses('AuthComponent', 'Model');

class User extends AppModel {

	public $displayField = 'username';
  // public $useTable = 'mockusers';
	public $user_role = 'visitor';
	var $actsAs =array("Containable");


	public $filterArgs = array(
		'q' => array(
			'type' => 'like',
			// 'encode' => true,
			// 'before' => false,
			// 'after' => false,
			'field' => array(
				'User.first_name',
				'User.last_name'
				)
			)
		//'q' => array('type' => 'subquery', 'method' => 'findByCategory', 'field' => 'Book.id')
        //'copy' => array('type' => 'like', 'field' => 'Book.copy'),
    );

	public $belongsTo = array(
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasAndBelongsToMany = array(
			 'Course' =>
					 array(
							 'className' => 'Course',
							 'joinTable' => 'courses_users',
							 'foreignKey' => 'user_id',
							 'associationForeignKey' => 'course_id',
							 // 'unique' => true,
							 // 'conditions' => '',
							 // 'fields' => '',
							 // 'order' => '',
							 // 'limit' => '',
							 // 'offset' => '',
							 // 'finderQuery' => '',
							 // 'with' => ''
					 ),
					 'Courseevent' =>
							array(
									 'className' => 'Courseevent',
									 'joinTable' => 'courseevents_users',
									 'foreignKey' => 'user_id',
									 'associationForeignKey' => 'courseevent_id',

										'conditions' => '',
							 ),
					'Collaboration' =>
							array(
									'className' => 'Collaboration',
									'joinTable' => 'collaborations_users',
									'foreignKey' => 'user_id',
									'associationForeignKey' => 'collaboration_id',
									// 'unique' => true,
									// 'conditions' => '',
									// 'fields' => '',
									// 'order' => '',
									// 'limit' => '',
									// 'offset' => '',
									// 'finderQuery' => '',
									// 'with' => ''
							)
	 );

	 public function getCourseList($options=array()){
		 	$boundModel = ClassRegistry::init('Course');
 			$p = $boundModel->find('list',$options);
			return $p;
	 }


	public function setUserStatus($g){
		return $this->save(array('status'=>$g));
	}

	public function saveAndHashPassword($g){
		// must confirm
		if($g['User']['password']==''){
//			$this->invalidate($notempty);
		}
		$g['User']['password'] = AuthComponent::password($g['User']['password']);
		$g['User']['confirmPassword'] = AuthComponent::password($g['User']['confirmPassword']);
		App::uses('Utitlity','Security');
		return $this->save($g);
	}

	// so that this can be called by controllers.   
	public function hashString($str){
		return AuthComponent::password($str);
	}

	public function getUserRoleByUserId($userId){
		//debug($userId);
		//$boundModel = ClassRegistry::init('Group');
		//debug($boundModel); exit;
		//$boundModel->find('first');
		$this->contain(array('Group'));
		return $this->find('first',array('conditions'=>array('User.id'=>$userId)));

		//debug($boundModel->getColumnTypes());
		//debug($boundModel); exit;
		$boundModel->create();
		$boundModelData = $boundModel->save(array());
		//CURRENTLY USING USERNAME
		$groups = array(
			$this->STUDENT,
			$this->STAFF,
			$this->ADMINISTRATOR,
			$this->DEVELOPER,
			$this->VISITOR
		);
		$boundModel = ClassRegistry::init('Aro');
		$p = $boundModel->find('first',array('fields'=>array('parent_id'),'conditions'=>array('alias'=>$userId)));
		//debug($p);
		foreach ($groups as $key => $value) {
			if ($value['id'] == $p['Aro']['parent_id']) {
				debug($value); exit;
				return $value;
			}
		}
	}





	public function getRoleIdByLabel($label){
			$groups = array(
				$this->STUDENT,
				$this->STAFF,
				$this->ADMINISTRATOR,
				$this->DEVELOPER,
				$this->VISITOR
			);
		foreach ($groups as $key => $value) {
			if ($value['label'] == $label) {
				return $value['id'];
			}
		}
		return null;
	}



	public function saveUserRoleByUserId($user,$role){
		$boundModel = ClassRegistry::init('Aro');
		$d = $boundModel->find(
			'first',
			array(
				'conditions' => array( 'alias'=>$user)
			)
		);
		$u = $this->find('first',array('conditions'=>array(
			'username'=>$user
		)));
		if(empty($d) && !empty($u)){
			$d = $boundModel->create();
			$d['Aro']['alias'] = $user;
			$d['Aro']['model'] = "User";
			debug($d);
		}
		$u = $this->find(
			'first',
			array(
				'conditions' => array('username'=>$user)
			)
		);
		$d['Aro']['parent_id']=$this->getRoleIdByLabel($role);
		return  $boundModel->save($d);
	}


	public function hashPassword($user){
		$this->id=$user['id'];
		$this->data = $this->read();
		App::uses('Utitlity','Security');
		if(!empty($this->data['User']['password'])) {
			return $this->save(array(
				'confirmPassword'=>AuthComponent::password($this->data['User']['confirmPassword']),
				'password'=>AuthComponent::password($this->data['User']['password']),
				));
		}
		return false;
	}



	public function wrapper($value, $options = array(), $rule = array()) {
		$method = $options[0][0];
		$options[0][0] = $this->data[$this->alias][$options[1][0]];

		if (method_exists($this, $method)) {
			$valid = $this->{$method}($value);
		} else {
			$valid = call_user_func_array(array('Validation', $method), $options[0]);
		}

		if (!$valid) {
			foreach($options[1] as $field) {
				$this->invalidate($field, $rule['message']);
			}
		}
		return $valid;
	}

	public function compare($value, $options = array(), $rule = array()) {
		$valid = current($value) == $this->data[$this->alias][$options[0]];
		if (!$valid) {
			$this->invalidate(isset($options[1])? $options[1] : $options[0], $rule['message']);
		}
		return $valid;
	}
}
