<?php
		Router::connect('/Navigations/Navlinks/admin_index/*', array('plugin'=>'Navigations','controller' => 'Navlinks', 'action' => 'admin_index'));
		///----------------------------------------------------------------------------------------------------------
		Router::connect('/BlogPost/BlogPosts/admin_index/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'admin_index'));
		Router::connect('/my/blogposts/edit/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'admin_edit'));
		Router::connect('/my/blogposts/edit', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'admin_edit'));
		Router::connect('/my/blogposts/edit/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'admin_edit'));
		///----------------------------------------------------------------------------------------------------------
		Router::connect('/my/blogposts/layouts/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'admin_layouts'));

		Router::connect('/my/blog/search/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'search_edit'));
		Router::connect('/blog/search/edit/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'search_edit'));
		Router::connect('/blog/search/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'search'));
		///----------------------------------------------------------------------------------------------------------
		Router::connect('/blogposts/iflash/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'iflash'));
		Router::connect('/my/blogposts/files/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'admin_files'));
		///----------------------------------------------------------------------------------------------------------
		Router::connect('/my/blogposts/delete/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'admin_delete'));
		///----------------------------------------------------------------------------------------------------------
		Router::connect('/my/blogposts/add/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'admin_add'));
		Router::connect('/my/blogposts/', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'admin_index'));
		Router::connect('/my/blogposts', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'admin_index'));
		Router::connect('/my/blogposts/search', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'search'));
		///----------------------------------------------------------------------------------------------------------
		Router::connect('/blog/view/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'view'));
		Router::connect('/blogposts/view/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'view'));
		Router::connect('/my/blogposts/view/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'view'));
		Router::connect('/raw/blogposts/view/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'raw_view'));
		///----------------------------------------------------------------------------------------------------------
		Router::connect('/embedded/blogposts/view/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'embedded_view'));
		Router::connect('/blogposts/*', array('plugin'=>'BlogPost','controller' => 'BlogPosts', 'action' => 'index'));

?>
