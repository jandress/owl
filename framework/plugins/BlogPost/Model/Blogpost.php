<?php
App::uses('AppModel', 'Model');
/**
 * Post Model
 *
 */
class Blogpost extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';
	public $useTable ="blogposts";
	public $editAction = '/my/blogposts/edit/';
	public $viewAction = '/blogposts/view/';
	var $actsAs = array('Search.Searchable', 'Containable','Polymorphic');
	// var $filterQs = 'q';


	public $filterArgs = array(
		'q' => array(
			'type' => 'like',
			// 'encode' => true,
			// 'before' => false,
			// 'after' => false,
			'field' => array(
				'Blogpost.title',
				'Blogpost.body'
				)
			)
		//'q' => array('type' => 'subquery', 'method' => 'findByCategory', 'field' => 'Book.id')
        //'copy' => array('type' => 'like', 'field' => 'Book.copy'),
    );



	


}
