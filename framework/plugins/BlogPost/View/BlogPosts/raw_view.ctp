<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo __($title_for_layout); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSS -->
	<?php
		if(!isset($concat_assets)){
			$concat_assets = true;
		}
		if(empty($cssBuild)){
			$cssBuild = 'common';
		}
	 	echo($this->AssetCompress->css($cssBuild, array('raw' => $concat_assets))."\n");
 	?>
	<script type="text/javascript" src = '/js/jquery.js'></script>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
	<style type="text/css" media="screen">
		body{background-attachment:fixed;background-color:#FFFFFF;background-image:none;}
	</style>
  </head>

  <body>

    <!-- Part 1: Wrap all page content here -->
    <div id="wrap">
      <!-- Begin page content -->
      <div class="container">
		<div class = "clearfix">&nbsp;</div>
        <div class="row-fluid content">
				<?php echo $this->element('view'); ?>
      	</div><!--/row-->
      </div>
      <div id="push"></div>
    </div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

  </body>
</html>
