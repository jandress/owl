<div class="well">
	<div class="white_well">
		<h2>Blog Search Results</h2>
		<?php
			$p = $this->BootstrapPaginator->pagination(array('div'=>'pagination'));
			echo $this->Paginator->counter(
			    'Page {:page} of {:pages}, showing {:current} records out of
			     {:count} total, starting on record {:start}, ending on {:end}'
			);
			echo $p;
			echo '<hr/>';
			$model = 'Blogpost';
			 foreach ($data as $key => $value):
					$var = $value[$model];
					$url = $var['viewActionFull'];
				?>
				<a href = "<?php echo $url;?>" >
					<h3><?php echo $var['title'] ?></h3>
				</a>
				<?php

				$s = Sanitize::clean(
					String::truncate( $var['body'], 249, array('ending' => '...','exact'=>true) )
					, array('encode' => true,'remove_html'=>true)
				);
				$s = str_replace('\n', '', $s);
				echo $s
				?>
				<br/>
				<a class = "" href = "<?php echo $url;?>">View ></a>
				<hr/>
				<div class = "clearfix">&nbsp;</div>
			<?php endforeach ?>
			<?php echo $p; ?>
	</div>
</div>
