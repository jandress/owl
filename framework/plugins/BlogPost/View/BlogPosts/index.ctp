<div class="col-md-12">
	<div class="">
		<h2><?php echo __( __("The Blog"));?></h2>
		<div class="navbar">
			<div class="navbar-inner">
				<div style="padding-top:8px" class="span9">
					<p>
						<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
					</p>
				</div>
				<div class="nav span3 pull-right">

				</div>
			</div>
		</div>
	</div>
	<div class="">
		<?php foreach ($Blogposts as $Blogpost): ?>
				<div class="bm_1_em">
					<h3>
						<a href = "/blogposts/view/<?php echo h($Blogpost['Blogpost']['id']); ?>"><?php echo h($Blogpost['Blogpost']['title']); ?></a>
					</h3>
					<p>
						<?php
							$p = $Blogpost['Blogpost']['body'];
						//	$p = Sanitize::html($p,array('remove'=>true));
							echo __(String::truncate(
							            $p,
							            350,
							            array(
							                'ending' => '...',
							                'exact' => false,
							                'html' => false
							            )
							        )
							); ?>&nbsp;
					</p>
					<b>
						<?php
 							echo $Blogpost['Blogpost']['created'];
						?>
					</b>
				</div>
				<div class="clear">
					&nbsp;
				</div>
			<?php endforeach; ?>
			<?php echo $this->BootstrapPaginator->pagination(); ?>

	</div>
</div>
