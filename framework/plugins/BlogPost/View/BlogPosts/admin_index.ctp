

<div class="">
	<div class="navbar-inverse">
		<div class = 'pull-left'>
			<h2 ><?php echo __( __('Blog Posts'));?></h2>
			<p><?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')));?></p>
		</div>
		<div class = ''>
			<div class="pull-right col-lg-6 tm_2_em">

				<?php echo $this->Form->create('BlogPost', array('url' => '/blog/search/edit/')); ?>
				<div class="input-group input-group-sm col-lg-6  pull-right">
					<input name="data[Blogpost][q]" class="form-control bm_1_px search-query" placeholder="Search" type="text" id="BlogPostQ" aria-describedby="sizing-addon3">
					<span class="input-group-btn">
							<input class="btn btn-default" type="button" type="submit" value="Search">
					</span>
				</div>
				<?php echo $this->Form->end(); ?>
				<a href = '/my/blogposts/add/' class = 'btn btn-default btn-sm pull-right' style="margin-right:1em;"><div class="glyphicon glyphicon-plus-sign">&nbsp;</div>New Entry&nbsp;</a>
			</div>
		</div>
		<div class = 'clear'></div>
	</div>










<div class="admin_padding">
	<?php echo $this->BootstrapPaginator->pagination(); ?>
	<table class="table table-striped table-bordered table-hover">
	<tr>
		<th class = "span1" style = "text-align:center" ><?php echo $this->BootstrapPaginator->sort('id');?></th>
		<th class = "span6"><?php echo $this->BootstrapPaginator->sort('title');?></th>
		<th class="actions span3" style = "text-align:center" ><?php echo __('Actions');?></th>
	</tr>
	<?php foreach ($posts as $post):?>
	<tr>
		<td class = "span1" style = "text-align:center" ><?php echo h($post['Blogpost']['id']); ?>&nbsp;</td>
		<td><?php echo h($post['Blogpost']['title']); ?>&nbsp;</td>
		<td class="actions span3" style = "text-align:center" >
			<a target = '_blank' href="/blog/view/<?php echo $post['Blogpost']['id']; ?>/">View</a>
			<span class = ''> | </span>
			<a href="/my/blogposts/edit/<?php echo $post['Blogpost']['id']; ?>/">Edit</a>
			<span class = ''> | </span>
			<?php
			echo $this->Form->postLink( __('Delete'), '/my/blogposts/delete/'.$post['Blogpost']['id'] , array($post['Blogpost']['id']) ,__('Are you sure you want to delete # %s?', $post['Blogpost']['title']) );
			?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->BootstrapPaginator->pagination(); ?>
</div>
