<div>
	<div class="navbar-inverse">
		<h1><?php  echo __($title_for_layout); ?></h1>
	</div>
	<div class = "admin_padding tm_2_em col-md-10">
		<?php echo $this->BootstrapForm->create('Blogpost', array('class' => ''));?>
		<fieldset>
			<?php
				echo $this->BootstrapForm->input('title',array('class'=>'col-md-12'));
				echo '<div class = "clearfix bm_2_em">&nbsp;</div>';
//echo $this->BootstrapForm->input('author', array('class'=>'col-md-12'));
			?>

			<?php
				echo $this->BootstrapForm->input('body',array('class'=>'col-md-12 ckeditor', 'style'=>"height:450px;"));
				echo $this->BootstrapForm->hidden('id');
			?>
			<div class = "clear bm_1_em"></div>
			<?php echo $this->BootstrapForm->submit(__('Submit'),array('div'=>'pull-right'));?>
		</fieldset>
		<?php echo $this->BootstrapForm->end();?>

	</div>
</div>
