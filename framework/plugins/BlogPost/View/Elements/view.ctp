<div class="white_well">
		<h1>
			<?php echo __($post['Blogpost']['title']); ?>		
		</h1>
		<?php if (!empty($post['Blogpost']['author'])): ?>
			<?php if ($post['Blogpost']['author'] !=''): ?>
				<h4>Contributed By:
					<?php
						echo $post['Blogpost']['author'];
					?>
				</h4>
			<?php endif ?>
		<?php endif ?>
		<h5>
			<?php
				echo $this->Time->nice($post['Blogpost']['created']);
			?>
		</h5>		
		<hr/>
		<div class = "clearfix" style = 'height:1px;'>&nbsp;</div>
		
		<?php
			if (!empty($post['Blogpost']['media_format'])) {
				if ($post['Blogpost']['media_format'] != 'quiz') {
					echo $this->element('media/'.$post['Blogpost']['media_format']); 			
				}else{
					echo $this->element('media/'.$post['Blogpost']['media_format']); 			  		
					echo '<div class="tm_3_em">'.$this->element('Quizmodules.quizzes/view').'</div>';
				}
			}
		?>		

		<?php if (!empty($includeDate)): ?>
			<h4><?php echo $this->Time->niceShort($post['Blogpost']['created']); ?></h4>					
		<?php endif ?>
		<?php echo __($post['Blogpost']['body']); ?>		
		<?php if (!empty($includeDate)): ?>
			<em>Last edited: <?php echo $this->Time->niceShort($post['Blogpost']['modified']); ?></em>			
		<?php endif ?>
		<div class = "clearfix tm_3_em">&nbsp;</div>

		<a class = 'btn disabled'>Comment on this article</a>
</div>