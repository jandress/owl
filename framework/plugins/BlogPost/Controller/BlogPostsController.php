<?php

App::uses('AppController', 'Controller');
/**
 * Posts Controller
 *
 * @property Post $Post
 */
class BlogPostsController extends AppController {

/**
 *  Layout
 *
 * @var string
 */
//	public $layout = 'frontend';
	var $uses = array('BlogPost.Blogpost', 'Quizmodules.Quiz','Navigations.Navlink');
	public $helpers = array('paginator'=>'TwitterBootstrap.BootstrapPaginator','TwitterBootstrap.BootstrapPaginator');

	public $formatListing = array(
		''=>'none'
		,'img'=>'Image'			
		,'iframe_flash'=>'Standalone Flash'
		,'modal_content'=>'Launch In Modal'
		,'flash'=>'Flash'
		,'video'=>'video'						
		,'quiz'=>'Quiz'	
	);

	public $paginate = array(
        'limit' => 20,
        'order' => array(
			// 'Book.title' => 'asc'
        )
		//, 'contain'=> array(
		// 	'Publisher'
		// )
		,'tag' => 'li'
    );



	public $components = array('Session','Fileutil','Search.Prg');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->setNavIdByAssociatedURL();
		$this->Blogpost->recursive = 0;
		$this->set('title_for_layout', "Blog Post Listing");
		$this->set('Blogposts', $this->paginate());		

	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */





	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('view','raw_view','embedded_view','search','index');
	}

	public function view($id = null) {
//		$g = $this->Navlink->find('all');
// 		$g = $this->Blogpost->find('all');
// 		foreach ($g as $key => $value) {
// 			debug($value);
// //			$this->Navlink->create();
// //			$this->Navlink->parent_id=564
// 		}
// 		exit;
		$this->setNavIdByAssociatedContent('BlogPost',$id);		
		$this->Blogpost->id = $id;
		if (!$this->Blogpost->exists()) {
			throw new NotFoundException(__('Invalid %s', __('post')));
		}
		$this->data = $post = $this->Blogpost->read(null, $id);
		$this->set('title_for_layout', $this->data['Blogpost']['title']);
		$this->set('post', $post);
	}
	
	public function raw_view($id){
		$this->view($id);
		$this->layout = 'ajax';
		$this->set('ROUTING_PREFIX', 'raw');		
	}
	
	public function embedded_view($id){
		$this->view($id);
		$this->layout = 'embedded';
		$this->set('ROUTING_PREFIX', 'embedded');		
	}
	
	public function search_edit(){
		$this->Prg->commonProcess();
		$this->setDashboardLayout();
		//$this->Blogpost->
		$n = $this->Blogpost->parseCriteria($this->passedArgs);
       	$this->paginate['conditions'] = $n;		
    	// $this->set('books', $this->paginate());
		$this->set('data',$this->paginate());
		$this->set('controller','Books' );
	}
	
	public function search(){
		$this->Prg->commonProcess();
		$n = $this->Blogpost->parseCriteria($this->passedArgs);
       	$this->paginate['conditions'] = $n;		
    	// $this->set('books', $this->paginate());
		$this->set('data',$this->paginate());
		$this->set('controller','Books' );
	}
	
	public function admin_files($g=null){
		$this->handleAjax();
		if(!empty($g)){
			switch ($g) {
				case 'quiz':
					$this->set('element','quizSelect');
					$quiz_list = $this->Quiz->find('list',array(  'order' => array('Quiz.name' => 'ASC'))); 
					$quizOptions = array(
						'label'=>'Quiz'
						,'options'=>$quiz_list
					);
					$this->set('quizOptions',$quizOptions);										
				break;
				case 'modal_content':
					$this->set('element','admin_files');
					$fileListing=$this->Fileutil->getFileListingForOptions(IMAGES.'media'.DS.'iframe_flash', array('fileNameAsKeys'=>true));
					$this->set('fileListing',$fileListing);
				break;
				//iframe_flash
				default:
					$this->set('element','admin_files');
					$fileListing=$this->Fileutil->getFileListingForOptions(IMAGES.'media'.DS.$g, array('fileNameAsKeys'=>true));
					$this->set('fileListing',$fileListing);
				break;
			}
		}else{
			$this->set('fileListing',array() );
		}
	}
		
	public function iflash($targetDirectory=null){
		$this->layout = 'ajax';
	}
	
	public function admin_layouts($g=null){
		//recieves the filename of the layout
		// i think this only exists in case 
		$this->handleAjax();		
		$this->set('formatListing',$this->formatListing );		
		// get the image directory by default
		$this->set('fileListing',array() );
//		$this->set('fileListing',$fileListing=$this->Fileutil->getFileListingForOptions(IMAGES.'media'.DS.'img', array('fileNameAsKeys'=>true, 'omitDirectories'=>true)));
	}


	public function admin_index() {
		$this->setDashboardLayout();				
		$this->set('posts', $this->paginate());		
	}

	public function admin_view($id = null) {
		$this->Blogpost->id = $id;
		if (!$this->Blogpost->exists()) {
			throw new NotFoundException(__('Invalid %s', __('post')));
		}
		$this->set('post', $this->Blogpost->read(null, $id));
	}

	public function admin_add() {
		$this->set('formatListing',$this->formatListing );				
		if ($this->request->is('post')) {
			$this->Blogpost->create();
			if ($this->Blogpost->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('post')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('/my/blogposts/');
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('post')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$this->title("New Post");		
	}

	public function admin_edit($id = null) {
//		$fileListing=$this->Fileutil->getFileListingForOptions(IMAGES.DS.'posts', array('fileNameAsKeys'=>true, 'omitDirectories'=>true));
//		$layoutListing=$this->Fileutil->getFileListingForOptions(IMAGES.DS.'posts', array('fileNameAsKeys'=>true, 'omitDirectories'=>true));
//		$layoutListing=$this->Fileutil->getFileListingForOptions(IMAGES.DS.'posts', array('fileNameAsKeys'=>true, 'omitDirectories'=>true));		
		$siteLayouts = APP.DS.'View'.DS.'Layouts'.DS.'posts';
		$s = array('fileNameAsKeys'=>true,'removeFileExtension'=>true, 'omitDirectories'=>true,'allowNullEntry'=>true);
		
		$layoutListing=$this->Fileutil->getFileListingForOptions($siteLayouts, $s);		
			if(
			empty($layoutListing) ||
			(count($layoutListing)<=1)
			){
				$pluginLayouts = CORE_PATH.'../plugins'.DS.'BlogPost'.DS.'View'.DS.'Layouts';
				$layoutListing = $this->Fileutil->getFileListingForOptions($pluginLayouts, $s);	
			}
		$this->set('layoutListing',$layoutListing );
		$this->Blogpost->id = $id;
		if (!$this->Blogpost->exists()) {
			throw new NotFoundException(__('Invalid %s', __('post')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Blogpost->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('post')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('/my/blogposts/');
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('post')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Blogpost->read(null, $id);
			$this->title("Editing Post: ".$this->request->data['Blogpost']['title']	);							
		}
	}

	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Blogpost->id = $id;
		if (!$this->Blogpost->exists()) {
			throw new NotFoundException(__('Invalid %s', __('post')));
		}
		if ($this->Blogpost->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('post')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect('/my/blogposts/');
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('post')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect('/my/blogposts/');
	}

	public function inlineedit($id){}
}
