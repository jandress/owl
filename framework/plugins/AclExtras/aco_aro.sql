-- phpMyAdmin SQL Dump
-- version 3.5.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 05, 2012 at 02:49 AM
-- Server version: 5.5.22
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `konark_market_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=171 ;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, NULL, NULL, 'controllers', 1, 340),
(2, 1, NULL, NULL, 'Books', 2, 31),
(3, 2, NULL, NULL, 'index', 3, 4),
(4, 2, NULL, NULL, 'view', 5, 6),
(5, 2, NULL, NULL, 'my_index', 7, 8),
(6, 2, NULL, NULL, 'my_add', 9, 10),
(7, 2, NULL, NULL, 'my_edit', 11, 12),
(8, 2, NULL, NULL, 'my_delete', 13, 14),
(9, 2, NULL, NULL, 'getCurrentUserPermissions', 15, 16),
(10, 2, NULL, NULL, 'setCurrenUser', 17, 18),
(11, 2, NULL, NULL, 'leftCol', 19, 20),
(12, 2, NULL, NULL, 'rightCol', 21, 22),
(13, 2, NULL, NULL, 'setCombinedPageScript', 23, 24),
(14, 2, NULL, NULL, 'setCombinedPageStyleSheet', 25, 26),
(15, 2, NULL, NULL, 'setDashboardLayout', 27, 28),
(16, 2, NULL, NULL, 'get_habtm_items', 29, 30),
(17, 1, NULL, NULL, 'Browses', 32, 63),
(18, 17, NULL, NULL, 'index', 33, 34),
(19, 17, NULL, NULL, 'view', 35, 36),
(20, 17, NULL, NULL, 'my_index', 37, 38),
(21, 17, NULL, NULL, 'my_view', 39, 40),
(22, 17, NULL, NULL, 'my_add', 41, 42),
(23, 17, NULL, NULL, 'my_edit', 43, 44),
(24, 17, NULL, NULL, 'my_delete', 45, 46),
(25, 17, NULL, NULL, 'getCurrentUserPermissions', 47, 48),
(26, 17, NULL, NULL, 'setCurrenUser', 49, 50),
(27, 17, NULL, NULL, 'leftCol', 51, 52),
(28, 17, NULL, NULL, 'rightCol', 53, 54),
(29, 17, NULL, NULL, 'setCombinedPageScript', 55, 56),
(30, 17, NULL, NULL, 'setCombinedPageStyleSheet', 57, 58),
(31, 17, NULL, NULL, 'setDashboardLayout', 59, 60),
(32, 17, NULL, NULL, 'get_habtm_items', 61, 62),
(33, 1, NULL, NULL, 'Cart', 64, 85),
(34, 33, NULL, NULL, 'index', 65, 66),
(35, 33, NULL, NULL, 'my_index', 67, 68),
(36, 33, NULL, NULL, 'getCurrentUserPermissions', 69, 70),
(37, 33, NULL, NULL, 'setCurrenUser', 71, 72),
(38, 33, NULL, NULL, 'leftCol', 73, 74),
(39, 33, NULL, NULL, 'rightCol', 75, 76),
(40, 33, NULL, NULL, 'setCombinedPageScript', 77, 78),
(41, 33, NULL, NULL, 'setCombinedPageStyleSheet', 79, 80),
(42, 33, NULL, NULL, 'setDashboardLayout', 81, 82),
(43, 33, NULL, NULL, 'get_habtm_items', 83, 84),
(44, 1, NULL, NULL, 'Categories', 86, 121),
(45, 44, NULL, NULL, 'index', 87, 88),
(46, 44, NULL, NULL, 'view', 89, 90),
(47, 44, NULL, NULL, 'add', 91, 92),
(48, 44, NULL, NULL, 'edit', 93, 94),
(49, 44, NULL, NULL, 'delete', 95, 96),
(50, 44, NULL, NULL, 'my_index', 97, 98),
(51, 44, NULL, NULL, 'my_add', 99, 100),
(52, 44, NULL, NULL, 'my_edit', 101, 102),
(53, 44, NULL, NULL, 'my_delete', 103, 104),
(54, 44, NULL, NULL, 'getCurrentUserPermissions', 105, 106),
(55, 44, NULL, NULL, 'setCurrenUser', 107, 108),
(56, 44, NULL, NULL, 'leftCol', 109, 110),
(57, 44, NULL, NULL, 'rightCol', 111, 112),
(58, 44, NULL, NULL, 'setCombinedPageScript', 113, 114),
(59, 44, NULL, NULL, 'setCombinedPageStyleSheet', 115, 116),
(60, 44, NULL, NULL, 'setDashboardLayout', 117, 118),
(61, 44, NULL, NULL, 'get_habtm_items', 119, 120),
(62, 1, NULL, NULL, 'Dashboard', 122, 141),
(63, 62, NULL, NULL, 'my_index', 123, 124),
(64, 62, NULL, NULL, 'getCurrentUserPermissions', 125, 126),
(65, 62, NULL, NULL, 'setCurrenUser', 127, 128),
(66, 62, NULL, NULL, 'leftCol', 129, 130),
(67, 62, NULL, NULL, 'rightCol', 131, 132),
(68, 62, NULL, NULL, 'setCombinedPageScript', 133, 134),
(69, 62, NULL, NULL, 'setCombinedPageStyleSheet', 135, 136),
(70, 62, NULL, NULL, 'setDashboardLayout', 137, 138),
(71, 62, NULL, NULL, 'get_habtm_items', 139, 140),
(72, 1, NULL, NULL, 'Home', 142, 167),
(73, 72, NULL, NULL, 'index', 143, 144),
(74, 72, NULL, NULL, 'my_edit', 145, 146),
(75, 72, NULL, NULL, 'my_index', 147, 148),
(76, 72, NULL, NULL, 'setAdminViewData', 149, 150),
(77, 72, NULL, NULL, 'getCurrentUserPermissions', 151, 152),
(78, 72, NULL, NULL, 'setCurrenUser', 153, 154),
(79, 72, NULL, NULL, 'leftCol', 155, 156),
(80, 72, NULL, NULL, 'rightCol', 157, 158),
(81, 72, NULL, NULL, 'setCombinedPageScript', 159, 160),
(82, 72, NULL, NULL, 'setCombinedPageStyleSheet', 161, 162),
(83, 72, NULL, NULL, 'setDashboardLayout', 163, 164),
(84, 72, NULL, NULL, 'get_habtm_items', 165, 166),
(85, 1, NULL, NULL, 'Pages', 168, 195),
(86, 85, NULL, NULL, 'about', 169, 170),
(87, 85, NULL, NULL, 'contact', 171, 172),
(88, 85, NULL, NULL, 'my_index', 173, 174),
(89, 85, NULL, NULL, 'setAdminViewData', 175, 176),
(90, 85, NULL, NULL, 'display', 177, 178),
(91, 85, NULL, NULL, 'getCurrentUserPermissions', 179, 180),
(92, 85, NULL, NULL, 'setCurrenUser', 181, 182),
(93, 85, NULL, NULL, 'leftCol', 183, 184),
(94, 85, NULL, NULL, 'rightCol', 185, 186),
(95, 85, NULL, NULL, 'setCombinedPageScript', 187, 188),
(96, 85, NULL, NULL, 'setCombinedPageStyleSheet', 189, 190),
(97, 85, NULL, NULL, 'setDashboardLayout', 191, 192),
(98, 85, NULL, NULL, 'get_habtm_items', 193, 194),
(99, 1, NULL, NULL, 'Publishers', 196, 227),
(100, 99, NULL, NULL, 'index', 197, 198),
(101, 99, NULL, NULL, 'welcome', 199, 200),
(102, 99, NULL, NULL, 'my_index', 201, 202),
(103, 99, NULL, NULL, 'my_view', 203, 204),
(104, 99, NULL, NULL, 'my_add', 205, 206),
(105, 99, NULL, NULL, 'my_edit', 207, 208),
(106, 99, NULL, NULL, 'my_delete', 209, 210),
(107, 99, NULL, NULL, 'getCurrentUserPermissions', 211, 212),
(108, 99, NULL, NULL, 'setCurrenUser', 213, 214),
(109, 99, NULL, NULL, 'leftCol', 215, 216),
(110, 99, NULL, NULL, 'rightCol', 217, 218),
(111, 99, NULL, NULL, 'setCombinedPageScript', 219, 220),
(112, 99, NULL, NULL, 'setCombinedPageStyleSheet', 221, 222),
(113, 99, NULL, NULL, 'setDashboardLayout', 223, 224),
(114, 99, NULL, NULL, 'get_habtm_items', 225, 226),
(115, 1, NULL, NULL, 'Purchases', 228, 247),
(116, 115, NULL, NULL, 'my_index', 229, 230),
(117, 115, NULL, NULL, 'getCurrentUserPermissions', 231, 232),
(118, 115, NULL, NULL, 'setCurrenUser', 233, 234),
(119, 115, NULL, NULL, 'leftCol', 235, 236),
(120, 115, NULL, NULL, 'rightCol', 237, 238),
(121, 115, NULL, NULL, 'setCombinedPageScript', 239, 240),
(122, 115, NULL, NULL, 'setCombinedPageStyleSheet', 241, 242),
(123, 115, NULL, NULL, 'setDashboardLayout', 243, 244),
(124, 115, NULL, NULL, 'get_habtm_items', 245, 246),
(125, 1, NULL, NULL, 'Transactions', 248, 277),
(126, 125, NULL, NULL, 'index', 249, 250),
(127, 125, NULL, NULL, 'shopping_cart', 251, 252),
(128, 125, NULL, NULL, 'delivery', 253, 254),
(129, 125, NULL, NULL, 'checkout', 255, 256),
(130, 125, NULL, NULL, 'confirm', 257, 258),
(131, 125, NULL, NULL, 'pay', 259, 260),
(132, 125, NULL, NULL, 'getCurrentUserPermissions', 261, 262),
(133, 125, NULL, NULL, 'setCurrenUser', 263, 264),
(134, 125, NULL, NULL, 'leftCol', 265, 266),
(135, 125, NULL, NULL, 'rightCol', 267, 268),
(136, 125, NULL, NULL, 'setCombinedPageScript', 269, 270),
(137, 125, NULL, NULL, 'setCombinedPageStyleSheet', 271, 272),
(138, 125, NULL, NULL, 'setDashboardLayout', 273, 274),
(139, 125, NULL, NULL, 'get_habtm_items', 275, 276),
(140, 1, NULL, NULL, 'Users', 278, 315),
(141, 140, NULL, NULL, 'index', 279, 280),
(142, 140, NULL, NULL, 'login', 281, 282),
(143, 140, NULL, NULL, 'logout', 283, 284),
(144, 140, NULL, NULL, 'my_index', 285, 286),
(145, 140, NULL, NULL, 'my_add', 287, 288),
(146, 140, NULL, NULL, 'my_edit', 289, 290),
(147, 140, NULL, NULL, 'my_delete', 291, 292),
(148, 140, NULL, NULL, 'isAdmin', 293, 294),
(149, 140, NULL, NULL, 'isPublisher', 295, 296),
(150, 140, NULL, NULL, 'exe', 297, 298),
(151, 140, NULL, NULL, 'getCurrentUserPermissions', 299, 300),
(152, 140, NULL, NULL, 'setCurrenUser', 301, 302),
(153, 140, NULL, NULL, 'leftCol', 303, 304),
(154, 140, NULL, NULL, 'rightCol', 305, 306),
(155, 140, NULL, NULL, 'setCombinedPageScript', 307, 308),
(156, 140, NULL, NULL, 'setCombinedPageStyleSheet', 309, 310),
(157, 140, NULL, NULL, 'setDashboardLayout', 311, 312),
(158, 140, NULL, NULL, 'get_habtm_items', 313, 314),
(159, 1, NULL, NULL, 'AclExtras', 316, 317),
(160, 1, NULL, NULL, 'AssetCompress', 318, 339),
(161, 160, NULL, NULL, 'Assets', 319, 338),
(162, 161, NULL, NULL, 'get', 320, 321),
(163, 161, NULL, NULL, 'getCurrentUserPermissions', 322, 323),
(164, 161, NULL, NULL, 'setCurrenUser', 324, 325),
(165, 161, NULL, NULL, 'leftCol', 326, 327),
(166, 161, NULL, NULL, 'rightCol', 328, 329),
(167, 161, NULL, NULL, 'setCombinedPageScript', 330, 331),
(168, 161, NULL, NULL, 'setCombinedPageStyleSheet', 332, 333),
(169, 161, NULL, NULL, 'setDashboardLayout', 334, 335),
(170, 161, NULL, NULL, 'get_habtm_items', 336, 337);

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, NULL, NULL, 'administrators', 1, 4),
(2, NULL, NULL, NULL, 'managers', 5, 6),
(3, NULL, NULL, NULL, 'publishers', 7, 10),
(4, NULL, NULL, NULL, 'users', 11, 14),
(5, 1, 'User', NULL, 'admin', 2, 3),
(6, 3, 'User', NULL, 'konark', 8, 9),
(7, 4, 'User', NULL, 'mowgli', 12, 13);

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(1, 1, 148, '1', '1', '1', '1'),
(2, 1, 149, '1', '1', '1', '1'),
(3, 3, 149, '1', '1', '1', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
