<?php

class PagekwikToolsController extends FpoPluginAppController {
	
	
	var $components = array('FpoPlugin.Plugins');
	
	public function beforeFilter(){
		parent::beforeFilter();
		$this->handleAjax();
		$this->Plugins->test();
	}
	
	public function index(){
	}

	public function admin_index(){
		$this->handleAjax();		
	}
	public function admin_edit(){
		$this->handleAjax();		
	}

	public function admin_view(){
		$this->handleAjax();
	}
	
	private function handleAjax(){
		$isAjax = false;
		if ( isset($this->params['requested']) && $this->params['requested'] == 1){
			$isAjax = true;
	    }
		if ($this->request->is('ajax')) {
			$isAjax = true;
		}
		if ($isAjax) {
		    $this->disableCache();
			$this->layout = "ajax";			
		}	
	}	

}

