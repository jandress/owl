
var ajax_slider = function(){

    var fo = {

        loaderString : '',
        contentDiv:"#ajax_container",
        targetSelect: '',
        nocache:false,
        targetDiv:null,
        link_class:'ajax_link',
        ajax_container:null,
        debug:false,

        _debug:function(msg){
            if(fo.debug){
                console.log(msg);
            }
        },

        loadBack: function(href){
            var n = ($("#"+fo.ajax_container).width());                  
            $.ajax({
                url: href,
                error: function(d){
                    alert('Error! Content not found or user login expired. Check to see if you are still logged in.');
                    fo._debug(d);                        
                },
                success: function(data){
                    fo.beforeSlide();                        
                    $("#"+fo.ajax_container).wrapInner("<div id='slidercontainer'></div>");  // check for removal                                  
                    $("#slidercontainer").wrapInner("<div id='slider'></div>");  // check for removal                                   
                    $("#slider").wrapInner("<div id='sliderC1' style = 'float:left;' ></div>");  // check for removal
                    $("#sliderC1").css({width:n}); 
                    $("<div id='sliderC2' style = 'float:left;'></div>").prependTo("#slider");  // check for removal 
                        try {
                            $("#sliderC2").html(data);     
                        }
                        catch (e) {
                           // statements to handle any exceptions
                           fo._debug('error: '+e); // pass exception object to error handler
                        }                        //$("#sliderC2").css({width:n}); 
                    $("#slider").css({width: $("#sliderC1").width() + $("#sliderC2").width()+200 });                                    
                    $("#slidercontainer").css({'margin-left': -n});
                    $("#"+fo.ajax_container).css({'overflow-x':'hidden'});                                        
                     $('#slidercontainer').animate({'margin-left': 0 },300, 
                         function(){
                            fo._onL();
                         }
                     )
                }
            })                                    
        },

        loadNext: function(href){
            fo._debug('ln: '+href);
            var n = ($("#"+fo.ajax_container).width());           
            $("#"+fo.ajax_container).css({'overflow-x':'hidden'});                
            $.ajax({
                url: href,
                error: function(d){
                    //alert('Error! Content not found or user login expired. Check to see if you are still logged in.');
                    fo.onError(d);
                },
                success: function(data){
                    fo.beforeSlide();
                    console.log($("#"+fo.ajax_container))
                    $("#"+fo.ajax_container).wrapInner("<div id='slidercontainer'></div>");  // check for removal                                  

                    console.log($("#slidercontainer"))
                    $("#slidercontainer").wrapInner("<div id='slider'></div>");  // check for removal                                   
                    $("#slider").wrapInner("<div id='sliderC1' style = 'float:left;' ></div>");  // check for removal
                    $("#sliderC1").css({width:n}); 
                    $("<div id='sliderC2' style = 'float:left;'></div>").appendTo("#slider");  // check for removal 

                     try {
                         $("#sliderC2").html(data);     
                     }
                     catch (e) {
                        // statements to handle any exceptions
                        fo._debug('error: '+e); // pass exception object to error handler
                     }

                    $("#sliderC2").css({width:n}); 
                    $("#slider").css({width: $("#sliderC1").width() + $("#sliderC2").width()+200 });                                    
                    $('#slidercontainer').animate({'margin-left': -n },300, function(){ fo._onL(); } );
                }
            })                                    
        },

        beforeSlide: function(response){
            // public callback hook
        },

        onLoaded: function(response){
            // public callback hook
            fo._debug('onLoaded'+response);
        },
        onError: function(response){
            // public callback hook
            fo._debug.log('ajax_slider load error:');
            fo._debug.log(response);
        },

        _onL: function(){
            fo._debug('_onL: '+fo.link_class);
            $("#sliderC1").remove();
            $("#sliderC2").unwrap();
            $("#sliderC2").unwrap();
            $($("#sliderC2").children()).unwrap();
            //--                
            fo.applyAjaxListenerToLinks(fo.link_class);                
            $("#"+fo.ajax_container).css({'overflow-x':'visible'});
            //--
            fo.onLoaded();
            try {
                onAjaxPageReady();
            }
            catch(err) {
                
            }
            
        },
        init: function(_targetDiv, link_class){

            if($(_targetDiv).length==0){
                console.log(_targetDiv+" does not exist.");
                return false
            }

            fo.ajax_container = "ajax_container"+new Date().getTime();
            $(_targetDiv).wrapInner("<div id='"+fo.ajax_container+"'  style = 'width:100%; overflow-y:visible !important;' ></div>");
            fo.link_class = link_class;
            fo.applyAjaxListenerToLinks(fo.link_class);
        },

        applyAjaxListenerToLinks: function(_targetButton,_contentDiv,_ldrString){
            $(_targetButton).unbind('click'); 
            $(_targetButton).bind('click', function() {
                var href = $(this).attr('href');
                fo.loadNext(href);
              return false;
            }); 
            $(_targetButton+"_back").unbind('click'); 
            $(_targetButton+"_back").bind('click', function() {
                var href = $(this).attr('href');
                fo.loadBack(href);
              return false;
            }); 
            fo.applyAjaxPostListener();
        },  


        doAjaxSubmit: function(href){
                $.ajax({
                    url:href,
                    type: "POST",
                    data:$("form").serialize(),
                    success: function(response){
                        fo.loadNext(response);
                    },
                    error: function(response){
                        fo.onError(response);
                    }                    
                }); 
        },

        doAjax: function(href){
          $.ajax({
                    url:href,
                    success: function(response){
                        fo.loadNext(response);
                    },
                    error: function(response){
                        fo.onError(response);
                    }                    
                });                 
        },


        applyAjaxPostListener: function(){
            // submits for values, hands off return value to loadNext or onError
            var _targetButton = $(".ajax_submit");
            //console.log('applyAjaxPostListener: '+($(".ajax_submit")))
            $(_targetButton).unbind('click'); 
            $(".ajax-submit-btn").css({'display':'none'});
            $(_targetButton).bind('click', function() {
                var href = $(this).attr('href');
                $.ajax({
                    url:href,
                    type: "POST",
                    data:$("form").serialize(),
                    success: function(response){
                        if($.parseJSON(response).href){
                            fo.loadNext($.parseJSON(response).href);
                        }
                    },
                    error: function(response){
                        fo.onError(response);
                    }                    
                }); 
              return false;
            }); 
        },

    }    
    return fo;         
    
}
