var file_browser = function(){
        var function_object = {

            i:1,
            speed:350,       
            fade_speed:250,
            currentTargetDir : '',    
            _original_url:'/assets/listing',
            _url:_original_url+'/',
            directoryPath:null,

             toPath: function(){
                var string = '';
                for : function(var i=0; i < directoryPath.length; i++) {
                    string = string+'/'+directoryPath[i];
                };
                string = string+'/';
                return string;
            },
            
            enterDirectory: function(g){
                directoryPath.push(g);
                loadDirectoryContents(_original_url+''+toPath())
            },

            back: function(){
                var i = directoryPath.length > 0 ? directoryPath.length-1 : 0;
                directoryPath.splice(i,1);  
                loadDirectoryContents( _original_url+''+toPath() );
            },

            loadDirectoryContents: function(_url){
                $("#file_listing_loading_graphic").css('display', "block"); 
                $("#sliding_content_container").html('');           
                $.ajax({
                    //the path to the listing...not the listed directory
                    url:_url,
                    success: function(data) {                   
                        $("#file_listing_loading_graphic").css('display', "none");
                        $("#sliding_content_container").css('opacity', "0");
                        $("#sliding_content_container").html(data);
                        $('#sliding_content_container').animate({
                            opacity:"1",
                          }, speed, function(){
                                // Animation complete.             
                        })
                    }                       
                });         
            },

            newFile: function(g){
                loadDirectoryContents('/assets/upload/'+g);
            },

            onUploadComplete: function(){    
                 $("#sliding_content_container").animate({
                        opacity:".0",
                      }, fade_speed, function(){
                        $("#sliding_content_container").empty();        
                    });            
            }            



        }    
        return function_object;         
}






        
       