<div class = "admin_file_listing_toolbar" style = 'z-index:10'>
	<div class="container">		
		<h4 class = "tm_4_px">Uploading to: <?php echo $file_dir_root_absolute.''.$target_directory?></h4>		
		<div class = "upload_toolbar_link btn-group bm_1_em pull-right" >
				<a class = 'btn btn-sm btn-default fb_ajax_link' href = '<?php echo $root_uri.''.$target_directory; ?>' >
					<span class = "glyphicon glyphicon-arrow-left" ></span>
					Back
				</a>
			</div>				
	</div>
</div>


<div class = "clearfix">&nbsp;</div>
<div class="well container">
	<div class="centered">
		<?php echo $this->BootstrapForm->create('dir'); ?>
		<?php echo $this->BootstrapForm->input('name',array('label'=>'New subdirectory name:')); ?>		
		<?php echo $this->Form->submit('submit',array('class'=>'btn tm_1_em')); ?>		
		<?php echo $this->BootstrapForm->end(); ?>		
	</div>
</div>