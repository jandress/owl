<table id="projects" class="table table-striped table-bordered table-hover"  >
	<thead>
		<tr class = 'gb_28'>
			<th class = "col-md-1" >&nbsp;</th>
			<th class = "span9">Name</th>
			<th class="actions col-md-2" style = 'text-align:center'>Options</th>
		</tr>
	</thead>			
	<tbody>
	<?php
	if(!empty($listing)):
	$i=0;
	foreach ($listing as $key => $value): ?>
		<tr id="<?php echo $key; ?>" <?php echo(($i++ % 2 == 0) ? ' class="altrow"' : '')?>>
			<td style = "text-align:center"><?php echo $i; ?></td>
			<?php 
				// not crazy about the hard coding taking place here.			
				$type =$value['type'];	
				$icon = $type;
				switch($type){
					case 'dir':
						$icon = 'glyphicon glyphicon-folder-close';
					break;
					case 'm4v':
						$icon = 'glyphicon glyphicon-facetime-video';
					break;
					case 'png':
					case 'jpg':
					case 'jpeg':
						$icon = 'glyphicon glyphicon-picture';
					break;
				}				
		?>
			<td><span
				 class = "file_listing_index">
			<?php
				// left side listing.
				// you've had a problem with trailing slashes becoming double slashes upon string assembly.  if you continue to have issues, make a helper
				$dirhref = $root_uri.$target_directory.''.$value['item'];
				$assethref = $file_dir_root_absolute.$target_directory.''.$value['item'];


				if($type == "dir"){
					echo('</span> '.'&nbsp;<span class = "'.$icon.'" ></span>&nbsp; <span class = "file_listing_label">');					
					echo('<a class = "fb_ajax_link" href=\''.$dirhref.'\'>'.$value['item'].'</a>');
				} else {

					///echo('hi<a href = "'.$assethref.'" target = "_blank">'.$value['item'].'</a>');							
					?>
					<ul style="list-style:none; padding:0">
			            <li class="dropdown">
			            <?php 				echo('</span> '.'&nbsp;<span class = "'.$icon.'" ></span>&nbsp; <span class = "file_listing_label">'); ?>
			              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<?php echo $value['item'];?>
			              </a>
			                <ul class="dropdown-menu " role="menu" style="text-decoration:none;">

			                    <li>	
									<a target="_blank" href="<?php echo $assethref ?>" >View</a>
			                    </li>			                	

			                    <li>	
									<a href="/downloads<?php echo $assethref ?>">Download</a>
			                    </li>			                	

			                	<li role="presentation" class="divider"></li>			                   			                   									                    
								<li>	
									<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/<?php echo $assethref ?>" >Copy File Path to clipboard</a>
			                    </li>				                    			                    
								<li>	
									<a  data-target="#myModal" role="button" data-toggle="modal"  href="/cpm/link/downloads<?php echo $assethref ?>" >Copy Download Link Syntax to clipboard</a>
			                    </li>	
								<li>	
									<a  data-target="#myModal" role="button" data-toggle="modal"  href="/cpm/downloads<?php echo $assethref ?>" >Copy Download Path to clipboard</a>
			                    </li>				                    			                    
			                </ul>
			            </li>
			          </ul>
					<?php
				}

			echo('</span>');
			?>
			</td>
			<td class="actions" style = 'text-align:center'>
				<?php
					if($type == "dir"){

						echo('<a class= "admin_file_listing_view_button glyphicon glyphicon-eye-open fb_ajax_link" href=\''.$dirhref.'\' ></a>');
						echo "<span class = 'actions_spacer'> | </span>";	

						?><a onclick="return confirm('Are you sure you want to delete \&quot;<?php echo $value['item']; ?>\&quot;?');" class="glyphicon glyphicon-trash" href="/assets/rmdir/<?php echo $target_directory.''.$value['item']; ?>"> </a><?php		
					} else {

						echo('<a class= "admin_file_listing_view_button glyphicon glyphicon-eye-open" href = "'.$assethref.'" target = "_blank"> </a>');
						echo "<span class = 'actions_spacer'> | </span>";	

						?><a onclick="return confirm('Are you sure you want to delete \&quot;<?php echo $value['item']; ?>\&quot;?');" class="glyphicon glyphicon-trash" href="<?php echo $root_uri; ?>delete/<?php echo $target_directory.''.$value['item']; ?>"> </a><?php
					}
				?>
			</td>
		</tr>				
	<?php 
	endforeach;
	else: 
	?>	
		<tr>
			<td colspan=4>No files were found.</td>
		</tr>           
		<?php endif ?>
	</tbody>
</table>