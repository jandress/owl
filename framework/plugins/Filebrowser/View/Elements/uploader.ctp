<div class = "admin_file_listing_toolbar" style = 'z-index:10'>
	<div class="container">		
		<h4 class = "tm_4_px">Uploading to: <?php echo $file_dir_root_absolute.''.$target_directory?></h4>		
		<div class = "upload_toolbar_link btn-group bm_1_em pull-right" >
				<a class = 'btn btn-sm btn-default fb_ajax_link' href = '<?php echo $root_uri.''.$target_directory; ?>' >
					<span class = "glyphicon glyphicon-arrow-left" ></span>
					Back
				</a>
			</div>				
	</div>
</div>
<div class = "clearfix">&nbsp;</div>
<div class="container">
	<div class="well">
		<div class="white_well centered">
			<div class="uploader_background_graphic" >
				&nbsp;
			</div>
			<div class = "clearfix  tm_1_em">&nbsp;</div>
			<div id="file-uploader-demo1">		
				<noscript>	</noscript>         
			</div>
		</div>
	</div>
</div>
<link href="/Filebrowser/css/fileuploader.css" rel="stylesheet" type="text/css">	
<script src="/Filebrowser/js/fileuploader.js" type="text/javascript"></script>
<?php if (empty($root_uri)): 
	$root_uri = '/assets/';
?>	
<?php endif ?>
<script>        
    function createUploader(){     
        var uploader = new qq.FileUploader({
            element: document.getElementById('file-uploader-demo1'),
			<?php 
				$debug = true;
				if (!empty($debug)){
					echo 'debug: "true",'."\n";
				}
				if (!empty($action)){
					echo 'action: "'.$action.'",'."\n";				
				}else{
					echo 'action: "'.$this->Html->url().'",'."\n";
				}
				if (!empty($allowedExtensions)){
					echo "allowedExtensions: ".$allowedExtensions.","."\n";
				}
			?>
			failedUploadTextDisplay: {
				mode: 'custom',
				maxChars: 40,
				responseProperty: 'error',
				enableTooltip: true
			},
			onComplete: function(id, fileName, responseJSON){
				<?php if (!empty($debug)): ?>
					//console.log('responseJSON: '+responseJSON);
				<?php endif ?>
				
				$('#sliding_content_container').fadeOut('slow', function(){
					<?php if (!empty($redirect)){
						if($redirect=="self"){
							$redirect = $root_uri.$target_directory;//$this->Html->url();
						}
						echo 'window.location = "'.$redirect.'";';
					}?>
				});
			},
			onSubmit: function(){
				uploader._options.params = {
						<?php 						
						if(!empty($model)){
							echo $model.':{';
							if(!empty($form_params)){
								$g = 0;
								$t= count($form_params);
								foreach ($form_params as $key => $value){
									echo $key.':'.$value."\n";
									$g++;
									if($g<$t){
										echo ',';
									}
								}
							}
							echo '}';
						}
						?>
				};				
			}			
        });     
		var g;
    }
   // window.onload = createUploader;     
createUploader();
</script>