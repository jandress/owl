<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
			<title>ESL WOW</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<link rel="stylesheet" type="text/css" href="/css/reset.css" />
			<link rel="stylesheet" type="text/css" href="/css/text.css" />
			<link rel="stylesheet" type="text/css" href="/css/960.css" />
			<link rel="stylesheet" type="text/css" href="/css/layout.css" />	
			<link rel="stylesheet" type="text/css" href="/css/admin.css" />				
			<script type="text/javascript" src="/js/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="/js/swfobject.js"></script>
	 </head>          
	<body>                                                            
		<div class="container_16">
			<?php echo $content_for_layout;?>
		</div>          
	</body>
</html>