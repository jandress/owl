<?php

	class FilebrowserController extends AppController {

		var $components = array('RequestHandler','Fileutil');
		var $helpers = array('Html',"Session");       
		var $uses = array('Filebrowser.Fileasset');
        var $default_layout = "file_management";              
        //--
        var $file_dir_root_relative = './media/'; // relative is needed by Fileutil. 
		var $file_dir_root_absolute = '/media/'; // i believe abs is needed by the view.
		//--
		var $root_uri = '/my/assets/';
		//-- 
		var $dir_contents;
		var $current_directory;
		var $directoryPathArray;

		function beforeFilter(){
			parent::beforeFilter();
//			$this->layout = $this->default_layout;
			$this->Auth->allow('downloads');
		}
		function admin_addir(){
			$this->setCommonVars();
			//	$this->layout = "ajax";   
			$g = $this->directoryArrayToString($this->params->params['pass'] );
			if ($this->request->is('post')) {
				$directory_path = str_replace('//','/',$g.'/'.$this->request->data['dir']['name']);
				$c = $this->Fileutil->createDirectoryIfNotExists($this->file_dir_root_relative.$directory_path);
				if($c){
					$this->redirect($this->root_uri.''.$directory_path);
				}
				exit;
			}
		}
		
		
		function admin_rmdir(){
			$this->setCommonVars();	
			if(count($this->dir_contents)==0){
				if($this->Fileutil->deleteDirAndContents($this->file_dir_root_relative.$this->current_directory)){
					$this->redirect($this->root_uri);
				}
			}
			if ($this->request->is('post')) {
				if($this->Fileutil->deleteDirAndContents($this->file_dir_root_relative.$this->current_directory)){
					$this->redirect($this->root_uri);
				}
			}
		}


		function admin_index(){              
///			debug($this->layout); exit;			
			$this->setCommonVars();				
			$this->handleAjax();										
		}
		function ajax_admin_index(){              
			// you are currently using the same address for serving the app as well as serving the listing used by the app,
			// and you think its are clever. only problem is
			// this is a failure of the design. 
			// it causes nested ajax from failing. 
			///debug($this->layout); exit;
			if(empty($this->isAjax)){
//				$this->redirect('/my/assets/');
			}else{
				$this->layout='nestedajax_layout';														
			}
			$this->setCommonVars();				
			
		}


		public function cpm($d = null){
			$this->layout='empty';
			//debug($this->request->pass);
			if(  $this->request->pass[0] =='link'){
				$b = $this->request->pass;
				unset($b[0]);
				$t = implode('/', $b);
				$this->set('d','<a href ="/'.$t.'">/'.$t.'</a>' );
			}else{
				$this->set('d',implode('/', $this->request->pass));				
			}
		}



		public function downloads($data){
			$g = $this->request->pass;
		//	debug($this->request);
			//debug(end($g));
		//	debug($this->request->here);
			//exit;
			$this->set('newName',end($g));
			$p = str_replace('downloads/','','.'.$this->request->here);
			$this->set('total_page',$p);
			$this->layout = 'empty';
		}
		
		
		
		function admin_static_layouts(){
			$this->file_dir_root_relative = '../webroot/user_managed/static_layouts/';
			$this->admin_index('/user_managed/static_layouts/');
		}	    
		
		
		function admin_upload($j=''){
			$this->set('debug', true );
			$this->set('redirect', 'self' );
			$this->setCommonVars();
			//--
			$this->directoryPathArray .=',""';
			$this->set('directoryPathArray', $this->directoryPathArray );	
			//--
			if ($this->request->is('post')) {
				echo '{success: '.$this->Fileasset->saveFile($this->request->data, $this->file_dir_root_relative.$this->current_directory).'}';
				exit;
			}
		}       
		
		       		
		function directoryArrayToString($ar,$trailingSlash = true){
			$g = '';
			foreach ($ar as $key => $value) {
				if($key == count($ar)-1){
					$g .= $value;
					if ($trailingSlash) {
						$g .= '/';						
					}
				}else{
					$g .= $value.'/';						
				}
			}
			return $g; 
		}
		

		function admin_listing($g=""){              	
			$this->layout = "file_management_listing";  
			$this->setCommonVars();	
		}	    
		              
		        
		function admin_preview($directory,$file){
			$this->layout="admin_file_preview_layout";
			$this->set('file',$file);			
		}


  		function admin_delete($directory=null,$file=null){
			$file_name = $this->directoryArrayToString($this->params->params['pass'], false);
			$str = '';
			$ar = $this->params->params['pass'];
			for ($i=0; $i < count($ar)-1; $i++) { 
				$str.=$ar[$i].'/';
			}
			
			if(file_exists($this->file_dir_root_relative.$file_name)){
				unlink($this->file_dir_root_relative.$file_name);
			}

			$this->redirect($this->root_uri.$str);
  			// if(file_exists($this->file_dir_root_relative.$directory.'/'.$file)){
  			// 	if(unlink($this->file_dir_root_relative.$directory.'/'.$file)){
  			// 		$this->redirect('/admin/files/');
  			// 	};
  			// }
  			die;
  		}

		private function getLast($str){
			// debug($str);
			// debug(strrpos($str,'/'));
			// debug(strlen($str));

			$ar = explode('/', $str);
			$n = count($ar);
			$str2 = '';
			if($n > 1){		
				if($ar[$n-1]==''){
					for ($i=0; $i < $n-2; $i++) { 
						$str2 .= $ar[$i].'/';
					}
				} else {
					for ($i=0; $i < $n-2; $i++) { 
						$str2 .= $ar[$i].'/';
					}
				}
			}
			return $str2;
		}

		
		private function setCommonVars(){
			$this->current_directory = $this->directoryArrayToString($this->params->params['pass'] );
			$this->dir_contents = $this->Fileutil->getDirectoryContents($this->file_dir_root_relative.$this->current_directory);
			//--
			$this->set('file_dir_root_relative',$this->file_dir_root_relative);
			$this->set('file_dir_root_absolute',$this->file_dir_root_absolute);			
			//--
			$this->set('target_directory',$this->current_directory ); //

			$this->set('filepath','/user_managed/'.$this->current_directory);			
			//--
			$this->set('dir_contents',$this->dir_contents);
			$this->set('listing',$this->dir_contents);
			$this->set('root_uri',$this->root_uri);
			//--	
			$ar = $this->params->params['pass'];
			$directoryPathArray = '';			
			//just use unset or whatever...this sucks
			foreach ($ar as $key => $value) {
				$directoryPathArray .= '"'.$value.'"';
				if($key < count( $ar )-1){
					$directoryPathArray .= ',';
				}
			}
			$this->set('directoryPathArray', $directoryPathArray );	
			$this->set('back', $this->root_uri.$this->getLast($this->current_directory));		
			// debug($this->current_directory);
			// debug($this->root_uri);exit;							
			
		}
		



    } 
?>