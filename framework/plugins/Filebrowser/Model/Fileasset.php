<?php
App::uses('MediaValidation', 'Media.Lib');

class Fileasset extends AppModel {
	

	public $useTable = false;
	public $actsAs = array(
		'Filebrowser.Uploadhandler'=>array('allowedExtensions' => array('pdf','epub','png','jpg','jpeg','m4v','mp4','mp3','webm','webmsd.webm','ppt','pptx','pdf','doc','docx','webp'))
	);


	public function saveFile($downloadData=null,$targetDirectory = null, $filetypes=null){
		$filename = $_GET['qqfile'];
		$return = $this->handleUpload(  $targetDirectory,  $filename);			
		return !empty($return['success']);
	}
}

/*


public function saveFile($downloadData=null,$targetDirectory = null, $filetypes=null){
	$filename = $_GET['qqfile'];
	//$baseName = uniqid();
	$return = $this->handleUpload(  $targetDirectory,  $filename);				
	// $this->data = array(
	// 	'Downloadasset'=>array(
	// 		//'id' => $dload['id'],
	// 		'download_id' => $dload['download_id'],
	// 		//'book_id' => $dload['book_id'],
	// 		'modified'=>date('Y-m-d H:i:s'),
	// 		'drm' => $dload['drm'],
	// 		'basename' => $baseName,		
	// 		'file'=> $_GET['qqfile'], //change this to original filename		
	// 		'dirname' => 'book_0'.$dload['book_id'],
	// 		'revision'=>1,
	// 		'description'=>$dload['description'],
	// 	    'filetype' => Mime_Type::guessExtension($_GET['qqfile']),		
	//     	'external_resource' => '',
	//     	'location' => ''	
	// 	)
	// );
	// $target_path = APP.'product_downloads'.DS.'book_0'.$dload['book_id'].DS;
	// $return = $this->handleUpload($target_path,$baseName);
	// error_log('$return: '.print_r($return,1));		
	// if(!empty($return['success'])){
	// 	// error_log('bookasset_saved: '.$baseName);
	// 	// error_log('$this->save: '.print_r($this->save($this->data['Downloadasset']),1));
	// 	// error_log('saved to database: '.print_r($d,1));
	// 	return $this->save($this->data['Downloadasset']);
	// }
	return !empty($return['success']);
}
*/