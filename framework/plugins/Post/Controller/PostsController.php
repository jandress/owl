<?php

App::uses('AppController', 'Controller');

/**
 * Posts Controller
 *
 * @property Post $Post
 */
class PostsController extends AppController {

/**
 *  Layout
 *
 * @var string
 */
//	public $layout = 'frontend';
	var $uses = array('Post.Post', 'Quizmodules.Quiz');
	public $helpers = array('paginator'=>'TwitterBootstrap.BootstrapPaginator','TwitterBootstrap.BootstrapPaginator');



	public $formatListing = array(
		''=>'none'
		,'img'=>'Image'
		,'iframe_flash'=>'Standalone Flash'
		,'modal_content'=>'Launch In Modal'
		,'flash'=>'Flash'
		,'video'=>'video'
		,'quiz'=>'Quiz'
	);

	public $paginate = array(
        'limit' => 30,
        'order' => array(
			// 'Book.title' => 'asc'
        )
		//, 'contain'=> array(
		// 	'Publisher'
		// )
		,'tag' => 'li'
    );



	public $components = array('Session','Fileutil','Search.Prg','Auth');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Post->recursive = 0;
		$g = $this->paginate();
	//	$this->layout = 'fluid';
		$this->set('data', $g);
		$this->handleAjax();
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */


	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('view','raw_view','embedded_view','search','standalone_view');
	}

	public function view($id = null) {
		$this->setNavIdByAssociatedContent('Post',$id);
		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid %s', __('post')));
		}

		$this->data = $post = $this->Post->read(null, $id);


		$this->set('title_for_layout', $this->data['Post']['title']);
		$this->layout  = "Virtuoso.".$this->data['Post']['layout'];
		$this->set('post', $post);
	}

	public function standalone_view($id = null) {
		$this->view($id);
		$this->layout = "ajax";
	}



	public function raw_view($id){
		$this->view($id);
		$this->layout = 'ajax';
		$this->set('ROUTING_PREFIX', 'raw');
	}

	public function embedded_view($id){
		$this->view($id);
		$this->layout = 'embedded';
		$this->set('ROUTING_PREFIX', 'embedded');
	}


	public function search_edit(){
		$this->Prg->commonProcess();
		$this->setDashboardLayout();
		//$this->Post->
		$n = $this->Post->parseCriteria($this->passedArgs);
       	$this->paginate['conditions'] = $n;
    	// $this->set('books', $this->paginate());
		$this->set('data',$this->paginate());
		$this->set('controller','Books' );
	}



	public function search(){
		$this->Prg->commonProcess();
		$n = $this->Post->parseCriteria($this->passedArgs);
       	$this->paginate['conditions'] = $n;
    	// $this->set('books', $this->paginate());
		$this->set('data',$this->paginate());
		$this->set('controller','Books' );
	}


	public function admin_files($g=null){
		$this->handleAjax();
		if(!empty($g)){
			switch ($g) {
				case 'quiz':
					$this->set('element','quizSelect');
					$quiz_list = $this->Quiz->find('list',array(  'order' => array('Quiz.name' => 'ASC')));
					$quizOptions = array(
						'label'=>'Quiz'
						,'options'=>$quiz_list
					);
					$this->set('quizOptions',$quizOptions);
				break;
				case 'modal_content':
					$this->set('element','admin_files');
					$fileListing=$this->Fileutil->getFileListingForOptions(IMAGES.'media'.DS.'iframe_flash', array('fileNameAsKeys'=>true));
					$this->set('fileListing',$fileListing);
				break;


				//iframe_flash
				default:
					$this->set('element','admin_files');
					$fileListing=$this->Fileutil->getFileListingForOptions(IMAGES.'media'.DS.$g, array('fileNameAsKeys'=>true));
					$this->set('fileListing',$fileListing);
				break;
			}
		}else{
			$this->set('fileListing',array() );
		}
	}


	public function iflash($targetDirectory=null){
		$this->layout = 'ajax';
	}

	public function admin_layouts($g=null){
		//recieves the filename of the layout
		// i think this only exists in case
		$this->handleAjax();

		$this->set('formatListing',$this->formatListing );
		// get the image directory by default
		$this->set('fileListing',array() );
//		$this->set('fileListing',$fileListing=$this->Fileutil->getFileListingForOptions(IMAGES.'media'.DS.'img', array('fileNameAsKeys'=>true, 'omitDirectories'=>true)));
	}


	public function admin_index() {
		$this->setDashboardLayout();

		$this->Post->recursive = 0;
		$g = $this->paginate();
		//$this->set('posts', $g);
		$this->set('data', $g);
		$this->handleAjax();
	}

	public function admin_view($id = null) {
		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid %s', __('post')));
		}
		$this->set('post', $this->Post->read(null, $id));
	}



	public function admin_add() {
		$this->set('formatListing',$this->formatListing );
		if ($this->request->is('post')) {
			$this->Post->create();
			$saved = $this->Post->save($this->request->data);
			if ($saved) {
				$this->Session->setFlash(
					__('The %s has been saved', __('post')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$g =  $this->Session->read('CourseEdit.CourseId');
				$this->Session->write('CourseEdit.contentId', $saved['Post']['id']);
				$this->Session->write('CourseEdit.contentType', "Post");
				$this->redirect("/my/sitemap/");
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('post')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$this->title("New Post");
	}





	public function admin_edit($id = null) {
//		$fileListing=$this->Fileutil->getFileListingForOptions(IMAGES.DS.'posts', array('fileNameAsKeys'=>true, 'omitDirectories'=>true));
//		$layoutListing=$this->Fileutil->getFileListingForOptions(IMAGES.DS.'posts', array('fileNameAsKeys'=>true, 'omitDirectories'=>true));
//		$layoutListing=$this->Fileutil->getFileListingForOptions(IMAGES.DS.'posts', array('fileNameAsKeys'=>true, 'omitDirectories'=>true));
		$siteLayouts = APP.DS.'View'.DS.'Layouts'.DS.'posts';
		$s = array('fileNameAsKeys'=>true,'removeFileExtension'=>true, 'omitDirectories'=>true,'allowNullEntry'=>true);

		$layoutListing=$this->Fileutil->getFileListingForOptions($siteLayouts, $s);
			if(
			empty($layoutListing) ||
			(count($layoutListing)<=1)
			){
				$pluginLayouts = CORE_PATH.'../plugins'.DS.'Post'.DS.'View'.DS.'Layouts';
				$layoutListing = $this->Fileutil->getFileListingForOptions($pluginLayouts, $s);
			}
		$this->set('layoutListing',$layoutListing );
		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid %s', __('post')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Post->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('post')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('/my/posts/');
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('post')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Post->read(null, $id);
			$this->title("Editing Post: ".$this->request->data['Post']['title']	);
		}
	//	debug($this->request->data);exit;
		$this->admin_layouts($this->request->data['Post']['layout']);
		// $this->admin_files($this->request->data['Post']['media_format']);
	}

	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid %s', __('post')));
		}
		if ($this->Post->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('post')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect('/my/posts/');
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('post')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect('/my/posts/');
	}
	public function inlineedit($id){

	}
}
