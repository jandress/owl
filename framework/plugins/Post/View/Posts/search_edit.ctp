<?php $alias = 'posts'; ?>

<div class="">
	<div class="navbar-inverse">
		<div class = 'pull-left'>
			<h2 ><?php echo __( __('Posts Search Results'));?></h2>
			<p><?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')));?></p>
		</div>
		<div class = ''>
			<div class="pull-right col-lg-6 tm_2_em">
				<?php echo $this->Form->create('Post', array('url' => '/my/posts/search/edit/')); ?>
				<div class="input-group input-group-sm col-lg-6  pull-right">
					<input name="data[Post][q]" class="form-control bm_1_px search-query" placeholder="Search" type="text" id="PostQ" aria-describedby="sizing-addon3">
					<span class="input-group-btn">
							<input class="btn btn-default" type="button" type="submit" value="Search">
					</span>
				</div>
				<?php echo $this->Form->end(); ?>
				<a href = '/my/posts/add/' class = ' ajax_link btn btn-default btn-sm pull-right' style="margin-right:1em;"><div class="glyphicon glyphicon-plus-sign">&nbsp;</div>New Post&nbsp;</a>
			</div>
		</div>
		<div class = 'clear'></div>
	</div>



	<div class="admin_padding">
		<?php echo $this->BootstrapPaginator->pagination(); ?>

		<table class="table table-striped table-bordered table-hover">
		<tr>
			<th class = "span1" style = "text-align:center" ><?php echo $this->BootstrapPaginator->sort('id');?></th>
			<th><?php echo $this->BootstrapPaginator->sort('title');?></th>
			<th><?php echo $this->BootstrapPaginator->sort('media_format');?></th>
			<th>&nbsp;<?php //echo $this->BootstrapPaginator->sort('media_file');?></th>
		</tr>
		<?php foreach ($data as $post): ?>
		<tr>
			<td class = "" style = "text-align:center" ><?php echo h($post['Post']['id']); ?></td>
			<td>
				<ul style = "list-style:none; padding:0" >
		            <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<?php echo h($post['Post']['title']); ?>
						</a>
						<ul class="dropdown-menu" role="menu" style = "text-decoration:none;">
								<li >
								<a class = "" target = '_blank' href="/<?php echo $alias ?>/view/<?php echo $post['Post']['id']; ?>/">View</a>
						    </li>
							<li >
								<a class = "ajax_link" href="/my/<?php echo $alias ?>/edit/<?php echo $post['Post']['id']; ?>/">Edit</a>
						    </li>
							<li>
								<?php  echo $this->Form->postLink( __('Delete'), '/my/posts/delete/'.$post['Post']['id'] , array($post['Post']['id']) ,__('Are you sure you want to delete # %s?', $post['Post']['title']) ); ?>
						    </li>

			                <li role="presentation" class="divider"></li>



							<li>
								<a target = "_blank" href = '/standalone/posts/view/<?php echo $post['Post']['id'] ?>'>View as Standalone</a>
		                    </li>




						</ul>
		            </li>
	         	</ul>
			</td>
			<td><?php //echo h($post['Post']['media_format']); ?>&nbsp;</td>
			<td><?php //echo h($post['Post']['media_file']); ?>&nbsp;</td>





		</tr>
		<?php endforeach; ?>
		</table>
		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
</div>
