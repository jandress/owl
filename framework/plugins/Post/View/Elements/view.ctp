<div>

	<div class="col-sm-6" style = "padding:0;">
	<h1><?php echo __($post['Post']['title']); ?></h1>	
	</div>			
	
	<div class = 'tm_2_em bm_2_em col-sm-3 pull_right' style = "text-align:right" id = "prev_next" >
			<?php 
				$style = 'btn-default';
				$nData = $this->TreeNav->children($leftNavlinks, !empty($is_manager),$selected_nav_parents); ?>
				<a href="<?php echo $nData['prev'] ?>" class = "btn btn-sm <?php echo $style; ?>">
					<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
				</a>
				<a href="<?php echo $nData['next'] ?>" class = "btn btn-sm <?php echo $style; ?>">
					<span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
				</a>
			</div>		

			<div class = "clearfix" style = 'height:1px;'>&nbsp;</div>		
			<?php echo __($post['Post']['body']); ?>		
			<div class = "clearfix tm_3_em">&nbsp;</div>

	</div>