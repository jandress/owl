<div>
	<div class="navbar-inverse">
		<h1><?php  echo __($title_for_layout); ?></h1>		
	</div>
	<div class = "admin_padding tm_2_em col-md-10">
		<?php echo $this->BootstrapForm->create('Post', array('class' => ''));?>
		<fieldset>
			<?php
				echo $this->BootstrapForm->input('title',array('class'=>'col-md-12'));
				echo '<div class = "clearfix bm_2_em">&nbsp;</div>';
			?>
			<div id="ajax_format">
				<?php //echo $this->element('ajax/admin_layouts'); ?>
			</div>
			<div class = "clear bm_1_em">&nbsp;</div>			
			




			<?php

				$layout_options = array(
					'options'=>array(
						'horizontal'=>'horizontal',
						'vert'=>'vertical',
						'split_horiz'=>'split_horiz',
						'split_horiz2'=>'split_horiz2',
						'split_vert'=>'split_vert',
						'split_vert2'=>'split_vert2'
					)
				);	
				echo $this->BootstrapForm->input('layout',$layout_options);
				?>
				<div class = "clear tm_1_em">&nbsp;</div>			
				<?php
				echo $this->BootstrapForm->input('body',array('class'=>'col-md-12 ckeditor', 'style'=>"height:450px;"));
				?> <div class = "clear bm_2_em"></div> <?php
				echo $this->BootstrapForm->Label("Json");
				echo $this->BootstrapForm->textArea('json',array('class'=>'col-md-12 ', 'style'=>"height:450px;"));
				?> <div class = "clear bm_2_em"></div> <?php
				echo $this->BootstrapForm->Label("Scripts");
				echo $this->BootstrapForm->textArea('scripts',array('label'=>'horse','class'=>'col-md-12 ', 'style'=>"height:450px;"));
				?> <div class = "clear bm_2_em"></div> <?php
				echo $this->BootstrapForm->Label("Ui");
				echo $this->BootstrapForm->textArea('ui',array('class'=>'col-md-12 ', 'style'=>"height:450px;"));

				echo $this->BootstrapForm->hidden('id');
			?>
			<div class = "clear bm_1_em"></div>
			<?php echo $this->BootstrapForm->submit(__('Submit'),array('div'=>'pull-right'));?>
		</fieldset>
		<?php echo $this->BootstrapForm->end();?>	
	</div>
</div>