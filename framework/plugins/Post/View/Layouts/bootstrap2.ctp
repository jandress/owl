<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo __($title_for_layout); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
	<?php
		if(!isset($concat_assets)){
			$concat_assets = true;
		}
		if(empty($cssBuild)){
			$cssBuild = 'common';
		}
	 	echo($this->AssetCompress->css($cssBuild, array('raw' => $concat_assets))."\n");
 	?>
	<script type="text/javascript" src = '/js/jquery.js'></script>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="/ico/apple-touch-icon-57-precomposed.png">
	<link rel="shortcut icon" href="/ico/favicon.png">
  </head>

  <body>



    <!-- Part 1: Wrap all page content here -->
    <div id="wrap">

      <!-- Begin page content -->
      <div class="container">
		<?php echo($this->element('navigation/top_nav')); ?>		
        <div class="row-fluid content">
			<div class="span3">
				<?php  echo $this->TreeNav->sitemap($navlinks, !empty($isAdmin)); ?>				
				<div class = "clearfix tm_3_em">&nbsp;</div>
			</div>
			<div class="span9">
				<?php echo $content_for_layout; ?>
			</div>
      	</div><!--/row-->
      </div>

      <div id="push"></div>
    </div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
	<?php echo $this->element('global/footer');?>	




  </body>
</html>
