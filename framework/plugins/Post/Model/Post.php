<?php
App::uses('AppModel', 'Model');
/**
 * Post Model
 *
 */
class Post extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';
	public $useTable ="posts";
	public $editAction = '/my/posts/edit/';
	public $viewAction = '/posts/view/';	
    var $actsAs = array('Search.Searchable', 'Polymorphic','Containable'); 			
	// var $filterQs='q';
	
	
	public $belongsTo = array(
	        'Quiz' => array(
	            'className'  => 'Quiz',
	            'conditions'   => array('Post.media_format' => 'quiz'),
	        )
	    );
	
	public $filterArgs = array(
		'q' => array(
			'type' => 'like', 
			// 'encode' => true, 
			// 'before' => false, 
			// 'after' => false, 
			'field' => array(
				'Post.title',
				'Post.body'
				)
			)
		//'q' => array('type' => 'subquery', 'method' => 'findByCategory', 'field' => 'Book.id')
        //'copy' => array('type' => 'like', 'field' => 'Book.copy'),
    );
	
}
