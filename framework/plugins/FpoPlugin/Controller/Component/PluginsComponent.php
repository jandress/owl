<?php
	class PluginsComponent extends Component {

		public function test() {
				debug('');			
				pr('------------------------------------------------------');
				debug($this->getPluginControllerNames());						
				pr('------------------------------------------------------');
				//so, it looks like this method gets a list of all controllers, and plugins, joins them together and then 
				// does various actions to both, 
				// get list of controllers..

				//  $Controllers = array_merge($Controllers, $PluginNames);
				// debug($Controllers);
				// look at each controller in app/controllers
				//        $methods = $this->_getClassMethods($this->_getPluginControllerPath($ctrlName));
				//debug($ctrlName.' is a plugin');
				//      $pluginNode = $aco->node('controllers/' . $this->_getPluginName($ctrlName));
				//$aco->create(array('alias' => $this->_getPluginControllerName($ctrlName));
				//$aco->create(array( 'alias' => $this->_getPluginName($ctrlName));
				//$log[] = 'Created Aco node for ' . $this->_getPluginName($ctrlName) . ' Plugin';
		}

		public function getControllerNames(){
		    App::uses('File', 'Utility');			
		    $ControllersFresh = App::objects('Controller');			
			$Controllers = array();
		    foreach ($ControllersFresh as $cnt) {
		        $Controllers[] = str_replace('Controller', '', $cnt);
		    }
			return $Controllers;
		}
		
		public function getPluginControllerNames(){
		    $Plugins = $this->_getPluginControllerNames();
		    $PluginNames = array();
		    foreach ($Plugins as $cnt) {
		        $PluginNames[] = str_replace('Controller', '', $cnt);
		    }
			pr($PluginNames[0]);	
			debug($this->_getPluginControllerName($PluginNames[0],'.') );
			return $PluginNames;
		}



		function _getClassMethods($ctrlName = null) {
			//
			pr($ctrlName);
		    if($this->_isPlugin($ctrlName)){
		        App::uses($this->_getPluginControllerName ($ctrlName), $this->_getPluginName ($ctrlName). 'Controller');
		    }
		    else
		        App::uses($ctrlName . 'Controller', 'Controller');
		    if (strlen(strstr($ctrlName, '.')) > 0) {
		        // plugin's controller
		        $ctrlName = str_replace('Controller', '', $this->_getPluginControllerName ($ctrlName));
		    }
		    $ctrlclass = $ctrlName . 'Controller';
		    $methods = get_class_methods($ctrlclass);
		    // Add scaffold defaults if scaffolds are being used
		    $properties = get_class_vars($ctrlclass);
			$properties['name'] = empty($properties['name']) ? $ctrlclass : empty($properties['name']); 
			pr($ctrlName);
			pr($ctrlclass);
		//	debug($methods);
			$properties['methods']=$methods;
			//debug($properties);
		    // if (array_key_exists('scaffold', $properties)) {
		    //     if ($properties['scaffold'] == 'admin') {
		    //         $methods = array_merge($methods, array('admin_add', 'admin_edit', 'admin_index', 'admin_view', 'admin_delete'));
		    //     } else {
		    //         $methods = array_merge($methods, array('add', 'edit', 'index', 'view', 'delete'));
		    //     }
		    // }
		    return $methods;
		}

		function _isPlugin($ctrlName = null) {
			// determines whether or not it is a plugin based on whether the input string is in the form 'pluginname.controllername'
		    $arr = String::tokenize($ctrlName, '.');
		    if (count($arr) > 1) {
		        return true;
		    } else {
		        return false;
		    }
		}

		function _getPluginControllerPath($ctrlName = null, $token='/') {
			//accepts directory string (pluginname/controllername). splits string by $token, returns either pluginname.controllername or just pluginname (or potentially controller name i guess)
		    $arr = String::tokenize($ctrlName, $token);
		    if (count($arr) == 2) {
		        return $arr[0] . '.' . $arr[1];
		    } else {
		        return $arr[0];
		    }
		}

		function _getPluginName($ctrlName = null,$token='.') {
			// splits a pluginname.controllername string by '.', returns first part.
		    $arr = String::tokenize($ctrlName, $token);
		    if (count($arr) == 2) {
		        return $arr[0];
		    } else {
		        return false;
		    }
		}

		function _getPluginControllerName($ctrlName = null, $token='/') {
			//accepts, pluginname.controllername.  splits string by $token, returns second part.
		    $arr = String::tokenize($ctrlName, $token);
		    if (count($arr) == 2) {
		        return $arr[1];
		    } else {
		        return false;
		    }
		}

		/**
		 * Get the names of the plugin controllers ...
		 *
		 * This function will get an array of the plugin controller names, and
		 * also makes sure the controllers are available for us to get the
		 * method names by doing an App::import for each plugin controller.
		 *
		 * @return array of plugin names.
		 *
		 *
		 */
		function _getPluginControllerNames() {
		    App::uses('Folder', 'Utility');
		    $folder = & new Folder();
		    $folder->cd(APP . 'Plugin');

		    // Get the list of plugins
		    $Plugins = $folder->read();
		    $Plugins = $Plugins[0];
		    $arr = array();

		    // Loop through the plugins
		    foreach ($Plugins as $pluginName) {
		        // Change directory to the plugin
		        $didCD = $folder->cd(APP . 'Plugin' . DS . $pluginName . DS . 'Controller');
		        if ($didCD) {
		            // Get a list of the files that have a file name that ends
		            // with controller.php
		            $files = $folder->findRecursive('.*Controller\.php');

		            // Loop through the controllers we found in the plugins directory
		            foreach ($files as $fileName) {
		                // Get the base file name
		                $file = basename($fileName);
		                // Get the controller name
		                //$file = Inflector::camelize(substr($file, 0, strlen($file) - strlen('Controller.php')));
		                if (!preg_match('/^' . Inflector::humanize($pluginName) . 'App/', $file)) {
		                    $file = str_replace('.php', '', $file);

		                    /// Now prepend the Plugin name ...
		                    // This is required to allow us to fetch the method names.
		                    $arr[] = Inflector::humanize($pluginName) . "." . $file;
		                }

		            }
		        }
		    }
		    return $arr;
		}	
		
		
		
		
		
		
		
	}
?>