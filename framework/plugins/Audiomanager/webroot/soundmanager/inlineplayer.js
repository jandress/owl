function InlinePlayer() {
	
	var _encapsulation = {
		
		links:[],
		sounds: [],
		soundsByURL: [],
		indexByURL: [],
		lastSound: null,
		soundCount: 0,
	
		showPlayIcon: function(val){

			$(val).children('i').removeClass('icon-pause');		
			$(val).children('i').addClass('icon-play');
		},
		showPauseIcon: function(val){
			$(val).children('i').addClass('icon-pause');
			$(val).children('i').removeClass('icon-play');		
		},
		showLoadingIcon: function(){
			$(val).children('i').addClass('icon-refresh');
			$(val).children('i').removeClass('icon-pause');		
			$(val).children('i').removeClass('icon-play');		
		},
		showErrorState: function(){
			$(val).addClass('btn-danger btn-disabled');
			$(val).children('i').removeClass('icon-play');		
			$(val).children('i').removeClass('icon-pause');		
			$(val).children('i').removeClass('icon-play');
		},
		stopSound: function(oSound) {
		    soundManager.stopAll();
		    soundManager.unload();
	  	},
	  
		handleClick: function(a){
			var oLinks = _encapsulation.links;			
				for (var i=0, j=oLinks.length; i<j; i++) {
					_encapsulation.showPlayIcon(oLinks[i]);						
				}			
			_encapsulation.handleClickAudio(a);			
			return false;
		},		
		getSoundByURL:function(sURL) {
			// if it doesnt == undefined, return it. else return null.
			//console.log('getSoundByURL: '+sURL);
	    	return (typeof _encapsulation.soundsByURL[sURL] != 'undefined' ? _encapsulation.soundsByURL[sURL] : null);
	  	},
		
		handleClickAudio: function(a){
				var sURL = a.getAttribute('href');
				sURL = '/media/audio/'+sURL;
				var soundURL = sURL;
			    var thisSound = _encapsulation.getSoundByURL(soundURL);				
				sm = soundManager;
				if (_encapsulation.lastSound) {
					// sound is playing, stop it. 					
				  	_encapsulation.stopSound(_encapsulation.lastSound);
					if(_encapsulation.lastSound == thisSound){
						_encapsulation.lastSound = null;
						return;
					}
				}
			    if (!thisSound) {
					// sound has not been played, create the sound
					thisSound = sm.createSound({ id:'inlineMP3Sound'+(soundURL), url:soundURL,type:(a.type||null) });
					_encapsulation.soundsByURL[soundURL] = thisSound;
					_encapsulation.sounds.push(thisSound);					
					_encapsulation.lastSound = thisSound;
					thisSound.play();
					_encapsulation.showPauseIcon(a);					
			    } else {				
					thisSound.play();
					_encapsulation.showPauseIcon(a);	
					_encapsulation.lastSound = thisSound;								
			    }			
			
		},
		
		
		init: function(){
		    var oLinks = $("a");
			var foundItems = 0;
			_encapsulation.links = []
			for (var i=0, j=oLinks.length; i<j; i++) {

				_encapsulation.stopSound();									
				if(soundManager.canPlayLink(oLinks[i])){
					if($(oLinks[i]).attr('href').indexOf("mp3") !== -1){
						_encapsulation.showPlayIcon(oLinks[i]);							
						if(!($(oLinks[i]).hasClass("btn"))){ $(oLinks[i]).addClass("btn audio_btn");}
						if(!($(oLinks[i]).hasClass("btn-primary"))){ $(oLinks[i]).addClass("btn-primary");}

						if(!$(oLinks[i]).children('i').hasClass('icon-play')){
							$(oLinks[i]).prepend( "<i class='icon-white icon-play'>&nbsp;</i>&nbsp;&nbsp;" );									
						}
						_encapsulation.links.push(oLinks[i]);
						var a = oLinks[i];
						$(oLinks[i]).click(function(){
							try{
								_encapsulation.handleClick(this);
							}catch(err){
								//console.log(err);
							}
							return false;
						});
					}
				}
			}
		},
	}
	_encapsulation.init()
	return _encapsulation;
}


var inlinePlayer = null;
inlinePlayer = InlinePlayer();