<?php
App::uses('AppController', 'Controller');
/**
 * Multichoices Controller
 *
 * @property Multichoice $Multichoice
 */
class MultichoicesController extends AppController {

/**
 *  Layout
 *
 * @var string
 */
/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
	public $uses = array('Quizmodules.Multichoice','Multichoiceanswer'=>'Quizmodules.Multichoiceanswer','Quizslide'=>'Quizmodules.Quizslide');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session','Auth','Quizmodules.QAdminbreadcrumb');
/**
 * index method
 *
 * @return void
 */

	var $defaultRedirect = '/my/multichoice/';

	public function beforeFilter(){
		parent::beforeFilter();
		//~ security
		$this->Auth->allow('view','checkanswer','admin_multihoiceanswers');
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Multichoice->recursive = 1;
		$p = $this->paginate();
		$this->set('multichoices', $p);
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->view($id);
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Multichoice->create();
			if ($saved && $multichoice_answer) {
				$this->Session->setFlash(
					__('The %s has been saved', __('multichoice')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect($this->defaultRedirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('multichoice')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$multichoiceanswers = $this->Multichoice->Multichoiceanswer->find('list');
		$this->set(compact('multichoiceanswers'));
	}


/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */

	public function admin_edit($id = null) {
		$this->Multichoice->id = $id;
		if (!$this->Multichoice->exists()) {
			throw new NotFoundException(__('Invalid %s', __('multichoice')));
		}

		$this->set('breadcrumbs' , $this->QAdminbreadcrumb->slideTypeEdit('Multichoice',$id));
		if ($this->request->is('post') || $this->request->is('put')) {
			$delConditions = array(
				'multichoice_id'=>$this->request->data['Multichoice']['id']
			);
			if (
				//$this->Multichoice->Multichoiceanswer->deleteAll($delConditions) &&
				$this->Multichoice->saveAll($this->request->data) //&&
				//$this->Multichoice->Multichoiceanswer->saveAll($this->request->data)
			){
				$this->Session->setFlash(
					__('The %s has been saved', __('multichoice')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				// $b=$this->Quizslide->updateResponseTime($this->request->data['Quizslide']['response_delay'],'Multichoice',$this->request->data['Multichoice']['id']);
				//debug($this->request->data); exit;
				if($this->checkAjax()){
					echo '{"href":"/my/multichoices/answers/'.$id.'"}';
					exit;
				}else{
					$this->redirect('/my/multichoices/answers/'.$id);
				}
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('multichoice')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Multichoice->read(null, $id);
		}
		$multichoiceanswers = $this->Multichoice->Multichoiceanswer->find('list');
		$this->set(compact('multichoiceanswers'));
	}


/*
<pre>Array
(
    [Post] => Array
        (
            [q] =>
        )

    [Multichoice] => Array
        (
            [multichoiceanswer_id] => 0
        )

    [Multichoiceanswer] => Array
        (
            [0] => Array
                (
                    [id] => 63
                    [multichoice_id] => 10
                    [name] => <p>In his book Bobby Fischer Teaches Chess, chess champion Bobby Fisher (1966) explains the object of chess for beginners. He writes, &quot;The object of Chess is to attack the enemy King in such a way that it cannot escape capture&quot; (p. 20). Since beginning chess players have a tendency to initiate a game without strategy in mind, Fischer&#39;s explanation provides important guidance to help them focus.</p>

                )

            [1] => Array
                (
                    [id] => 64
                    [multichoice_id] => 10
                    [name] => <p>In his book, Bobby Fisher (1966) writes, &quot;The object of Chess is to attack the enemy Kind in such a way that it cannot escape capture&quot; (p. 20). This quote speaks for itself for people who are just learning to play Chess.</p>

                )

            [2] => Array
                (
                    [id] =>
                    [multichoice_id] => 10
                    [name] => Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                )

        )

)
</pre>
*/



//ok, so this is the post data from jquery.
// you cant make the third one the correct answer on first creation because thre is no id.
// so basically you need so save the answers, get the third one, from [Multichoice][multichoiceanswer_id]
//this->Multichoice->saveAnswer
// then






	public function admin_addrow($mc_id, $key){
		$this->Multichoiceanswer->create();
		$a  = $this->Multichoiceanswer->save(array('multichoice_id'=>$mc_id));
		$answer = $a['Multichoiceanswer'];



		$answer['p']='poo';
		$answer['multichoice_id'] = $mc_id;
		$this->set('multichoice_id' , $mc_id);
		$this->set('answer' , $answer);
		$this->set('key', $key);
		$this->set('id', $answer['id']);
	}







	public function admin_answers($id = null) {

		$this->Multichoice->id = $id;

		if (!$this->Multichoice->exists()) {
			throw new NotFoundException(__('Invalid %s', __('multichoice')));
		}

		$d = $this->Multichoice->read();
		if ($this->request->is('post') || $this->request->is('put')) {
			foreach ($this->request->data['Multichoiceanswer'] as $key => $value) {
				$ar[$key] = $value['id'];
			}
			$delConditions = array( 'Multichoiceanswer.multichoice_id'=>$id);
			if(empty($this->request->data['Multichoice']['multichoiceanswer_id'])){
				$i=0;
			}else{
				  	$i = $this->request->data['Multichoice']['multichoiceanswer_id'];
			}

			if($i > count($this->request->data['Multichoiceanswer'])-1){
				$i=0;
			}
			//pr($this->request->data); exit;
			// delete all multichoices for this multichoice, resave them & any new ones & save the correct answer on the multichoice table.
			//ok so this is dangerous.  because of this whole delete, resave situation,
			// ok, so you need to deal with when you delete the "correct" item.  it causes all answer to delete
			if (
				$this->Multichoiceanswer->deleteAll($delConditions)

				&& $this->Multichoice->saveAnswer($id, $this->request->data['Multichoiceanswer'][$i]['id'] )
				&& // pr(
						$this->Multichoiceanswer->saveAll($this->request->data['Multichoiceanswer'])
					//)
			){


				//exit;
				if($this->checkAjax()){
					echo '{"href":"'.'/my/quizslides/'.$d['Quizslide']['quiz_id'].'"}';
					exit;
				}else{
					$this->Session->setFlash(
						__('The %s has been saved', __('multichoice')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);
					$this->redirect('/my/quizslides/'.$d['Quizslide']['quiz_id']);
				}
			} else {

				if($this->checkAjax()){
					echo '{"href":"'.'/my/quizslides/'.$d['Quizslide']['quiz_id'].'"}';
				}else{
					$this->Session->setFlash(
						__('The %s could not be saved. Please, try again.', __('multichoice')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-error'
						)
					);
					$this->redirect('/my/quizslides/'.$d['Quizslide']['quiz_id']);
				}
				exit;
			}
		} else {
			$this->request->data = $this->Multichoice->read(null, $id);
		}
		$multichoiceanswers = $this->Multichoice->Multichoiceanswer->find('list');
		$this->set(compact('multichoiceanswers'));
	}


/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Multichoice->id = $id;
		if (!$this->Multichoice->exists()) {
			throw new NotFoundException(__('Invalid %s', __('multichoice')));
		}
		if ($this->Multichoice->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('multichoice')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->defaultRedirect);
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('multichoice')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->defaultRedirect);
	}
}
