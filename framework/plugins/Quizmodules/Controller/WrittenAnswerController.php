<?php
App::uses('AppController', 'Controller');
//App::uses('QuizmodulesController', 'Controller');
require_once('QuizzesController.php');
/**
 * Writtenanswers Controller
 *
 * @property Writtenanswer $Writtenanswer
 */
class WrittenanswerController extends QuizzesController {

/**
 *  Layout
 *
 * @var string
 */
	public $layout = 'frontend';
/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'Form'=>'TwitterBootstrap.BootstrapForm','TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
	public $uses = array('Writtenanswer');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session','Auth','Quizmodules.QAdminbreadcrumb');
	public $defaultRedirect = '/my/written/';
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Writtenanswer->recursive = 0;
		$this->set('writtenanswers', $this->paginate());
	}
	
	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('view','checkanswer');
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */

	public function checkanswer($questionId,$answer){
		$this->layout='ajax';
		$this->Writtenanswer->id = $questionId;		
		$data =$this->Writtenanswer->read();
		echo $data['Writtenanswer']['answer_text'] == $answer;
	}

	public function view($id = null) {
		$this->Writtenanswer->id = $id;
		if (!$this->Writtenanswer->exists()) {
			throw new NotFoundException(__('Invalid %s', __('writtenanswer')));
		}
		$neighbors = $this->Writtenanswer->find('neighbors');
		$prev = empty($neighbors['prev']['Writtenanswer']['id']) ? 'first' : $neighbors['prev']['Writtenanswer']['id'];
		$next = empty($neighbors['next']['Writtenanswer']['id']) ? 'last' : $neighbors['next']['Writtenanswer']['id'];
		$this->set(compact('prev','next'));
		$writtenanswer = $this->Writtenanswer->read(null, $id);
		$this->set('writtenanswer', $writtenanswer);
 
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->set('correct_submission',$writtenanswer['Writtenanswer']['answer_text']==$this->request->data['Writtenanswer']['answer_text']);
		}
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Writtenanswer->recursive = 0;
		$this->set('writtenanswers', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Writtenanswer->id = $id;
		if (!$this->Writtenanswer->exists()) {
			throw new NotFoundException(__('Invalid %s', __('writtenanswer')));
		}
		$this->set('writtenanswer', $this->Writtenanswer->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Writtenanswer->create();
			if ($this->Writtenanswer->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('writtenanswer')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect($this->defaultRedirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('writtenanswer')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		// $fibAnswers = $this->Writtenanswer->find('list');
		// $this->set(compact('fibAnswers'));
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Writtenanswer->id = $id;
		if (!$this->Writtenanswer->exists()) {
			throw new NotFoundException(__('Invalid %s', __('writtenanswer')));
		}


		$this->set('breadcrumbs' , $this->QAdminbreadcrumb->slideTypeEdit('Writtenanswer',$id)); 							
		


		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Writtenanswer->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('writtenanswer')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				
				if($this->checkAjax()){
					echo $this->defaultRedirect;//'/my/multichoices/answers/'.$id;
					exit;
				}else{
					$this->redirect($this->defaultRedirect);
				}
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('writtenanswer')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
				return false;
				exit; 
			}
		} else {
			$this->request->data = $this->Writtenanswer->read(null, $id);
		}
		// $fibAnswers = $this->Writtenanswer->FibAnswer->find('list');
		// $this->set(compact('fibAnswers'));
	}
	


/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Writtenanswer->id = $id;
		if (!$this->Writtenanswer->exists()) {
			throw new NotFoundException(__('Invalid %s', __('writtenanswer')));
		}
		if ($this->Writtenanswer->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('writtenanswer')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->defaultRedirect);
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('writtenanswer')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->defaultRedirect);
	}
}
