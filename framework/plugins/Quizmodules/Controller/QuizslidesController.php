<?php
class QuizslidesController extends QuizmodulesAppController {


		//public $layout = 'frontend';
		public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator','Quizmodules.Breadcrumbs');
		public $components = array('Session','Quizmodules.QAdminbreadcrumb','Auth');


		public $slide_options; 

		var $uses = array(
			'Quizmodules.Quizslide'
			,'Quizmodules.Quiz'
			,'Quizmodules.Multichoiceanswer'
			,'Quizmodules.Quizresponse'
			,'Quizmodules.Wordselect'
			,'Quizmodules.Quiztextslide'

		);

		var $paginate = array('limit'=>200);


		public function beforeFilter(){
			parent::beforeFilter();
			$this->Auth->allow('view','admin_view');
			$this->slide_options = $this->Quizslide->slide_options;
			//$this->QAdminbreadcrumb->doTest();
			//debug($this->Quizslide->slide_options); exit;
		}



		public function getElementAsAjax($element=null,$element2=null,$paramString=null){
			$this->layout = 'ajax';
			$serializedParams = $this->params['named'];
			if(!empty($element) && !empty($element2)){
				$this->set('element',$element.'/'.$element2);
				$this->set('elementParameters',$serializedParams);
			}
		}

		function slideeditfields($type){
			$this->set('element',$type);
			$serializedParams = $this->params['named'];
			$this->layout = 'ajax';
			$this->set('elementParameters',$serializedParams);
		}


		public function admin_delete($quizslide_id = null) {
			$this->Quizslide->id = $quizslide_id;
			$this->data = $this->Quizslide->read();
//			debug($this->data); 
//			$this->setDashboardLayout();
			$this->setDashboardLayout();
			if (!$this->Quizslide->exists()) {
				throw new NotFoundException(__('Invalid %s', __('Quizslide')));
			}
			if ($this->request->is('post') || $this->request->is('put')) {
				if($this->Quizslide->delete($this->data['Quizslide']['id'])){
					//$this->redirect('/my/quizslides/'.$this->data['Quiz']['id']);
					$this->redirect( '/my/quizslides/'.$this->data['Quiz']['id'] );
				}
			}
		}



		public function list_all(){
			$this->data = $this->paginate('Quizslide');
			$this->set('quizslides', $this->paginate('Quizslide'));
		}

		public function admin_index($id = null){
			$this->setDashboardLayout();
			$this->set('breadcrumbs' , $this->QAdminbreadcrumb->quizslidelisting($id));
			///debug($this->paginate('Quizslide')); exit;
			if(isset($id)){
				$this->Quiz->id = $id;
				if (!$this->Quiz->exists()) {
					throw new NotFoundException(__('Invalid %s', __('quiz')));
				}
				$this->data = $this->Quiz->read(null, $id);
				// $this->paginate = array(
				//     'conditions' => array('Quizslide.quiz_id'=>$id)
				// );
				$this->paginate = array('order'=>'order','conditions'=>array('Quizslide.quiz_id'=>$id));
			}else{
					$this->set('list_all',true );
					$this->paginate = array();
			}
			//$this->Quizslide->contain('')
			$this->set('quizslides', $this->paginate('Quizslide'));
			$this->handleAjax();
		}



		public function view($id = null){
			$this->Quizslide->id = $id;
			if (!$this->Quizslide->exists()) {
				throw new NotFoundException(__('Invalid %s', __('quiz')));
			}
			$data = $this->Quizslide->read(null, $id);
			//debug($data); exit;
			//--
			$class = $data['Quizslide']['class'];
			$foreign_id = $data['Quizslide']['foreign_id'];
			//
			$this->loadPolyMorphicAssociations($class, $foreign_id); // do not like how this goes off to do its thing. whatever thatis.
			//I wonder if all this is necessary.....
			$this->addAdditionalScript('Quizmodules.quizslides_view');
			$prev_and_next = $this->_getPreviousAndNext($data['Quizslide']['quiz_id'], $data['Quizslide']['id']);
			
			$this->set('quizslides', $prev_and_next['quizslides']);
			$this->set('id_array', $prev_and_next['id_array']);
			//debug($prev_and_next);// exit;
			$this->set('prev', $prev_and_next['prev']);
			$this->set('next', $prev_and_next['next']);
			$this->set('cookie_data', $this->Quizresponse->getQuizData());
			// parsing post data to check answer...

			if ($this->request->is('post') || $this->request->is('put')) {

				$related_data = $this->data[$this->data['Quizslide']['class']];

				//--
				$Checkanswer = $this->Quizslide->checkAnswer($this->data);
				//--
				$this->Session->setFlash(
										$Checkanswer['message'],
										//print_r($Checkanswer,true),
										'alert',
										array(
											'plugin' => 'TwitterBootstrap',
											'class' =>  $Checkanswer['correct'] ? 'alert-success' : 'alert-error'
											,'delay'=>$data['Quizslide']['response_delay']*1000
										)
									);

				if(empty($prev_and_next['next'])){
					echo '{"href":"/quizzes/end/"}';
				}else{
					echo '{"href":"/quizslides/view/'.$prev_and_next['next']['Quizslide']['id'].'"}';
				}
				exit;
			}else{
				$this->data = $data;
				return;
			}
		}



		private function _getPreviousAndNext($quiz_id, $quizslide_id){
			$last = null;
			$next = null;
			$id_array_str = '';
			$conditions = array('quiz_id'=>$quiz_id);
			$qs = $this->Quizslide->find('all', array('conditions' => $conditions,'order'=>'Quizslide.order','fields'=>array('id','name','quiz_id','order')));
//			$qs = $this->Quizslide->find('all', array('conditions' => $conditions,'order'=>'Quizslide.order'));

			foreach ($qs as $key => $value){
				$id_array_str .= $value['Quizslide']['id'];
				$id_array_str .= $key < count($qs)-1 ? ',' : '';
				if($value['Quizslide']['id'] == $quizslide_id){
					if($key<count($qs)-1){
						$next = $qs[$key+1];
					}
					$prev = $last;
				}
				$last = $value;
			}
			return array(
					'id_array' => $id_array_str,
					'prev' => $prev,
					'next' => $next,
					'quizslides' => $qs
				);
		}


		private function loadPolyMorphicAssociations($class, $foreign_id){
				// dynamically load  polymorphic associated class
				$associated_class = ClassRegistry::init($class);
				if(!empty($associated_class->hasMany)){
					// dynamically load associations of polymorphic classes b/c they are lost throught the polymorphic behavior
					foreach ($associated_class->hasMany as $key => $value) {
						$associated_class_assotiation = ClassRegistry::init($key);
						$associated_class_assotiation->contain();
						$cond = array(
							'conditions'=> array($key.'.'.$value['foreignKey'].' = '.$foreign_id)
						);
						
						$b = $associated_class_assotiation->find('all',$cond);


						$this->set($key,$b);
						// debug($key);
						// debug($b);

						$this->set(strtolower($key),$b);
					}
				}

		}

		// public function admin_relocate($id = null,$direction=null) {
		// 	$this->layout = 'ajax';
		// 	$p = $this->Quizslide->find('first', array( 'conditions' => array( 'Quizslide.id'=> $id )));
		// 	$r = $this->Quizslide->find('all',array( 'conditions'=>array('Quizslide.quiz_id'=>$p['Quizslide']['quiz_id']),'fields' => array('id','quiz_id','order','name'),'order'=>array('order','id') ));
		// 	$d = $r;
		// 	switch($direction){
		// 		case 'up':
		// 		foreach ($d as $key => $value) {
		// 			if($d[$key]['Quizslide']['id']==$id){
		// 				continue;
		// 			}
		// 			$d[$key]['Quizslide']['order']="$key";
		// 			if($key < count($d)-1){
		// 				$o = array(
		// 					'this'=>$value,
		// 					'next'=>$d[$key+1]
		// 				);
		// 				if($o['next']['Quizslide']['id']==$id){
		// 					$t = $key+1;
		// 					$d[$key]['Quizslide']['order']= "$t";
		// 					$d[$key+1]['Quizslide']['order']="$key";
		// 				}
		// 			}
		// 		}
		// 		break;
		// 		case 'down':
		// 		foreach ($d as $key => $value) {
		// 				if($key >= 1){
		// 					if($d[$key-1]['Quizslide']['id']==$id){
		// 						continue;
		// 					}
		// 				}
		// 				$d[$key]['Quizslide']['order']="$key";
		// 				//loop through results reassigning order.
		// 				if($key < count($d)-1){
		// 					$o = array(
		// 						'this'=>$value,
		// 						'next'=>$d[$key+1]
		// 					);
		// 					if($o['this']['Quizslide']['id'] == $id){
		// 						$t = $key+1;
		// 						$p = $key;
		// 						$d[$key]['Quizslide']['order']= "$t";
		// 						$d[$key+1]['Quizslide']['order']="$p";
		// 					}
		// 				}
		// 		}
		// 		break;
		// 	}
		// 	$response = $this->Quizslide->saveAll($d);
		// 	$this->set('reponse', $response);
		// 	return $response;
		// }




			public function moveup($id = null){
				$this->Quizslide->contain();
				$g = $this->Quizslide->find('first',array('conditions'=>array('id'=>$id)));
				$b = $this->Quizslide->moveup($id);

				if(!empty($id)){
					$this->paginate = array('order'=>'order','conditions'=>array('Quizslide.quiz_id'=>$g['Quizslide']['quiz_id']));
				}
				$this->set('quizslides', $this->paginate());
			}


			public function movedown($id = null){
				$this->Quizslide->contain();
				$g = $this->Quizslide->find('first',array('conditions'=>array('id'=>$id)));
				$b = $this->Quizslide->movedown($id);
				if(!empty($id)){
					$this->paginate = array('order'=>'order','conditions'=>array('Quizslide.quiz_id'=>$g['Quizslide']['quiz_id']));
				}
				$this->set('quizslides', $this->paginate());
			}


		public function admin_add($quiz_id = null) {
			$this->set('slide_options',$this->slide_options );
			$this->setDashboardLayout();
			$this->set('breadcrumbs' , $this->QAdminbreadcrumb->quizslidelisting($quiz_id));
			if ($this->request->is('post') || $this->request->is('put')) {
				//---------------------------------------------------------------------------
						// debug('wtf');
						// $this->Quiztextslide->create();
						// $u = array(
						// 	'name'=>'',
						// 	'text'=>'',
						// 	'subhead'=>''
						// );
						// //debug($this->Quiztextslide->beforeSave());
						// debug($this->Quiztextslide->save($u));
				//---------------------------------------------------------------------------
				$quizslidedata = $this->Quizslide->saveNewQuizslide($this->data);
				if($quizslidedata){
					//saved, if it's ajax, output the redirect url, otherwise redirec
					$redirect = '/my/'.lcfirst(Inflector::pluralize($quizslidedata['Quizslide']['class']).'/edit/'.$quizslidedata['Quizslide']['foreign_id']);
					if($this->checkAjax()){
						$this->Session->setFlash(
										"Saved",
										'alert',
										array(
											'plugin' => 'TwitterBootstrap',
											'class' =>  'alert-success'
										)
									);
						echo '{"href":"'.$redirect.'"}';
						exit;
					}else{
						try {
							$this->redirect($redirect);
						} catch (Exception $e) {
						    echo 'Caught exception: ',  $e->getMessage(), "\n";
						}
						exit;
					}
				}
			}else{
				$this->data = $this->Quiz->find('first',array( 'conditions' => array('Quiz.id'=>$quiz_id)));
			}
		}

		public function admin_edit($quizslide_id = null) {
			// $g = array('Quizslide' => array('id' => '390','class' => 'Quiztextslide','foreign_id' => '364','quiz_id' => '7','name' => 'SDASDASDASDAs','order' => '0','response_delay' => '3'),'Quiz' => array('id' => '7','name' => 'Steve\'s Test Quiz','Description' => 'This is the test description.'),'Quiztextslide' => array('id' => '364','text' => 'DAsdasDAdas','name' => ' sdaDAsd','subhead' => 'aDasdas','display_field' => ' sdaDAsd'));
			// debug($g);
			// exit;
			$this->Quizslide->recursive = 1;
			$this->Quizslide->id = $quizslide_id;
			$this->setDashboardLayout();
			//------------
			$this->set('breadcrumbs' , $this->QAdminbreadcrumb->quizSlideEditOverview($quizslide_id));
			//------------
			$quizslidedata = $this->Quizslide->read();

			
			if ($this->request->is('post') || $this->request->is('put')) {
				 // ok for some reason it appears that $quizslidedata is returning for multichoices etc on creating a new, but not for anything else.
				$quizslidedata2 = $this->Quizslide->save($this->request->data);
				// debug($quizslidedata);
				// debug($quizslidedata2);

				if($quizslidedata['Quizslide']['class'] != $quizslidedata2['Quizslide']['class']){
					//  debug("!shenanigans!");
					// delete the old. 
				}

				if($quizslidedata){
					//saved, if it's ajax, output the redirect url, otherwise redirec
					$redirect = '/my/'.lcfirst(Inflector::pluralize($quizslidedata['Quizslide']['class']).'/edit/'.$quizslidedata['Quizslide']['foreign_id']);
					//debug($redirect);
					if($this->checkAjax()){
						echo '{"href":"'.$redirect.'"}'; exit;
					}else{
						try {
							$this->redirect($redirect);
						} catch (Exception $e) {
						    echo 'Caught exception: ',  $e->getMessage(), "\n";
						}
						exit;
					}
				}
			}



			//------------------
			if (!$this->Quizslide->exists()) {
				throw new NotFoundException(__('Invalid %s', __('Quizslide')));
			}
			$this->data = $quizslidedata;
			//--
			$class = $quizslidedata['Quizslide']['class'];
			$b = $this->slide_options[$class];
			//assembling select data
			unset($this->slide_options[$class]);
			$a = array(
				 $class => " - Current ".$b,
				'*'.$class => " - New ".$b,				 );

			if (!isset($this->data[$class]['id'])) {

					unset($a[$class]);
					$this->set('orphaned',1);
			}
			$p = array_merge($a,$this->slide_options);
			$this->set('slide_options', $p );
			$this->set('slide_options_json', json_encode($p) );
		}

		public function admin_view($id = null){
			$this->view($id);
		}

}
