<?php	
	class QAdminbreadcrumbComponent extends Component {

		/// [QUIZ LISTING] -> [QUIZ OVERVIEW] -> [QUIZSLIDE LISTING] -> [QUIZSLIDE OVERVIEW EDIT] -> [ SPECIFIC {SLIDETYPE} EDIT ] -> [ SPECIFIC {SLIDETYPE} ADDITIONAL REQUIREMENTS ]

		private function _quizListing(){
			return array(
				'label'=>'Quizzes',

				'attributes'=>array(
					'href'=>'/my/quizzes/',
					'class'=>"",
				),								
			);
		}
		
		private function _quizOverview($ar){
			// /my/quizzes/edit/7
			return array(
					'label'=> $ar['Quiz']['name'],				
					'attributes'=>array(
						'href'=>'/my/quizzes/edit/'.$ar['Quiz']['id'],			
						'class'=>"",
					),				
				);
		}
		private function _quizslidelisting($qid){
			// /my/quizslides/7
			 // debug($ar);
			 // exit;
			return array(
				'label'=>'Slides',
				'attributes'=>array(
					'href'=> '/my/quizslides/'.$qid,					
					'class'=>"",
				),				
			);
		}
		private function _quizSlideEditOverview($ar){
			// /my/quizslides/edit/142
			return array(
				'label'=>$ar['Quizslide']['name'],
				
				'attributes'=>array(
					'href'=>'/my/quizslides/edit/'.$ar['Quizslide']['id'],
					'class'=>"",
				),								
			);
		}
		private function _slideTypeEdit($ar){
			// /my/quizslides/edit/142
			return array(
				'label'=>$ar['Quizslide']['class'].' '.$ar['Quizslide']['foreign_id'].': '.$ar[$ar['Quizslide']['class']]['name'],
				'attributes' => array(
					'href'=> '/my/'.Inflector::pluralize(strtolower($ar['Quizslide']['class'])).'/edit/'.$ar['Quizslide']['foreign_id'],
					'class'=>"",
				),								
			);
		}


		public function slideTypeEdit($class, $foreign_id){
 			//[QUIZ LISTING] -> [QUIZ OVERVIEW] -> [QUIZSLIDE LISTING] -> [QUIZSLIDE OVERVIEW EDIT] -> [ SPECIFIC {SLIDETYPE} EDIT ]
			$quizslide = ClassRegistry::init('Quizslide');
			$ar = $quizslide->find('first',array('conditions'=>array('class'=>$class,'foreign_id' => $foreign_id)));			
			$t = $class.'.id';
			$model = ClassRegistry::init($class);
			$ar2 = $model->find('first',array('conditions'=>array($t => $foreign_id)));
			//debug($ar); exit;
			foreach ($ar2 as $key => $value) {
				$ar[$key]=$value;				
			}

			// debug($ar);
			// debug($ar2);			
			// exit;
			return array(
					$this->_quizListing(),
					$this->_quizOverview($ar),
					$this->_quizslidelisting($ar['Quizslide']['quiz_id']),
					$this->_quizSlideEditOverview($ar),
					$this->_slideTypeEdit($ar)					
				);
		}


		public function quizOverview($quiz_id){
			$quiz = ClassRegistry::init('Quiz');
			$ar = $quiz->find('first',array('conditions'=>array('Quiz.id' => $quiz_id) ));
			return array(
					$this->_quizListing(),
					$this->_quizOverview($ar),
					//$this->_quizslidelisting($ar)
			);

		}

		public function quizslidelisting($quiz_id){
			$quizslide = ClassRegistry::init('Quiz');
			$ar = $quizslide->find('first',array('conditions'=>array('Quiz.id' => $quiz_id)));
			return array(
					$this->_quizListing(),
					$this->_quizOverview($ar),
					$this->_quizslidelisting($quiz_id)
			);
		}

		public function quizSlideEditOverview($id){
			$quizslide = ClassRegistry::init('Quizslide');
			$ar = $quizslide->find('first',array('conditions'=>array('Quizslide.id' => $id)));

			return array(
					$this->_quizListing(),
					$this->_quizOverview($ar),
					$this->_quizslidelisting($ar['Quiz']['id']),
					$this->_quizSlideEditOverview($ar)
				);
		}



	}




		/*

<select name="data[Quizslide][class]" id="QuizslideClass">




new


Fillintheblank
Wordselect
Multichoice
Sorting


		$this->set('breadcrumbs' , $this->QAdminbreadcrumb->slideTypeEdit('Quiztextslide',$id)); 							
		<?php echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>




Writtenanswer
Dragndrop
Flashcard




*/	
?>