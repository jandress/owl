<?php
App::uses('AppController', 'Controller');
/**
 * Wordselects Controller
 *
 * @property Wordselect $Wordselect
 */
class WordselectsController extends AppController {

/**
 *  Layout
 *
 * @var string
 */


	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
	public $uses = array('Quizmodules.Wordselect','Wordselect'=>'Quizmodules.Wordselectanswer');
	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Session','Auth','Quizmodules.QAdminbreadcrumb');
	private $default_redirect = '/my/wordselects';


	public $paginate = array(
		'limit' => 20
	);

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Wordselect->recursive = 0;
		$this->set('wordselects', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Wordselect->id = $id;
		if (!$this->Wordselect->exists()) {
			throw new NotFoundException(__('Invalid %s', __('wordselect')));
		}
		//$this->layout = 'frontend';
		$neighbors = $this->Wordselect->find('neighbors');
		$prev = empty($neighbors['prev']['Wordselect']['id']) ? 'first' : $neighbors['prev']['Wordselect']['id'];
		$next = empty($neighbors['next']['Wordselect']['id']) ? 'last' : $neighbors['next']['Wordselect']['id'];
		$this->set(compact('prev','next'));

		$this->data = $this->Wordselect->read(null, $id);
		$this->set('wordselect', $this->data);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Wordselect->create();
			if ($this->Wordselect->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('wordselect')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect($this->default_redirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('wordselect')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Wordselect->id = $id;
		if (!$this->Wordselect->exists()) {
			throw new NotFoundException(__('Invalid %s', __('wordselect')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Wordselect->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('wordselect')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				//$this->redirect($this->default_redirect);
				echo '{"href":"'.$this->default_redirect.'"}';
				exit;
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('wordselect')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Wordselect->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Wordselect->id = $id;
		if (!$this->Wordselect->exists()) {
			throw new NotFoundException(__('Invalid %s', __('wordselect')));
		}
		if ($this->Wordselect->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('wordselect')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->default_redirect);
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('wordselect')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->default_redirect);
	}/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		//$this->Wordselect->find('first');
		$this->Wordselect->recursive = 1;
		$this->set('wordselects', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Wordselect->id = $id;
		if (!$this->Wordselect->exists()) {
			throw new NotFoundException(__('Invalid %s', __('wordselect')));
		}
		$this->data = $this->Wordselect->read(null, $id);
		debug($this->data);exit;
		$this->set('wordselect', $this->data);
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Wordselect->create();
			if ($this->Wordselect->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('wordselect')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect($this->default_redirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('wordselect')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Wordselect->id = $id;

		if (!$this->Wordselect->exists()) {
			throw new NotFoundException(__('Invalid %s', __('wordselect')));
		}
		$this->set('breadcrumbs' , $this->QAdminbreadcrumb->slideTypeEdit('Wordselect',$id));



		if ($this->request->is('post') || $this->request->is('put')) {
			$d = $this->Wordselect->read();

			$this->default_redirect = '/my/quizslides/'.$d['Quizslide']['quiz_id'];
			if ($this->Wordselect->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('wordselect')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);

				$this->redirect($this->default_redirect);
				//echo '{"href":"'.$this->default_redirect.'"}';
				exit;
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('wordselect')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Wordselect->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Wordselect->id = $id;
		if (!$this->Wordselect->exists()) {
			throw new NotFoundException(__('Invalid %s', __('wordselect')));
		}
		if ($this->Wordselect->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('wordselect')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->default_redirect);
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('wordselect')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->default_redirect);
	}

	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('view','checkanswer');
	}

	public function checkanswer($id, $answer){
		$this->layout = 'ajax';
		$this->Wordselect->id = $id;
		$data = $this->Wordselect->read();
		$this->set('reponse',$data['Wordselect']['answer'] == $answer);
	}


}
