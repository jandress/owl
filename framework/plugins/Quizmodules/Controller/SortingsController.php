<?php
App::uses('AppController', 'Controller');
/**
 * Sortings Controller
 *
 * @property Sorting $Sorting
 * @property PaginatorComponent $Paginator
 */
class SortingsController extends AppController {

/**
 *  Layout
 *
 * @var string
 */
	//public $layout = 'general_frontend';

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator','Quizmodules.Breadcrumbs');	
	public $uses = array('Sorting','Sortingpiece','Quizslide');
	public $components = array('Paginator','Quizmodules.QAdminbreadcrumb','Userassets.Fileutil');

	 
/**
 * Components
 *
 * @var array
 */
	public $defaultRedirect ='/my/quizzes';

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Sorting->recursive = 0;
		$this->set('sortings', $this->paginate());
	}


	public function admin_index() {
		$this->Sorting->recursive = 0;
		$this->set('sortings', $this->paginate());
	}


/**
 * view method
 *
 * @param string $id
 * @return void
 */
	

	public function view($id = null) {
		exit;
		$this->Sorting->id = $id;

		if (!$this->Sorting->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sorting')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			//$b = $this->Sorting->save($this->request->data);
			//print_r($this->request->data['Sorting']['id']);
			$this->Sorting->contain();
			$b = $this->Sorting->find('first',array('conditions'=>array('id'=>$this->request->data['Sorting']['id'])));
			//print_r(); exit;
			if($this->request->data['Sorting']['answer']==$b['Sorting']['answer']){
					$this->Session->setFlash(
						$b['Sorting']['correct_text'],
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success',
							'title' => 'Correct!'
						)
					);	
			}else{
					$this->Session->setFlash(
						$b['Sorting']['incorrect_text'],
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-danger',
							'title' => 'Incorrect!'
						)
					);	
			}
			if ($b) {
				$this->defaultRedirect = '/sortings/view/'.$id;				
				if($this->checkAjax()){
					echo '{"href":"'.$this->defaultRedirect.'"}';					
					exit;
				}else{
					$this->redirect($this->defaultRedirect);
				}	
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('sorting')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Sorting->read(null, $id);

			$this->set('sortingpiece',$this->request->data['Sortingpiece']);
			
		}
	}	


	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('check');
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */




	public function admin_image($id = null) {
		if ($this->request->is('post') || $this->request->is('put')) {
				$this->Sorting->id = $id;			
				if (!$this->Sorting->exists()) {
					throw new NotFoundException(__('Invalid %s', __('sorting')));
				}
				$this->request->data['Sortingpiece']['sorting_id'] = $this->request->data['Sorting']['id'];
				$d = $this->Sortingpiece->save($this->request->data);
				if ($d) {
					$this->Session->setFlash(
						__('The %s has been saved', __('sorting')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);
					$r = $this->Quizslide->find('first',array('conditions'=>array('Quizslide.class'=>'Sorting','Quizslide.foreign_id'=>$d['Sortingpiece']['sorting_id'])));
					//$this->redirect('/my/sortingpieces/saved/'.$r['Quizslide']['id']);				
					if($this->checkAjax()){
						$this->defaultRedirect = '/my/sortings/order/'.$id;
						// if there are sortings pieces, go to arrange_sorting_pieces 
						// if there are no sorting pieces, go to new sorting piece. 
						echo '{"href":"'.$this->defaultRedirect.'"}';
						exit;
				}else{
					$this->redirect($this->defaultRedirect);
				}	
				} else {
					$this->Session->setFlash(
						__('The %s could not be saved. Please, try again.', __('sorting')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-error'
						)
					);
				}
			} else {
				$this->set('file_listing',$this->Fileutil->getDirectoryContents('./media/sorting'));
				//$this->request->data = array('Sortingpiece'=>array('sorting_id'=>$id,'id'=>''),'Sorting'=>array('id'=>$id));
				$this->request->data = $this->Sorting->find('first',array('conditions'=>array('Sorting.id'=>$id)));
			}		
	}


	private function sanitizePieces(){
		$this->Sortingpiece->deleteAll(array('Sortingpiece.type'=>'text','Sortingpiece.name'=>''));
	}



	public function new_sortings_piece($id = null) {
		//$this->layout='ajax';
		if ($this->request->is('post') || $this->request->is('put')) {
			
				$this->Sorting->id = $id;			
				if (!$this->Sorting->exists()) {
					throw new NotFoundException(__('Invalid %s', __('sorting')));
				}
				$d = $this->Sortingpiece->save($this->request->data);

				if($d['Sorting']['answer']==''){
					$d['Sorting']['answer'] = $d['Sortingpiece']['id'];					
				}else{
					$d['Sorting']['answer'] = $d['Sorting']['answer'].','.$d['Sortingpiece']['id'];										
				}
				$this->Sorting->save($d);
				if ($d) {
					$this->Session->setFlash(
						__('The %s has been saved', __('sorting')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);
					$r = $this->Quizslide->find('first',array('conditions'=>array('Quizslide.class'=>'Sorting','Quizslide.foreign_id'=>$d['Sortingpiece']['sorting_id'])));
					$this->defaultRedirect = '/my/sortings/order/'.$id;
					$this->sanitizePieces();
					if($this->checkAjax()){
						// if there are sortings pieces, go to arrange_sorting_pieces 
						// if there are no sorting pieces, go to new sorting piece. 
						echo '{"href":"'.$this->defaultRedirect.'"}';
						exit;
					}else{
						$this->redirect($this->defaultRedirect);
					}	
				} else {
					$this->Session->setFlash(
						__('The %s could not be saved. Please, try again.', __('sorting')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-error'
						)
					);
				}
			} else {
				$this->request->data = $this->Sorting->find("first", array('conditions'=>array('Sorting.id' => $id)));
			}
	}
	





	public function admin_view($id = null) {
		$this->Sorting->id = $id;
		if (!$this->Sorting->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sorting')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$b = $this->Sorting->checkAnswer($this->request->data);

			//print_r($this->request->data['Sorting']['id']);
			$this->Sorting->contain();
			$b = $this->Sorting->find('first',array('conditions'=>array('id'=>$this->request->data['Sorting']['id'])));
			if($this->request->data['Sorting']['answer']==$b['Sorting']['answer']){
					$this->Session->setFlash(
						__('Correct', __('sorting')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);	
			}else{
					$this->Session->setFlash(
						__('Incorrect', __('sorting')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-danger'
						)
					);	
			}



			if ($b) {
				if($this->checkAjax()){
					$this->defaultRedirect = '/my/sortings/edit/'.$id;
					//$this->defaultRedirect = '/my/sortings/order/'.$id;					
					echo '{"href":"'.$this->defaultRedirect.'"}';					
					exit;
				}else{
					$this->redirect($this->defaultRedirect);
				}	
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('sorting')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Sorting->read(null, $id);
			$ar = array();
			foreach ($this->request->data['Sortingpiece'] as $key => $value) {
				array_push($ar,array('Sortingpiece'=>$value ));
			}
			$this->set('sortingpiece',$ar);			
		}
	}	



	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
		//	throw new MethodNotAllowedException();
		}
		$this->Sorting->id = $id;
		if (!$this->Sorting->exists()) {
			throw new NotFoundException(__('Invalid %s', __('Sorting')));
		}
		if ($this->Sorting->delete($id)) {
			$this->Session->setFlash(
				__('The %s was deleted', __('Sorting')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect('/my/sortings/');
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('sortingpiece')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect('/my/sortings/');
	}




	public function admin_edit_order($id = null) {
		$this->Sorting->id = $id;
		if (!$this->Sorting->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sorting')));
		}

		$this->set('breadcrumbs' , $this->QAdminbreadcrumb->slideTypeEdit('Sorting',$id)); 


		if ($this->request->is('post') || $this->request->is('put')) {

			$b = $this->Sorting->save($this->request->data);
			if ($b) {
				$this->Session->setFlash(
					__('The %s has been saved', __('sorting')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
					$g = $this->Quizslide->find('first',array('conditions'=>array('Quizslide.foreign_id'=>$id,'Quizslide.class'=>'Sorting')));
					$this->defaultRedirect = '/my/quizslides/'.$g['Quizslide']['quiz_id'];
				if($this->checkAjax()){
					// if there are sortings pieces, go to arrange_sorting_pieces 
					// if there are no sorting pieces, go to new sorting piece. 
					echo '{"href":"'.$this->defaultRedirect.'"}';					
					exit;
				}else{
					$this->redirect($this->defaultRedirect);
				}	
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('sorting')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Sorting->read(null, $id);
			$g = $this->Sortingpiece->find('list',array('conditions'=>array('sorting_id' => $id),'fields'=>array('id')));
			$str = implode(',',$g);

			// debug(
			// 	$str
			// 	); 
			// 	debug($this->request->data['Sorting']['answer']); 
			// 	exit;
//			exit;
			// if(
			// 	$this->request->data['Sorting']['answer'] == "" ||
			// 
			// 
			// ){
			// 	
			// }
		}
	}




		public function indevelopment() {
			
			
			$str = 'Now is the time~or all good men~to come to~!~the aid of their country~apples~bears~consectetur adipisicing elit! sed do eiusmod tempor incididunt ut laboretorium et dolore magna aliqua.';
			$str .="~Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
			$str .= "~".$str;

			// debug($str);
			// debug(explode("~",$str));
			$arr = array();
			foreach (explode("~",$str) as $key => $value) {
				$type = $value == "!" ? 'image' : "text";
				$_b = array(
					'name'=>$value,
					'sorting_id' => '22',
					'position' => $key,
					'type' => $type
				);
				array_push($arr,$_b);
///				debug($_b);
			}			
			$ar2 = array();
			$ar2['Sortingpiece'] = $arr;
			// debug( $ar2 );
			// exit;
//			explode ( string $delimiter , string $string [, int $limit = PHP_INT_MAX ] )
		
			// sorting_id
			// Sortingpiece
		//	exit;
		// 'id' => '124',
		// 		'name' => 'One',
		// 		'sorting_id' => '22',
		// 		'position' => '0',
		// 		'type' => 'text'
			$id = 22;
			$this->Sorting->id = $id;
			// debug($this->Sortingpiece->deleteAll(array('Sortingpiece.sorting_id'=>$id)));
//			 debug($this->Sortingpiece->saveAll($ar2['Sortingpiece']));

//			exit;
			/*
			'Sorting' => array(
					'id' => '22',
					'directive' => 'Slide 1 Directive',
					'name' => ' Slide 1 Name',
					'correct_text' => 'Correct!',
					'incorrect_text' => 'Incorrect!',
					'answer' => '124,146,126,145,147',
					'viewAction' => '/multichoices/view/',
					'viewActionFull' => '/multichoices/view/22',
					'editAction' => '/my/multichoices/edit/',
					'editActionFull' => '/my/multichoices/edit/22'
				),
				
				
				'Sortingpiece' => array(
						(int) 0 => array(
							'id' => '124',
							'name' => 'One',
							'sorting_id' => '22',
							'position' => '0',
							'type' => 'text'
						),
			*/
			
			
			if (!$this->Sorting->exists()) {
				throw new NotFoundException(__('Invalid %s', __('sorting')));
			}

			$this->set('breadcrumbs' , $this->QAdminbreadcrumb->slideTypeEdit('Sorting',$id)); 

			if ($this->request->is('post') || $this->request->is('put')) {

				$b = $this->Sorting->save($this->request->data);
				if ($b) {
					$this->Session->setFlash(
						__('The %s has been saved', __('sorting')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);
						$g = $this->Quizslide->find('first',array('conditions'=>array('Quizslide.foreign_id'=>$id,'Quizslide.class'=>'Sorting')));
						$this->defaultRedirect = '/my/quizslides/'.$g['Quizslide']['quiz_id'];
					if($this->checkAjax()){
						// if there are sortings pieces, go to arrange_sorting_pieces 
						// if there are no sorting pieces, go to new sorting piece. 
						echo '{"href":"'.$this->defaultRedirect.'"}';					
						exit;
					}else{
						$this->redirect($this->defaultRedirect);
					}	
				} else {
					$this->Session->setFlash(
						__('The %s could not be saved. Please, try again.', __('sorting')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-error'
						)
					);
				}
			} else {
				$this->request->data = $this->Sorting->read(null, $id);
				$g = $this->Sortingpiece->find('list',array('conditions'=>array('sorting_id' => $id),'fields'=>array('id')));
				$str = implode(',',$g);

				// debug(
				// 	$str
				// 	); 
				// 	debug($this->request->data); 
				// 	exit;
	//			exit;
				// if(
				// 	$this->request->data['Sorting']['answer'] == "" ||
				// 
				// 
				// ){
				// 	
				// }
			}
		}





	public function admin_edit($id = null) {
		$this->Sorting->id = $id;
		$this->set('breadcrumbs' , $this->QAdminbreadcrumb->slideTypeEdit('Sorting',$id)); 
		if (!$this->Sorting->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sorting')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Sorting->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('sorting')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				//$this->redirect($this->defaultRedirect);
				// $this->defaultRedirect;
				if($this->checkAjax()){
					$b = $this->Sorting->Sortingpiece->find('count',array('conditions'=>array('sorting_id'=>$id)));
					//print_r($b);exit;
					if(empty($b)){
						$this->defaultRedirect = '/my/sortings/newpiece/'.$id;
					}else{						
						$this->defaultRedirect = '/my/sortings/order/'.$id;
						
					}
					// if there are sortings pieces, go to arrange_sorting_pieces  if there are no sorting pieces, go to new sorting piece. 
					echo '{"href":"'.$this->defaultRedirect.'"}';
					exit;
				}else{
					$this->redirect('/my/sortings/order/'.$id);
				}	
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('sorting')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Sorting->read(null, $id);
		}
	}




/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete_sortings_piece($id = null) {
		if (!$this->request->is('post')) {
	//		throw new MethodNotAllowedException();
		}
		$this->Sortingpiece->id = $id;
		if (!$this->Sortingpiece->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sorting')));
		}

		$g = $this->Sortingpiece->read();
		$g['Sorting']['answer'] = str_replace(','.$id,'', $g['Sorting']['answer']);
		$g['Sorting']['answer'] = str_replace($id.',','', $g['Sorting']['answer']);
		$this->Sorting->save($g); 		
		if ($this->Sortingpiece->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('sorting')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->set('response',"true");
		}else{
			$this->set('response',false);			
			$this->Session->setFlash(
				__('The %s was not deleted', __('sorting')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-error'
				)
			);			
		}
		// $this->redirect(array('action' => 'index'));
		$this->set('response',"true");		
	}/**
 * my_index method
 *
 * @return void
 */
	public function my_index() {
		$this->Sorting->recursive = 0;
		$this->set('sortings', $this->paginate());
	}

/**
 * my_view method
 *
 * @param string $id
 * @return void
 */
	public function my_view($id = null) {
		$this->Sorting->id = $id;
		if (!$this->Sorting->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sorting')));
		}
		$this->set('sorting', $this->Sorting->read(null, $id));
	}

	
	public function check($id = null) {
		$this->layout='ajax';
		$this->set('response',true);
	}


/**
 * my_edit method
 *
 * @param string $id
 * @return void
 */

	public function saved($id = null) {
		$this->layout = "ajax";
		$this->set('id',$id);
	}
		
	public function edit_sortings_piece($id = null) {
		$this->Sortingpiece->id = $id;
		//$this->layout='ajax';
		if (!$this->Sortingpiece->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sorting')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$d = $this->Sortingpiece->save($this->request->data);
			if ($d) {
				$this->Session->setFlash(
					__('The %s has been saved', __('sorting')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$r = $this->Quizslide->find('first',array('conditions'=>array('Quizslide.class'=>'Sorting','Quizslide.foreign_id'=>$d['Sortingpiece']['sorting_id'])));
				//debug($r);
				$this->defaultRedirect = '/my/sortings/order/'.$r['Quizslide']['foreign_id'];
//				echo '{"href":"'.$this->defaultRedirect.'"}';					
				/*

				'Sortingpiece' => array(
						'sorting_id' => '2',
						'name' => 'The Rain'
					)

				*/
				$this->redirect($this->defaultRedirect);	
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('sorting')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Sortingpiece->read(null, $id);
		}
	}


	public function my_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Sorting->id = $id;
		if (!$this->Sorting->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sorting')));
		}
		if ($this->Sorting->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('sorting')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('sorting')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
}
