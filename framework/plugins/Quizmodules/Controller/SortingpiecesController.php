<?php
App::uses('AppController', 'Controller');
/**
 * Sortingpieces Controller
 *
 * @property Sortingpiece $Sortingpiece
 * @property PaginatorComponent $Paginator
 */
class SortingpiecesController extends AppController {

/**
 *  Layout
 *
 * @var string
 */
	public $layout = 'general_frontend';

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Sortingpiece->recursive = 0;
		$this->set('sortingpieces', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Sortingpiece->id = $id;
		if (!$this->Sortingpiece->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sortingpiece')));
		}
		$this->set('sortingpiece', $this->Sortingpiece->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		if ($this->request->is('post')) {
			$this->Sortingpiece->create();
			if ($this->Sortingpiece->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('sortingpiece')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('sortingpiece')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$sortings = $this->Sortingpiece->Sorting->find('list');
		$this->set(compact('sortings'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		$this->Sortingpiece->id = $id;
		if (!$this->Sortingpiece->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sortingpiece')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Sortingpiece->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('sortingpiece')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('sortingpiece')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Sortingpiece->read(null, $id);
		}
		$sortings = $this->Sortingpiece->Sorting->find('list');
		$this->set(compact('sortings'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Sortingpiece->id = $id;
		if (!$this->Sortingpiece->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sortingpiece')));
		}
		if ($this->Sortingpiece->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('sortingpiece')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('sortingpiece')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}/**
 * my_index method
 *
 * @return void
 */
	public function my_index() {
		$this->Sortingpiece->recursive = 0;
		$this->set('sortingpieces', $this->paginate());
	}

/**
 * my_view method
 *
 * @param string $id
 * @return void
 */
	public function my_view($id = null) {
		$this->Sortingpiece->id = $id;
		if (!$this->Sortingpiece->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sortingpiece')));
		}
		$this->set('sortingpiece', $this->Sortingpiece->read(null, $id));
	}

/**
 * my_add method
 *
 * @return void
 */
	public function my_add() {
		if ($this->request->is('post')) {
			$this->Sortingpiece->create();
			if ($this->Sortingpiece->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('sortingpiece')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('sortingpiece')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$sortings = $this->Sortingpiece->Sorting->find('list');
		$this->set(compact('sortings'));
	}

/**
 * my_edit method
 *
 * @param string $id
 * @return void
 */
	public function my_edit($id = null) {
		$this->Sortingpiece->id = $id;
		if (!$this->Sortingpiece->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sortingpiece')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Sortingpiece->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('sortingpiece')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('sortingpiece')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Sortingpiece->read(null, $id);
		}
		$sortings = $this->Sortingpiece->Sorting->find('list');
		$this->set(compact('sortings'));
	}

/**
 * my_delete method
 *
 * @param string $id
 * @return void
 */
	public function my_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Sortingpiece->id = $id;
		if (!$this->Sortingpiece->exists()) {
			throw new NotFoundException(__('Invalid %s', __('sortingpiece')));
		}
		if ($this->Sortingpiece->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('sortingpiece')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('sortingpiece')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
}
