<?php
class FlashcardsController extends QuizmodulesAppController {
	
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');	
	
	var $uses = array(
		'Quizmodules.Flashcard'
		,'Quizmodules.Quiz'
		,'Quizmodules.Quizslide'
		,'Quizmodules.Quizresponse'
		
	);
	

	var $components = array('Quizmodules.QAdminbreadcrumb');

	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('view');

	}
	
	
	public function admin_index(){
		$this->setDashboardLayout();
		$this->handleAjax();		
		$this->set('flashcards', $this->paginate());
	}
	

	public function index(){}

	public function admin_view($id) {
		// $this->view($id);
		// $this->setDashboardLayout();		
	}

	public function standalone_view($id){
		// $this->view($id);
		// $this->layout = 'standalone_quiz_layout';
	}

	public function view($id) {}
	
	public function admin_add() {
		$this->setDashboardLayout();
		if ($this->request->is('post')) {
			$this->Flashcard->create();
			$saved = $this->Flashcard->save($this->request->data);
			$this->set('slide_options',$this->slide_options );			
			if ($saved) {
				$this->Session->setFlash(
					__('The %s has been saved', __('quiz')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);



				//$this->redirect($this->defaultRedirect);
				if($this->checkAjax()){
					echo '{"href":"'.$defaultRedirect.'"}'; exit;
				}else{
					$this->redirect($this->defaultRedirect);
				}	
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('quiz')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
	}

	
	
	
	public function admin_delete($id = null) {
		$this->setDashboardLayout();
		$this->Flashcard->id = $id;		
		$data = $this->Flashcard->read();		
		if ($this->request->is('post') || $this->request->is('put')) {
			$d = $this->Flashcard->delete($data['Quiz']);			
			if ($d) {
				$this->Session->setFlash(
					__('The %s has been deleted', __('quiz')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('/my/quizzes/');
			} else {
				$this->Session->setFlash(
					__('The %s could not be deleted. Please, try again.', __('quiz')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Flashcard->read(null, $id);
		}
		$this->set('quizslides', $this->paginate());
	}
	




	public function admin_edit($id = null) {
		$this->Flashcard->id = $id;
		$this->setDashboardLayout();
		$this->set('slide_options',$this->slide_options );
		if (!$this->Flashcard->exists()) {
			throw new NotFoundException(__('Invalid %s', __('quiz')));
		}

		$this->set('breadcrumbs' , $this->QAdminbreadcrumb->slideTypeEdit('Flashcard',$id)); 							


		if ($this->request->is('post') || $this->request->is('put')) {
			$d = $this->Flashcard->save($this->request->data);
			$b = $this->Quizslide->find('first',array('conditions'=>array('class'=>'Flashcard','foreign_id'=>$id)));
			// set flash whether or not it's been saved.  if this is ajax, return the next url, if its not ajax, redirect. 
			if ($d && $b) {
				$this->Session->setFlash(
					__('The %s has been saved', __('quiz text slide')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				if($this->checkAjax()){

					echo '{"href":"'.'/my/quizslides/'.$b['Quiz']['id'].'"}'; exit;

				}else{
					$this->redirect('/my/quizslides/'.$b['Quiz']['id']);
				}
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('quiz text slide')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
				if($this->checkAjax()){
					echo '/my/quizslides/'.$id;
					exit;
				}else{
					echo false;
					exit;
				}				
			}
		} else {
			$this->request->data = $this->Flashcard->read(null, $id);
		}
		$this->handleAjax();					
	}
	
	
	
}