<?php
class QuizzesController extends QuizmodulesAppController {

	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator','Quizmodules.Breadcrumbs');

	var $uses = array(
		'Quizmodules.Quiz'
		,'Quizmodules.Quizslide'
		,'Quizmodules.Quizresponse'
	);

	public $defaultRedirect = '/my/quizzes/';
	public $components = array('Session','Quizmodules.QAdminbreadcrumb','Auth');




	public $slide_options = array(
		'Quiztextslide'=>'Text-only slide'
		,'Multichoice'=>'Multiple Choice'
		,'Flashcard'=>'Flashcards'
	);

	public $paginate = array(
		'limit' => 20
	);

	public function getElementAsAjax($element=null,$element2=null,$paramString=null){
		$this->layout = 'ajax';
		$serializedParams = $this->params['named'];

		if(!empty($element) && !empty($element2)){
			$this->set('element',$element.'/'.$element2);
			$this->set('elementParameters',$serializedParams);
			//debug($serializedParams);
		}
	}
	public function beforeFilter(){
		parent::beforeFilter();
		//~potentially a security issue...
		$this->Auth->allow('view','end','admin_end','getElementAsAjax','standalone_view');

	}
	function slideeditfields($type){
		$this->set('element',$type);
		$serializedParams = $this->params['named'];
		$this->layout = 'ajax';
		$this->set('elementParameters',$serializedParams);
	}

	public function admin_index(){
		//debug($this->here);
		$this->setDashboardLayout();
		$this->set('quizzes', $this->paginate());
		$this->handleAjax();
	}

	function quizslide_index(){
		echo 'quizslide_index';
	}

	public function index(){
		///$this->Quiz->contain();
		$this->setDashboardLayout();
		$this->set('quizzes', $this->paginate());
	}

	public function record(){
		$this->Quizresponse->recordAnswer($this->data);
	}

	public function end(){
		$this->handleAjax();
		$this->set('data',$this->Quizresponse->talleyQuiz());
	}
	public function admin_end(){
		$this->handleAjax();
		$this->end();
	}
	public function admin_view($id) {
		$this->view($id);
		}

	public function standalone_view($id){
		$this->view($id);
		$this->layout = 'standalone_quiz_layout';
	}

	public function view($id) {
		$this->setNavIdByAssociatedContent('Quiz',$id);
		//$this->layout = 'ajax';
		$this->Quiz->id = $id;
		if (!$this->Quiz->exists()) {
			throw new NotFoundException(__('Invalid %s', __('quiz')));
		}
		$d	= $this->Quiz->read();
		//$o	= $this->Quizslide->find('first',array('conditions'=>array('Quizslide.quiz_id'=>$id),'order'=>array('Quizslide.order')));
		$qsl = $this->Quizslide->find('all',array('conditions'=>array('quiz_id'=> $id),'order'=>array('Quizslide.order')));

		// $this->set('quizslide_list',$qsl);
		//---
		$id_array_str = '';
		foreach ($qsl as $key => $value) {
			$id_array_str .= $value['Quizslide']['id'];
			$id_array_str .= ',';
		}
		$g = rtrim($id_array_str, ",");
		$this->set("id_array",$g);
		$this->data = array(
			'Quiz' =>$d['Quiz'],
			'Quizslide' => $qsl[0]['Quizslide']
		);
		/*
				1	id
				2	user_id
				3	quiz_id
				4	quizslide_id
				5	description
				6	timestamp
		*/
		$this->Quizresponse->start($this->current_user['User']['id'],$d['Quiz']['id']);

	}

	public function admin_add() {
		$this->setDashboardLayout();
		if ($this->request->is('post')) {
			$this->Quiz->create();
			$saved = $this->Quiz->save($this->request->data);
			$this->set('slide_options',$this->slide_options );
			if ($saved) {
				$this->Session->setFlash(
					__('The %s has been saved', __('quiz')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				//$this->redirect($this->defaultRedirect);
				echo '{"href":"'.'/my/quizslides/'.$saved['Quiz']['id'].'"}';
				exit;

			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('quiz')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
	}




	public function admin_delete($id = null) {
		$this->setDashboardLayout();
		$this->Quiz->id = $id;
		//$data = $this->Quiz->read();
		// /debug($data); exit;
		$g = str_replace($_SERVER['HTTP_ORIGIN'],'',$_SERVER['HTTP_REFERER']);
			// array|string $search,
			// array|string $replace,
			// string|array $subject,

		
		if ($this->request->is('post') || $this->request->is('put')) {
			$d = $this->Quiz->delete();
			if ($d) {
				$this->Session->setFlash(
					__('The %s has been deleted', __('quiz')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect($g);
			} else {
				$this->Session->setFlash(
					__('The %s could not be deleted. Please, try again.', __('quiz')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Quiz->read(null, $id);
		}
		$this->set('quizslides', $this->paginate());
	}

	public function autorelocate($id){
		if ($this->request->is('post') || $this->request->is('put')) {
			if(!empty($this->request->data['Quizslide'])){
				foreach ($this->request->data['Quizslide'] as $key => $value) {
					$this->Quizslide->save($value);
				}
			}
			$this->redirect('/my/quizzes/edit/'.$id);
		}

		$this->Quizslide->recursive = -1;
		$this->Quizslide->contain();
		$d = $this->Quizslide->find('all',array(
				'conditions'=>array('Quizslide.quiz_id'=>$id)
			));
		$t = array();
		foreach ($d as $key => $value) {
			$p = $value;
			preg_match_all('!\d+!', $value['Quizslide']['name'], $matches);
			$p['Quizslide']['order']= $matches[0][0];
			$t[$key] = $p['Quizslide'];
		}
		$this->set('data',$t);
	}

	public function admin_edit($id = null) {
		$this->Quiz->id = $id;
		$this->setDashboardLayout();
		$this->set('slide_options',$this->slide_options );
		$this->set('breadcrumbs' , $this->QAdminbreadcrumb->quizOverview($id));
		if (!$this->Quiz->exists()) {
			throw new NotFoundException(__('Invalid %s', __('quiz')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$d = $this->Quiz->save($this->request->data);
			if ($d) {
				$this->Session->setFlash(
					__('The %s has been saved', __('quiz')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				if($this->checkAjax()){
					echo '{"href":"'.'/my/quizslides/'.$id.'"}';
					exit;
				}else{
					$this->redirect('/my/quizslides/'.$id);
					exit;
				}

			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('quiz')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Quiz->read(null, $id);
		//	debug($this->request->data);
			// exit;
		}
		$this->handleAjax();
	}



}
