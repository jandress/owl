<?php
App::uses('AppController', 'Controller');
/**
 * Dragndrops Controller
 *
 * @property Dragndrop $Dragndrop
 */
class DragndropsController extends AppController {

/**
 *  Layout
 *
 * @var string
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
	public $uses = array('Quizmodules.Dragndrop','Quizmodules.Dragndrop','Quizmodules.Dragndropanswer','Quizslide');
	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Session','Auth');
	private $default_redirect = '/my/dragndrops';

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Dragndrop->recursive = 0;
		$this->set('dragndrops', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {

		// $this->Dragndrop->id = $id;
		// if (!$this->Dragndrop->exists()) {
		// 	throw new NotFoundException(__('Invalid %s', __('dragndrop')));
		// }
		$this->layout = 'test';
//		$this->layout = 'frontend';		
		// $neighbors = $this->Dragndrop->find('neighbors');
		// $prev = empty($neighbors['prev']['Dragndrop']['id']) ? 'first' : $neighbors['prev']['Dragndrop']['id'];
		// $next = empty($neighbors['next']['Dragndrop']['id']) ? 'last' : $neighbors['next']['Dragndrop']['id'];
		// $this->set(compact('prev','next'));
		// $this->set('dragndrop', $this->Dragndrop->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Dragndrop->create();
			if ($this->Dragndrop->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('dragndrop')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect($this->default_redirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('dragndrop')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Dragndrop->id = $id;
		if (!$this->Dragndrop->exists()) {
			throw new NotFoundException(__('Invalid %s', __('dragndrop')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			///debug($this->request->data); exit;
			if ($this->Dragndrop->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('dragndrop')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);

				$redirect = $this->Quizslide->find('first',array(
					'conditions' => array(
						'class'=>'Dragndrop',
						'Quizslide.foreign_id'=>$this->request->data['Dragndrop']['id']
					))
				);
				if($this->checkAjax()){
					echo '{"href":"/my/quizslides/'.$redirect['Quizslide']['quiz_id'].'"}';
					exit;

				}else{
					$this->redirect('/my/quizslides/'.$redirect['Quizslide']['quiz_id']);
				}			
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('dragndrop')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Dragndrop->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Dragndrop->id = $id;
		if (!$this->Dragndrop->exists()) {
			throw new NotFoundException(__('Invalid %s', __('dragndrop')));
		}
		if ($this->Dragndrop->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('dragndrop')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->default_redirect);
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('dragndrop')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->default_redirect);
	}/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Dragndrop->recursive = 0;
		$this->set('dragndrops', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Dragndrop->id = $id;
		if (!$this->Dragndrop->exists()) {
			throw new NotFoundException(__('Invalid %s', __('dragndrop')));
		}
		$this->set('dragndrop', $this->Dragndrop->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Dragndrop->create();
			if ($this->Dragndrop->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('dragndrop')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect($this->default_redirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('dragndrop')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Dragndrop->id = $id;
		if (!$this->Dragndrop->exists()) {
			throw new NotFoundException(__('Invalid %s', __('dragndrop')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Dragndrop->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('dragndrop')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect($this->default_redirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('dragndrop')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Dragndrop->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Dragndrop->id = $id;
		if (!$this->Dragndrop->exists()) {
			throw new NotFoundException(__('Invalid %s', __('dragndrop')));
		}
		if ($this->Dragndrop->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('dragndrop')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->default_redirect);
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('dragndrop')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->default_redirect);
	}

	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('view','checkanswer');
	}
	
	


}

