<?php
App::uses('AppController', 'Controller');
//App::uses('QuizmodulesController', 'Controller');
require_once('QuizzesController.php');
/**
 * Fillintheblanks Controller
 *
 * @property Fillintheblank $Fillintheblank
 */
class FillintheblanksController extends QuizzesController {

/**
 *  Layout
 *
 * @var string
 */
	public $layout = 'frontend';
/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */


	public $components = array('Session','Auth','Quizmodules.QAdminbreadcrumb');
	public $defaultRedirect = '/my/fillin/';
	public $uses = array('Fillintheblank','Fibanswer');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Fillintheblank->recursive = 0;
		$this->set('fillintheblanks', $this->paginate());
	}
	
	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('view','checkanswer');
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */

	public function answerrow(){
		$this->layout='ajax';
	}
	
	
	// public function checkanswer($questionId,$answer=null){		
	// 	$this->layout='ajax';
	// 	$this->Fillintheblank->id = $questionId;		
	// 	$data =$this->Fillintheblank->read();
	// 	$val = false;
	// 	foreach ($data['Fibanswer'] as $key => $value) {
	// 		if($value['answer_text'] == $this->request->data['response']){
	// 			$val = true;			
	// 		}
	// 	}
	// 	$this->set('value', $val);
	// }


	public function view($id = null) {
		$this->Fillintheblank->id = $id;
		if (!$this->Fillintheblank->exists()) {
			throw new NotFoundException(__('Invalid %s', __('fillintheblank')));
		}
		$neighbors = $this->Fillintheblank->find('neighbors');
		$prev = empty($neighbors['prev']['Fillintheblank']['id']) ? 'first' : $neighbors['prev']['Fillintheblank']['id'];
		$next = empty($neighbors['next']['Fillintheblank']['id']) ? 'last' : $neighbors['next']['Fillintheblank']['id'];
		$this->set(compact('prev','next'));
		//	$this->layout = 'frontend';		
		$this->data = $this->Fillintheblank->read(null, $id);
		//	$this->set('fillintheblank', $this->Fillintheblank->read(null, $id));
		$this->layout = 'quiz_layout';		
	}


	public function admin_index() {
		$this->Fillintheblank->recursive = 0;
		$this->set('fillintheblanks', $this->paginate());
	}

	public function admin_view($id = null) {
		$this->Fillintheblank->id = $id;
		if (!$this->Fillintheblank->exists()) {
			throw new NotFoundException(__('Invalid %s', __('fillintheblank')));
		}
		$this->set('fillintheblank', $this->Fillintheblank->read(null, $id));
	}
	
	
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Fillintheblank->create();
			if ($this->Fillintheblank->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('fillintheblank')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect($this->defaultRedirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('fillintheblank')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$fibAnswers = $this->Fillintheblank->FibAnswer->find('list');
		$this->set(compact('fibAnswers'));
	}


	public function admin_answers($id = null) {
		$this->Fillintheblank->id = $id;
		if (!$this->Fillintheblank->exists()) {
			throw new NotFoundException(__('Invalid %s', __('fillintheblank')));
		}
		$this->set('breadcrumbs' , $this->QAdminbreadcrumb->slideTypeEdit('Fillintheblank',$id)); 							
		if ($this->request->is('post') || $this->request->is('put')) {
			$s = $this->Fibanswer->saveAnswers($this->request->data);
			$g =$this->Fillintheblank->find('first',array('conditions'=>array('Fillintheblank.id'=>$id )));

			if($s){
				$this->Session->setFlash(
					__('The %s hav been saved', __('answers')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$redirct ='/my/quizslides/'.$g['Quizslide']['quiz_id'];

				if($this->checkAjax()){
				echo '{"href":"'.$redirct.'"}';
					exit;
				}else{
					$this->redirect($redirct);
				}		


				exit;
			}else{
				$this->Session->setFlash(
						__('Error Saving', __('fillintheblank')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-error'
						)
					);
				}
		} else {
			$this->request->data = $this->Fillintheblank->read(null, $id);
		}
	}





	public function admin_edit($id = null) {
		$this->Fillintheblank->id = $id;
		if (!$this->Fillintheblank->exists()) {
			throw new NotFoundException(__('Invalid %s', __('fillintheblank')));
		}
		$this->set('breadcrumbs' , $this->QAdminbreadcrumb->slideTypeEdit('Fillintheblank',$id)); 							
		if ($this->request->is('post') || $this->request->is('put')) {
			$g = $this->Fillintheblank->save($this->request->data);
			if ($g) {
				$this->Session->setFlash(
					__('The %s has been saved', __('fillintheblank')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				if($this->checkAjax()){
						echo '{"href":"/my/fillintheblanks/answers/'.$g['Fillintheblank']['id'].'"}';
					exit;
				}else{
					$this->redirect('/my/fillintheblanks/answers/'.$g['Fillintheblank']['id']);
				}		
				exit;
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('fillintheblank')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Fillintheblank->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Fillintheblank->id = $id;
		if (!$this->Fillintheblank->exists()) {
			throw new NotFoundException(__('Invalid %s', __('fillintheblank')));
		}
		if ($this->Fillintheblank->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('fillintheblank')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->defaultRedirect);
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('fillintheblank')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect($this->defaultRedirect);
	}
}
