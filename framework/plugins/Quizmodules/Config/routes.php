<?php
		Router::connect('/my/quizzes/add/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'admin_add'));
		//Router::connect('/Quizmodules/Quizzes/addslide/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'admin_add'));
		Router::connect('/Quizmodules/Quizzes/delete/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'admin_delete'));
		Router::connect('/my/quizzes/slideeditfields/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'slideeditfields'));
		
		Router::connect('/my/quizzes/edit/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'admin_edit'));
		Router::connect('/quizzes/view/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'view'));
		Router::connect('/quizzes/record/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'record'));
		Router::connect('/my/quizzes/view/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'admin_view'));
		Router::connect('/my/quizzes/end/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'admin_end'));
		Router::connect('/quizslides/quizzes/end/', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'admin_end'));
		//----------------------
		Router::connect('/quizzes/start/', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'start'));
		Router::connect('/my/quizzes/start/', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'start'));
		Router::connect('/quizzes/end/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'end'));
		//----------------------


		Router::connect('/my/quizzes/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'admin_index'));
		Router::connect('/quizzes', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'index'));
		Router::connect('/Quizmodules/Quizzes/index/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'index'));
		//----------------------
		Router::connect('/Quizmodules/Quizslides/delete/*', array('plugin'=>'Quizmodules','controller' => 'Quizslides', 'action' => 'admin_delete'));
		Router::connect('/my/quizslides/delete/*', array('plugin'=>'Quizmodules','controller' => 'Quizslides', 'action' => 'admin_delete'));
		Router::connect('/my/quizslides/add/*', array('plugin'=>'Quizmodules','controller' => 'Quizslides', 'action' => 'admin_add'));
		Router::connect('/my/quizslides/edit/*', array('plugin'=>'Quizmodules','controller' => 'Quizslides', 'action' => 'admin_edit'));
		Router::connect('/quizslides/view/*', array('plugin'=>'Quizmodules','controller' => 'Quizslides', 'action' => 'view'));
		Router::connect('/my/quizslides/view/*', array('plugin'=>'Quizmodules','controller' => 'Quizslides', 'action' => 'admin_view'));
		Router::connect('/my/quizslides/list/*', array('plugin'=>'Quizmodules','controller' => 'Quizslides', 'action' => 'list_all'));
		Router::connect('/my/quizslides/*', array('plugin'=>'Quizmodules','controller' => 'Quizslides', 'action' => 'admin_index'));		
		Router::connect('/quizmodules/written/add', array('plugin'=>'Quizmodules','controller' => 'WrittenAnswer', 'action' => 'admin_add'));
		Router::connect('/ajax/quiz/element/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'getElementAsAjax'));

		Router::connect('/my/flashcards/edit/*', array('plugin'=>'Quizmodules','controller' => 'flashcards', 'action' => 'admin_edit'));
		Router::connect('/my/flashcards/*', array('plugin'=>'Quizmodules','controller' => 'flashcards', 'action' => 'admin_index'));
		//----------------------
		// Router::connect('/Quizmodules/Wordselects', array('plugin'=>'Quizmodules','controller' => 'Wordselects', 'action' => 'admin_index'));
		Router::connect('/Quizmodules/Wordselects/add/*', array('plugin'=>'Quizmodules','controller' => 'Wordselects', 'action' => 'admin_add'));
		Router::connect('/Quizmodules/Wordselects/add/*', array('plugin'=>'Quizmodules','controller' => 'Wordselects', 'action' => 'admin_add'));
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		Router::connect('/my/quiztextslides/edit/*', array('plugin'=>'Quizmodules','controller' => 'Quiztextslides', 'action' => 'admin_edit'));
		Router::connect('/my/quiztextslides/*', array('plugin'=>'Quizmodules','controller' => 'Quiztextslides', 'action' => 'admin_index'));
		Router::connect('/my/multichoices/addrow/*', array('plugin'=>'Quizmodules','controller' => 'Multichoices', 'action' => 'admin_addrow'));
		Router::connect('/my/multichoice', array('plugin'=>'Quizmodules','controller' => 'Multichoices', 'action' => 'admin_index'));
		Router::connect('/my/multichoices/', array('plugin'=>'Quizmodules','controller' => 'Multichoices', 'action' => 'admin_index'));
		Router::connect('/my/multichoices/add', array('plugin'=>'Quizmodules','controller' => 'Multichoices', 'action' => 'admin_add'));
		Router::connect('/my/multichoices/add', array('plugin'=>'Quizmodules','controller' => 'Multichoices', 'action' => 'admin_add'));
		Router::connect('/my/multichoices/edit/*', array('plugin'=>'Quizmodules','controller' => 'Multichoices', 'action' => 'admin_edit'));
		Router::connect('/my/multichoices/answers/*', array('plugin'=>'Quizmodules','controller' => 'Multichoices', 'action' => 'admin_answers'));
		Router::connect('/my/multichoices/view/*', array('plugin'=>'Quizmodules','controller' => 'Multichoices', 'action' => 'admin_view'));
		Router::connect('/multichoices/view/*', array('plugin'=>'Quizmodules','controller' => 'Multichoices', 'action' => 'view'));
		Router::connect('/Quizmodules/Multichoices/delete/*', array('plugin'=>'Quizmodules','controller' => 'Multichoices', 'action' => 'admin_delete'));
		Router::connect('/my/multichoice/checkanswer/*', array('plugin'=>'Quizmodules','controller' => 'Multichoices', 'action' => 'checkanswer'));
		//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		Router::connect('/ajax/quiz/element/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'getElementAsAjax'));
		//----------------------
		Router::connect('/my/quizzes/autorelocate/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'autorelocate'));
		Router::connect('/quizslides/movedown/*', array('plugin'=>'Quizmodules', 'controller' => 'Quizslides', 'action' => 'movedown'));
		Router::connect('/quizslides/moveup/*', array('plugin'=>'Quizmodules', 'controller' => 'Quizslides', 'action' => 'moveup'));

		// Router::connect('/my/quizslides/relocate/*', array('plugin'=>'Quizmodules','controller' => 'Quizslides', 'action' => 'admin_relocate'));
		// Router::connect('/quizslides/relocate/*', array('plugin'=>'Quizmodules','controller' => 'Quizslides', 'action' => 'admin_relocate'));

		Router::connect('/standalone/quizzes/view/*', array('plugin'=>'Quizmodules','controller' => 'Quizzes', 'action' => 'standalone_view'));