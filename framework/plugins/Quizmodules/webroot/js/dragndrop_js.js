var dragndrop = function(){

	var canvas;// = document.getElementById('canvas');
	var HEIGHT;// = canvas.height;
	var WIDTH;// = canvas.width;
	var context;
	var INTERVAL = 20; 	
	var canvasInterval;
	var timer;
	var redrawCanvas = true;
	var font = '17px sans-serif';
	
	// interactivity stuff
	var mouseX;
	var mouseY;
	var lastMouseX = 0;
	var lastMouseY= 0;	
	var isDragging= 0;
	var textheight = 20; /// fuck I hate html 
	var selected_hotzone = false;
	var selected_punctuation;
	var renderClips = [];	
	var hotZones = [];
	var interactiveClips = [];
	var textclips = [];
	var punctuationclips = [];
	var selectionClip; // selectionClip:TextBlock;
	var isAdmin;		
	var redraw = true;
	var selectionText = '';
	var strokeColor = "#ececec";
	var backgroundColor = "#ececec";

	var punct_text = [
		{ 'text' : "'",'value' : 'single-quote' },
		{ 'text' : ".", 'value' : 'period' },			
		{ 'text' : ",", 'value' : 'comma' },		
		{ 'text' : ";",'value' : 'semicolon' },		
		{ 'text' : ":",'value' : 'colon' },		
		{ 'text' : "!",'value' : 'exclaimation' },		
		{ 'text' : "(",'value' : 'left-paren' },			
		{ 'text' : ")",'value' : 'right-paren' },		
		{ 'text' : "{",'value' : 'left-brace' },		
		{ 'text' : "}",'value' : 'right-brace' },		
		{ 'text' : "[", 'value' : 'left-bracket' },		
		{ 'text' : "]", 'value' : 'right-bracket'},			
		{ 'text' : "-",'value' : 'hyphen'},		
		{ 'text' : '"','value' : 'quote'} 
	];

	function _drawDraggedCharacter(tile){
		//context.fillStyle = 'red';							
		var t = _checkForClosePoints( 60 );		
		if(!t){
		}else{
			//set styles and draw arrow
			context.beginPath();				    
		    context.strokeStyle =  '#a1a0a0';    
			context.lineWidth = 1;				    
			context.strokeStyle =  'red';    					
			//draw arrow
			var arrowPointY = t.y+4;
			var arrowPointLength = 8;				
			var arrowShaftLength = 12;				
			var arrow_width = 3;				
			var letter_bottom = arrowPointY+12;				
			t.x+=3
			context.moveTo(mouseX+3, mouseY+22);
			context.lineTo(t.x, arrowPointY-arrowShaftLength);
			//context.lineTo(t.x, arrowPointY-arrowShaftLength);
			context.lineTo(t.x, arrowPointY)
			context.lineTo(t.x-arrow_width, arrowPointY-arrowPointLength)						
			context.moveTo(t.x, arrowPointY);
			context.lineTo(t.x+arrow_width, arrowPointY-arrowPointLength)						
			context.moveTo(t.x, letter_bottom);
			context.lineTo(t.x-arrow_width, letter_bottom);
			context.lineTo(t.x+arrow_width, letter_bottom);
		    context.stroke();
		}			
		selectionClip.draw();	
	}



	function _checkForClosePoints(range){
		function DistanceTwoPoints(x1, x2,  y1, y2){
		    var dx = x1-x2;
		    var dy = y1-y2;
		    return Math.sqrt(dx * dx + dy * dy);
		}
		var shortestPoint;
		var _x;
		var _y;
		selected_hotzone = false;
		for (var i = 0; i < hotZones.length; i++) {
			if(
				Math.abs( hotZones[i].x - mouseX ) < range &&
				Math.abs( hotZones[i].y - mouseY ) < range
				){
					var g = hotZones[i];
					if(!shortestPoint){
						shortestPoint = Math.abs(DistanceTwoPoints(mouseX, g.x, mouseY , g.y));	
						selected_hotzone = g;
					}
					var c  = Math.abs(DistanceTwoPoints(mouseX, g.x, mouseY , g.y));
					if(c < shortestPoint){
						shortestPoint = c;
						selected_hotzone = g;
					}
				}
		};
		if(selected_hotzone){
			return selected_hotzone;
		}
		return false
	}



	function _initTextField(val){
		textclips = []
		renderClips = [];
		hotZones = [];
		interactiveClips = interactiveClips ? interactiveClips : [];				
		var spaceBefore = new TextBlock;
		spaceBefore.type = "before"
		spaceBefore.setText(" ");
			//--
		renderClips.push( spaceBefore );
		hotZones.push( spaceBefore );		

		for (var i = 0; i < val.length; i++) {
			//--
			var tile = new TextBlock;
			tile.type = "text"
			tile.setText(val[i]);
			//--
			var spaceAfter = new TextBlock;
			spaceAfter.setText(" ");
			textclips.push(tile);
			spaceAfter.type = 'after';	
			//--
			renderClips.push( tile );
			renderClips.push(spaceAfter)										
			hotZones.push( spaceAfter );		
			interactiveClips.push(spaceAfter);
		}		
		_needsRedraw();		
	}

	function _needsRedraw(){
		redraw = true;
		_draw();
	}


	function _applyAdminListener(wordArray){
		letterPlacements = [];
		//DragndropDescription is the editable field. 
		$("#DragndropDescription").keyup(function(){
		 var $input = $(this)
		  if (timer) {
		    clearTimeout(timer);
		  }
		  timer = setTimeout(function () {
		  	$("#DragndropAnswer").val('');
			public_api.init($("#DragndropDescription").val());
			_needsRedraw();			 			
		  }, 300);
		});
		_needsRedraw()
	}


	function _drawParagraph(){
		var t;
		var xposStarting = 4;				
		var xPos = xposStarting;
		var yPos = 100;		
		var ySpacing = 25;
		var xSpacing = -2.75;

			function wordExceedsBounds(_t,xPos){
				var _xp = xPos+xposStarting;
				//get current xpos and increment it by the width of each letter, then check that value against width
				var _string = '';
				for (var i = _t+1; i < renderClips.length; i++) {
					var tile = renderClips[i];	
					_string +=tile.text;
					//
					// reached the next word
					if(!runonce){
						if(tile.type == "text" && tile.text == " "){
						}						
					}
					if(tile.type == "text" && tile.text == " "){
						if(_xp+(xposStarting*2) > WIDTH){
							return true;
						}
						return false;
						continue;
					}
					textMetrics = context.measureText(tile.text);		
					_xp+=textMetrics.width+xSpacing;
				}				
				return false;
			}

		for (var i = 0; i < renderClips.length; i++) {
			t = renderClips[i];
			// setting x and y position.
			if(t.text == " "){
				if(wordExceedsBounds(i,xPos)){
					 xPos = xposStarting;
					 yPos += ySpacing;							
				}
			}					
			t.x = xPos;
			t.y = yPos;					
			xPos += t.width +xSpacing ;				
		}		
		HEIGHT = yPos+100;
		// redraw outline,
		$("#canvas-container").css({'height' : yPos+100 });															
		context.fillStyle = "#ffffff";
		context.strokeStyle = strokeColor;
		context.lineWidth = '1';
		context.fillRect(0, 0, WIDTH, HEIGHT);
		context.strokeRect(0, 0, WIDTH, HEIGHT);		
		// draw renderClips
		for (var i = 0; i < renderClips.length; i++) {
			t = renderClips[i];
			t.draw();
		}
		runonce = true

	}	
	
	var runonce = false;

	function _getMouse(e){
		var element = canvas;
		var offsetX = 0;
		var offsetY = 0;
		// Calculate offsets
		if (element.offsetParent) {
			do {
				offsetX += element.offsetLeft;
				offsetY += element.offsetTop;
			} while ((element = element.offsetParent));
		}	
		// Calculate the mouse location
		mouseX = e.pageX - offsetX;
		mouseY = e.pageY - offsetY;
	}				

	function _initPunctuationNav(){
		var max_width = 400;
		var xposStarting = 10;
		var tile;
		var textMetrics;	
		var xPos = xposStarting;
		var yPos = 10;
		var xSpacing = 20;
		var ySpacing = 22;		
		punctuationclips = [];
		var obj;
		interactiveClips = interactiveClips ? interactiveClips : [];		

		for (var i = 0; i < punct_text.length; i++) {
			tile = new TextBlock;
			obj = punct_text[i];
			tile.text = obj.text;
			tile.value = obj.value;
			tile.type = "nav"
			punctuationclips.push(tile);
			textMetrics = context.measureText(tile.text);	
			
			if(xPos+textMetrics.width+xposStarting>max_width){
				xPos = xposStarting;
				yPos += ySpacing;
			}

			context.fillStyle = 'black';							
			context.fillText(tile.text, xPos, yPos + 2);
			tile.x = xPos;					
			tile.y = yPos;	
			tile.width = textMetrics.width;
			tile.height = textheight; //textMetrics.height;
			xPos+=textMetrics.width+xSpacing;
			interactiveClips.push(tile);			
		}
		selectionClip = new TextBlock;
		selectionClip.type = 'dragging';
	}

	



	function _drawNav(){
		context.fillStyle = backgroundColor;
		context.strokeStyle = strokeColor;
		context.lineWidth = '1';
		context.fillRect(0, 0, WIDTH, 39);
		context.strokeRect(0, 0, WIDTH, 37);
		for (var i = 0; i < punctuationclips.length; i++) {
			punctuationclips[i].draw();
		}

	}

	function _draw(){	
		if(!document.getElementById(canvasName)){
			clearTimeout(timer);
			clearInterval(canvasInterval);
		}
		//------------		
		if(!redraw){return;}

		context.clearRect(0, 0, canvas.width, canvas.height);		
		_drawParagraph();
		_drawNav();
		//------------
		if(isDragging){
			document.getElementById(canvasName).style.cursor = "none";											
			_drawDraggedCharacter()
		}else{
			//document.getElementById('canvas').style.cursor = "default";											
		}			
		redraw = false;

	}

	function _mouseDown(e){
		_getMouse(e);
		for (var i = 0; i < interactiveClips.length; i++) {
			interactiveClips[i].mouseDown(e);
		}		
		_needsRedraw();;		
	}
	function _mouseUp(e){
		_getMouse(e);		
		for (var i = 0; i < interactiveClips.length; i++) {
			interactiveClips[i].mouseUp(e);
		}				
		if(selected_hotzone){
			selected_hotzone.setText(selectionText);
		}
		_updateFields()
		document.getElementById(canvasName).style.cursor = "default";													
		_needsRedraw();;				
	}
	function _mousemove(e){
		_getMouse(e);	
		for (var i = 0; i < interactiveClips.length; i++) {
			interactiveClips[i].mouseOver(e);
		}							
		_needsRedraw();;		
	}
	function _textToId(g){
		for (var i = 0; i < punct_text.length; i++) {
			if(g == punct_text[i].text){
				return punct_text[i].value;
			}	
		};
		return false;
	}


	function _idToText(g){
		for (var i = 0; i < punct_text.length; i++) {
			if( g == punct_text[i].value ){
				return punct_text[i].text;
			}	
		};
		return false;
	}

	function _updateFields(){
		var B = _getCurrentPlacements();
		$("#DragndropAnswer").val(B)
		$("#DragndropAnswer").attr("value",B);
	}

	function _getCurrentPlacements(){
		var ar = [];
		for (var i = 0; i < hotZones.length; i++) {
			var B = _textToId(hotZones[i].text);
			if(B){
				ar.push('{"p":"' +B+'", "i":'+i+' }')
			}
		};
		return ar.join('~');
	}


	function _resize(){
		// context.clearRect(0, 0, WIDTH, HEIGHT);
		var s = $("#"+canvasName).parent().width();
		//WIDTH = s-300;
		//HEIGHT =400;// $("#instructions").height();
		//--
		canvas.width = s;
		canvas.height = HEIGHT;
		_needsRedraw();								
	}



	//public parts
	var public_api = {
		init:function(g, _canvas, _isAdmin ){
//			var _isAdmin = true;
			canvasName = _canvas;
			isAdmin = _isAdmin ? _isAdmin : false;
			canvas = document.getElementById(_canvas);
			HEIGHT = $("#"+_canvas).parent().width();
			WIDTH = $("#"+_canvas).parent().width();
			canvas.width = $("#"+_canvas).parent().width();
			
			context = canvas.getContext('2d');
			context.textBaseline = 'top';
			_initTextField(g.split(''));
			_initPunctuationNav();
			canvasInterval = setInterval(_draw, INTERVAL);
			document.onmousedown = _mouseDown;	           
			document.onmouseup = _mouseUp;	           			
			document.onmousemove = _mousemove;	           			
			public_api.showThesePlacements([]);			
			if(isAdmin){
				_applyAdminListener();
				if($("#DragndropAnswer").val().split('~')[0].length!=0){
					public_api.showThesePlacements( $("#DragndropAnswer").val().split('~'));
				}
				
			}
			$( window ).resize(function() {
				_resize()
			});
			_resize();
		},

		showAnswers:function(){
			public_api.showThesePlacements($("#DragndropCorrectAnswer").val().split('~'));
		},
		showThesePlacements:function(placementsArray){
			for (var i = 0; i < hotZones.length; i++) {
				hotZones[i].setText(' ');
			}
			for (var i = 0; i < placementsArray.length; i++) {
				var obj = $.parseJSON(placementsArray[i])				
				hotZones[obj.i].setText(_idToText(obj.p));
			};
			_needsRedraw();		
		},
		getCurrentPlacements:function(){ return _getCurrentPlacements()},
	}


	function TextBlock(val,text){		
		this.text = '';
		this.value = 0;
		this.type = undefined;
		this.index = 0;
		this.selected = false;
		this.x = 0;
		this.y = 0;
		this.width = 0;
		this.height = 22;

		this.setText = function(val){
			this.text = val
			context.font = font;			
			this.textMetrics = context.measureText(this.text);
			this.width = this.textMetrics.width;
		}

		this.draw=function(){
			context.fillStyle =  'black';  			
			switch(this.type){
				case('dragging'):
					context.fillStyle =  'red';    									
					this.text = selectionText;
					this.x = mouseX;
					this.y = mouseY;					
				break;
				case('before'):
				case('after'):
					context.fillStyle =  'red';    									
				break;
				case('nav'):
					if(this.selected){
						context.fillStyle =  '#ececec';    										
					}
				break;
				default:

				break;
			}	
			context.font = font;
			context.textBaseline = 'top';			
			context.fillText(this.text, this.x, this.y);
		}

		this.drawHighlighted = function(e){
			context.fillStyle = '#ececec';	
			context.fillText(this.text, this.x, this.y + 2);
		};

		this._hitTest = function(){
			return mouseX > this.x 
 				&& mouseX < this.x + this.width
				&& mouseY > this.y
 				&& mouseY < this.y + this.height;
		};

		this.mouseDown=function(e){
			if(this._hitTest()){
				selected = true;
				isDragging = this.selected = true;
				selected_punctuation = this;
				selectionText = this.text;				
				switch(this.type){
					case('dragging'):			
					break;
					case('before'):
					case('after'):
					 this.text = '';
					break;
					case('nav'):
					break;
					default:
				break;
				}	
 			} else{
 				this.selected = false;
 			} 
 			return this.selected;
		}	
		this.mouseUp=function(e){
			isDragging = this.selected = false;
		}
		this.mouseOver=function(e){
			if(this._hitTest()){
				document.getElementById(canvasName).style.cursor = "pointer";
			}
		}		
	}
	// -------------------------------------------------------------------------------------------------------------------------
	//End Textblock ------------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	return public_api;
}
