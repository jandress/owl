<?php if (!empty($isAjax)): ?>
	<?php if (empty($next)): ?>
			function next(val){ 
				ajax_helper.loadUrl( "/quizzes/end/")				
			}
	<?php else: ?>		
		function next(val){ 
			ajax_helper.loadUrl("<?php echo $nurl; ?>")				
		}		
	<?php endif ?>
<?php else: ?>
	<?php if (empty($next)): ?>
			function next(val){ window.location = "../quizzes/end/"; }
	<?php else: ?>		
			function next(val){ window.location = "<?php echo $nurl; ?>"; }
	<?php endif ?>
<?php endif ?>