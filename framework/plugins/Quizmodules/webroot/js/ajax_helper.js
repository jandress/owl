var ajaxhelper = function() {       
/*
		// removed automatic initialization
		var ajax_helper = ajaxhelper();	
		ajax_helper.applyAjaxListenerToLinks(_targetButton,_targetDiv,_ldrString );
		//-----
		var ajax_helper2 = ajaxhelper();	
		ajax_helper2.initNewHelper(_targetDiv2, _targetButton2, _loaderString2 );	
		ect...
		//-----
*/
			
	var function_object = {				
		loaderString : '',
		targetDiv:"#ajax_container",
		territoryPriceDiv:"#PricingPrice",
		targetSelect: '',
		nocache:false,
		onLoadComplete: function(){},		
		onloadBegin: function(){},

		loadUrl: function(_url){
			//empty out the div and display a preloader
			if (function_object.loaderString!='') {
				console.log('function_object.loaderString: '+function_object.loaderString);
				$(function_object.targetDiv).empty();
				$(function_object.targetDiv).html(function_object.loaderString);
			};
			function_object.onloadBegin();

			$.ajax({
				url: _url,
				context: $(function_object.targetDiv),
				success: function(data){
				//	console.log('success: '+data);
					//return;
					$(this).empty();
					$(this).html(data);  
					function_object.applyAjaxListenerToLinks();
					function_object.onLoadComplete();
				}
			});
		},
		
		applyAjaxListenerToLinks: function(_targetButton,_targetDiv,_ldrString){
			// CHECK DEFAULTS, (also check if    g = a || b works here)
			// also change this to a single param
			function_object.targetButton = typeof( _targetButton ) == "undefined" ? function_object.targetButton : _targetButton;			
			function_object.targetDiv = typeof( _targetDiv ) == "undefined" ?  function_object.targetDiv : _targetDiv;			
			function_object.loaderString = typeof( _ldrString ) == "undefined" ? function_object.loaderString : _ldrString;						

			var g = $(function_object.targetDiv);
			$(function_object.targetButton).bind('click', function() {
				var n = $(function_object.targetDiv)
				$(function_object.targetDiv).empty();	
				function_object.loadUrl($(this).attr('href'));
				//console.log(function_object);
			  return false;
			});	
		}	
		
		
		,convertASelectToJSON: function(_targetSelect){			
			var items = $(_targetSelect+" > option").map(function() {
			    var opt = {};
			    opt[$(this).val()] = $(this).text();
			    return opt;
			}).get();
			return items;
		}
			
			
		,applyAjaxListenerToSelect: function(_targetSelect,_targetDiv,_urlbasebeforeparams,_ldrString){
			// CHECK DEFAULTS, (also check if    g = a || b works here)
			// also change this to a single param
			function_object.targetSelect = typeof( _targetSelect ) == "undefined" ? function_object.targetSelect : _targetSelect;			
			function_object.targetDiv = typeof( _targetDiv ) == "undefined" ?  function_object.targetDiv : _targetDiv;			
			function_object.loaderString = typeof( _loaderString ) == "undefined" ? function_object.loaderString : _loaderString;						
			$(function_object.targetSelect).change(function(e) {
				var val = e.target.value;
				$(function_object.targetDiv).wrapInner('<div id="tmp" />');
					console.log($('#tmp').width());
					$(function_object.targetDiv).empty();						
				 	function_object.loadUrl(_urlbasebeforeparams+"/"+val);								
				// });
			  return false;
			});
		}
			

	}
	return function_object;
}; 