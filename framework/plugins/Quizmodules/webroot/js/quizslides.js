var responseDelay = 1000;
var next;
var previous;		
var index = 0;

 //ajax_helper = main_content_ajax_helper;
// ajax_helper.applyAjaxListenerToLinks("#start_btn","#quiz_ajax_content",'');
//ajax_helper = main_content_ajax_helper;
var debugging = false;


function nextSlide(){
	//ifDebugLog('nextSlide()')
	//console.log("nextSlide: "+ar[index]);
	main_content_ajax_helper.loadNext(ar[index]);
	index++;
//	alert('dont forget to re-enable next slide in quizslides.js');
}
function previousSlide(){
	//ifDebugLog('previousSlide()')
	main_content_ajax_helper.loadUrl(previous);		
}


function getDelay(){
	return responseDelay;
}



function ifDebugLog(str){
	if(debugging){
		//console.log(str);
	}
}


function displayResponse(response) {
	var actv;
	var inactv;		
	ifDebugLog('displayResponse: '+response);
    if (response == '1') {
		ifDebugLog("response == '1'")
		actv = "#result_correct";
		inactv = "#result_incorrect";
    } else {
		actv = "#result_incorrect";
		inactv = "#result_correct";
    }
	ifDebugLog('$(actv).show(): '+$(actv));
    $(actv).show();
    $(inactv).hide();
	ifDebugLog('getDelay(): '+getDelay())
	$(actv).delay(getDelay()).fadeOut(1000,function(){
		nextSlide();
	});
};


function submitAnswer(val){
	console.log('submitAnswer: '+val)
	//ifDebugLog('submitAnswer');
	$("#checking_msg").show();
	var targetDiv = "#message_area";
	//$(targetDiv).empty().html('<div class="alert"><h5><em>Loading...</em></h5></div>');
	$(targetDiv).empty();		
	showQuizLoader()	
	$.ajax({
		url:val.url,
		context: $(targetDiv),
		type: "POST",
		data: {response:val.answer},			
		success: function(response){
			ifDebugLog('response: '+response)
			hideQuizLoader()			
			$("#checking_msg").hide();
			$(targetDiv).empty();
			displayResponse(response);
		}
	});		
	$("#submit").addClass('disabled');
	$("#submit").removeClass('btn-success');	
}


function showQuizLoader(){
	//ifDebugLog('showQuizLoader()');
	 $("#quiz_loader_graphic").css('display',"block");
	 $("#quiz_loader_graphic").css('z-index', 1020)	
	//--
	// $("#quiz_loader_graphic").css('width',$("#quiz_container").width());	
	// $("#quiz_loader_graphic").css('height',$("#quiz_container").height());
		
	// $("#quiz_loader_gif").css('margin-left',($("#quiz_container").width()/2)-400);
	// $("#quiz_loader_gif").css('margin-top',($("#quiz_loader_graphic").height()/2)-20);
	//--
	// #quiz_loader_graphic_background
};
function hideQuizLoader(){
	//ifDebugLog('hideQuizLoader()');
	$("#quiz_loader_graphic").css('display',"none");	
};

// ajax_helper.onloadBegin = showQuizLoader;
// ajax_helper.onLoadComplete = hideQuizLoader
