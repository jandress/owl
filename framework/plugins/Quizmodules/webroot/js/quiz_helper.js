var quizHelper = function() {       


	var function_object = {					
		
		addRowWithRowCount: function(_url,element){
			function_object.addRow(_url+'/key:'+$(element).length);
		},

		addRow: function(_url){
			$.ajax({
				url: _url,
				context: $('#foo > tbody:last'),
				success: function(data){
					$('#answers_table > tbody:last').append(data);					
				//	console.log(data);
				}
			});
		},
		removeRow:function(rowId){
			if($('.multichoice_answer_row').length>1){
				$('#'+rowId).remove();						
			}else{
				alert('Fortune Cookie says: Unlike in life, this quiz can have no questions without answers. Just edit the remaining field.');
			}
		},
		
		displayResponse:function(val){
			$('#response_message').css({display:'block'});				
			if(val){					
				$('#result_incorrect').css({display:'none'});			
				$('#result_correct').css({display:'block'});					
				$('#response_message').removeClass('alert-danger').addClass('alert-success').animate({opacity:1}).delay(2000).animate({opacity:0},{complete:next});
			}else{
				$('#result_incorrect').css({display:'block'});			
				$('#result_correct').css({display:'none'});					
				$('#response_message').removeClass('alert-success').addClass('alert-danger').animate({opacity:1}).delay(2000).animate({opacity:0},{complete:function(){
					$('#question_container').animate({opacity:0},{complete:next})																			
				}});					
			}			
		}
	}
	
	return function_object;
		
}; 

var quiz_helper = quizHelper();
