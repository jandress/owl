var sortable = function() {

	var fo = {
		
		selectedPunctuation : ',',
		letterPlacements : [],	
		timer: null,
		selectedpiece:null,
		answer:null,
		admin:false,
		
		setNavDisabled:function(disabled){
			//enables / disables sorting nav delete and edit buttons.			
			if(disabled){
				$("#sortable_nav_delete_btn").addClass('disabled');								
				$("#sortable_nav_edit_btn").addClass('disabled');				
			}else{
				$("#sortable_nav_delete_btn").removeClass('disabled');								
				$("#sortable_nav_edit_btn").removeClass('disabled');
			}
		},

		deselect:function(clip){
			$( clip ).removeClass('selected');	
			fo.setNavDisabled(true);
			fo.selectedpiece = null;
		},


		setUpDrag: function(opt) {
			var draggedClip = $( opt );
			// disable text selection			
			// all this logic occurs within a mousedown listener.   
			//	$(opt).disableSelection();
			$(draggedClip).on("mouseover", function(e) {
		    	e.preventDefault(); // supposed to prevent that selection issue.  
			});				
			$(draggedClip).on("mousedown", function(e) {				
		    	e.preventDefault(); // supposed to prevent that selection issue.  
				if($( e.currentTarget ).hasClass('selected')){
					//if is selected, deselect. 
					fo.deselect(e.currentTarget);
					return;
				}
				fo.setNavDisabled(false); //enables / disables sorting nav delete and edit buttons.
				fo.selectedpiece = $(e.currentTarget).attr('id').replace("sortablepiece_", "");

				$("#sortable_nav_edit_btn").attr({"href":'/my/sortingpieces/edit/'+fo.selectedpiece});
				//--
				//loop through the array of sortable pieces to deselect all the other pieces when you click on it. 
				var n =  $('.sortable_piece');
				
				for (var i=0; i < n.length; i++) {
					if( n[i] == e.currentTarget){ continue; } // skip the selcted piece
					if($( n[i] ).hasClass('selected')){
						$(n[i]  ).removeClass('selected');								
					}
				};

				// dragging code here
				var targ = this;
				//establish drag offsets / setting up for drag.
		        var $drag = $(this).addClass('draggable selected');
			    var z_idx = $drag.css('z-index');
			 	var drg_h = $drag.outerHeight(); 
				var drg_w = $drag.outerWidth();
				var pos_y = ($drag.offset().top + drg_h - e.pageY)-25;
				var pos_x = $drag.offset().left + drg_w - e.pageX+2;
			    $drag.css('z-index', 1000).parents();			
				// the actual dragging
				
				//--
		        $(targ).offset({
		            top:e.pageY + pos_y,
		            left:e.pageX + pos_x - drg_w
		        });
		
				$("body").on("mousemove", function(e) {
			        $(targ).offset({
			            top:e.pageY + pos_y ,
			            left:e.pageX + pos_x - drg_w
			        })
			       	//fo.sortPieces(); 
			    });
			
				// stop drag, remove listeners, remove styling and call sort pieces. 
				$("body").on("mouseup", function(e) {
					$("body").unbind('mousemove'); 								
					$("body").unbind('mouseup');														
		            $(draggedClip).removeClass('draggable').css('z-index', z_idx);
					fo.sortPieces();
		        });
		        // --
	        });	//end mousedown listener 		
	    },
	
	
	
		orderBySmallest: function(arr){
			// just reurns a 
			var order = [];	
			function divsToArray(n){
				// not sure whats up with this one, why does it need to be 
				var p = [];
				for (var i=0; i < n.length; i++) {
					p.push(n[i]);
				};
				return p;
			}			
			function getSmallest(ar)
			{
				// recursve subrouting to assemble the array of pieces in order from smallest to largest 
				if(ar.length==0){
					return order;
				}
				var smallest = ar[0];
				for (var i=0; i < ar.length; i++) {
					if(ar[i].offsetLeft < smallest.offsetLeft ){
						smallest = ar[i];
					}
				};
				order.push(smallest);
				ar.splice(ar.indexOf(smallest),1)
				return 	getSmallest(ar);
			}
			return getSmallest(divsToArray(arr))
		},		


		sortPieces: function(){
			var orderedList = fo.orderBySmallest($('.sortable_piece'));
			var answer = []; //array of numerical ids
			var sortingPieceObject; 
			for (var i=0; i < orderedList.length; i++) {
				//--
				answer.push($(orderedList[i]).attr('id').replace("sortablepiece_", "")); //numerical ids, for setting answer 
				sortingPieceObject = $("#Sortingpiece"+answer[answer.length-1]);
				//--				
			};
			if(fo.admin){
				 $("#SortingAnswer").val(answer);
			}else{
				$("#SortingSubmission").val(answer); // update the form field if needed
			}
			// clear the stage, and 
			///if you want this to work on mousemove, this part needs to only execute after passing a conditional. 
			$("#sortable_stage").empty();
			for (var i=0; i < orderedList.length; i++) {
				$("#sortable_stage").append($(orderedList[i]).css({position:'relative', 'top':'', 'left':''}));
			};	
			fo.setUpDrag( orderedList );
		},
	
		serializeSubmission: function(){
			var b = fo.orderBySmallest($('.sortable_piece'));
			var t = [];
			for (var i=0; i < b.length; i++) {
				t.push($(b[i]).attr('id').replace("sortablepiece_", ""));
			};
			return t;
		}
	} // end fo
	return fo;
}





