function ImageBlock(){	

	var pathToImages = '/media/sorting/';
	
	var scope = {
	
		data:null,
		context:null,
		id:null,
		siblings:[],
		text:'',
			
		src:'',
		value:0,
		type:"image",

		index:0,
		selected:false,
		x:0,
		y:0,
		width:0,
		height:0,
		//
		loaded:false,
		imageObj:null,
		style:"",	
		scope:this,
		
		init:function(_context, _text, _id, _siblings){
			scope.context = _context;
			scope.text = _text;
			scope.id = _id;			
			scope.data =  {  'text' : _text, 'id' : _id, 'siblings: ' : _siblings };
			scope.CreateImage(_text);			
		},
		
		
		CreateImage:function (src){
			scope.text = src;
			scope.imageObj = new Image()
			scope.imageObj.onload = function() {
				scope.loaded = true;
				scope.width = this.naturalWidth+4;
				scope.height = this.naturalHeight;
		    };	
			scope.imageObj.onerror = function() {
				scope.CreateImage('/img/loading.gif');
		    };	
			scope.src = scope.imageObj.src = pathToImages+src;
		},
		
		// setData:function(src, id){	
		// 	scope.id = id;
		// 	scope.CreateImage(pathToImages+src)        
		// 	scope.text ="Image: "+id+", src: "+src				
		// },

		draw:function(){
			var context = scope.context
			if(scope.loaded){
				switch(scope.style){
					case('mouseover'):					
						// context.strokeWidth = 2;
						// context.strokeStyle =  '#666666';    														
						// context.rect(x,y,(width-fontSize/2),height);
						// context.stroke();												
					break;
					case('selected'):
						context.globalAlpha   = .2;					
						// context.strokeWidth = 2;
						// context.strokeStyle =  '#BD362F';    														
						// context.rect(x,y,(width-fontSize/2),height);
						// context.stroke();
					break;
					default:
					break;
				}
				context.drawImage(scope.imageObj, scope.x, scope.y );	
				context.globalAlpha   = 1;									
			}
			//------------
			return {				
				'x' : scope.x, 
				'y' : scope.y, 
				'width' : scope.width,  
				'height': scope.height,
				'value':scope.value,
				'type':'image'				
			}
		},	

		selection:function(val){
			
		},

		mouseDown:function(e){
			scope.style = "selected"
		},

		mouseUp:function(e){
			scope.style = ""
		},

		mouseOver:function(e){},

		mouseOut:function(e){}	,		

		indicateRollOff:function(){},

		indicateRollOver:function(){},	

		hitTest:function(){
			return mouseX > this.x 
 				&& mouseX < this.x + this.width
				&& mouseY > this.y
 				&& mouseY < this.y + this.height;
		},

		getTargetPoint:function(){
			var left = x
			var right = x+width
			var center = x + width/2
			//------------------------------------------				
			var target = {"x": 0, "y":y}
			//------------------------------------------				
			if( mouseX >  center  ){					
				rolledOverSide = "right";
				target.x = right;
			}else{
				target.x = left;
				target.y = y;				
				rolledOverSide = "left";					
			}
			return {
				"x":target.x,
				"y":target.y
			}
		},			

		getPieceGroupBeginning : function(){
			// returns the first clip in the group
			return scope;
		},
		
		getPieceGroupEnd : function(){
			// returns the last clip in the group				
			return scope;
		},
		
		getPieceGroupBeginningPoint : function(){
			// returns the first clip in the group
			return scope.x
		},		
		getPieceGroupEndPoint : function(){
			// returns the first clip in the group
			var t = scope;
			return t.x+t.width
		},
		
		
		getPieceTop : function(){
			return scope.y
		},
		
		getPieceBottom : function(){
			return scope.y+scope.height
		},	


	}
	scope.siblings = [scope];

	return scope;	
}