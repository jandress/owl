function TextBlock(){	
	
	var scope = {
		siblings:null,
		scope:this,
		id:null,		
		data: null,
		text : null,
		//--
		fontSize:14,
		font : '14px sans-serif',
		leading : 5,		
		//--
		value : 0,
		type : "Text",
		index:0,
		selected:false,
		x:0,
		y:0,
		width:0,
		height:24,		
		style:"",
		width:0,
		context:null,
		//----------			
		draw:function(){
			if( !scope.context ) return;
			switch(scope.style){
				case('selected'):
					scope.context.fillStyle =  '#ffffff';    														
					scope.context.fillRect(this.x-2,this.y,(this.width)+2,this.height);
					scope.context.fillStyle =  '#ececec';
					scope.context.font = scope.font;
					scope.context.textBaseline = 'top';			
					scope.context.fillText(scope.text, scope.x, scope.y);							
				break;
				case('mouseover'):
					scope.context.fillStyle =  '#ececec';    														
					scope.context.fillRect(this.x-2,this.y,(this.width)+2,this.height);
					scope.context.fillStyle =  '#666666';
					scope.context.font = scope.font;
					scope.context.textBaseline = 'top';			
					scope.context.fillText(scope.text, scope.x, scope.y);					
				break;
				case('targeted'):
					// scope.context.fillStyle =  '#BD362F';    														
					// scope.context.fillRect(this.x-2,this.y,(this.width)+2,this.height);
					scope.context.fillStyle =  '#BD362F';
					scope.context.font = scope.font;
					scope.context.textBaseline = 'top';			
					scope.context.fillText(scope.text, scope.x, scope.y);					
				break;					
				default:
					scope.context.fillStyle =  '#666666';
					scope.context.font = scope.font;
					scope.context.textBaseline = 'top';			
					scope.context.fillText(scope.text, scope.x, scope.y);				
				break;
			}						
		},
		
		
		init:function(_context, _text, _id, _siblings){
			scope.context = _context;
			scope.text = _text;
			scope.id = _id;
			scope.siblings = _siblings;
			scope.data  = 	{  'text' : _text, 'id' : _id, 'siblings: ' : _siblings };
			scope.context.fillStyle =  '#ffffff';
			scope.context.font = scope.font;			
			scope.context.fillText(scope.text, scope.x, scope.y);						
			var textMetrics = scope.context.measureText( scope.text );
			scope.width = textMetrics.width+4;		
		},
		
		mouseDown:function(){
			for (var i=0; i < scope.siblings.length; i++) {
				scope.siblings[i].style = "selected";
			};			
		},
		
		mouseUp:function(){
			if(scope.style != "targeted"){
				scope.style = ""						
			}	
		},
		
		mouseMove:function(){},
		
		mouseOver:function(){
			//scope.style = "mouseover"
			for (var i=0; i < scope.siblings.length; i++) {
				scope.siblings[i].style = "mouseover";
			};
		},			

		mouseOut:function(){
			scope.style = ""
		},			
		
		selection:function(val){
			for (var i=0; i < scope.siblings.length; i++) {
				scope.siblings[i].style = 'targeted';
			};
		},

		hitTest:function(){
			return mouseX > this.x 
 				&& mouseX < this.x + this.width
				&& mouseY > this.y
 				&& mouseY < this.y + this.height;
		},			

		getTargetPoint:function(){
			var left = this.getPieceGroupBeginning();
			var right = this.getPieceGroupEnd();	
			//------------------------------------------				
			var center = left.x + ((right.x+right.width)-left.x)/2
			var target = {"x":0 ,"y" : 0};
			//------------------------------------------
			if( mouseX > ( center ) ){
				rolledOverSide = "right";
				target.x = right.x+right.width-3;
				target.y = right.y;
			}else{
				target.x = left.x-3;
				target.y = left.y;				
				rolledOverSide = "left";					
			}
			return {
				"x":target.x,
				"y":target.y
			}
		},			

		getPieceGroupBeginning : function(){
			// returns the first clip in the group
			return scope.siblings[0]
		},
		
		getPieceGroupBeginningPoint : function(){
			// returns the first clip in the group
			return scope.siblings[0].x
		},		
		
		getPieceGroupEndPoint : function(){
			// returns the first clip in the group
			var t = scope.siblings[scope.siblings.length-1]
			return t.x+t.width
		},		
		
		getPieceGroupEnd : function(){
			// returns the last clip in the group				
			return scope.siblings[scope.siblings.length - 1];
		},
		
		getPieceTop : function(){
			return scope.siblings[0].y
		},
		
		getPieceBottom : function(){
			var t = scope.siblings[scope.siblings.length-1]
			return t.y+t.height
		},
		

	}
	return scope;
}