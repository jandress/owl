function DraggedCharacter(canvas, context){	
	
	// Arrow appearance
	var pt;
	var arrowPointY = 10;
	var letter_bottom = arrowPointY+12;
	var arrowPointLength = 6;
	var arrowShaftLength = 10;
	var arrow_width = 3;
	var strokeStyle = '#BD362F';
	var strokeWidth = 1.5
					
	var scope = {							
		dragging:false,			
		imageBlock:new ImageBlock(),
		textBlock:new TextBlock(),		
		tx:0,
		ty:0,
		arrowPosition:'below',
		target : {
			x:11,
			y:43
		},		
		
		init:function(renderClip){
				var t = "";
				if( renderClip.type == "image" ){
					scope.dragging = scope.imageBlock;
					scope.dragging.init(context, renderClip.text, renderClip.id, renderClip.siblings);
				}else{
					if(!renderClip.siblings) return;
					for (var j=0; j < renderClip.siblings.length; j++) {
						t += renderClip.siblings[j].text;
						if(j < renderClip.siblings.length-1){
							t += " ";						
						}
					};
					scope.dragging = scope.textBlock;
					scope.dragging.init(context, t, renderClip.id, renderClip.siblings);
				}			
		},
				
		drag: function(renderClip, pointTo){
			//console.log(pointTo)
			if(scope.dragging){
				scope.dragging.x = mouseX;
				scope.dragging.y = mouseY;			
				scope.dragging.draw()
			}
			scope.target.x = pointTo.x-1;
			scope.target.y = pointTo.y;
			
			if(mouseY > pointTo.y){
				scope.arrowPosition ="below";
			}else{
				scope.arrowPosition ="above";				
			}
			
			scope.drawArrow()
		},	
		
		
		drawArrow: function(){		
				///--				
				context.beginPath();				    					
				context.lineWidth = strokeWidth;
				context.strokeStyle = strokeStyle;		
				//--
				t = {
					x:scope.target.x,
					y:scope.target.y,					
				}
				switch(scope.arrowPosition.toLowerCase()){
					case('below'):
						t.y = scope.target.y+16
						pt = {
							x:t.x,
							y:t.y
						}		
						context.moveTo(t.x, pt.y+arrowShaftLength);
						context.lineTo(t.x, pt.y);
						context.lineTo(t.x-arrow_width, pt.y+arrowPointLength)												
						context.moveTo(t.x, pt.y);
					 	context.lineTo(t.x+arrow_width, pt.y+arrowPointLength)												
						// draw from arrow to draggable
						context.moveTo(t.x, pt.y+arrowShaftLength);
						context.lineTo(scope.dragging.x, scope.dragging.y);						
						// context.lineTo(t.x, t.y);
					break;
					case('above'):
					default:
						pt = {
							x:t.x,
							y:t.y
						}		
						context.moveTo(t.x, pt.y-arrowShaftLength);
						context.lineTo(t.x, pt.y);
						context.lineTo(t.x-arrow_width, pt.y-arrowPointLength)												
						context.moveTo(t.x, pt.y);
					 	context.lineTo(t.x+arrow_width, pt.y-arrowPointLength)
						context.moveTo(t.x, pt.y-arrowShaftLength);
						context.lineTo(scope.dragging.x, scope.dragging.y);							
					break;					
				}
				context.stroke();
			//	scope.drawStrokeToArrow();
		},	

		drawStrokeToArrow: function(){
				context.beginPath();				    					
				context.lineWidth = strokeWidth;
				context.strokeStyle = strokeStyle;	
				t = {
					x:scope.target.x,
					y:scope.target.y,					
				}						




				switch(scope.arrowDirection.toLowerCase()){
					case('up'):
						t.y = scope.target.y+16
						pt = {
							x:t.x,
							y:t.y
						}		
						context.moveTo(t.x, pt.y+arrowShaftLength);
					//	context.bezierCurveTo(scope.dragging.x,scope.dragging.y,200,100,200,20);						
						context.lineTo(scope.dragging.x, scope.dragging.y);						
					break;
					case('down'):
					default:
						pt = {
							x:t.x,
							y:t.y
						}		
						context.moveTo(t.x, pt.y-arrowShaftLength);
						context.lineTo(scope.dragging.x, scope.dragging.y);													
					break;					
				}
				context.stroke();				
		}	
		
	}
	return scope;	
}



