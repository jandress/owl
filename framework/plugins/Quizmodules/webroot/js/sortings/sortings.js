var sorting = function(){		
	
	// GLOBALS....todo, encapsulate these 
	// canvas vars
	var canvas; // = document.getElementById('canvas');
	var HEIGHT; // = canvas.height;
	var WIDTH; // = canvas.width;
	var context; 	
	var canvasInterval;
	var INTERVAL_LENGTH = 20; 	
	var redraw = true;
	// misc interactivity stuff
	//--
	var _data;
	var _originalRawData;
	//--
	var rolledOver = null
	var selected = null
	var rolledOverSide = "left"
	
	var dragging = false;
	var mousedDown = false;	
	var paragraph;
	var draggedCharacter;
	var moveToIndex;	
	var lastNearestPoint = false;	
	var verifyRandom = true;	
	
	
	
	Array.prototype.move = function (old_index, new_index) {
	    if (new_index >= this.length) {
	        var k = new_index - this.length;
	        while ((k--) + 1) {
	            this.push(undefined);
	        }
	    }
	    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
	    return this; // for testing purposes
	};
	
	if(Array.prototype.equals)
	    console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
	// attach the .equals method to Array's prototype to call it on any array
		
	var public_api = {		
	
		onRelocate: function (){},
		onSelect: function (val){},
		
		init: function(json , answer, random){
			console.log('init')
			
			_originalRawData = [].concat(json)						
			if(random){
				randomOrder(json,answer)
			}else{
				orderByAnswer(json , answer)
			}
		}
		
	}
	
	
	function shuffle(array) {
	  var currentIndex = array.length, temporaryValue, randomIndex;
	  // While there remain elements to shuffle...
	  while (0 !== currentIndex) {
	    // Pick a remaining element...
	    randomIndex = Math.floor(Math.random() * currentIndex);
	    currentIndex -= 1;
	    // And swap it with the current element.
	    temporaryValue = array[currentIndex];
	    array[currentIndex] = array[randomIndex];
	    array[randomIndex] = temporaryValue;
	  }
	  return array;
	}
	
	
	function orderByAnswer(_data, _t){
		var ar = [];
		//iterate over the answer and the json data, and create an array of the json items in the order of the answer.
		//----------------------
		if(_t[0 ]== "")	{
			init(_data);		
			return 
		}	
		for (var j=0; j < _t.length; j++) {
			for (var i=0; i < _data.length; i++) {
				if( _t[j] == _data[i].id){
					ar.push( _data[i] );
				}
			};
		};
		// if there are more items in the json than there are in the answer, 
		if( _data.length > _t.length ){
			for (var i = _t.length; i < _data.length; i++) {
				ar.push(_data[i])
				if(i<_data.length-1){
					_t +=","+_data[i].id	
				}
			};
		}
		init(ar);
		return ar;///_data
	}
	
	function randomOrder(data,answer){
		// randomize then call init,
		var originalOrder = [].concat(data);			
		function getIds(_d){
			var or = [];
			for (var i=0; i < _d.length; i++) {
				or.push(_d[i].id);
			};			
			return or;
		}
		function compare(ar1, ar2){
			return ar1.toString() == ar2.toString();
		}
		
		var shuffled_data = shuffle(data);					
		var t = compare(getIds(shuffled_data),answer);
		if(verifyRandom){
			while(compare(getIds(shuffled_data),answer)){
				shuffled_data = shuffle(data);	
			}	
		}
		init(shuffled_data);
	}
	
	function init(json , answer){
		_data = json;
		// set up canvas......
		canvasName = "canvas";			
		canvas = document.getElementById(canvasName);
		HEIGHT = $("#"+canvasName).parent().height();
		WIDTH = $("#"+canvasName).parent().width();
		canvas.width = $("#"+canvasName).parent().width();
		
		context = canvas.getContext('2d');
		context.textBaseline = 'top';
		canvasInterval = setInterval(_draw, INTERVAL_LENGTH);
		//--
		initObjects();
		//--
		document.onmousemove = _mousemove;
		document.onmousedown = _mouseDown;	           
		document.onmouseup = _mouseUp;	           
		//--
		$( window ).resize(function() {
			_resize()
		});
		_resize();
	}
	
	function initObjects(){
		paragraph = InteractiveParagraph(canvas, context);
		paragraph.init(_data);
		draggedCharacter = DraggedCharacter(canvas, context);	
	}
	
	function _draw(){
		if(!document.getElementById(canvasName)){
			clearInterval(canvasInterval);
		}
		if(!redraw){ return; }		
		paragraph.draw();
		if(dragging){
			draggedCharacter.drag(dragging, lastNearestPoint)						
		}		
	}	
	
	//--------------------		
	function getIndexInData(id){
		for (var i=0; i < _data.length; i++) {
			if(_data[i].id==id){
				return i;
			}
		};
	}
	

	function _mousemove(e){
		_getMouse(e);	
		paragraph.mousemove();	
		if(dragging){
			var _p = paragraph.getNearestPoint();
			if(lastNearestPoint != _p){							
				moveFrom = getIndexInData(dragging.id)
				moveTo = paragraph.getPieceIndexForItem(_p) 
				if(moveTo > moveFrom){
					moveTo -= 1
				}
			}
			lastNearestPoint = _p
		}
	}
	
	
	function _mouseDown(e){		
		mousedDown = true	
		_getMouse(e);		
		dragging = paragraph.mouseDown()		
		draggedCharacter.init(dragging);					
	}	

	function _mouseUp(e){
		mousedDown = dragging = false;
		_getMouse(e);			
		var m_up = paragraph.mouseUp()
		if(m_up){
			public_api.onSelect(m_up)			
		}else{
			public_api.onSelect(false)						
		}		
		if(lastNearestPoint ){
			_data.move(moveFrom,moveTo);
			initObjects();			
			public_api.onRelocate(_data)			
			moveFrom = moveTo = lastNearestPoint = false;
		}
	}
		
	function _resize(){
		var s = $("#"+canvasName).parent().width();
		if (s == canvas.width && canvas.height == HEIGHT){
			return;
		}
		canvas.width = s;
		canvas.height = HEIGHT;
		redraw = true;								
	}		

	function _getMouse(e){
		var element = canvas;
		var offsetX = 0;
		var offsetY = 0;
		// Calculate offsets
		if (element.offsetParent) {
			do {
				offsetX += element.offsetLeft;
				offsetY += element.offsetTop;
			} while ((element = element.offsetParent));
		}	
		// Calculate the mouse location
		mouseX = e.pageX - offsetX;
		mouseY = e.pageY - offsetY;
	}

	return public_api;
}