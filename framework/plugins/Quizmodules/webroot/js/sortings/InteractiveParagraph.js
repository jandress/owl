function InteractiveParagraph(canvas, context){	
	
	// constants
	var fontSize = 14;
	var font = fontSize+'px sans-serif';
	var leading = 5;
	// Text & Image Rendering Vars
	var h_padding = 10
	var v_padding = 40	
	var dragging = false;
	
	var scope = {				
		
		renderClips:null,
		pieces:null,
		pieceXPositions: null,
		pieceYPositions: null,
		images:null,
		renderClipsOriginalOrder : [],
		images_loaded : 0,		
		width:400,
		_data:null,
		
		init:function(__data){	

			scope._data = __data;
			scope.pieces = [];
			// converts json database records into TextBlock and ImageBlock objects for canvas manipulation.  breaks up the text pieces into 1 TextBlock for each word with an array that stores it's siblings.
			scope.renderClips = []; // all paragraph objects that get rendered.
			scope.images = []
			var _rci = 0; // render clip iterator.  	
			var xPos = 0;
			for (var i=0; i < scope._data.length; i++) {
				// images
				if(scope._data[i].type.toLowerCase()=="image"){
					scope.renderClips[_rci] = ImageBlock()	
					scope.renderClips[_rci].init(context, scope._data[i].name, scope._data[i].id, [])
					scope.pieces.push([scope.renderClips[_rci]])
					scope.images.push(scope.renderClips[_rci]);					
					_rci++;
				}else{	
					// break a phrase string up into individual words that are associated via a "sibling" array.
					var p = scope._data[i].name.split(" ");
					var pieceArray = []; // how a word knows it's siblings in a piece.
					for (var k=0; k < p.length; k++) {
						scope.renderClips[_rci] = TextBlock()	
						scope.renderClips[_rci].init(context, p[k], scope._data[i].id, pieceArray)
						pieceArray.push(scope.renderClips[_rci]);										
						_rci++					
					};
					scope.pieces.push(pieceArray)
				}			
			}
			//	scope.assemblePoints();
		}		

		,positionExceedsCanvasBounds: function(piece, _xpos){
			return _xpos+piece.width+fontSize/2 > canvas.width;
		}

		,checkForOverlap:function (piece){
			function checkForVerticalOverlap(_img){
				if(piece == _img){ return false}; 
				
				var p_t = piece.y; // piece top
				var i_t = _img.y; // image top
				var i_b = i_t+_img.height; // +leading
				
				var piece_top_lower_than_image_top = (p_t > i_t);
				var piece_top_higher_than_image_bottom = (p_t < i_b)
				var conflicted = piece_top_lower_than_image_top && piece_top_higher_than_image_bottom;
				return conflicted;
			}			
			var p_l = piece.x; // piece_left
			var p_r = p_l+piece.width;
			var overlapped;
			//  iterate over images, check the image position against the diff word position.
			for(var i = 0; i < scope.images.length; i++){
				var x_overlapped = false;				
				var _img = scope.images[i]		

				if(piece == _img){ return false; }
				//--
				var i_l = _img.x;
				var i_r = _img.x+_img.width;
					//--
				var piece_left_to_left_of_image_left = p_l <= i_l;
				var piece_right_to_the_right_of_the_image_left = p_r > i_l;
				//
				var piece_left_to_the_left_of_the_image_right = p_l < i_r;
				var piece_right_to_the_right_of_the_image_right  = p_r > i_r;	
				if(piece_left_to_left_of_image_left && piece_right_to_the_right_of_the_image_left){
					x_overlapped = true;					
				}
				if(piece_left_to_the_left_of_the_image_right && piece_right_to_the_right_of_the_image_right){
					x_overlapped = true;
				}				
				if(x_overlapped){
					if(checkForVerticalOverlap(_img)){
						return i_r;						
					}
				}
			}
			return false
		}

		,clearPositions: function (){
			for (var i=0; i < scope.images.length; i++) {
				scope.images[i].x = scope.images[i].y = -1000
			}			
			for (i=0; i < renderClips.length; i++) {			
				scope.renderClips[i].x = scope.renderClips[i].y = -1000
			}
		}
				
		,_getClipCurrentlyUnderMouse: function(e){
			_getMouse(e);	
			for (var i = 0; i < renderClips.length; i++) {
				if(renderClips[i].hitTest()){
					return renderClips[i];
				}
			}		
			return false;
		}
						
		, draw : function(){	
				//clear the canvas and draw from scratch.
				context.clearRect(0, 0, canvas.width, canvas.height);		
				//---
				xpos = h_padding;
				ypos = v_padding;
				//--
				// for each render clip
				scope.clearPositions();
				//---
				for (var i=0; i < scope.renderClips.length; i++) {	
					var piece = scope.renderClips[i];
					// check to see if this image will go past the width of the canvas.  if so, set xpos to 0 and increment ypos
					if( scope.positionExceedsCanvasBounds(scope.renderClips[i], xpos) ){
						xpos =  h_padding;
						ypos += fontSize+leading;
					}
					// next you need to check to see if the placement will 
					//-----
					scope.renderClips[i].x = xpos;
					scope.renderClips[i].y = ypos;
					var overlaps = scope.checkForOverlap( scope.renderClips[i] ); 
					if(overlaps){
						scope.renderClips[i].x = overlaps
					}				

					var g = scope.renderClips[i];
					g.draw()				
					xpos = g.x+g.width			
				};		
				if(scope.images_loaded >= scope.images.length){
					scope.setHeight();
				}
				scope.assemblePoints();				
		}
		
		,setHeight: function(){
			
			var c = scope.renderClips[scope.renderClips.length-1];
		
		}
		
		,clearPositions: function(){
			for (var i=0; i < scope.images.length; i++) {
				scope.images[i].x = scope.images[i].y = -1000
			}			
			for (i=0; i < scope.renderClips.length; i++) {			
				scope.renderClips[i].x = scope.renderClips[i].y = -1000
			}
		}
		
		,mousemove: function(){
			var siblings = [];
			for (var i = 0; i < scope.renderClips.length; i++) {
				//check all the objects to see if you are intereacting with them
				if(dragging){ return; }
				if(scope.renderClips[i].hitTest() && dragging ){
					scope.renderClips[i].mouseOver();
					siblings = scope.renderClips[i].siblings;
				}else{
					if( siblings.indexOf(scope.renderClips[i]) ==-1 ){
						scope.renderClips[i].mouseOut();								
					}
				}
			}
		}
		
		,mouseDown: function(e){		
			mousedDown = true	
			var siblings = [];			
			var clickedOnPiece = false;
			
			for (var i = 0; i < scope.renderClips.length; i++) {
				//------------------------------------------------
				if(scope.renderClips[i].hitTest()){				
					clickedOnPiece = scope.renderClips[i];					
					siblings = clickedOnPiece.siblings;
					clickedOnPiece.mouseDown();
				}else{
					if( siblings.indexOf(scope.renderClips[i]) ==-1 ){
						scope.renderClips[i].mouseOut();								
					}				
				}
				//------------------------------------------------			
			}	
			if(clickedOnPiece){
				dragging = clickedOnPiece;
				return clickedOnPiece;
			}
			dragging = false;			
			return false;					
		}

		,mouseUp: function (e){
			mousedDown = false;
			for (var i = 0; i < scope.renderClips.length; i++) {
				scope.renderClips[i].mouseUp();				
				if(scope.renderClips[i].hitTest()){
					if(scope.renderClips[i]==dragging){
						dragging.selection();
						return dragging.siblings[0];
					}
				}
			}
			return false;
		}

		,getNearestPoint: function(){
			var diff = -1;
			var pieces = scope.pieces;
			for (var i=0; i < pieces.length; i++) {
				var piece = pieces[i][0];
				 var dx = piece.x - mouseX;
				 var dy = piece.y - mouseY;			
				 var distance = Math.abs(Math.sqrt(dx * dx + dy * dy));			
				 if( (diff == -1) || (diff > distance)){
					diff = distance
					_closest = piece				
				}
			};
			return _closest;
		}		
		
		,assemblePoints:function(){
			pieceXPositions = [];
			pieceYPositions = [];
			for (var i=0; i < scope.pieces.length; i++) {
				pieceXPositions.push(scope.pieces[i][0].getPieceGroupBeginningPoint())
				pieceXPositions.push(scope.pieces[i][0].getPieceGroupEndPoint())	
				//--
				pieceYPositions.push(scope.pieces[i][0].getPieceTop())
				
			};
		}		
		
		,getPieceIndexForItem: function (clip){
			piece_index = -1;
			for (var i=0; i < scope.pieces.length; i++) {
				if(scope.pieces[i].indexOf(clip)>-1){
					return i
				}
			};
			return piece_index;
		}
	}
	return scope;	
}