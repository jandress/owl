    
// session flashes are called in the ajax layout. 

var qn=function(){

	var fo = {
        // array(
        //      'message' => 'Correct',
        //      'element' => 'alert',
        //      'params' => array(
        //          'plugin' => 'TwitterBootstrap',
        //          'class' => 'alert-success'
        //      )
        // )
        flash:function(params){
            var p = params.params.class;
            var title = params.params.title ? params.params.title : ''; 
            var type = params.params.class;
            switch(params.params.class){
                case("alert-success"):
                    type = "success";
                break;
                case("alert-danger"):
                case("alert-error"):                
                    type = "error";                
                break;
            }
            if(params.params.delay){
                var delay = params.params.delay;
            }else{
                delay = 1000;
            }

           //  var title = params.title ? params.title : "title";
           //  var text = params.message ? params.message : "text";            
           //  var delay = params.delay ? params.delay : 6;            
           // // var title = params.callback ? params.callback : "title";            
           //  //title,message,delay,callback
            
            if( title == '' || !title ){
                return new PNotify({
                    text: params.message,
                    delay:delay,
                    type:type
                });
            }else{
                return new PNotify({
                    title: title,
                    text: params.message,
                    delay:delay,
                    type:type
                });
            }
        },



		displaymessage:function(title,message,delay,callback){
			return new PNotify({
                title: title,
                text: message,
                delay:delay,
				after_close:callback 
            });
		},
		displaySuccess:function(title,message,delay,callback){
			new PNotify({
                title: title,
                text: message,
                type: 'success',
                icon: 'ui-icon ui-icon-flag',
                delay:delay,
				after_close:callback 
            });
		},
		displayFailure:function(title,message,delay,callback){
            alert(1)
			new PNotify({
                title: title,
                text: message,
                type: 'error',
                delay:delay,
                after_close:callback                     
            });
		}
	}
	return fo;
}

var site_flash = qn();

