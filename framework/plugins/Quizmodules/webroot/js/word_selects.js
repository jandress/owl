var word_selects = function(){

	var fo = {

		spannedWords : '',
		selectedWords:[],
		style: 'single',

		applyInteractivity: function(paragraphclass, _style){

			if(!_style){
				_style = 'multi'
			}
			var paragraphclass = '.word-split';
			fo.style = _style;

			words = $(paragraphclass).text().replace(/(\r\n|\n|\r)/gm,"").split(' ');
			//create a string, take all the words in the original string and surround each word with spans.
			var b;
			 $(words).each(function(i, item) {
			   if (item.length > 0) {
			   	 b = '<span class = "'+i+'">' + item + '</span> ';
			     fo.spannedWords += b;
			   }
			 });
//console.log(fo.spannedWords);
return

			//console.log(fo.spannedWords);
				//replace the text with our new string
			$(paragraphclass).html(fo.spannedWords);
			 // click a word to do a full highlight and save in an array or, if highlighted, unhighlight and remove from array
			var answer;
			$(paragraphclass).addClass('word-split_');
			$(paragraphclass).removeClass('word-split');


			$(".word-split_ span").click(function(){
				//var word = $(this);

				if(fo.style.toLowerCase() == 'single'){
					fo.onWordSelectSingle($(this));
				}else{
					fo.onWordSelectMultiple($(this));
				}
			});
		},




		updateWordSelectField: function($input){
			////console.log('updateWordSelectField: '+($input.val()));
			 var spannedWords = ''
		      words = $input.val().split(' ');

		      $(words).each(function(i, item) {
		      	console.log(i);
		      	console.log(item);
		        if (item.length > 0) {
		          spannedWords += '<span class="'+i+'">' + item + '</span> ';
		        }
		      });
				////console.log(spannedWords);
		      //if (spannedWords.length > $(".selector").html().length) {
		        $(".selector").html(spannedWords);
		      //}
		},



		showAnswers: function(val){

			var n = val.split(',');
			var g = $('.word-split_ span');
			console.log('n '+n);
			console.log('g: ');
			console.log(g);
			for (var i=0; i < n.length; i++) {
				word = g[ n[i] ];
				console.log(word);
  				$(word).addClass("selected");
			};

		},


		onWordSelectSingle: function(word){
			//desect all words
			//replace array with [answer]
			var g = $('.word-split span');

			for (var i=0; i < g.length; i++) {
				if ( $(g[i]).hasClass("selected") ){
					$(g[i]).removeClass("selected");
				}
			  fo.selectedWords = [];
			};

			if ( !(word.hasClass("selected") )){
				fo.selectedWords = [ fo.getWordIndex(word)  ];
  				word.addClass("selected");
			}
		},

		onWordSelectMultiple: function(word){
				// if selected else if not selected.
			if ( word.hasClass("selected") ){
			  word.removeClass("selected");
			  var index = fo.selectedWords.indexOf( fo.getWordIndex(word) );
			  fo.selectedWords.splice(index, 1);
				//
			  answer = fo.selectedWords.sort(fo.sortNumber).join(",");
			} else {
			  fo.selectedWords.push( fo.getWordIndex(word) )
			  word.addClass("selected");
			  answer = fo.selectedWords.sort(fo.sortNumber).join(",")
			}
			if($("#response")){
				$("#response").val(fo.selectedWords);
			}

		},

		getWordIndex: function(word){
		  return parseInt(word.attr("class").split(" ").pop("word"));
		},

		sortNumber: function(a, b){
		  return a - b;
		},

		getSelections: function(){
			var str = '';
			for (var i=0; i < fo.selectedWords.length; i++) {
				str += fo.selectedWords[i];
				if(i < fo.selectedWords.length - 1){
					str += ',';
				}
			};
			return str;
		}



	}
	return fo;
}
