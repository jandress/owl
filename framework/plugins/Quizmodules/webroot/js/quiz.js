var quiz_interface=function(){

	var fo ={

		ids:null,
		debugging:false,
		responseDelay:100000,
		index:0,
		urlString:'/quizslides/view/',
		endString:'/quizzes/end/',
		responseString:'',
		flasher:null,

		start:function(){
			$.ajax({
				url:'/quizzes/start/',
				type: "POST",
				success: function(response){
					console.log('response');
				}
			});		
		},


		setQuizSlides:function(id_array){
			fo.ids = id_array;
		},


		nextSlide:function(id_array){
			var g = fo.urlString+""+fo.ids[fo.index]; 
			//	console.log(g);
			if(fo.index==fo.ids.length){
				g = fo.endString;
			}
			fb_ajax_helper.loadNext(g);			
			fo.index++;
		},

		previousSlide:function(id_array){
			fo.index--;
			fb_ajax_helper.loadBack(g);						
		},


		submitAnswer:function(id_array){

		},

		getDelay: function(){
			return fo.responseDelay;
		},		

		ifDebugLog: function(str){
			if(fo.debugging){
				console.log(str);
			}
		},


		onCorrect:function(){
				$.ajax({
				url:'/quizzes/record/',
				type: "POST",
				data: {
					qslid:qslid,
					qsid:qsid,
					r:1
				},			
				success: function(response){
					// $("#content").empty();
					// $("#content").html(response);
					fo.flasher.displaySuccess("title","message", fo.responseDelay, function(){
						fo.nextSlide()
					});				
				}
			});	

		},
		onInCorrect:function(){
				$.ajax({
				url:'/quizzes/record/',
				type: "POST",
				data: {
					qslid:qslid,
					qsid:qsid,
					r:0
				},	
				success: function(response){
					fo.flasher.displayFailure("title","message", fo.responseDelay, function(){
						fo.nextSlide()
					});				
				}
			});	
		},

		displayResponse: function(response) {
			var actv;
			var inactv;		
			fo.ifDebugLog('displayResponse: '+response);
		    if (response == '1') {
				fo.ifDebugLog("response == '1'")
				fo.onCorrect();
		    } else {
				fo.onInCorrect();
		    }
		},


		submitAnswer: function(val){
			//ifDebugLog('submitAnswer');
			//$("#checking_msg").show();
			var t = fo.flasher.displaymessage("Checking","Hang on, this will just take a second...", 1000);
			var targetDiv = "#message_area";
			//$(targetDiv).empty().html('<div class="alert"><h5><em>Loading...</em></h5></div>'); 
			$(targetDiv).empty();		
			//fo.showQuizLoader()	
			$.ajax({
				url:val.url,
				context: $(targetDiv),
				type: "POST",
				data: {response:val.answer},			
				success: function(response){
					t.remove();
					fo.ifDebugLog('response: '+response)
					$("#checking_msg").hide();
					$(targetDiv).empty();
					fo.displayResponse(response);
				}
			});		
			$("#submit").addClass('disabled');
			$("#submit").removeClass('btn-success');	
		}
	}
	//parameterize this.
	//fo.flasher = site_flasher;
	return fo;
}
quiz = quiz_interface();


function submitAnswer(val){
	quiz.submitAnswer(val);
}

/*

	ok, so how 


	an alert with "you have scored x out of y correct"
	at the end of each quiz you have the talley



	
	each time a quizslide loads,  it records the user_id, quiz_id, quizslide_id & timestamp.  
	each time there is a submission, it records correct or incorrect.  on quiz end, you must be able to submit or tally quiz score.
	quiz_responses table. 
	

	//----------------
		id
		user_id
		time_stamp
		time_started
		time_responed.
		user_reponse
		was_correct
	//----------------






	at the end of the quiz, you must be able to 


	user_id,
	quiz id.
	quiz_session_id. /// begins upon clicking "get started."
	quizslide_id. 
	timestamp to start.
	each 






*/