var admin_word_selects = function(qs){

	var fo = {
		selectedWords: [],
		questionString:qs,

		checkAnswer: function(g){
			return $("#WordselectAnswer").val() == $("#correct_answer").val() ;
		},


		showAnswers: function(g){
			// $.inArray(fo.getWordIndex(word)
			var t = $("#correct_answer").val().split(",")
			var g = $(".selectables");
			for (var i = 0; i < g.length; i++) {
				$(g[i]).removeClass('selected')
			}
			for (i = 0; i < t.length; i++) {
					$("#span_"+t[i]).addClass('selected');
			}
			$("#WordselectAnswer").val($("#correct_answer").val());
		},


		applyInteractivity: function(){
			fo = fo;
			// click a word to do a full highlight and save in an array; or if highlighted, unhighlight and remove from array
			fo.selectedWords = [];
			var words, answer, timer, index;
			var $selector = $(".selector");
			// add click listeners to the interactive "selector" spans.

			$('.selector').click(function(e){
					var word = $(e.target);
					if (e.target.nodeName === 'SPAN') {
						//fill in the form field
						$("#WordselectAnswer").val(fo.hilightWord(word));
				  }
			});
			fo.updateWordSelectField(fo.questionString);
		},



		applyAdminInteractivity: function(){
			fo = fo;
			// click a word to do a full highlight and save in an array; or if highlighted, unhighlight and remove from array
			fo.selectedWords = [];
			var words, answer, timer, index;
			var $selector = $(".selector");
			// the keyboard listener
			$(".question-input").keyup(function(){
			 var $input = $(this)
			 // prevent repeat.
			  if (timer) { clearTimeout(timer); }
				//clear the "answer" though it is actually a "response" field
			  $("#WordselectAnswer").val('');
			  answer, fo.selectedWords = [];
				// repeately update the display to match the input field.
			  timer = setTimeout(function () {
			   	fo.updateWordSelectField( fo.questionString);
			  }, 300);
			});
			// add click listeners to the interactive "selector" spans.
			$('.selector').click(function(e){
					var word = $(e.target);
					if (e.target.nodeName === 'SPAN') {
						//fill in the form field
						$("#WordselectAnswer").val(fo.hilightWord(word));
				  }
			});
			fo.updateWordSelectField(fo.questionString);
		},


		hilightWord: function(word){
			var answer;
			//if selected unselect.
	    if ( word.hasClass("selected") ){
					//unselect
		        word.removeClass("selected");
		        index = fo.selectedWords.indexOf( fo.getWordIndex(word) );
		        fo.selectedWords.splice(index, 1);
		        answer = fo.selectedWords.sort(fo.sortNumber).join(",");
		      } else {
						//select
						/// if the word is in the fo.selectedWords array, add it to the selected words array.
		        if ($.inArray(fo.getWordIndex(word), fo.selectedWords) !== 0) {
		          fo.selectedWords.push( fo.getWordIndex(word) );
		        }
		        word.addClass("selected");
		        answer = fo.selectedWords.sort(fo.sortNumber).join(",")
		      }
			return answer
		},

		indicateInitialHighlights: function(g){
			if(g==''){ return; }
			for (var i=0; i < g.length; i++) {
				fo.hilightWord($($(".selector span")[g[i]]));
			};
		},

		getWordIndex: function(word){
			var g = word;
		  return parseInt(word.attr("id").replace("span_",""));
		},

		sortNumber: function(a, b){
		  return a - b;
		},


		updateWordSelectField: function(_originalString){
			 console.log(_originalString)
			var originalString = _originalString.replace(new RegExp("\\\\", "g"), "");
			//-----------------------------------------------------------------			
			var openingTag = "<i>";
			var closingTag = "</i>";
			//var originalString = $input.val();
			var sanitizedStringString = originalString.replace(openingTag,'').replace(closingTag,'');
			var opentags = [];
			var closetags = []; //$input.val().indexOf("</i>");
			words = originalString.split(' ');

			//split the string by spaces, go through each word and check for tags,
			$(words).each(function(i, item){
				// put some conditionals here to make sure an opening tag can not follow another open tag
				// if it has an opening tag, check to see if its at the beginning of the word. if it is, add it @ this index. 
				if (item.indexOf(openingTag) >= 0) {
					//console.log("first index of "+item.indexOf("<i>"));
					if(item.lastIndexOf(openingTag) ==0){
						//the tag is not at the beginning of the word.
						opentags.push({
							'tag':openingTag,
							'i':i
						})
					}
				}


				//if has a closing tag.
				if (item.indexOf(closingTag) >= 0) {
					if(item.lastIndexOf(closingTag) ==item.length-closingTag.length){
						closetags.push({
							'tag':closingTag,
							'i':i
						});
					}
				}
			});


			var _spannedWords = '';
			var _g = sanitizedStringString.split(' ');
			$(_g).each(function(i, item) {
				if (item.length > 0) {
					for (var t = 0; t < opentags.length; t++){
						if(opentags[t].i == i){
							_spannedWords += opentags[t].tag;
						}
					}
					_spannedWords += '<span id = "span_'+ i +'" class="selectables">' + item + '</span> ';
					for (t = 0; t < closetags.length; t++){
						if(closetags[t].i == i){
							_spannedWords += closetags[t].tag;
						}
					}
				}
			});
		  $(".selector").html(_spannedWords);
		},






	}
	return fo;
}
