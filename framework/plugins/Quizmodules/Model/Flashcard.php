<?php
App::uses('AppModel', 'Model');

class Flashcard extends AppModel {
	
	var $actsAs = array('Polymorphic','Containable','Quizmodules.Quizslidecontent'); 		

	public function save($data = NULL, $validate = true, $fieldList = array()){		
		$d = $this->sanitizeDataForQuizslideCreation($data);
		$p = parent::save(
			$d, 
			$validate, 
			$fieldList
		);
		return $p;
	}

  public $hasOne = array(
    'Quizslide' => array(
      'className' => 'Quizslide',
      'foreignKey' => 'foreign_id',
      'dependent' => false,
      'conditions' => array('Quizslide.class'=>'Flashcard'),
      'fields' => '',
      'order' => '',
      'limit' => '1',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => ''
    )
  );




}
