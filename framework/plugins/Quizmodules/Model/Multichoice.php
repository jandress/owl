<?php
App::uses('AppModel', 'Model');
/**
 * Multichoice Model
 *
 * @property Multichoiceanswer $Multichoiceanswer
 * @property Multichoiceanswer $Multichoiceanswer
 */
class Multichoice extends AppModel {
/**
 * Use database config
 *
 * @var string
 */
	////public $useDbConfig = 'quizzes';

	var $actsAs = array('Polymorphic','Containable'); 	
	var $editAction = '/my/multichoices/edit/';
	var $viewAction = '/multichoices/view/';
	public $displayField = 'name';
	
	
/**
 * Use table
 *
 * @var mixed False or table name
 */
//	public $useTable = 'multichoice';
/**
 * Display field
 *
 * @var string
 */
/**
 * Validation rules
 *
 * @var array
 */


// Array
// (
// [responses] => 81
// [Post] => Array
// (
// [q] => 
// )

// [Multichoice] => Array
// (
// [id] => 13
// )

// [Quizslide] => Array
// (
// [class] => Multichoice
// )
// )
// 		return $this->find('first',array('conditions'=>array('id'=>$data['Multichoice']['id'])));
// 	}
// Array
// (
// [Multichoice] => Array
// (
// [id] => 13
// [directive] => Choose the strongest thesis statement based on the topic idea provided.
// [name] =>
// Based on a topic choice of teacher pay, which thesis would be the strongest?



// [multichoiceanswer_id] => 82
// [correct_text] => Correct! This thesis provides a clear topic and strong assertion.
// [incorrect_text] => Incorrect! Look for the thesis statement that provides both the topic and an assertion about that topic 



	public function checkAnswer($data){
		$d = $this->find('first',array('conditions'=>array('Multichoice.id'=>$data['Multichoice']['id'])));
		if($data['responses']== $d['Multichoice']['multichoiceanswer_id']){
			$correct = true;
			$message = $d['Multichoice']['correct_text'];
		}else{
			$correct = false;
			$message = $d['Multichoice']['incorrect_text'];
		}
		return array(
			'correct' => $correct,
			'message' => $message,
			'data'=>$d
			);
	}




function saveAnswer($mcId,$mcAId){
    // $this->id = $mcId;
    // $this->multichoiceanswer_id = $mcAId;
   	return $this->save(
		array(
			'id'=>$mcId,
			'multichoiceanswer_id'=>$mcAId,
			)
   		);
}

 public $belongsTo = array(
      'CorrectAnswer' => array(
          'className' => 'Quizmodules.Correctmultichoiceanswer',
          'foreignKey' => 'multichoiceanswer_id',
          'conditions' => '',
          'fields' => '',
          'order' => ''
      )
  );

  public $hasMany = array(
      'Multichoiceanswer' => array(
          'className' => 'Quizmodules.Multichoiceanswer',
          'foreignKey' => 'multichoice_id',
          'dependent' => true,
          'conditions' => '',
          'fields' => '',
          'order' => '',
          'limit' => '',
          'offset' => '',
          'exclusive' => '',
          'finderQuery' => '',
          'counterQuery' => ''
      )
  );

  public $hasOne = array(
    'Quizslide' => array(
      'className' => 'Quizslide',
      'foreignKey' => 'foreign_id',
      'dependent' => false,
      'conditions' => array('Quizslide.class'=>'Multichoice'),
      'fields' => '',
      'order' => '',
      'limit' => '1',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => ''
    )
  );



}


