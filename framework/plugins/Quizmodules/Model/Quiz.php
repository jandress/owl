<?php
App::uses('AppModel', 'Model');
/**
 * Quiz Model
 *
 */
class Quiz extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	var $actsAs = array('Polymorphic','Containable'); 		
	var $editAction = '/my/quizzes/edit/';
	var $viewAction = '/quizzes/view/';
	
	
	 public $hasMany = array(
	      'Quizslide' => array(
	          'className' => 'Quizmodules.Quizslide',
	          'foreignKey' => 'quiz_id',
	          'dependent' => false,
	          'conditions' => '',
	          'fields' => '',
	          'order' => 'Quizslide.order',
	          'limit' => '',
	          'offset' => '',
	          'exclusive' => '',
	          'finderQuery' => '',
	          'counterQuery' => ''
	      )
	  );
}
