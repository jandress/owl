<?php 

	class QuizslidecontentBehavior extends ModelBehavior {

		/*
			this is just a stopgap / kludge to force some of these slidetype models to accept empty data
		*/

		public function sanitizeDataForQuizslideCreation($model, $data){
			$d = $data;
			if(!isset($data)){
				$d = array(

				);
			}
			$b = $model->getColumnTypes();
			foreach ($b as $key => $value) {
				if($key ==$model->primaryKey){ continue; }
				if(!isset($d[$key])){
					$d[$key] = '';
				}
			}
			return $d;
		}

	}