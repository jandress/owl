<?php
App::uses('AppModel', 'Model');
/**
 * Quizslide Model
 *
 */
class Quizslide extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'class';
	public $actsAs = array('Polymorphic','Containable','PagekwikTools.Flatlist');

	public $belongsTo = array(
		'Quiz' => array(
			'className' => 'Quiz',
			'foreignKey' => 'quiz_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);


	public $slide_options = array(
		'Quiztextslide'=>'Text-only slide'
		,'CagedActivity'=>'Caged Activity'
		,'CagedQuiz'=>'Caged Quiz'
		,'IntervalActivity'=>'Interval Activity'
		,'IntervalQuiz'=>'Interval Quiz'
		,'SongProfile'=>'Song Profile'
		,'Multichoice'=>'Multiple Choice'
		,'Flashcard'=>'Flashcards'
	);



	public function checkAnswer($data){
		//debug($data); exit;
		$associatedModel = ClassRegistry::init($data['Quizslide']['class']);
		return $associatedModel->checkAnswer($data);
	}

	public function updateResponseTime($time,$class,$fid){
		$cond = array('Quizslide.class'=>$class,'Quizslide.foreign_id'=>$fid);
		return $this->updateAll(array('response_delay'=>$time) ,$cond);
	}

	public function saveNewQuizslide($d){
		$this->create();
		// save the associated model
		$modelToBind = $d['Quizslide']['class'];
		$boundModel = ClassRegistry::init($modelToBind);



		//debug($boundModel->getColumnTypes());
		//debug($boundModel); exit;

		$boundModel->create();
		$boundModelData = $boundModel->save(array());
		if(!$boundModelData){
			throw new Exception("Error Processing Request", 1);
		}
		$d['Quizslide']['foreign_id'] = $boundModelData[$modelToBind]['id'];
		$d['Quizslide']['quiz_id'] = $d['Quiz']['id'];
		$r = parent::save($d);
		//--------
		return $r;
	}

	public function save($data = NULL, $validate = true, $fieldList = array()){
		$d = $data;
		$old = $this->find('first',array('conditions'=>array('Quizslide.id'=>$d['Quizslide']['id'])));
		// find the quizslide record, if there is a difference in polymorphic type or ID,
		if(
			$old['Quizslide']['foreign_id'] != $data['Quizslide']['foreign_id'] ||
			$old['Quizslide']['class'] != $data['Quizslide']['class']  ||
			substr( $data['Quizslide']['class'] , 0 ,1 ) == "*"
		){

			//delete the previous record and save the new one.
			$class = $data['Quizslide']['class'];
				// if new {current quizslide type}
			 if(substr( $data['Quizslide']['class'] , 0 ,1 ) == "*"){
				$o = strlen($data['Quizslide']['class'])-1;
				$class = substr( $data['Quizslide']['class'] , 1 ,$o);
			 }
			 $d['Quizslide']['class'] = $data['Quizslide']['class'] =$class;
			///DELETE OLD ASSOCIATED QUIZSLLIDETYPE DATA, RECREATE EACH TIME.
			$oldModel = ClassRegistry::init($class);
			$g = $oldModel->find('all',array('id'=>$old['Quizslide']['foreign_id']));
			if(!empty($g)){
				$oldModelData = $oldModel->delete( $old['Quizslide']['foreign_id'] );
			}
			/// create empty of the new one.
			$newModel = ClassRegistry::init($class);

			// debug($newModel);
			
			$nmd = $newModel->create();
			// debug($nmd);
			// debug($newModel->find('all'));
			// debug($class);
			// $test = array(); //array( 'Flashcard'=> array('name'=>'test','description'=>'description','response'=>''));
			// $newModellData = $newModel->save($test);
			if($nmd){
				$d['Quizslide']['foreign_id'] = $nmd[$class]['id'];
			}
			// debug($d); exit;
		}
		$r = parent::save($d,$validate,$fieldList);
		return $r;
	}
			
	public function delete($id = NULL, $cascade = true){
		// save the associated model
		$this->id = $id;
		$d = $this->read();
		$modelToBind = $d['Quizslide']['class'];
		$boundModel = ClassRegistry::init($modelToBind);
		// debug($modelToBind );
		// debug($d['Quizslide']['foreign_id']); 
		$boundModel->delete( $d['Quizslide']['foreign_id'] );
		
		return parent::delete($id , $cascade);
	}
	
}



/*


array(
	'Quizslide' => array(
		'id' => '21',
		'class' => 'Multichoice',
		'foreign_id' => '13',
		'quiz_id' => '2',
		'name' => 'Thesis Statement Quiz Question 2',
		'order' => '0',
		'response_delay' => '7'
	),

	'Quiz' => array(
		'id' => '2',
		'name' => 'Thesis Statement Quiz',
		'Description' => 'Take the quiz to test your knowledge of thesis statements.'
	),

	'Multichoice' => array(
		'id' => '13',
		'directive' => 'Choose the strongest thesis statement based on the topic idea provided.',
		'name' => '<p>Based on a topic choice of teacher pay, which thesis would be the strongest?</p>
',
		'multichoiceanswer_id' => '82',
		'correct_text' => 'Correct! This thesis provides a clear topic and strong assertion.',
		'incorrect_text' => 'Incorrect! Look for the thesis statement that provides both the topic and an assertion about that topic without announcing.',
		'response_delay' => '5',
		'viewAction' => '/multichoices/view/',
		'viewActionFull' => '/multichoices/view/13',
		'editAction' => '/my/multichoices/edit/',
		'editActionFull' => '/my/multichoices/edit/13',
		'display_field' => '<p>Based on a topic choice of teacher pay, which thesis would be the strongest?</p>
'
	)
)


*/
