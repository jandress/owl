<?php

App::uses('AppModel', 'Model');
App::import('Model', 'CakeSession');

App::uses('CakeTime', 'Utility');



class Quizresponse extends AppModel {


	var $actsAs = array('Containable'); 		
	public $cookieName = 'shopping_cart1';


	
/*
	id
	user_id
	quiz_id
	quizslide_id
	description 
	timestamp 
*/


	 public $belongsTo = array(
	      'User' => array(
	          'className' => 'User',
	          'foreignKey' => 'user_id',
	          'dependent' => false,
	          'conditions' => '',
	      ),
	      'Quiz' => array(
	          'className' => 'Quizmodules.Quiz',
	          'foreignKey' => 'quiz_id',
	          'dependent' => false,
	          'conditions' => '',
	          'fields' => '',
	          'order' => 'Quizslide.order',
	          'limit' => '',
	          'offset' => '',
	          'exclusive' => '',
	          'finderQuery' => '',
	          'counterQuery' => ''
	      ),	      
	      'Quizslide' => array(
	          'className' => 'Quizmodules.Quizslide',
	          'foreignKey' => 'quizslide_id',
	          'dependent' => false,
	          'conditions' => '',
	          'fields' => '',
	          'order' => 'Quizslide.order',
	          'limit' => '',
	          'offset' => '',
	          'exclusive' => '',
	          'finderQuery' => '',
	          'counterQuery' => ''
	      )	    
	);

		public function start($userId, $quizId){
			$this->create();
			//$this->save(array('user_id' =>$userId, 'quiz_id'=>$quizId,'description'=>"starting", 'timestamp'=>date("Y-m-d H:i:s")))	;
			$s = $this->save(array('user_id' =>$userId, 'quiz_id'=>$quizId,'description'=>"starting", 'timestamp'=>date("Y-m-d H:i:s")));
			//$this->save($starting['starting']);
			$session = new CakeSession($this->cookieName);	
			$starting = array(
				'start' => array(
					'user_id' =>$s['Quizresponse']['user_id'],
					'quiz_id'=>$s['Quizresponse']['quiz_id'],
					'description'=>"starting", 
					'timestamp'=>$s['Quizresponse']['timestamp'],
					'quiz_session_id' =>$s['Quizresponse']['id']
					)
			);
			CakeSession::write($this->cookieName,$starting);				
		}



		public function recordAnswer($d){
			$cd = CakeSession::read($this->cookieName);				
			$s = array(
				'user_id' =>$cd['start']['user_id'],
				'quiz_id'=>$cd['start']['quiz_id'],
				'quizsession' => $cd['start']['quiz_session_id'],				
				'quizslide_id' => $d['qslid'],
				'description' => empty($d['r']) ? "incorrect"  : "correct",
				'timestamp'=>date("Y-m-d H:i:s")
				);
			return $this->save($s);
			//qs_id
			//user_id,	
			// 'user_id' =>$s['Quizresponse']['user_id'],
			// 'quiz_id'=>$s['Quizresponse']['quiz_id'],
			// 'description'=>"starting", 
			// 'timestamp'=>$s['Quizresponse']['timestamp'],
			// 'quiz_session_id' =>$s['Quizresponse']['id']
		}



		public function talleyQuiz(){
			$cd = CakeSession::read($this->cookieName);
			
			$total = $this->find('count',array('conditions'=>array(
				'quizsession' => $cd['start']['quiz_session_id']
			)));
			$correct = $this->find('count',array(
					'conditions'=>array(
						'Quizresponse.quizsession' => $cd['start']['quiz_session_id'],
						'Quizresponse.description'=>"correct",
					)
				)
			);
			$incorrect = $this->find('count',array(
					'conditions'=>array(
						'Quizresponse.quizsession' => $cd['start']['quiz_session_id'],
						'Quizresponse.description'=>"incorrect",
					)
				)
			);
			$session = $this->find('first',array(
					'conditions'=>array(
						'Quizresponse.quizsession' => $cd['start']['quiz_session_id'],
					)
				)
			);
			return array(
				'total'=>$total,
				'correct'=>$incorrect,
				'incorrect'=>$incorrect,
				'session'=>$session,
				'data'=>$cd				
			);

		}


		public function getQuizData(){
			$session = new CakeSession($this->cookieName);	
			return CakeSession::read($this->cookieName);				
		}


		
		public function success($id){
			$g = $this->getCart();
			$t = array_search($id,$g);
			if($t===false){		
				array_push($g,$id);						
			}
			CakeSession::write($this->cookieName,$g);		
			return $g;
		}

		
		public function remove($id){
			$g = $this->getCart();
			$t = array_search($id,$g);
			if(!($t===false)){
				array_splice($g, $t, 1);	
			}
			CakeSession::write($this->cookieName,$g);		
			return $g;
		}
		

		public function getResponses(){
			$c = CakeSession::read($this->cookieName);
			if(is_null($c)){
				$this->createCart();
			}
			$g = CakeSession::read($this->cookieName);						
			return $g;
		}
		
		
		
		public function end(){
			$g = $this->getCart();		
		}






	 
}










