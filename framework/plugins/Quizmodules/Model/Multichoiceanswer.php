<?php
App::uses('AppModel', 'Model');
/**
 * Multichoiceanswer Model
 *
 * @property Multichoice $Multichoice
 * @property Multichoice $Multichoice
 */
class Multichoiceanswer extends AppModel {
/**
 * Use database config
 *
 * @var string
 */
	//public $useDbConfig = 'quizzes';
	public $useTable = 'multichoiceanswers';
	var $actsAs = array('Containable'); 	
	
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
/**
 * Validation rules
 *
 * @var array
 */
	// public $validate = array(
	// 	'name' => array(
	// 		'notempty' => array(
	// 			'rule' => array('notempty'),
	// 			//'message' => 'Your custom message here',
	// 			//'allowEmpty' => false,
	// 			//'required' => false,
	// 			//'last' => false, // Stop validation after this rule
	// 			//'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasOne associations
 *
 * @var array
 */
// 	public $hasOne = array(
// 		'Multichoice' => array(
// 			'className' => 'Multichoice',
// 			'foreignKey' => 'multichoiceanswer_id',
// 			'conditions' => '',
// 			'fields' => '',
// 			'order' => ''
// 		)
// 	);
// 
// /**
//  * belongsTo associations
//  *
//  * @var array
//  */
// 	public $belongsTo = array(
// 		'Multichoice' => array(
// 			'className' => 'Multichoice',
// 			'foreignKey' => 'multichoice_id',
// 			'conditions' => '',
// 			'fields' => '',
// 			'order' => ''
// 		)
// 	);
}
