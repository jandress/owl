<div class="navbar-inverse admin-padd">
	<div class = "admin_padding">
	<h2><?php echo __( __('Flashcards'));?></h2>
			<div class = "pull-right">
				<?php echo $this->element('Quizmodules.quizzes/quiztype_list_btn'); ?>               		
			</div>
			<div class='span9' style = 'padding-top:8px'>
				<p>
					<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
				</p>
			</div>
	</div>
	<div class = "clearfix">&nbsp;</div>
</div>





<div class="admin_padding">
	<div class="row-fluid">
	

			<table class="table table-striped table-bordered table-hover">

				<tr>
					<th class="span1"><?php echo $this->BootstrapPaginator->sort('id');?></th>
					<th class="span4"><?php echo $this->BootstrapPaginator->sort('name');?></th>					
					<th class="actions span2"><?php echo __('Actions');?></th>
				</tr>



			<?php
			// debug($flashcards);

			 foreach ($flashcards as $flashcard): 

			 	?>
				<tr>
					<td><?php echo h($flashcard['Flashcard']['id']); ?>&nbsp;</td>
					<td><?php echo h($flashcard['Flashcard']['name']); ?>&nbsp;</td>
					<td>
						<?php echo $this->element('Quizmodules.Quizslides/edit_view_btn',array('__d'=>$flashcard, '_quiztype'=>'Flashcard')) ?>

					</td>
					
				</tr>
			<?php endforeach; ?>
			</table>
			<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
</div>	