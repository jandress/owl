<div class="navbar-inverse admin-padd">
	<div class = "admin_padding">
		<h2>Multiple Choice Questions</h2>
			<div class = "pull-right">
				<?php echo $this->element('Quizmodules.quizzes/quiztype_list_btn'); ?>               		
			</div>
			<div class='span9' style = 'padding-top:8px'>
				<p>
					<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
				</p>
			</div>
	</div>
	<div class = "clearfix">&nbsp;</div>
</div>





<div class="admin_padding">
	<div class="row-fluid">
			<table class="table table-striped table-bordered table-hover">
				<tr>
					<th class="span1"><?php echo $this->BootstrapPaginator->sort('id');?></th>
					<th class="span4"><?php echo $this->BootstrapPaginator->sort('directive');?></th>
					<th class="span4"><?php echo $this->BootstrapPaginator->sort('name');?></th>
					<th class="span1">Answers</th>
					<th class="actions span2"><?php echo __('Quizslide');?></th>
				</tr>
			<?php foreach ($multichoices as $multichoice): ?>
				<tr>
					<td><?php echo h($multichoice['Multichoice']['id']); ?>&nbsp;</td>
					<td><?php echo h($multichoice['Multichoice']['directive']); ?>&nbsp;</td>
					<td><?php echo h($multichoice['Multichoice']['name']); ?>&nbsp;</td>
					<td>

						<?php foreach ($multichoice['Multichoiceanswer'] as $key => $value): 
							$icon = $value['id'] == $multichoice['CorrectAnswer']['id'] ? 'icon-ok' : 'icon-remove';
							echo('&nbsp; <div class = "'.$icon.' tm_3_px bm_10_px">&nbsp; </div> '.$value['name'].'<br/>');
							echo '<div class = "clearfix" style= "line-height:2px; ">&nbsp;</div>';
							?>
						
						<?php endforeach ?>
					</td>
					<td>
							<?php echo $this->element('Quizmodules.Quizslides/edit_view_btn',array('__d'=>$multichoice, '_quiztype'=>'Multichoice')) ?>
					</td>
				</tr>
			<?php endforeach; ?>
			</table>
			<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
</div>	