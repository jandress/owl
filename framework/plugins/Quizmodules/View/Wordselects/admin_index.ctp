<div class="row-fluid">






	<div class="navbar-inverse admin-padd">

		<div class = "admin_padding">
			<h2><?php echo __( __('Wordselects'));?></h2>


				<div class = "pull-right">
					<?php echo $this->element('Quizmodules.quizzes/quiztype_list_btn'); ?>
				</div>


				<div class='span9' style = 'padding-top:8px'>
					<p>
						<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
					</p>
				</div>
		</div>
		<div class = "clearfix">&nbsp;</div>
	</div>






		<div class="admin_padding">


	<?php echo $this->BootstrapPaginator->pagination(); ?>

				<table class="table table-striped table-bordered table-hover">
					<tr>
						<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
						<th><?php echo $this->BootstrapPaginator->sort('name');?></th>
						<th><?php echo $this->BootstrapPaginator->sort('question');?></th>
						<th><?php echo $this->BootstrapPaginator->sort('Quiz slide');?></th>


					</tr>
					<?php foreach ($wordselects as $wordselect): ?>
						<tr>
							<td><?php echo ($wordselect['Wordselect']['id']); ?>&nbsp;</td>
							<td><?php echo ($wordselect['Wordselect']['name']); ?>&nbsp;</td>
							<td><?php echo ($wordselect['Wordselect']['question']); ?>&nbsp;</td>
							<td>

						<?php echo $this->element('Quizmodules.Quizslides/edit_view_btn',array('__d'=>$wordselect, '_quiztype'=>'Wordselect')) ?>






							&nbsp;</td>

						</tr>
					<?php endforeach; ?>
				</table>



		</div>

		<?php echo $this->BootstrapPaginator->pagination(); ?>

</div>
