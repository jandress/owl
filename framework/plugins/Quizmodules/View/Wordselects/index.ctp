<div class="row-fluid">
	<div class="span9">
		<h2><?php echo __( __('Wordselects'));?></h2>
		
		<p>
			<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
		</p>

		<table class="table">
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('directive');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('hint');?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($wordselects as $wordselect): ?>
			<tr>
				<td><?php echo h($wordselect['Wordselect']['id']); ?>&nbsp;</td>
				<td><?php echo h($wordselect['Wordselect']['name']); ?>&nbsp;</td>
				<td><?php echo h($wordselect['Wordselect']['directive']); ?>&nbsp;</td>
				<td><?php echo h($wordselect['Wordselect']['hint']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $wordselect['Wordselect']['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $wordselect['Wordselect']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $wordselect['Wordselect']['id']), null, __('Are you sure you want to delete # %s?', $wordselect['Wordselect']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>

		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('New %s', __('Wordselect')), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__( __('Wordselect Answers')), array('controller' => 'wordselect_answers', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Wordselect Answer')), array('controller' => 'wordselect_answers', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>