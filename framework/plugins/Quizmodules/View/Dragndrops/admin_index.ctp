<div class="navbar-inverse admin-padd">
	<div class = "admin_padding">
		<h2><?php echo __( __('Dragndrops'));?></h2>
			<div class = "pull-right">
				<?php echo $this->element('Quizmodules.quizzes/quiztype_list_btn'); ?>               		
			</div>
			<div class='span9' style = 'padding-top:8px'>
				<p>
					<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
				</p>
			</div>
	</div>
	<div class = "clearfix">&nbsp;</div>
</div>











<div class="admin_padding">



		<table class="table table-striped table-bordered table-hover">
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('description');?></th>
				<th class="actions"><?php echo __('Quizslide');?></th>
			</tr>
		<?php foreach ($dragndrops as $dragndrop): ?>
			<tr>
				<td><?php echo h($dragndrop['Dragndrop']['id']); ?>&nbsp;</td>
				<td><?php echo h($dragndrop['Dragndrop']['name']); ?>&nbsp;</td>
				<td><?php echo h($dragndrop['Dragndrop']['description']); ?>&nbsp;</td>
					<td>
							<?php echo $this->element('Quizmodules.Quizslides/edit_view_btn',array('__d'=>$dragndrop, '_quiztype'=>'Dragndrop')) ?>
					</td>					
			</tr>
		<?php endforeach; ?>
		</table>

		<?php echo $this->BootstrapPaginator->pagination(); ?>

</div>