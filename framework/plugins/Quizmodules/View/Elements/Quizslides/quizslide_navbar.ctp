<?php
	//~ this should eb in the controller
	if(empty($next)){
		$nurl = "/my/quizzes/end/";
	}else{
		$nurl = "/quizslides/view/".$next['Quizslide']['id'];		
	}
	$purl = "/quizslides/view/".$prev['Quizslide']['id'];
?>


<div class="navbar tm_1_em" >
	<div class="navbar-inner">
		<div class="btn-group pull-right tm_8_px rm_7_px">
			<?php if (empty($prev)): ?>
				<a class = 'btn btn-sm btn btn-default disabled'>Previous</a>
			<?php else: ?>	
				<a href = '<?php echo $purl; ?>' class = 'prev_next btn btn-default btn-sm ajax_link '>Previous</a>
			<?php endif ?>

			<?php if (empty($nurl)): ?>
				<a class = 'btn  btn-sm btn-default disabled'>Next</a>
			<?php else: ?>	
				<a href = '<?php echo $nurl; ?>' class = 'prev_next btn btn-default btn-sm ajax_link '>Next</a>				
			<?php endif ?>
		</div>
	</div>	
</div>