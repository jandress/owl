<?php
 if (isset($__d['Quizslide']['id'])): 
	$__value = $__d['Quizslide'];
?>
	<ul style = "list-style:none; padding:0" >
		<li class="dropdown">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			Quizslide <?php echo $__value['id'] ?>: <?php echo $__value['name'] ?>
		  </a>
		    <ul class="dropdown-menu " role="menu" style = "text-decoration:none;">
		        <li>	
					<a class = "" target = "_blank" href="/my/quizslides/view/<?php echo $__value['id'] ?>" >View</a>
		        </li>
				<li>	
					<a class = "" target = "_blank" href="/my/quizslides/edit/<?php echo $__value['id'] ?>" >Edit</a>
		        </li>
		    </ul>
		</li>
	</ul>
<?php else: ?>
	<div class = 'alert alert-danger'>
		Orphaned <?php echo $_quiztype ?> , This <?php echo $_quiztype ?> is not owned by any quizslide.
		



	<?php
	$d = '/my/'.strtolower(Inflector::pluralize($_quiztype)).'/delete/'.$__d[$_quiztype]['id'] ;
	echo $this->Form->postLink(__('Delete'),$d,array('class'=>'btn btn-sm btn-danger tm_1_em'));

	// array('url' => '/my/'.strtolower(Inflector::pluralize($_quiztype)), $__d[$_quiztype]['id']), null, __('Are you sure you want to delete quiz # %s and all related slides?', $__d[$_quiztype]['id'])); ?>

	</div>
<?php endif ?>



 