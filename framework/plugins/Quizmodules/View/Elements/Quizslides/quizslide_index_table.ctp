<table class="table table-striped table-bordered table-hover">
	<tbody>
		<tr>
			<th style="text-align:center;">Id</th>
			<th>Name</th>

			<th class = "span1">&nbsp;</th>
		</tr>
	<?php
		//debug($quizslides);
		foreach ($quizslides as $quizslide):
			$quizslide = $quizslide['Quizslide'];
			?>
		<tr>
			<td style="text-align:center;"><?php echo h($quizslide['id']); ?>&nbsp;</td>


			<td>
				<ul style = "list-style:none; padding:0" >
		            <li class="dropdown">
		              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<?php echo h($quizslide['class']).' '.$quizslide['foreign_id'].': '.h($quizslide['name']); ?>&nbsp;
		              </a>
		                <ul class="dropdown-menu " role="menu" style = "text-decoration:none;">
		                    <li >
								<a class="" href = '/my/quizslides/view/<?php echo $quizslide['id']; ?>' target = "_blank" >View</a>
		                    </li>
							<li >
								<a class="" href = '/my/quizslides/edit/<?php echo $quizslide['id']; ?>'>Edit</a>
		                    </li>
							<li>
								<?php echo $this->Form->postLink(__('Delete'), '/my/quizslides/delete/'.$quizslide['id'], null, __('Are you sure you want to delete # %s?', $quizslide['id'])); ?>
		                    </li>
		                </ul>
		            </li>
		          </ul>
			</td>


			

			<?php if (count($quizslides)>1): ?>
				<td style = "text-align:center;" >
					<span class = "btn-group-vertical repos_btns">
						<a href = "/quizslides/moveup/<?php echo $quizslide['id']; ?>" class = 'btn btn-default btn-xs'><span class = "glyphicon glyphicon-arrow-up"></span></a>
						<a  href = "/quizslides/movedown/<?php echo $quizslide['id']; ?>" class = 'btn btn-default btn-xs'><span class = "glyphicon glyphicon-arrow-down"></span></a>
					</span>
				</td>

			<?php
			else:
				?>
				<td style = "text-align:center;" >

				</td>					
				<?php
			 endif; ?>



		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
	<?php echo $this->BootstrapPaginator->pagination(); ?>
