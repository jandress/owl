<?php
	$_g = $this->data['Quizslide']['class'];	
	if (empty($this->data[$_g])): ?>
		<h2>Error missing or orphaned <em><?php echo $_g ?></em></h2>
		<p>
		the <em><?php echo $_g ?></em> activity for this quizslide has been deleted or does not exist. Check the cms for this slide.
		</p>
		<div class="tm_2_em">
			<?php echo $this->element('Quizmodules.Quizslides/quizslide_navbar'); ?>			
		</div>
	<?php else: ?>
		<?php
		if (empty($isAjax)): ?>
			<div id='quiz_container'>
				<div id="quiz_ajax_content">
					<?php echo $this->element($this->data['Quizslide']['class'].'/view_elements'); ?>
					<?php echo $this->element('Quizmodules.Quizslides/quizslide_navbar'); ?>
				</div>
			</div>
		<?php else: ?>
			<?php echo $this->element($this->data['Quizslide']['class'].'/view_elements'); ?>
			<?php echo $this->element('Quizmodules.Quizslides/quizslide_navbar');
		endif ?>


	<?php endif; ?>
