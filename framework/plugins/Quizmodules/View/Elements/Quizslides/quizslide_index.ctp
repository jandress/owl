<?php $quizid = $this->data['Quiz']['id']; ?>
<div class="row-fluid">
	<div class="navbar-inverse">

	<div class="col-md-6">
		<h2><?php echo __( __('Slide Listing For Quiz #'.$this->data['Quiz']['id']));?><br/></h2>
		<h3><em><?php echo $this->data['Quiz']['name']; ?></em></h3>
	</div>
	<div class="col-md-6">
		<a class = 'btn btn-default btn-sm tm_2_em bm_1_em pull-right rm_1_em' target ="_blank" href = "/quizzes/view/<?php echo $this->data['Quiz']['id']; ?>"  >View Quiz &nbsp;<span class='icon-eye-open tm_2_px'>&nbsp;</span></a>
	</div>




		<div class = "clear bm_1_em">&nbsp;</div>

		</div>
			<div class = "admin_subsub">
			<div class="admin_padding tm_10_px">
					<?php
						echo $this->Breadcrumbs->breadcrumbs($breadcrumbs);
						$navContent = '<div class="btn-group pull-right bm_10_px">';
						$navContent .= $this->BootstrapLayout->iconButton('Add A New Slide','icon-plus',array('side'=>'left','href'=>'/my/quizslides/add/'.$quizid));
						$navContent .= '</div>';
					?>
					<?php echo $navContent; //$this->BootstrapLayout->navbar($navContent ); ?>
					<div style = "line-height:1px; height:1px;" class = "clearfix">&nbsp;</div>
			</div>
		</div>
		<div class = "admin_padding" id = "landing_tier_table">
			<?php echo $this->element('Quizmodules.Quizslides/quizslide_index_table'); ?>
		</div>
</div>




<script type="text/javascript">
	function bindRepos(){
		$(".repos_btns a").unbind('click');
		$(".repos_btns a").click(function(e){
			e.preventDefault();
			var _url = $(e.currentTarget).attr("href");
			$("#landing_tier_table").empty();
			$.ajax({
				url: _url,
				context: $("#landing_tier_table"),
				success: function(data){
					//console.log('success: '+data);
					//return;
					$(this).empty();
					$(this).html(data);
					bindRepos();
				}
			});
			return false;
		});
	}
	bindRepos();
</script>
