<script type="text/javascript">	

	var _slideContentClass = "<?php echo $this->data['Quizslide']['class']; ?>";
	var _originalFid="<?php echo $this->data['Quizslide']['foreign_id']; ?>";
	var _originaltitle = "<?php echo $this->data[$this->data['Quizslide']['class']]['name']; ?>";
	var changed = false;
	var jsn = <?php echo $slide_options_json; ?>

	function updateTable(val){
		//console.log(val);
		$("#slide_content_details").html(val);
	}

	function changeNav(val){
		if(val==_slideContentClass){
			changed = false;
			site_flash.displaySuccess("Ok!","This setting will preserve previously existing content.", 2000, function(){});
			updateTable(_slideContentClass+': '+_originalFid+"  "+_originaltitle);	
			$("#QuizslideForeignId").val(_originalFid);
		}else{
			if(!changed){
				site_flash.displayFailure("Warning!","Changing the quizslide content type will result the previous slide being overwritten.", 2000, function(){});
				changed = true;
			}	
			if(val=="new"){
				updateTable(jsn[val]);	
			}else{
				updateTable("New "+jsn[val]);					
			}
			$("#QuizslideForeignId").val('');
		}
	}
	$("#QuizslideClass").change(function(e){
		changeNav(e.target.value);
	});
</script>


