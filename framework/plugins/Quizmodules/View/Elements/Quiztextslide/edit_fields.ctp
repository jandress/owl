<div class = "admin_padding col-md-9">
	<h2>Text Slide</h2>
		<?php 
			echo $this->BootstrapForm->create('Quiztextslide');
			echo $this->BootstrapForm->input('Quiztextslide.name',array('class'=>'col-md-12', 'label'=>'Head')); 
		?>
		<div class ="clear clearfix tm_10_px">&nbsp;</div>	
		<?php 
			echo $this->BootstrapForm->input('Quiztextslide.subhead',array('class'=>'col-md-12', 'label'=>'Subhead')) 
		?>
		<div class ="clear clearfix tm_10_px">&nbsp;</div>	
		<?php 
			echo $this->BootstrapForm->input('Quiztextslide.text',array('class'=>'col-md-12 ckeditor', 'label'=>'Copy'));
		 ?>
		<?php 
			
			echo $this->BootstrapForm->submit('submit', array('class'=>"pull-right btn tm_1_em"));
			echo $this->BootstrapForm->end();
		?>
</div>
<div class ="clear"></div>



  