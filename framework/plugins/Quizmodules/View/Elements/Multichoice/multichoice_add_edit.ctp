<div class="row-fluid">
	<div>
		<?php if (!empty($this->data['Multichoice']['name'])): ?>
				<h2><?php echo 'Editing Multiple Choice Question: '?></h2>			
				<h3><?php echo $this->data['Multichoice']['name']; ?></h3>
		<?php else: ?>
				<h2>New Multiple Choice Question</h2>			
		<?php endif ?>
			<hr/>		
		<?php echo $this->BootstrapForm->create('Multichoice', array('class' => 'form-horizontal'));?>

		<?php echo $this->element('Quizmodules.Multichoice/edit_fields'); ?>							

		<?php echo $this->BootstrapForm->submit(__('Submit'),array('class'=>'btn pull-right'));?>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
