<h3>Multiple Choice Question</h3>							

<fieldset>
	<?php
	echo $this->BootstrapForm->create('Multichoice');
	$ct = "Correct!";
	$ict = "Incorrect!";	
	$size = array('class'=>'col-md-10');
	echo $this->BootstrapForm->input('Multichoice.directive', array('class'=>'col-md-12 ckeditor',	'required' => 'required','helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;'
	));
?>
	<?php echo $this->BootstrapForm->input('Multichoice.name', array('class'=>'col-md-12 ckeditor', 'label'=>'Question text')); ?>
	<div class = "clearfix">&nbsp;</div>
<?php

	echo $this->BootstrapForm->hidden('Multichoice.id');	
	echo $this->BootstrapForm->input('Multichoice.correct_text', array(
		'required' => 'required'
		,'label'=>'Response to Correct Answer'		
		,'default'=>$ct		
		,'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;'
		,'class'=>'col-md-12'
		)
	);
	echo $this->BootstrapForm->input('Multichoice.incorrect_text', array(
		'label'=>'Response to Incorrect Answer'
		,'default'=>$ict		
		,'required' => 'required'
		,'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;'
		,'class'=>'col-md-12'
		)
	);
	//echo $this->element('Quizslides/response_delay');
	echo $this->BootstrapForm->submit('Next >',array('class'=>'btn pull-right tm_1_em'));
	echo $this->BootstrapForm->end();
	

?>
</fieldset>


<div class = "clearfix tm_2_em">&nbsp;</div>

