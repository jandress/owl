<script src="/Quizmodules/js/multichoice_admin.js"></script>
<script type="text/javascript">
	var ma = multichoice_admin();
	function addRow(){		
		ma.addRow('/my/multichoices/addrow/'+<?php echo $this->data['Multichoice']['id']; ?>+"/"+$('.multichoice_answer_row').length);
	};
	function removeRow(id){		
		ma.removeRow(id);
	};
</script>
<?php 
echo $this->BootstrapForm->create('Multichoiceanswers', array('class' => 'form-horizontal'));?>
	<table id='answers_table' class="table table-striped table-bordered table-hover">
		 <thead>
			<tr>
				<th class = 'col-md-1' style='text-align:center;'>
					Correct Answer
				</th>
				<th class = 'col-md-11'>
					Potential Response
				</th>								
				<th>	
				<a class="btn btn-default pull-right" onclick="addRow()">
					<i class="icon-plus"></i>
					&nbsp; New Response
				</a>
				</th>	
			</tr>
			</thead>
		<tbody>							
		<?php
		 	if (!empty($this->data['Multichoiceanswer'])): 
				foreach ($this->data['Multichoiceanswer'] as $key => $answer) {
					echo $this->element('Quizmodules.Multichoice/edit_answer_table_row',array('answer'=>$answer,'key'=>$key));							
				}
			?>
		<?php endif ?>
		</tbody>															
	</table>




<?php 
	echo $this->BootstrapForm->submit('Submit',array('class'=>'btn btn-default  tm_1_em pull-right'));
	echo $this->BootstrapForm->end();
?>