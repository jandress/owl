<?php echo $this->Form->create('Quizslide'); ?>
<?php echo $this->Form->create('quiz_id'); ?>
<?php echo $this->Form->hidden('Quizslide.class'); ?>


<?php echo $this->Form->input('Multichoice.id'); ?>
<div class="bm_1_em">

	<h3><?php echo h($this->data['Multichoice']['directive']); ?></h3>
	<?php
//	debug($this->data);
	 echo ($this->data['Multichoice']['name']); ?>
	<div class = "clearfix">&nbsp;</div>
	<div class="tm_1_em pl_1_em pr_1_em">
		<?php echo $this->element('shared/correct_incorrect_alerts',array('class'=>'Multichoice')); ?>	
	</div>		
	<div class="pl_1_em pr_2_em">
		<table class="table table-striped table-bordered">
			<?php foreach ($multichoiceanswer as $key => $v): 

				$value = $v['Multichoiceanswer']; ?>		
				  <tr>
				    <td class="span1" style="text-align:center">
						<input type="radio" name="responses" value="<?php echo $value['id'] ?>" />

					</td>
				    <td class="pt_10_px pl_1_em">
						<?php echo $value['name'] ?>
					</td>
				  </tr>
			<?php endforeach ?>	
		</table>
		<div class = "clearfix" style='height:1px;'>&nbsp;</div>
		<div class="pl_3_em pr_4_em tm_2_em">
			<a id = 'submit' href = "<?php echo $here; ?>" class = 'btn btn-block btn-large rm_2_em  ajax_submit' >Submit</a>	
		</div>
		<div class = "clearfix">&nbsp;</div>
	</div>
</div>		

<?php echo $this->Form->end(); ?>




 

<?php if (empty($isAjax)): ?>
	<script type="text/javascript" charset="utf-8" src = '/Quizmodules/js/quizslides.js' ></script>	
<?php endif ?>




<script type="text/javascript">
	var selected;
	var value;
	<?php $id = $this->data['Multichoice']['id'];  ?>
	$("input:radio[name=responses]").click(function() {
	    value = $(this).val();
		$("#submit").removeClass('disabled');
		$("#submit").addClass('btn-success');

//		$("#submit").unbind('click');
		// $("#submit").click(function(){
		// 	submit();
		// })
	});
	// function submit(){
	// 	submitAnswer({
	// 		url:'/my/multichoice/checkanswer/'+<?php echo $id ?>+'/'+value,
	// 		type: "POST",
	// 		data: {response:value},	
	// 	});
	// };
	$(document).ready(function () {
		$('input:radio').attr('checked',false);					
		$('input[name=responses]').attr('disabled',false);					
	});
</script>