<?php





$rowId = uniqid(); 
if (empty($answer)) {
	$answer = array();
}
if (empty($answer['id'])) {
	$answer['id']='';
}
if (empty($answer['name'])) {
	$answer['name']='';
}
if (empty($answer['multichoice_id'])) {
	$answer['multichoice_id']='';
}
if (empty($key)) {
	$key = 0;	
}
if (!empty($elementParameters['key'])) {
	$key = $elementParameters['key'];	
}
$checked = '';
if (!empty($this->data['CorrectAnswer']['id'])) {
	if ($answer['id']==$this->data['CorrectAnswer']['id']) {
		$checked = "checked = 'checked'";
	}
}
?>





<tr id = '<?php echo $rowId ?>' class='multichoice_answer_row'>
	<td style='text-align:center;' class = 'pt_1_em pb_1_em'>

		<input type="radio" name="data[Multichoice][multichoiceanswer_id]" value="<?php echo $key ?>" <?php echo $checked ?>>
		<?php
		// this part  and the multichoice_id needs an empty() conditional. value
		//		<input type="hidden" name="data[Multichoiceanswer][Multichoice][id]" value="1" id="MultichoiceId"/>		
		echo($answer['id']);
		?>
	</td>
	<td class = 'pt_1_em pb_1_em'>										
		<input type="hidden" name="data[Multichoiceanswer][<?php echo $key ?>][id]" value="<?php echo $answer['id'] ?>" id="MultichoiceAnswerId<?php echo $answer['id'].$key ?>"/>		
		<input type="hidden" name="data[Multichoiceanswer][<?php echo $key ?>][multichoice_id]" value="<?php echo $answer['multichoice_id'] ?>" id="MultichoiceId<?php echo $answer['id'].$key ?>"/>		
		<textarea name="data[Multichoiceanswer][<?php echo $key ?>][name]" class="ckeditor col-md-12" rows="3" id="Answer<?php echo $key ?>"><?php echo htmlentities($answer['name']); ?></textarea>
	</td>
	<td style='text-align:center;' class = 'pt_1_em pb_1_em'>
		<a class='btn btn-default' onclick = 'removeRow("<?php echo $rowId ?>")'><div class="icon-minus">&nbsp;</div> <?php echo $answer['id'] ?></a>
	</td>																
</tr>