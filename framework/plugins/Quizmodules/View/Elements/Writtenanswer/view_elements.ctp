<div class="offset1 col-md-10">
	<div id="question_container">
		<h3><?php echo h($this->data['Writtenanswer']['directive']); ?></h3>
		<h4>
			<?php echo h($this->data['Writtenanswer']['question']); ?>
		</h4>
		<div class = "clearfix">&nbsp;</div>

		<?php echo $this->Form->create('Writtenanswer'); ?>
		<?php echo $this->Form->input('Writtenanswer.id'); ?>
		<?php echo $this->Form->input('Writtenanswer.answer_text',array('label'=>'','class'=>'col-md-12','value'=>'')); ?>						
			<?php echo $this->element('shared/correct_incorrect_alerts',array('class'=>'Writtenanswer')); ?>
		<a onclick = 'checkAnswer()' class = 'btn pull-right'>Submit</a>	
		<div class = "clearfix">&nbsp;</div>
		<?php echo $this->Form->end(); ?>						
	</div>
</div>		
<div class = "clearfix">&nbsp;</div>
<hr/>
<?php echo $this->element('Quizmodules.shared/prev_next_arrows'); ?>		
<div class = "clearfix">&nbsp;</div>


<?php if (empty($isAjax)): ?>
	<script type="text/javascript" charset="utf-8" src = '/Quizmodules/js/quizslides.js' ></script>	
<?php endif ?>

<script type="text/javascript">
	function checkAnswer(){
		//checkanswer($questionId,$answer)
		submitAnswer({
			url:'/written/checkanswer/'+$("#WrittenanswerId").val()+"/"+$("#WrittenanswerAnswerText").val(),
			type: "POST",
			data: {response:$("#WrittenanswerAnswerText").val()},	
		});
	}
</script>