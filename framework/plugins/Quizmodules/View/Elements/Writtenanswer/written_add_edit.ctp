
<div class="row-fluid">
	<div class = "navbar-inverse">
		<?php if (!empty($this->data)):?>
			<h2>Editing: <?php echo $this->data['Writtenanswer']['name']; ?></h2>
		<?php else: ?>
			<h2>New fill in answer.</h2>
		<?php endif ?>		
	</div>


		<div class="admin_subsub"> 
			<div class="admin_padding tm_10_px bm_12_px">
				<?php  echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
			</div>
		</div>


	<div class="admin_padding">
		<?php echo $this->BootstrapForm->create('Writtenanswer', array('class' => 'form-horizontal'));?>
		<?php echo $this->element('Quizmodules.Writtenanswer/edit_fields'); ?>							
		<?php
		// echo $this->BootstrapForm->submit(__('Submit'),array('class'=>' btn pull-right'));
		echo '';
		?>
		<a href = '<?php echo $here; ?>' class='btn btn-default ajax_submit tm_1_em pull-right'>Next!</a>				
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>