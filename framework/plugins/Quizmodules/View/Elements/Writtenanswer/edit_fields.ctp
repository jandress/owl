<?php $span='col-md-12' ?>
<h3>Written Answer</h3>
<fieldset>
		<?php
		echo $this->BootstrapForm->input('Writtenanswer.directive', array(
			'required' => 'required',
			'class'=>$span,
			'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
		);
		echo $this->BootstrapForm->input('Writtenanswer.name', array(
			'required' => 'required',
			'class'=>$span,
			'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
		);
		// echo $this->BootstrapForm->input('fib_correct_answer_id', array(
		// 	'required' => 'required',
		// 	'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
		// );
		echo $this->BootstrapForm->input('Writtenanswer.correct_answer_response', array(
			'required' => 'required',
			'value'=>'Correct!',
			'class'=>$span,					
			'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
		);
		echo $this->BootstrapForm->input('Writtenanswer.incorrect_answer_response', array(
			'required' => 'required',
			'value'=>'Incorrect!',
							'class'=>$span,	
			'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
		);
		echo $this->BootstrapForm->input('Writtenanswer.answer_text', array(
			'required' => 'required',
			'class'=>$span,	
			'label'=>'Answer',
			'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
		);
		?>
</fieldset>
<div class = "clearfix">&nbsp;</div>
