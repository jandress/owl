<div class="btn-group btn-group-vertical redirect_buttons" >
	<a class="btn btn-small sitemap_small_button up-btn" href="/my/<?php echo $controller; ?>/relocate/<?php echo $id; ?>/up/" >
		<div class = "sitemap_icon icon-chevron-up">&nbsp;</div>					
	</a>
	<a class="btn btn-small sitemap_small_button down-btn" href="/my/<?php echo $controller; ?>/relocate/<?php echo $id; ?>/down/">
		<div class = "sitemap_icon icon-chevron-down">&nbsp;</div>				
	</a>
</div>