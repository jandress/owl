<?php echo $this->Form->create('Quizslide'); ?>
<?php echo $this->Form->hidden('quiz_id'); ?>
<?php echo $this->Form->hidden('Quizslide.class'); ?>
<?php echo $this->Form->hidden('Wordselect.question'); ?>
<?php echo $this->Form->hidden('Wordselect.id'); ?>
<?php $wordselect = $this->data['Wordselect'];

 ?>
<div class="offset1 col-md-10">
   <div id="question_container">
	     <span class="question-id" style="display:none;"><?php echo h($wordselect['id']); ?></span>
	     <h3><?php echo h($wordselect['directive']); ?></h3>
	     <h4><?php echo h($wordselect['name']); ?></h4>
		<div class="tm_2_em bm_3_em">
			<div class="selector span9 tm_1_em bm_1_em word-split"></div>
			<div class="hint" style="display:none;">
		      	<?php echo h($wordselect['hint']); ?>
		    </div>
		</div>
	   <?php echo $this->element('Quizmodules.shared/correct_incorrect_alerts',array('class'=>'Wordselect')); ?>
	</div>
</div>


<?php echo $this->BootstrapForm->hidden('correct_answer',array('value'=>$this->data['Wordselect']['answer'])); ?>
<?php echo $this->BootstrapForm->hidden('Wordselect.answer'); ?>
<?php echo $this->BootstrapForm->hidden('response'); ?>

<div class = "clearfix bm_2_em">&nbsp;</div>
<div class = "clearfix">&nbsp;</div>
<div class="pull-right btn-group">
	<?php echo ''; //		  <a class="hint btn">Hint</a> ?>
    <a class="btn btn-sm btn-default" onClick = 'showAnswers()'>Show Answers</a>
    <a class = 'btn btn-sm btn-default  ajax_submit' id = 'submit' href = "<?php echo $here; ?>" >Submit</a>
	<?php
  //echo $this->BootstrapForm->submit('Submit');
	 echo '';?>
</div>
<div class = "clear clearfix tm_2_em bm_2_em">&nbsp;</div>
<?php echo $this->BootstrapForm->end();?>

<script type="text/javascript" src='/Quizmodules/js/admin_word_selects.js' charset="utf-8"></script>

<script type="text/javascript" >
	var ws;
	$( document ).ready( function(){
		ws = admin_word_selects("<?php echo addslashes($this->data['Wordselect']['question']) ?>")
		ws.applyInteractivity();		
	});
  function showAnswers() {
  		ws.showAnswers();
  };
</script>
