<h2>Word Selection</h2>


<div class="admin_padding">
	<div class="">
		<?php echo $this->BootstrapForm->create('Wordselect', array('class' => 'form-horizontal'));?>
		<div class = "clearfix">&nbsp;</div>
		<?php
			$span = 'col-md-10';
			echo $this->BootstrapForm->input('Wordselect.name', array(
				'required' => 'required',
				'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;'
				,'class'=>$span
				)
			);
			echo '<div class = "clearfix bm_1_em">&nbsp;</div>';
			echo $this->BootstrapForm->input('Wordselect.directive', array(
				'required' => 'required',
				'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;'
				,'class'=>$span
				)
			);
			echo '<div class = "clearfix bm_1_em">&nbsp;</div>';
			echo $this->BootstrapForm->input('Wordselect.hint', array(
				'required' => 'required',
				'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;'
				,'class'=>$span
				)
			);
			echo '<div class = "clearfix bm_1_em">&nbsp;</div>';
			echo $this->BootstrapForm->input('Wordselect.question', array(
				'required' => 'required',
				'class'=>'question-input',
				'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;'
				,'class'=>$span.' question-input'
				)
			);
			echo $this->BootstrapForm->hidden('Wordselect.id');
		?>
		<div class = "clearfix bm_2_em">&nbsp;</div>


		<div class="well">
			<div class="white_well pt_1_px pb_1_px">
				<h5>
					Click words to select. Click again to deselect.
				</h5>
			</div>


			<div class="white_well">
				<div class="selector span9 tm_1_em bm_1_em word-split"></div>
				<div class = "clearfix">&nbsp;</div>
			</div>
		</div>


		<?php echo $this->BootstrapForm->hidden('Wordselect.answer', array('class'=>$span)); ?>
		<div class = "clearfix bm_2_em">&nbsp;</div>
		<?php echo $this->BootstrapForm->submit('Submit',array('class'=>'btn btn_default btn-default pull-right'));?>
		<?php echo $this->BootstrapForm->end();?>
	</div>



</div>







<script type="text/javascript" src='/Quizmodules/js/admin_word_selects.js' charset="utf-8"></script>
<script type="text/javascript" >
	var ws = admin_word_selects("<?php echo addslashes($this->data['Wordselect']['question']) ?>")
	ws.applyAdminInteractivity();
	//aws.updateWordSelectField($(".question-input"));
	//aws.indicateInitialHighlights($("#WordselectAnswer").val().split(','));
</script>
