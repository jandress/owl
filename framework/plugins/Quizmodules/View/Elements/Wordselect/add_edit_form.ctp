<div class = "navbar-inverse">
	<h2>
		<?php echo __('New %s', __('Wordselect')); ?>
	</h2>
</div>


<div class="admin_subsub">
	<div class="admin_padding tm_10_px bm_12_px">
		<?php  echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
	</div>
</div>


<div class="admin_padding">
	<div class="">
			<?php echo $this->element("Quizmodules.Wordselect/edit_fields"); ?>
	</div>
</div>
