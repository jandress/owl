<script type="text/javascript">	

	var sorting = function(){		
		// Text & Image Rendering Vars
		var fontSize = 14;
		var font = fontSize+'px sans-serif';
		var leading = 5;
		var pathToImages = '/media/sorting/';
		var h_padding = 10
		var v_padding = 40
		// canvas vars
		var canvas; // = document.getElementById('canvas');
		var HEIGHT; // = canvas.height;
		var WIDTH; // = canvas.width;
		var context; 
		var canvasInterval;
		var INTERVAL_LENGTH = 20; 	
		var redraw = true;
		// misc interactivity stuff

		var renderClips = [];	
		var renderClipsOriginalOrder = [];	
		var images = [];	
		var _data;
		var _originalRawData;

		var images_loaded = 0;
		var rolledOver = null
		var selected = null
		var rolledOverSide = "left"
		var dragging = false;
		//public parts
		var public_api = {		
			
			init:function(json , randomize){
				//--
				_originalRawData = json
				_data = json;
				//--
				if(randomize){
					_data = shuffle( _data );
				} else{
					_data = orderByAnswer( _data );									
				}
				canvasName = "canvas";			
				canvas = document.getElementById(canvasName);
				HEIGHT = $("#"+canvasName).parent().height();
				WIDTH = $("#"+canvasName).parent().width();
				canvas.width = $("#"+canvasName).parent().width();
				context = canvas.getContext('2d');
				context.textBaseline = 'top';
				canvasInterval = setInterval(_draw, INTERVAL_LENGTH);
				//
				document.onmousedown = _mouseDown;	           
				document.onmouseup = _mouseUp;	           			
				document.onmousemove = _mousemove;
				//

				_initObjects( _data );
				//
				$( window ).resize(function() {
					_resize()
				});
				_resize();				
			}
	
			,askDelete: function(){
				if(confirm("Are you sure you want to delete this?")){
					$.ajax({
						url: '/my/sortingpieces/delete/'+selected.id,
					  	type: "POST",
						success: function(data){
							_remove()			
				
						}
					});
					
				}
			}

			,showAnswers: function (){
				public_api.init(_originalRawData, false);
			}
		}

		function orderByAnswer(_data){
			var _t = $("#SortingAnswer").val().split(',');
			var ar = [];
			//iterate over the answer and the json data, and create an array of the json items in the order of the answer.
			console.log(_t)
			console.log(_data)
			for (var j=0; j < _t.length; j++) {
				for (var i=0; i < _data.length; i++) {
					if( _t[j] == _data[i].id){
						ar.push( _data[i] );
					}
				};
			};
			
			// if there are more items in the json than there are in the answer, 
			if( _data.length > _t.length ){
				
				for (var i = _t.length; i < _data.length; i++) {
					ar.push(_data[i])
					_t +=","+_data[i].id
				};
			}

			$("#SortingAnswer").val(_t.toString());			
			return ar;///_data
		}

		// privates
		function _initObjects(_data /* json db content */  ){
			// converts json database records into TextBlock and ImageBlock objects for canvas manipulation.  breaks up the text pieces into 1 TextBlock for each word with an array that stores it's siblings.
			renderClips = []; // all paragraph objects that get rendered.
			images = []
			var _rci = 0; // render clip iterator.  Because you are having to split up the "text pieces" into words, whereas image clips have no siblings.  you could probably do this more elegantly, but "getting it done" is primary here.   
			for (var i=0; i < _data.length; i++) {
				// create image blocks and text blocks.  the text blocks are assembled in groups of "sibling" arrays.   
				if(_data[i].type.toLowerCase()=="image"){
					renderClips[_rci] = new ImageBlock
					renderClips[_rci].setData( _data[i].name , _data[i].id );
					images.push(renderClips[_rci]);					
					_rci++
				}else{			
					var p = _data[i].name.split(" ");
					var pieceArray = []; // how a word knows it's siblings in a piece.
					for (var k=0; k < p.length; k++) {
						renderClips[_rci] = new TextBlock	
						pieceArray.push(renderClips[_rci]);					
						renderClips[_rci].setData( p[k] , _data[i].id, pieceArray); // ~! piece array might a copy of this array rather than a pointer to it.  so it might not be full for each textblock.  need to verify.					
						_rci++					
					};
				}
			};
			renderClipsOriginalOrder = [].concat(renderClips);
			redraw = true;		
		}
		
		function checkForOverlap(piece){
			function checkForVerticalOverlap(_img){
				if(piece == _img){ return false}; 
				var p_t = piece.y; // piece top
				var i_t = _img.y; // image top
				var i_b = i_t+_img.height; // +leading
				var piece_top_lower_than_image_top = (p_t > i_t);
				var piece_top_higher_than_image_bottom = (p_t < i_b)
				var conflicted = piece_top_lower_than_image_top && piece_top_higher_than_image_bottom;
				return conflicted;
			}			
			var p_l = piece.x; // piece_left
			var p_r = p_l+piece.width;
			var overlapped;
			//  iterate over images, check the image position against the current word position.
			for(var i = 0; i < images.length; i++){
				var x_overlapped = false;				
				var _img = images[i]		
				
				if(piece == _img){ return false; }
				//--
				var i_l = _img.x;
				var i_r = _img.x+_img.width;
					//--
				var piece_left_to_left_of_image_left = p_l <= i_l;
				var piece_right_to_the_right_of_the_image_left = p_r > i_l;
				//
				var piece_left_to_the_left_of_the_image_right = p_l < i_r;
				var piece_right_to_the_right_of_the_image_right  = p_r > i_r;	
				if(piece_left_to_left_of_image_left && piece_right_to_the_right_of_the_image_left){
					x_overlapped = true;					
				}
				if(piece_left_to_the_left_of_the_image_right && piece_right_to_the_right_of_the_image_right){
					x_overlapped = true;
				}				
				if(x_overlapped){
					if(checkForVerticalOverlap(_img)){
						return i_r;						
					}
				}
			}
			return false
		}
		
		function positionExceedsCanvasBounds(piece, _xpos){
			return _xpos+piece.width+fontSize/2 > canvas.width;
		}

		function clearPositions(){
			for (var i=0; i < images.length; i++) {
				images[i].x = images[i].y = -1000
			}			
			for (i=0; i < renderClips.length; i++) {			
				renderClips[i].x = renderClips[i].y = -1000
			}
		}

		function _draw(){	
			if(!document.getElementById(canvasName)){
				clearInterval(canvasInterval);
			}
			if(!redraw){return;}
			//clear the canvas and draw from scratch.
			context.clearRect(0, 0, canvas.width, canvas.height);		
			//---
			xpos = h_padding;
			ypos = v_padding;
			//--
			// for each render clip
			clearPositions();
			//---
			for (var i=0; i < renderClips.length; i++) {	
				var piece = renderClips[i];
				// check to see if this image will go past the width of the canvas.  if so, set xpos to 0 and increment ypos
				if( positionExceedsCanvasBounds(renderClips[i], xpos) ){
					xpos =  h_padding;
					ypos += fontSize+leading;
				}
				// next you need to check to see if the placement will 
				//-----
				renderClips[i].x = xpos;
				renderClips[i].y = ypos;
				var overlaps = checkForOverlap( renderClips[i] ); 
				if(overlaps){
					renderClips[i].x = overlaps
				}				
				var g = renderClips[i].draw()
				xpos = g.x+g.width			
			};		
			if(images_loaded >= images.length){
				setHeight();
			}
			drawDraggedCharacter();			
	//		redraw = false
		}

		// -------------------------------------------------------------------------------------------------------------------------
		// objects............
		// -------------------------------------------------------------------------------------------------------------------------

		function ImageBlock(){		
			this.src = '';
			this.value = 0;
			this.type = "image";
			this.text = "";

			this.index = 0;
			this.selected = false;
			this.x = 0;
			this.y = -1000;
			this.width = 0;
			this.height = 0;
			this.loaded = false;
			this.imageObj;
			this.style = ""	
			this.id = null
			this.siblings = [this];		
			var scope = this; // hack b/c javascript is wonky.  in the image.onload callback, "this" scope refers to the image object.  sigh...
			//--
			this.CreateImage = function (src){
				scope.imageObj = new Image()
				scope.imageObj.onload = function() {
					scope.loaded = true;
					scope.width = this.naturalWidth+fontSize/2
					scope.height = this.naturalHeight;
					images_loaded++
		        };	
				scope.imageObj.onerror = function() {
					scope.CreateImage('/img/loading.gif');
		        };	
				scope.src = scope.imageObj.src = src;
			}
			this.setData = function(src, id){	
				scope.id = id;
				scope.CreateImage(pathToImages+src)        
				scope.text ="Image: "+id+", src: "+src				
			}
						
			this.draw = function(){
				if(scope.loaded){
					context.drawImage(this.imageObj, this.x, this.y );						
					switch(scope.style){
						case('mouseover'):					
							context.strokeWidth = 2;
							context.strokeStyle =  '#666666';    														
							context.rect(this.x,this.y,(this.width-fontSize/2),this.height);
							context.stroke();												
						break;
						case('selected'):
							context.strokeWidth = 2;
							context.strokeStyle =  '#BD362F';    														
							context.rect(this.x,this.y,(this.width-fontSize/2),this.height);
							context.stroke();
						break;
						default:
						break;
					}
				}
				return {				
					'x' : scope.x, 
					'y' : scope.y, 
					'width' : scope.width,  
					'height': this.height,
					'value':this.value,
					'type':'image'				
				}
			}
		
			this.mouseDown = function(e){
				var ht = this._hitTest()
				if(ht){						
					for (var i=0; i < renderClips.length; i++) {
							renderClips[i].style = ''															
					};
					if(selected == this){
						// do unselect
						selected = null;						
						_disableEditUi()
					}else{
						// select
						selected = this;				
						this.style = 'selected'										
						_enableEditUi(selected.id)						
					}
				}
				 return ht
			}	

			this.mouseUp = function(e){
				var ht = this._hitTest();
				if(ht){
					if(selected != this && selected){
						reorder()			
					}
				}
				return ht	
			}
		
		
			this.mouseOver = function(e){
				return this._hitTest();
			}
			this.mouseOut = function(e){
				if(this._hitTest()){				
					this.style = ''
				}
			}			
			this.indicateRollOff = function(){
				if(this.style != 'selected'){
			//		this.style = ''
				}
			};
			
			this.indicateRollOver = function(){};	
					
			this._hitTest = function(){
				return mouseX > this.x 
	 				&& mouseX < this.x + this.width
					&& mouseY > this.y
	 				&& mouseY < this.y + this.height;
			};
	
			this.getTargetPoint = function(){
				var left = this.x
				var right = this.x+this.width
				var center = this.x + this.width/2
				//------------------------------------------				
				var target = {"x": 0, "y":this.y}
				//------------------------------------------				
				if( mouseX >  center  ){					
					rolledOverSide = "right";
					target.x = right;
				}else{
					target.x = left;
					target.y = this.y;				
					rolledOverSide = "left";					
				}
				return {
					"x":target.x,
					"y":target.y
				}
			}			
			this.getPieceGroupBeginning = function(){
				return this
			}
			this.getPieceGroupEnd = function(){
				return this
			}
	
		}
		
		function TextBlock(){	
				
			this.text = '';
			this.value = 0;
			this.type = "text";
			this.index = 0;
			this.selected = false;
			this.x = 0;
			this.y = 0;
			this.width = 0;
			this.height = fontSize+leading;
			this.siblings = null;
			this.style = ""
			var scope = this;
			this.id = null; // piece ID

			this.render = function(){
				switch(scope.style){
					case('selected'):
						context.fillStyle =  '#BD362F';    														
						context.fillRect(this.x-2,this.y,(this.width)+2,this.height);
						context.fillStyle =  '#ffffff';										
					break;

					case('mouseover'):
						context.fillStyle =  '#ececec';    														
						context.fillRect(this.x-2,this.y,(this.width)+2,this.height);
						context.fillStyle =  '#666666';
					break;
					case('targeted'):
						context.fillStyle =  '#BD362F';    														
						context.fillRect(this.x-2,this.y,(this.width)+2,this.height);
						context.fillStyle =  '#ffffff';
					break;					
					default:
					break;
				}				
				context.font = font;
				context.textBaseline = 'top';			
				context.fillText(this.text, this.x, this.y);			
			}
					
			this.setData = function(text, id, siblings){
				this.text = text;
				this.siblings = siblings;
				scope.id = id;
				this.render();
				this.textMetrics = context.measureText( this.text );
				this.width = this.textMetrics.width+fontSize / 2
			}

			this.draw=function(){
				context.fillStyle =  'black';  			
				this.render();
				return {				
					'x' : scope.x, 
					'y' : scope.y, 
					'width' : scope.width,  
					'height': scope.height,
					'value':scope.value,
					'type':'image',		
					'text':scope.text		
				}
			}
			

			
			this.mouseDown = function(e){
				var ht = this._hitTest();
				if(ht){
					if(selected == this){
						// do unselect
						selected = null;	
						for (var i=0; i < this.siblings.length; i++) {
							this.siblings[i].style = '';				
						};												
						_disableEditUi()
					}else{
						// select
						selected = this;
						for (var i=0; i < this.siblings.length; i++) {
							this.siblings[i].style = 'selected'					
						};
						for (var i=0; i < renderClips.length; i++) {
							if(this.siblings.indexOf(renderClips[i]) < 0){
								renderClips[i].style = ''					
							}						
						};				
						_enableEditUi(selected.id)						
					}
				}
				return ht				
			}	
			this.mouseUp = function(e){
				var ht = this._hitTest();
				if(ht){
					// draged over a different item
					if(selected != this && selected){
						reorder()			
					}					
				}	
				return ht				
			}
			
			this.mouseOver = function(e){
				return this._hitTest();
			}
			
			this.mouseOut = function(e){
				if(this._hitTest()){				
					for (var i=0; i < this.siblings.length; i++) {
						this.siblings[i].style = '';				
					};
				}
			}			
			
			this.indicateRollOff = function(){
				if(this.style != 'selected'){
					for (var i=0; i < this.siblings.length; i++) {
						this.siblings[i].style = '';
					};
				}
			};
						
			this.indicateRollOver = function(){
				if(this.style != 'selected'){
					for (var i=0; i < this.siblings.length; i++) {
						this.siblings[i].style = 'mouseover';
					};					
				}
			};			
			
			this._hitTest = function(){
				return mouseX > this.x 
	 				&& mouseX < this.x + this.width
					&& mouseY > this.y
	 				&& mouseY < this.y + this.height;
			};
			
			this.getTargetPoint = function(){
				var left = this.getPieceGroupBeginning();
				var right = this.getPieceGroupEnd();	
				//------------------------------------------				
				var center = left.x + ((right.x+right.width)-left.x)/2
				var target = {"x":0 ,"y" : 0};
				//------------------------------------------
				if( mouseX > ( center ) ){
					rolledOverSide = "right";
					target.x = right.x+right.width-3;
					target.y = right.y;
				}else{
					target.x = left.x-3;
					target.y = left.y;				
					rolledOverSide = "left";					
				}
				return {
					"x":target.x,
					"y":target.y
				}
			}			
			this.getPieceGroupBeginning = function(){
				// returns the first clip in the group
				return scope.siblings[0]
			}
			this.getPieceGroupEnd = function(){
				// returns the last clip in the group				
				return scope.siblings[scope.siblings.length - 1];
			}			
		}

		// -------------------------------------------------------------------------------------------------------------------------
		//End Textblock ------------------------------------------------------------------------------------------------------------
		// -------------------------------------------------------------------------------------------------------------------------
	
		function setAnswerValue(val){
			if(val[val.length-1]==","){
				val = val.slice(0,val.length-1);
			}
			$("#SortingAnswer").val(val)
		}
	
		function drawDraggedCharacter(tile){	
				//set styles and draw arrow
				if(!dragging){
					return;
				}
				if((!selected || !rolledOver) || ( selected == rolledOver)){
					return
				}
				var rolledOverTargetPoint = rolledOver.getTargetPoint();
				console.log()
				var t  = {
					x:rolledOverTargetPoint.x,
					y:rolledOverTargetPoint.y
				}
				context.beginPath();				    
								    context.strokeStyle =  '#BD362F';    
				context.lineWidth = 1;				    
				context.strokeStyle =  '#BD362F';    					
				//draw arrow
				var arrowPointY = t.y+4;
				var arrowPointLength = 8;				
				var arrowShaftLength = 15;				
				var arrow_width = 3;				
				var letter_bottom = arrowPointY+12;				

				context.moveTo(selected.x, selected.y);
				context.lineTo(selected.x, selected.y-10);
				context.lineTo(t.x, arrowPointY-arrowShaftLength);
				context.lineTo(t.x, arrowPointY)
				context.lineTo(t.x-arrow_width, arrowPointY-arrowPointLength)						
				context.moveTo(t.x, arrowPointY);
				context.lineTo(t.x+arrow_width, arrowPointY-arrowPointLength)						
				context.moveTo(t.x, t.y);					
				context.lineTo(t.x, letter_bottom);
				context.lineTo(t.x-arrow_width, letter_bottom);
				context.lineTo(t.x+arrow_width, letter_bottom);
				context.stroke();
		}
		// -------------------------------------------------------------------------------------------------------------------------
		//End DragIndicator ------------------------------------------------------------------------------------------------------------
		// -------------------------------------------------------------------------------------------------------------------------
		function _mouseDown(e){		
			_getMouse(e);	
			dragging = true;
			for (var i = 0; i < renderClips.length; i++) {
				renderClips[i].mouseDown(e);
			}			
			redraw = true;							
		}
	
		function _mouseUp(e){
			_getMouse(e);	
			dragging = false;
			for (var i = 0; i < renderClips.length; i++) {
				renderClips[i].mouseUp(e);
			}			
			//selected = null;								
			redraw = true;				
		}
	
		function _mousemove(e){
			_getMouse(e);	
			var ht = false 
			for (var i = 0; i < renderClips.length; i++) {
				var g = renderClips[i].mouseOver(e);
				if(g){
					rolledOver = renderClips[i];
					ht = true;
				}
			}							
			for (var i = 0; i < renderClips.length; i++) {
				if(rolledOver){
					if(rolledOver.siblings.indexOf(renderClips[i])>=0){
						renderClips[i].indicateRollOver()
					}else{
						renderClips[i].indicateRollOff()
					}
				}else{
					renderClips[i].indicateRollOff()
				}
			}			
			if(ht){
				document.getElementById(canvasName).style.cursor = "pointer";
			}else{
				document.getElementById(canvasName).style.cursor = "default";
			}
			redraw = true;				
		}

		function _disableEditUi(){
			$("#sortable_nav_edit_btn").addClass("disabled")
			$("#sortable_nav_delete_btn").addClass("disabled")			
		}
		
		function _enableEditUi(id){
			$("#sortable_nav_edit_btn").removeClass("disabled")
			$("#sortable_nav_delete_btn").removeClass("disabled")							
			$("#sortable_nav_edit_btn").attr("href",'/my/sortingpieces/edit/'+selected.id)			
		}
				
		function logPieces(ar){
			for (var i=0; i < ar.length; i++) {
				console.log("logPieces: "+ar[i].text);
			};
		}
				
		function reorder(){
			// reorders the render array as a result of drag / drop
			// indexes of pieces to relocate
			var startPiece = selected.siblings[0]
			var endPiece = selected.siblings[selected.siblings.length-1]
			var startIndex = renderClips.indexOf(startPiece)
			var endIndex = renderClips.indexOf(endPiece)	
			var working = [].concat(renderClips);					
		

			var piecesToMove = working.splice(startIndex, selected.siblings.length)
			var y = working;
			var rolledOverBeginingIndex = rolledOver.getPieceGroupBeginning()
			var rolledOverEndIndex = rolledOver.getPieceGroupEnd()						
			var insertPoint;
			//======
			if(rolledOverSide == "right"){
				 insertPoint = working.indexOf(rolledOverEndIndex)+1;								
			}else{
				insertPoint = working.indexOf(rolledOverBeginingIndex);
			}
			// left side 
			//======
			var leftSide = working.splice(0,insertPoint);
			var rightSide = piecesToMove.concat(working)
			renderClips = leftSide.concat(rightSide);																													
			redraw = true;		
			var str = ''
			var last_id = -1
			for (var i=0; i < renderClips.length; i++) {
				if(renderClips[i].id == last_id){
					continue; 
				}
				str += renderClips[i].id;							
				if(i < renderClips.length - 1 ){
					str += ","
				}
				last_id = renderClips[i].id
			};
			setAnswerValue(str);
			//	selected = 	rolledOver = null;
			setHeight()
		}
			
		function _remove(){
			// removes the selected piece form the render array as a result of drag / drop
			var startPiece = selected.siblings[0]
			var endPiece = selected.siblings[selected.siblings.length-1]
			var startIndex = renderClips.indexOf(startPiece)
			var endIndex = renderClips.indexOf(endPiece)				
			var m = renderClips.splice(startIndex, endIndex)
			//======
			redraw = true;		
		}
				
		function setHeight(){
			var h = 0;
			var t;
			for (var i=0; i < images.length; i++) {
				t = images[i].y + images[i].height;
				h = t > h ? t : h;
			};
			HEIGHT = h+v_padding		
			if(images.length == 0){
				HEIGHT	= 200
			}			
			_resize();							
		}

		function _resize(){
			var s = $("#"+canvasName).parent().width();
			if (s == canvas.width && canvas.height == HEIGHT){
				return;
			}
			canvas.width = s;
			canvas.height = HEIGHT;
			redraw = true;								
		}
		
		function _getMouse(e){
			var element = canvas;
			var offsetX = 0;
			var offsetY = 0;
			// Calculate offsets
			if (element.offsetParent) {
				do {
					offsetX += element.offsetLeft;
					offsetY += element.offsetTop;
				} while ((element = element.offsetParent));
			}	
			// Calculate the mouse location
			mouseX = e.pageX - offsetX;
			mouseY = e.pageY - offsetY;
		}

		function shuffle(array) {
		  var currentIndex = array.length, temporaryValue, randomIndex;

		  // While there remain elements to shuffle...
		  while (0 !== currentIndex) {

		    // Pick a remaining element...
		    randomIndex = Math.floor(Math.random() * currentIndex);
		    currentIndex -= 1;

		    // And swap it with the current element.
		    temporaryValue = array[currentIndex];
		    array[currentIndex] = array[randomIndex];
		    array[randomIndex] = temporaryValue;
		  }

		  return array;
		}

		return public_api;
	}
	
	
</script>
