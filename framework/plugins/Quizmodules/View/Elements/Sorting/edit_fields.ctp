<fieldset>
	<?php
		echo $this->BootstrapForm->create('Sorting');
		$ct = "Correct!";
		$ict = "Incorrect!";	
		$size = array('class'=>'col-md-10');
		//-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
		echo $this->BootstrapForm->input('name', array('class'=>'col-md-12'));
		echo $this->BootstrapForm->input('directive', array('class'=>'col-md-12',	'required' => 'required','helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;'
		));		
	?>
	<div class = "clearfix">&nbsp;</div>
	<?php
		echo $this->BootstrapForm->hidden('id');	
		echo $this->BootstrapForm->input('correct_text', array(
			'required' => 'required'
			,'label'=>'Response to Correct Answer'		
			,'default'=>$ct		
			,'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;'
			,'class'=>'col-md-12'
			)
		);
		echo $this->BootstrapForm->input('Sorting.incorrect_text', array(
			'label'=>'Response to Incorrect Answer'
			,'default'=>$ict		
			,'required' => 'required'
			,'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;'
			,'class'=>'col-md-12'
			)
		);
		//echo $this->element('Quizslides/response_delay');
		echo $this->BootstrapForm->submit('submit',array('class'=>'btn pull-right tm_1_em'));
		echo $this->BootstrapForm->end();
	?>	
</fieldset>