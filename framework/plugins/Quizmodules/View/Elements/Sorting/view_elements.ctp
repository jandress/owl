	<?php echo $this->BootstrapForm->create('Sorting'); ?>
	<?php echo $this->BootstrapForm->hidden('Sorting.id',array('class'=>'span10')); ?>
	<?php echo $this->BootstrapForm->hidden('Quizslide.class',array('class'=>'span10')); ?>
	<?php echo $this->BootstrapForm->hidden('Sorting.answer',array('class'=>'span10','value'=>'')); ?>
	<?php echo $this->BootstrapForm->hidden('Sorting.correctAnswer',array('class'=>'span10','value'=>$this->data['Sorting']['answer'])); ?>	

	<div class="col-large-12">
		<h1><?php echo h($this->data['Sorting']['name']); ?></h1>		
		<h3><?php echo h($this->data['Sorting']['directive']); ?></h3>		

	</div>

	<div class = "clearfix">&nbsp;</div>
	<div id="canvas-container" class = 'well'>
		<canvas id="canvas" style = '' >Your browser does not support HTML5.</canvas>
	</div>
	<br/>
	<div class = "clear clearfix bm_2_em ">&nbsp;</div>

	<div class="btn-group pull-right admin_padding">
			<a onclick="showAnswers()" class="btn btn-sm btn-danger">Show Answers</a>
			<a id = 'submit' href = "<?php echo $here; ?>" class = 'btn btn-success btn-sm rm_2_em  ajax_submit' >Submit</a>	
	</div>
	<?php echo $this->BootstrapForm->end(); ?>
	<div class="clear">&nbsp;</div>	   

<div class = "clear clearfix">&nbsp;</div>


<?php

	if(!empty($sortingpiece )){
		$ar = array();
		foreach ($sortingpiece as $key => $value) {
			array_push($ar, $value['Sortingpiece']);
		}		
		$sortingpiece = $ar;
	}else{
		$sortingpiece = $this->data['Sortingpiece'];
	}	
	
	
?>


<script src="/Quizmodules/js/sortings/DraggedCharacter.js"></script>
<script src="/Quizmodules/js/sortings/ImageBlock.js"></script>
<script src="/Quizmodules/js/sortings/Textblock.js"></script>
<script src="/Quizmodules/js/sortings/InteractiveParagraph.js"></script>
<script src="/Quizmodules/js/sortings/sortings.js"></script>


<?php

 if (count($sortingpiece) > 0): ?>	
	<script type="text/javascript">
		function showAnswers(){
			$("#SortingAnswer").val($("#SortingCorrectAnswer").val());
			g.init( <?php echo json_encode($sortingpiece); ?>, $("#SortingCorrectAnswer").val().split(","));
		}		
		
		
		function askDelete(){
			if(confirm("Are you sure you want to delete this?")){
				$.ajax({
					url: '/my/sortingpieces/delete/'+selected,
				  	type: "POST",
					success: function(data){
						_remove()			

					}
				});
			}
		}
		function _disableEditUi(){
			$("#sortable_nav_edit_btn").addClass("disabled")
			$("#sortable_nav_delete_btn").addClass("disabled")			
		}

		function _enableEditUi(id){
			$("#sortable_nav_edit_btn").removeClass("disabled")
			$("#sortable_nav_delete_btn").removeClass("disabled")							
			$("#sortable_nav_edit_btn").attr("href",'/my/sortingpieces/edit/'+id)			
		}	
		//--
		var g = sorting();
		var selected;
		//-
		g.onSelect = function(val){
			if(val){
				selected = val.id
				_enableEditUi(val.id)						
				if(val.type.toLowerCase()=="image"){
					console.log('yes')
					$("#sortable_nav_edit_btn").addClass("disabled")
				}			
			}else{
				_disableEditUi()
			}
		}
		g.onRelocate = function(data){
			var str = '';
			for (var i=0; i < data.length; i++) {
				str+=data[i].id
				if(i < data.length-1){
					str += ","
				}
			};
			$("#SortingAnswer").val(str);
		}
		function onAjaxPageReady(){
			g.init( <?php echo json_encode($sortingpiece); ?>, $("#SortingCorrectAnswer").val().split(","), true );
		}
	    
	</script>
<?php endif ?>
