<?php $uid = uniqid(); ?>
<div class="navbar-inverse">
	<h2>Drag And Drop Edit Fields</h2>
</div>


<div class = "admin_padding tm_1_em">
	<?php echo $this->BootstrapForm->create('Dragndrop'); ?>
	<?php echo $this->BootstrapForm->hidden('Dragndrop.id',array('class'=>'col-md-12')); ?>
	<?php echo $this->BootstrapForm->input('Dragndrop.name',array('class'=>'col-md-12')); ?>
	<?php echo $this->BootstrapForm->input('Dragndrop.instructions',array('class'=>'col-md-12','label'=>'Instructions')); ?>
	<?php echo $this->BootstrapForm->input('Dragndrop.response',array('class'=>'col-md-12','label'=>'Response to answer')); ?>
	<?php echo $this->BootstrapForm->input('Dragndrop.ieinstructions',array('class'=>'col-md-12','label'=>'IE Instructions')); ?>
	<hr/>
	<?php echo $this->BootstrapForm->input('Dragndrop.description',array('class'=>'col-md-12','label'=>'Paragraph')); ?>
	<small>Note.  If you make changes to the text, you will have to recreate your dragndrop positions.</small>
	<hr/>

	<div class="" style='padding-top:0px'>		

			<?php 
				if (!empty($this->data['Dragndrop']['answer'])) {
					echo $this->Form->hidden('Dragndrop.correct_answer',array('label'=>'','class'=>'col-md-12 tm_1_px','value'=>$this->data['Dragndrop']['answer'])); 		# code...
				}else{
					echo $this->Form->hidden('Dragndrop.correct_answer',array('label'=>'','class'=>'col-md-12 tm_1_px')); 		# code...		
				}
			?>	
				<?php echo $this->Form->hidden('Dragndrop.answer',array('label'=>'','class'=>'col-md-12 tm_1_px')); ?>	



			<div class = "clearfix">&nbsp;</div>
			<div id="canvas-container" style = ''><canvas id="canvas-<?php echo $uid; ?>" style = 'position:absolute;' >Your browser does not support HTML5.</canvas></div>

	</div>
		<?php 
	 		echo $this->BootstrapForm->submit('Next ->',array('class'=>'btn pull-right'));
		 ?>
	<?php echo $this->BootstrapForm->end(); ?>
</div>



<?php if ($isIE): ?>
	<?php echo $this->element('Quizmodules.Dragndrop/dragndrop_js_ie'); ?>							
<?php else: ?>
	<?php echo $this->element('Quizmodules.Dragndrop/dragndrop_js'); ?>							
<?php endif ?>







<script type="text/javascript">
	var d = dragndrop();				
	d.init($("#DragndropDescription").val(), "canvas-<?php echo $uid; ?>" ,1);
</script>


