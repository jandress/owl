<?php 
	$ar = array(
		'period',
		'comma',
		'singlequote',
		'quote',
		'semicolon',
		'colon',
		'exclaimation',
		'left-paren',
		'right-paren',
		'left-qbracket',
		'right-qbracket',
		'left-bracket',
		'right-bracket',
		'hyphen'
		);
?>
<div class="navbar" style = "margin-bottom:-1px">
	<div class="navbar-inner" style = 'text-align:center'>
		<div class="btn-group punctuation_icons" style = 'text-align:center'>
			<?php foreach ($ar as $key => $value): ?>
			<a class = 'btn punctuation_btn pi_<?php echo $value ?>'>
				<div class="punctuation_icon <?php echo $value ?> ">
					&nbsp;
				</div>
			</a>
			<?php endforeach ?>
		</div>

		<?php if (empty($admin)): ?>
				<div class="btn-group pull-right">
					<a id = "showAnswerBtn" onclick='showAnswers()'  class = 'btn btn-primary'>Show Answers</a>
				</div>			
		<?php endif ?>



	</div>
</div>