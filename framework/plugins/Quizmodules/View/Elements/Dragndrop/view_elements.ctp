<?php $uid = uniqid(); ?>
<?php echo $this->Form->create('Quizslide'); ?>
<?php echo $this->Form->hidden('quiz_id'); ?>
<?php echo $this->Form->hidden('Dragndrop.answer'); ?>
<?php echo $this->Form->hidden('Quizslide.class'); ?>
<?php echo $this->Form->hidden('Dragndrop.id'); ?>
<?php
	if (!empty($this->data['Dragndrop']['answer'])) {
		echo $this->Form->hidden('Dragndrop.correct_answer',array('label'=>'','class'=>'col-md-12 tm_1_px','value'=>$this->data['Dragndrop']['answer'])); 		# code...
	}else{
		echo $this->Form->hidden('Dragndrop.correct_answer',array('label'=>'','class'=>'col-md-12 tm_1_px')); 		# code...
	}
?>


<div class="white_well">
	<h2>Drag And Drop</h2>
	<h4><?php echo($this->data['Dragndrop']['name']); ?></h4>


	<?php if ($isIE): ?>
		<?php if (!empty($this->data['Dragndrop']['ieinstructions'])): ?>
			<p>
				<?php echo $this->data['Dragndrop']['ieinstructions'] ?>
			</p>
		<?php endif ?>
	<?php else: ?>
		<?php if (!empty($this->data['Dragndrop']['instructions'])): ?>
			<p id ="instructions" >
				<?php echo $this->data['Dragndrop']['instructions'] ?>
			</p>
		<?php endif ?>
	<?php endif ?>



	</div>
	<?php if ($isIE): ?>
		<div class="alert">
	 		IE detected. Rendering in reduced functionality mode.
		</div>
		<?php if ($isIECM): ?>
			<div class="alert alert-danger">
				Your IE is set to (nonstandard) COMPATIBILITY MODE.
				Rendering as IE 7.
			</div>
		<?php endif ?>
	<?php endif ?>



<?php echo $this->BootstrapForm->create('Dragndrop'); ?>
<?php echo $this->BootstrapForm->hidden('Dragndrop.id',array('class'=>'col-md-12')); ?>
<?php  echo $this->Form->hidden('Dragndrop.description',array('label'=>'','class'=>'col-md-12 tm_1_px','value'=>$this->data['Dragndrop']['description']));
?>




<div class = "clear clearfix">&nbsp;</div>
	<div class="white_well">
	<div class="tm_1_em pl_1_em ">
		<?php echo $this->element('shared/correct_incorrect_alerts',array('class'=>'Dragndrop')); ?>
	</div>


    <div id="canvas-container" style = ''><canvas id="canvas-<?php echo $uid; ?>" style = 'position:absolute;' >Your browser does not support HTML5.</canvas></div>

<div class = "clearfix">&nbsp;</div>


	<div class = 'btn-group pull_right pull-right bm_1_em tm_1_em'>
		<a id = 'submit' href = "<?php echo $here; ?>" class = 'btn btn-success btn-large  ajax_submit' >Submit</a>
		<a id = 'showanswers' onclick = "showAnswers()" class = 'btn btn-primary btn-large ' >Show Answers</a>
	</div>

	<div class = "clearfix tm_1_em bm_1_em">&nbsp;</div>

</div>
<?php echo $this->Form->end(); ?>


<?php if (empty($isAjax)): ?>
	<script type="text/javascript" charset="utf-8" src = '/Quizmodules/js/quizslides.js' ></script>
<?php endif ?>


<?php if ($isIE): ?>
	<?php echo $this->element('Quizmodules.Dragndrop/dragndrop_js_ie'); ?>
<?php else: ?>
	<?php echo $this->element('Quizmodules.Dragndrop/dragndrop_js'); ?>
<?php endif ?>

<div class = "clear">&nbsp;</div>



<script type="text/javascript">
	var d;
	function onSlideLoaded(){
			d = dragndrop();
			// DragndropCorrectAnswer
			d.init($("#DragndropDescription").val(),"canvas-<?php echo $uid; ?>");
	}
	onSlideLoaded()
	main_content_ajax_helper.onLoaded = onSlideLoaded;
	//main_content_ajax_helper.beforeSlide=function(){alert('yes');}
	function showAnswers(){
		d.showAnswers();
	};
	function test(){
		alert(d.getAnswers())
	}
</script>
