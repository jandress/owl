<div id ="flip_spacer">
	<div id ='flip-container' class="flip-container tm_1_em" ontouchstart="">
	<div class="flipper">
		


		<div class="front">
			<div id="front_content">
				<div class="well">
					<h2>Flashcards</h2>
					<h3><?php echo($this->data['Flashcard']['name']); ?></h3>				
					<p><?php echo($this->data['Flashcard']['description']); ?></p>		
					<div class="bm_2_em tm_4_em pl_8_em pr_8_em">
						<a onclick = "document.querySelector('#flip-container').classList.toggle('hover')" class = 'btn btn-large btn-block btn-success'>Flip</a>
					</div>
					<div class = "bm_2_em clear clearfix">&nbsp;</div>
				</div>
			</div>
		</div>



		<div class="back">
			<div id="back_content">
				<div class="well">
					<h3><?php echo($this->data['Flashcard']['name']); ?></h3>				
					<p><?php echo($this->data['Flashcard']['response']); ?></p>		
					<div class="bm_2_em tm_4_em pl_8_em pr_8_em">
						<a onclick = "document.querySelector('#flip-container').classList.toggle('hover')" class = 'btn btn-large btn-block btn-success'>Flip Back</a>
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript" >
	var n = 300;
	
	function dosize(ar){
		


		if($("#back_content").height()>$("#front_content").height()){
			var n = $("#back_content").height()	
		}else{
			var n = $("#front_content").height()		
		}
		$("#flip_spacer").css({'height':n+14});				


	}	
	dosize([
		'#front_content .well',
		'#back_content .well'
		]);
</script>

