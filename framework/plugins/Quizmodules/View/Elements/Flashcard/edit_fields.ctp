<div class = "navbar-inverse">
		<h2>Flashcard Edit Fields</h2>
</div>



<div class="admin_subsub"> 
	<div class="admin_padding tm_10_px bm_12_px">
		<?php  echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
	</div>
</div>



<div class="admin_padding">
		<?php echo $this->BootstrapForm->create('Flashcard'); ?>
		<?php echo $this->BootstrapForm->hidden('Flashcard.id',array('class'=>'col-md-12')); ?>
		<?php echo $this->BootstrapForm->input('Flashcard.name',array('class'=>'col-md-12')); ?>
		<?php echo $this->BootstrapForm->input('Flashcard.description',array('class'=>'col-md-12')); ?>
		<div class = "tm_1_em">&nbsp;</div>
		<?php echo $this->BootstrapForm->input('Flashcard.response',array('class'=>'col-md-12')); ?>
		<?php echo $this->BootstrapForm->submit("Next ->",array('class'=>'tm_1_em btn btn-default pull-right')); ?>		
		<?php echo $this->BootstrapForm->end(); ?>		
</div>
	

<?php if ($isAjax): ?>
	<script type="text/javascript">	
		var neditor = CKEDITOR.replace( 'FlashcardDescription' );
		var neditor2 = CKEDITOR.replace( 'FlashcardResponse' );
	</script>	
<?php endif ?>