<?php $span='col-md-12' ?>
<div>
	<?php
		echo $this->BootstrapForm->hidden('Fillintheblank.id');
		echo $this->BootstrapForm->input('Fillintheblank.name', array(
			'required' => 'required',
			'class'=>$span,
			'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
		);
		echo $this->BootstrapForm->input('Fillintheblank.h2', array(
			'class'=>$span,
			'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
		);
		echo $this->BootstrapForm->input('Fillintheblank.question_beginning', array(
			'label'=>'First part of question',
			'required' => 'required',
			'class'=>$span,
			'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
		);

		echo $this->BootstrapForm->input('Fillintheblank.question_end', array(
			'label'=>'Second part of question',
			'required' => 'required',
			'class'=>$span,
			'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
		);

		echo $this->BootstrapForm->input('Fillintheblank.correct_text', array(
			'label'=>'Correct answer message',
			'required' => 'required',
			'default'=>'Correct!',
			'class'=>$span,					
			'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
		);
		echo $this->BootstrapForm->input('Fillintheblank.incorrect_text', array(
			'required' => 'required',
			'label'=>'Incorrect answer message',
			'default'=>'Incorrect!',
			'class'=>$span,	
			'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
		);
	?>
	<div class = "clearfix tm_2_em">&nbsp;</div>
</div>

