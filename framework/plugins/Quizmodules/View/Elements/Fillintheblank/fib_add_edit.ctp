<div class = "navbar-inverse">
	<?php if (!empty($this->data)):?>
		<h2>
			Fill in the blank question:
		</h2>
		<h3>
			<?php echo $this->data['Fillintheblank']['question_beginning'].'____________'.$this->data['Fillintheblank']['question_end']; ?>
		</h3>
	<?php else: ?>
		<h3>Fill in the blank question</h3>
	<?php endif ?>
</div>




<div class="admin_subsub"> 
	<div class="admin_padding tm_10_px bm_12_px">
		<?php  echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
	</div>
</div>





<div class="admin_padding">
		<?php echo $this->BootstrapForm->create('Fillintheblank', array('class' => 'form-horizontal'));?>
		<?php echo $this->element("Quizmodules.Fillintheblank/edit_fields"); ?>	
		<?php echo $this->BootstrapForm->submit('Next >',array('class'=>'btn btn-default tm_1_em pull-right'));?>
		<?php echo $this->BootstrapForm->end();?>
</div>
