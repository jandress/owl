<?php echo $this->Form->create('Quizslide'); ?>
<?php echo $this->Form->create('quiz_id'); ?>
<?php echo $this->Form->hidden('Quizslide.class'); ?>
<?php echo $this->Form->input('Fillintheblank.id'); ?>


<div class="offset1 col-md-10">
	<div id="question_container">
		<h3><?php echo h($this->data['Fillintheblank']['name']); ?></h3>
		<h4><?php echo h($this->data['Fillintheblank']['h2']); ?></h4>
		<div class = "clearfix ">&nbsp;</div>
			<p> 
			<?php echo h($this->data['Fillintheblank']['question_beginning']); ?> 
				<input type="text" id="answer" required="required" name="data[response]"> <?php echo h($this->data['Fillintheblank']['question_end']); ?> 
			</p>
			<?php echo $this->element('Quizmodules.shared/correct_incorrect_alerts',array('class'=>'Fillintheblank')); ?>
			<div id="message_area"> </div>

		<div class="pl_3_em pr_4_em tm_2_em">
			<a id = 'submit' href = "<?php echo $here; ?>" class = 'btn btn-block btn-large rm_2_em  ajax_submit' >Submit</a>	
		</div>
	</div>
</div>		
<div class = "clearfix">&nbsp;</div>
<hr/>
<?php echo $this->element('Quizmodules.shared/prev_next_arrows'); ?>		
<div class = "clearfix">&nbsp;</div>


<?php echo $this->Form->end(); ?>



<?php if (empty($isAjax)): ?>
	<script type="text/javascript" charset="utf-8" src = '/Quizmodules/js/quizslides.js' ></script>	
<?php endif ?>
<script type="text/javascript">
	// (function() {
	// 	$("span.missing-word").html("<input type='text'></input>");
	// 	$("a.submit").click(function(){
	// 	  submitAnswer({
	// 	    url: '/fillintheblanks/checkanswer/'+($("#FillintheblankId").val()),			
	// 	    problemType: 'fillintheblanks',
	// 	    answer: $("#answer").val()
	// 	  });
	// 	});
	// })();




  $('#answer').keyup(function () {
		   $("#submit").removeClass('disabled');
	   		$("#submit").addClass('btn-success');

   });
 	// $("#answer").change(function(e){
 	// 	alert('e');
 	// });
// 	    value = $(this).val();
// 		
// 		$("#submit").addClass('btn-success');

// //		$("#submit").unbind('click');
// 		// $("#submit").click(function(){
// 		// 	submit();
// 		// })
// 	});

</script>