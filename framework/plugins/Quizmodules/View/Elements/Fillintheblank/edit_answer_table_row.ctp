<?php 
$rowId = uniqid(); 
if (empty($answer)) {
	$answer = array();
	$answer['id']='';
	$answer['answer_text'] = '';
}
if (empty($key)) {
	$key = 0;	
}
if (!empty($elementParameters['key'])) {
	$key = $elementParameters['key'];	
}
?>

<tr id = '<?php echo $rowId ?>' class='multichoice_answer_row'>
	<td style='text-align:center;' >	
		&nbsp;
	</td>
	<td class = 'pt_1_em pb_1_em'>		
		<input type="hidden" name="data[Fibanswer][<?php echo $key ?>][id]" value="<?php echo $answer['id'] ?>" id="MultichoiceId<?php echo $answer['id'] ?>"/>		
		<input type="text" name="data[Fibanswer][<?php echo $key ?>][answer_text]"  value= "<?php echo $answer['answer_text']; ?>" id="Answer" class = 'col-md-9' maxlength="500" required="required" >
		<span class="help-inline"><span class="label label-important">Required</span>&nbsp;</span>
	</td>
	<td style='text-align:center;' class = 'pt_1_em pb_1_em'>
		<a class='btn btn-default' onclick = 'quiz_helper.removeRow("<?php echo $rowId ?>")'><div class="icon-minus">&nbsp;</div></a>
	</td>																
</tr>
