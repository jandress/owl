<h3>Accepted Answers</h3>														
<table id='answers_table' class="table table-striped table-bordered table-hover">
		
 <thead>
	<tr>
		<th class = 'span1'>
			&nbsp;
		</th>								
		<th>
			&nbsp;
		</th>
		<th>
			&nbsp;
		</th>	
	</tr>
	</thead>
<tbody>							
	<?php if (!empty($this->data['Fibanswer'])): 
		foreach ($this->data['Fibanswer'] as $key => $answer) {
			echo $this->element('Quizmodules.Fillintheblank/edit_answer_table_row',array('answer'=>$answer,'key'=>$key));							
		}
		?>
	<?php else: ?>
		<?php echo $this->element('Quizmodules.Fillintheblank/edit_answer_table_row'); ?>							
	<?php endif ?>
</tbody>															
</table>					


<a class="btn pull-right" onclick="quiz_helper.addRowWithRowCount('/ajax/quiz/element/Quizmodules.Fillintheblank/edit_answer_table_row','.multichoice_answer_row')">
	<i class="icon-plus"></i>&nbsp;Add
</a>
<div class = "clearfix">&nbsp;</div>

