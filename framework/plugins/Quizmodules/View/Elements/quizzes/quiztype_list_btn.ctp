<?php if ($isDeveloper || $isAdmin): ?>
	<ul style = "list-style:none; padding:0" >
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle btn btn-sm btn-default tm_1_em" data-toggle="dropdown">
	          		Quizslide Types
	          </a>
	            <ul class="dropdown-menu  dropdown-menu-right" role="menu" style = "text-decoration:none;">
	            <?php
		            $types = array(
						array(
							'label' => "Text-only slide",
							'url'=>'/my/quiztextslides/'
							),
						array(
							'label' => "Fill In The Blank",
							'url'=>'/my/fillintheblanks/'
							),					
						array(
							'label' => "Multiple Choice",
							'url'=>'/my/multichoices/'
							),
						array(
							'label' => "Wordselect",
							'url'=>'/my/wordselects/'
							),
						array(
							'label'=>'Flashcards',
							'url'=>'/my/flashcards/'
							),					
						array(
							'label' => "Sentence Combining / Sorting ",
							'url'=>'/my/sortings/'
							),
						array(
							'label' => "Drag And Drop",
							'url'=>'/my/dragndrops/'
							)
		            );
	            ?>
	            <?php foreach ($types as $key => $value): ?>
	       			<li>	
						<a class = "	" href = '<?php echo $value['url']; ?>'><?php echo $value['label']; ?></a>
	                </li>
	            <?php endforeach ?>

	            </ul>
	        </li>
	</ul>	 
<?php endif ?>