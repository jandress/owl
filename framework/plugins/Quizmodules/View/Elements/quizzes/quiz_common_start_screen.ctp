<h2><?php echo $this->data['Quiz']['name'] ?></h2>
<p><?php echo $this->data['Quiz']['Description'] ?></p>
<div class = "clearfix">&nbsp;</div>
<div class="centered bm_2_em tm_1_em">
	<?php if ( !empty($this->data['Quizslide']) ): ?>
		<a id ="start_btn" class = "btn btn-large btn-success quiz_ajax_link" href = "/quizslides/view/<?php echo $this->data['Quizslide']['id'] ?>/" >Let's get started, shall we?</a>
	<?php else: ?>
		<div class="alert">
			<p>
				There are no slides for this quiz.
			</p>
		</div>
	<?php endif ?>
</div>
<div class = "clearfix">&nbsp;</div>
