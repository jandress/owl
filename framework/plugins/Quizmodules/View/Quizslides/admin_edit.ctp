<?php
	$quiz = $this->data['Quiz'];
	$quizid = $quiz['id'];
?>
<div class="row-fluid">
		<div class="navbar-inverse ">
			<h1>
				New Slide for quiz:
			</h1>
			<h2><em>
					<?php echo $quiz['name']; ?>
				</em>
			</h2>
		</div>
		
		<div class="admin_subsub"> 
			<div class="admin_padding tm_10_px">
				<?php  echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
			</div>
				<div class="clear bm_1_em">&nbsp;</div>			
		</div>
		

		


		<div class="admin_padding tm_1_em">		
			<?php echo $this->BootstrapForm->create('Quizslide');?>				
			<?php echo $this->BootstrapForm->input('id');?>	
			<?php echo $this->BootstrapForm->input('Quiz.id');?>	
			<?php echo $this->BootstrapForm->hidden('Quiz.name');?>	



			<?php
				if(!empty($this->data['Quizslide']['foreign_id'])){
					echo $this->BootstrapForm->hidden('Quizslide.foreign_id',array('value'=>$this->data['Quizslide']['foreign_id']));	
				}else{
					echo $this->BootstrapForm->hidden('Quizslide.foreign_id');
				}
			 ?>	

			
			<?php echo $this->BootstrapForm->input('Quizslide.name', array(
				'required' => 'required',
				'label'=>'Name / Description',
				'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;'
				,'class'=>'col-md-12'	
				)
			);
 			?>

			<?php //echo $this->BootstrapForm->input('Quizslide.response_delay'); 
					echo $this->element('Quizslides/response_delay'); ?>	

			<div class = "clearfix bm_1_em">&nbsp;</div>
			<?php
				if(!empty($this->data['Quizslide']['class'])){
						$class = $this->data['Quizslide']['class'];
				}
				if(!empty($this->data['Quizslide']['foreign_id'])){
					$foreign_id = $this->data['Quizslide']['foreign_id'];				
				}
				if(!empty($this->data[$class]['name'])){
					$name = $this->data[$class]['name'];	
				}				
			?>		
			<table class="table table-striped table-bordered table-hover">
				<tr>
					<th class = "col-md-2">
						Slide Type
					</th>
					<th>
					</th>
				</tr>
				<tr>
					<td>
						<?php echo $this->Form->input('Quizslide.class', array('options' => $slide_options,'label'=>'')) ?>
					</td>
					<td>
						<div id = "slide_content_details" class="">

		
<?php if (empty($orphaned)):
		$name = $this->data[$class]['name'];	



 ?>
<?php
	if(!empty($class)){
		echo($class.': '.$foreign_id.', '.$name); 
	}				
 ?>		
<?php else: ?>
	<div class="alert alert-danger" role="alert">Warning! The indicated quizslide activity for this slide is not found.  You will need to create a new activity.</div>
<?php endif ?>






						</div>
					</td>				
				</tr>
			</table>
			<div class = "clearfix bm_1_em">&nbsp;</div>




			<?php 
			echo $this->Form->submit('Next',array('class'=>'btn  pull-right'));
			echo $this->Form->end();
			?>			
			<div class = "clearfix bm_2_em">&nbsp;</div>
			<?php echo $this->element('Quizmodules.Quizslides/quizslide_edit_js'); ?>
		</div>
</div>
