<div class="well wb">
	<h2>
		Quizslides
	</h2>
	<?php echo $this->BootstrapPaginator->pagination(); ?>
	<table class="table table-striped table-bordered table-hover tm_1_em">
		<tr>
			<th style="text-align:center;">
				<?php echo $this->BootstrapPaginator->sort('id');?>
			</th>
			
			
			<th>
				<?php echo $this->BootstrapPaginator->sort('name');?>
			</th>
			<th><?php echo $this->BootstrapPaginator->sort('class');?></th>

			
			
						
			<th class="actions" style = "text-align:center"><?php echo __('Actions');?></th>
			<th class = "span1">&nbsp;</th>	
		</tr>
	<?php 
		foreach ($this->data as $quiz): 
			$qsuid = $quiz['Quizslide'][ 'id'];
			?>
		<tr>
			<td style="text-align:center;"><?php echo h($quiz['Quizslide'][ 'id']); ?>&nbsp;</td>
			<td><?php echo h($quiz['Quizslide'][ 'name']); ?>&nbsp;</td>
			<td><?php echo h($quiz['Quizslide'][ 'class']).': '.$quiz['Quizslide'][ 'foreign_id'] ?>&nbsp;</td>			
			<td class="actions" style = "text-align:center">
				<a href = '/quizslides/view/<?php echo $qsuid; ?>' target = "_blank" >View</a> | 
				<a href = '/my/quizslides/edit/<?php echo $qsuid; ?>'>Edit</a> | 
				<?php echo $this->Form->postLink(__('Delete'), '/my/quizslides/delete/'.$qsuid, null, __('Are you sure you want to delete # %s?', $qsuid)); ?>
			</td>
			<td style = 'text-align:center'>
				&nbsp;
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->BootstrapPaginator->pagination(); ?>
	
</div>