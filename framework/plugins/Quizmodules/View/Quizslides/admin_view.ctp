<div class="navbar-inverse">
	<div class="col-md-6">
		<h3>
			Viewing: <em><?php echo $this->data['Quizslide']['name']; ?></em>
		</h3>

		<p>
			<?php echo $this->data['Quizslide']['class'].': '.$this->data['Quizslide']['foreign_id']?>,  Viewing Quizslide #<?php echo $this->data['Quizslide']['id'] ?>
		</p>
	</div>
	<div class = "col-md-6">
		<div class="nav span3 pull-right">
			<a class="btn tm_1_em btn-sm btn-default pull_right ajax_link_back" href = "/my/quizslides/<?php echo $this->data['Quizslide']['quiz_id'] ?>" >back etc</a>
		</div>
	</div>

	<div class = "clear"></div>
</div>
<div class = "admin_padding">
	<?php echo $this->element('Quizmodules.Quizslides/slide');?>
</div>



