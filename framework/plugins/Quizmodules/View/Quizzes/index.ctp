<div>
	<div class="navbar-inverse admin-padd">
		<h1 class = "pull-left" >Quizzes</h1>
		<div class = "admin_padding">

		</div>
		<div class = "clearfix">&nbsp;</div>
	</div>
	<div class="admin_padding">
	<?php echo $this->BootstrapPaginator->pagination(); ?>


	<div class = "pull-right">
		<?php echo $this->element('Quizmodules.quizzes/quiztype_list_btn'); ?>
	</div>



	<div class = "clearfix tm_1_em">&nbsp;</div>
		<table class="table table-striped table-bordered table-hover tm_2_em">
		<tr>
			<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
			<th><?php echo $this->BootstrapPaginator->sort('name');?></th>
			<th><?php echo $this->BootstrapPaginator->sort('Description');?></th>
			<th class = 'span1 centered' >Slides<?php echo '' //$this->BootstrapPaginator->sort('Slides');?></th>

		</tr>
		<?php foreach ($quizzes as $quiz):

			?>
			<tr>

				<td>
					<?php echo htmlentities($quiz['Quiz']['id']); ?>&nbsp;
				</td>

				<td>
					<ul style = "list-style:none; padding:0" >
			            <li class="dropdown">
			              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<?php echo htmlentities($quiz['Quiz']['name']); ?>&nbsp;
			              </a>
			                <ul class="dropdown-menu " role="menu" style = "text-decoration:none;">
								<li >
									<a target = "_blank" href = '/quizzes/view/<?php echo $quiz['Quiz']['id'] ?>'>View</a>
			                    </li>
								<li>
									<a target = "_blank" href = '/standalone/quizzes/view/<?php echo $quiz['Quiz']['id'] ?>'>View as Standalone</a>
			                    </li>
								<li>
									<a class="lightbox_link" href = '/standalone/quizzes/view/<?php echo $quiz['Quiz']['id'] ?>'>View in Lightbox</a>
			                    </li>
			                </ul>
			            </li>
			          </ul>
				</td>

				<td>
					<?php echo strip_tags(String::truncate( ($quiz['Quiz']['Description']),200,array('exact'=>false)));?>
				</td>

				<td style = "text-align:center;">

					<ul style = "list-style:none; padding:0" >
			            <li class="dropdown">
			              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			              	<?php  echo(count($quiz['Quizslide'])); ?>
			              </a>
			                <ul class="dropdown-menu  dropdown-menu-right" role="menu" style = "text-decoration:none;">
			                    <?php foreach ($quiz['Quizslide'] as $key => $value): ?>
			                    <li>
			                    	<a target = "_blank" href="/my/quizslides/view/<?php echo $value['id']; ?>" class=""><?php echo $value['class'].': '.$value['name']; ?></a>

			                    </li>
			                    <?php endforeach ?>
			                </ul>
			            </li>
			        </ul>

				</td>


			</tr>
		<?php endforeach; ?>
	</table>
	<?php echo $this->BootstrapPaginator->pagination(); ?>
	<div class = "clearfix tm_1_em">&nbsp;</div>
	</div>
	<div class = "clearfix tm_1_em">&nbsp;</div>
</div>
