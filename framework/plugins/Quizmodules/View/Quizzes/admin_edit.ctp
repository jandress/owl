<?php $quizid = $this->data['Quiz']['id']; ?>

<div>


	<div class="navbar-inverse admin_padding">
		<div class="col-md-6">
			<h2><?php echo __('Editing %s', __('Quiz #'.$this->data['Quiz']['id'])); ?></h2>
			<h3><em><?php echo $this->data['Quiz']['name']; ?></em></h3>
		</div>
		<div class="col-md-6">
			<a class = 'btn btn-default btn-sm tm_2_em bm_1_em pull-right rm_1_em' target ="_blank" href = "/quizzes/view/<?php echo $this->data['Quiz']['id']; ?>"  >View Quiz &nbsp;<span class='icon-eye-open tm_2_px'>&nbsp;</span></a>
		</div>
		<div class="clear clearfix">&nbsp;</div>
	</div>





	<div class = "admin_subsub">
		<div class="admin_padding tm_1_em">
				<?php
					echo $this->Breadcrumbs->breadcrumbs($breadcrumbs);
					$navContent = '<div class="btn-group pull-right">';
					if(count($this->data['Quizslide'])){
						$navContent .= $this->BootstrapLayout->iconButton('Slide listing','icon-th-list',array('side'=>'left','href'=>'/my/quizslides/'.$quizid));
					}
					$navContent .= $this->BootstrapLayout->iconButton('Add A New Slide','icon-plus',array('side'=>'left','href'=>'/my/quizslides/add/'.$quizid));
					$navContent .= '</div>';
				?>
				<?php echo $navContent; //$this->BootstrapLayout->navbar($navContent ); ?>
				<div style = "line-height:1px; height:1px;" class = "clearfix">&nbsp;</div>

		</div>
		<div class="clear bm_5_px">&nbsp;</div>
	</div>






	<div class = "admin_padding col-md-12">
		<?php $quizid = $this->data['Quiz']['id']; ?>
		<div class = "clearfix">&nbsp;</div>
		<?php echo $this->BootstrapForm->create('Quiz', array('class' => 'form-horizontal'));?>
		<?php
			echo $this->BootstrapForm->input('name', array(
				'required' => 'required',
				'label' => "Name of this quiz",
				'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;',
				'class'=>'col-md-12'
				)
			);
			echo $this->BootstrapForm->hidden('id');
			echo $this->BootstrapForm->input('Description',array('class'=>'col-md-12 ckeditor'));
		?>
		<?php echo $this->BootstrapForm->submit(__('Next'),array('class'=>'pull-right btn tm_1_em'));?>
		<div class="clearfix clear">&nbsp;</div>
		<?php echo $this->BootstrapForm->end();?>
	</div>



</div>



<?php $this->JS->buffer("initWyswyg('#QuizDescription')"); ?>
