<div class="well wb">
		<h2>Does this look good?</h2>
</div>


<div class="well wb">
	<?php
		echo $this->Form->create('Quizslide');
	?>
	<table class="table table-striped table-bordered table-hover ">
		<tr>
			<th class="span1" style="text-align:center">Order</th>					
			<th>Name</th>
			<th style="text-align:center;" class="span1" >&nbsp;</th>			
		</tr>
	<?php 
	foreach ($data as $key => $quiz):
			$qsuid = $quiz['id'];
			
			?>
		<tr>
			<td style="text-align:center">
				<?php echo $quiz['order'] ?>
				<input type="hidden" id="id_<?php echo $qsuid; ?>_order" name="data[Quizslide][<?php echo $key ?>][order]" value="<?php echo $quiz['order'] ?>"  >								
			</td>
			<td>
				<input type="hidden" id="id_<?php echo $qsuid; ?>_name" name="data[Quizslide][<?php echo $key ?>][name]" value="<?php echo $quiz['name'] ?>" >								
				<?php echo $quiz['name'] ?>
			</td>
			<td>
				<input type="hidden" id="id_<?php echo $qsuid; ?>_id"  name="data[Quizslide][<?php echo $key ?>][id]" value="<?php echo $quiz['id'] ?>" >				
			</td>			
		</tr>
	<?php endforeach; ?>
	</table>
	<?php
			echo $this->Form->submit("Submit",array('class'=>'btn btn-success'));
			echo $this->Form->end();
			echo $this->Form->create('cancel');
				echo $this->Form->hidden('value',array('value'=>false));
				echo $this->Form->submit("cancel",array('class'=>'btn btn-danger'));
			echo $this->Form->end();			
			?>

	<div class = "clearfix">&nbsp;</div>
</div>