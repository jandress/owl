<div>
	<div class="navbar-inverse">
		<div class="col-md-6 pt_1_em">
			<h3>
				Viewing: <em><?php echo $this->data['Quiz']['name']; ?></em>
			</h3>			
		</div>
		<div class="col-md-6  tm_1_em bm_1_em">
			<a class="btn btn-sm btn-default pull-right ajax_link_back" href = "/my/quizzes/" >back etc</a>
		</div>
		<div class = "clear clearfix">&nbsp;</div>
	</div>
	<div class = "admin_padding">
		<div id="quiz_ajax_content">
			<?php echo $this->element('Quizmodules.quizzes/quiz_common_start_screen'); ?>
		</div>
		<?php $this->Js->buffer($this->element('Quizmodules.quizzes/quiz_ajax_js')); ?>
	</div>
</div>