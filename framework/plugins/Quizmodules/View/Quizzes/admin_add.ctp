<div class="navbar-inverse">
	<h1 class = "pull-left" >Quizzes</h1>
	<a class="btn btn-sm btn-default ajax_link pull-right tm_1_em" href="/my/quizzes/add/"><div class="glyphicon glyphicon-plus-sign">&nbsp;</div>&nbsp;New Quiz</a>
	<div class = "clearfix">&nbsp;</div>
</div>
<div  class="admin_padding">
	<div class="admin_padding">
		<?php echo $this->BootstrapForm->create('Quiz', array('class' => 'form-horizontal'));?>
				<?php
				echo $this->BootstrapForm->input('name', array(
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;'
					,'class'=>'col-md-10'	
					)
				);
				echo '<div class = "clearfix">&nbsp;</div>';
				echo $this->BootstrapForm->input('Description',array('class'=>'col-md-12'));
				echo '<div class = "clearfix tm_1_em">&nbsp;</div>';
				?>

				<a id = 'submit' href = "<?php echo $here; ?>" class = 'btn btn-default btn-block btn-large rm_2_em tm_1_em  ajax_submit' >Submit</a>	
				<?php echo $this->BootstrapForm->end();?>
	</div>		
</div>