<div>
	<div class="navbar-inverse">
		<h1 class = "pull-left" >Quizzes</h1>
		<div class = "admin_padding">
			<a class="btn btn-sm btn-default ajax_link pull-right tm_1_em" href="/my/quizzes/add/"><div class="glyphicon glyphicon-plus-sign">&nbsp;</div>&nbsp;New Quiz</a>
		</div>
		<div class = "clearfix">&nbsp;</div>
		<p>
			<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
		</p>
	</div>
	<div class="admin_padding">

	<?php echo $this->BootstrapPaginator->pagination(); ?>


	<div class = "pull-right">
		<?php echo $this->element('Quizmodules.quizzes/quiztype_list_btn'); ?>
	</div>



	<div class = "clearfix tm_1_em">&nbsp;</div>
		<table class="table table-striped table-bordered table-hover tm_2_em">
		<tr>
			<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
			<th><?php echo $this->BootstrapPaginator->sort('name');?></th>
			<th><?php echo $this->BootstrapPaginator->sort('Description');?></th>
			<th class = 'span1 centered' >Slides<?php echo '' //$this->BootstrapPaginator->sort('Slides');?></th>

		</tr>
		<?php foreach ($quizzes as $quiz):

			?>
			<tr>

				<td>
					<?php echo htmlentities($quiz['Quiz']['id']); ?>&nbsp;
				</td>

				<td>
					<ul style = "list-style:none; padding:0" >
			            <li class="dropdown">
			              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<?php echo htmlentities($quiz['Quiz']['name']); ?>&nbsp;
			              </a>
			                <ul class="dropdown-menu " role="menu" style = "text-decoration:none;">

								<li>
									<a class = "" href = '/my/quizzes/edit/<?php echo $quiz['Quiz']['id'] ?>'>Edit</a>
			                    </li>
			                    <li>
									<a class = "" target = "_blank" href = '/quizzes/view/<?php echo $quiz['Quiz']['id'] ?>'>View</a>
			                    </li>
								<li>
									<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $quiz['Quiz']['id']), null, __('Are you sure you want to delete quiz # %s and all related slides?', $quiz['Quiz']['id'])); ?>
			                    </li>
			                	<li role="presentation" class="divider"></li>

								<li>
									<a target = "_blank" href = '/standalone/quizzes/view/<?php echo $quiz['Quiz']['id'] ?>'>View as Standalone</a>
			                    </li>
								<li>
									<a class="lightbox_link" href = '/standalone/quizzes/view/<?php echo $quiz['Quiz']['id'] ?>'>View in Lightbox</a>
			                    </li>
			                </ul>
			            </li>
			          </ul>
				</td>

				<td>
					<?php echo strip_tags(String::truncate( ($quiz['Quiz']['Description']),200,array('exact'=>false)));?>
				</td>

				<td style = "text-align:center;">

					<ul style = "list-style:none; padding:0" >
			            <li class="dropdown">
			              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			              	<?php  echo(count($quiz['Quizslide'])); ?>
			              </a>
			                <ul class="dropdown-menu  dropdown-menu-right" role="menu" style = "text-decoration:none;">
			           			<li >
									<a class = "" href = '/my/quizslides/<?php echo $quiz['Quiz']['id'] ?>'>Edit Slides</a>
			                    </li>
								<li >
									<a class = "ajax_link" href = '/my/quizslides/add/<?php echo $quiz['Quiz']['id'] ?>'>New Slide</a>
			                    </li>
			                	<li role="presentation" class="divider"></li>
			                    <?php foreach ($quiz['Quizslide'] as $key => $value): ?>
			                    <li>
			                    	<a target = "_blank" href="/my/quizslides/view/<?php echo $value['id']; ?>" class=""><?php echo $value['class'].': '.$value['name']; ?></a>

			                    </li>
			                    <?php endforeach ?>
			                </ul>
			            </li>
			        </ul>

				</td>


			</tr>
		<?php endforeach; ?>
	</table>
	<?php echo $this->BootstrapPaginator->pagination(); ?>
	<div class = "clearfix tm_1_em">&nbsp;</div>
	</div>
	<div class = "clearfix tm_1_em">&nbsp;</div>
</div>
