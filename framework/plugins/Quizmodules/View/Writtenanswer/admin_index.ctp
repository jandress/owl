<div class="row-fluid">
	<div class="white_well">
		<h2>"Written Answer" Questions</h2>
		<div class="navbar">
			<div class="navbar-inner">
				<div class='span9' style = 'padding-top:8px'>
					<p>
						<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
					</p>
				</div>
				<div class="nav span3 pull-right">
					<a href = '/my/written/add/' class = 'btn pull-right'><div class="icon-plus">&nbsp;</div>&nbsp;New Question</a>
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="white_well">		
		<table class="table table-striped table-bordered table-hover">
			<tr>
				<th class='span1'><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th class='span3'><?php echo $this->BootstrapPaginator->sort('directive');?></th>
				<th class='span3'><?php echo $this->BootstrapPaginator->sort('question');?></th>
				<th class="actions span3" style ='text-align:center'><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($writtenanswers as $writtenanswers): ?>
			<tr>
				<td><?php echo h($writtenanswers['Writtenanswer']['id']); ?>&nbsp;</td>
				<td><?php echo h($writtenanswers['Writtenanswer']['directive']); ?>&nbsp;</td>
				<td><?php echo h($writtenanswers['Writtenanswer']['question']); ?>&nbsp;</td>
				<td style="text-align:center" class="actions">
					<a target = '_blank' href="/written/view/<?php echo $writtenanswers['Writtenanswer']['id']; ?>">View</a><span class=""> | </span>
					<a href="/my/written/edit/<?php echo $writtenanswers['Writtenanswer']['id']; ?>">Edit</a><span class=""> | </span>
					<form method="post" style="display:none;" id="post_512cc309a420c" name="post_512cc309a420c" action="/Quizmodules/Writtenanswers/delete/1"><input type="hidden" value="POST" name="_method"></form><a onclick="if (confirm('Are you sure you want to delete # 1?')) { document.post_512cc309a420c.submit(); } event.returnValue = false; return false;" href="#">Delete</a>				
				</td>			
			</tr>
		<?php endforeach; ?>
		</table>
		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
</div>