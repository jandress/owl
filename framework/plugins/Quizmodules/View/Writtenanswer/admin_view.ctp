<div class="row-fluid">
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('')), array('controller' => '', 'action' => 'add'));?> </li>
		</ul>
	</div>
	<div class="span9">
		<h2><?php  echo __('Fillintheblank');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($fillintheblank['Fillintheblank']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Directive'); ?></dt>
			<dd>
				<?php echo h($fillintheblank['Fillintheblank']['directive']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Question'); ?></dt>
			<dd>
				<?php echo h($fillintheblank['Fillintheblank']['question']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Fib Answer'); ?></dt>
			<dd>
				<?php echo $this->Html->link($fillintheblank['FibAnswer']['name'], array('controller' => 'fib_answers', 'action' => 'view', $fillintheblank['FibAnswer']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Correct Answer Response'); ?></dt>
			<dd>
				<?php echo h($fillintheblank['Fillintheblank']['correct_answer_response']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Incorrect Answer Response'); ?></dt>
			<dd>
				<?php echo h($fillintheblank['Fillintheblank']['incorrect_answer_response']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Fillintheblank')), array('action' => 'edit', $fillintheblank['Fillintheblank']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Fillintheblank')), array('action' => 'delete', $fillintheblank['Fillintheblank']['id']), null, __('Are you sure you want to delete # %s?', $fillintheblank['Fillintheblank']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__( __('Fillintheblanks')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Fillintheblank')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__( __('Fib Answers')), array('controller' => 'fib_answers', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Fib Answer')), array('controller' => 'fib_answers', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Fib Answers')); ?></h3>
	<?php if (!empty($fillintheblank['FibAnswer'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Name'); ?></th>
				<th><?php echo __('Fillintheblank Id'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($fillintheblank['FibAnswer'] as $fibAnswer): ?>
			<tr>
				<td><?php echo $fibAnswer['id'];?></td>
				<td><?php echo $fibAnswer['name'];?></td>
				<td><?php echo $fibAnswer['fillintheblank_id'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'fib_answers', 'action' => 'view', $fibAnswer['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'fib_answers', 'action' => 'edit', $fibAnswer['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'fib_answers', 'action' => 'delete', $fibAnswer['id']), null, __('Are you sure you want to delete # %s?', $fibAnswer['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
</div>
