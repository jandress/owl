<div class="navbar-inverse ">
	<h2>
		Viewing Sentence Combining / Sorting activity: 
	</h2>	
	<h3>
		 <?php echo $this->data['Sorting']['name']; ?>
	</h3>		
	<p>
		<?php echo($this->data['Sorting']['directive']); ?>		
	</p>
</div>
<div class="admin_subsub"> 
	<div class="admin_padding tm_10_px">
		<div class="nav pull-right btn-group tm_4_px">
			<a id="sortable_nav_edit_btn" href="/my/sortings/edit/<?php echo $this->data['Sorting']['id']; ?>" class="btn btn-default btn-sm">
				<i class="icon-edit">&nbsp;</i>
				Edit
			</a>
		</div>
			<div class = "clear clearfix bm_1_em">&nbsp;</div>					
	</div>		
	<div class = "clear clearfix">&nbsp;</div>
</div>
<?php echo $this->element('Sorting/view_elements'); ?>