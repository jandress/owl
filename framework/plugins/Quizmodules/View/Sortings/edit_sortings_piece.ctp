<?php echo $this->BootstrapForm->create('Sortingpiece');?>	
<div>
	<div class="navbar-inverse">
		<?php echo $this->BootstrapForm->hidden('Sorting.id'); ?>
		<h3>New Sorting Piece</h3>
	</div>
	<div class = "admin_subsub">
	</div>
	<div class="admin_padding tm_1_em">
		<?php echo $this->BootstrapForm->hidden('Sortingpiece.sorting_id',array('value'=>$this->data['Sorting']['id'])); ?>
		<?php echo $this->BootstrapForm->input('Sortingpiece.name',array('class'=>'col-md-12 ','label'=>'')); ?>
		<?php echo $this->BootstrapForm->submit("Submit",array('class'=>'btn btn-default btn-success btn-large pull-right bm_1_px tm_1_em'));?>									


	</div>	
</div>	
<?php echo $this->BootstrapForm->end();?>