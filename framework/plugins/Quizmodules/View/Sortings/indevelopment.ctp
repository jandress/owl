
<div class="navbar-inverse ">
	<h2>
		Sentence Combining / Sorting Order<br/>IN DEVELOPMENT!!!!
	</h2>	

	<a href = "/my/sortings/view/<?php echo $this->data['Sorting']['id']; ?>" class="btn btn-default ajax_link pull-right">
		<i class="icon-eye-open">&nbsp;</i>
		View
	</a>
	<h2>
		<?php echo $this->data['Sorting']['name']; ?>	
	</h2>	
	<p>
		Order the words and phrases into the desired correct order then hit Next to save it. 
	</p>
</div>
<div class="admin_subsub"> 
	<div class="admin_padding tm_10_px">
		<div class="pull-left tm_4_px">
			<?php  echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
		</div>
		<span class="pull-right tm_4_px lm_8_px">
			<ul style = "list-style:none; padding:0; margin-bottom:0px;" >
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle btn btn-default btn-sm" data-toggle="dropdown">
					<i class="icon-plus">&nbsp;</i>
					New
				  </a>
				    <ul class="dropdown-menu dropdown-menu-right" role="menu" style = "text-decoration:none;">
						<li>	
							<a class = "" href = "/my/sortingpieces/new/<?php echo $this->data['Sorting']['id']; ?>" >
								New Text Element
							</a>
				        </li>
				        <li>	
							<a class = ""  href = "/my/sortingpieces/admin_image/<?php echo $this->data['Sorting']['id']; ?>">
								New Image 
							</a>
				        </li>			                    			                    
				    </ul>
				</li>
			</ul>					
		</span>
		<div class="nav pull-right btn-group tm_4_px">
			<a id="sortable_nav_edit_btn" onclick="editSortingPiece()" class="btn-sm btn btn-default disabled">
				<i class="icon-edit">&nbsp;</i>
				Edit Item
			</a>
			<a id="sortable_nav_delete_btn" onclick="askDelete()" class="btn-sm btn btn-default btn-danger disabled">
				<i class="icon-trash icon-white">&nbsp;</i>
				delete
			</a>						
		</div>
	</div>		
	<div class = "bm_1_em clear clearfix">&nbsp;</div>
</div>

<div class="admin_padding tm_1_em">		
	<?php
	 echo $this->BootstrapForm->create('Sorting'); ?>
	<?php echo $this->BootstrapForm->input('Sorting.id',array('class'=>'span10')); ?>
	<?php echo $this->BootstrapForm->input('Sorting.answer',array('class'=>'span10')); ?>
	<div class = "clear clearfix bm_2_em">&nbsp;</div>
	<div class = "clearfix">&nbsp;</div>
	<div id="canvas-container" style = ''><canvas id="canvas" style = '' >Your browser does not support HTML5.</canvas></div>
	<br/>
	
		<div class = "clear clearfix bm_2_em">&nbsp;</div>
	Ok, right now you have the pieces coming out as json.   
	
	Next you need to write the function that draws this content as json. Steps:<br/><br/>
	
			<strike>	1. get the database content into a format that your javascript can read.   either json or a deliminated string.  probably start with deliminated strings.   probably easier.</strike>
			<br/>
			<strike>2. break up the deliminated string into a js array, create canvas draw function.<br/></strike>
			<strike>3. elaborate on canvas the draw function so that it mimics the flash paragraph layout engine. using that logic.			</strike><br/>
			<strike>4. add styles gray and white, red and white<br/>			</strike>
			
			<strike>5. make it interactive so that when you click on words it selects the piece.  This will be difficult to do in canvas.  you will probably have to create json objects that contain coordinates for each piece.  How do you store the relationship of the words to pieces for javascript?  How will they need to be accessed? how will they need to be manipulated?<br/></strike>
	
						<strike>6. Create an object to visualize mouse interactions. Maybe break these up into 2 different objects.   1 to maniuplate the canvas, 1 to handle and visualise mouse interactions. And 1 to store the data.  How to you prevent this from becoming spaghetti?<br/>			</strike>
			
				<strike>7.  reordering	</strike>
			 <br/>
			8. setting / saving / showing  answer<br/>
				<strike>9. integration into edit screen<br/></strike>
				<strike>10. integration into view screen.<br/>	</strike>
			<br/>
			 
			
				
	
	

	<?php $sortingpiece = $this->data['Sortingpiece']; ?>
	<?php echo $this->BootstrapForm->submit('submit',array('class'=>'btn pull-right tm_1_em')); ?>
	<?php echo $this->BootstrapForm->end(); ?>
	<div class="clear">&nbsp;</div>	   
</div>

<div class = "clear clearfix">&nbsp;</div>

<?php echo $this->element('Quizmodules.Sorting/new_sorting_js'); ?>													
<script type="text/javascript">
	var g= dragndrop()
	g.init(<?php echo json_encode($sortingpiece); ?>)
</script>