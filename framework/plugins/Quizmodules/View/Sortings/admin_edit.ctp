<div class="row-fluid">
		<div class="navbar-inverse ">
			<h2>
				Editing Sentence Combining / Sorting Slide: 
			</h2>
			<h3>
				<?php echo $this->data['Sorting']['name']; ?>
			</h3>
			<p>
				Hit "Next" once you are done editing. 
			</p>
		</div>
		<div class="admin_subsub"> 
			<div class="admin_padding tm_10_px bm_12_px">
				<?php echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
				<div class="nav pull-right btn-group tm_4_px">
					<a id="sortable_nav_edit_btn" href="/my/sortings/view/<?php echo $this->data['Sorting']['id']; ?>" class="btn btn-default ajax_link btn-sm">
						<i class="icon-eye-open">&nbsp;</i>
						View
					</a>
				</div>
			</div>	
			<div class = "clear clearfix">&nbsp;</div>
		</div>
		<div class="admin_padding tm_1_em">		
		<?php  echo $this->element('Sorting/edit_fields');?>	
		</div>
</div>
