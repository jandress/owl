<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo __($title_for_layout); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<?php if ($isIE): ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php endif ?>
    <!-- CSS -->
	<?php
		if(!isset($concat_assets)){
			$concat_assets = true;
		}
		if(empty($cssBuild)){
			$cssBuild = 'common';
		}
	 	echo($this->AssetCompress->css($cssBuild, array('raw' => $concat_assets))."\n");
 	?>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
	<link rel="shortcut icon" href="/ico/favicon.png">
	<script type="text/javascript" src = '/js/jquery.js'></script>	
	<?php echo $this->element('/fluid_theme/background_css'); ?>	
  </head>
  <body>
    <div class = "container">
		<div class="tm_1_em row-fluid">
			<div class="centered_text">

				<div class="alert alert-success centered_text">
					Saved Successfully
				</div>
				<div class="pl_2_em pr_2_em">
					<a class = "btn btn-block btn-success" target = "_parent" href ="/my/quizslides/edit/<?php echo $id ?>" >Close</a>
				</div>
			</div>

    	</div>
    </div>
  </body>
</html>