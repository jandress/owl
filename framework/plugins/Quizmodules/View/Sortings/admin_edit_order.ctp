
<div class="navbar-inverse ">
	<h2>
		Sentence Combining / Sorting Order
	</h2>	
	<a target = "_blank" href = "/my/sortings/view/<?php echo $this->data['Sorting']['id']; ?>" class="btn btn-default pull-right rm_1_em">
		<i class="icon-eye-open">&nbsp;</i>
		View
	</a>
	<h2>
		<?php echo $this->data['Sorting']['name']; ?>	
	</h2>	
	<p>
		Order the words and phrases into the desired correct order then hit Next to save it. 
	</p>
</div>
<div class="admin_subsub"> 
	<div class="admin_padding tm_10_px">
		<div class="pull-left tm_4_px">
			<?php  echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
		</div>
		<span class="pull-right tm_4_px lm_8_px">
			<ul style = "list-style:none; padding:0; margin-bottom:0px;" >
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle btn btn-default btn-sm" data-toggle="dropdown">
					<i class="icon-plus">&nbsp;</i>
					New	
				  </a>
				    <ul class="dropdown-menu dropdown-menu-right" role="menu" style = "text-decoration:none;">
						<li>	
							<a class = "" href = "/my/sortingpieces/new/<?php echo $this->data['Sorting']['id']; ?>" >
								New Text Element
							</a>
				        </li>
				        <li>	
							<a class = ""  href = "/my/sortingpieces/admin_image/<?php echo $this->data['Sorting']['id']; ?>">
								New Image 
							</a>
				        </li>			                    			                    
				    </ul>
				</li>
			</ul>					
		</span>
		<div class="nav pull-right btn-group tm_4_px">
			<a id="sortable_nav_edit_btn"  class="btn-sm btn btn-default disabled">
				<i class="icon-edit">&nbsp;</i>
				Edit Item
			</a>
			<a id="sortable_nav_delete_btn" onclick="askDelete()" class="btn-sm btn btn-default btn-danger disabled">
				<i class="icon-trash icon-white">&nbsp;</i>
				delete
			</a>						
		</div>
	</div>		
	<div class = "bm_1_em clear clearfix">&nbsp;</div>
</div>

<div class="admin_padding tm_1_em">		
	<?php
	 echo $this->BootstrapForm->create('Sorting'); ?>
	<?php echo $this->BootstrapForm->input('Sorting.id',array('class'=>'span10')); ?>
	<?php echo $this->BootstrapForm->input('Sorting.answer',array('class'=>'span10')); ?>
	<div class = "clear clearfix bm_2_em">&nbsp;</div>
	<div class = "clearfix">&nbsp;</div>
	<div id="canvas-container" style = 'height:400px;'><canvas id="canvas" style = '' >Your browser does not support HTML5.</canvas></div>	
	<br/>
	<div class = "clear clearfix bm_2_em">&nbsp;</div>
	<?php $sortingpiece = $this->data['Sortingpiece']; ?>
	<?php echo $this->BootstrapForm->submit('submit',array('class'=>'btn pull-right tm_1_em')); ?>
	<?php echo $this->BootstrapForm->end(); ?>
	<div class="clear">&nbsp;</div>	   
</div>

<script src="/Quizmodules/js/sortings/DraggedCharacter.js"></script>
<script src="/Quizmodules/js/sortings/ImageBlock.js"></script>
<script src="/Quizmodules/js/sortings/Textblock.js"></script>
<script src="/Quizmodules/js/sortings/InteractiveParagraph.js"></script>
<script src="/Quizmodules/js/sortings/sortings.js"></script>
<?php if (count($sortingpiece) > 0): ?>	
	<script type="text/javascript">
		var g = sorting();
		g.init( <?php echo json_encode($sortingpiece); ?>, $("#SortingAnswer").val().split(",") );
		//	g.init(<?php echo json_encode($sortingpiece); ?>, $("#SortingAnswer").val() )
		var selected;
		//------------------
		g.onSelect = function(val){
			if(val){
				selected = val.id
				_enableEditUi(val.id)						
				if(val.type.toLowerCase()=="image"){
					console.log('yes')
					$("#sortable_nav_edit_btn").addClass("disabled")
				}			
			}else{
				_disableEditUi()
			}
		}
		function askDelete(){
			if(confirm("Are you sure you want to delete this?")){
				$.ajax({
					url: '/my/sortingpieces/delete/'+selected,
				  	type: "POST",
					success: function(data){
						_remove()			

					}
				});

			}
		}
		function _disableEditUi(){
			$("#sortable_nav_edit_btn").addClass("disabled")
			$("#sortable_nav_delete_btn").addClass("disabled")			
		}

		function _enableEditUi(id){
			$("#sortable_nav_edit_btn").removeClass("disabled")
			$("#sortable_nav_delete_btn").removeClass("disabled")							
			$("#sortable_nav_edit_btn").attr("href",'/my/sortingpieces/edit/'+id)			
		}
		g.onRelocate = function(data){
			var str = '';
			for (var i=0; i < data.length; i++) {
				str+=data[i].id
				if(i < data.length-1){
					str += ","
				}
			};
			$("#SortingAnswer").val(str);
		}
	</script>
<?php endif ?>
<div class = "clear clearfix">&nbsp;</div>