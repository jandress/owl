<div class="navbar-inverse admin-padd">
	<div class = "admin_padding">
		<h2><?php echo __( __('Sorting Exercises'));?></h2>
			<div class = "pull-right">
				<?php echo $this->element('Quizmodules.quizzes/quiztype_list_btn'); ?>               		
			</div>
			<div class='span9' style = 'padding-top:8px'>
				<p>
					<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
				</p>
			</div>
	</div>
	<div class = "clearfix">&nbsp;</div>
</div>



<div class="admin_padding">
	<div class="row-fluid">
			<table class="table table-striped table-bordered table-hover">
				<tr>
					<th class="span1"><?php echo $this->BootstrapPaginator->sort('id');?></th>
					<th class="span4"><?php echo $this->BootstrapPaginator->sort('directive');?></th>
					<th class="span4"><?php echo $this->BootstrapPaginator->sort('name');?></th>
					<th class="actions span2"><?php echo __('Quizslide');?></th>
				</tr>

			<?php foreach ($sortings as $sorting): ?>
				<tr>
					<td><?php echo h($sorting['Sorting']['id']); ?>&nbsp;</td>
					<td><?php echo h($sorting['Sorting']['directive']); ?>&nbsp;</td>
					<td><?php echo h($sorting['Sorting']['name']); ?>&nbsp;</td>
					<td>
						<?php echo $this->element('Quizmodules.Quizslides/edit_view_btn',array('__d'=>$sorting, '_quiztype'=>'Sorting')) ?>
					</td>			
				</tr>
			<?php endforeach; ?>


			</table>
			<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
</div>

