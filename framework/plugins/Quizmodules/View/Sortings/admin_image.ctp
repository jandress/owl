<div class="navbar-inverse ">
	<h2>
		New Image for Re-ordering plugin.
	</h2>	
	<h2>
		<?php echo $this->data['Sorting']['name']; ?>	
	</h2>	
</div>
<div class="admin_subsub"> 
	<div class="admin_padding tm_10_px">
	</div>		
	<div class = "clear clearfix">&nbsp;</div>
</div>

<div class="admin_padding tm_1_em">		





<?php 
	echo $this->Form->create();
	echo $this->Form->hidden('Sorting.id');
	echo $this->Form->hidden('Sortingpiece.name');
	echo $this->Form->hidden('Sortingpiece.type',array('value'=>'image'));
 ?>

<table id="projects" class="table table-striped table-bordered table-hover"  >
	<thead>
		<tr class = 'gb_28'>
			<th class = "col-md-2">Name</th>
			<th class="actions" style = 'text-align:center'>&nbsp;</th>
		</tr>
	</thead>			
	<tbody>
	<?php
	if(!empty($file_listing)):
	$i=0;
	foreach ($file_listing as $key => $value): ?>
			<tr id="<?php echo $value['item'] ?>" <?php echo(($i++ % 2 == 0) ? ' class="altrow"' : '')?>>
				<?php 
					// not crazy about the hard coding taking place here.			
					$type =$value['type'];	
					$icon = $type;
					switch($type){
						case 'dir':
							$icon = 'glyphicon glyphicon-folder-close';
						break;
						case 'm4v':
							$icon = 'glyphicon glyphicon-facetime-video';
						break;
						case 'png':
						case 'jpg':
						case 'jpeg':
							$icon = 'glyphicon glyphicon-picture';
						break;
					}				
			?>
			<td style = "text-align:center;">
				<?php if ($type != "dir"): ?>
					<img src="/media/sorting/<?php echo $value['item'] ?>">
				<?php endif ?>
			</td>
			<td class="actions" style = 'text-align:center'>
				<?php if($type != "dir"){
					echo('<a href = "'.$value['item'].'" target = "_blank">'.$value['item'].'</a>');							
				} ?>
			</td>
		</tr>				
	<?php 
	endforeach;
	else: 
	?>	
		<tr>
			<td colspan=4>No files were found.</td>
		</tr>           
		<?php endif ?>
	</tbody>
</table>

<?php 
	//echo $this->Form->submit();
	echo $this->Form->end();
?>

<a href = '<?php echo $here; ?>' class='btn btn-default ajax_submit tm_1_em pull-right'>Next!</a>						 

</div>	


<script type="text/javascript">
	$("tr").bind('click', function(e){
		$("#SortingpieceName").val($(this).attr('id'));
    	$(this).addClass('highlight').siblings().removeClass('highlight');
	});
</script>