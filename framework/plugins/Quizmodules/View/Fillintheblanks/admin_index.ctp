<div class="navbar-inverse admin-padd">
	<div class = "admin_padding">
		<h2>"Fill In the Blank" Questions</h2>
			<div class = "pull-right">
				<?php echo $this->element('Quizmodules.quizzes/quiztype_list_btn'); ?>               		
			</div>
			<div class='span9' style = 'padding-top:8px'>
				<p>
					<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
				</p>
			</div>
	</div>
	<div class = "clearfix">&nbsp;</div>
</div>






<div class="admin_padding">
	<div class="white_well">
		<table class="table table-striped table-bordered table-hover">
			<tr>
				<th class='span1'><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th class='span3'><?php echo $this->BootstrapPaginator->sort('name');?></th>
				<th class='span3'><?php echo $this->BootstrapPaginator->sort('question');?></th>
				<th class="actions span3" style ='text-align:center'><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($fillintheblanks as $fillintheblank): ?>
			<tr>
				<td><?php echo h($fillintheblank['Fillintheblank']['id']); ?>&nbsp;</td>
				<td><?php echo h($fillintheblank['Fillintheblank']['name']); ?>&nbsp;</td>
				<td><?php echo h($fillintheblank['Fillintheblank']['question_beginning']).'____'.h($fillintheblank['Fillintheblank']['question_end']); ?>&nbsp;</td>
				<td>
							<?php echo $this->element('Quizmodules.Quizslides/edit_view_btn',array('__d'=>$fillintheblank, '_quiztype'=>'Wordselect')) ?>
				</td>			
			</tr>
		<?php endforeach; ?>
		</table>
		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
</div>