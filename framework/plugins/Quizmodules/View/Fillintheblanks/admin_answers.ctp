<div class = "navbar-inverse">
	<h2>
		Answers for Fill in the blank question:
	</h2>
	<h3>
		<?php echo $this->data['Fillintheblank']['question_beginning'].'____________'.$this->data['Fillintheblank']['question_end']; ?>
	</h3>
</div>
<?php 
	//debug($this->data);
	echo '';
 ?>
<div class="admin_subsub"> 
	<div class="admin_padding tm_10_px bm_12_px">
		<?php  echo $this->Breadcrumbs->breadcrumbs($breadcrumbs); ?>
	</div>
</div>
<div class="admin_padding">
		<?php echo $this->BootstrapForm->create('Fillintheblank', array('class' => 'form-horizontal')); ?>
		<?php echo $this->BootstrapForm->input('Fillintheblank.id'); ?>
		<?php 
			echo $this->element("Quizmodules.Fillintheblank/fib_answer_edit");  
		?>

		<?php echo $this->BootstrapForm->submit('Submit',array('class'=>'btn btn-default tm_1_em pull-right'));?>
		<?php echo $this->BootstrapForm->end();?>
</div>
