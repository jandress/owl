<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
        <title><?php echo $title_for_layout; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">    
    <link href="/css/dashboard.css" rel="stylesheet">
    <link href="/css/layout_aids.css" rel="stylesheet">
    <link href="/css/icons.css" rel="stylesheet">        


    <link href="/Quizmodules/css/pnotify.custom.min.css" rel="stylesheet">

    <script src="/js/jquery.js"></script>    
    <script src="/js/bootstrap.min.js"></script>

    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
            .admin_padding{
                padding-left: 15px;
                padding-right: 15px;                                
            }
           .navbar-inverse {
                color:#ffffff;
            }
            .navbar-inverse h1, .navbar-inverse h2, .navbar-inverse h3 {
                margin-top: 0px;
            }
            .navbar-inverse_left{
                float:left;
            }

            .navbar-inverse{
                padding: 15px;
                background-color: #333333;
                box-shadow: 0 5px 8px #AAAAAA;
            }
            .admin_content .pagination{
                margin: 0 !important;
                margin-top: 10px !important;
                float:right;
            }
            .admin_content table{
                margin: 0 !important;
                margin-top: 10px !important;
            }
            .clearfix{
                line-height: 1px !important;
                height:1px !important;
                max-height:1px !important;
                float:none;
                clear: both;
            }

    </style>
  </head>


  <body>

   
    <div class="container-fluid">
      <div class="row" >
        <div class="content" id = "content">
          <?php echo $content_for_layout; ?>
        </div>
        <div class = "clearfix bm_1_em">&nbsp;</div>        
        <!-- end main -->
      </div>
    </div>
    
    

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
    <script src="/PagekwikTools/js/ajax_slider.js"></script>    
    <script src="/Navigations/js/sitemap.js"></script>    

    <script src="/Quizmodules/js/pnotify.custom.min.js"></script>    
    <script src="/Quizmodules/js/quiz_notification.js"></script>    
    <script type="text/javascript">
            var main_content_ajax_helper = ajax_slider();
            main_content_ajax_helper.init("#content",'.ajax_link');            


    </script>
  </body>




</html>
