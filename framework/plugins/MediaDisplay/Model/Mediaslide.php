<?php
App::uses('AppModel', 'Model');
/**
 * Mediaslide Model
 *
 * @property MediaslidePoint $MediaslidePoint
 */
class Mediaslide extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	// public $hasMany = array(
	// 	'MediaslidePoint' => array(
	// 		'className' => 'MediaslidePoint',
	// 		'foreignKey' => 'mediaslide_id',
	// 		'dependent' => false,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'exclusive' => '',
	// 		'finderQuery' => '',
	// 		'counterQuery' => ''
	// 	)
	// );
	
	public $belongsTo = array(
		'Mediashow' => array(
			'className' => 'Mediashow',
			'foreignKey' => 'mediashow_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
