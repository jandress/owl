<?php
App::uses('AppModel', 'Model');
/**
 * MediaslidePoint Model
 *
 * @property Mediaslide $Mediaslide
 */
class MediaslidePoint extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Mediaslide' => array(
			'className' => 'Mediaslide',
			'foreignKey' => 'mediaslide_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
