<?php
App::uses('AppController', 'Controller');
/**
 * Mediashows Controller
 *
 * @property Mediashow $Mediashow
 */
class MediashowsController extends AppController {

/**
 *  Layout
 *
 * @var string
 */

	public $layout = 'ajax';

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		//debug($this->Fileutil->getDirectories('./media/slideshows/')); exit;
		$this->set('mediashows', $this->Fileutil->getDirectories('./media/slideshows/'));
	}


	public function view($id = null,$format='interactive') {
		$this->set('dir','/media/slideshows/'.$id);
		$f = $this->Fileutil->getFileListing('./media/slideshows/'.$id);
		$this->set('files', $f);
		$this->set('format',$format );
		//$this->setDashboardLayout('_one_col');
	}

}
