<?php
App::uses('AppController', 'Controller');
/**
 * MediaslidePoints Controller
 *
 * @property MediaslidePoint $MediaslidePoint
 */
class MediaslidePointsController extends AppController {

/**
 *  Layout
 *
 * @var string
 */
/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->MediaslidePoint->recursive = 0;
		$this->set('mediaslidePoints', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->MediaslidePoint->id = $id;
		if (!$this->MediaslidePoint->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mediaslide point')));
		}
		$this->set('mediaslidePoint', $this->MediaslidePoint->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->MediaslidePoint->create();
			if ($this->MediaslidePoint->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('mediaslide point')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('mediaslide point')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$mediaslides = $this->MediaslidePoint->Mediaslide->find('list');
		$this->set(compact('mediaslides'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->MediaslidePoint->id = $id;
		if (!$this->MediaslidePoint->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mediaslide point')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->MediaslidePoint->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('mediaslide point')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('mediaslide point')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->MediaslidePoint->read(null, $id);
		}
		$mediaslides = $this->MediaslidePoint->Mediaslide->find('list');
		$this->set(compact('mediaslides'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->MediaslidePoint->id = $id;
		if (!$this->MediaslidePoint->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mediaslide point')));
		}
		if ($this->MediaslidePoint->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('mediaslide point')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('mediaslide point')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}/**
 * my_index method
 *
 * @return void
 */
	public function my_index() {
		$this->MediaslidePoint->recursive = 0;
		$this->set('mediaslidePoints', $this->paginate());
	}

/**
 * my_view method
 *
 * @param string $id
 * @return void
 */
	public function my_view($id = null) {
		$this->MediaslidePoint->id = $id;
		if (!$this->MediaslidePoint->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mediaslide point')));
		}
		$this->set('mediaslidePoint', $this->MediaslidePoint->read(null, $id));
	}

/**
 * my_add method
 *
 * @return void
 */
	public function my_add() {
		if ($this->request->is('post')) {
			$this->MediaslidePoint->create();
			if ($this->MediaslidePoint->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('mediaslide point')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('mediaslide point')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$mediaslides = $this->MediaslidePoint->Mediaslide->find('list');
		$this->set(compact('mediaslides'));
	}

/**
 * my_edit method
 *
 * @param string $id
 * @return void
 */
	public function my_edit($id = null) {
		$this->MediaslidePoint->id = $id;
		if (!$this->MediaslidePoint->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mediaslide point')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->MediaslidePoint->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('mediaslide point')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('mediaslide point')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->MediaslidePoint->read(null, $id);
		}
		$mediaslides = $this->MediaslidePoint->Mediaslide->find('list');
		$this->set(compact('mediaslides'));
	}

/**
 * my_delete method
 *
 * @param string $id
 * @return void
 */
	public function my_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->MediaslidePoint->id = $id;
		if (!$this->MediaslidePoint->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mediaslide point')));
		}
		if ($this->MediaslidePoint->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('mediaslide point')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('mediaslide point')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
}
