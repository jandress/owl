<?php
App::uses('AppController', 'Controller');
/**
 * Mediaslides Controller
 *
 * @property Mediaslide $Mediaslide
 */
class MediaslidesController extends AppController {

/**
 *  Layout
 *
 * @var string
 */

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Mediaslide->recursive = 0;
		$this->set('mediaslides', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Mediaslide->id = $id;
		if (!$this->Mediaslide->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mediaslide')));
		}
		$this->set('mediaslide', $this->Mediaslide->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Mediaslide->create();
			if ($this->Mediaslide->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('mediaslide')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('mediaslide')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Mediaslide->id = $id;
		if (!$this->Mediaslide->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mediaslide')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Mediaslide->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('mediaslide')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('mediaslide')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Mediaslide->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Mediaslide->id = $id;
		if (!$this->Mediaslide->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mediaslide')));
		}
		if ($this->Mediaslide->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('mediaslide')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('mediaslide')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}/**
 * my_index method
 *
 * @return void
 */
	public function my_index() {
		$this->Mediaslide->recursive = 0;
		$this->set('mediaslides', $this->paginate());
	}

/**
 * my_view method
 *
 * @param string $id
 * @return void
 */
	public function my_view($id = null) {
		$this->Mediaslide->id = $id;
		if (!$this->Mediaslide->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mediaslide')));
		}
		$this->set('mediaslide', $this->Mediaslide->read(null, $id));
	}

/**
 * my_add method
 *
 * @return void
 */
	public function my_add() {
		if ($this->request->is('post')) {
			$this->Mediaslide->create();
			if ($this->Mediaslide->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('mediaslide')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('mediaslide')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
	}

/**
 * my_edit method
 *
 * @param string $id
 * @return void
 */
	public function my_edit($id = null) {
		$this->Mediaslide->id = $id;
		if (!$this->Mediaslide->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mediaslide')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Mediaslide->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('mediaslide')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('mediaslide')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Mediaslide->read(null, $id);
		}
	}

/**
 * my_delete method
 *
 * @param string $id
 * @return void
 */
	public function my_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Mediaslide->id = $id;
		if (!$this->Mediaslide->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mediaslide')));
		}
		if ($this->Mediaslide->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('mediaslide')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('mediaslide')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
}
