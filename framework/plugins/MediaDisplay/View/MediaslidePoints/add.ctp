<div class="row-fluid">
	<div class="span9">
		<?php echo $this->BootstrapForm->create('MediaslidePoint', array('class' => 'form-horizontal'));?>
			<fieldset>
				<legend><?php echo __('Add %s', __('Mediaslide Point')); ?></legend>
				<?php
				echo $this->BootstrapForm->input('name', array(
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('copy', array(
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('mediaslide_id', array(
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);
				?>
				<?php echo $this->BootstrapForm->submit(__('Submit'));?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__( __('Mediaslide Points')), array('action' => 'index'));?></li>
			<li><?php echo $this->Html->link(__( __('Mediaslides')), array('controller' => 'mediaslides', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('New %s', __('Mediaslide')), array('controller' => 'mediaslides', 'action' => 'add')); ?></li>
		</ul>
		</div>
	</div>
</div>