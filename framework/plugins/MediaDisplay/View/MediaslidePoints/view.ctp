<div class="row-fluid">
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('')), array('controller' => '', 'action' => 'add'));?> </li>
		</ul>
	</div>
	<div class="span9">
		<h2><?php  echo __('Mediaslide Point');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($mediaslidePoint['MediaslidePoint']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Name'); ?></dt>
			<dd>
				<?php echo h($mediaslidePoint['MediaslidePoint']['name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Copy'); ?></dt>
			<dd>
				<?php echo h($mediaslidePoint['MediaslidePoint']['copy']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Mediaslide'); ?></dt>
			<dd>
				<?php echo $this->Html->link($mediaslidePoint['Mediaslide']['name'], array('controller' => 'mediaslides', 'action' => 'view', $mediaslidePoint['Mediaslide']['id'])); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Mediaslide Point')), array('action' => 'edit', $mediaslidePoint['MediaslidePoint']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Mediaslide Point')), array('action' => 'delete', $mediaslidePoint['MediaslidePoint']['id']), null, __('Are you sure you want to delete # %s?', $mediaslidePoint['MediaslidePoint']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__( __('Mediaslide Points')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Mediaslide Point')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__( __('Mediaslides')), array('controller' => 'mediaslides', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Mediaslide')), array('controller' => 'mediaslides', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

