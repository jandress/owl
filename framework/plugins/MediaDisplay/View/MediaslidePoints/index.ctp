<div class="row-fluid">
	<div class="span9">
		<h2><?php echo __( __('Mediaslide Points'));?></h2>

		<p>
			<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
		</p>

		<table class="table">
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('copy');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('mediaslide_id');?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($mediaslidePoints as $mediaslidePoint): ?>
			<tr>
				<td><?php echo h($mediaslidePoint['MediaslidePoint']['id']); ?>&nbsp;</td>
				<td><?php echo h($mediaslidePoint['MediaslidePoint']['name']); ?>&nbsp;</td>
				<td><?php echo h($mediaslidePoint['MediaslidePoint']['copy']); ?>&nbsp;</td>
				<td>
					<?php echo $this->Html->link($mediaslidePoint['Mediaslide']['name'], array('controller' => 'mediaslides', 'action' => 'view', $mediaslidePoint['Mediaslide']['id'])); ?>
				</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $mediaslidePoint['MediaslidePoint']['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $mediaslidePoint['MediaslidePoint']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $mediaslidePoint['MediaslidePoint']['id']), null, __('Are you sure you want to delete # %s?', $mediaslidePoint['MediaslidePoint']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>

		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('New %s', __('Mediaslide Point')), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__( __('Mediaslides')), array('controller' => 'mediaslides', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Mediaslide')), array('controller' => 'mediaslides', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>