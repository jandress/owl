<div class="row-fluid">
	<div class="well wb">
	<h2><?php echo __( __('Media Show Folders'));?></h2>
	<div class = "clearfix">&nbsp;</div>
</div>
<table class="table table-bordered wb">
	
	<tr>
		<th>
			Directory
		</th>
		<th class="actions span4" style='text-align:center'><?php echo __('Actions');?></th>
	</tr>
	
	<?php foreach ($mediashows as $mediashow): ?>
		<tr>
			<td><?php echo( $mediashow ); ?>&nbsp;</td>
			<td class="actions">
				<a href = '/media/<?php echo $mediashow ?>' class='btn btn-block' target = '_blank'>View</a>
			</td>
		</tr>
	<?php endforeach; ?>
	
	
</table>

</div>