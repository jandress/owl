<div class="row-fluid">
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('')), array('controller' => '', 'action' => 'add'));?> </li>
		</ul>
	</div>
	<div class="span9">
		<h2><?php  echo __('Mediaslide');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($mediaslide['Mediaslide']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Image'); ?></dt>
			<dd>
				<?php echo h($mediaslide['Mediaslide']['image']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Name'); ?></dt>
			<dd>
				<?php echo h($mediaslide['Mediaslide']['name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Content'); ?></dt>
			<dd>
				<?php echo h($mediaslide['Mediaslide']['content']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Mediaslide')), array('action' => 'edit', $mediaslide['Mediaslide']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Mediaslide')), array('action' => 'delete', $mediaslide['Mediaslide']['id']), null, __('Are you sure you want to delete # %s?', $mediaslide['Mediaslide']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__( __('Mediaslides')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Mediaslide')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__( __('Mediaslide Points')), array('controller' => 'mediaslide_points', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Mediaslide Point')), array('controller' => 'mediaslide_points', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Mediaslide Points')); ?></h3>
	<?php if (!empty($mediaslide['MediaslidePoint'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Name'); ?></th>
				<th><?php echo __('Copy'); ?></th>
				<th><?php echo __('Mediaslide Id'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($mediaslide['MediaslidePoint'] as $mediaslidePoint): ?>
			<tr>
				<td><?php echo $mediaslidePoint['id'];?></td>
				<td><?php echo $mediaslidePoint['name'];?></td>
				<td><?php echo $mediaslidePoint['copy'];?></td>
				<td><?php echo $mediaslidePoint['mediaslide_id'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'mediaslide_points', 'action' => 'view', $mediaslidePoint['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'mediaslide_points', 'action' => 'edit', $mediaslidePoint['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'mediaslide_points', 'action' => 'delete', $mediaslidePoint['id']), null, __('Are you sure you want to delete # %s?', $mediaslidePoint['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
</div>
