<div class="row-fluid">
	<div class="span9">
		<?php echo $this->BootstrapForm->create('Mediaslide', array('class' => 'form-horizontal'));?>
			<fieldset>
				<legend><?php echo __('Add %s', __('Mediaslide')); ?></legend>
				<?php
				echo $this->BootstrapForm->input('image', array(
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('name', array(
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('content');
				?>
				<?php echo $this->BootstrapForm->submit(__('Submit'));?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__( __('Mediaslides')), array('action' => 'index'));?></li>
			<li><?php echo $this->Html->link(__( __('Mediaslide Points')), array('controller' => 'mediaslide_points', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('New %s', __('Mediaslide Point')), array('controller' => 'mediaslide_points', 'action' => 'add')); ?></li>
		</ul>
		</div>
	</div>
</div>