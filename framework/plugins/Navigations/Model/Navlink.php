<?php
App::uses('AppModel', 'Model');
/**
 * Navlink Model
 *
 * @property Navlink $ParentNavlink
 * @property Navlink $ChildNavlink
 */

class Navlink extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
//	var $actsAs=array('Polymorphic','Containable','Navigations.GroupTree');
	var $actsAs=array('Containable','Navigations.GroupTree');
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'class' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),

		'in_nav' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'in_next' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ParentNavlink' => array(
			'className' => 'Navlink',
			'foreignKey' => 'parent_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ChildNavlink' => array(
			'className' => 'Navlink',
			'foreignKey' => 'parent_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


	private function saveChildren($id_for_parent,$avatar){
			$this->contain();
			$b = $this->find('all',array(
				'conditions'=> array('Navlink.parent_id'=>$id_for_parent)
			));
			foreach ($b as $key => $value) {
				$value['Navlink']['avatar']=$avatar;
				$this->saveChildren($value['Navlink']['id'],$avatar);
				parent::save($value);
			}
	}
	public function save($data = null, $validate = true, $fieldList = array()) {
		if(!empty($data['Navlink']['avatar'])){
			$this->saveChildren($data['Navlink']['id'],$data['Navlink']['avatar']);
		}else{
//			$this->saveChildren($data['Navlink']['id']);
		}

		return parent::save($data, $validate, $fieldList);
    }



	public $avatars = array(
		'sherlock'=>'sherlock',
		'zombie'=>'zombie',
		'twain'=>'twain',
		'hipster'=>'hipster',
		'digital'=>'digital',
		'judge'=>'judge',
		'grad'=>'grad',
		'world'=>'world'
	);



	// $boundModel->deleteAll(array('Landingtout.landingtier_id'=>$id));
	// return parent::delete ($id, $cascade);


	// public function delete( $id = null,  $cascade = true){
	// 	//this function serves to delete
	// 	$this->contain();
	// 	$b = $this->read();
	// 	$boundModel = ClassRegistry::init($b['Navlink']['class']);
	// 	$t = $boundModel->deleteAll(array($b['Navlink']['class'].'.id' => $b['Navlink']['foreign_id']));
	// 	return parent::delete( $id,  $cascade );
	// }



	public function deleteNavlinkAndContent( $id = null,  $cascade = true){
		//this function serves to delete the navlink and it's content.
		$this->contain();
		$b = $this->read();
		$boundModel = ClassRegistry::init($b['Navlink']['class']);
		$t = $boundModel->deleteAll(array($b['Navlink']['class'].'.id' => $b['Navlink']['foreign_id']));
		return $this->delete( $id,  $cascade );
	}









}
