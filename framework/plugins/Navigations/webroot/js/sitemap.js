function closeChildNavLinks(d) {
	var address = d.split("n")[0];
	var g = $(".sitemap_row");
	for(var i = 0; i < g.length; i++){
		var row_id = $(g[i]).attr('id');
		var t = row_id.split("n")[0];
		var b = "SM"+address
		if(t!=b){
			if(t.substring(0,b.length)==b){
				$("#"+row_id).remove();
			}
		}
		var x;
	}
	$("#expand_"+d).css('display','block');	
	$("#collapse_"+d).css('display','none');
	return false;
};



function getChildNavLinks( d ) {
	// child_id should be the navlink id.   change this to reflect.
	var child_id = d.split("n")[1]
	var address = d.split("n")[0]
	var div = $("#SM"+d);
	$('<tr id= "sitemapchildren_'+address+'" ><td id = "sitemap_loader" class="centered"><div class = "bm_2_em"><em><h4>Loading...</h4></em><img src="/img/loader.gif" alt = ""/></div></td></tr>').insertAfter(div);
	$.ajax({
		url: '/my/navlinks/children/'+child_id+"/"+address,
		success: function(data){
			// insert after div? where is this getting inserted?
			//console.log(data);
			$("<tr> "+data+" </tr>").insertAfter(div);
			$("#sitemap_loader").remove();
			$("#expand_"+d).css('display','none');
			$("#collapse_"+d).css('display','block');
			initSitemap()
		}
	});
};

function initSitemap(){
	$(".expand_button").unbind('click');
	$(".collapse_btn").unbind('click');
	$(".expand_button").click(function(e){
		var g = $(e.currentTarget).attr('href');
		getChildNavLinks(g);
		return false;
	});
	$(".collapse_btn").click(function(e){
		var g = $(e.currentTarget).attr('href');
		closeChildNavLinks(g);
		return false;
	});
}


initSitemap()