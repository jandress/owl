<?php
	App::uses('AppController', 'Controller');

	class NavlinksController extends AppController {

	var $defaultRedirect = '/my/sitemap/';
	

	public $helpers = array(
		'TwitterBootstrap.BootstrapHtml',
		'TwitterBootstrap.BootstrapForm',
		'TwitterBootstrap.BootstrapPaginator',
		'Navigations.TreeNav'
	);

	public $components = array(
		'Session',
		'TreeNav',
		'Paginator',
		'Auth',
		// 'Userassets.Fileutil'
	);

	public $uses = array(
		'Navlink',
		'Avatar',
		'Virtuoso.Interval'
		,'Post'
	);

	var $classOptions=array(
		'options'=>array(
			'Post'=>'Post',
			'Url'=>'Url',
			'Activity'=>'Activity',
			'Activity'=>'Caged Activity',
			'Virtuoso.Interval'=>'Interval',
			'Dashboard'=>'Dashboard',
			'Landingpage'=>'Landing Page',
			'Quiz'=>'Quiz'
		),
		'label'=>'Content Type'
	);


	public $paginate = array();


	public function beforeFilter(){
		parent::beforeFilter();
		$this->set('title_for_layout', "Site Map");
		$this->Auth->allow('index','admin_children');

		$this->set('avatars',$this->Avatar->avatars);
	}


	public function generateposts(){
		$this->Navlink->contain();
		$g = $this->Navlink->find("all");
		foreach ($g as $key => $value) {
			 debug($value);  	# code...
			$p = array('title'=>$value['Navlink']['name']);
			
			
			$this->Post->create(
				$p	
			);
			$saved = $this->Post->save($p);
			debug($saved);
			$value['Navlink']['foreign_id'] = $saved['Post']['id'];
			debug($this->Navlink->save($value));
		}
		exit;
	}

	private function getSitemap(){
		$this->Navlink->recursive = 3;
		$this->Navlink->contain('ChildNavlink');
		$g = $this->Navlink->find('all', array(
				'order' => array('Navlink.lft'),
				//'conditions'=>array('Navlink.id != 0')
			)
		);
		return $g;
	}

	public function admin_list() {

		$this->setDashboardLayout();
		$this->data = $this->paginate();
		//debug($this->data); exit;
		$this->set('navtree', $this->data);
		// $this->set('navtree', $this->Navlink->find('all',
		// 		array(
		// 			///'order'=>array('Navlink.lft asc'),
		// 			'conditions'=>array(
		// 				array('AND' => array(
		// 				    array('Navlink.id !=' => 1),
		// 				    array('Navlink.id !=' => 2)
		// 				))
		// 			)
		// 		)
		// 	));
	}

	public function setAvatars(){
		$this->Navlink->contain();
		$allLinks = $this->Navlink->find('all');
		foreach ($allLinks as $key => $value) {
			$path = $this->Navlink->getPath($value['Navlink']['id']);
			$rootId = 1;
			if(count($path)>2){
				$rootNode = $path[2];
				$rootId = $rootNode['Navlink']['id'];
			}
			$p = $this->Avatar->getAvatarBySectionId($rootId);
			$value['Navlink']['avatar'] = $p['image'];
			$this->Navlink->save($value);
		}
		return true;
	}

	public function getavatar($id){
		$this->handleAjax();
		$response = $this->Navlink->find('first', array('conditions'=>array('Navlink.id'=>$id)));
		$this->set('response',$response['Navlink']['avatar']);
	}

	public function admin_delete($id = null) {
		// deletes this link and its children.
		$this->Navlink->id = $id;
		if (!$this->Navlink->exists()) {
			throw new NotFoundException(__('Invalid %s', __('navlink')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			$this->Navlink->id = $this->request->data['id'];
			if ($this->Navlink->delete()) {
				$this->Session->setFlash(
					__('The %s has been saved', __('navlink')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect($this->defaultRedirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('navlink')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {

		}
		$children = $this->getChildrenList($id);
		$this->set(compact('parentNavlinks','children'));
	}


	// deletes this link only
	public function admin_remove($id = null) {
		$this->Navlink->id = $id;
		if (!$this->Navlink->exists()) {
			throw new NotFoundException(__('Invalid %s', __('navlink')));
		}
		$this->Navlink->id = $id;
		$g = $this->Navlink->removeFromTree($id);
		if ($g) {
			$this->Session->setFlash(
				__('The %s has been removed', __('navlink')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect($this->defaultRedirect);
		} else {
			$this->Session->setFlash(
				__('The %s could not be removed. Please, try again.', __('navlink')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-error'
				)
			);
		}
		$children = $this->getChildrenList($id);
		$this->set(compact('parentNavlinks','children'));
	}


	public function admin_edit($id = null) {
		//$this->Navlink->recover();
		$this->setDashboardLayout();
		$this->set('classOptions',$this->classOptions);
		$this->Navlink->id = $id;
		if (!$this->Navlink->exists()) {
			throw new NotFoundException(__('Invalid %s', __('navlink')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Navlink->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('navlink')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);

				$this->redirect($this->defaultRedirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('navlink')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->paginate['conditions'] = array('Navlink.parent_id'=>$id);
			$this->set('navlinks', $this->paginate());
			$this->request->data = $this->Navlink->read(null, $id);
		}


		$g = array(
			'options' => $this->Avatar->getimagesAsOptions($this->Fileutil->getFileListing('.'.$this->Avatar->avatarDirectory))
		);



		$g['value'] = $this->data['Navlink']['avatar'];
		$this->set('fileOptions', $g);

		$this->setfidData($this->request->data['Navlink']['class']);
		$parentNavlinks = $this->Navlink->ParentNavlink->find('list',array('order'=>array('name'),'conditions'=>array('id!='.$id)));
		$children = $this->getChildrenList($id);
		$this->set(compact('parentNavlinks','children'));
	}

	public function getfid($g){
		$this->layout = 'ajax';
		$this->setfidData($g);
	}

	public function admin_relocate($id,$direction){
			$this->layout = 'ajax';
			if($direction=='up'){
				//$this->Navlink->recover();

				$this->Navlink->moveUp($id);
				$this->redirect($this->defaultRedirect);
			}
			if($direction=='down'){
				//$this->Navlink->recover();
				$this->Navlink->moveDown($id);
			}
			$this->redirect('/my/sitemap/');
			exit;
	}

/*
	private function getForeignIdList($g){
		if($g=='Url'){
			$this->set('label',$g);
			return;
		}

		
		// 		$result = $TestModel->bindModel(array(

		//~ binding a model by the model's string name!!
		$this->loadModel($g);
		$this->set('foreign_id_options',$this->$g->find('list'));
	}
*/
	public function admin_index($id=2) {
		$this->layout = 'sitemap';
		$this->addAdditionalScript('/Navigations/js/sitemap');
		$this->admin_children(2);
	}
	public function generate_avatars($id=2) {
		debug($this->setAvatars());
		exit;
	}

	public function index(){
		$this->addAdditionalScript('/Navigations/js/sitemap');
		///	$this->setDashboardLayout('_one_col');
		$this->admin_children(2);
		$this->layout = 'sitemap';
	}

	public function admin_children($id=2, $address = 0){
		//$this->Navlink->contain('ChildNavlink');
		$g = $this->Navlink->find('all', array(
				'order' => array('Navlink.lft')
				,'conditions'=>array('Navlink.parent_id' => $id)
			)
		);
		$this->set('navlinks',$g);
		//$level = array_count();
		//$this->set('level',$level);
		$this->set('address',$address);
	}


	public function admin_add($parentId=2) {
		$this->set('classOptions',$this->classOptions);
		$this->setDashboardLayout();

		if ($this->request->is('post')) {

			// $this->Navlink->create();
			// debug($this->Navlink->save($this->request->data));
			// exit;
			if ($this->Navlink->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('navlink')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect($this->defaultRedirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('navlink')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$this->data = $this->Navlink->create();
		$this->setfidData('Post');
		$n = $this->data;
		$n['Navlink']['parent_id'] = $parentId;
		$this->data = $n;
		$parentNavlinks = $this->Navlink->ParentNavlink->find('list',array('order'=>'name'));
		// $parentNavlinks = $this->Navlink->find('all');
		$g = array(
			'options' => $this->Avatar->getimagesAsOptions($this->Fileutil->getFileListing('.'.$this->Avatar->avatarDirectory))
		);
		$g['value'] = $this->data['Navlink']['avatar'];
		 $this->set('fileOptions', $g);

		$this->set(compact('parentNavlinks'));
	}

	private function setfidData($g){
		$conditions = array();
		if($g=='Url'){
			$this->set('label',$g);
			return;
		}
		if($g=='Branchingslide'){
			$conditions=array('conditions'=>array('Branchingslide.parent_id'=>0));
		}
		if($g=='Post'){
			$conditions['order']='title';
		}


		//~ binding a model by the model's string name!!
		// debug($g); exit;
		$this->loadModel($g);
		$y = $this->$g->find('list',$conditions);
		$p=array();
		foreach ($y as $key => $value) {
			$p[$key] = $value.' ('.$g.' id: '.$key.')';
		}
		$p[0] = "New ".$g;
		$q = ksort($p);
		$this->set('foreign_id_options', $p);
	}



	public function getChildrenList($id = 0){
		$parent = $this->Navlink->find('first', array('conditions' => array('Navlink.id' => $id)));
		$data = $this->Navlink->find('threaded', array(
		    'conditions' => array(
		        'Navlink.lft >=' => $parent['Navlink']['lft'],
		        'Navlink.rght <=' => $parent['Navlink']['rght']
		    )
		));
		return $data;//$this->TreeNav->RecursiveChildren($data);
	}




	public function recover(){
		debug($this->Navlink->recover());
		exit;
	}
	public function validate(){
		debug($this->Navlink->validate());
		exit;
	}



	}
