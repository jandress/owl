<div class="row-fluid">
		<div class="white_well">	
				<h2>Sitemap</h2>
				<h5>Click + to expand</h5>
		</div>
		<?php if (!empty($is_manager)): ?>
			<div class="navbar">


					<div class="navbar-inner">
						<div style="margin: 0;" class="btn-toolbar">
							  <div class="btn-group pull-right">	
								<a href = '/my/navlinks/add/' class = 'btn'><div class="icon-plus">&nbsp;</div>&nbsp;New Menu / Link</a>
								<a href = '/my/navlinks/' class = 'btn' ><i class = 'icon-list'>&nbsp;</i> &nbsp;View All as a flat list</a>
							  </div>
						  </div>	
					</div>


			</div>			
		<?php endif ?>
		<div class="white_well">
			<ul id='sitemap' class=''>
				<?php  echo $this->TreeNav->sitemap($navlinks, !empty($is_manager)); ?>				
			</ul>
		</div>
</div>

	<script type="text/javascript" >
		$("#expandAll").change(function(){
			sitemapstyler($("#expandAll").is(':checked'));
		});
		<?php if (!empty($openSitemap)): ?>
			var initialyOpened = true;
		<?php else: ?>
			var initialyOpened = false;		
		<?php endif ?>
	</script>

<?php echo $this->Html->script('sitemap'); ?>