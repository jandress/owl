<div class="navbar-inverse admin_padding">
  <div class="btn-group pull-right tm_1_em pr_1_em">
    <a class="btn btn-sm btn-default " href="/my/navlinks/add/"><div class="glyphicon glyphicon-plus-sign">&nbsp;</div>&nbsp;New Menu / Link</a>
    <a class="btn btn-sm btn-default " href="/my/navlinks/list"><i class="glyphicon glyphicon-list">&nbsp;</i> &nbsp;View All as a flat list</a>
  </div>
  <div class = "navbar-inverse_left pt_h_em">
    <h2>Sitemap</h2>
  </div>
  <div class = "clear clearfix tm_1_em"></div>
</div>

<div class="admin_padding tm_1_em" >


<table class="table table-striped table-bordered table-hover">
    <tbody>
    <tr></th>
      <tr>
        <?php  echo $this->element('Navigations.recursive_sitemap_navlinks',array('is_manager'=>true,'navlinks'=>$navlinks)); ?>
      </tr>
    </tbody>
</table>



  <?php
///null var to disable this here, but keeping the code for a bit
if (!empty($isAdmin) && !empty($null_variable)): ?>
  <div class="">
    <a href = '/my/navlinks/generate_avatars' class="btn btn-block">Build Navlink Avatars</a>
  </div>
<?php endif ?>
</div>
