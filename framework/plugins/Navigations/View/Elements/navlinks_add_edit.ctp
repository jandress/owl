<div class="">



		<div class = "navbar-inverse">
			<?php if (empty($this->data['Navlink']['name'])): ?>
				<h2>New Navlink
				</h2>				
			<?php else: ?>
				<h2>Editing Nav Link: <?php echo $this->data['Navlink']['name']; ?></h2>
			<?php endif ?>
		</div>





		<div class="admin_padding">
			<?php echo $this->BootstrapForm->create('Navlink', array('class' => 'form-horizontal'));?>

			<?php

				echo $this->BootstrapForm->input('parent_id', array(
					'required' => 'required',
					'options'=>$parentNavlinks,
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);

				echo $this->BootstrapForm->input('name', array(
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);
			?>


				<table>
					<tr>
						<th></th>
						<th></th>
						<th></th>
					</tr>
					<tr>
						<td>
							<?php echo $this->BootstrapForm->input('in_next', array()); ?>
						</td>
						<td>
							<?php
									echo $this->BootstrapForm->input('in_nav', array());
							 ?>
						</td>
						<td>

							<div class="control-group">
								<label class="control-label" for="ownership">Link Target</label>
								<div class="controls">
									<?php
									$options=array(
										'_blank'=>'New Window'
										,'_self'=>'This window'
									);
									echo $this->BootstrapForm->select('window', $options);
									?>
								</div>
							</div>


						</td>
					</tr>
				</table>


			<div class="tm_3_em bm_5_em">
				<?php echo $this->BootstrapForm->input('avatar',$fileOptions); ?>
			</div>



			<?php
				$nl = $this->data['Navlink'];
				// debug($classOptions);
				// debug($this->data);
				echo $this->BootstrapForm->input('class',$classOptions);
			?>
			<div id="classAdmin">
				<?php echo $this->element('fid'); ?>
			</div>
			<div class="clear"></div>

			<?php echo $this->BootstrapForm->hidden('id'); ?>

			<div class="pull_right tm_1_em">
				<?php echo $this->BootstrapForm->submit(__('Submit'),array('class'=>array('btn pull-right')));?>
			</div>

			<?php echo $this->BootstrapForm->end();?>
		</div>
</div>







<script type="text/javascript" >
		function addModuleToPage(val){
			var targetDiv = "#classAdmin";
			$(targetDiv).html('<div class="icon-refresh offset3">&nbsp;</div>');
			$.ajax({
				url: '/navlinks/getfid/'+val,
				context: $(targetDiv),
				success: function(data){
					$(this).html(data);
					//$(data).appendTo(targetDiv);
				}
			});
		}
		function updateAvatar(val){
			// $(targetDiv).html('<div class="icon-refresh offset3">&nbsp;</div>');
			$.ajax({
				url: '/navlinks/getavatar/'+val,
				success: function(data){
					$("#NavlinkAvatar").val(data);
					//$(data).appendTo(targetDiv);
				}
			});
		}

		function deleteModule(div_id,thingie_id){}


		$("#NavlinkClass").change(function(e){
			addModuleToPage(e.target.value);
		});
		$("#NavlinkParentId").change(function(e){
			updateAvatar(e.target.value);
		});
</script>
