<div class="row-fluid">
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('')), array('controller' => '', 'action' => 'add'));?> </li>
		</ul>
	</div>
	<div class="span9">
		<h2><?php  echo __('Navlink');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($navlink['Navlink']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Title'); ?></dt>
			<dd>
				<?php echo h($navlink['Navlink']['title']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Class'); ?></dt>
			<dd>
				<?php echo h($navlink['Navlink']['class']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Parent Navlink'); ?></dt>
			<dd>
				<?php echo $this->Html->link($navlink['ParentNavlink']['title'], array('controller' => 'navlinks', 'action' => 'view', $navlink['ParentNavlink']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Lft'); ?></dt>
			<dd>
				<?php echo h($navlink['Navlink']['lft']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Rght'); ?></dt>
			<dd>
				<?php echo h($navlink['Navlink']['rght']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('In Nav'); ?></dt>
			<dd>
				<?php echo h($navlink['Navlink']['in_nav']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('In Next'); ?></dt>
			<dd>
				<?php echo h($navlink['Navlink']['in_next']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Foreign Id'); ?></dt>
			<dd>
				<?php echo h($navlink['Navlink']['foreign_id']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Navlink')), array('action' => 'edit', $navlink['Navlink']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Navlink')), array('action' => 'delete', $navlink['Navlink']['id']), null, __('Are you sure you want to delete # %s?', $navlink['Navlink']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__( __('Navlinks')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Navlink')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__( __('Navlinks')), array('controller' => 'navlinks', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Parent Navlink')), array('controller' => 'navlinks', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Navlinks')); ?></h3>
	<?php if (!empty($navlink['ChildNavlink'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Title'); ?></th>
				<th><?php echo __('Class'); ?></th>
				<th><?php echo __('Parent Id'); ?></th>
				<th><?php echo __('Lft'); ?></th>
				<th><?php echo __('Rght'); ?></th>
				<th><?php echo __('In Nav'); ?></th>
				<th><?php echo __('In Next'); ?></th>
				<th><?php echo __('Foreign Id'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($navlink['ChildNavlink'] as $childNavlink): ?>
			<tr>
				<td><?php echo $childNavlink['id'];?></td>
				<td><?php echo $childNavlink['title'];?></td>
				<td><?php echo $childNavlink['class'];?></td>
				<td><?php echo $childNavlink['parent_id'];?></td>
				<td><?php echo $childNavlink['lft'];?></td>
				<td><?php echo $childNavlink['rght'];?></td>
				<td><?php echo $childNavlink['in_nav'];?></td>
				<td><?php echo $childNavlink['in_next'];?></td>
				<td><?php echo $childNavlink['foreign_id'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'navlinks', 'action' => 'view', $childNavlink['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'navlinks', 'action' => 'edit', $childNavlink['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'navlinks', 'action' => 'delete', $childNavlink['id']), null, __('Are you sure you want to delete # %s?', $childNavlink['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
</div>
