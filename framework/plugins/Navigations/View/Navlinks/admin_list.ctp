<div class="row-fluid">
	<div class="white_well">
				<h2><?php echo __( __('Navigation Sets'));?></h2>
				<div class="navbar">
					<div class="navbar-inner">
						<div class='span9' style = 'padding-top:8px'>
							<p>Displaying <?php echo count($navtree) ?> of <?php echo count($navtree) ?> records</p>
						</div>
						<div class="nav span3 pull-right">
							<a href = '/my/navlinks/add/' class = 'btn pull-right'><div class="icon-plus">&nbsp;</div>&nbsp;New Navlink</a>
						</div>				
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover">
					<tr>
						<th  style="text-align:center" class = 'span1' ><?php echo $this->BootstrapPaginator->sort('id');?></th>
						<th><?php echo $this->BootstrapPaginator->sort('title');?></th>
						<th><?php echo $this->BootstrapPaginator->sort('parent_id');?></th>
						<th class="actions" style="text-align:center" ><?php echo __('Actions');?></th>
					</tr>
					<?php 
				//	debug($navtree);
					foreach ($navtree as $key => $nl):

							$navlink = $nl['Navlink'];
							$class= $navlink['class'];
							$hasPairing = !empty($nl[$class]);
							$content =  $hasPairing ? $nl[$class] : array();
							$hasParent = !empty($nl['ParentNavlink']);
							$parent = $nl['ParentNavlink'];
							$childrenCount = 0;
							if(!empty($nl['ChildNavlink'])){
								$childrenCount = count($nl['ChildNavlink']);						
							}
						?>
						<tr> 
							<td style="text-align:center">
								 <small><?php echo h($navlink['id']); ?>&nbsp;</small>
							</td>
							<td>
								<small><?php echo h($navlink['name']); ?></small>
							</td>					
							<td>
								<small>
									<?php if ($hasParent): ?>
										<a href = '/my/navlinks/edit/<?php echo $parent['id'] ?>' target = "_blank"><?php echo $parent['id'] ?> : <?php echo h($parent['name']); ?>
									<?php else: ?>
										No Parent
									<?php endif ?>
								</small>
							</td>					
							<td class="actions span2" style="text-align:center" >

								<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $navlink['id'])); ?><span class = ''> | </span>

								<?php 
								//echo $this->Form->postLink(__('Delete'), array('url' => '/my/navlinks/delete/'.$navlink['id'], $navlink['id']), null, __('Are you sure you want to delete %s? It has '.$childrenCount.' children that will also be deleted.', $navlink['name'])); 
								//postLink(string $title, mixed $url = null, array $options = array (), string $confirmMessage = false)¶
								
								echo $this->Form->postLink(__('Delete'),'/my/navlinks/delete/'.$navlink['id'],array('data'=>array('id'=>$navlink['id'])),__('Are you sure you want to delete this link? It has '.$childrenCount.' children that will also be deleted.'));
								
								?>
							</td>
						</tr>
					<?php endforeach ?>
				</table>
	
	</div>
</div>