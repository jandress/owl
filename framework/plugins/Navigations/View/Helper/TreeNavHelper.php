<?php

//	App::uses('Helper', 'View');
	App::uses('AppHelper', 'View/Helper','Helper', 'View');
	

	class TreeNavHelper extends AppHelper {
		var $level = -1;
		var $i = 0;
		var $helpers = array('TwitterBootstrap.BootstrapPaginator','Html','BootstrapPaginator','BootstrapForm','Form');
	
		private function getRelocationLinks($id){
			$str2 = '<div class="pull-right">
				<a  style = "margin-right:14px" href="/my/navlinks/edit/'.$id.'" >
					Edit
				</a>				<div class="btn-group btn-group-vertical" style = "margin-right:14px" >
					<a class="btn btn-small relocation_arrows" href="/my/navlinks/relocate/'.$id.'/up/" >
						<div class = "icon icon-chevron-up">&nbsp;</div>					
					</a>
					<a class="btn btn-small relocation_arrows" href="/my/navlinks/relocate/'.$id.'/down/">
						<div class = "icon icon-chevron-down">&nbsp;</div>				
					</a>				
				</div>			</div>';
			$str2.='<div class = "clearfix" style="line-height:2px">&nbsp;</div>';			
			return $str2;
		}

		function sitemapLevel($array, $admin = false) { 
			$str='';
		    if (count($array)) { 				
		        foreach ($array as $vals) { 		      
			      	$navlink = $vals['Navlink'];
					$class = $navlink['class'];
					$fid = $navlink['foreign_id'];
					$content = array();
					$hasContent = false;
					//--------
					if(!empty($vals[$class])){
						$content = $vals[$class];
						$hasContent = true;						
					};
					//--------
					$str.= "<li id=\"".$navlink['id']."\">"; 
						if($hasContent && ($admin)){
							//view action & edit action.  pulling content from $content. ( $content = $vals[$class]).\
							//when the $content gets deleted...this needs to degrade gracefully
							$str.= "<div class = 'span9'>"; 					
								$str.= "<a  href='".$content['viewActionFull']."' >".$navlink['name']."</a>";
								$str.= "<div style='display:block;'><small><a  href='".$content['editActionFull']."' >".ucfirst(strtolower($class.": ".$content['id'].', '.$content['display_field']))."</a></small></div>"; 
							$str.= "</div>"; 					
						}else{
							if (strtolower($navlink['class'])=='url') {
								$str.= "<a  href='".$navlink['foreign_id']."' >".$navlink['name']."</a>";							
							}else{
								$str.= "<a  href='".$content['viewActionFull']."' >".$navlink['name']."</a>";							
							}
						}
					if($admin){						
						$str.= $this->getRelocationLinks($navlink['id']);						
					}		            
					if (count($vals['ChildNavlink'])) { 
		       			$str.= "\n<ul>\n"; 				
	            		$str.= $this->sitemap($vals['children'],$admin);
		        		$str.= "</ul>\n"; 		
		            }
		            $str.= "</li>\n"; 
		        } 
		    }
			return $str;
		}




		function sitemap($array, $admin = false) { 
			$str ='';
			$str = '<ul class = "sitemap">';			
			$str .= $this->sitemapLevel($array, $admin);
			$str.='</ul>';
			return $str;
		} 	
		
		
	
		//------------------------------------------------------------------------------------------------------------------------------
		// Left Nav
		//------------------------------------------------------------------------------------------------------------------------------

		private $lastLinkValue;
		private $prevValue;
		private $nextValue;
		private $currentIsSet = false;		
		//if $nextValue is null, prevValue is not null, and its not this link, then the next link in iteration is nextvalue.
		
		private function recurse($array, $routing_prefix = '',$level=0){
			$this->level++;
			$str= ""; 		
		    if (!empty($array)) { 				
		        foreach ($array as $vals) { 
					$navlink = $vals['Navlink'];
					$window = " target = '".$navlink['window']."' ";					
					// active link or not...checking to see if the linked asset is in the ancestry array.
					// perhaps that should be check to see if the navlink is in the ancestry array.
					//$navLinkClass = $this->inAncestry($vals[$navlink['class']]['id'],$navlink['class']) ? 'active' : '';
					$ia = $this->inAncestry($navlink['id']);
					$is_current = $this->isCurrent($navlink['id']);
					$navLinkClass = '';
					if($ia){
						$navLinkClass = 'active';
					}
					//--
					//	pr($href);
					// there are problems here if you try and use a class that is does not have a polymorphic associated model.
					// if it is url...then 
					if (strtolower($navlink['class'])=='url') {
						$href = $navlink['foreign_id'];
					}else{
						//~ this does not fail gracefully if it fails to find the polymorphic relationship
						// if(empty($vals[$navlink['class']]['viewActionFull'])){
						// 	//debug($vals);
						// 	$href = '';
						// }else{
						// 	$href = $vals[$navlink['class']]['viewActionFull'];																		
						// }
						$href = '/'.Inflector::pluralize(strtolower($navlink['class'])).'/view/'.$navlink['foreign_id'];
					}
					//------------------------------------------------------------------------------------------------------------------------										
					if( $routing_prefix != ''){
						$href = $routing_prefix.'/'.$href;						
					}					
					//------------------------------------------------------------------------------------------------------------------------					
					// these lines must remain in this precise order. if this is the current link, then the last link was previous. 
					// if the last link has been set, and its not the current link, then it must be the next link. 
					// there is probably a better way of setting that immediately when $is_current = true. try that if neccessary.
					
					// the problem with this is that if 
					if($is_current){
						$this->prevValue = $this->lastLinkValue;
						$this->currentIsSet = true;
					}
					if(!empty($this->prevValue) && !$is_current && empty($this->nextValue)){
						$this->nextValue = $href;
					}
					if($this->currentIsSet && !$is_current && empty($this->prevValue) && empty($this->nextValue)){
						// this is for the first link to make sure that next value gets set.
						$this->nextValue = $href;						
					}
					if(empty($this->prevValue)){
						$this->lastLinkValue = $href;	
					}

					$microTime = microtime(true);
					// Convert the microtime to a string that removes the decimal


					mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
					$timestamp = strtoupper(md5(uniqid(rand(), true)));

					//$timestamp = sprintf("%s%06d", date("YmdHis", $microTime), ($microTime - floor($microTime)) * 1000000);
					$t_id = "ncl_".$timestamp;


	
					//------------------------------------------------------------------------------------------------------------------------
					$str.= "<li class = 'treenav_link level_".$this->level." noliststyle' id=\"navlink_".$navlink['id'].rand()."\">";					
						$divClass = $navLinkClass == "active" ? 'activediv' : 'inactivediv'; 
						$str.= "<div class=' ".$divClass."_".$this->level."'>";
						if($href==''){
							$str.= "<span class = 'disabled ".$navLinkClass." navlevel_".$this->level."' >".$navlink['name']."</span>"; 							
						} else {
							$iconPos = '';
							// Has children
							if (count($vals['ChildNavlink'])) { 
								//if it's active, use the the down chevron. otherwise the right pointing one. 
								$iconPos = $navLinkClass == "active" ? 'glyphicon glyphicon-chevron-down' : 'glyphicon glyphicon-chevron-right'; 
								$str.="<div class = 'tree_nav_nested_children' >";
								$str.="<div style = 'text-align:center; width:20px;  float:left; '><a data-target = '".$t_id."' class='chevron_btn ".$iconPos."'>&nbsp;</a></div>";
								$str.= "<a class = 'float:left;".$navLinkClass." navlevel_".$this->level."' ".$window." href='".$href."' >".$navlink['name']."</a>"; 							
								$str.="</div>";
							}else{
								$str.="<div>";						
								$str.="<div style = 'width:20px;  float:left;  float:left;'><div>&nbsp;</div></div>";
								$str.= "<a class = ' ".$navLinkClass." navlevel_".$this->level."' ".$window." href='".$href."' >".$navlink['name']."</a>"; 							
								$str.="</div>";
							}	
						}
						$str.= "</div>";
						//---
		            $str.= "</li>\n";
					
					
					//if it has children, create uls, and recurse. if exists in ancestry array, tag ul as open, else closed.
		            if (count($vals['ChildNavlink'])) { 
						$ulClass = $this->inAncestry($vals['Navlink']['id']) ? 'open' : 'closed';
						$ulClass.= ' noliststyle '.' ul_level_'.($this->level+1);
						$thisstl = '';
						if($navLinkClass != "active"){
							$thisstl = "class = 'hidden'";
						}
						$str.= '<ul id = "'.$t_id.'" '.$thisstl.'>';
							//	$str.= $this->recurse($vals['children'],$this->admin);		
							$level++;
							$str.= $this->recurse($vals['children'],$routing_prefix,$level);		
    					 $str.=  "</ul>\n";				
		            }



		        } 
		    } 
			$this->level--;	
			return $str;
		}



		function navigation($array, $admin = false, $selection_descendancy=null) { 

			$this->admin = $admin;
			if(empty($selection_descendancy)){
				$selection_descendancy = array();
			}
			$this->selection_descendancy = $selection_descendancy;	
//			debug($this->selection_descendancy)
			echo '<ul class = "treenav ul_level_0" >'.$this->recurse($array).'</ul>';
			return 'children is the current prefered api call...will come back to this';
		} 	
		
		
		
		
		function children($array, $admin = false, $selection_descendancy=null, $options = null) { 
			$array = $array[0]['children'];
			$this->admin = $admin;
			if(empty($selection_descendancy)){
				$selection_descendancy = array();
			}
			$this->selection_descendancy = $selection_descendancy;	
			
			$ulClass = empty($options['ulClass']) ? 'treeNav' : $options['ulClass'];
			$divClass = empty($options['divClass']) ? 'treeNavDiv' : $options['divClass'];							
			$nav = '<div class="'.$divClass.'"><ul class = " noliststyle '.$ulClass.' ul_level_0" >'.$this->recurse($array).'</ul></div>';
			
			$prevLink = '<a href = "'.$this->prevValue.'">< Previous</a>';
			$nextLink = '<a href = "'.$this->nextValue.'">Next ></a>';		
			$prevUrl = $this->prevValue;
			$nextUrl = $this->nextValue;			
			return array(
					'prev'=>$this->prevValue,
					'next'=>$this->nextValue,
					'prevLink'=>$prevLink,
					'nextLink'=>$nextLink,
					'nav'=>$nav
				);
		}
		
		///margin-inline-start: -5.5rem; padding-inline-start: 5.5rem;
		function previousNext($array, $selection_descendancy=null) { 
			$currentPage = $selection_descendancy[count($selection_descendancy)-1];
			return '';
		}
		
		//conditionals @ line 213 and link 220 are quick hacks
		
        private function isCurrent($g, $class='Navlink'){
			$current = $this->getCurrent();
			if(empty($current[$class])){
				return null;
			}
			return $current[$class]['id']==$g;
		}
		
		private function getCurrent(){
			if(!count($this->selection_descendancy)){
				return array();
			}
			return $this->selection_descendancy[count($this->selection_descendancy)-1];
		}
		
		
        private function inAncestry($g, $class='Navlink'){
			// check to see if the object's id is in the selection_descendancy array.
            foreach ($this->selection_descendancy as $key => $value) {       
				if (!empty($value[$class])) {
	                if(($value[$class]['id']*1)==$g){
	                    return true;
	                }
				}else{
					return false;
				}
            }
            return false;
        }

	
}
