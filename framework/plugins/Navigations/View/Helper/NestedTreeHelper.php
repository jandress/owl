<?php 
    class NestedTreeHelper extends AppHelper {

        var $level = -1;
        var $linkNumber = 0;
        var $selectedItems;
		var $allLinks;
		//----
		
		// i misunderstood the role of inAncestry()...it just determines if the link is selected and opened...
		// the recursive structure is determined by array_key_exists($key, $thisLevelLinks[$key]) when foreaching over $thisLevelLinks.
		
		
		// check below to see the various inputs 
		
		// this class is a little odd....it doesnt follow the typical pattern you associate with recursion, its actually iterative input with recursive style output. 
		// it prints the entire tree and tags <uls> with "open" or "closed" <li><spans> get tagged with "active" or ''.  Perhaps for styling purposes it would be better to tag the <li><span><a's>
		
		// at getUL($thisLevelLinks, $selected) it iterates over $thisLevelLinks. $thisLevelLinks identifies an individual level's links, starting with top level links at the api, 
		// and then when iterating over $thisLevelLinks calling and getLiAndChildren($key, $value) on each.
		// each iteration adds <li><a><span> and then checks for children by calling inAncestry($pageId) 
				
		// inAncestry($pageId) checks the page id vs the keys of $this->selectedItems(array) (see below for $this->selectedItems structure. (a zero-indexed array of full model arrays) )
		//
		
		
		//---		
		// there are no child links within child links in the input, but that is how it is assembled though child link determiniation via inAncestry()
		// inAncestry($pageId) checks the page id vs the keys of $this->selectedItems (array)
		// $this->selectedItems is a zero-indexed flat array of descendancy. the keys of $this->selectedItems are zero-indexed and meaningless.  the values are [Model]( model's various keys and values)
		//---
		
		//
		// it is comparing 2 different sets of arrays.  1 of which is static, once of which is iterative over the "SETS OF LINK SETS" for each level. 





		// only public function there is. called once.
        public function getNav($thisLevelLinks, $allLinks, $selectedItems = null ){

			$this->allLinks = $allLinks;
            $this->selectedItems = $selectedItems;       			
			//debug($this->selectedItems);
			//$thisLevelLinks identifies an individual levels links, starting with top level links here, and then 
			//
			//allLinks is actually alllinks, child active and inactive alike.

			// see below for param exmple data
			// $thisLevelLinks is a simple zero indexed array of top-level links see below for example
			// $selectedItems is a zero indexed array of associative model array see belot.
			// pr($thisLevelLinks);
			// debug($this->selectedItems);
			// return '';

            return $this->getUL($thisLevelLinks, true);
        }



        private function getUL($thisLevelLinks, $selected ) {
			//simple 1/2 of a recursive pair. Creates the ul and iterates over the links, calling getLiAndChildren()
			//$this->level is the level of child descendant depth, $thisLevelLinks is set by the api call AND getLiAndChildren() based on inAncestry($pageId)
            $this->level++;			
	            $result = '<ul class = " '.($selected == true ? " open " : " closed ").' level_'.$this->level.'">'."\n";			
	            foreach( $thisLevelLinks as $arrayKey => $label ) {
	                $result .= $this->getLiAndChildren($arrayKey, $label)."\n";
	            }
	            $result .= '</ul>'."\n";           
            $this->level--;
            return $result;
        }

		/*
			private function getLiAndChildren($key, $value, $allLinks)
			creates 
				<li>
					<a href ="/pages/'.$key.'/" >
						<span class = " {active or nothing} navLink" >Label Value</span>
					</a>
					<ul>
						call $this->getUL($allLinks[$key], $allLinks,$t);
					</ul>					
				</li>
				
					//allLinks is alllinks, child active and inactive alike.
					// would be cool to extend that inAncestry() function to the dashboardNav.
					// this also produces the question....how do you get ALL the links?
					// what if you wanted to produce a list of ALL links, tag them with ids and classes, and have them open & close based on javascript?

					// there is no loop here...you are just checking if this
					// $key = a numerical array key. Page probably Page Id.   This is an issue if you want to decouple from the pages model/controller and
					// use with multiple model/controller sets.
				
		*/

		var $Rlimit = 10;
		var $R = 0;

        private function getLiAndChildren($key, $label) {
			// called iteratively.  checks children and calls the iterator getUL()
			//opens li tag. determines if it's selected, then tags the span with active or '', prints value of 
            $selected = $this->inAncestry($key);
            $str = '<li>'."\n".'<a href ="/pages/'.$key.'/" ><span class = "'.($selected == true ? "active " : "").'navLink" >'.$label.'</span></a>'."\n";
            if(array_key_exists($key, $this->allLinks)) {	
				$str .= $this->getUL($this->allLinks[$key], $selected == true)."\n";					
            }
            $str .= '</li>'."\n";
            return $str;
        }




        private function inAncestry($g){
			// is $g a page id
			// compares $g against the Model.id 
			// $this->selectedItems is a zero-indexed flat array of decendancy they have parent_ids  
			// this function is called once for every single link to check if the id of that link is in this array
            foreach ($this->selectedItems as $key => $value) {       
				if (!empty($value['Navlink'])) {
	                if(($value['Navlink']['id']*1)==$g){
			//			debug($value);
	                    return true;
	                }
				}else{
					//debug($value);
					return false;
				}
            }
            return false;
        }


    }

/*

//--------------------------------------------------------------------------------------------------------------------------------------------
	$thisLevelLinks  is set once on api call and additionally on iteration if inAncestry()
	
	
	$thisLevelLinks = Array (
	    [1] => Home
	    [6] => Getting Ready To Write
	    [24] => Developing Your Ideas
	    [12] => Revising Your Work
	    [17] => Editing and Polishing
	)


	$thisLevelLinks = Array
	(
	    [163] => new Field
	    [165] => add new page test1
	    [166] => add new page test4
	)

	etc 

//--------------------------------------------------------------------------------------------------------------------------------------------



	$allLinks = Array(
			    [0] => Array
			        (
			            [1] => Home
			            [6] => Getting Ready To Write
			            [24] => Developing Your Ideas
			            [12] => Revising Your Work
			            [17] => Editing and Polishing
			        )

			    [7] => Array
			        (
			            [40] => Talk with Students
			            [32] => Class Notes
			            [39] => Review Readings
			            [41] => Key Words & Ideas
			            [42] => Review ideas
			            [43] => Next Step
			        )

			    [1] => Array
			        (
			            [163] => new Field
			            [165] => add new page test1
			            [166] => add new page test4
			        )

			    [6] => Array
			        (
			            [167] => Adding another new page 2
			            [10] => Developing a Thesis
			            [51] => Mapping Your Ideas
			            [164] => testpage2
			        )
				etc...
	)
	
	//--------------------------------------------------------------------------------------------------------------------------------------------	
	
	
	
	$selectedItems = Array
			(
			    [0] => Array
			        (
			            [Page] => Array
			                (
			                    [id] => 1
			                    [title] => Home
			                    [layout] => animation
			                    [file] => ESLWOW_Row005.swf
			                    [copy] => You have to write a paper for your college class? Well then, let's get started. Click on "Getting Ready to Write,” on the left.
			                    [link_label] => Home
			                    [parent_id] => 0
			                    [lft] => 1
			                    [rght] => 22
			                    [in_nav] => 1
			                    [in_next] => 1
			                )

			        )

			    [1] => Array
			        (
			            [Page] => Array
			                (
			                    [id] => 165
			                    [title] => add new page test1
			                    [layout] => video
			                    [file] => Welcome-Final.flv
			                    [copy] => Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			                    [link_label] => add new page test1
			                    [parent_id] => 1
			                    [lft] => 18
			                    [rght] => 19
			                    [in_nav] => 1
			                    [in_next] => 1
			                )

			        )

			)
	
	
	
	
	
	
	
	
	
	
	
	
*/

?>