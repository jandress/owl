# Bash script to get filename details | 16-06-2011 | r3dux
# Usage: fileParts <extension-with-no-dot> i.e. fileParts cpp
 
# For each file with the given extension in the current directory...
for file in ./*.$1; do
 
	# If a file exists with a given extension...
	if [ -e "$file" ]; then
 
		fullPath=$(readlink -f "$file")
		echo "Full path and filename          is: " $fullPath
 
		echo "Relative path and filename      is: " $file
 
		fileWithoutPath=$(basename $file)
		echo "Filename only                   is: " $fileWithoutPath
 
		extension=${fileWithoutPath##*.}
		echo "File extension only             is: " $extension
 
		fileWithoutExtension=${fileWithoutPath%.*}
		echo "Filename only without extension is: " $fileWithoutExtension
 
		echo
	else
		echo "No files of type $1 found!"
		exit 0
 
	fi # End of if file exists condition				
 
done # End of for each file loop