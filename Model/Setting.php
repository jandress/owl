<?php
App::uses('AppModel', 'Model');
/**
 * Setting Model
 *
 */
class Setting extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'default_layout';
	public $layout_options = array(
		'fluid'=>'fluid',
		'standard'=>'standard'					
	);
}
