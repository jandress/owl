all these aliases are a fix for inconsistent behaviour among containable model associations with plugin models. (as well as models associated with the polymorphic behavior )

ugly or not, they haven't become a problem worth fixing, so we're keeping them until they do become problematic. 

