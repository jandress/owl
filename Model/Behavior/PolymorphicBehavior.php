<?php
/* SVN FILE: $Id: polymorphic.php 1375 2009-08-03 09:05:08Z AD7six $ */

/**
 * Polymorphic Behavior.
 *
 * Allow the model to be associated with any other model object
 *
 * PHP versions 4 and 5
 *
 * Copyright (c) 2008, Andy Dawson
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright (c) 2008, Andy Dawson
 * @link          www.ad7six.com
 * @package       base
 * @subpackage    base.models.behaviors
 * @since         v 0.1
 * @version       $Revision: 1375 $
 * @modifiedby    $LastChangedBy: AD7six $
 * @lastmodified  $Date: 2009-08-03 09:05:08 +0000 (Mon, 03 Aug 2009) $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * PolymorphicBehavior class
 *
 * @uses          ModelBehavior
 * @package       base
 * @subpackage    base.models.behaviors
 */
class PolymorphicBehavior extends ModelBehavior {

/**
 * defaultSettings property
 *
 * @var array
 * @access protected
 */
	protected $_defaultSettings = array(
		'modelField' => 'class',
		'foreignKey' => 'foreign_id'
	);

/**
 * setup method
 *
 * @param mixed $Model
 * @param array $config
 * @return void
 * @access public
 */
	public function setup(Model $Model, $settings = array()) {
		// create local settings. just modelField and foreignKey.
		if (!isset($this->settings[$Model->alias])) {
			$this->settings[$Model->alias] = $this->_defaultSettings;
		}
		$this->settings[$Model->alias] = array_merge($this->settings[$Model->alias], (array) $settings);
	}


/**
 * afterFind method
 *
 * @param mixed $Model
 * @param mixed $results
 * @param bool $primary
 * @access public
 * @return void
 */

	public function afterFind(Model $Model, $results, $primary = false) {
		//debug('');
		//debug($Model->alias);
		extract($this->settings[$Model->alias]);
		/*
			unpacks $_defaultSettings as local vars $modelField, $foreignKey.
					$this->settings[$Model->alias] = array('modelField' => 'class','foreignKey' => 'foreign_id'); 
			// (same as $this->_defaultSettings )
			// $Model->alias is the model's alias. duh. in this case it starts out as Quizslide.
						
			$models get a list of possibly associated models. // this does not return plugin models unless aliases to their models have been added to app/Model
			
		*/

		if (App::import('Vendor', 'MiCache')) {
			$models = MiCache::mi('models');
		} else {
			$models = App::objects('Model');
		}

		// if this model is not included in the results, return;
		if(empty($results[0][$Model->alias])){
			return;
		}
		//$modelField = class //$foreignKey = foreign_id
		$thisModelObject = $results[0][$Model->alias]; // starts out as 'Quizslide'
		if (
			$primary 
			&& isset($thisModelObject[$modelField]) 
			&& isset($thisModelObject[$foreignKey]) 
			&& $Model->recursive > 0
		) {
			// $results is a zero indexed array of this Model and its associations.  It starts out with just the hard coded associations in place.
			// $Model is the whole model object. (object(Quizslide) {})  This is before the dynamic associations have been assigned.
			// loop over all the results, and bind the models 
			foreach ($results as $key => $result) {
				$associated = array();
				/* 
				
					$result = 
					
					array(
						'Quizslide' => array(
							'id' => '84',
							'class' => 'Writtenanswer',
							'foreign_id'= > '2',
							'quiz_id' => '17',
							'name' => 'Rewrite and Edit',
							'order' => '0'
						),
						'Quiz' => array(
							'id' => '17',
							'name' => 'Try It Out - 20 Most Common Errors',
							'Description' => 'Read the passage that follows from an essay on the Salem witch trials. It contains some of the errors you have just learned about from the.'
						)
					)
				
				
				
					$result[$Model->alias][$modelField]  should be the alias of the associated model field. in this case Writtenanswer (that's what comes out of the class field.)
					so in this case what we are dealing with here is 
					
					$model = $result[Quizslide][class] = 'Writtenanswer'
										
					(I think) $model should be $dynamicallyAssociatedModel. = Writtenanswer
				*/
				$model = Inflector::classify($result[$Model->alias][$modelField]);
				$foreignId = $result[$Model->alias][$foreignKey];
				if ($model && $foreignId && in_array($model, $models)) {
					$result = $result[$Model->alias];
					if (!isset($Model->$model)) {
						$belongTo =  array(
							'conditions' => array($Model->alias . '.' . $modelField => $model),
							'foreignKey' => $foreignKey
						);
						$Model->bindModel(array('belongsTo' => array(
							$model => $belongTo
						)));
					}
					$conditions = array($model . '.' . $Model->$model->primaryKey => $result[$foreignKey]);
					$recursive = -1;
					$associated = $Model->$model->find('first', compact('conditions', 'recursive'));					
				//	debug($Model);
					// //debug($Model->$model);
					// here is the problem. on the LAMP server, $Model->$model is getting set to AppController instead of Writtenanswer 
					//(EVEN THOUGH) AppModel['alias'] IS GETTING SET TO THE ALIAS OF OUR POLYMORPHIC MODEL.
										
					// our polymorphic model.					
					// debug($Model); //  (object(Quizslide) {})
					// debug($Model->$model); object(CagedActivity)
					$name = $Model->$model->display($result[$foreignKey]);
					$associated[$model]['display_field'] = $name?$name:'*missing*';
					$results[$key][$model] = $associated[$model];					
				}
			}
		} elseif(isset($results[$Model->alias][$modelField])) {
			//debug($Model->alias.' return');
			$associated = array();
			$model = Inflector::classify($result[$Model->alias][$modelField]);
			$foreignId = $results[$Model->alias][$foreignKey];
			if ($model && $foreignId) {
				$result = $results[$Model->alias];
				if (!isset($Model->$model)) {
					$Model->bindModel(array('belongsTo' => array(
						$model => array(
							'conditions' => array($Model->alias . '.' . $modelField => $model),
							'foreignKey' => $foreignKey
						)
					)));
				}
				$conditions = array($model . '.' . $Model->$model->primaryKey => $result[$foreignKey]);
				$recursive = -1;
				$associated = $Model->$model->find('first', compact('conditions', 'recursive'));
				//debug($Model);
				////debug($associated );
				$name = $this->display($result[$foreignKey]);
				$associated[$model]['display_field'] = $name?$name:'*missing*';
				$results[$model] = $associated[$model];
				//---				
			}		
		}
		if(!empty($results[$Model->alias])){
			$model = $Model->alias;			
			//one result
			if(!empty($Model->viewAction)){
				$results[$Model->alias]['viewAction'] = $Model->viewAction;
				$results[$Model->alias]['viewActionFull'] = $Model->viewAction.$results[$model]['id'];
			}
			if(!empty($Model->editAction)){
				$results[$Model->alias]['editAction'] = $Model->editAction;
				$results[$Model->alias]['editActionFull'] = $Model->editAction.$results[$model]['id'];
			}		
			//$results['content'] = $results[$Model->alias];								
		}elseif(!empty($results[0][$Model->alias])) {
			//debug($Model->alias.' return');
			// array of results
			$model = $Model->alias;
			foreach ($results as $key => $value) {
				//one result
				if(!empty($Model->viewAction)){
					//$results[$key][$Model->alias]['viewAction'] = $Model->viewAction.$value[$model]['id'];
					$results[$key][$Model->alias]['viewAction'] = $Model->viewAction;
					$results[$key][$Model->alias]['viewActionFull'] = $Model->viewAction.$value[$model]['id'];
				}
				if(!empty($Model->editAction)){
					//$results[$key][$Model->alias]['editAction'] = $Model->editAction.$value[$model]['id'];					
					$results[$key][$Model->alias]['editAction'] = $Model->editAction;
					$results[$key][$Model->alias]['editActionFull'] = $Model->editAction.$value[$model]['id'];					
				}
			}
		}
		return $results;
	}

/**
 * display method
 *
 * Fall back. Assumes that find list is setup such that it returns users real names
 *
 * @param mixed $id
 * @return string
 * @access public
 */

	public function display($Model, $id = null) {
		if (!$id) {
			if (!$Model->id) {
				return false;
			}
			$id = $Model->id;
		}
		// ////debug($Model->alias . '.' . $Model->primaryKey); //'Datatypea.id' for instance
		return current($Model->find('list', array('conditions' => array($Model->alias . '.' . $Model->primaryKey => $id))));
	}
}
?>