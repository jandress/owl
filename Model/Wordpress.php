<?php
App::uses('AppModel', 'Model');
/**
 * Avatar Model
 *
 */
class Wordpress extends AppModel {

/**
 * Display field
 *
 * @var string
 */
 
	public $useDbConfig = "wordpress";
	public $displayField = 'name';
	public $useTable = 'wp_posts';
	public $avatarDirectory = '/owls';
	
}
