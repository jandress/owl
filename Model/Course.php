<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');
App::uses('String', 'Utility');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */



class Course extends AppModel {

	// var $actsAs = array('Search.Searchable','Containable');

	var $actsAs =array("Containable");


	// public $validate = array(
  //        'name' => 'required'
  //    );




	public $hasAndBelongsToMany = array(
			 'User' =>
					 array(
							 'className' => 'User',
							 'joinTable' => 'courses_users',
							 'foreignKey' => 'course_id',
							 'associationForeignKey' => 'user_id',
					 )
	 );


	 public $hasMany = array(
		 'Courseevent' => array(
			 'className' => 'CourseEvent',
			 'foreignKey' => 'course_id',
			 'conditions' => '',
			 'fields' => '',
			 'order' => ''
		 ),
	 );

	 public $belongsTo = array(
		 'Avatar' => array(
			 'className' => 'Avatar',
			 'foreignKey' => 'avatar_id',
			 'conditions' => '',
			 'fields' => '',
			 'order' => ''
		 ),
		 'Landingpage' => array(
			 'className' => 'Landingpage',
			 'foreignKey' => 'landingpage_id',
			 'conditions' => '',
			 'fields' => '',
			 'order' => ''
		 ),
		 'Navlink'	=> array(
					 'className' => 'Navlink',
					 'foreignKey' => 'navlink_id'
		 )
	 );


	// 'user_id' => '4',
 	// 'course_id' => '1'

	 public function saveNew($formData,$user){
		 $formData['Course']['navlink_id'] = 7;
		 $b = $this->create($formData['Course']);
		 // debug($b);
		 // debug();
		 $saved = $this->save();
		 if($saved){
			 	$boundModel = ClassRegistry::init("CoursesUser");
				$boundModel->create(array('CoursesUser'=>array(
						'user_id'=> 4,
						'course_id'=> $saved['Course']['id'],
				)));
				$s = $boundModel->save();
				if($s){
						$saved['Course']['CoursesUser']=$s['CoursesUser'];
				}

				debug($saved);
				debug($s);
				exit;
				return $saved;
		 }




	 }





}
