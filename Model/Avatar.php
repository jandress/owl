<?php
App::uses('AppModel', 'Model');
/**
 * Avatar Model
 *
 */
class Avatar extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $avatarDirectory = '/img/icons';

	//ugh come back and fix this.  file util exists as a controller.   need to change it or
	function getimagesAsOptions($owlDirContents){
		$ar= array();
		foreach ($owlDirContents as $key => $value) {
		$n = explode('badge_med_',$value);
		if(count($n)>1){
			$g = explode('.png',$n[1]);
			if(count($g)>1){
				$ar[$g[0]]=$value;
			}
		}
	}
		return $ar;
	}

}
