<?php
App::uses('AppModel', 'Model');
/**
 * Avatar Model
 *
 */
class Chord extends AppModel {


    public $chord_list = array(
        array('key' => "c", "name"=>"C", "structure" =>  array("x","3","2","0","1","0"))
        , array('key' => "a", "name"=>"A", "structure" =>  array("x","0","2","2","2","0"))
        , array('key' => "g", "name"=>"G", "structure" =>  array("3","2","0","0","0","3"))
        , array('key' => "e", "name"=>"E", "structure" =>  array("0","2","2","1","0","0"))
        , array('key' => "d", "name"=>"D", "structure" =>  array("x","x","0","2","3","2"))
        , array('key' => "f", "name"=>"F", "structure" =>  array("1","3","3","2","1","1"))

        , array('key' => "b", "name"=>"B", "structure" =>  array("x","2","4","4","4","2"))



        , array('key' => "a_minor", "name"=>"Am", "structure" =>  array("x","0","2","2","1","x"))
        , array('key' => "b_minor", "name"=>"Bm", "structure" =>  array("x","2","4","4","3","2"))
        , array('key' => "c_minor", "name"=>"Cm", "structure" =>  array("x","3","5","5","4","3"))
        , array('key' => "d_minor", "name"=>"Dm", "structure" =>  array("x","x","0","2","3","1"))
        , array('key' => "e_minor", "name"=>"Em", "structure" =>  array("0","2","2","0","0","0"))
        , array('key' => "f_minor", "name"=>"Fm", "structure" =>  array("1","3","3","1","1","1"))



        , array('key' => "a_flat", "name"=>"A&#9837;", "structure" =>  array("x","1","3","3","3","1"))
        , array('key' => "a_6", "name"=>"A6", "structure" =>  array("x","0","2","2","2","2"))
        , array('key' => "a_7", "name"=>"A7", "structure" =>  array("x","0","2","2","2","3"))        
        , array('key' => "a_9", "name"=>"A9", "structure" =>  array("x","0","2","4","2","4"))
        , array('key' => "a_m_6", "name"=>"Am6", "structure" =>  array("x","0","2","2","1","2"))
        , array('key' => "a_m_7", "name"=>"Am7", "structure" =>  array("x","0","2","2","1","2"))
        , array('key' => "a_major_7", "name"=>"AMaj7", "structure" =>  array("x","0","2","1","2","0"))
        , array('key' => "a_dim", "name"=>"ADim", "structure" =>  array("x","x","1","2","1","2"))
        , array('key' => "a_dim", "name"=>"ADim", "structure" =>  array("x","x","1","2","1","2"))
        , array('key' => "a_plus", "name"=>"A+", "structure" =>  array("x","0","3","2","2","1"))

        , array('key' => "a_sus", "name"=>"A Sus", "structure" =>  array("x","0","3","2","2","1"))


        , array('key' => "b", "name"=>"B", "structure" =>  array("x","2","4","4","4","2"))
        , array('key' => "b6", "name"=>"B6", "structure" =>  array("2","2","4","4","4","4"))
        , array('key' => "b7", "name"=>"B7", "structure" =>  array("x","2","1","2","0","2"))
        , array('key' => "b9", "name"=>"B9", "structure" =>  array("x","2","1","2","2","2"))
        , array('key' => "b_minor_6", "name"=>"Bm6", "structure" =>  array("x","x","4","4","3","4"))
        , array('key' => "b_major_7", "name"=>"BMaj7", "structure" =>  array("x","2","4","3","4","x"))
        , array('key' => "bdim", "name"=>"BDim", "structure" =>  array("x","x","0","1","0","1"))
        , array('key' => "bdim", "name"=>"BDim", "structure" =>  array("x","x","0","1","0","1"))
        , array('key' => "b_plus", "name"=>"B+", "structure" =>  array("x","x","3","2","2","1"))
        , array('key' => "b_sus", "name"=>"Bsus", "structure" =>  array("x","x","3","3","4","1"))

        , array('key' => "b_flat", "name"=>"B&#9837;", "structure" =>  array("x","1","3","3","3","1"))
        , array('key' => "b_flat_minor", "name"=>"B&#9837;m", "structure" =>  array("x","1","3","3","2","1"))
        , array('key' => "b_flat_6", "name"=>"B&#9837;6", "structure" =>  array("x","x","3","3","3","3"))
        , array('key' => "b_flat_7", "name"=>"B&#9837;7", "structure" =>  array("x","x","1","1","1","2"))
        , array('key' => "b_flat_maj_7", "name"=>"B&#9837;Maj7", "structure" =>  array("x","1","3","2","3","x"))
        , array('key' => "b_flat_diminished", "name"=>"B&#9837;Dim", "structure" =>  array("x","x","2","3","2","3"))

        , array('key' => "b_flat_plus", "name"=>"B&#9837;+", "structure" =>  array("x","x","x","3","3","2"))
        , array('key' => "b_flat_sus", "name"=>"B&#9837;Sus", "structure" =>  array("x","x","3","3","4","1"))





        , array('key' => "c_sharp_minor", "name"=>"C&#x266f;m", "structure" => array("x","x","2","1","2","0"))
        , array('key' => "c_sharp_minor", "name"=>"C&#x266f;m", "structure" => array("x","x","2","1","2","0"))
        , array('key' => "c_6", "name"=>"C6", "structure" => array("x","x","2","2","1","3"))
        , array('key' => "c_7", "name"=>"C7", "structure" => array("x","3","2","3","1","0"))        
        , array('key' => "c_9", "name"=>"C9", "structure" => array("x","3","2","3","3","3"))                
        , array('key' => "c_minor_6", "name"=>"Cm6", "structure" => array("x","x","`1","2","1","3"))                
        , array('key' => "c_minor_7", "name"=>"Cm7", "structure" => array("x","x","1","3","1","3"))                
        , array('key' => "c_major_7", "name"=>"CMaj7", "structure" => array("x","3","2","0","0","0"))                
        , array('key' => "c_diminished", "name"=>"Cdim", "structure" => array("x","x","1","2","1", "2"))                
        , array('key' => "c_plus", "name"=>"C+", "structure" => array("x","x","2","1","1", "0"))                
        , array('key' => "c_sus", "name"=>"Csus", "structure" => array("x","x","3","0","1", "3"))                








        , array('key' => "d_6", "name"=>"D6", "structure" => array("x","0","0","2","0","2"))
        , array('key' => "d_flat_6", "name"=>"D&#9837;6", "structure" => array("x","x","3","3","2","4"))
        , array('key' => "d_flat_7", "name"=>"D&#9837;7", "structure" => array("x","x","3","4","2","4"))
        , array('key' => "d_flat_9", "name"=>"D&#9837;7", "structure" => array("x","4","3","4","4","4"))
        , array('key' => "c_sharp_minor_6", "name"=>"C&#x266f;m6", "structure" => array("x","x","2","3","2","4"))
        , array('key' => "c_sharp_minor_7", "name"=>"C&#x266f;m7", "structure" => array("x","x","2","4","2","4"))
        , array('key' => "d_flat_maj_7", "name"=>"D&#9837;Maj7", "structure" => array("x","4","3","1","1","1"))        
        , array('key' => "c_sharp_dim", "name"=>"C&#x266f;m7", "structure" => array("x","x","2","3","2","3"))
        , array('key' => "d_flat_plus", "name"=>"D&#9837;+", "structure" => array("x","x","3","2","2","1"))        
        , array('key' => "d_flat_sus", "name"=>"D&#9837;sus", "structure" => array("x","x","3","3","4","1"))        
        , array('key' => "d_7", "name"=>"D7", "structure" => array("x","0","0","2","1","2"))
        , array('key' => "d_9", "name"=>"D9", "structure" => array("2","0","0","2","1","0"))
        , array('key' => "d_minor_6", "name"=>"Dm6", "structure" => array("x","x","0","2","0","1"))
        , array('key' => "d_major_7", "name"=>"Dmaj7", "structure" => array("x","x","0","2","2","2"))
        , array('key' => "d_dim", "name"=>"Ddim", "structure" => array("x","x","0","1","0","1"))
        , array('key' => "d_plus", "name"=>"D+", "structure" => array("x","x","0","2","2","1"))
        , array('key' => "d_sus", "name"=>"Dus", "structure" => array("x","x","0","2","3","3"))
        , array('key' => "d_flat", "name"=>"D&#9837;", "structure" => array("x","x","3","1","2","1"))
        // C ----------------

        , array('key' => "e_diminished_7", "name"=>"Edim7", "structure" =>  array("0","7","6","7","8","0"))
        , array('key' => "e_major_9", "name"=>"Emaj9", "structure" =>  array("0","2","4","1","4","0"))
        , array('key' => "a_add9_add_sharp_11", "name"=>"Aadd9add#11", "structure" =>  array("x","0","2","4","4","0"))
        , array('key' => "g_major_9", "name"=>"gMaj9", "structure" =>  array("3","x","0","2","0","2"))
        , array('key' => "f_major_9_c", "name"=>"FMaj9/C", "structure" =>  array("x","3","3","0","1","0"))
        , array('key' => "e_6", "name"=>"E6", "structure" =>  array("0","2","2","1","2","0"))
        , array('key' => "e_7", "name"=>"E7", "structure" =>  array("0","2","2","1","3","0"))
        , array('key' => "e_9", "name"=>"E9", "structure" =>  array("0","2","0","1","0","2"))
        , array('key' => "e_minor_6", "name"=>"Em6", "structure" =>  array("0","2","2","0","2","0"))
        , array('key' => "e_minor_7", "name"=>"Em7", "structure" =>  array("0","2","0","0","0","0"))
        , array('key' => "e_major_7", "name"=>"Emaj7", "structure" =>  array("0","2","1","1","0","0"))
        , array('key' => "e_diminished", "name"=>"Edim", "structure" =>  array("x","x","2","3","2","3"))
        , array('key' => "e_augmented", "name"=>"E+", "structure" =>  array("x","x","2","1","1","0"))
        , array('key' => "e_suspended", "name"=>"Esus", "structure" =>  array("0","2","2","2","0","0"))
        , array('key' => "e_flat", "name"=>"E&#9837;", "structure" =>  array("x","x","3","1","2","1"))
        , array('key' => "e_flat_minor", "name"=>"E&#9837;m", "structure" =>  array("x","x","4","3","4","2"))
        , array('key' => "e_flat_6", "name"=>"E&#9837;6", "structure" =>  array("x","x","1","3","1","3"))
        , array('key' => "e_flat_7", "name"=>"E&#9837;7", "structure" =>  array("x","x","1","3","2","3"))
        , array('key' => "e_flat_9", "name"=>"E&#9837;9", "structure" =>  array("1","1","1","3","2","1"))
        , array('key' => "e_flat_minor_6", "name"=>"E&#9837;m6", "structure" =>  array("x","x","1","3","1","2"))
        , array('key' => "e_flat_minor_7", "name"=>"E&#9837;m7", "structure" =>  array("x","x","1","3","2","2"))
        , array('key' => "e_flat_maj_7", "name"=>"E&#9837;maj7", "structure" => array("x","x","1","3","3","3"))
        , array('key' => "e_flat_dim", "name"=>"E&#9837;dim", "structure" => array("x","x","1","2","1","2"))
        , array('key' => "e_flat_plus", "name"=>"E&#9837;+", "structure" => array("x","x","1","0","0","3"))
        , array('key' => "e_flat_sus", "name"=>"E&#9837;sus", "structure" => array("x","x","1","3","4","4"))
        , array('key' => "f", "name"=>"F", "structure" => array("1","3","3","2","1","1"))
        , array('key' => "f_minor", "name"=>"Fm", "structure" => array("1","3","3","1","1","1"))



        

    );
    






    
}