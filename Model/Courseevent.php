<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');
App::uses('String', 'Utility');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */

class Courseevent extends AppModel {

	// var $actsAs = array('Search.Searchable','Containable');
	var $actsAs = array('Containable');
	public $hasAndBelongsToMany = array(
		 'User' =>
				array(
						 'className' => 'User',
						 'joinTable' => 'courses_users',
						 'foreignKey' => 'course_id',
						 'associationForeignKey' => 'user_id',
						  'conditions' => '',
				 ),
	 );

	 public $belongsTo = array(
		 'Course' => array(
			 'className' => 'Course',
			 'foreignKey' => 'course_id',
			 'conditions' => '',
			 'fields' => '',
			 'order' => ''
		 )
	 );


	 public function getCourseEventsForUserWithinDates($userId=0, $dateBeginning = '2020-08-08 00:00:00.000000', $dateEnd = '2999-08-08 00:00:00.000000'){
		 $y = ClassRegistry::init("CourseeventsUsers")->find('list', array('fields'=>array("courseevent_id"),'conditions'=>array('user_id'=>$userId)));
 	 	 $this->contain();
		 $t = array('Courseevent.id'=>$y,'Courseevent.datetime BETWEEN ? AND ?' => array($dateBeginning, $dateEnd));
		 $g = $this->find('all',array('conditions' => $t ));
		 $t = array();
		 	// break out the year, month, day and time as separate fields from the datetime.
		 foreach ($g as $key => $value) {
			 $b = $value['Courseevent'];
			 $n =  explode ( " ", $value['Courseevent']['datetime']);
			 $o = explode ( "-", $n[0]);
			 $b['year'] = $o['0'];
			 $b['month']= $o['1'];
			 $b['day']= $o['2'];
			 $b['time'] = $n[1];
			 array_push($t, $b);
		 }
		 return $t;
	 }






}
