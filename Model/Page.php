<?php
App::uses('AppModel', 'Model');
/**
 * Page Model
 *
 * @property Post $Post
 */
class Page extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $actsAs = array('Polymorphic');
	public $useTable = false;
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		// 'Post' => array(
		// 	'className' => 'Post',
		// 	'joinTable' => 'pages_posts',
		// 	'foreignKey' => 'page_id',
		// 	'associationForeignKey' => 'page_id',
		// 	'unique' => 'keepExisting',
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => '',
		// 	'limit' => '',
		// 	'offset' => '',
		// 	'finderQuery' => '',
		// 	'deleteQuery' => '',
		// 	'insertQuery' => ''
		// ),
		// 'Contentmodule' => array(
		// 	'className' => 'Contentmodule',
		// 	'joinTable' => 'pages_contentmodules',
		// 	'foreignKey' => 'page_id',
		// 	'associationForeignKey' => 'contentmodule_id',
		// 	'unique' => 'keepExisting',
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => '',
		// 	'limit' => '',
		// 	'offset' => '',
		// 	'finderQuery' => '',
		// 	'deleteQuery' => '',
		// 	'insertQuery' => ''
		// )
	);
	
	

}
