<?php
	App::uses('AppModel', 'Model');
	class Dashboard extends AppModel {

		var $useTable = false;


		var $user_nav_items = array(

		);

		var $administrator_nav_items = array(

			
			array(
				'label'=>'User Page',
				'url' => 'u',
			),
			array(
				'label'=>'Stubs',
				'url' => 'stub',
			),
			array(
				'label'=>'Practice Sesssion',
				'url' => 'practicesessions',
			),
			array(
				'label'=>'Practice Plans',
				'url' => 'practiceplans',
			),	
			array(
				'label'=>'Vocabulary',
				'url' => 'vocab',
			),			
			array(
				'label'=>'Courses',
				'url' => 'courses'
			)			
			,array(
				'label'=>'Activities',
				'url' => 'activities'
			)
			,array(
				'label'=>'Intervals',
				'url' => 'intervals'
			)
		    ,array(
				'label'=>'Posts',
				'url' => 'posts'
			)
		  ,array(
				'label'=>'Users',
				'url' => 'users',
		  ),
			array( 'label'=>'Settings','url' => 'settings'),
			array( 'label'=>'Routes','url' => 'dynamicroutes'),
		);


	private function getAdministratorNavs(){
		return
		array(
			array(
				'label'=>'Dashboard',
				'url' => 'dashboard',
				'child_links' => $this->administrator_nav_items
			)
		);
	}

	private function getStudentNavs(){
		return  $section_items = array(
			array(
				'label'=>'Sitemap'
				,'url' => 'navlinks'
			),
	
			array(
				'label'=>'Forum'
				,'url' => 'stub/forum_listing'
			),			

			array(
				'label'=>'Dashboard',
				'url' => 'dashboard',
			),

			array(
				'label'=>'My Profile'
				,'url' => 'profile'
			),
			array(
				'label'=>'Stubs',
				'url' => 'stub'
			)

		);

	}



	private function getUserNavItems(){
		return $this->getDeveloperNavs();
	}

	function getLeftNavItems($usergroup = "user") {

		$g = array();
		switch($usergroup){
			case( "developers"):
			case( "dev"):
				$g = $this->getDeveloperNavs();
			break;
			case( "admin"):
			case( "administrators"):
				$g = $this->getAdministratorNavs();
			break;
			case( "staff"):
			case( "teachers"):
			case( "teacher"):
			//	$g = $this->getStaffNavs();
			break;
			case( "students"):
			case( "student"):
			default:
				//$g = $this->getUserNavItems();
				$g = $this->getStudentNavs();
			break;
		}
		return $g;
	}



}



