<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');
App::uses('String', 'Utility');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */

class Fpo extends AppModel {
	
	
	
	var $hipsterIpusm = "Aesthetic blue bottle aute nisi. Fugiat bushwick aliqua ethical, twee lo-fi ullamco YOLO art party odd future dolor aute cupidatat direct trade thundercats. Laborum neutra actually, raw denim twee tofu +1. Sunt dreamcatcher proident tousled sartorial +1. Vice brooklyn whatever trust fund dolor. Fap brooklyn fugiat, wes anderson adipisicing enim hashtag labore vice marfa tonx blue bottle vinyl. Occupy retro narwhal, placeat quinoa vegan messenger bag williamsburg tattooed esse. Mixtape dreamcatcher bicycle rights +1 truffaut, twee sartorial vinyl stumptown gluten-free quis aesthetic officia quinoa typewriter. Duis craft beer +1 echo park, salvia anim tofu 8-bit chillwave nesciunt ugh cillum. Duis ethnic est enim, bicycle rights deep v blue bottle pug ullamco viral vice deserunt. Synth +1 bespoke, umami pitchfork chambray mollit irony pork belly. Qui sed marfa, mcsweeney's plaid polaroid eu ad organic nesciunt. Letterpress craft beer cred jean shorts next level, fugiat DIY street art voluptate try-hard tumblr. Trust fund gastropub forage, put a bird on it excepteur odio gluten-free viral pickled ut ethnic lomo VHS.  Delectus lomo cupidatat literally, lo-fi tonx wes anderson vegan art party master cleanse. Umami meggings williamsburg duis typewriter voluptate, pork belly assumenda. Austin vinyl sapiente swag reprehenderit you probably haven't heard of them. Ex assumenda nulla accusamus, dolor bicycle rights veniam non terry richardson. Chillwave fap elit, carles fashion axe mumblecore umami tofu. Quis readymade nihil tempor art party 90's. Mumblecore nulla art party pinterest williamsburg. Cred exercitation in locavore. Sapiente authentic gastropub, leggings minim proident nesciunt wolf literally organic hoodie church-key. Biodiesel eu aliqua, anim banjo incididunt forage kogi carles sriracha nihil trust fund. Pork belly williamsburg magna, odio you probably haven't heard of them adipisicing blue bottle cred. Keytar williamsburg exercitation viral banh mi. Letterpress culpa high life typewriter gentrify ugh. Mlkshk semiotics odd future shoreditch, qui laborum mumblecore aesthetic. Do you need some dummy text? *sigh* Of course you do.  I bet you still use Helvetica too…";
	var $hipsterIpusmNeat =  "Tattooed trust fund keytar gentrify cardigan. Sartorial beard synth, banksy plaid skateboard umami cred ethical occupy. Marfa try-hard small batch you probably haven't heard of them. Locavore tofu fixie meh. Truffaut hashtag brooklyn deep v, semiotics direct trade butcher. Blog portland mumblecore, butcher truffaut hoodie readymade tousled flexitarian cray sartorial polaroid. You probably haven't heard of them tumblr narwhal chillwave hella forage, trust fund viral vinyl craft beer whatever occupy YOLO. Locavore put a bird on it fingerstache yr beard. American apparel stumptown lo-fi next level banh mi, mlkshk fap post-ironic etsy craft beer shoreditch locavore hoodie master cleanse. Tumblr high life typewriter, post-ironic blue bottle pinterest fixie authentic try-hard kale chips carles. Chillwave synth VHS umami hella. Gluten-free art party banh mi plaid blue bottle vinyl. Flannel 90's williamsburg tofu deep v yr. Selvage ethnic salvia odd future, biodiesel beard fanny pack swag wes anderson photo booth seitan next level. 90's scenester banjo, bushwick keytar thundercats street art polaroid blue bottle. Bespoke sustainable ugh high life forage, deep v +1 scenester aesthetic mustache blue bottle mixtape quinoa odd future. Hella small batch wes anderson godard neutra farm-to-table, sriracha vegan. Chillwave DIY craft beer, beard bushwick pop-up four loko. Hashtag blue bottle bushwick cosby sweater twee squid. Pour-over chillwave wes anderson, sustainable gluten-free terry richardson mustache beard +1 marfa odd future mlkshk. Vinyl church-key stumptown, fingerstache 8-bit before they sold out meh. Forage readymade yr portland, actually tumblr church-key put a bird on it. +1 mcsweeney's terry richardson, skateboard DIY 90's single-origin coffee ethnic small batch church-key pork belly. Flannel ethnic hella, synth retro tattooed master cleanse tonx. +1 artisan try-hard jean shorts tonx selfies next level deep v, 3 wolf moon viral. Blue bottle retro umami quinoa hashtag pug mlkshk sriracha fingerstache you probably haven't heard of them four loko disrupt godard. Synth williamsburg pork belly selvage small batch. Banh mi williamsburg thundercats, plaid direct trade intelligentsia yr art party umami master cleanse chambray pug.";


	public $useDbConfig = 'esl_wow';
//	public $useTable = 'quizslides';    
//	public $useTable = 'quiztextslides';    	
	public $useTable = 'dragndrops';    	
	
	public function getFpoTitle(){
		return 'Hello World!';
	}
	
	public function getFpoSubTitle(){
		return String::truncate($this->hipsterIpusm,100,$this->ar);
	}
	//String::truncate($this->defaultIpsum,$trunc
	public function getCopy($trunc = null){
		if(!empty($trunc)){
			$ar = array(
			    'ellipsis' => '...',
			    'exact' => true,
			    'html' => false
			);
			return String::truncate($this->hipsterIpusmNeat,$trunc,$ar);
		}
		return $this->defaultIpsum;
	}	
	
	public function getFpoTouts($number = 10, $trunc = 20){
		$ar = array();
		for ($i=0; $i < $number; $i++) { 
			# code...
			$ar[$i]=$this->getFpoTout($trunc);
		}
		return $ar;
	}

	public function getFpoTout($trunc = 20){
		$ar = array(
			'head'=>'',
			'sub_head'=>'',			
			'link'=>array(
				'label'=>'View Details »',
				'href'=>'./#/',
				'target'=>'_self'				
			),
			'copy'=>$this->getCopy($trunc)
			);
		return $ar;
	}
}
