<?php
App::uses('Branchingslide', 'Model');

/**
 * Branchingslide Test Case
 *
 */
class BranchingslideTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.branchingslide');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Branchingslide = ClassRegistry::init('Branchingslide');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Branchingslide);

		parent::tearDown();
	}

}
