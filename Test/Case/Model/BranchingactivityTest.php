<?php
App::uses('Branchingactivity', 'Model');

/**
 * Branchingactivity Test Case
 *
 */
class BranchingactivityTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.branchingactivity');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Branchingactivity = ClassRegistry::init('Branchingactivity');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Branchingactivity);

		parent::tearDown();
	}

}
