<?php
App::uses('MediaslidePoint', 'Model');

/**
 * MediaslidePoint Test Case
 *
 */
class MediaslidePointTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.mediaslide_point', 'app.mediaslide');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MediaslidePoint = ClassRegistry::init('MediaslidePoint');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MediaslidePoint);

		parent::tearDown();
	}

}
