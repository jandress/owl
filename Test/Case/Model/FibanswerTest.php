<?php
App::uses('Fibanswer', 'Model');

/**
 * Fibanswer Test Case
 *
 */
class FibanswerTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.fibanswer', 'app.fillintheblank');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Fibanswer = ClassRegistry::init('Fibanswer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Fibanswer);

		parent::tearDown();
	}

}
