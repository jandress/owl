<?php
App::uses('BranchingActivitySlide', 'Model');

/**
 * BranchingActivitySlide Test Case
 *
 */
class BranchingActivitySlideTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.branching_activity_slide');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->BranchingActivitySlide = ClassRegistry::init('BranchingActivitySlide');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BranchingActivitySlide);

		parent::tearDown();
	}

}
