<?php
App::uses('Landingpage', 'Model');

/**
 * Landingpage Test Case
 *
 */
class LandingpageTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.landingpage', 'app.page', 'app.post', 'app.pages_post', 'app.contentmodule', 'app.pages_contentmodule');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Landingpage = ClassRegistry::init('Landingpage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Landingpage);

		parent::tearDown();
	}

}
