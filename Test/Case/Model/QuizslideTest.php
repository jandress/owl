<?php
App::uses('Quizslide', 'Model');

/**
 * Quizslide Test Case
 *
 */
class QuizslideTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.quizslide');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Quizslide = ClassRegistry::init('Quizslide');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Quizslide);

		parent::tearDown();
	}

}
