<?php
App::uses('Mediashow', 'Model');

/**
 * Mediashow Test Case
 *
 */
class MediashowTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.mediashow', 'app.mediaslide');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Mediashow = ClassRegistry::init('Mediashow');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Mediashow);

		parent::tearDown();
	}

}
