<?php
App::uses('Mediaslide', 'Model');

/**
 * Mediaslide Test Case
 *
 */
class MediaslideTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.mediaslide', 'app.mediaslide_point');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Mediaslide = ClassRegistry::init('Mediaslide');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Mediaslide);

		parent::tearDown();
	}

}
