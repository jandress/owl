<?php
App::uses('Twistaplot', 'Model');

/**
 * Twistaplot Test Case
 *
 */
class TwistaplotTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.twistaplot');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Twistaplot = ClassRegistry::init('Twistaplot');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Twistaplot);

		parent::tearDown();
	}

}
