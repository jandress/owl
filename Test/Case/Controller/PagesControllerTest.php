<?php
App::uses('PagesController', 'Controller');

/**
 * TestPagesController *
 */
class TestPagesController extends PagesController {
/**
 * Auto render
 *
 * @var boolean
 */
	public $autoRender = false;

/**
 * Redirect action
 *
 * @param mixed $url
 * @param mixed $status
 * @param boolean $exit
 * @return void
 */
	public function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

/**
 * PagesController Test Case
 *
 */
class PagesControllerTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.page', 'app.post', 'app.pages_post', 'app.contentmodule', 'app.pages_contentmodule', 'app.fpo', 'app.dashboard');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Pages = new TestPagesController();
		$this->Pages->constructClasses();
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Pages);

		parent::tearDown();
	}

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {

	}
/**
 * testHome method
 *
 * @return void
 */
	public function testHome() {

	}
/**
 * testDashboard method
 *
 * @return void
 */
	public function testDashboard() {

	}
/**
 * testMyIndex method
 *
 * @return void
 */
	public function testMyIndex() {

	}
/**
 * testView method
 *
 * @return void
 */
	public function testView() {

	}
/**
 * testMyView method
 *
 * @return void
 */
	public function testMyView() {

	}
/**
 * testMyAdd method
 *
 * @return void
 */
	public function testMyAdd() {

	}
/**
 * testMyEdit method
 *
 * @return void
 */
	public function testMyEdit() {

	}
/**
 * testMyDelete method
 *
 * @return void
 */
	public function testMyDelete() {

	}
}
