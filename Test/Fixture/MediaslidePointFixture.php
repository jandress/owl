<?php
/**
 * MediaslidePointFixture
 *
 */
class MediaslidePointFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'name' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'copy' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'mediaslide_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'indexes' => array(),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 1,
			'copy' => 1,
			'mediaslide_id' => 1
		),
	);
}
