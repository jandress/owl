<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');



// class Fileutil
/**
 * Pages Controller
 *
 * @property Page $Page
 * @property SessionComponent $Session
 */
class PagesController extends AppController {

/**
 *  Layout
 *
 * @var string
 */
	//public $layout = 'frontend';

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('Calendar','TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator','HabtmTable','Js');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session','Auth','Session','Fileutil');
	public $uses = array('Courseevent', 'Course','Page','Contentmodule','Twistaplot','Quiz','Fpo','Quizslide','Quiztextslide','Dragndrop','Fpo','Navlink','Wordpress',"Course","User","Mockuser","Navlink");

/**
 * my_index method
 *
 * @return void
 */
	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('tree','testing','display/*','display');
	}

	public function my_index() {
		$this->Page->recursive = 0;
		$this->set('pages', $this->paginate());
		if ($this->request->is('post') || $this->request->is('put')) {
			if($this->Landingpage->save($this->request->data)){
			}
		}else{
		//	$this->data = $this->Landingpage->find('first',array('name'=>'home'));
		}
	}

	public function index(){
		$this->redirect('/my/landingpages/');
	}

	public function theme(){
	    //$data = $this->Navlink->generateTreeList(null, null, null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
		//$this->set('data',$data );
	}

	public function display(){
		$this->layout ="background_only";
		$this->set('element','stubs/'.$this->params['pass'][0]);
	}


	public function testing(){
		//debug($this->Post->find('all')); exit;
		$d = $this->Post->find('all');
		foreach ($d as $key => $value) {
			$d = $this->createWordPressEntryFromPost($value['Post'],array('limit'=>4));
			debug($this->Wordpress->Save($d));
		}
	}


	function createWordPressEntryFromPost($post){
		$g = array();
		//--
		$g['id'] = $post['id'];
		$g['guid']='/?p='.$post['id'];
		$g['post_title'] = $post['title'];
		$g['post_content'] = $post['body'];
		$g['post_excerpt'] = String::truncate($post['title']);
		$g['post_content_filtered'] = $g['pinged'] = $g['to_ping'] = '';
		//--
		$g['post_date'] = $g['post_date_gmt'] = $post['created'];
		$g['post_modified'] = $g['post_modified_gmt'] = $post['modified'];
		//--
		return array('Wordpress' => $g);
	}

	public function tree(){
	    $data = $this->Navlink->generateTreeList(null, null, null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
		$this->set('data',$data );
		$this->layout = 'fluid_one_col';
	}

/**
 * my_view method
 *
 * @param string $id
 * @return void
 */
	public function my_view($id = null) {

		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(__('Invalid %s', __('page')));
		}
	//	pr($this->Page->read(null, $id));
		$this->set('page', $this->Page->read(null, $id));
	}

	public function view($id = null) {

		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(__('Invalid %s', __('page')));
		}
	//	pr($this->Page->read(null, $id));
		$this->set('page', $this->Page->read(null, $id));
	}



	public function emailtest(){
		//~security:  remove log statements from Tickets
		// $to = 'email@jonandress.com';
		// $subject = 'test';
		// $message = 'hi there';
		// $email = new CakeEmail();
		// $email->from(array('noreply@owl.com' => ''));
		// $email->to($to);
		// $email->subject($subject);
		// debug($email->send($message));
		$to = 'email@jonandress.com';
		$subject = 'test subject';
		$message = 'hello this is a test';
		$headers = 'From: webmaster@example.com' . "\r\n" .'Reply-To: nobody@pagekwik.com';
		if( mail($to, $subject, $message, $headers) ){
			echo 'Mail was sent';
		} else {
			echo 'Couldn\'t send the mail';
		}
		exit;
	}


/**
 * my_add method
 *
 * @return void
 */
	public function my_add() {
		$this->title('New Page');
		if ($this->request->is('post')) {
			$this->Page->create();
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('page')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('page')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$this->layout = 'admin';
		//$posts = $this->Page->Post->find('list');
		// $contentmodules = $this->Page->Contentmodule->find('list');
		// $this->set(compact('posts', 'contentmodules'));
	}
public function my_stub(){
	exit;
}



	public function stub($file = ''){
		//  debug($this->layout); exit;
		$y = $this->Courseevent->getCourseEventsForUserWithinDates(4,  '2000-08-08', '2999-08-08');
		//debug($y);
		$this->set('events',$y);
		if (!empty($this->data)) {
			$this->User->save($this->data);
		}
		$g = $this->Fileutil->getDirectoryContents('../View/elements/stubs');
		$this->set('listing', $g);
		$this->set('include', $file);
		$this->set('userId', $this->current_user['User']['id']);
		switch ($file) {
			case '':
					$this->set('listing', $g);
				break;
			case('course_selects'):
				$this->course_selects($g, $file);
			break;
			default:
				// $this->doCSV();
			break;
		}
	}







	public function navs() {
		$this->Navlink->contain();
		$t = $this->Navlink->find("all");
		$n = $t;
		foreach ($n as $key => $value) {
			$nl = $value['Navlink'];
			if ($nl['class'] == 'Url') {
				$n[$key]['Navlink']['url'] = $nl['foreign_id'];
			}else{
				$n[$key]['Navlink']['url'] = '/'.strtolower(Inflector::pluralize($nl["class"])).'/view/'.$nl["foreign_id"];
			}
		}
		debug($n);
		debug($this->Navlink->saveAll($n));
		// exit;
	}



	private function doCSV(){
		// $dirFiles1 = array();
		// $dirFiles = array();
		// $row = 1;
		// if (($handle = fopen("../Config/MOCK_DATA.csv", "r")) !== FALSE) {
		// 	//	mockusers
		// 	$a = array('Mockuser'=>array());
		// 	$row++;
		// 	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		//     $num = count($data);
		//     //	echo "<p> $num fields in line $row: <br /></p>\n";
		//     $row++;
		// 		$b = array('id'=> ($row+5) );
		// 		$b['first_name'] = $data[1];
		// 		$b['last_name'] = $data[2];
		//
		// 		$b['email'] = $data[3];
		//
		// 		$b['ip_address'] = $data[5];
		// 		$st = substr($b['first_name'],0,1);
		// 		$b['username'] = $st.$b['last_name'];
		// 		$b['password'] = "6ec8ffe12d85dffc32a8df11a67a1bf81e4c28ca";
		// 		array_push($a['Mockuser'], $b);
		//   }
			$a = $this->User->find('all');
			foreach ($a as $key => $value) {

					// $u = $this->Mockuser->save();

					// $value['User']['email'] = $value['User']['email_address'];
					// $value['User']['ip_address'] = "127.0.10.1";
					// $t = $this->Mockuser->create($value['User']);
					// $t = $this->Mockuser->save();
					//  debug($t);
					// // // debug($u);
					//  debug($t);




			}
		  // fclose($handle);
		}



			//
			// public function my_new() {
			// 	$this->set('model','Book');
			// 	$this->setDashboardLayout();
			// 	if ($this->request->is('post')) {
			// 		$this->Book->create();
			// 		$t = $this->Book->save($this->request->data);
			// 		if ($t) {
			// 			$this->Session->setFlash(__('The book has been saved'));
			// 			$this->redirect('/my/books/edit/'.$t['Book']['id'].'/');
			// 		} else {
			// 			$this->Session->setFlash(__('The book could not be saved. Please, try again.'));
			// 		}
			// 	} else {}
			// 	$publishers = $this->Book->Publisher->find('list');
			// 	$categories = $this->Book->Category->find('list');
			// 	$this->set(compact('publishers', 'categories'));
			// }
			//




		// $directory_path = str_replace('//','/', $directory_path.'/');
		// debug($directory_path);
		// exit;
		// if ($handle = opendir($directory_path)) {
		// 		while (false !== ($file = readdir($handle))) {
		// 		if ( substr($file,0,1) !='.' && $file !="index.php" ) {
		// 			$g = array(
		// 				'item'=>$file,
		// 				'type'=> is_dir($directory_path.$file) ? 'dir' :strtolower(substr($file, strrpos($file,'.')+1))
		// 			);
		// 			array_push($dirFiles1,$g);
		// 		}
		// 		}
		// 		closedir($handle);
		// }
		// sort($dirFiles1);


	// }




	private function course_selects($g, $file){
			$list = $this->Course->find("list");
			$this->set('model','Course');
			$this->set('list', $list);
			$this->User->Behaviors->attach('Containable');
			$this->User->contain(array('Course'));
			$ar = array();
			foreach ($this->current_user['Course'] as $key => $value) {
				array_push($ar, $value['id']);
			}
			$this->set('selections', $ar);
	}



	private function gdc($directory_path,$sort=false){
			$dirFiles1 = array();
			$dirFiles = array();
			$directory_path = str_replace('//','/',$directory_path.'/');
			if ($handle = opendir($directory_path)) {
					while (false !== ($file = readdir($handle))) {
					if ( substr($file,0,1) !='.' && $file !="index.php" ) {
						$g = array(
							'item'=>$file,
							'type'=> is_dir($directory_path.$file) ? 'dir' :strtolower(substr($file, strrpos($file,'.')+1))
						);
						array_push($dirFiles1,$g);
					}
					}
					closedir($handle);
			}
			sort($dirFiles1);
			return $dirFiles1;
		}




/*

private function gdc($directory_path,$sort=false){
		$dirFiles1 = array();
		$dirFiles = array();
		$directory_path = str_replace('//','/',$directory_path.'/');
		if ($handle = opendir($directory_path)) {
				while (false !== ($file = readdir($handle))) {
				if ( substr($file,0,1) !='.' && $file !="index.php" ) {
					$g = array(
						'item'=>$file,
						'type'=> is_dir($directory_path.$file) ? 'dir' :strtolower(substr($file, strrpos($file,'.')+1))
					);
					array_push($dirFiles1,$g);
				}
				}
				closedir($handle);
		}
		sort($dirFiles1);
		return $dirFiles1;
	}
*/




	private function dodir($directory_path){
		//$directory_path = 'video';
		if ($handle = opendir($directory_path)) {
		    while (false !== ($file = readdir($handle))) {
					$fileExt = end(explode('.', $file));
	 				$t = basename($file, ".".$fileExt);
					if($t!='.' && $t !='..'&& $t !='.DS_Store'){
						$g = $directory_path.'/'.str_replace(' ','_',$t);
						echo $g;
						mkdir($g);
						$newname = $g.'/video.'.$fileExt;
						rename($directory_path.'/'.$file,$newname);
						echo '</br>';
					}
		    }
		    closedir($handle);
		}
	}


	public function admin_nav($val){
		$this->layout = 'ajax';
		$na = $this->Dashboard->getLeftNavItems($val);
		$this->set('left_nav_array', $na);
		$this->set('nav_style',$val );
	}




/**
 * my_edit method
 *
 * @param string $id
 * @return void
 */

	public function my_landing() {
		$this->Page->recursive = 0;
		$this->set('pages', $this->paginate());
		if ($this->request->is('post') || $this->request->is('put')) {
			if($this->Landingpage->save($this->request->data)){
			}
		}else{
			$this->data = $this->Landingpage->find('first',array('name'=>'home'));
		}
	}


		var $layouts_array = array(
			"composite" => "Composite Pages",
			"static" => "Static Pages",
			"existing" => "Landing Pages"
		);


	public function my_edit($id = null) {
		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(__('Invalid %s', __('page')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('page')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('page')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Page->read(null, $id);
		}
		$this->title($this->request->data['Page']['name']);
		// $posts = $this->Page->Post->find('list');
		// $contentmodules = $this->Page->Contentmodule->find('list');
		$this->set('layouts_array',$this->layouts_array);
		//$this->set(compact('posts', 'contentmodules','layouts_array'));
	}


	public function home() {
		$this->layout = $this->getLayout();
	}


/**
 * my_delete method
 *
 * @param string $id
 * @return void
 */
	public function my_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(__('Invalid %s', __('page')));
		}
		if ($this->Page->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('page')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('page')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
}









/*



'Post' => array(
			'id' => '11',
			'title' => 'Assignment Analysis',
			'body' => '<p><strong>Analyzing Your Assignment and Thinking Rhetorically</strong></p>',
			'created' => '2013-03-06 20:17:52',
			'modified' => '2015-04-22 21:09:46',
			'media_file' => '',
			'layout' => '',
			'media_format' => '',
			'quiz_id' => '0'
		)
'Wordpress' => array(
			'ID' => '1',
			'post_author' => '1',
			'post_date' => '2016-01-30 01:49:12',
			'post_date_gmt' => '2016-01-30 01:49:12',
			'post_content' => 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!',
			'post_title' => 'Hello world!',
			'post_excerpt' => '',
			'post_status' => 'publish',
			'comment_status' => 'open',
			'ping_status' => 'open',
			'post_password' => '',
			'post_name' => 'hello-world',
			'to_ping' => '',
			'pinged' => '',
			'post_modified' => '2016-01-30 01:49:12',
			'post_modified_gmt' => '2016-01-30 01:49:12',
			'post_content_filtered' => '',
			'post_parent' => '0',
			'guid' => 'http://wordpress/?p=1',
			'menu_order' => '0',
			'post_type' => 'post',
			'post_mime_type' => '',
			'comment_count' => '1'
		)
*/
