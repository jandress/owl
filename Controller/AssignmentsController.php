<?php
App::uses('AppController', 'Controller');
/**
 * Avatars Controller
 *
 * @property Avatar $Avatar
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AssignmentsController extends AppController {

	public $components = array('Paginator', 'Session','Userassets.Fileutil');

	public function beforeFilter(){
		parent::beforeFilter();
	}

	public function index() {

	}

	// function getimagesAsOptions($owlDirContents){
	// 	$ar= array();
	// 	foreach ($owlDirContents as $key => $value) {
	// 	$n = explode('badge_med_',$value);
	// 	if(count($n)>1){
	// 		$g = explode('.png',$n[1]);
	// 		if(count($g)>1){
	// 			$ar[$g[0]]=$value;
	// 		}
	// 	}
	// 	}
	// 	return $ar;
	// }

	public function view($id = null) {
		// if (!$this->Avatar->exists($id)) {
		// 	throw new NotFoundException(__('Invalid avatar'));
		// }
		// $options = array('conditions' => array('Avatar.' . $this->Avatar->primaryKey => $id));
		// //	debug($this->Avatar->find('first', $options));
		// $this->set('avatar', $this->Avatar->find('first', $options));
    //
		// $this->layout = 'ajax';
		// $this->set( 'filepath' ,$this->Avatar->avatarDirectory );
	}

	public function my_index() {
		
		// $this->Avatar->recursive = 0;
		// $this->set('avatars', $this->Paginator->paginate());
	}


	public function my_view($id = null) {
		// if (!$this->Avatar->exists($id)) {
		// 	throw new NotFoundException(__('Invalid avatar'));
		// }
		// $options = array('conditions' => array('Avatar.' . $this->Avatar->primaryKey => $id));
		// $this->set('avatar', $this->Avatar->find('first', $options));
	}

	public function my_add() {
		// $this->set('fileOptions',$this->getimagesAsOptions($this->Fileutil->getFileListing( '.'.$this->Avatar->avatarDirectory )));
		// if ($this->request->is('post')) {
		// 	$this->Avatar->create();
		// 	if ($this->Avatar->save($this->request->data)) {
		// 		$this->Session->setFlash(__('The avatar has been saved.'));
		// 		return $this->redirect(array('action' => 'index'));
		// 	} else {
		// 		$this->Session->setFlash(__('The avatar could not be saved. Please, try again.'));
		// 	}
		// }
	}


	public function my_edit($id = null) {
		// if (!$this->Avatar->exists($id)) {
		// 	throw new NotFoundException(__('Invalid avatar'));
		// }
		// if ($this->request->is(array('post', 'put'))) {
		// 	if ($this->Avatar->save($this->request->data)) {
		// 		$this->Session->setFlash(__('The avatar has been saved.'));
		// 		return $this->redirect(array('action' => 'index'));
		// 	} else {
		// 		$this->Session->setFlash(__('The avatar could not be saved. Please, try again.'));
		// 	}
		// } else {
		// 	$options = array('conditions' => array('Avatar.' . $this->Avatar->primaryKey => $id));
		// 	$this->data =  $this->request->data = $this->Avatar->find('first', $options);
		// }
		// $g = array(
		// 	'options' => $this->getimagesAsOptions($this->Fileutil->getFileListing('.'.$this->Avatar->avatarDirectory))
		// );
		// $g['value'] = $this->data['Avatar']['name'];
		// $this->set('fileOptions', $g);
	}

	public function my_delete($id = null) {
	// 	$this->Avatar->id = $id;
	// 	if (!$this->Avatar->exists()) {
	// 		throw new NotFoundException(__('Invalid avatar'));
	// 	}
	// 	$this->request->onlyAllow('post', 'delete');
	// 	if ($this->Avatar->delete()) {
	// 		$this->Session->setFlash(__('The avatar has been deleted.'));
	// 	} else {
	// 		$this->Session->setFlash(__('The avatar could not be deleted. Please, try again.'));
	// 	}
	// 	return $this->redirect(array('action' => 'index'));
	 }
}
