<?php
	class UserManagementComponent extends Component {

		var $components = array('Acl');
		var $administrators = 1;
		var $managers = 2;
		var $publishers = 3;
		var $users = 4;


		/*
			ALTER TABLE `users` DROP `id`
			ALTER TABLE `users` ADD `id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST
			///-----------------------------------------------------------------------------------------------------------
			///-----------------------------------------------------------------------------------------------------------
			// $this->UserManagement->generateUsergroups();
			// $this->UserManagement->createAcos();
			///-----------------------------------------------------------------------------------------------------------
			///-----------------------------------------------------------------------------------------------------------
			//
			//assignUserToGroup('','','');
			// $this->UserManagement->createAros();
			// ./Console/cake acl view aro
			// ./Console/cake AclExtras.AclExtras -h
			// ./Console/cake AclExtras.AclExtras aco_sync

			//assignUserToGroup('','','');


		*/

		public function setUpPermissions(){
			/// DONT DELETE YET!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			$this->createAcos();
			$this->Acl->allow( 'administrators', 'User/isAdmin');
			$this->Acl->allow( 'publishers', 'User/isPublisher' , $action = '*');
			//$this->Acl->allow( 'administrators', 'Dashboard','my_browses');
			//$this->Acl->allow( 'publishers', 'Dashboard/my_books' , $action = '*');
		}
		var $groups=array(
			1 => array(
				'alias' => 'administrators'
			),
			2 => array(
				'alias' => 'managers'
			),
			3 => array(
				'alias' => 'publishers'
			),
			4 => array(
				'alias' => 'users'
			),
		);




		function createAros()
		{
			$aro =& $this->Acl->Aro;
			//Here's all of our group info in an array we can iterate through
			$groups = $this->groups;
			//Iterate and create ARO groups
			foreach($groups as $data) {
				//Remember to call create() when saving in loops...
				$aro->create();
				//Save data
				$aro->save($data);
			}
			//Other action logic goes here...
		}
		 function assignUserToGroup($alias, $group ){
				//$aro = $this->Acl->Aro;
				$aro = new Aro();
				$new_user = array(
					'alias' =>$alias, // $this->data['User']['username'],
					'model' => 'User',
					'parent_id' => $group, // 'parent_id' => 4,
				);
				$aro->create();
				$aro->save($new_user);
		}
		public function createUser($alias){
			$this->assignUserToGroup($alias, $this->users);
		}
		public function createAdministrator($alias){
			$this->assignUserToGroup($alias, $this->administrators);//'administrators',$this->administrators  );
		}
		public function createPublisher($alias){
			$this->assignUserToGroup($alias,$this->publishers );
		}

		public function initialize(Controller $controller) {
			// saving the controller reference for later use
			$this->controller = $controller;
		}


		public function grantPermissions(){
			//$this->Acl->check('warriors/Aragorn', 'Weapons');
			//$this->Acl->check('publishers', 'Publishers');
		}


		function generateUsergroups(){
			$aro =& $this->Acl->Aro;
			$groups =$this->groups;
			foreach($groups as $data) {
				$aro->create();
				$aro->save($data);
			}
			exit;
		}




		function createAcos() {}

		// function setup($g,$userAlias="",$d=""){
		// 	$b="<br/>";
		// 	switch ($g) {
		// 		case 3:
		// 			if(($userAlias == "")||($d=="")){
		// 				echo $b."missing params";
		// 				return;
		// 			}
		// 			$this->UserManagement->assignUserToGroup($userAlias, $d);
		// 			// $this->UserManagement->assignUserToGroup("konark",3);
		// 			// $this->UserManagement->assignUserToGroup("mowgli",4);
		// 		break;
		// 		case 4:
		// 			$this->Acl->allow("administrators", "Users/isAdmin");
		// 			$this->Acl->allow("administrators", "Users/isPublisher");
		// 			$this->Acl->allow("publishers", "Users/isPublisher");
		// 		break;
		// 		default:
		// 			echo $b.$b.'1.  ./Console/cake schema create DbAcl'.$b.'2. ./Console/cake AclExtras.AclExtras aco_sync'.$b;
		// 		break;
		// 	}
		// }
		//





	}

?>
