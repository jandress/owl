<?php

App::uses('Helper', 'View');

class TreeNavComponent extends Component {
	
	
	// function __construct() {
	// 	$this->view = ClassRegistry::getObject('view');		
	// }
	
	public function recursiveChildren($array) { 
		$str = '';
	    if (count($array)) { 
	        $str.= "\n<ul>\n"; 
	        foreach ($array as $vals) { 
	            $str.= "<li id=\"".$vals['Navlink']['id']."\">".$vals['Navlink']['id'].' '.$vals['Navlink']['title']; 
	            if (count($vals['ChildNavlink'])) { 
					$str.=$this->recursiveChildren($vals['children']); 
	            } 
	            $str.= "</li>\n"; 
	        } 
	        $str.= "</ul>\n"; 
	    } 
	return $str;
	}
	
	
	
	
}
