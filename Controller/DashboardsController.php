<?php
App::uses('AppController', 'Controller');
/**
 * Settings Controller
 *
 * @property Setting $Setting
 */
class DashboardsController extends AppController {


	 public $helpers = array('Text');
	// public $components = array('Session','Userassets.Fileutil');
	 public $uses = array('User','Setting','Navlink','MessagingSystem.Message','Course');




	public function beforeFilter(){
		parent::beforeFilter();
		// $this->Auth->allow('push');
	}





	public function view() {
		$messages = $this->Message->find("all", array(
			'conditions'=>array(
				'Message.rec_id' => $this->current_user['User']['id']
			)
		));
		$this->set('current_user' , $this->current_user);
		// debug($messages);
		//debug("/View/Dashboards/view/   includes a custon view element for each user role.    found in /view/elements./");
		//debug($this->current_user); exit;
		$this->set('messages', $messages);
	}





}
