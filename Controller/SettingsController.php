<?php
App::uses('AppController', 'Controller');
/**
 * Settings Controller
 *
 * @property Setting $Setting
 */
class SettingsController extends AppController {


	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
	public $components = array('Session','Userassets.Fileutil');
	public $uses = array('Setting','Navlink');




	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('push');
	}

	public function push($reset=null){
		$this->layout = 'ajax';
		if(isset($reset)){
			if($reset=='reset'){
				$this->Setting->find('first');
				$this->Setting->updateAll(array(
				    'pushcode' => '0',
				    'pushdb' => '0'
				));
				$settings = $this->Setting->find('first'); 
				$this->settings = $settings['Setting'];
				$this->set('settings', $this->settings);
			}
		}
	}



	public function admin_theme(){
		$d = $this->Setting->find('first');		
		if ($this->request->is('post') || $this->request->is('put')) {
			$d['Setting']['stylesheet'] = $this->data['Setting']['stylesheet'];
			if($this->Setting->save($d)){
				$this->Session->setFlash(
					__('The %s has been saved', __('setting')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				if(empty($this->isAjax)){
					$this->redirect('/my/theme/');
				}else{
					echo '{"href":"'.'/my/theme/"}';					
				}
				exit;
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('setting')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$this->data = $d;
		$this->set('stylesheet_options',$this->Fileutil->getFileListingForOptions('css/stylesheet'));		
	}



	public function my_index() {
		$id = 1;
		//debug($this->Fileutil->getFileListingForOptions('css/stylesheet')); exit;
		$this->set('layout_options',$this->Setting->layout_options);
		$this->set('stylesheet_options',$this->Fileutil->getFileListingForOptions('css/stylesheet'));		

		$this->Setting->id = 1;
		$this->set('navlink_options', $this->Navlink->find('list'));
		if (!$this->Setting->exists()) {
			throw new NotFoundException(__('Invalid %s', __('setting')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Setting->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('setting')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('setting')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Setting->read(null, $id);
		}
	}

}
