<?php
App::uses('AppController', 'Controller');


class CoursesController extends AppController {

		public $components = array('Paginator', 'Session','Userassets.Fileutil');
	  	public $uses = array('UserManagement.User','Course','CoursesUser','Avatar','Landingpage');

		var $classOptions=array(
			'options'=>array(
				'Post'=>'Post'
				, 'Quiz'=>'Quiz'
				,'Url'=>'Url'
				,'Landingpage'=>'Landing Page'
				,'Branchingslide'=>'Branching Activity'

			),
			'label'=>'Content Type'
		);


		public function beforeFilter(){
			parent::beforeFilter();
		}

		public function index(){}


		public function my_add(){
			if ($this->request->is('post')) {
				// CoursesUser
				$b = $this->request->data;
				// $b['Course']
				$p = $this->Course->create($this->request->data);
				$this->Course->saveNew($this->request->data,$this->current_user['User']);
			   debug($this->request->data);
				 exit;
			}
			$landingpage_list = $this->Landingpage->find('list');
			$landingpage_list[0] = "--";
			ksort($landingpage_list);
			$landingpage_options = array('options'=>$landingpage_list);
			$avatar_options = array('options'=>$this->Avatar->getimagesAsOptions($this->Fileutil->getFileListing( '.'.$this->Avatar->avatarDirectory )));
			$this->set(compact('avatar_options','landingpage_options'));
		}

		private function setfidData($g){
			$conditions = array();
			if($g=='Url'){
				$this->set('label',$g);
				return;
			}
			if($g=='Branchingslide'){
				$conditions=array('conditions'=>array('Branchingslide.parent_id'=>0));
			}
			if($g=='Post'){
				$conditions['order']='title';
			}
			//~ binding a model by the model's string name!!
			$this->loadModel($g);
			$y = $this->$g->find('list',$conditions);
			$p=array();
			foreach ($y as $key => $value) {
				$p[$key] = $value.' ('.$g.' id: '.$key.')';
			}
			$p[0] = "New ".$g;
			$q = ksort($p);
			$this->set('foreign_id_options', $p);
		}

		public function navlinkchildren($id=2, $address = 0){
				//$this->Navlink->contain('ChildNavlink');
				$g = $this->Navlink->find('all', array(
						'order' => array('Navlink.lft')
						,'conditions'=>array('Navlink.parent_id' => $id)
					)
				);
				$this->set('navlinks',$g);
				//$level = array_count();
				//$this->set('level',$level);
				$this->set('address',$address);
		}



		public function my_dashboard($course_id) {
			$this->addAdditionalScript('/js/courses_sitemap');
			$found = false;
			$this->Course->id = $course_id;
			$this->Course->contain();
			$g = $this->Course->read();
			///this is for changing the navlink.
			// /my/courses/dashboard/courseId -> /my/courses/addcontent/navLinkId -> /my/posts/add ->  /my/courses/dashboard/courseId
			$contentId = $this->Session->read('CourseEdit.contentId');
			$contentType = $this->Session->read('CourseEdit.contentType');
			if(
				!empty($contentId) &&
				!empty($contentId)
			){
				$nav = $this->Session->read('CourseEdit.navId');
				$this->Navlink->id = $nav;
				$this->Navlink->contain();
				$p = $this->Navlink->read();
				$p['Navlink']['foreign_id'] = $contentId;
				$p['Navlink']['$contentType'] = $contentType;
				if($this->Navlink->save($p)){
					$this->Session->setFlash(
						__('content saved.'),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);
					$this->Session->write('CourseEdit.contentId',null);
					$this->Session->write('CourseEdit.contentType',null);
					$this->Session->write('CourseEdit.navId',null);
				}
			}
			if (!empty($this->data)) {
				$g['Course']['navlink_id'] = $this->data['Navlink']['navlink_id'];
				if($this->Course->save($g['Course'])){
					$this->Session->setFlash(
						__('Navigation has been updated.'),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);
				}
			}
			$this->set_dashboard_navigation_links($g['Course']['navlink_id']);
			$navlink =  array("Navlink" => array(
				'options' => array($this->Navlink->find('list',array('conditions'=>array('parent_id'=>2)))),
				'value'=>$g['Course']['navlink_id']
			));
			foreach ($this->current_user['Course'] as $key => $value) {
				if($value['id']==$course_id){
					$found = $value;
					break;
				}
			} // end foreach
			$this->set("value",$value);
			$this->set("navlink_data", $navlink);

			$this->Session->write('CourseEdit.CourseId', $course_id);
		}



		public function my_editcontent($parentId) {
			debug($here);
				$this->set('classOptions',$this->classOptions);
				$this->setDashboardLayout();
				// $this->set('parentNavId',$parentId);
				// debug($this->request->is(array('post', 'put')));
				// debug($this->request);
				// exit;
				if ($this->request->is(array('post', 'put'))) {
					$saved = $this->Navlink->save($this->request->data);
					// debug($saved);
					$this->Session->write('CourseEdit.navId', $saved['Navlink']['id']);
					if ($saved) {
						$this->Session->setFlash(
							__('The %s has been saved', __('navlink')),
							'alert',
							array(
								'plugin' => 'TwitterBootstrap',
								'class' => 'alert-success'
							)
						);
						if(empty($this->defaultRedirect)){
							$this->defaultRedirect = $this->here;
						}
						// debug($saved['Navlink']['foreign_id']); exit;
						if ($saved['Navlink']['foreign_id'] == "0") {
							$g = '/my/'.strtolower(Inflector::pluralize($saved['Navlink']["class"])).'/add';
							$this->redirect($g);
						}
					} else {
						$this->Session->setFlash(
							__('The %s could not be saved. Please, try again.', __('navlink')),
							'alert',
							array(
								'plugin' => 'TwitterBootstrap',
								'class' => 'alert-error'
							)
						);
					}
				} else {
					$this->Navlink->id = $parentId;
					// debug($parentId);
					$this->data = $this->Navlink->find('first',array('conditions'=>array('Navlink.id'=>$parentId)));
					// debug($this->data);
				}

				$this->setfidData('Post');
				$n = $this->data;
				// debug($parentId);
				// debug($n); exit;
				$n['Navlink']['id'] = $parentId;
				$this->data = $n;
				$parentNavlinks = $this->Navlink->ParentNavlink->find('list',array('order'=>'name'));
				$g = array(
					'options' => $this->Avatar->getimagesAsOptions($this->Fileutil->getFileListing('.'.$this->Avatar->avatarDirectory))
				);
				$g['value'] = $this->data['Navlink']['avatar'];
				$this->set('fileOptions', $g);
				$this->set(compact('parentNavlinks'));
		}

		public function my_addcontent($parentId=2) {
			$this->set('classOptions',$this->classOptions);
			$this->setDashboardLayout();
			$this->set('parentNavId',$parentId);
			debug($this->request->is('post')); exit;
			if ($this->request->is('post')) {
				$saved = $this->Navlink->save($this->request->data);
				$this->Session->write('CourseEdit.navId', $saved['Navlink']['id']);
				if ($saved) {
					$this->Session->setFlash(
						__('The %s has been saved', __('navlink')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);
					if(empty($this->defaultRedirect)){
						$this->defaultRedirect = $this->here;
					}
					if ($saved['Navlink']['foreign_id'] == "0") {
						$g = '/my/'.strtolower(Inflector::pluralize($saved['Navlink']["class"])).'/add';
						$this->redirect($g);
					}
				} else {
					$this->Session->setFlash(
						__('The %s could not be saved. Please, try again.', __('navlink')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-error'
						)
					);
				}
			}else{
				$this->data = $this->Navlink->create();
			}
			$this->setfidData('Post');
			$n = $this->data;
			$n['Navlink']['parent_id'] = $parentId;
			$this->data = $n;
			$parentNavlinks = $this->Navlink->ParentNavlink->find('list',array('order'=>'name'));
			$g = array(
				'options' => $this->Avatar->getimagesAsOptions($this->Fileutil->getFileListing('.'.$this->Avatar->avatarDirectory))
			);
			$g['value'] = $this->data['Navlink']['avatar'];
				$this->set('fileOptions', $g);
			$this->set(compact('parentNavlinks'));
		}

		public function deleteLinkAndContent($id = null) {
			// deletes this link and its children.
			$this->Navlink->id = $id;
			if (!$this->Navlink->exists()) {
				throw new NotFoundException(__('Invalid %s', __('navlink')));
			}
			if ($this->request->is('post') || $this->request->is('put')) {

				$this->Navlink->id = $this->request->data['id'];
				if ($this->Navlink->deleteNavlinkAndContent()) {
					$this->Session->setFlash(
						__('The %s has been saved', __('navlink')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-success'
						)
					);
					$this->redirect($this->defaultRedirect);
				} else {
					$this->Session->setFlash(
						__('The %s could not be saved. Please, try again.', __('navlink')),
						'alert',
						array(
							'plugin' => 'TwitterBootstrap',
							'class' => 'alert-error'
						)
					);
				}
			} else {
			}
			$children = $this->getChildrenList($id);
			$this->set(compact('parentNavlinks','children'));
		}

		function my_index(){

			
		}


		public function my_edit() {
		$this->layout = 'dashboard';
		if (!empty($this->data)) {
				$this->CoursesUser->deleteAll(array("user_id"=>4));
				// debug($this->data);
				$g = $this->current_user;
				$b = $this->User->saveAll($this->data);
				//debug($this->current_user);
					//exit;
					$this->Session->setFlash(
						'Courses Saved!',
						'alert-success',
						array('plugin' => 'PagekwikTools')
				);
				if($g['User']['status']==0){
					return $this->redirect('/confirmsignup/');
				}
				if(
					$g['User']['status']==1 ||
					$g['User']['status']==2
			){
					return $this->redirect('/my/dashboard/');
				}
		}
		$this->set('userId', $this->current_user['User']['id']);
		$list = $this->Course->find("list");
		$this->set('model','Course');
		$this->set('list', $list);
		$this->User->Behaviors->attach('Containable');
		$this->User->contain(array('Course'));
		$ar = array();
		foreach ($this->current_user['Course'] as $key => $value) {
			array_push($ar, $value['id']);
		}
		$this->set('selections', $ar);
		}







}




/*


'User' => array(
		'password' => '*****',
		'id' => '4',
		'first_name' => 'mowgli',
		'last_name' => 'andress',
		'email_address' => 'jon@konarkpublishers.com',
		'username' => 'mowgli',
		'ip_address' => '127.0.10.1',
		'group_id' => '2',
		'status' => '1'
	),


'Course' => array(
		'CoursesUser' => array(
					'id' => '574',
					'user_id' => '4',
					'course_id' => '1'
				),

				$this->current_user['User']['id']
*/
