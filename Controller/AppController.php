<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');
App::uses('Security', 'Utility');
App::uses('Auth', 'Component');
App::uses('Sanitize', 'Utility');
App::uses('Model', 'CakeSession');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	var $uses = array(
		'Fpo'
		,'Setting'
		,'Dashboard'
		,'Navigations.Navlink'
		,'Avatar'
		,'UserManagement.Aro'
		,'User'
		,'Post'


		,'Virtuoso.Scale'
		,'EslwowQuizslide'



		,'Quiztextslide'
		,'Multichoice'
		,'Multichoiceanswer'
		,'EslwowMultichoiceanswer'
		,'EslwowNavlink'
		,'Navlink'
	);

	//
	// EslwowPost
	// EslwowQuiz
	// EslwowQuiz
	// EslwowQuizslide
	//


	var $components = array(
		'Session'
		,'Auth'
		,'MobileDetect'
		,'Fileutil'
		,'RequestHandler'
		,'Cookie'
		//  ,'DebugKit.Toolbar'
	);

	public $helpers = array(
		"Form"=>'TwitterBootstrap.BootstrapForm'
		,'TwitterBootstrap.BootstrapForm'
		,"Html"
		,"Session"
		,"Js"
		,'Time'
		,'BootstrapLayout'
		,'TwitterBootstrap.BootstrapPaginator'
		// ,'Paginator' => array('className' => 'TwitterBootstrap.BootstrapPaginator')
		,'AssetCompress.AssetCompress'
		,'RecursiveNav'
		,'Navigations.NestedTree'
		,'Navigations.TreeNav'
	);

	public $layout_prefix = 'standard';
	public $navId = 2;
	public $current_user;
	public $user_role='null';
	public $additional_scripts;
	public $isAjax;
	public $autoDetectAdmin = true;
	public $checkAjaxAutomatically = true;



	public function beforeFilter(){
		// debug($_SERVER['HTTP_REFERER']); exit;
		$this->Auth->allow('*');
		parent::beforeFilter();
		$this->checkAjax();
		$settings = $this->Setting->find('first');
		$this->settings = $settings['Setting'];
		$this->set('settings', $this->settings);
		$this->set('here', $this->params->here);
		$this->additional_scripts = array();
		$this->set('additional_scripts', $this->additional_scripts);
		$this->Session->renew();
		$this->set('ROUTING_PREFIX',''); //~wtf is this?
		$this->set('title_for_layout',$this->title_for_layout);
		$this->Auth->allow('setlayout','index');
		// debug("this may be a security risk");
		$this->set('concat_assets',true);
		$g = strpos($this->request->url,'my/');
		$this->handleUserStatus();
		if(!($g===false) && $this->autoDetectAdmin){
			// $this->setDashboardLayout();
		}
		if( $this->checkAjaxAutomatically){
			$this->handleAjax();
		}
		//----------------------------------------
		$isIECM = (ereg('MSIE 7',$_SERVER['HTTP_USER_AGENT'])) ? true : false;
		$isIE9 = (ereg('MSIE 9',$_SERVER['HTTP_USER_AGENT'])) ? true : false;
	  	$isIE10 = (ereg('MSIE 10',$_SERVER['HTTP_USER_AGENT'])) ? true : false;
		$isTablet = $this->MobileDetect->detect('isTablet');
		$isMobile = $this->MobileDetect->detect('isMobile');
		$isIE = ($isIE9 || $isIE10 || $isIECM);
		//---------
		$this->set(compact('isIE','isIECM','isIE9','isIE10','isTablet','isMobile'));
		//----------------------------------------
		//$t = $this->request->referer();
		$this->set('scaleList', $this->Scale->scales());
		$this->addAdditionalScript("/js/common");

		if($this->name==='CakeError'){
			$this->layout = 'default';
		}

	}

	protected function handleUserStatus(){
		$this->current_user = $this->Auth->user();
		//debug($this->current_user); exit;
		// $this->current_user['user_dir'] = $this->User->hashString(
		// 	$this->current_user['username'] 
		// );
		

		// Configure::write("current_user",$this->current_user);
		$this->User->recursive = 3;
		$this->User->id = $this->current_user['id'];

		$containments = array('Group','Course.Avatar',"Course.Landingpage","Course.Landingpage","Course.Landingpage","Course.Landingpage.Dynamicroute");
		// $containments = array("Course.Avatar");
		// $this->User->contain($containments);

		$this->current_user = $this->User->read();
		if($this->current_user){
			$this->current_user['User']['user_dir'] =
			$this->user_dir = $this->User->hashString(
				$this->current_user['User']['username'] 
			);
		}
		
		Configure::write("current_user",$this->current_user);
		//  debug($this->current_user); exit;
		$this->set('current_user',$this->current_user);

		if(!empty($this->current_user['Group']['name'])){
			$this->user_role = $this->current_user['Group']['name'];
		}else{
			$this->user_role = null;
		}
		$this->set('isAdmin',1);
		$this->set('user_role', $this->user_role);

		$this->isStaff();
		$this->isAdmin();
		$this->isDeveloper();
		$this->isStudent();
		

		$na = $this->Dashboard->getLeftNavItems($this->user_role);
		$this->set('left_nav_array', $na);
	}

	protected function isStaff(){
		$t = $this->user_role == 'staff' || $this->isAdmin();
		$this->set('isStaff',$t);
		return $t;
	}
	protected function isAdmin(){
		$t = $this->user_role == 'administrators' || $this->isDeveloper() || $this->user_role == 'teachers';
		$this->set('isAdmin',$t);
		return $t;
	}
	protected function isDeveloper(){
		$t = $this->user_role == 'developers';
		$this->set('isDeveloper',$t);
		return $t;
	}

	protected function isStudent(){
		$t = $this->user_role == 'students';
		$this->set('isStudent',$t);
		return $t;
	}

	public function beforeRender(){
		// this doesnt have to be in before filter here...you could put it in the before filter in the appropriate descendant controllers.
		// debug($this->navId); exit;
		$this->setNavTree($this->navId);
		$layout_pos = CakeSession::read('layoutpos');
		if($layout_pos !="b"){
			$layout_pos = "a";
		}
		$this->set('layoutpos',$layout_pos);
	}

	public function setDashboardLayout($specialLayout = null,$params=null,$bookId=null) {
		if(!isset($specialLayout)){
			//$this->layout = 'dashboard$_orig';
			$this->layout = 'dashboard';
		//	$this->layout = $this->layout_prefix.'_admin';
		}else{
			$this->layout = $this->layout_prefix.$specialLayout;
		}
		if(isset($params)){
			foreach ($params as $key => $value) {
				$this->set($key,$value);
			}
		}

		// $na = $this->Dashboard->getLeftNavItems($this->user_role);
		// $this->set('left_nav_array', $na);
		$this->handleAjax();
	}

	public function title($val){
		$this->set('title_for_layout',$val);
	}

	function checkAjax(){
		$this->isAjax = false;
		if ( isset($this->params['requested']) && $this->params['requested'] == 1){
			$this->isAjax = true;
	    }
		if ($this->request->is('ajax')) {
			$this->isAjax = true;
		}
		$this->set('isAjax',$this->isAjax );
		return $this->isAjax;
	}

	public function handleAjax(){
		$this->checkAjax();
		if ($this->isAjax) {
		    $this->disableCache();
			$this->layout = "ajax";
		}
	}

	public function setFlash($message){
		$this->Session->setFlash(
		    'Message!',
		    'flash_no_spam',
		    array('plugin' => 'PagekwikTools')
		);
	}

	public function addAdditionalScript($script){
		array_push($this->additional_scripts,$script);
		$this->set('additional_scripts',$this->additional_scripts);
	}


	public function addAdditionalScripts($ar){
		// debug($ar); exit;
		foreach ($ar as $key => $value) {
			$this->addAdditionalScript($value);
		}
	}

	public function getAdditionalScripts($script){
		return $this->additional_scripts;
	}

	// ALL NAVIGATION RELATED

	public function setNavIdByAssociatedContent($modelName, $foreignId){
		$this->loadModel($modelName);
		$d = $this->Navlink->find('first',array('conditions'=>array('Navlink.class'=>$modelName,'Navlink.foreign_id'=>$foreignId)));
		if (empty($d)) {
			$this->navId = 2;
		}else{
			$this->navId = $d['Navlink']['id'];
		}
	}

	public function setNavIdByAssociatedURL(){
			$d = $this->Navlink->find('first',array('conditions'=>array('Navlink.foreign_id'=>$_SERVER['REQUEST_URI'])));
			//debug($d);
			if (empty($d)) {
				$this->navId = 2;
			}else{
				$this->navId = $d['Navlink']['id'];
			}
	}

	public function setNavTree($navlink_id = 1){

		// debug($this->Navlink->verify()); exit;
		$this->Navlink->id = $navlink_id;
		$path = $this->Navlink->getPath( $navlink_id );
		// debug( $navlink_id );
		// debug($path);
		// debug($path); exit;
		$rootId = 1;

		if(count($path)>2){
			$rootNode = $path[2];
			$rootId = $rootNode['Navlink']['id'];
		}

		$l = $path[count($path)-1]['Navlink'];
		$this->set('owl_avatar', array('image'=>$l['avatar'],'title'=>$l['name']));
		$this->set('selected_nav_parents',$path);

		$g = $this->Navlink->find('threaded', array(
			'order' => array('Navlink.lft')
			,'conditions'=>array(
				'Navlink.id !='=>$this->settings['sitemap_parent_id'],
				'Navlink.in_nav'=>'1'
				)
			)
		);
		$this->set('leftNavlinks', $g);
		//debug(count($ae));
		// debug($g); exit;

		$this->set('nav_data', $g );
		return $g;
	}



/*
	public function set_dashboard_navigation_links($id=2, $address = 0){
		//$this->Navlink->contain('ChildNavlink');
		$g = $this->Navlink->find('all', array(
				'order' => array('Navlink.lft')
				,'conditions'=>array('Navlink.parent_id' => $id)
			)
		);
		$this->set('navlinks',$g);
		//$level = array_count();
		//$this->set('level',$level);
		$this->set('address',$address);

		return array('navlinks'=>$g,"address");
	}




*/






	public function setlayout($a){
		CakeSession::write('layoutpos',$a);
		echo CakeSession::read('layoutpos');
		exit;
	}


}
