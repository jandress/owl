from pydub import AudioSegment
import os
import numpy as np
import shutil


class AudioFileSorter:
    def __init__(self, directory, good_folder='good', outliers_folder='outliers', 
                 duration_deviation_threshold=1000, loudness_deviation_threshold=3):
        """
        Initialize the sorter with thresholds and directory names.
        """
        self.directory = directory
        self.good_folder = os.path.join(directory, good_folder)
        self.outliers_folder = os.path.join(directory, outliers_folder)
        self.duration_deviation_threshold = duration_deviation_threshold  # in milliseconds
        self.loudness_deviation_threshold = loudness_deviation_threshold  # in dBFS

    def get_mean_duration_and_loudness(self):
        durations = []
        loudnesses = []

        for file in os.listdir(self.directory):
            if file.endswith((".wav", ".mp3")):
                file_path = os.path.join(self.directory, file)
                audio = AudioSegment.from_file(file_path)

                durations.append(len(audio))
                loudnesses.append(audio.dBFS)

        if durations and loudnesses:
            mean_duration = np.mean(durations)
            mean_loudness = np.mean(loudnesses)
        else:
            mean_duration = mean_loudness = 0

        return mean_duration, mean_loudness

    def sort_files(self):
        mean_duration, mean_loudness = self.get_mean_duration_and_loudness()

        # Create target directories if they don't exist
        for folder in [self.good_folder, self.outliers_folder]:
            if not os.path.exists(folder):
                os.makedirs(folder)
                print(f"Created directory: {folder}")

        for file in os.listdir(self.directory):
            if file.endswith((".wav", ".mp3")):
                file_path = os.path.join(self.directory, file)
                audio = AudioSegment.from_file(file_path)

                duration_deviation = abs(len(audio) - mean_duration)
                loudness_deviation = abs(audio.dBFS - mean_loudness)

                # Check deviation from mean
                if duration_deviation <= self.duration_deviation_threshold and \
                   loudness_deviation <= self.loudness_deviation_threshold:
                    # Move to 'good' folder
                    shutil.move(file_path, os.path.join(self.good_folder, file))
                    print(f"Moved '{file}' to {self.good_folder}")
                else:
                    # Move to 'outliers' folder and log the reason
                    outlier_reason = "Duration" if duration_deviation > self.duration_deviation_threshold else "Loudness"
                    if outlier_reason == "Duration" and len(audio) < 2000:
                        print(f"length '{len(audio)}'")
                        shutil.move(file_path, os.path.join(self.outliers_folder, file))
                        print(f"Moved '{file}' to {self.outliers_folder} due to exceeding {outlier_reason} threshold.")
                    else: 
                        shutil.move(file_path, os.path.join(self.good_folder, file))
                        print(f"Moved '{file}' to {self.good_folder}")
                    if outlier_reason == "Loudness":
                            print(f"loudness '{audio.dBFS}'")
                            print(f"Moved '{file}' to {self.outliers_folder} due to exceeding {outlier_reason} threshold.")

# Usage
# Replace 'path/to/directory' with the actual directory path
# Adjust duration_deviation_threshold and loudness_deviation_threshold as needed
sorter = AudioFileSorter('audio/HEAVY/strings/6/segments', duration_deviation_threshold=1000, loudness_deviation_threshold=15)
sorter.sort_files()



