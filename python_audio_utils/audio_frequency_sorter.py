import librosa
import os
import numpy as np
import shutil

class AudioFrequencySorter:
    def __init__(self, directory):
        """
        Initialize the sorter with the directory name.
        """
        self.directory = directory

    def get_spectral_centroid(self, file_path):
        """
        Get the spectral centroid of the audio file.
        """
        try:
            y, sr = librosa.load(file_path)
            spectral_centroid = np.mean(librosa.feature.spectral_centroid(y=y, sr=sr))
            return spectral_centroid
        except Exception as e:
            print(f"Error processing {file_path}: {e}")
            return None

    def sort_files(self):
        """
        Sort files based on their spectral centroid and rename them.
        """
        spectral_file_map = {}

        for file in os.listdir(self.directory):
            if file.endswith((".wav", ".mp3")):  # Include other formats if needed
                file_path = os.path.join(self.directory, file)
                spectral_centroid = self.get_spectral_centroid(file_path)

                if spectral_centroid is not None:
                    spectral_file_map[file] = spectral_centroid
                else:
                    error_folder = os.path.join(self.directory, 'errors')
                    if not os.path.exists(error_folder):
                        os.makedirs(error_folder)
                        print(f"Created directory: {error_folder}")
                    shutil.move(file_path, os.path.join(error_folder, file))
                    print(f"Moved '{file}' to {error_folder} due to error.")

        # Sort files by spectral centroid
        sorted_files = sorted(spectral_file_map, key=spectral_file_map.get)

        # Rename files
        for index, file in enumerate(sorted_files):
            _, ext = os.path.splitext(file)
            new_name = f"{index:02d}{ext}"
            old_path = os.path.join(self.directory, file)
            new_path = os.path.join(self.directory, new_name)
            os.rename(old_path, new_path)
            print(f"Renamed '{file}' to '{new_name}'")

# Example usage
sorter = AudioFrequencySorter('segments')
sorter.sort_files()
