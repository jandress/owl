from pydub import AudioSegment
from pydub.silence import detect_nonsilent
import json

class AudioSpriteMapper:
    def __init__(self, audio_file_path, min_silence_len=100, silence_thresh=-46):
        self.audio_file_path = audio_file_path
        self.min_silence_len = min_silence_len  # Minimum length of silence to consider in ms
        self.silence_thresh = silence_thresh    # Silence threshold in dB

    def generate_sprite_map(self):
        try:
            audio = AudioSegment.from_file(self.audio_file_path)
            print(f"Loaded audio file: {self.audio_file_path}")

            # Detect non-silent chunks
            nonsilent_chunks = detect_nonsilent(
                audio, 
                min_silence_len=self.min_silence_len, 
                silence_thresh=self.silence_thresh
            )

            sprite_map = {
                "resources": [self.audio_file_path],
                "spritemap": {}
            }

            for index, (start, end) in enumerate(nonsilent_chunks):
                tone_name = str(index)
                sprite_map["spritemap"][tone_name] = {
                    "start": start / 1000.0,  # Convert to seconds
                    "end": end / 1000.0,      # Convert to seconds
                    "loop": False
                }

            return json.dumps(sprite_map, indent=2)

        except Exception as e:
            print(f"Error processing audio file: {e}")
            return json.dumps({})

    def save_sprite_map_to_file(self, output_file):
        sprite_map_json = self.generate_sprite_map()
        if sprite_map_json:
            try:
                with open(output_file, 'w') as file:
                    file.write(sprite_map_json)
                print(f"Saved sprite map to {output_file}")
            except Exception as e:
                print(f"Error writing to file {output_file}: {e}")




mapper = AudioSpriteMapper("./audio/HEAVY/STRINGS/6/6.wav")
mapper.save_sprite_map_to_file("./audio/HEAVY/STRINGS/6/6.json")
