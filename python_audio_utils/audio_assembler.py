from pydub import AudioSegment
import os

class AudioAssembler:
    def __init__(self, target_directory, spacing):
        self.target_directory = target_directory
        self.spacing = spacing  # Fixed interval for the start of each segment in milliseconds

    def assemble_audio(self, export_file="assembled_audio.wav"):
        # Initialize an empty audio segment
        assembled_audio = AudioSegment.silent(duration=0)

        # Current position in the assembled audio
        current_position = 0

        # Check if the target directory exists
        if not os.path.exists(self.target_directory):
            print(f"Error: The directory '{self.target_directory}' does not exist.")
            return

        # Iterate over files in the directory and assemble them
        files_found = False
        for file in sorted(os.listdir(self.target_directory)):
            files_found = True
            # print("1")
            if file.endswith((".wav", ".mp3")):  # Include other formats if needed
                # print("2")
                files_found = True
                file_path = os.path.join(self.target_directory, file)
                
                try:
                    audio_segment = AudioSegment.from_file(file_path)
                    segment_length = len(audio_segment)
                    print(f"Processing file {file}, Length: {segment_length} ms")

                    if segment_length == 0:
                        print(f"Warning: The file {file} is empty.")
                        continue

                    # Calculate the actual start time for this segment
                    segment_start_time = self.spacing * (current_position // self.spacing)

                    # Add silence if necessary to align this segment to its start time
                    if current_position < segment_start_time:
                        silence_duration = segment_start_time - current_position
                        silence = AudioSegment.silent(duration=silence_duration)
                        assembled_audio += silence
                        current_position += silence_duration
                        print(f"Added silence of {silence_duration} ms")

                    # Add the audio segment
                    assembled_audio += audio_segment
                    current_position += segment_length

                except Exception as e:
                    print(f"Error processing file {file}: {e}")

        if not files_found:
            print("No valid audio files found in the directory.")
            return

        # Ensure the total duration does not exceed multiples of the spacing
        total_duration = self.spacing * (current_position // self.spacing)
        assembled_audio = assembled_audio[:total_duration]
        print(f"Total duration of assembled audio: {len(assembled_audio)} ms")

        # Export the assembled audio
        try:
            output_path = os.path.join(self.target_directory, export_file)
            assembled_audio.export(output_path, format="wav")
            print(f"Exported assembled audio to {output_path}")
        except Exception as e:
            print(f"Error exporting assembled audio: {e}")

# Usage
assembler = AudioAssembler("./audio/HEAVY/STRINGS/6/segments/tapered", 1000)  # Each segment starts every 1000 milliseconds
assembler.assemble_audio()
