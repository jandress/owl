from pydub import AudioSegment
import os

def analyze_with_pydub(file_path):
    audio = AudioSegment.from_file(file_path)
    duration = len(audio)  # Duration in milliseconds
    loudness = audio.dBFS  # Loudness in dBFS
    return duration, loudness

# Paths to the audio files
good_file_path = './segments/good.wav'
bad_file_path = './segments/bad.wav'

# Analyzing the audio files
good_duration, good_loudness = analyze_with_pydub(good_file_path)
bad_duration, bad_loudness = analyze_with_pydub(bad_file_path)

(good_duration, good_loudness), (bad_duration, bad_loudness)
