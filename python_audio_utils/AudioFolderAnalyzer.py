from pydub import AudioSegment
import os

class AudioFolderAnalyzer:
    def __init__(self, directory):
        self.directory = directory

    def analyze_folder(self):
        durations = []
        loudnesses = []

        for file in os.listdir(self.directory):
            if file.endswith((".wav", ".mp3")):  # Include other formats if needed
                file_path = os.path.join(self.directory, file)
                audio = AudioSegment.from_file(file_path)

                # Collect duration and loudness
                durations.append(len(audio))  # Duration in milliseconds
                loudnesses.append(audio.dBFS) # Loudness in dBFS

        if not durations or not loudnesses:
            return None

        # Compute averages and extremes
        avg_duration = sum(durations) / len(durations)
        avg_loudness = sum(loudnesses) / len(loudnesses)
        min_duration = min(durations)
        max_duration = max(durations)
        min_loudness = min(loudnesses)
        max_loudness = max(loudnesses)

        return {
            "average_duration_ms": avg_duration,
            "average_loudness_dbfs": avg_loudness,
            "min_duration_ms": min_duration,
            "max_duration_ms": max_duration,
            "min_loudness_dbfs": min_loudness,
            "max_loudness_dbfs": max_loudness
        }

# Usage
# Note: Replace 'path/to/good_files_directory' with the actual directory path containing the good audio files.
analyzer = AudioFolderAnalyzer('segments/good')
good_files_analysis = analyzer.analyze_folder()
print(good_files_analysis)
# good_files_analysis
# {
#     'average_duration_ms': 6592.636363636364,
#     'average_loudness_dbfs': -23.393641344167744, 
#     'min_duration_ms': 2443, 
#     'max_duration_ms': 10312, 
#     'min_loudness_dbfs': -30.939339728115655, 
#     'max_loudness_dbfs': -16.420404880052697
# }
