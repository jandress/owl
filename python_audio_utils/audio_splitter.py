    # def split_and_export(self, min_silence_len=100, silence_thresh=-46, keep_silence=55, export_format="wav"):



import os
from pydub import AudioSegment
from pydub.silence import detect_nonsilent

class AudioSplitter:
    def __init__(self, file_path, min_silence_len=100, silence_thresh=-46, seek_step=1):
        self.file_path = file_path
        self.min_silence_len = min_silence_len
        self.silence_thresh = silence_thresh
        self.seek_step = seek_step

    def split_and_save_segments(self):
        # Load audio file
        audio = AudioSegment.from_file(self.file_path)
        print(f"Loaded audio file: {self.file_path}")

        # Detect nonsilent parts
        nonsilent_parts = detect_nonsilent(
            audio, 
            min_silence_len=self.min_silence_len, 
            silence_thresh=self.silence_thresh, 
            seek_step=self.seek_step
        )
        print(f"Detected {len(nonsilent_parts)} segments in the audio.")

        # Create the 'segments' directory
        segments_dir = os.path.join(os.path.dirname(self.file_path), 'segments')
        if not os.path.exists(segments_dir):
            os.makedirs(segments_dir)
            print(f"Created directory: {segments_dir}")

        # Save each segment
        for i, (start, end) in enumerate(nonsilent_parts):
            segment = audio[start:end]
            segment_file_path = os.path.join(segments_dir, f"segment_{i}.wav")
            segment.export(segment_file_path, format="wav")
            print(f"Exported segment {i} to {segment_file_path}")

splitter = AudioSplitter("./audio/HEAVY/STRINGS/6/raw.WAV")
splitter.split_and_save_segments()
