import librosa
import os
import shutil
import numpy as np

class ToneDuplicateChecker:
    def __init__(self, directory):
        self.directory = directory
        self.duplicates_dir = os.path.join(directory, 'duplicates')
        if not os.path.exists(self.duplicates_dir):
            os.makedirs(self.duplicates_dir)

    def find_dominant_frequency(self, file_path):
        # Load the audio file
        y, sr = librosa.load(file_path)

        # Get the frequency spectrum
        D = librosa.stft(y)
        magnitudes = librosa.amplitude_to_db(abs(D), ref=np.max)
        frequencies = librosa.fft_frequencies(sr=sr)

        # Find the peak frequency
        peak_index = np.argmax(np.mean(magnitudes, axis=1))
        peak_frequency = frequencies[peak_index]

        return peak_frequency

    def move_file_to_duplicates(self, file):
        src_path = os.path.join(self.directory, file)
        dest_path = os.path.join(self.duplicates_dir, file)
        shutil.move(src_path, dest_path)

    def check_duplicates(self):
        if not os.path.exists(self.directory):
            print(f"Directory does not exist: {self.directory}")
            return

        frequencies = {}
        duplicates = []

        for file in os.listdir(self.directory):
            if file.endswith((".wav", ".mp3")):  # Add other formats if needed
                file_path = os.path.join(self.directory, file)
                try:
                    freq = self.find_dominant_frequency(file_path)
                    print(f"File: {file}, Frequency: {freq} Hz")

                    if freq in frequencies:
                        # Move previous duplicate to duplicates folder
                        self.move_file_to_duplicates(frequencies[freq])
                        frequencies[freq] = file
                        duplicates.append(file)
                    else:
                        frequencies[freq] = file
                except Exception as e:
                    print(f"Error processing file {file}: {e}")

        if duplicates:
            print("Duplicates found and moved to 'duplicates' folder:")
            for dup in duplicates:
                print(f"{dup} is a duplicate")
        else:
            print("No duplicates found.")

checker = ToneDuplicateChecker("audio/strings/2/original_raw/segments")
checker.check_duplicates()






