import os
import re

class FileRenamer:
    def __init__(self, directory, start_number=0):
        self.directory = directory
        self.start_number = start_number

    def is_hidden_or_system_file(self, file_name):
        """Check if a file is a system or hidden file."""
        return file_name.startswith('.')

    def natural_sort_key(self, s):
        """A sorting key to sort strings containing numbers naturally."""
        return [int(text) if text.isdigit() else text.lower() for text in re.split('([0-9]+)', s)]

    def rename_files(self):
        """Rename files in the directory using natural sorting to avoid conflicts."""
        if not os.path.exists(self.directory):
            print(f"Directory does not exist: {self.directory}")
            return

        files = os.listdir(self.directory)
        files = [f for f in files if not self.is_hidden_or_system_file(f)]
        files.sort(key=self.natural_sort_key)

        # Renaming process
        temp_names = []
        original_extensions = {}

        for index, file in enumerate(files):
            old_path = os.path.join(self.directory, file)
            _, ext = os.path.splitext(file)
            original_extensions[index] = ext
            temp_name = f"temp_{index:04d}"
            temp_path = os.path.join(self.directory, temp_name)
            os.rename(old_path, temp_path)
            temp_names.append(temp_name)

        for temp_index, temp_name in enumerate(temp_names):
            temp_path = os.path.join(self.directory, temp_name)
            ext = original_extensions[temp_index]
            new_name = f"{self.start_number + temp_index:02d}{ext}"
            new_path = os.path.join(self.directory, new_name)
            os.rename(temp_path, new_path)
            print(f"Renamed '{temp_name}' to '{new_name}'")

        print("Finished renaming files.")

renamer = FileRenamer("./audio/HEAVY/STRINGS/6/segments")
renamer.rename_files()


