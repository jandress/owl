from pydub import AudioSegment
from pydub.silence import detect_nonsilent
import os

class AudioTaper:
    def __init__(self):
        pass

    def edit_file(self, file_path):
        # Base directory of the file
        base_directory = os.path.dirname(file_path)

        # Directory for edited files
        edited_directory = os.path.join(base_directory, "tapered")
        if not os.path.exists(edited_directory):
            os.makedirs(edited_directory)

        try:
            # Process the file
            audio = AudioSegment.from_file(file_path)
            original_length = len(audio)
            print(f"Processing file: {file_path}, Length: {original_length} ms")

            # Check if the audio length is shorter than 5 seconds (5000 ms)
            if original_length < 5000:
                print(f"File is shorter than 5 seconds. Processing for tapering and length adjustment.")

                # Calculate the midpoint for tapering
                midpoint = original_length // 2

                # Taper volume from midpoint to end
                audio = audio.fade(to_gain=-120, start=midpoint, end=original_length)

                # Add silence to make the file 5 seconds long
                silence_duration = 5000 - original_length
                silence = AudioSegment.silent(duration=silence_duration)
                audio += silence
            else:
                # Taper volume from 2.5s to 4.75s for files longer than 5 seconds
                audio = audio.fade(to_gain=-120, start=2500, end=4750)

            # Ensure the file ends at 5s
            audio = audio[:5000]

            # Save the file
            edited_file_name = os.path.basename(file_path)
            edited_file_path = os.path.join(edited_directory, edited_file_name)
            audio.export(edited_file_path, format=edited_file_name.split('.')[-1])
            
            print(f"Processed and saved {edited_file_name}")

        except Exception as e:
            print(f"Error processing file {file_path}: {e}")

    def edit_all_files_in_directory(self, directory_path):
        if not os.path.exists(directory_path):
            print(f"Error: The directory '{directory_path}' does not exist.")
            return

        files_processed = 0
        for file in os.listdir(directory_path):
            if file.endswith((".wav", ".mp3")):  # Add other audio formats if needed
                self.edit_file(os.path.join(directory_path, file))
                files_processed += 1

        if files_processed == 0:
            print("No valid audio files found in the directory.")
        else:
            print(f"Processed {files_processed} files in the directory.")

# Usage
taper = AudioTaper()
# taper.edit_file("path/to/single/audiofile.mp3")  # Edit a single file
taper.edit_all_files_in_directory("./audio/HEAVY/STRINGS/6/segments")  # Edit all files in a directory



