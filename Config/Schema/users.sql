-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Sep 15, 2020 at 03:55 PM
-- Server version: 5.7.25
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `owl`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `email_address` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `country` text,
  `group_id` int(16) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `password`, `status`, `email_address`, `username`, `country`, `group_id`) VALUES
(1, 'Jon', 'Andress', 'ac4adf42bd0387c17bc427bb542bc4f0a026e0d5', 1, 'email@jonandress.com', 'admin', 'USA', 3),
(2, 'Jon', 'Andress', 'cf460f9af66add063081828112b030bf4e9378b1', 1, 'jonandress@gmail.com', 'csands1', NULL, 1),
(4, 'mowgli', 'andress', '6ec8ffe12d85dffc32a8df11a67a1bf81e4c28ca', 1, 'jon@konarkpublishers.com', 'mowgli', 'Afghanistan', 1),
(3, 'Steve', 'Seagraves', '6ec8ffe12d85dffc32a8df11a67a1bf81e4c28ca', 1, 'sseagraves@tecmasters.com', 'sseagraves', NULL, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `username_2` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
