<?php
/**
 * Main Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
//	Router::connect('/', array('controller' => 'pages', 'action' => 'home'));

/**
 * ...and connect the rest of 'Pages' controller's urls.
 */

	// Router::connect('/', array('plugin'=>'Forum', 'controller' => 'Forum', 'action' => 'admin_index','parameters'=>'24'));
	// Router::connect('/', array('/my/courses/dashboard/7'));
	Router::connect('/', array('plugin'=>'Virtuoso','controller' => 'Virtuoso', 'action' => 'index'));
	Router::connect('/tree/*', array('controller' => 'Pages', 'action' => 'tree'));
	Router::connect('/api/push/*', array('controller' => 'Settings', 'action' => 'push'));
	Router::connect('/my/theme/*', array('controller' => 'Settings', 'action' => 'admin_theme'));
	Router::connect('/layout/*', array('controller' => 'App', 'action' => 'setlayout'));
	Router::connect('/stub/*', array('controller' => 'Pages', 'action' => 'stub'));
	Router::connect('/devs/*', array('controller' => 'Pages', 'action' => 'devs'));
	Router::connect('/my/devs/*', array('controller' => 'Pages', 'action' => 'devs'));
	Router::connect('/my/stub/*', array('controller' => 'Pages', 'action' => 'stub'));
	Router::connect('/testing/*', array('controller' => 'Pages', 'action' => 'testing'));
	Router::connect('/testing/*', array('controller' => 'Pages', 'action' => 'testing'));	
	Router::connect('/pages/navs', array('controller' => 'pages', 'action' => 'navs'));
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));




/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();
/**
 * Load the CakePHP default routes. Remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';


	//		Router::connect('/navlinks/getavatar/*', array('plugin'=>'Navigations','controller' => 'Navlinks', 'action' => 'getavatar'));	
