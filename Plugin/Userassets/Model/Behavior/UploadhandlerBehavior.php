<?php 

	class UploadhandlerBehavior extends ModelBehavior {

		protected $_defaultSettings = array();
	    public $allowedExtensions = array('pdf','epub','png','jpg','jpeg','m4v','mp4','mp3','webm','webmsd.webm','ppt','pptx','pdf','doc','docx','webp','wav');
	    public $sizeLimit = 10485760;
	    public $file;

	    function getSize() {
	        if (isset($_SERVER["CONTENT_LENGTH"])){
	            return (int)$_SERVER["CONTENT_LENGTH"];
	        } else {
	            throw new Exception('Getting content length is not supported.');
	        }      
	    }



	    private function checkServerSettings(){        
			// check settings in this file vs server limits
	        $maxPostSize = $this->toBytes(ini_get('post_max_size'));
	        $maxUploadSize = $this->toBytes(ini_get('upload_max_filesize'));        
	        if ($maxPostSize < $this->sizeLimit || $maxUploadSize < $this->sizeLimit){
	            $desiredSize = max(1, $this->sizeLimit / 1024 / 1024) . 'M';             
				//increase post_max_size and upload_max_filesize to 10M , current: 20M , 20971520
	            die("{'error':'increase post_max_size and upload_max_filesize to $desiredSize '}");    
	        }        
	    }

	    private function toMegaBytes($str){
			$g = $str;
	        return ($g / 1024 / 1024).'M';
	    }


	    private function toBytes($str){
	        $val = trim($str);
	        $last = strtolower($str[strlen($str)-1]);
	        switch($last) {
	            case 'g': $val *= 1024;
	            case 'm': $val *= 1024;
	            case 'k': $val *= 1024;        
	        }
	        return $val;
	    }

	    /**
	     * Returns array('success'=>true) or array('error'=>'error message')
	     */
	    public function handleUpload($model, $uploadDirectory, $newFileName = null, $createTargetDirectory = true,$replaceOldFile = true){
		//	error_log('$uploadDirectory: '.$uploadDirectory.' $newFileName: '.$newFileName.' $createTargetDirectory: '.$createTargetDirectory.'  $replaceOldFile: '.$replaceOldFile);
			if($createTargetDirectory){
				$this->createDirectoryIfNotExists($model, $uploadDirectory);			
			}
	        if (!is_writable($uploadDirectory)){
	            return array('error' => "Server error. Upload directory: $uploadDirectory | isn't writable.");
	        }
			//file name and extension
	        $pathinfo = pathinfo($_GET['qqfile']);
	        $filename = $pathinfo['filename'];
//			error_log(print_r($pathinfo, true));
		
	        //$filename = md5(uniqid());
//			error_log('ext: ');
	        $ext = @$pathinfo['extension'];		
			// error_log(strtolower($ext));					
			// error_log('ext');
			// error_log('allowedExtensions');	
			// error_log(print_r($this->allowedExtensions,1));
			// hide notices if extension is empty
	        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
	            $these = implode(', ', $this->allowedExtensions);
				error_log('File has an invalid extension, it should be one of '. $these . '.');
	            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
	        }
	
	        if(	!in_array(strtolower($ext), $this->allowedExtensions)){
	            $these = implode(', ', $this->allowedExtensions);
	            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
	        }
	
	
			if(!empty($newFileName)){
				$filename = $newFileName;			
			}
			// if the file exists, add a random filename to it.
	        if(!$replaceOldFile){
	            /// don't overwrite previous files that were uploaded
	            while (file_exists($uploadDirectory . $filename)) {
	                $filename .= rand(10, 99);
	            }
	        }
	        if ($this->_saveFile($model, $uploadDirectory . $filename)){
	            return array('success'=>true);
	        } else {
	            return array('error'=> 'Could not save uploaded file.' .
	                'The upload was cancelled, or server error encountered');
	        }
	    }  

		public function createDirectoryIfNotExists(Model $model, $directoryPath){
			$v = explode(DS,$directoryPath);
			$g = '';
			if(file_exists($directoryPath)){
				return;
			}
			foreach ($v as $key => $value) {
					$g .= $value."/";
					if(!file_exists($g)){
						mkdir($g, 0755, true);					
					}else{
					}
			}			
		}

	    protected function _saveFile($Model, $path) {    
			// open stream and write to tempdirectory
	        $input = fopen("php://input", "r");
	        $temp = tmpfile();
	        $realSize = stream_copy_to_stream($input, $temp);
	        fclose($input);
			// check the filesize.  if the real size is not the expected file size, fail.
	        if ($realSize != $this->getSize()){            
	            return false;
	        }
			// error_log('allowedExtensions');
			// error_log(print_r($this->settings[$Model->alias]['allowedExtensions'],1));
			// error_log('temp');
			// error_log(
			// 	print_r($temp,1)
			// );			
			$deepCheck = false;
			if ($deepCheck) {
				if(
					!in_array(Mime_Type::guessExtension($temp) , $this->settings[$Model->alias]['allowedExtensions'] )
				){
					error_log('INVALID_FILE: '.Mime_Type::guessExtension($temp).', '.print_r($this->settings[$Model->alias]['allowedExtensions'],1));
					$Model->invalidate('hiddenValidationField',"Improper filtype");
					fclose($temp);
					return false;
				}
			}
				
	        $target = fopen($path, "w");        
	        fseek($temp, 0, SEEK_SET);
	        stream_copy_to_stream($temp, $target);
	        fclose($target);
	        return true;
	    }

		/**
		 * Setup
		 *
		 * @param Model $Model
		 * @param array $settings See defaultSettings for configuration options
		 * @return void
		 */

		public function setup(Model $Model, $settings = array()) {
			if (!isset($this->settings[$Model->alias])) {
				$this->settings[$Model->alias] = $this->_defaultSettings;
			}
			$this->settings[$Model->alias] = array_merge($this->settings[$Model->alias], (array) $settings);
		}


		public function deleteFile(Model $Model, $cascade = true) {
			extract($this->settings[$Model->alias]);
			$result = $Model->find('first', array(
				'conditions' => array($Model->primaryKey => $Model->id),
				'fields'     => array('dirname', 'basename','filetype'),
				'recursive'  => -1,
			));
			if (!$result[$Model->alias]['dirname'] || !$result[$Model->alias]['basename'] || !$result[$Model->alias]['filetype']) {
				echo "return true";
				return true;
			}
			$file = APP.'product_downloads'.DS.$result['Download']['dirname'].DS.$result['Download']['basename'].'.'.$result['Download']['filetype'];
			if(file_exists($file)){
				unlink($file);
			}
			return !file_exists($file);
		}

 

	}
?>