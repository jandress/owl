<?php

	class UserassetsController extends UserassetsAppController {

		//-- 
		var $components = array('RequestHandler','Userassets.Fileutil');
		var $helpers = array('Html',"Session");       
		var $uses = array('Userassets.Fileasset');
        var $default_layout = "file_management";              
        //--
        var $file_dir_root_relative = './users/'; // relative is needed by Fileutil. 
		var $file_dir_root_absolute = '/users/'; // i believe abs is needed by the view.
		//--
		var $root_uri = '/my/userassets/';
		//-- 
		var $dir_contents;
		var $current_directory;
		var $directoryPathArray;
		var $collaborationString = '';
		//-- 


		function beforeFilter(){
			parent::beforeFilter();
			$user_hash = $this->current_user['User']['user_dir'];
			$this->set('user_hash',$user_hash);
			$this->file_dir_root_absolute = $this->file_dir_root_absolute.''.$this->current_user['User']['user_dir'].'/';
			$this->file_dir_root_relative = '.'.$this->file_dir_root_absolute;
			$this->set('file_path',$this->file_dir_root_relative);
			// debug($this->layout);
			$this->Auth->allow('downloads');			
		}


		function admin_addir(){
			$this->setCommonVars();
			//	$this->layout = "ajax";   
			$g = $this->directoryArrayToString($this->params->params['pass'] );
			if ($this->request->is('post')) {
				$directory_path = str_replace('//','/',$g.'/'.$this->request->data['dir']['name']);
				$c = $this->Fileutil->createDirectoryIfNotExists($this->file_dir_root_relative.$directory_path);
				if($c){
					$this->redirect($this->root_uri.''.$directory_path);
				}
				exit;
			}
		}
		
		function admin_rmdir(){
			$this->setCommonVars();	
			if(count($this->dir_contents)==0){
				if($this->Fileutil->deleteDirAndContents($this->file_dir_root_relative.$this->current_directory)){
					$this->redirect($this->root_uri);
				}
			}
			if ($this->request->is('post')) {
				if($this->Fileutil->deleteDirAndContents($this->file_dir_root_relative.$this->current_directory)){
					$this->redirect($this->root_uri);
				}
			}
		}

		function playtrack($track){
			$ar = $this->params->params['pass'];
			array_pop($ar);
			$this->current_directory = $this->directoryArrayToString($ar );
			// debug($ar);
			// debug($this->params->params['pass'][count($this->params->params['pass'])-1]);
			$this->set('file_dir_root_relative',$this->file_dir_root_relative);
			$this->set('file_dir_root_absolute',$this->file_dir_root_absolute);			
			// $this->set('filepath','/user_managed/'.$this->current_directory);			
			// debug($this->current_directory);
			// rtrim returns a string with whitespace (or other characters) stripped from the end of string.
			 $if = rtrim($this->file_dir_root_absolute."".$this->current_directory ,"/").'/';
			// debug($if);

			$initial_file = $this->params->params['pass'][count($this->params->params['pass'])-1];
			 $this->set("initial_file", $initial_file);
			 $this->set("filename", $initial_file);
			 $this->set('file_path_full', $if);			 
			 $this->set('file_path', $if);			 
			$this->set('root_uri',$this->root_uri);
			// $this->layout = "dashboard";
			$this->addAdditionalScript('/Virtuoso/Virtuoso/js/EventManager');
			$this->addAdditionalScript('/Userassets/js/audio_player');
			$this->addAdditionalScript('/Userassets/js/standalone_player_instantiation');
			$this->handleAjax();				
		}

		function admin_index(){              
			//  debug($this->layout); exit;			
			$this->setCommonVars();				
			$this->handleAjax();				

			$this->layout = "user_area_layout";
			$this->set('columns', 3);
			$this->set('guid', 'ua-');
			$this->addAdditionalScript('/Userassets/js/audio_player');
			$this->addAdditionalScript('/Userassets/js/user_asssets_file_browser_instantiation');
			
		}


		// function collaboration_area(){  

		// 	//all other colaborations are my/c/1


		// 	// $ar = $this->params['pass'];
		// 	//  array_shift($ar);
		// 	//  foreach ($ar as $key => $value) {
		// 	// 	$this->collaborationString = $this->collaborationString.$value.'/';
		// 	//  }
		// 	// $this->file_dir_root_absolute = $this->file_dir_root_absolute.''.$this->current_user['User']['user_dir'].'/'.$collaborationString;
		// 	// $this->file_dir_root_relative = '.'.$this->file_dir_root_absolute;
		// 	//  debug($this->layout); exit;			
		// 	$this->set('file_path',$this->file_dir_root_relative);
		// 	//  $this->params['pass'][1]
		// 	$this->setCommonVars(true);				
		// 	// $this->handleAjax();				
		// 	// $collaboration
		// 	$this->layout = "user_area_layout";
		// 	$this->set('columns', 2);
			

		// }








		

		function ajax_admin_index(){              
			// you are currently using the same address for serving the app as well as serving the listing used by the app,
			// and you think its are clever. only problem is
			// this is a failure of the design. 
			// it causes nested ajax from failing. 
			///debug($this->layout); exit;
			if(empty($this->isAjax)){
				//	$this->redirect('/my/userassets/');

			} else {

				$this->layout='nestedajax_layout';														

			}
			$this->setCommonVars();							
		}

		public function cpm($d = null){
			$this->layout='empty';
			//debug($this->request->pass);
			if(  $this->request->pass[0] =='link'){
				$b = $this->request->pass;
				unset($b[0]);
				$t = implode('/', $b);
				$this->set('d','<a href ="/'.$t.'">/'.$t.'</a>' );
			}else{
				$this->set('d',implode('/', $this->request->pass));				
			}
		}

		public function main_public(){
		}

		// public function dashboard(){
		// 	exit;
		// }

		public function downloads($data){
			$g = $this->request->pass;
			$this->set('newName',end($g));
			$p = str_replace('downloads/','','.'.$this->request->here);
			$this->set('total_page',$p);
			$this->layout = 'empty';
		}
				
		function admin_static_layouts(){
			$this->file_dir_root_relative = '../webroot/user_managed/static_layouts/';
			$this->admin_index('/user_managed/static_layouts/');
		}	    
				
		function admin_upload($j=''){
			$this->set('debug', true );
			$this->set('redirect', 'self' );
			$this->setCommonVars();
			//--
			$this->directoryPathArray .=',""';
			$this->set('directoryPathArray', $this->directoryPathArray );	
			//--
			if ($this->request->is('post')) {
				echo '{success: '.$this->Fileasset->saveFile($this->request->data, $this->file_dir_root_relative.$this->current_directory).'}';
				exit;
			}
		}       
		
		function move_file($j=''){
			debug("move_file"); exit;
		}       
		       		
		function directoryArrayToString($ar,$trailingSlash = true){
			$g = '';
			foreach ($ar as $key => $value) {
				if($key == count($ar)-1){
					$g .= $value;
					if ($trailingSlash) {
						$g .= '/';						
					}
				}else{
					$g .= $value.'/';						
				}
			}
			return $g; 
		}
		
		function admin_listing($g=""){              	
			$this->layout = "file_management_listing";  
			$this->setCommonVars();	
		}	    
		              	        
		function admin_preview($directory,$file){
			$this->layout="admin_file_preview_layout";
			$this->set('file',$file);			
		}

  		function admin_delete($directory=null,$file=null){
			// debug(" admin_delete "); exit;
			$file_name = $this->directoryArrayToString($this->params->params['pass'], false);
			$str = '';
			$ar = $this->params->params['pass'];
			for ($i=0; $i < count($ar)-1; $i++) { 
				$str.=$ar[$i].'/';
			}
			
			if(file_exists($this->file_dir_root_relative.$file_name)){
				unlink($this->file_dir_root_relative.$file_name);
			}

			$this->redirect($this->root_uri.$str);
  			// if(file_exists($this->file_dir_root_relative.$directory.'/'.$file)){
  			// 	if(unlink($this->file_dir_root_relative.$directory.'/'.$file)){
  			// 		$this->redirect('/admin/files/');
  			// 	};
  			// }
  			die;
  		}

		private function getLast($str){
			// for the back button.
			// this is way more complicated than it needs to be. query string to array, 
			$ar = explode('/', $str);
			$n = count($ar);
			$str2 = '';
			if($n > 1){		
				if($ar[$n-1]==''){
					for ($i=0; $i < $n-2; $i++) { 
						$str2 .= $ar[$i].'/';
					}
				} else {
					for ($i=0; $i < $n-2; $i++) { 
						$str2 .= $ar[$i].'/';
					}
				}
			}
			return $str2;
		}

		private function format_directory($inputString){
			$lowercaseString = strtolower($inputString);
			$formattedString = str_replace(' ', '_', $lowercaseString);		
			return $formattedString;
		}

		private function setCommonVars($isColabArea=false){
			$ar = $this->params->params['pass'];
			if($isColabArea){
				// $user_dir = $this->current_user['User']['user_dir'];
				//  debug($this->current_user['Collaboration']);
				//  debug($ar[0]);
				 $collab_dir = '';
				 foreach ($this->current_user['Collaboration'] as $key => $value) {
					if($value['id'] == $ar[0]){
						array_shift($ar);
						$collab_dir = $this->format_directory($value['name']);
						array_unshift($ar,$collab_dir);
						array_unshift($ar,'collaborations');
					}
				 }
				// debug($collab_dir);

				// $this->file_dir_root_relative = 'be0fbff5ded65f6524a26955bb72965cb6823249/collaborations/the_mississippi_burnouts/';				
			}
			$this->current_directory = $this->directoryArrayToString($ar );
			$this->dir_contents = $this->Fileutil->getDirectoryContents($this->file_dir_root_relative.$this->current_directory);
			//--
			$this->set('file_dir_root_relative',$this->file_dir_root_relative);
			$this->set('file_dir_root_absolute',$this->file_dir_root_absolute);			
			//--
			$this->set('target_directory',$this->current_directory ); //
			
			$this->set('filepath',$this->file_dir_root_absolute.$this->current_directory);			
			//--
			$this->set('dir_contents',$this->dir_contents);
			$this->set('listing',$this->dir_contents);
			$this->set('root_uri',$this->root_uri);
			//--	
			$ar = $this->params->params['pass'];
			$directoryPathArray = $ar;			
			
			
			//just use unset or whatever...this sucks
			// $directoryPathArray = explode(",",)
			// foreach ($ar as $key => $value) {
			// 	$directoryPathArray .= '"'.$value.'"';
			// 	if($key < count( $ar )-1){
			// 		$directoryPathArray .= ',';
			// 	}
			// }
			// debug($directoryPathArray); exit;

			$this->set('directoryPathArray', $directoryPathArray );	
			$this->set('back', $this->root_uri.$this->getLast($this->current_directory));		
		}
		
    } 
?>