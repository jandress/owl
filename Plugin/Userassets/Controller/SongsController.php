<?php
App::uses('AppController', 'Controller');
/**
 * Avatars Controller
 *
 * @property Avatar $Avatar
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SongsController extends AppController {

	public $components = array('Userassets.Forum');
	var $helpers = array('Userassets.Community');
	var $uses = array('Song','Forumentry');

	public function beforeFilter(){
		parent::beforeFilter();
	}

	public function index() {

	}

	

	public function view($id = 2) {
        $this->layout = "user_area_layout";
        $this->set('columns', 2);
        $g = $this->Forum->viewEntry($id,$this);
        // debug($g);  exit;
        $this->set('list', $g);
        $this->set('row_element', 'user_area/community/community_title_row');
   }




   public function collaborations_add(){
	// debug($this->params['pass']);
	$this->layout = "user_area_layout";
	$this->set('columns', 2);
	if ($this->request->is('post'))	 {
		$d = $this->request->data;
		$d['Forumentry']['user_id'] = $this->current_user['User']['id'];
		// $d['Forumentry']['course_id'] = $id;
		$d['Forumentry']['parent_id'] =	1;
		$date = new DateTime();
		$s = $date->format('Y-m-d H:i:s');
		$d['Forumentry']['timestamp'] =	$s;
		$b = $this->Forumentry->create($d);
		$p = $this->Forumentry->save();
		if($p){
			$this->redirect("/my/forumentry/".$p['Forumentry']['id']);
		}
	}
	// debug($this->params);
	$g = $this->params['pass'];
	array_unshift($g,"/users",$this->current_user['User']['user_dir'],'collaborations');
	$file = implode("/",$g);
	// debug($file);
	$this->set("file",$file);	 
	$this->set("data",$g);
   }

	public function my_index() {
		
	}


	public function my_view($id = null) {

	}

	public function my_add() {

	}

	public function my_edit($id = null) {
	}

	public function my_delete($id = null) {

	 }
}
