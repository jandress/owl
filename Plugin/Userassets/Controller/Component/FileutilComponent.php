<?php	
	class FileutilComponent extends Component {

		public function initialize(Controller $controller) {
			// saving the controller reference for later use
			$this->controller = $controller;
		}
			
		public function getDirectoryContents($directory_path,$sort=false){
			return $this->gdc($directory_path, $sort);
		}
			
		private function gdc($directory_path,$sort=false){
				$dirFiles1 = array();      
				$dirFiles = array();      
				$directory_path = str_replace('//','/',$directory_path.'/');
				
				if ($handle = opendir($directory_path)) {
				    while (false !== ($file = readdir($handle))) {
						if ( substr($file,0,1) !='.' && $file !="index.php" ) {
							$g = array(
								'item'=>$file,
								'type'=> is_dir($directory_path.$file) ? 'dir' :strtolower(substr($file, strrpos($file,'.')+1))
							);
			 				array_push($dirFiles1,$g);
						}
				    }
				    closedir($handle);
				}			              
				sort($dirFiles1);				
				return $dirFiles1;
		}
							
	
		public function getFileListingForOptions($directory_path,$options = null){
			// fileNameAsKeys
			// removeFileExtension
			// omitDirectories
			// ommittedFileNames
			$dirFiles =array();
			///			$dirFiles = array('');     
			if(isset($options['allowNullEntry'])){
				// instead of this, you should have an "add item at beginning of (and end of)  list funciton"
				$dirFiles['']='None';				
			} 
			if(!isset($options['fileNameAsKeys'])){
				// instead of this, you should have an "add item at beginning of (and end of)  list funciton"
				$options['fileNameAsKeys']=true;				
			} 
			//debug('1');
			if ($handle = opendir($directory_path)) {
			//debug('2');				
			    while (false !== ($file = readdir($handle))) {							

					if(isset($options['removeFileExtension'])){							
						$u = explode('.', $file);
						$fileExt = end($u);
						$t = basename($file, ".".$fileExt);
						$file = $t;
					}
					//debug($dirFiles);
					//debug($file);
					if($this->fileMeetsRequirements($file,$options,$directory_path)){
						//debug( $dirFiles);
						array_push( $dirFiles,$file);																					
					}
			    }
			    closedir($handle);
			
			}
			
			//debug('4');					
			sort($dirFiles);
			// keys as numbers or filenames?
			if(isset($options['fileNameAsKeys'])){							
				if($options['fileNameAsKeys']){
					$dirFiles1=array();
					foreach ($dirFiles as $key => $value) {
						$v = $value;
						if(isset($options['prependWithPath'])){							
							$v = $options['prependWithPath'].$value;
						}
						$dirFiles1[$v]=$value;
					}			
					$dirFiles = $dirFiles1;		
				}
			}				
			return $dirFiles;
		}
		
		public function getFilesAsKeysNoDirectories($directory_path,$options = null){
			// $options['ommittedFileNames'] = array()		
			$options = isset($options) ? $options : array();
			$options['omitDirectories'] = 1;			
			$options['fileNameAsKeys'] = 1;	
			return $this->getFileListingForOptions($directory_path,$options);
		}

		public function getFilesAsKeysNoDirectoriesAsXml($directory_path,$options = null){
			// $options['ommittedFileNames'] = array()					
			$options =  isset($options) ? $options : array();
			$options['omitDirectories'] = 1;// ANY VALUE IS TRUE			
			$g = $this->getFileListingForOptions($directory_path,$options);
			$n = "\n".'<doc>';
			foreach ($g as $key => $value) {
				$n .= "\n".'	<item>'."\n".'		<file>'.$value.'</file>'."\n".'		<seconds>2</seconds>'."\n".'	</item>';
			}
			$n .="\n".'</doc>'."\n";
			return $n;
		}		

		public function getFileListingNoDirectories($directory_path,$options = null){
			// $options['ommittedFileNames'] = array()					
			$options =  isset($options) ? $options : array();
			$options['omitDirectories'] = 1;// ANY VALUE IS TRUE			
			return $this->getFileListingForOptions($directory_path,$options);
		}

		public function getFileListing($directory_path,$options = null){
			// $options['omitDirectories'] = 1;// ANY VALUE IS TRUE
			// $options['ommittedFileNames'] = array()					
			$options =  isset($options) ? $options : array();
			$options['fileNameAsKeys'] = 1;	
			return $this->getFileListingForOptions($directory_path,$options);
		}

		public function getFileNamesAsKeys($directory_path,$options = null){
			// $options['omitDirectories'] = 1;// ANY VALUE IS TRUE
			// $options['ommittedFileNames'] = array()					
			$options =  isset($options) ? $options : array();
			$options['fileNameAsKeys'] = 1;	
			$options['removeFileExtension'] = 1;						
			return $this->getFileListingForOptions($directory_path,$options);
		}

		public function getFileNamesAsKeysNoDirectories($directory_path,$options = null){
			// $options['ommittedFileNames'] = array()		
			$options = isset($options) ? $options : array();
			$options['omitDirectories'] = 1;								
			return $this->getFileNamesAsKeys($directory_path,$options);
		}
		
		public function getDirectories($directory_path,$options = null){
			$dirFiles1 = array();      
			$dirFiles = array();      
			$directory_path = str_replace('//','/',$directory_path.'/');
			if ($handle = opendir($directory_path)) {
			    while (false !== ($file = readdir($handle))) {
					if ( substr($file,0,1) !='.' && $file !="index.php" && is_dir($directory_path.$file) ) {
		 				array_push($dirFiles1,$file);
					}
			    }
			    closedir($handle);
			}			              
			sort($dirFiles1);		
			//------------------------------------------	
			if(isset($options['fileNameAsKeys'])){							
				if($options['fileNameAsKeys']){
					$dirFiles2=array();
					foreach ($dirFiles1 as $key => $value) {
						$v = $value;
						if(isset($options['prependWithPath'])){							
							$v = $options['prependWithPath'].$value;
						}
						$dirFiles2[$v]=$value;
					}
					$dirFiles1 = $dirFiles2;		
				}
			}					
			return $dirFiles1;
		}

		private function fileMeetsRequirements($fileName,$options = null,$directory_path=null){
			if(substr($fileName,0,1)==='.'){
				return false;
			}
			if(!isset($options)){
				$options = array();
			}
			if(!isset($options['ommittedFileNames'])){
				$options['ommittedFileNames'] = array();
			}
			array_push($options['ommittedFileNames'],'.');
			array_push($options['ommittedFileNames'],'..');						
			foreach ($options['ommittedFileNames'] as $key => $value) {
				if($fileName==$value){
					return false;
				}
			}
			if(isset($options['omitDirectories'])){
				if(is_dir( $directory_path.'/'.$fileName ) ){
					return false;
				}
			}
			return true;
		}

		public function createDirectoryIfNotExists($directoryPath){
			$v = explode('/',$directoryPath);
			$g = '';
			if(file_exists($directoryPath)){
				return;
			}
			foreach ($v as $key => $value) {
					$g .= $value."/";				
					if(!file_exists($g)){
						mkdir($g, 0777, true);											
					}else{
						// echo($g.' exists ');						
					}
				echo("\n<br/>");					
			}	
			return true;
		}

		public function deleteDirAndContents($dirPath) {
		    if (! is_dir($dirPath)) {
		        throw new InvalidArgumentException("$dirPath must be a directory");
		    }
		    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
		        $dirPath .= '/';
		    }
		    $files = glob($dirPath . '*', GLOB_MARK);
		    foreach ($files as $file) {
		        if (is_dir($file)) {
		            self::deleteDirAndContents($file);
		        } else {
		            unlink($file);
		        }
		    }
		    rmdir($dirPath);
			return true;
		}

	}
	
?>