<?php

App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('Security', 'Utility');
App::uses('Auth', 'Component');


class UserAreaController extends UserassetsAppController {

	// 
	// var $helpers = array('Html',"Session" , "Js",'TwitterBootstrap.BootstrapHtml',  'TwitterBootstrap.BootstrapForm','UserManagement.UserManagementForm' );
	//  public $components = array('Acl', 'Session','Security','Auth','UserManagement','RequestHandler','Email');
	public $components = array('Userassets.Forum');
	var $helpers = array('Userassets.Forum');
	var $default_redirect = '/my/dashboard/';

	
	var $uses = array('Forum.Forumentry');
	

   

    function beforeFilter() {
		parent::beforeFilter();
		// debug($this->layout);
		// $this->set('page_script',"common.js");
		// $this->Security->requireSecure('login', 'checkout','signup','my_index','admin_edit','profile','password');
		// $this->Auth->allow(array("login",'logout',"signup","my_delete","lost_recovery_login","signup","publisher_signup"))
    }


/*


Plugin/Userassets/View/UserArea/collaborations_add.ctp
 /Userassets/View/UserArea/collaborations_edit.ctp
 /Userassets/View/UserArea/collaborations_index.ctp
 /Userassets/View/UserArea/collaborations_view.ctp
 /Userassets/View/UserArea/collaborator_public_page.ctp
 /Userassets/View/UserArea/dashboard.ctp
 /Userassets/View/UserArea/user_public_page.ctp
 /Userassets/View/UserArea/welcome_screen.ctp



 

 */


	public function dashboard() {
		 $this->layout = "user_area_layout";
		//$this->layout = "user_area_layout_2_col";
		// $this->set('columns', 2);
		$cu = Configure::read("current_user");
		$g = $this->Forumentry->getListingsForUser($cu['User']['id']);
		$this->set('list', $g);
		$this->set('row_element', 'user_area/community/community_title_row');
	}


	public function notifications() {
		$this->set('columns', 2);
		$this->layout = "user_area_layout";
	}


	public function collaborations_index() {
		$this->set('columns', 2);
		$this->layout = "user_area_layout";
	}


	public function community_index() {
		$this->layout = "user_area_layout";
		$this->set('columns', 2);
		$this->set('row_element', 'user_area/community/community_title_row');
	}


	public function collaborations_view($id=0) {
		$this->layout = "user_area_layout";
	}
	
	public function song_collaboration() {
		$this->layout = "user_area_layout";
	}


}
