<?php
	App::uses('AppController', 'Controller');
	/**
	 * Avatars Controller
	 *
	 * @property Avatar $Avatar
	 * @property PaginatorComponent $Paginator
	 * @property SessionComponent $Session
	 */
	class AudioPlayerController extends UserassetsAppController {

		var $components = array('Userassets.Fileutil');

		public function beforeFilter(){
			parent::beforeFilter();
		}


		/*


		params => array(
		'plugin' => 'Userassets',
		'controller' => 'AudioPlayer',
		'action' => 'inline_player',
		'named' => array(),
		'pass' => array(
			(int) 0 => 'player_dest1',
			(int) 1 => '1708545738994-8u2znyvv9el',
			(int) 2 => 'be0fbff5ded65f6524a26955bb72965cb6823249',
			(int) 3 => 'collaborations',
			(int) 4 => 'the_mississippi_burnouts',
			(int) 5 => 'raw_jams',
			(int) 6 => 'crosses.mp3'
		),
		'isAjax' => false
	)

	*/

		public function inline_player($guid, $file ) {
			$remainingArray = array_slice($this->params['pass'], 1);
			$if = implode("/", $remainingArray);
			$initial_file = '/users/'.$if;
			$this->set('initial_file', $initial_file);
			$this->set('guid', $guid);
			$this->layout = "ajax";
		}

		


		public function standalone_player($guid, $file ) {
			$remainingArray = array_slice($this->params['pass'], 1);
			$if = implode("/", $remainingArray);
			$initial_file = '/users/'.$if;
			$this->set('initial_file', $initial_file);
			$this->set('guid', $guid);
			$this->layout = "standalone_player";
		}



		
		
	}

?>