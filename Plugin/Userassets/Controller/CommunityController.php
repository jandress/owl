<?php

App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('Security', 'Utility');
App::uses('Auth', 'Component');


class CommunityController extends UserassetsAppController {

	// var $uses = array('MessagingSystem.Message','UserManagement.Ticket','UserManagement.User','UserManagement.Group','Publisher','UserManagement.Message');
	// var $helpers = array('Html',"Session" , "Js",'TwitterBootstrap.BootstrapHtml',  'TwitterBootstrap.BootstrapForm','UserManagement.UserManagementForm' );
	//  public $components = array('Acl', 'Session','Security','Auth','UserManagement','RequestHandler','Email');
	public $components = array('Userassets.Forum');
	var $helpers = array('Userassets.Community');


	var $default_redirect = '/my/dashboard/';

    function beforeFilter() {
		parent::beforeFilter();
		// $this->set('page_script',"common.js");
    }



	public function index() {
		 $this->layout = "user_area_layout";
		$g = $this->Forum->listing();
		$this->set('list', $g);
		$this->set('row_element', 'user_area/community/community_title_row');
	}




	public function view($id = 2) {
        $this->layout = "user_area_layout";
        $this->set('columns', 2);
        $g = $this->Forum->viewEntry($id,$this);
        // debug($g);  exit;
        $this->set('list', $g);
        $this->set('row_element', 'user_area/community/community_title_row');
   }




	// public function notifications() {
	// 	$this->set('columns', 2);
	// 	$this->layout = "user_area_layout";
	// }


	// public function collaborations_index() {
	// 	$this->set('columns', 2);
	// 	$this->layout = "user_area_layout";
	// }


	// public function community_index() {

	// 	$this->layout = "user_area_layout";
	// 	$this->set('columns', 2);
	// 	$this->set('row_element', 'user_area/community/community_title_row');
	// }


	// public function collaborations_view($id=0) {
	// 	$this->layout = "user_area_layout";
	// }
	
	// public function song_collaboration() {
	// 	$this->layout = "user_area_layout";
	// }


}
