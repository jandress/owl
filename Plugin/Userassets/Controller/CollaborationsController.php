<?php

App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('Security', 'Utility');
App::uses('Auth', 'Component');


class CollaborationsController extends UserassetsAppController {

	
	public $components = array('Forum.Forum','Userassets.Collab');
	var $helpers = array('Forum.Forum');
	public $uses= array("Userassets.Collaboration","Forum.Forumentry");
	
    function beforeFilter() {
		parent::beforeFilter();
		// $this->set('page_script',"common.js");
		// $this->Auth->allow(array("login",'logout',"signup","my_delete","lost_recovery_login","signup","publisher_signup"))
    }

	public function dashboard(){
		 $this->layout = "user_area_layout";
		$g = $this->Forum->listing();
		$this->set('list', $g);
		$this->set('row_element', 'user_area/community/community_title_row');
	}


	public function notifications() {
		$this->set('columns', 2);
		$this->layout = "user_area_layout";
	}


	public function collaborations_index() {
		$this->set('columns', 2);

		// $b = $this->Collab->getAndSetCollabsforViewFromCollabId(1,$this);
		// debug($this->current_user);
		$this->set('collaborations', $this->current_user['Collaboration']);
		$this->layout = "user_area_layout";
	}

	public function collaborations_view($id=0) {
		$this->layout = "user_area_layout";
		$c = $this->checkMembership($id);
		
		// $this->set('columns', 2);
		$this->set('collaboration',$c);
		$this->set('row_element', 'user_area/community/community_title_row');
		$cu = Configure::read("current_user");

		$g = $this->Forumentry->getListingsForCollaboration($id);
		// $g = $this->Forumentry->getListingsForUser($cu['User']['id']);
		$this->set('list', $g);
	}

	public function collaborations_add() {
		$this->set('columns', 2);
	}


	


	private function checkMembership($id){
		$found = false;
		$c = false;
		foreach ($this->current_user['Collaboration'] as $key => $value) {
			if( $value['id'] == $id ){
				$found = true;
				$c = $value;
			}
		}
		if($found == false){
			//throw new ErrorException($message, 0, $severity, $filename, $lineno);
			throw new ErrorException('Error. You\'re not part of this Collaboration');
		}
		$c['directory'] = $this->format_directory($c['name']);
		return $c;
	}
	
	public function song_collaboration() {
		$this->layout = "user_area_layout";
	}


	function format_directory($inputString) {
		$lowercaseString = strtolower($inputString);
		$formattedString = str_replace(' ', '_', $lowercaseString);		
		return $formattedString;
	}
	
	
	// public function files() {
	// 	$this->set('columns', 2);
	// 	$i = $this->params['pass'];
	// 	$collab_id = $i[0];
	// 	$directory = $this->format_directory($i[1]);
	// 	$this->checkMembership($collab_id);
	// }




}
