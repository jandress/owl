<?php


/*
 * Test for features on this system.
 */

$hasFileinfo = extension_loaded('fileinfo');
$hasImagick = extension_loaded('imagick');

/*
 * Bootstrap the `mm` library. We are putting the library into the include path which
 * is expected (by the library) in order to be able to load classes.
 */
$mm = dirname(dirname(__FILE__)) . DS . 'Lib' . DS . 'mm';
//pr($mm);exit;
if (strpos(ini_get('include_path'), $mm) === false) {
	ini_set('include_path', $mm.DS.'src'.PATH_SEPARATOR.ini_get('include_path'));
}
// 
// /**
//  * Configure the MIME type detection. The detection class is two headed which means it
//  * uses both a glob (for matching against file extensions) and a magic adapter (for
//  * detecting the type from the content of files). Available `Glob` adapters are `Apache`,
//  * `Freedesktop`, `Memory` and `Php`. These adapters are also available as a `Magic`
//  * variant with the addtion of a `Fileinfo` magic adapter. Not all adapters require
//  * a file to be passed along with the configuration.
//  *
//  * @see TransferBehavior
//  * @see MetaBehavior
//  * @see MediaHelper
//  */
require_once 'Mime/Type.php';

if ($hasFileinfo) {
	Mime_Type::config('Magic', array(
		'adapter' => 'Fileinfo'
	));
} else {
	Mime_Type::config('Magic', array(
		'adapter' => 'Freedesktop',
		'file' => $mm . DS . 'data' . DS . 'magic.db'
	));
}
if ($cached = Cache::read('mime_type_glob')) {
	Mime_Type::config('Glob', array(
		'adapter' => 'Memory'
	));
	foreach ($cached as $item) {
		Mime_Type::$glob->register($item);
	}
} else {
	Mime_Type::config('Glob', array(
		'adapter' => 'Freedesktop',
		'file' => $mm . DS . 'data' . DS . 'glob.db'
	));
	Cache::write('mime_type_glob', Mime_Type::$glob->to('array'));
}

?>