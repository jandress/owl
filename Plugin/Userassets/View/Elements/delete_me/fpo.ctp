<div>
	<div class="admin_file_listing_toolbar navbar navbar-default " style="z-index:10">
	<div class="admin_file_listing_container">
		<div class="container-fluid bm_4_px">		
			

		
			
			<a class="btn btn-sm btn-default" href="/my/userassets/"><span class="glyphicon glyphicon-home"></span></a>
			

						
			
			
			 

			<div class="upload_toolbar_link btn-group  pull-right">
				
				

					<a class="btn btn-sm btn-default fb_ajax_link" href="/my/userassets/addir/">
						<span class="icon-folder-close ">&nbsp;</span>
						 New Directory.
					</a>

					<a class="btn btn-sm btn-default fb_ajax_link" href="/my/userassets/upload/">
						<span class="icon-upload ">&nbsp;</span>
						Upload New File.
					</a>

							</div>

		</div>		
	
	</div>
	<div class="clear">&nbsp;</div>			
</div>
	
	

<table id="projects" class="table table-striped table-bordered table-hover">
	<thead>
		<tr class="gb_28">
			<th class="col-md-1">&nbsp;</th>
			<th class="span9">Name</th>
			<th class="actions col-md-2" style="text-align:center">Options</th>
		</tr>
	</thead>			
	<tbody>
			<tr id="0" class="altrow">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
				</span> &nbsp;<span class="glyphicon glyphicon-folder-close"></span>&nbsp; <span class="file_listing_label"><a class="fb_ajax_link" href="/my/userassets/jams">jams</a>				</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">
									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/jams">View</a>
									</li>			                	

									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/jams">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/jams">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/jams">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/jams">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/jams">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;jams\&quot;?');" class="" href="/assets/rmdir/jams">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
			<tr id="1">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
				</span> &nbsp;<span class="glyphicon glyphicon-folder-close"></span>&nbsp; <span class="file_listing_label"><a class="fb_ajax_link" href="/my/userassets/releases">releases</a>				</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">
									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/releases">View</a>
									</li>			                	

									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/releases">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/releases">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/releases">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/releases">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/releases">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;releases\&quot;?');" class="" href="/assets/rmdir/releases">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
			<tr id="2" class="altrow">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
											<a href="#sky_lake.jpg" class="tracklist">
								sky_lake.jpg							</a>
										</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">
									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/sky_lake.jpg">View</a>
									</li>			                	

									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/sky_lake.jpg">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/sky_lake.jpg">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/sky_lake.jpg">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/sky_lake.jpg">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/sky_lake.jpg">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;sky_lake.jpg\&quot;?');" class="" href="/assets/rmdir/sky_lake.jpg">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
			<tr id="3">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
				</span> &nbsp;<span class="glyphicon glyphicon-folder-close"></span>&nbsp; <span class="file_listing_label"><a class="fb_ajax_link" href="/my/userassets/tracks">tracks</a>				</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">
									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/tracks">View</a>
									</li>			                	

									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/tracks">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/tracks">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/tracks">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/tracks">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/tracks">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;tracks\&quot;?');" class="" href="/assets/rmdir/tracks">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
			<tr id="4" class="altrow">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
				</span> &nbsp;<span class="glyphicon glyphicon-folder-close"></span>&nbsp; <span class="file_listing_label"><a class="fb_ajax_link" href="/my/userassets/uploads">uploads</a>				</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">
									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/uploads">View</a>
									</li>			                	

									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/uploads">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/uploads">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/uploads">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/uploads">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/uploads">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;uploads\&quot;?');" class="" href="/assets/rmdir/uploads">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
			<tr id="5">
	
	
	
		<td style="text-align:center">
			<!-- <img src = "/owls/small/"> -->
		</td>
	
	
	
	
	







			<td>
				<span class="file_listing_index">
				</span> &nbsp;<span class="glyphicon glyphicon-folder-close"></span>&nbsp; <span class="file_listing_label"><a class="fb_ajax_link" href="/my/userassets/vocab">vocab</a>				</span>
			</td>








		


			<td>
				<span class="file_listing_index">
										<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
														<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">
									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/vocab">View</a>
									</li>			                	

									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/vocab">Download</a>
									</li>			                	

									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/vocab">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/vocab">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/vocab">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/vocab">Open in standalone player</a>
									</li>			


									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;vocab\&quot;?');" class="" href="/assets/rmdir/vocab">
											Delete File
										</a>		
									</li>			
									

								</ul>
							</li>
						</ul>
										</span>
			</td>



		</tr>				
		</tbody>
</table>	
</div>