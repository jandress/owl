
	

<table id="" class="table table-striped table-bordered table-hover">
	<thead>
		<tr class="">
			<th class="col-md-1">&nbsp;</th>
			<th class="span9">&nbsp;</th>
			<th class="actions col-md-2" style="text-align:center">&nbsp;</th>
		</tr>
	</thead>			
	<tbody>
	<?php

	$variable = array(
		array(
			'name' => 'Uploads',
			'link' => 'uploads'
		),
		array(
			'name' => 'Workspace',
			'link' => 'workspace'
		),

		array(
			'name' => 'Idea Bin',
			'link' => 'idea_bin'
		),
		array(
			'name' => 'Raw Jams',
			'link' => 'raw_jams'
		),

		array(
			'name' => 'Public',
			'link' => 'public'
		)


	);

	// debug($variable);

	foreach ($variable as $key => $value): ?>



		<tr id="0" class="altrow">
		
			<td style="text-align:center">
				<!-- <img src = "/owls/small/"> -->
			</td>
			<td style = "font-size:14pt; padding:19px"  >
				<span class="file_listing_index" ></span> &nbsp;<div class="pull-left glyphicon glyphicon-folder-close">
					&nbsp;
				</div>&nbsp; 
				<div style = "font-size:14pt; margin-top:-2px" class="file_listing_label pull-left ">&nbsp; 
					<a class="fb_ajax_link" target = "_blank" href="/my/userassets/collaborations/<?php echo $collaboration['directory']; ?>/<?php echo $value['link']; ?>" >
						<?php echo $value['name']; ?> </a>			
					</div>
			</td>









			<td>
				<span class="file_listing_index">
						<ul style="list-style:none; padding:0">
							<li class="dropdown" style="text-align:center">
								<a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown">
								<span class="glyphicon glyphicon-plus-sign"></span>
							</a>
								<ul class="dropdown-menu " role="menu" style="text-decoration:none;">
									<li>	
										<a target="_blank" href="/playtrack/collaborations">Open File in Standalone Player</a>
									</li>			           
									<li>	
										<a target="_blank" href="/users/be0fbff5ded65f6524a26955bb72965cb6823249/collaborations">View</a>
									</li>			                	
									<li>	
										<a href="/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/collaborations">Download</a>
									</li>			                	
									<li role="presentation" class="divider"></li>			                   			                   									                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm//users/be0fbff5ded65f6524a26955bb72965cb6823249/collaborations">Copy File Path to clipboard</a>
									</li>				                    			                    
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/link/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/collaborations">Copy Download Link Syntax to clipboard</a>
									</li>	
									<li>	
										<a data-target="#myModal" role="button" data-toggle="modal" href="/cpm/downloads/users/be0fbff5ded65f6524a26955bb72965cb6823249/collaborations">Copy Download Path to clipboard</a>
									</li>			
									<li>	
										<a href="/my/Userassets/playtrack/users/be0fbff5ded65f6524a26955bb72965cb6823249/collaborations">Open in standalone player</a>
									</li>			
									<li>	
										<a onclick="return confirm('Are you sure you want to delete \&quot;collaborations\&quot;?');" class="" href="/assets/rmdir/collaborations">
											Delete File
										</a>		
									</li>			
								</ul>
							</li>
						</ul>
					</span>
			</td>










		</tr>
	<?php endforeach;?>
	</tbody>
</table>	

<div class = "clear clearfix tm_4_em">&nbsp;</div>