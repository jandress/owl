    <div class = "nav ">
            <div class="btn-group" >
                <div>
                    <a id = "step_back_btn_<?php echo  $guid; ?>" class = "btn">
                        <span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span>
                    </a>
                    <a id = "back_btn_<?php echo  $guid; ?>" class = "btn">
                        <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
                    </a>
                    <a id = "play_btn_<?php echo  $guid; ?>" class = "btn">
                        <span id="playpause_<?php echo  $guid; ?>" class="glyphicon glyphicon-play" aria-hidden="true"></span>
                    </a>
                    <a id = "forward_btn_<?php echo  $guid; ?>" class = "btn">
                        <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
                    </a>
                    <a id = "step_forward_btn_<?php echo  $guid; ?>" class = "btn">
                        <span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span>
                    </a>
                    <a id = "stop_btn_<?php echo  $guid; ?>" class = "btn">
                        <span id="stop_<?php echo  $guid; ?>" class="glyphicon glyphicon-stop" aria-hidden="true"></span>
                    </a>												
                </div>		
            </div>	
            <span id = "counter_<?php echo  $guid; ?>">00:00</span>
            <div id = "songLabel_<?php echo  $guid; ?>" style = "color:#ffffff" class = "pull-right tm_10_px"></div>
            <div class = "clear clearfix bm_3_px">&nbsp;</div>
    </div>	

