


<div class = "container-fluid">
    <div id = "waveform_container">
        <canvas id = "waveFormBackground" style = "border: none; border-radius:15px; height:140px; position:absolute; background-color:#33363b " class = "gs_0 "></canvas>
        <canvas id = "waveFormBox" style = "border: none; height:140px; position:relative; " class = " "></canvas>
    </div>
</div>


<div class = "container-fluid">
    <div class = "nav ">
            <div class="btn-group" >
                <div>
                    <a id = "step_back_btn" class = "btn">
                        <span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span>
                    </a>
                    
                    <a id = "back_btn" class = "btn">
                        <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
                    </a>
                    <a id = "play_btn" class = "btn">
                        <span id="playpause" class="glyphicon glyphicon-play" aria-hidden="true"></span>
                    </a>
                    <a id = "forward_btn" class = "btn">
                        <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
                    </a>
                    <a id = "step_forward_btn" class = "btn">
                        <span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span>
                    </a>
                    <a id = "stop_btn" class = "btn">
                        <span id="playpause" class="glyphicon glyphicon-stop" aria-hidden="true"></span>
                    </a>												
                </div>		
            </div>	
            <span id = "counter">00:00</span>
            <div id = "songLabel" style = "color:#ffffff" class = "pull-right tm_10_px"></div>
            <div class = "clear clearfix bm_3_px">&nbsp;</div>

    </div>	

</div>







<?php 
    // /$filepath = "/"."users/".$current_user['User']['user_dir'];// ."/jams/";
    // debug($filepath);
    if( isset($listing)){
        $g = array();
        foreach ($listing as $key => $value) {
            if($value=="." || $value==".."|| $value==".DS_Store") continue;
            array_push($g,$value);
        }
        
        if(count($g) && !isset($initial_file)){
            // item
            // type
            foreach ($g as $key => $value) {
                // debug($value);
                if(
                    $value['type'] == "mp3" ||
                    $value['type'] == "mp4" ||
                    $value['type'] == "wav"
                    ){
                        $filename = $value['item'];
                        break;
                }
            }
            $initial_file = '';
            if(
                isset($filepath) &&
                isset($filename)    
            ){
                $initial_file = $filepath."/".$filename;
            }
            
        }
    }
    // $file_path = "users/".$current_user['User']['user_dir']; //."/jams/";
    // debug($initial_file);
    // debug($filepath);
    // debug($initial_file);

?>



<script>





var filepath = "<?php echo $filepath;?>";
//  console.log(filepath);

// Waveform
var waveFormBoxCanvas = document.getElementById("waveFormBox");
var waveFormBackgroundCanvas = document.getElementById("waveFormBackground");

var waveFormBoxContext = waveFormBoxCanvas.getContext("2d");
var waveFormBackgroundContext = waveFormBackgroundCanvas.getContext("2d");
//---------------
var duration = 0;
waveFormBoxContext.fillStyle = "#e4a138";
waveFormBackgroundContext.fillStyle = "#e4a138";

var boxWidth = $("#waveform_container").width();
var boxHeight = $("#waveform_container").height();
$("#waveFormBox").width(boxWidth)
$("#waveFormBackground").width(boxWidth)


waveFormBoxContext.canvas.width  = waveFormBackgroundContext.canvas.width = boxWidth;
waveFormBoxContext.canvas.height  = waveFormBackgroundContext.canvas.height = boxHeight;

// end Waveform

//player stuff
window.AudioContext = window.AudioContext || window.webkitAudioContext;
const audioContext = new AudioContext();
let currentBuffer = null;



var timer = setInterval(function(){
    if(player.currentTime == 0) return;
    function millisToMinutesAndSeconds(millis) {
      var minutes = Math.floor(millis / 60000);
      var seconds = ((millis % 60000) / 1000).toFixed(0);
      return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    }
    var per = player.currentTime / player.duration
    $("#counter").html(millisToMinutesAndSeconds(player.currentTime*1000)+" / "+millisToMinutesAndSeconds(duration*1000))
    waveFormBackgroundContext.clearRect(0, 0, boxWidth, boxHeight);
    waveFormBackgroundContext.fillStyle = "#222222";
    waveFormBackgroundContext.beginPath();
    waveFormBackgroundContext.fillRect(0, 0, boxWidth*per, boxHeight);

}, 1000)


const visualizeAudio = url => {
    fetch(url)
    .then(response => response.arrayBuffer())
    .then(arrayBuffer => audioContext.decodeAudioData(arrayBuffer))
    .then(audioBuffer => visualize(audioBuffer));
};
const visualize = audioBuffer =>{
    B = filterData(audioBuffer)
    var j=0			
    var K 
    waveFormBoxContext.clearRect(0, 0, boxWidth, boxHeight);
    duration = player.duration;
    waveFormBoxContext.beginPath();
    for (var i = 0; i < B.length; i++) {
        // waveFormBoxContext.moveTo(j, boxHeight/2);
        K = B[i]
        k = (boxHeight/2) - (boxHeight/2) * K
        waveFormBoxContext.strokeStyle = '#e4a138';
        waveFormBoxContext.moveTo(j, k);
        // waveFormBoxContext.moveTo(j, boxHeight/2);				
        k2 = (boxHeight/2) + (boxHeight/2)*K


        waveFormBoxContext.lineTo(j, k2);
        j+=3
    }
    waveFormBoxContext.stroke();			
}


const filterData = audioBuffer => {
  const rawData = audioBuffer.getChannelData(0); // We only need to work with one channel of data
   // const rawData = normalizeData(audioBuffer.getChannelData(0))
  // console.log(rawData)
  const samples = boxWidth; // Number of samples we want to have in our final data set
  const blockSize = Math.floor(rawData.length / samples); // the number of samples in each subdivision
  const filteredData = [];
  for (let i = 0; i < samples; i++) {
    let blockStart = blockSize * i; // the location of the first sample in the block
    let sum = 0;
    for (let j = 0; j < blockSize; j++) {
      sum = sum + Math.abs(rawData[blockStart + j]) // find the sum of all the samples in the block
    }
    filteredData.push(sum / blockSize); // divide the sum by the block size to get the average
  }
  return filteredData;
}






<?php 
if ( 
    isset($initial_file) && 
    isset($filename) && 
    isset($filepath)
) { ?> 
    var player = new Audio("<?php echo $initial_file; ?>");
    player.src = "<?php echo $initial_file; ?>";
    visualizeAudio(player.src )
    var b = "<?php echo $filename; ?>";
    $("#songLabel").html(
        b.replace("<?php echo $filepath; ?>","")
        )   

    player.play();
<?php }else{
    ?> var player = new Audio(); <?php 
}
?>



$(window).ready(function(){
        $(".tracklist").click(function(e){
            var src = filepath+"/"+$(e.currentTarget).attr("href").replace("#","");
            console.log(src);
            $("#songLabel").html($(e.currentTarget).attr("href").replace("#",""))   
            console.log(filepath)
            console.log(filepath)
            console.log(src)
            player.src = src
            player.play();
            visualizeAudio(src)			
            return false
        });


        $(".songlist_link").click(function(e){

            var src = filepath+$(e.currentTarget).attr("href").replace("#","");
            $("#songLabel").html($(e.currentTarget).attr("href").replace("#",""))
            player.src = src
            player.play();
            visualizeAudio(src)			
            return false
        });


        $("#play_btn").click(function(){
            console.log(player)
            if($("#playpause").hasClass("glyphicon-play")){
                $("#playpause").removeClass("glyphicon-play").addClass("glyphicon-pause");	
                // player =  new Audio("./mp3s/"+track).play()
                player.play();

            }else{
                $("#playpause").removeClass("glyphicon-pause").addClass("glyphicon-play");	
                player.pause();
            }	
        })

        $("#waveform_container").click(function(e){
            const rect = waveFormBackgroundCanvas.getBoundingClientRect()
            const x = event.clientX - rect.left
            const y = event.clientY - rect.top
            player.currentTime = (x  / boxWidth )*player.duration
            return false
        });


        $("#stop_btn").click(function(e){
            player.pause();
            player.currentTime = 0;
            clearInterval(timer);
            console.log("stop")
        });





})





























// visualizeAudio("./mp3s/ecoomic_systems.wav")

/*

AudioBuffer {length: 5061147, duration: 114.76523809523809, sampleRate: 44100, numberOfChannels: 1}
duration
: 
114.76523809523809
length
: 
5061147
numberOfChannels
: 
1
sampleRate
: 
44100
[[Prototype]]
: 
AudioBuffer





AudioBuffer {length: 18313627, duration: 415.27498866213153, sampleRate: 44100, numberOfChannels: 2}
duration
: 
415.27498866213153
length
: 
18313627
numberOfChannels
: 
2
sampleRate
: 
44100
[[Prototype]]
: 
AudioBuffer


*/




</script>