<table id="projects" class="table table-striped table-hover"  >
		
	<tbody>
	<?php


	if(!empty($listing)):
	$i=0;
	foreach ($listing as $key => $value): 
		$file_type = $value['type'];	
		$object_type = "dir";
		$icon = $file_type; ?>
		<tr id="<?php echo $key; ?>" <?php echo(($i++ % 2 == 0) ? ' class="altrow"' : '')?> >		
		<?php
		switch($file_type){
			case 'dir':
				$object_type = "dir";
				$icon = 'glyphicon glyphicon-folder-close';
				echo $this->element('user_area/file_browser/file_listing_table_directory_row',array(
					'value' => $value,
					'type' => $object_type,
					'icon' => $icon
					,'i'=>$i
				));
			break;
			case 'm4v':
			case 'mp3':
			case 'wav':				
				$object_type = "audio";		
				$icon = 'glyphicon glyphicon-facetime-video';
				echo $this->element('user_area/file_browser/file_listing_table_audio_row',array(
					'value' => $value,
					'type' => $object_type,
					'icon' => $icon
					,'i'=>$i
				));
			break;
			case 'png':
			case 'jpg':
			case 'jpeg':
				$object_type = "image";						
				$icon = 'glyphicon glyphicon-picture';
				echo $this->element('user_area/file_browser/file_listing_table_image_row',array(
					'value' => $value,
					'type' => $object_type,
					'icon' => $icon
					,'i'=>$i
				));
			break;
		}	?>
		</tr>
	<?php 
	endforeach;
	else: 
	?>	
		<tr>
			<td colspan=4>No files were found.</td>
		</tr>           
		<?php endif ?>
	</tbody>
</table>