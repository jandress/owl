<div class = "admin_file_listing_toolbar tm_2_em  " style = 'z-index:10;'>
	<div class = "admin_file_listing_container">
		<div class="container-fluid bm_4_px">		
			

		
			
			<a class = "btn btn-sm " href = "/my/userassets/" ><span class = "glyphicon glyphicon-home"></span></a>
			

			<?php 
				$link = '';
				//debug($file_dir_root_relative); 
				//  debug($directoryPathArray); 

				foreach ($directoryPathArray as $key => $value): 
					$link.=$value.'/';
					$icon =  "glyphicon-folder-\close";
					if($key == count($directoryPathArray)-1){
						$icon =  "glyphicon-folder-open";

					}
					// 
				?>
				 
				<a class = "btn btn-sm " href = "/my/userassets/<?php echo $link; ?>"><span class = "glyphicon <?php echo $icon; ?> ">&nbsp;</span><?php echo $value; ?></a>

				<?php
			
			endforeach; ?>
			
			
			
			<?php if (!is_writable($file_dir_root_relative.''.$target_directory)): ?>
				<h5 class="tm_7_px">Write Protected Directory</h5>
			<?php endif ?> 

			<div class = "upload_toolbar_link btn-group  pull-right"  >
				<?php if($target_directory!=''): ?>
					<?php /*
							<a class = 'btn btn-small' href = '<?php echo $back; ?>' alt = "Back">
							<a class = 'btn btn-small' onclick = 'back()' alt = "Back">
							
						*/
					 ?>
					<a class = 'btn btn-sm  fb_ajax_link_back' href = '<?php echo $back; ?>'>
						<span class = "glyphicon glyphicon-arrow-left" >&nbsp;</span>
						Back
					</a>
				<?php endif ?>

				<?php if (is_writable($file_dir_root_relative.''.$target_directory)): ?>


					<a class = 'btn btn-sm  fb_ajax_link'  data-target="#myModal" data-toggle="modal"  href = '<?php echo $root_uri."addir/".$target_directory; ?>'>
						<span class = "glyphicon glyphicon-folder-close " >&nbsp;</span>
						 New Directory.
					</a>

					<a data-target="#myModal" role="button" data-toggle="modal"  class = 'btn btn-sm  fb_ajax_link' href = '<?php echo $root_uri ?>upload/<?php echo($target_directory); ?>' >
						<span class = "glyphicon glyphicon-upload " >&nbsp;</span>
						Upload New File.
					</a>

				<?php else: ?>


					<a class = 'btn btn-sm disabled btn-danger fb_ajax_link'  >
						<span class = "icon-upload " >&nbsp;</span>
						 New Directory.
					</a>
					

					<a class = 'btn btn-sm disabled btn-danger  fb_ajax_link' >
						<span class = "icon-upload " >&nbsp;</span>
						Upload New File.
					</a>


				<?php endif ?>
			</div>

		</div>		
	
	</div>
	<div class="clear">&nbsp;</div>			
</div>

