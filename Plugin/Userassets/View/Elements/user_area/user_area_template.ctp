<?php // layout is user_area_layout.ctp ?>
<? 

if(!isset($columns)){
    $columns = 3;
}





if ($columns == 3 ): ?>
<div class="container-fluid tm_2_em ">
        <div id="user_left_sidebar" class=" col-md-2">
            <?php
                foreach ($left_sidebar_elements as $key => $value) {
                    echo $this->element($value); 
                }
            ?>
        </div>    

        <div  id="user_main_content"  class="col-md-8">
            <?php
                foreach ($main_content_elements as $key => $value) {
                    echo $this->element($value); 
                }
            ?>
        </div>

        <div  id="user_right_sidebar"  class="col-md-2">
            <?php
                foreach ($right_sidebar_elements as $key => $value) {
                    echo $this->element($value); 
                }
            ?>
        </div>
</div>
<?php endif; ?>


<? 
if ($columns == 2 ): ?>
<div class="container-fluid tm_2_em ">
        <div id="user_left_sidebar" class=" col-md-2">
            <?php
                foreach ($left_sidebar_elements as $key => $value) {
                    echo $this->element($value); 
                }
            ?>
        </div>    

        <div  id="user_main_content"  class="col-md-10">
            <?php
                foreach ($main_content_elements as $key => $value) {
                    echo $this->element($value); 
                }
            ?>
        </div>
</div>
<?php endif; ?>




