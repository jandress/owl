<?php
		$badges = array(
				"badge_small_general.png",
				"badge_small_grad.png",
				"badge_small_greek.png",
				"badge_small_hipster.png",
				"badge_small_judge.png",
				"badge_small_nerd.png",
				"badge_small_sherlock.png",
				"badge_small_twain.png",
				"badge_small_world.png",
				"badge_small_zombie.png"
		);
		$fe = $data['Forumentry'];
		$d = $fe;
		debug($fe);

		$user = $data['User'];
		if($fe['child_count'] == 1){
			$comment_string = ", 1 Comment";
		} else {
			$comment_string = ", ".$fe['child_count'].' comments';
		}
?>



<div class="navbar navbar-inverse">
	<a class="" href="/my/forum/<?php echo $data['Course']['id']; ?>"><h2 class=" col-sm-4"><?php echo $data['Course']['name']; ?></h2></a>
	<ul class="nav navbar navbar-nav pull-right">
		<li class = "active">
			<a class="" href="/my/forum/<?php echo $data['Course']['id'] ?>">Forum Home</a>
		</li>
	</ul>
</div>

<div class = "tm_1_em container-fluid">
		<div class=" forum_entry" id="e_<?php echo($fe['id']); ?>">
		<!-- arrows and avatar-->
		<div>
			<div style="width:120px">
				<?php
					$downvoted = $upvoted = $hasVote = false;
					if (!empty($data['Vote'][0])) {
						$hasVote = true;
						$upvoted = $data['Vote'][0]['value'];
						$downvoted = !$upvoted;
					}
					// debug($hasVote);
					// debug($upvoted);
					// debug(is_null($upvoted));
				 ?>
				<div class="forum_voter_arrows">
						<a href="#<?php echo $d['id']; ?>"><div class="arrow up <?php if($upvoted){ echo " upvoted "; } ?>">&nbsp;</div></a>
						<div class="rank_number">
								<?php
								echo($d['upvotes']-$d['downvotes']);
								?>
						</div>
						<a href="#<?php echo $d['id'] ?>"><div class="arrow down <?php if($downvoted){ echo " downvoted "; } ?>">&nbsp;</div></a>
				</div><!-- end forum_voter_arrows -->
			  <div class="avatar pull-left">
			    <img src="/owls/<?php echo $badges[rand(1,8)] ?>" alt="" />
			  </div>
			</div>
		</div>
		<!-- Title, copy and post action links -->
		<div class="col-sm-10 forum_entry_field">
		  <h3>
		    <?php echo $fe['title']; ?>
		  </h3>
		  <div class="">
		    <h6 class="gt_15" style="display:inline">Submitted yesterday by</h6>
				<h6 class="gt_2" style="display:inline">
					<a href="/my/forum/user/<?php echo $user['id'];?>"><?php echo $user['username'];?></a>
				</h6><h6 class="gt_15" style="display:inline"><?php echo $comment_string; ?></h6>
		  </div>
		  <div class="tm_1_em col-sm-10 pl_0" >
		    <div class = "r5" style="border:1px solid #555555; padding:10px;">
		        <p>
		          <?php echo $fe['copy']; ?>
		        </p>
		    </div>
				<ul>
		      <li>
						1 Comment
					</li>
		      <li><a class = " edit_btn" href="#<?php echo $fe['id']; ?>" >Share</a></li>
					<?php if ($user['id'] == $current_user['User']['id']): ?>
						<li>
								<a class = "" href="/my/forumentry/edit/<?php echo $fe['id']; ?>" >Edit</a>
						</li>
						<li>
								<a class = "" href="/my/forumentry/delete/<?php echo $fe['id']; ?>/<?php echo  $fe['id'] ?>" >Delete</a>
						</li>
					<?php endif; ?>
		    </ul>
		  </div>
		</div><!-- End Title, copy and post action links -->
		<div class="clear clearfix">&nbsp;</div>
		<div class="clear clearfix bm_6_em">&nbsp;</div>
		<hr/>
		
		<!--  text input box-->
		<div class="col-sm-6 tm_1_em">
			<?php echo $this->BootstrapForm->create('cc', array('class' => ''));?>
			<?php
					// echo $this->BootstrapForm->textarea('copy');
					?>
						<textarea style = "background-color:#202020; border-color:#454545; color:#ececec; border-radius:15px;" name="data[cc][copy]" id="ccCopy"></textarea>
					<?php
					echo $this->BootstrapForm->hidden('action',array("value"=>'new'));
					echo $this->BootstrapForm->hidden('parent_id',array("value"=>$fe['id']));
					echo $this->BootstrapForm->hidden('root_id',array("value"=>$fe['id']));
					echo $this->BootstrapForm->submit('Submit', array('action' => 'submit', 'class'=>"btn btn-lrg btn-primary pull-right tm_2_em"));
					echo $this->BootstrapForm->end();
			?>
		</div>
		<div class="clear clearfix ">&nbsp;</div>
		<div class="tm_4_em">
			<?php if (!empty($data['ChildForumentry'])): ?>
				<?php foreach ($data['ChildForumentry'] as $key => $value): ?>
							<div class="child_comment">
										<?php
												echo $this->element('Forum.forum_discussion_row', array('badges'=>$badges,'data'=>$value,'root_id' => $fe['id']));
												echo '<div class="clear clearfix bm_2_em">&nbsp;</div>';
										?>
							</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>




</div>
<?php // debug($data); ?>


<script src = "/forum/js/forum_ui.js" type="text/javascript"></script>
