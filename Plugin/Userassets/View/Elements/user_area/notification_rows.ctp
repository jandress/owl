<div class="tm_5_em">
<h2>Notifications</h2>
<div class="tm_2_em"></div>
    



<div class="alert alert-primary">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>
        <h4>Welcome to Sonosphere!</h4>
        <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Labore consectetur repellat nobis, distinctio aliquid sapiente placeat at error maxime maiores? Alias ducimus eius ipsam quos quaerat cupiditate. Fugit, tenetur quia?
        </p>

        <a class= "btn btn-primary">Take a tour</a>

    </strong> 
</div>



<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>
        <h4>
            You're in a collaboration: <em>The Mississippi Burnouts</em>
        </h4>
        <a href = "/my/stub/collaboration_page" class= "btn btn-success">Go there</a>


    </strong> 
</div>



<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>
        <h4>You haven't studied in 63 days!</h4>
        <p>Are you Stagnating?!?!</p>
    </strong> 
</div>










                </div>