<h3>File Space</h3>
<img style  = "width:100%; margin-left:-5%" src="/img/pie.png" alt="">




<?php

function getDirSize($dirPath) {
    $size = 0;
    $dir = new RecursiveDirectoryIterator($dirPath, RecursiveDirectoryIterator::SKIP_DOTS);
    $files = new RecursiveIteratorIterator($dir, RecursiveIteratorIterator::CHILD_FIRST);
    foreach ($files as $file) {
        if ($file->isFile()) {
            $size += $file->getSize();
        }
    }
    return $size;
}

// Usage example
$directoryPath = getcwd().'/users/be0fbff5ded65f6524a26955bb72965cb6823249';
$directorySize = getDirSize($directoryPath);
$g = number_format($directorySize / 1048576, 2);
echo "Directory size: $g MB";



// echo getcwd().'/users/be0fbff5ded65f6524a26955bb72965cb6823249';
?>

<h4>You have used 60% of your alloted space</h4>