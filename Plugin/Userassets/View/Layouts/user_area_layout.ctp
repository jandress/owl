<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="/favicon.ico">
      <title><?php echo $title_for_layout; ?></title>

      <!-- user_area_layout -->
      <?php if (file_exists("css/stylesheet/".$settings['stylesheet'])): ?>
              <link href="/css/stylesheet/<?php echo $settings['stylesheet']; ?>" rel="stylesheet" title = "theme">
      <?php else: ?>
              <link href="/css/stylesheet/bootstrap.min.css" rel="stylesheet"  title = "theme">
      <?php endif;
      //debug(file_exists("css/stylesheet/".$settings['stylesheet']));exit;
      ?>
      <!-- Custom styles for this template -->
      <link href="/css/dashboard.css" rel="stylesheet">
      <link href="/css/layout_aids.css" rel="stylesheet">
      <link href="/css/icons.css" rel="stylesheet">
      <link href="/css/calendar.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="/PagekwikTools/jquery-ui/jquery-ui.min.css">
      <link href="/Quizmodules/css/pnotify.custom.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="/PagekwikTools/wysihtml5/bootstrap3-wysihtml5.min.css">
      
      
      <link href="/css/style.css" rel="stylesheet">
      
      
      
      <!--CSS -->
      <script src="/js/jquery.js"></script>
      <script src="/js/bootstrap.js"></script>
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="/js/ie10-viewport-bug-workaround.js"></script>


      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

     
        <script src="/Virtuoso/Virtuoso/js/EventManager.js"></script>
    </head>
    <body>
      <?php echo $this->element('navigation/top_nav_full_width'); ?>
      <div class="container-fluid">

        <div class="row" >
          <?php
          

            ///column layout is handled by 'user_area/user_area_template'
            echo $content_for_layout; 
          
          
          ?>
        </div>
        <div class = "clear clearfix bm_5_em" >&nbsp;</div>
      </div>
      <div style = "height:0px;">&nbsp;</div>

      
      <script src="/PagekwikTools/js/ajax_slider.js"></script>
      <script src="/Quizmodules/js/pnotify.custom.min.js"></script>
      <script src="/Quizmodules/js/quiz_notification.js"></script>
      <script src="/js/siteJsWrapper.js"></script>
            <?php
            /// debug($additional_scripts);
                echo $this->Js->writeBuffer();
                echo $this->Session->flash('flash', array(
                    'element' => 'flash',
                    'params' => array('plugin' => 'PagekwikTools')
                ));
                // this flash should probably be included in the sites, rather than an a plugin.
                // also the site flash currently resides in the quiz plugin. that needs to be relocated to somewhere universal.
            ?>
        <!-- Event Modal -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-body">
                            <p>Loading...</p>
                        </div>/
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- Event modal -->
      <?php foreach ($additional_scripts as $key => $value): ?>
          <script type="text/javascript" src='<?php echo $value ?>.js'></script>
      <?php endforeach; ?>
      <?php echo $this->fetch('bottom_script') ?>
      <script type="text/javascript">
          $( document ).ready(function() {  onPageLoad(); });
      </script>    
    </body>
  </html>
