<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo __($title_for_layout); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/layout_aids.css" />

	<link rel="stylesheet" type="text/css" href="/css/style.css" />
	<style type="text/css" media="screen">

	.clear{
		float:none;
		clear:both;
		height:1px;
	}
	.file_manager a{
		color:#666666 !important;
	}
	.file_manager a:hover{
		text-decoration: none !important;
	}
		.admin_file_listing_toolbar{
			padding-top:10px;
			background-color:#333333;
			box-shadow:0 5px 8px #AAAAAA;
		}
		.uploader_background_graphic{
			background-image:url("/Userasset/img/download_symbol.png");
			width: 150px;
			height: 150px;
			margin: auto;
			background-repeat: no-repeat;
		}
		.upload_toolbar_link{
			margin-right:10px;
		}
		.admin_file_listing_view_button{
			text-align: center;
		}		
		.admin_file_listing_toolbar, .admin_file_listing_toolbar {
			color:#ffffff !important;
		}		

		.file_path {
			float:left;
			margin-left:1em;
		}
		.actions_spacer{
			margin-left:1em;
			margin-right:1em;
		}
		.admin_file_listing_toolbar h4, .admin_file_listing_toolbar h5, .admin_file_listing_toolbar h6 {
			display:inline;
			float: left;
			color: #ffffff;
			margin-top: 0px;
			margin-left: 1em;
		}
		.admin_file_listing_toolbar h5, .admin_file_listing_toolbar h6{
			color:#b92204;
		}
		.qq-upload-failed-text {
		     display: none; 
		}


	</style>
	<script type="text/javascript" src = '/js/jquery.js'></script>
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="/ico/apple-touch-icon-57-precomposed.png">
	<link rel="shortcut icon" href="/ico/favicon.png">
  </head>

  <body>
    <div id="wrap" class="file_manager">
      <div class="">
			<?php
			echo($this->element('fb_top_nav')); 
			echo '';
			?>		
			<div class = "clearfix visible-desktop" style="margin-top:2em;">&nbsp;</div>
			<div class = "" id="admin_files_scroll_pane">
					<?php echo $content_for_layout; ?> 			 						
			</div>
      </div>
      <div id="push"></div>
    </div>

	<div id="footer" class ="navbar navbar-inverse navbar-fixed-bottom">
	      <div class="container">
			&nbsp;
	      </div>
	</div>
	<script type="text/javascript" src = '/js/jquery.js'></script>
	<script src="/PagekwikTools/js/ajax_slider.js"></script>    
	<script type="text/javascript">
	        var fb_ajax_helper = ajax_slider();
	        fb_ajax_helper.init("#admin_files_scroll_pane",'.fb_ajax_link');            
	        //fb_ajax_helper.applyAjaxListenerToLinks('.fb_ajax_link');            
	</script>
  </body>
</html>
