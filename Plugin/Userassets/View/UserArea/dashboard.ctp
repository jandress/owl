<?php 

    // layout is user_area_layout.ctp 
    //columns are handled by 'user_area/user_area_template'
?>

<?php 
    $l = array(
        // 'user_area/user_dashboard_user_icon',
        // // 'user_area/user_dashboard_nav_main'
    );
    $r = array(
        // 'user_area/file_monitor'
        // ,'user_area/notification_rows'  
        'user_area/file_monitor'      
    );
    $m = array(
     'user_area/community/dashboard_community_feed',
     'user_area/notification_rows'
   );
    echo $this->element('user_area/user_area_template' ,array(
        'left_sidebar_elements' => $l,
        'right_sidebar_elements' => $r,
        'main_content_elements' => $m
    )); 
?>



