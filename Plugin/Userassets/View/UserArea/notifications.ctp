<?php // layout is user_area_layout.ctp ?>
<div class="container-fluid tm_2_em ">
        <div id="user_left_sidebar" class=" col-md-2">
            <?php echo $this->element(// 'user_area/user_dashboard_user_icon'); ?>
            <?php echo $this->element(// 'user_area/user_dashboard_nav_main'); ?>
        </div>    

        <div  id="user_main_content"  class="col-md-8">
            <h3>content here!</h3>
            <div class="clear clearfix tm_1_em">&nbsp;</div>
            <div class="tm_1_em">
                <?php echo $this->element('user_area/notification_rows'); ?>
            </div>
        </div>

        <div  id="user_right_sidebar"  class="col-md-2">
            <?php echo $this->element('user_area/file_monitor'); ?>
        </div>
</div>