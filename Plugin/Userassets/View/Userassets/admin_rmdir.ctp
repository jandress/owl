<div class = "admin_file_listing_toolbar" style = 'z-index:10'>
	<div class="container">		
		<a href = '/assets/' class = 'btn pull-right'>Back</a>
	</div>
</div>

<div class="well">
	<div class="white_well">
		
		<div class="alert alert-error">
			<h2>The directory <em>"<?php echo $file_dir_root.''.$target_directory?>"</em> is not empty!</h2>		
			<h4>The directory's contents will also be deleted:</h4>
		</div>		
		
		<table id="projects" class="table table-striped table-bordered table-hover"  >
			<thead>
				<tr class = 'gb_28'>
					<th class = "span1" >&nbsp;</th>
					<th class = "span8">Name</th>
					<th class="actions span3" style = 'text-align:center'>&nbsp;</th>
				</tr>
			</thead>			
			<tbody>
			<?php
			if(!empty($dir_contents)):
			$i=0;
			foreach ($dir_contents as $key => $value): ?>
				<tr id="<?php echo $key; ?>" <?php echo(($i++ % 2 == 0) ? ' class="altrow"' : '')?>>
					<td style = "text-align:center"><?php echo $i; ?></td>
					<?php 		
						$type =$value['type'];	
						$icon = $type;
						switch($type){
							case '> > dir':
								$icon = 'icon-folder-close';
							break;
							case 'm4v':
								$icon = 'icon-film';
							break;
							case 'png':
							case 'jpg':
							case 'jpeg':
								$icon = 'icon-picture';
							break;
						}				
				?>
					<td><span
						 class = "file_listing_index">
					<?php
						echo('</span> '.'&nbsp;<span class = "'.$icon.'" ></span>&nbsp; <span class = "file_listing_label">');
						if($type == "dir"){
							echo('<a>'.$value['item'].'</a>');
						} else {
							echo('<a target = "_blank">'.$value['item'].'</a>');							
						}
					echo('</span>');
					?>
					</td>
					<td class="actions" style = 'text-align:center'></td>
				</tr>				
			<?php 
			endforeach;
			else: 
			?>	
				<tr>
					<td colspan=4>No files were found.</td>
				</tr>           
				<?php endif ?>
			</tbody>
		</table>
		
		
		<div class="btn-group pull-right">
				<a href = '' class = 'btn btn-success'>Cancel</a>
				<?php echo $this->Form->postLink(__('Delete All'), null, array('class' => 'btn btn-danger'), __('Are you sure you want to Delete %s?','"'.$target_directory.'"'));?>
		</div>
		<div class = "clearfix">&nbsp;</div>
		
	</div>
</div>