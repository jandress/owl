<div id = "#modal-content">
    <div class ="pl_1_em pr_1_em">
        <div class = "well tm_1_em">
          <textarea style = "width:100%" name="box-content" id="box-content" rows="5" cols="70">
              <?php echo $d ?>
          </textarea>
          <div class = "tm_1_em pl_2_em pr_2_em">
              <a data-clipboard-target="box-content" id="copy-button" style = "width:100%" class = "btn btn-success">Copy to Clipboard</a>

          </div>
        </div>
  </div>
  <script type="text/javascript">
        // main.js
      var client = new ZeroClipboard( document.getElementById("copy-button"), {
        moviePath: "/PagekwikTools/js/zeroclipboard/ZeroClipboard.swf"
      } );
      client.on( "load", function(client) {
        // alert( "movie is loaded" );
        client.on( "complete", function(client, args) {
          // `this` is the element that was clicked
          this.style.display = "none";
        } );
      } );
  </script>
</div>