
class AudioVisualizer  extends EventsManager{
    constructor(guid, audioContext) {
      super();      
      var scope = this
      this.guid = guid;
      this.borderColor = "#000000";
      this.waveformColor = "#e4a138";
      this.progressBarColor = "#333333";
      this.audioContext = audioContext;
      this.waveFormBoxCanvas = document.getElementById("waveFormBox_"+guid);
      this.waveFormBackgroundCanvas = document.getElementById("waveFormBackground_"+guid);
      this.waveFormBoxContext = this.waveFormBoxCanvas.getContext("2d");
      this.waveFormBoxContext.lineWidth = .5;
      



      // this.boxWidth = $("#waveform_container_1").width()-20//1170;
      this.boxHeight = 140;
      // this.waveFormBackgroundCanvas.style.width =  this.waveFormBoxCanvas.style.width = this.getBoxWidth();
      // this.waveFormBackgroundCanvas.style.height = this.waveFormBoxCanvas.style.height = this.boxHeight;
      // $("#waveFormBox_"+guid).width =this.getBoxWidth()
      // $("#waveFormBox_"+guid).height = this.boxHeight;
      this.waveFormBackgroundContext = this.waveFormBackgroundCanvas.getContext("2d");
      $("#waveform_container_"+guid).click(function(e){
        var parentOffset = $(this).parent().offset(); 
        var relX = (e.pageX - parentOffset.left) / scope.getBoxWidth()// $("#waveform_container_"+guid).width();
        console.log(relX)
        scope.onClickedTo(relX);
      })
      //---------------
      var duration = 0;
      this.waveFormBoxContext.fillStyle = this.waveformColor;
      // $("#waveFormBackground_"+guid).width(this.boxWidth)
    }

    onClickedTo(percent){
      this.announceEvent("waveform_event", percent);
    }


    visualizeAudio(url) {
        var audioContext = this.audioContext;
        fetch(url)
        .then(response => response.arrayBuffer())
        .then(arrayBuffer => audioContext.decodeAudioData(arrayBuffer))
        .then(audioBuffer => this.drawWaveForm(audioBuffer));
    };

    drawWaveForm(audioBuffer){
      var scope = this;
      this.audioBuffer = audioBuffer;
      this.draw()
    }

    draw(){

      this.waveFormBoxCanvas.width = this.getBoxWidth();
      this.waveFormBackgroundCanvas.width = this.getBoxWidth();


        var audioBuffer = this.audioBuffer
        var scope = this;
          function filterData(audioBuffer, scope){
            const rawData = audioBuffer.getChannelData(0); // We only need to work with one channel of data
            const samples = scope.getBoxWidth(); // Number of samples we want to have in our final data set
            const blockSize = Math.floor(rawData.length / samples); // the number of samples in each subdivision
            const filteredData = [];
            for (let i = 0; i < samples; i++) {
              let blockStart = blockSize * i; // the location of the first sample in the block
              let sum = 0;
              for (let j = 0; j < blockSize; j++) {
                sum = sum + Math.abs(rawData[blockStart + j]) // find the sum of all the samples in the block
              }
              filteredData.push(sum / blockSize); // divide the sum by the block size to get the average
            }
            return filteredData;
          }
        var buffer = filterData(audioBuffer,scope)
        this.waveFormBoxContext.clearRect(0, 0, this.getBoxWidth(), this.boxHeight);
        this.waveFormBoxContext.beginPath();
        this.waveFormBoxContext.strokeStyle = 'red';    
        var visualizerLineHeight =  this.boxHeight 
        var maxLineHeight = this.boxHeight 

        var c = 0;
        for (var i = 0; i < buffer.length; i++) {
          c = c < buffer[i] ? buffer[i] : c;
        }  
        var mxHeightPercentOfBoxHeight = 1
        var adjustment = mxHeightPercentOfBoxHeight - c;
        var midpoint = this.boxHeight/2
        var thisLineHeight;
        var halfLineHeight;
        var x = 0			
        var y = 0;

        for (var i = 0; i < buffer.length; i++) {
          thisLineHeight = visualizerLineHeight * (buffer[i]+(adjustment*buffer[i]))
          halfLineHeight = thisLineHeight/2
          y = midpoint - thisLineHeight/2
          this.waveFormBoxContext.moveTo(x, midpoint-halfLineHeight);
          this.waveFormBoxContext.lineTo(x, midpoint+halfLineHeight);
          x += 3
        }    
        this.waveFormBoxContext.stroke();		
        //draw a black outline around it. 	
        this.waveFormBoxContext.beginPath()
        this.waveFormBoxContext.strokeStyle = this.borderColor;    
        this.waveFormBoxContext.rect(0, 0,this.getBoxWidth(), this.boxHeight)
        this.waveFormBoxContext.stroke();		
        // this.drawProgress(percent)
    }
    getBoxWidth(){
      return $("#waveform_container_"+this.guid).width()-0;
    }
    drawProgress(percent){
        this.percent = percent;
        //draw a black outline around it. 	
//        var relX = (e.pageX - parentOffset.left) / ($("#waveform_container_"+guid).width()-20);// $("#waveform_container_"+guid).width();
        this.waveFormBackgroundContext.clearRect(0, 0, this.getBoxWidth() , this.boxHeight)
        this.waveFormBackgroundContext.beginPath()
        this.waveFormBackgroundContext.lineWidth = .5;
        this.waveFormBackgroundContext.strokeStyle = this.borderColor;   
        this.waveFormBackgroundContext.fillStyle = this.progressBarColor;
        this.waveFormBackgroundContext.rect(0, 0,this.getBoxWidth()*percent, this.boxHeight)
        this.waveFormBackgroundContext.fill();
        this.waveFormBackgroundContext.stroke();		
    }
}

class PlayerUi extends EventsManager{

    constructor(guid) {
      super();
      this.guid = guid;
      this.defineInteractivity(guid,this);
    }    

    defineInteractivity(guid,scope){
      $("#play_btn_"+this.guid).click(function(){
        scope.announceEvent("ui_event", "play_pause");
      })
      $("#stop_"+this.guid).click(function(){
        scope.announceEvent("ui_event", "stop");
      })
    }
    displayPaused(){
      $("#playpause_"+this.guid).removeClass("glyphicon-pause").addClass("glyphicon-play");	
    }
    displayPlaying(){
      $("#playpause_"+this.guid).removeClass("glyphicon-play").addClass("glyphicon-pause");	        
    }
    setTimeLabel(str){
      $("#counter_"+this.guid).html(str)
    }
  }
  

class PlayerModel extends EventsManager {
    constructor(guid) {
      super();
      this.guid = guid;
      this.player = new Audio();
      this.playing = false;
    }
    loadFile(completeFilePath){
      this.player.src = completeFilePath;
    }
    stop(){
      this.playing = false;
      this.player.pause();
      this.player.currentTime = 0;
      this.announceEvent("model_event", "stop");
    }
    play(){
      this.player.play().then(() => {
          // Play started successfully
          this.playing = true;
          this.announceEvent("model_event", "playing");
        }).catch((error) => {
          // Handle error, for example, due to autoplay policy
          // console.error("Playback failed:", error);
        });
      this.playing = true;
      this.announceEvent("model_event", "playing");
    }
    pause(){
      this.playing = false;      
      this.player.pause() 
      this.announceEvent("model_event","paused");
    }
    currentTimeByPercent(val){
      this.player.currentTime = val*this.player.duration
    }
    currentTime(val){
      this.player.currentTime = val
    }
    
    getDuration(){
      return this.player.duration; 
    }
    getCurrentTime(){
      return this.player.currentTime; 
    }
}

class AudioPlayer  extends EventsManager {

    static instances;
    constructor(initialFile, guid, autoPlay = false) {
          super();
          console.log("AudioPlayer: ",initialFile, guid)
          // console.log(initialFile, guid)
          window.AudioContext = window.AudioContext || window.webkitAudioContext;
          const audioContext = new AudioContext();

          this.guid = guid;
          this.playerModel = new PlayerModel(guid); 
          this.playerUi = new PlayerUi(guid); 
          this.waveform = new AudioVisualizer(guid,audioContext); 
          ///---------------
          this.playerUi.listenForEvent("ui_event", function(e){
            this.parseUiEvent(e[0])
          }, this);
          this.playerModel.listenForEvent("model_event", function(e){
            console.log("this.playerModel.listenForEvent", "model_event", e[0])
            this.parseModelEvent(e[0])
          }, this);
          this.waveform.listenForEvent("waveform_event", function(e){
            // console.log("model_event")
            this.setProgressPercent(e[0])
          }, this);
          ///---------------
          if(!AudioPlayer.instances){
            AudioPlayer.instances = [];
          }
          AudioPlayer.instances.push(this);
        //   console.log(AudioPlayer.instances);
          this.loadFile(initialFile);
        //   this.stopAll()
          if(autoPlay ){
            this.playerModel.play()
          }
    }


    parseUiEvent(key){
      console.log("parseUiEvent ",key, this.guid)
      switch (key) {
        case "play_pause":
            if(  this.playerModel.playing) {
              this.pause();
            } else {
              this.play();
            }
          break;
          case "stop" :
             this.stop();
            // this.playerModel.stop() 
            // this.stopAll()
          break;          
          case "next" :
            scope.announceEvent("player_event", "next");
          break;          
          case "previous" :
            scope.announceEvent("player_event", "previous");
          break;                  
        default:
          break;
      }
    }

    parseModelEvent(key){
        console.log("parseModelEvent ",key, this.guid)
      switch (key) {
        case "playing" :
              this.playerUi.displayPlaying()
          break;
          case "paused" :
              this.playerUi.displayPaused();
          break;          
          case "stop" :
              this.playerUi.displayPaused();
          break;              
        default:
          break;
      }
    }


    loadFile(filePath,  autoplay = false){
      // console.log(filePath)
      this.playerModel.stop();
      this.playerModel.loadFile(filePath); 
      this.waveform.visualizeAudio(filePath); 
      this.waveform.drawProgress(0)
      if(autoplay){
        this.playerModel.play();
      }
    }



    createTimer(){
      if(  !this.playerModel.playing ){
        clearInterval(this.timer)
      }
      var string;
      var scope = this;
      var duration = scope.playerModel.getDuration();
      this.timer = setInterval(function(){
        var currentTime = scope.playerModel.getCurrentTime()
        var per = currentTime / duration
        scope.waveform.drawProgress(per)
        function millisToMinutesAndSeconds(millis) {
          var minutes = Math.floor(millis / 60000);
          var seconds = ((millis % 60000) / 1000).toFixed(0);
          return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
        }
        if(duration==currentTime){
           scope.playerModel.stop();
        }
        string = "0"+millisToMinutesAndSeconds(currentTime * 1000);
        scope.playerUi.setTimeLabel(string)
      }, 50)
    }


    stop(){
      this.playerModel.stop() 
      // this.clearInterval(this.timer)
    }

    play(){
    //   this.stopAll();
      this.playerModel.play() 
      this.createTimer()
    }

    pause(){
      this.playerModel.pause() 
    }
    setProgressPercent(percent){
      this.playerModel.currentTimeByPercent(percent)
    }

    redraw(){
      this.waveform.draw()
    }
    stopAll(){
        for (let i = 0; i < AudioPlayer.instances.length; i++) {
            const element = AudioPlayer.instances[i];
             element.stop();
        }
    }


}


// //initialize this player and make any other players on the page stop.
// if (typeof sAudioPlayerAr === 'undefined') {
//   sAudioPlayerAr = []
// } else {
//   console.log('sAudioPlayerAr is defined.');
// }
// for (let i = 0; i < sAudioPlayerAr.length; i++) {
//   const element = sAudioPlayerAr[i];
//   element.stop();
// }
