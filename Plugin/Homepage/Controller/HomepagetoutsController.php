<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Homepagetouts Controller
 *
 * @property Homepagetout $Homepagetout
 * @property SessionComponent $Session
 */
class HomepagetoutsController extends AppController {

/**
 *  Layout
 *
 * @var string
 */
	//public $layout = 'frontend';

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session','Auth','Session','Security','Userassets.Fileutil');
	public $uses = array('Homepage.Homepagetout');
/**
 * my_index method
 *
 * @return void
 */
	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('view');
		$this->set('cta_target_options',array('options'=>array('_self'=>'_self','_blank'=>'_blank')) );
	}
	
	
	function getOwlImageOptions(){
		$owlDirContents = $this->Fileutil->getFilesAsKeysNoDirectories('./owls');
		$ar= array();
		foreach ($owlDirContents as $key => $value) {
		$n = explode('badge_med_',$value);
		if(count($n)>1){
			$g = explode('.png',$n[1]);
			if(count($g)>1){
				$ar[$g[0]]=$g[0];
			}

		}
		}
		return $ar;
//		debug($ar); exit;
	}

	public function view() {
		$b=$this->Homepagetout->find('all',array('order'=>'order'));
		$this->set('homepagetouts', $b);
		$this->layout ='owl_homepage';
	}

	public function admin_index() {
		$this->Homepagetout->recursive = 0;
		$this->paginate=array(
			'order'=>'order'
		);
		$this->set('homepagetouts', $this->paginate());
	}

	public function admin_add() {
		$this->set('owl_image_options',$this->getOwlImageOptions());		
		$this->title('New Homepagetout');
		if ($this->request->is('post')) {
			$this->Homepagetout->create();
			if ($this->Homepagetout->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('page')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('/my/homepage/');
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('page')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}else{
			$this->data = $this->Homepagetout->create();
		}
		
	//	$this->layout = 'admin';
		//$posts = $this->Homepagetout->Post->find('list');
		// $contentmodules = $this->Homepagetout->Contentmodule->find('list');
		// $this->set(compact('posts', 'contentmodules'));
	}

	public function admin_edit($id = null) {
		$this->Homepagetout->id = $id;
		if (!$this->Homepagetout->exists()) {
			throw new NotFoundException(__('Invalid %s', __('page')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Homepagetout->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('page')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('/my/homepage/');
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('page')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Homepagetout->read(null, $id);
		}
		$this->title($this->request->data['Homepagetout']['head']);
		// $posts = $this->Homepagetout->Post->find('list');
		// $contentmodules = $this->Homepagetout->Contentmodule->find('list');
		$this->set('layouts_array',$this->layouts_array);
		$this->set('owl_image_options',$this->getOwlImageOptions());
		
		
		//$this->set(compact('posts', 'contentmodules','layouts_array'));
	}

	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Homepagetout->id = $id;
		if (!$this->Homepagetout->exists()) {
			throw new NotFoundException(__('Invalid %s', __('page')));
		}
		if ($this->Homepagetout->delete()) {
			$this->Session->setFlash(
				__('The %s was deleted', __('tout')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect('/my/homepage/');
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('tout')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect('/my/homepage/');
	}
	
	
	
	
	
	public function moveup($id){
		$g = $this->Homepagetout->find('first',array('conditions'=>array('Homepagetout.id'=>$id)));
		$b = $this->Homepagetout->moveup($id);
		$this->admin_index();		
	}
	public function movedown($id = null){
		$g = $this->Homepagetout->find('first',array('conditions'=>array('Homepagetout.id'=>$id)));
		$b = $this->Homepagetout->movedown($id);
		$this->admin_index();
	}


	
}
