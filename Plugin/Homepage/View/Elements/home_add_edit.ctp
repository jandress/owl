<?php echo $this->BootstrapForm->create('Landingpage', array('class' => '_form-horizontal'));?>
<div class="tm_1_em">
	<fieldset>
		<?php	
			echo $this->BootstrapForm->hidden('id',array('class'=>'col-md-12'));
			echo $this->BootstrapForm->input('title',array('class'=>'col-md-12','label'=>'Title'));
			echo $this->BootstrapForm->input('cmsname',array('class'=>'col-md-6','label'=>'Cms Label'));
			echo $this->BootstrapForm->hidden('home');
			?>&nbsp;
			<button type="button" class="btn btn-sm btn-default btn-info" data-toggle="popover" title="Cms-only title" data-content="This optional field is to differentiate different pages with the same name in the cms. If you leave it blank, the title will be used on the index page and others.">?</button>
			<?php
			echo $this->BootstrapForm->input('copy',array(''));	
			echo $this->BootstrapForm->input('layout', $layout_options);			
		?>
		<div class = "clearfix bm_1_em">&nbsp;</div>					
	</fieldset>
</div>
<?php echo $this->BootstrapForm->submit('Next');?>
<?php echo $this->BootstrapForm->end();?>


<a class = 'btn btn-default pull-right tm_1_em ajax_submit' href = "/my/landingpages/tiers/*">Next</a>

<div class = "clearfix">&nbsp;</div>




<script type="text/javascript">
$(function () {
  $('[data-toggle="popover"]').popover()

})
</script>

