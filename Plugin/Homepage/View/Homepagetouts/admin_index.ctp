<div class="navbar-inverse">
	<h2><?php echo 'Content Touts for Home page'; ?></h2>
	<div class = "clearfix">&nbsp;</div>			
</div>

<div class="admin_subsub "> 
	<div class="admin_padding tm_10_px bm_12_px">
		<a href = '/' target = '_blank' class = 'btn btn-default btn-sm pull-right'>View Homepage</a>
	<div class = "clearfix">&nbsp;</div>					
	</div>
</div>


<div class="admin_padding">		
	<div class = "clearfix">&nbsp;</div>						
	<a class = "btn btn-sm tm_1_em btn-default pull-right" href="/my/homepages/touts/add/"> New Tout</a>
	<div class = "clearfix">&nbsp;</div>						
	<div class = "tm_1_em" id = "landing_tout_table">
		<?php echo $this->element('home_tout_table') ?>	
	</div>		
</div>

<div class = "clearfix">&nbsp;</div>

<script type="text/javascript">
	function bindRepos(){
		$(".repos_btns a").unbind('click');
		$(".repos_btns a").click(function(e){
			e.preventDefault();
			var _url = $(e.currentTarget).attr("href");
			$("#landing_tout_table").empty();
			$.ajax({
				url: _url,
				context: $("#landing_tout_table "),
				success: function(data){
					//console.log('success: '+data);
					//return;
					$(this).empty();
					$(this).html(data);  
					bindRepos();
				}
			});	
			return false;
		});
	}
	bindRepos();
</script>