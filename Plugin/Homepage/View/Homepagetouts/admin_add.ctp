<div class="navbar-inverse">
	<h1><?php echo __('Adding new %s', __('Tout')); ?></h1>
	<p>Adding a new tout to the Home Page</p>	
	<div class = "clearfix">&nbsp;</div>			
</div>

<div class="admin_subsub "> 
	<div class="admin_padding tm_10_px bm_12_px">
		<div class="btn-group breadcrumbs_nav"><a href="/my/homepage/" class="">Homepage Tout Index</a>&nbsp;</div>
	</div>
</div>


<div class="admin_padding">
	<?php echo $this->element('home_tout_edit'); ?>
</div>

<div class = "clearfix">&nbsp;</div>
