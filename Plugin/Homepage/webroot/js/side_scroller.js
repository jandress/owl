sideScroller = function(target,id) {       
	var _hzWidth =  50;                                               
	var _onOpacity = .5;
	var _offOpacity =  .1;
	var _scroller_width = $(target).parent().width();		

	

	var right = "RIGHT";
	var left = "LEFT";	      
	
	var function_object = {	

		positionIndex:0,   
		_target:target,  			
		_leftHz:null,
		_rightHz:null,

		init:  function() {			                                   
			$(target).wrapInner('<div style="" id="' +id+ '" />');                                                       
				// Left hitzone.....
			_leftHz = id+"_left_hz";                                   
			$(target).append('<div class = "browse_scroller_hitzone left_hitzone" id = "'+_leftHz+'" style ="margin-left:-1.5em; width:'+_hzWidth+'px;" />');                                                                                        			
	    	
	    	function_object.applyInteractivity("#"+_leftHz,left,id);           
				// Right hitzone...
			_rightHz = id+"_right_hz";
			

			$(target).append('<div class = "scroller_hitzone_right browse_scroller_hitzone right_hitzone" id = "'+_rightHz+'" style="width:'+_hzWidth+'px; margin-left:'+((_scroller_width - _hzWidth)+25)+'px;" />');    			                                                                                                                                  			
	    	
	    	function_object.applyInteractivity("#"+_rightHz,right,id);       


			$("#"+_leftHz).wrapInner('<div class="hit_zone_graphic" />');                                                       
			$("#"+_rightHz).wrapInner('<div class="hit_zone_graphic" />');                                                       			
			if($("#"+id).width() < _scroller_width){
				$("#"+_leftHz).css("display","none");
				$("#"+_rightHz).css("display","none");					
			}			
		},


		applyInteractivity: function(hz, direction, target){
			var _params =  
				{
					hz:hz, 
					direction:direction, 
					target:"#"+target,
					num_children:$("#"+target).children('.browse_tout').length
				};
				
				var browse_tout_width = 193;									
				$(_params.target).css("width", (_params.num_children+1)*browse_tout_width );				
				$(hz).css('cursor', 'pointer');   

				$(hz).mouseenter(_params,function(e) {
					//	$(hz).stop().animate({opacity:_onOpacity});                  					
				});
				$(hz).mouseleave( _params, function(e) {
					//	$(hz).stop().animate({opacity:_offOpacity});
				});                     

				$(hz).click( _params, function(e) {  
 					var targetWidth = $(_params.target).width();
					var current_position = parseFloat($(_params.target).css('margin-left'));
					var current_index = current_position / _scroller_width;
					var tx=0;
					_scroller_width =$(function_object._target).parent().width();									

					if( _params.direction == left){
						current_index ++;
						if( current_index >=0 ){
							current_index = 0;
							//console.log("hide left");
						}
						tx = (_scroller_width*current_index)+_hzWidth;						
					}
					if( _params.direction == right){
						current_index --;
						tx = _scroller_width*(current_index);
						if((tx+targetWidth) <= (_scroller_width *1.5)){
							tx = (_scroller_width)-(targetWidth+_hzWidth);
						}
					}					
					$(_params.target).animate({"margin-left":(tx)});         
			});		
			
			
			
			
			
				
		}
   	}
   	function_object.init();
 	return function_object;
}; 




aalert("YES!!!");


var browsePage = function() {       
	var tiers = $(".scroll_row");
	var scrollers = [];
	var function_object = {
		init: function (div_name,ar){
			for (var i=0; i < tiers.length; i++) {
        		scrollers.push(sideScroller(tiers[i],'scroller_'+i));
			}
		}
	}
   	function_object.init();
	return function_object;
}; 

( function($) {                       
	$(document).ready(function(){  
	   browsePage();
	});
} ) ( jQuery );                   