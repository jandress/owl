
		var rotationPosition = 0;
		var totalNumberOfPanels = 3;
		var contentIndex = 0;
		var carousel;
		var radius = 200;
		var currentId = 0;


		$("#left_arrow").click(function(e) {
			console.log("contentIndex: "+contentIndex);
			contentIndex = contentIndex > 0 ? contentIndex - 1 : 9;
			currentId = ids[contentIndex];


			//console.log("contentIndex: "+contentIndex+", currentId: "+currentId);

			advanceCarousel(-1);
		});
		$("#right_arrow").click(function(e) {
			console.log("contentIndex: "+contentIndex+", currentId: "+currentId);
			contentIndex = contentIndex <   9 ? contentIndex + 1 : 0;
			currentId = ids[contentIndex];

			advanceCarousel(1);
		});

		$(".condensed_landing_tout_link").mouseover(function(e) {
			var id = $(e.currentTarget).attr('id');
			currentId = id;
		//	console.log(currentId);
			if(contentIndex == ids.indexOf(id)){
				return;
			}
			var direction = contentIndex > ids.indexOf(id) ? "left" : "right";
			contentIndex = ids.indexOf(id);
			if (direction=="left"){
				advanceCarousel(-1)
			}else if (direction=="right"){
				advanceCarousel(1)
			}
			return false;
		});


	function advanceCarousel(increment){
			///console.log("carousel.theta: "+carousel.theta+"  increment: "+increment);
			rotationPosition+=increment;
			var targetFigure = rotationPosition%3;
			// ~ this is probably simple subtraction...come back and refactor this.
			targetFigure = targetFigure == -1 ? 2: targetFigure;
			targetFigure = targetFigure == -2 ? 1: targetFigure;
			var nid = ids.indexOf(currentId);
			$("#figure_"+targetFigure+" img").attr('src',currentId+".png");
			$("#figure_"+targetFigure+" a").attr('href',ctas[nid])
//			$("#figure_"+targetFigure+" small").html(copy[nid])
//			alert($("#copy_"+nid).html());
           carousel.rotation = (carousel.theta * -1)*rotationPosition;
           carousel.transform();
	}
	   var init = function() {
	     carousel = new Carousel3D( document.getElementById('carousel') );
         onNavButtonClick = function( event ){
           var increment = parseInt( event.target.getAttribute('data-increment') );
			advanceCarousel(increment);
         };
	     carousel.panelCount = parseInt( totalNumberOfPanels, 10);
	     carousel.modify();
	     setTimeout( function(){
			document.body.addClassName('ready');
			$("#carousel figure").css('display',"inline");
		}, 0);
	   }; // end init();
	   window.addEventListener( 'DOMContentLoaded', init, false);
