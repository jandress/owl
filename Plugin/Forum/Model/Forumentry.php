<?php
App::uses('AppModel', 'Model');
/**
 * Navlink Model
 *
 * @property Navlink $ParentNavlink
 * @property Navlink $ChildNavlink
 */

class Forumentry extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	//public $displayField = 'name';
	//	var $actsAs=array('Polymorphic','Containable','Navigations.GroupTree');


	// public $recursive = 4;
	var $actsAs=array('Containable','Navigations.GroupTree');

	public $belongsTo = array(
		'ParentForumentry' => array(
			'className' => 'Forumentry',
			'foreignKey' => 'parent_id',
		),
		'User'=>array(
			'className' => 'User',
			'foreignKey' => 'user_id',
		),
		'Collaboration'=>array(
			'className' => 'Collaboration',
			'foreignKey' => 'collaboration_id',
		)

	);


	public $hasMany = array(
		'ChildForumentry' => array(
			'className' => 'Forumentry',
			'foreignKey' => 'parent_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Vote' => array(
			'className' => 'Vote',
			'foreignKey' => 'forumentry_id',
			'conditions' => array()
		),
		
	);

	/*
		Basically, votes should result in each item being reordered by rank = (upvotes - downvotes) on it's tier.
		They should never be moved to a different parent. unless ad admin is doing it for maintenance
		Forumentry
		// debug($this->childCount($parent_id , true	)); exit;
		// debug( $this->recover("parent"),1 ); exit;
	*/

	public function reparent($conditions){
			$this->contain();
			$conditions = array(
				'Forumentry.id != 1',
				'Forumentry.parent_id = 0'
			);
			$t = $this->find("all",array('conditions'=>$conditions));
			$b = $t;
			foreach ($b as $key => $value) {
				 $b[$key]['Forumentry']['parent_id'] = 1;
			}
			debug($this->saveAll($b)); exit;
	}

	public function getForumentry($id = 0){
		if($id==1){
			throw new Exception('Invalid Forum Entry');
		}
		$this->recursive = 4;
		$t = $this->find("first",
			array(
				"conditions" => array(
						'Forumentry.id'=>$id
				)
			)
		);
		// debug($t);
		if(empty($t)){
			return false;
		}
		$cc = $this->childCount($t['Forumentry']['id']);
		// debug($cc);
		$t['Forumentry']['child_count'] = $cc;
		return $t;
	}

	public function getTopLevelListing(  $outside_resource = 0){
		$this->contain("User","Vote");
		$t = $this->find("all",
			array(
				"conditions" => array(
						'Forumentry.parent_id'=>1
						,'Forumentry.outside_resource'=>$outside_resource
				)
			)
		);
		foreach ($t as $key => $value) {
			$b = $value;
			$cc = $this->childCount( $b['Forumentry']['id']	);
			$t[$key]['Forumentry']['child_count'] = $cc;
		}
		return $t;
	}




		public function getFullTopLevelListing(){
			$this->contain("User");
			$t = $this->find("all",
				array(
					"conditions" => array(
							'Forumentry.parent_id'=>1
					)
				)
			);
			foreach ($t as $key => $value) {
				$b = $value;
				$cc = $this->childCount( $b['Forumentry']['id']	);
				$t[$key]['Forumentry']['child_count'] = $cc;
			}
			return $t;
		}



	// ugly hack to get around the difficulty in using variables in Cakephp association conditions.
	//Configure::write("current_user"); is set in beforeFilter in AppController.
	public function find(  $type = 'first' ,  $query = array() ){
		$cu = Configure::read("current_user");
		$this->hasMany['Vote']['conditions']['user_id'] = $cu['User']['id'];
		return parent::find($type,$query );
	}

	private function createGUID() {
		return String::uuid();

		// $data = openssl_random_pseudo_bytes(16);
	
		// // Set version to 0100 (Version 4)
		// $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
		// // Set bits 6-7 to 10
		// $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
	
		// // Output the GUID in hexadecimal format with hyphens
		// return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}
		
	
	private function deleteDirectory($dir) {
		if (!file_exists($dir)) {
			return true;
		}
		if (!is_dir($dir)) {
			return unlink($dir);
		}
		foreach (scandir($dir) as $item) {
			if ($item == '.' || $item == '..') {
				continue;
			}
	
			if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
				return false;
			}
		}
		return rmdir($dir);
	}

	private function renameFile(){

	}

	private function moveFile(){
		
	}
	private function getFileAndDeleteOthers($dir){
		///loop through the the directory, get the first file in the directory and then delete everything else in it.
		$dirFiles = array();
		if ($handle = opendir($dir)) {
			while (false !== ($file = readdir($handle))) {				
				if(
					$file =='.' ||
					$file =='..'|| 
					$file =='.DS_Store'						
				){
					continue;
				}
				$ext = pathinfo($file, PATHINFO_EXTENSION);
				if(
					($ext == "png" ||
					$ext == "gif" ||
					$ext == "webp" ||
					$ext == "jpeg" ||
					$ext == "jpg" ||
					$ext == "wav" ||
					$ext == "mp3" ||
					$ext == "mv4"  )
					&& count($dirFiles) == 0
					){
						array_push( $dirFiles, $file);
				}else{
					// delete any other files in the directory. 
					// array_push( $dirFiles2,$file);
					if($file){
						unlink($dir.$file); // Delete subsequent image files
					}
				}
			}
			closedir($handle);
		}
		return  isset($dirFiles[0]) ? $dirFiles[0] : 0;
	}

	private function getNewName($file = null){
		if($file ){
			// rename file
			$ext = pathinfo($file, PATHINFO_EXTENSION);
			$guid = $this->createGUID();
			$newFileName = $guid.'.'.$ext;
			if(
				! is_null($file)
			){
				$data['Forumentry']['outside_resource'] = $newFileName;
			}
		} else {
			return 0;
		}
		return $newFileName;
	}

	private function saveAndReturnEntryData(){}
	

	private function setRootIds($parent_id = 1, $root_id=1){
		$this->contain();
		$g = $this->find('all', array('conditions'=>array('Forumentry.parent_id'=>$parent_id)));
		foreach ($g as $key => $value) {
			$value['Forumentry']['root_id'] = $root_id;
			if($value['Forumentry']['parent_id'] != 0){
				$this->save($value);	
				$this->doThing2($value['Forumentry']['id'], $root_id);
			}
		}
	}

	private function setAllRootIds($parent_id = 1){
		$this->contain();
		$g = $this->find('all', array('conditions'=>array('Forumentry.parent_id'=>$parent_id)));
		foreach ($g as $key => $value) {
			$value['Forumentry']['root_id'] = $parent_id;
			if($value['Forumentry']['parent_id'] != 0){
				$this->doThing2($value['Forumentry']['id'], $value['Forumentry']['id']);
			}
		}
	}



	public function getListingsForCollaboration($id){
		return $this->find('all',array('conditions' => array(
			'Forumentry.collaboration_id'=>$id
		)));
	}

	public function getListingsForUser($uid){
		$this->contain();
		// there ought to be a way to do all this in a single query.
		// you are ultimately looking for an array of unique root_ids , 
		$g = $this->find('all',array(
			'conditions'=>array(
				'Forumentry.user_id'=> $uid
				,'Forumentry.root_id != 1'
				)
			,'fields'=>array('id','root_id')

		));

		$b = array();

		foreach ($g as $key => $value) {
			if (!in_array($value['Forumentry']['root_id'], $b)) {
				$b[] = $value['Forumentry']['root_id'];
			}
		}
		$this->contain('User','ChildForumentry');

		$g = $this->find('all',array(
			'conditions'=>array(
				'Forumentry.id'=> $b
				)
			)
		);
		return $g;
	}




	public function saveFileAndEntrysave($data = null, $params = array()){
		// create a new filename for the file that is is in the user tmp folder, 
			/* 
				1. save the entry to the database 
				2. get the ForumEntry id
				3. create the /discussions/entry_{id} folder. 
				4. copy the file to the discucsions folder. 
			*/
		 	$cu = Configure::read("current_user");
			$user_directory = "./users/".$cu['User']['user_dir'];
			// file selection rom tmp folder and tmp folder sanitization.
			$tmpFile = $this->getFileAndDeleteOthers($user_directory.'/tmp/');		
			$newFileName = $this->getNewName($tmpFile);
			
			// debug($tmpFile);  
			// debug($newFileName); 
			/// new entry. create it. 



			if(
				file_exists($user_directory.'/tmp/'.$tmpFile)
			){
				if($data['Forumentry']['id'] == ""){
					// save new one
					$this->create();
					$data['Forumentry']['outside_resource'] = $newFileName;
				}else{
					/// editing existing entry.... check for existing file record in entry
					$data['Forumentry']['outside_resource'] = $newFileName;
				}
			}
			$this->contain();
			$date = new DateTime();

			$s = $date->format('Y-m-d H:i:s');
			$data['Forumentry']['timestamp'] =	$s;
			$data['Forumentry']['user_id'] = $cu['User']['id'];


			////////!!!!!!!!!!!!
			$g = $this->save($data);
			// debug($g);
			if(
				file_exists($user_directory.'/tmp/'.$tmpFile)
			){
				if( $g['Forumentry']['outside_resource'] != "0" ){
					// create desitination folder if ! exists and move the file there.  
					$p = './discussions/entry_'.$g['Forumentry']['id'];
					if( file_exists($p)){
						$this->deleteDirectory($p);
						mkdir($p, 0755);
					}else{
						mkdir($p, 0755);
					}
					///the file may already be in the destination, if not check 
					if(file_exists($p."/".$newFileName)){

					}else{
						// if(file_exists($p."/".$newFileName)){}
						rename($user_directory.'/tmp/'.$tmpFile, './discussions/entry_'.$g['Forumentry']['id'].'/'.$newFileName);
					}
					$this->outside_resource = $newFileName;
				};
			}	
			return $g;
	}



}
