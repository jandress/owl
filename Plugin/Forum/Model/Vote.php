<?php

class Vote extends AppModel {


  public $belongsTo = array(
		'User'=>array(
			'className' => 'User',
			'foreignKey' => 'user_id',
		),
		'Forumentry' => array(
			'className' => 'Forumentry',
			'foreignKey' => 'forumentry_id',
		)
	);



// on double downvote the vote is getting deleted but the change to the journalentry field isnt work.
// same for upvotes.

// simple upvote works.







  public function submitvote($id, $val, $userId){
    // ($id = 0,$val=0,$this->current_user['User']['id']);

    // verify that this user hasnt already voted on this entry.
        //if the entry exists and is positive, (and this is an upvote) then downvote.
        // if the entry exists snd is positive and this is a downvote then decrement by 2.

        //if the entry is negative and this is postive then increment by 2
        // if the entry is negative and this is negative then increment by 1

    // if the entry does not exist, create and save it.  then

    debug("looking up forum entry");
    // Look up the particular entry.
    $entry = ClassRegistry::init('Forumentry');
    $ent = $entry->find("first",array('conditions'=>array('id'=>$id)));
    debug("looking up Vote");

    //
    $vt = $this->find("first",array(
      'conditions'=>array(
        'Vote.user_id'=>$userId,
        'Vote.forumentry_id'=>$id
      )
    ));
    $voteExists = !empty($vt['Vote']);
    if($voteExists){
      //if the entry exists
      debug("vote exists");
      $g = array();
      $g['user_id'] = $userId;
      $g['forumentry_id'] = $id;
      $g['id'] = $vt['Vote']['id'];

      if($vt['Vote']['value']){
        debug("vote value is positive");
        //and the existing Vote value is is positive,
        if($val){
          // the submitted vote is an upvote then delete
          debug("duplicate upvote. deleting");
          $this->delete($vt['Vote']['id']);
          $ent['Forumentry']['upvotes'] = intval($ent['Forumentry']['upvotes'])-1;

        }else{
          //and this is a downvote then decrement by 2.
          $g['value'] = 0;
          debug("save 0");
          debug($this->save($g));
          $ent['Forumentry']['upvotes'] = intval($ent['Forumentry']['upvotes'])-1;
          $ent['Forumentry']['downvotes'] = intval($ent['Forumentry']['downvotes'])+1;

        }
      } else {
        debug("vote value is negative");
        //and the existing Vote value is negative
        if($val){
          // the submitted vote is postive
          $g['value'] = 1;
          debug("save 1");
          debug($this->save($g));
          $ent['Forumentry']['upvotes'] = intval($ent['Forumentry']['upvotes'])+1;
          $ent['Forumentry']['downvotes'] = intval($ent['Forumentry']['downvotes'])-1;
        }else{
          // the submitted vote is negative
          debug("duplicate downvote. deleting: ");
          $this->delete($vt['Vote']['id']);
          $ent['Forumentry']['downvotes'] = intval($ent['Forumentry']['downvotes'])-1;
        }
      }
      debug("exiting vote adjustment.");
      debug($vt['Vote']);
    }else{
      debug("vote does not exist.");
      if($val){
        $ent['Forumentry']['upvotes'] = intval($ent['Forumentry']['upvotes'])+1;
      }else{
        $ent['Forumentry']['downvotes'] = intval($ent['Forumentry']['downvotes'])+1;
      }
      // a vote for this entry for this user already exists.
      $this->create();
      $g = array(
        "user_id" => $userId,
        "forumentry_id" => $ent['Forumentry']['id'],
        "user_id" => $userId,
      );
      if ($val) {
        $g['value'] = 1;
      }else{
        $g['value'] = 0;
      }
      $h = array(
        'Vote'=>$g
      );
      $saved = $this->save($h);
      debug("vote saved: ");
      debug($saved);
    }


    debug("increment or decrement it's votes");
    // increment or decrement it's votes
    debug("saving the entry: ");
    debug($ent);
    $entry_saved = $entry->save($ent);
    exit;
  }



}



/*
array(
	'id' => '9',
	'user_id' => '1025',
	'forumentry_id' => '3',
	'value' => '1'
)
*/




/*

id
user_id
forumentry_id
value

array(
	'Forumentry' => array(
		'id' => '1',
		'title' => 'Root',
		'copy' => 'Everything that is a child of this entry is a top level post. ',
		'upvotes' => '4',
		'downvotes' => '2',
		'user_id' => '1',
		'parent_id' => '0',
		'lft' => '3',
		'rght' => '30',
		'timestamp' => '2020-12-16 16:20:00',
		'course_id' => '0'
	)
)

*/
