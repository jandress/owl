<?php

class FpoPluginModel extends FpoPluginAppModel {


	var $useTable = false;


	public function getLayout($i = 0,$dir = '../View/Layouts/fpos'){
		$handle = opendir($dir);
		$ar = array();

		while (false !== ($file = readdir($handle))) {
			if($file=='.' || $file =='..' || $file =='.DS_Store'){ continue; }
			array_push($ar, str_replace ('.ctp', '', $file ));
		}
		$t = count($ar);
		$i = rand(0,$t-1);
		$file = $ar[$i];
			//$file = 'extend';
		$this->title($file);		
		return 'fpos/'.$file;
	}
	
	
}

