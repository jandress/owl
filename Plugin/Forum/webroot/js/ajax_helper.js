
var ajaxhelper = function() {
/*
		// removed automatic initialization
		var ajax_helper = ajaxhelper();
		ajax_helper.applyAjaxListenerToLinks(_targetButton,_targetDiv,_ldrString );
		//-----
		var ajax_helper2 = ajaxhelper();
		ajax_helper2.initNewHelper(_targetDiv2, _targetButton2, _loaderString2 );
		ect...
		//-----
*/



	var function_object = {

		loaderString : '<div style="width:100%; text-align:center;" ><h4 style="">Loading</h4></div>',
		targetDiv:"#ajax_container",
		territoryPriceDiv:"#PricingPrice",
		targetButton: '.ajax_links a'
		nocache:false,

		onLoadComplete: function(){},

		loadUrl: function(_url){
			//empty out the div and display a preloader
			$(function_object.targetDiv).empty();
			$(function_object.targetDiv).html(function_object.loaderString);
			$.ajax({
				url: _url,
				context: $(function_object.targetDiv),
				success: function(data){
					$(this).empty();
					$(this).html(data);
					function_object.applyAjaxListenerToLinks();
					function_object.onLoadComplete();
				}
			});
		},

		applyAjaxListenerToLinks: function(_targetButton,_targetDiv,_ldrString){
			// CHECK DEFAULTS, (also check if    g = a || b works here)
			// also change this to a single param
			function_object.targetButton = typeof( _targetButton ) == "undefined" ? function_object.targetButton : _targetButton;
			function_object.targetDiv = typeof( _targetDiv ) == "undefined" ?  function_object.targetDiv : _targetDiv;
			function_object.loaderString = typeof( _loaderString ) == "undefined" ? function_object.loaderString : _loaderString;

			var g = $(function_object.targetDiv);
			$(function_object.targetButton).bind('click', function() {
				var n = $(function_object.targetDiv)
				$(function_object.targetDiv).wrapInner('<div id="tmp" />')
				//  $('#tmp').animate({ opacity: 0.25, left: -($('#tmp').width())}, 500, function() {

				// });
				$(function_object.targetDiv).empty();
				function_object.loadUrl($(this).attr('href'));
				////console.log(function_object);
			  return false;
		});



		applyAjaxListenerToASelect: function(_targetSelect,_targetDiv,keyValueObject,_ldrString){
			// CHECK DEFAULTS, (also check if    g = a || b works here)
			// also change this to a single param
			function_object.targetButton = typeof( _targetSelect ) == "undefined" ? function_object.targetButton : _targetButton;
			function_object.targetDiv = typeof( _targetDiv ) == "undefined" ?  function_object.targetDiv : _targetDiv;
			function_object.loaderString = typeof( _loaderString ) == "undefined" ? function_object.loaderString : _loaderString;

			var g = $(function_object.targetDiv);
			$(function_object.targetSelect).change(function(e) {
				//---
				var val = e.target.value;
				//console.log(keyValueObject.val);
				//console.log(keyValueObject[val]);
				//---
				if!(typeof( keyValueObject.val ) == "undefined")){
					//if the key does not exist.
				}
				// var n = $(function_object.targetDiv)
				// $(function_object.targetDiv).wrapInner('<div id="tmp" />')
				// //  $('#tmp').animate({ opacity: 0.25, left: -($('#tmp').width())}, 500, function() {
				//
				// // });
				// $(function_object.targetDiv).empty();
				// function_object.loadUrl($(this).attr('href'));
				// ////console.log(function_object);
			  return false;
			});




		},
	}
	return function_object;
};



//var option = $('<option></option>').attr("value", "option value").text("Text");
// $("#selectId").empty().append(option);
// Territory functions
/*

<select id="PricingPrice" name="data[Pricing][price]">
<option value="null">--</option>
<option value="1">£1</option>
<option value="2">£2</option>
<option value="5">£5</option>
<option value="10">£10</option>
<option value="15">£15</option>
<option value="20">£20</option>
<option value="25">£25</option>
<option value="40">£40</option>
<option value="45">£45</option>
<option value="50">£50</option>
<option value="75">£75</option>
<option value="100">£100</option>
<option value="150">£150</option>
<option value="200">£200</option>
</select>
*/
