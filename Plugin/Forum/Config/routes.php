<?php
   // inlineuploade
   Router::connect('/my/forum/upload/song/*', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'admin_upload_song')); 
   
   Router::connect('/my/forum/add/song/*', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'admin_add_song')); 
   Router::connect('/my/forum/collaboration/add/*', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'admin_add_collaboration')); 
   Router::connect('/my/forum/add/*', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'admin_add'));

   Router::connect('/my/forum/user/*', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'user'));
   Router::connect('/my/forum/*', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'admin_index'));
   Router::connect('/my/forums/*', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'full_listing'));
   Router::connect('/my/forumentry/vote/*', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'vote'));
   Router::connect('/my/forumentry/edit/*', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'admin_edit'));
   Router::connect('/my/forumentry/delete/*', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'admin_delete'));
      
   // Router::connect('/my/forumentry/view/*', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'view'));
   Router::connect('/my/forumentry/*', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'admin_view'));
	Router::connect('/inlineuploader', array('plugin'=>'Forum','controller' => 'Forum', 'action' => 'admin_inline_add_song'));		
	Router::connect('/inlineuploader/*', array('plugin'=>'Forum','controller' => 'Forum', 'action' => 'admin_inline_add_song'));		
   
   // Router::connect('/', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'full_listing'));
   // Router::connect('/my/forums/*', array('plugin'=>'Forum','controller' => 'forum', 'action' => 'full_listing'));

 
 ?>
