<?php
    $badges = array(
        "badge_small_general.png",
        "badge_small_grad.png",
        "badge_small_greek.png",
        "badge_small_hipster.png",
        "badge_small_judge.png",
        "badge_small_nerd.png",
        "badge_small_sherlock.png",
        "badge_small_twain.png",
        "badge_small_world.png",
        "badge_small_zombie.png"
    );
     $b = count( $badges );
     $i = rand(2, $b);
     if(!empty($data['Forumentry'])){
       $d = $data['Forumentry'];
     }else{
       $d = $data;
     }
     $user = $data['User'];
  ?>

<div class="forum_discussion_row tm_1_em ">
  <div class=" col-sm-8 ">
    <div style="margin-top:5px;">
        <div  style="width:120px">
          <!-- forum_voter_arrows -->
          <?php
            $downvoted = $upvoted = $hasVote = false;
            if (!empty($d['Vote'][0])) {
              $hasVote = true;
              $upvoted = $d['Vote'][0]['value'];
              $downvoted = !$upvoted;
            }
           ?>
          <div class="forum_voter_arrows">
              <a href="#<?php echo $d['id']; ?>"><div class="arrow up <?php if($upvoted){ echo " upvoted "; } ?>">&nbsp;</div></a>
              <div class="rank_number">
                  <?php
                  echo($d['upvotes']-$d['downvotes']);
                  ?>
              </div>
              <a href="#<?php echo $d['id'] ?>"><div class="arrow down <?php if($downvoted){ echo " downvoted "; } ?>">&nbsp;</div></a>
          </div><!-- end forum_voter_arrows -->
          <!-- avatar -->
          <div class="avatar pull-left">
            <img src="/owls/badge_small_grad.png" alt="" />
          </div><!-- end avatar -->
        </div>
        <div class=" col-sm-10 ">

        <div class = "media_dest" id = "row_media_dest_<?php echo $d['id']; ?>" style = "width:100%" data-guid = "<?php echo $d['id'] ?>" data-initial-file = "<?php echo $d['outside_resource'] ?>" ></div>            
          <?php           
            if($d['outside_resource']){
              $ext = pathinfo($d['outside_resource'], PATHINFO_EXTENSION);
              if(
                $ext=="mp3"
                || $ext=="mp4"
                || $ext=="m4v"
                || $ext=="wav"
              ){
                echo $this->element('Userassets.user_area/audio_player/audio_player'
                ,array(
                  'guid'=> $d['id'],'initial_file' => $d['outside_resource']
                )
              );
              }
              if(
                $ext=="jpg"
                || $ext=="jpeg"
                || $ext=="png"
                || $ext=="gif"
              ){
               
              }
            } 
          ?>
        <div class = "r5 comment_text comment_text_a" style=""><p><?php echo $d['copy']; ?></p></div>
        <div class="clear clearfix">&nbsp;</div>
          <div class="row_links">
              <ul>
                <li> <a class=""><?php echo $d['id']; ?></a> </li>
                <li>
                  <a class= "" href ="/my/forum/user/<?php echo $data['User']['id']; ?>" ><?php echo $data['User']['username']; ?>, </a>
                </li>
                <li>
                    Submitted <?php echo $this->Time->timeAgoInWords($d['timestamp']); ?>
                </li>
                <?php if ($user['id'] == $current_user['User']['id']): ?>
                  <li>
                      <a class = " edit_btn" href="#<?php echo $d['id']; ?>" >Edit</a>
                  </li>
                  <li>
                      <a class = "" href="/my/forumentry/delete/<?php echo $d['id']; ?>/<?php echo $root_id ?>" >Delete</a>
                  </li>
                <?php endif; ?>
                <li><a class = " reply_btn" href="#<?php echo $d['id']; ?>_<?php echo $data['parent_id'] ?>"> Reply</a></li>

                <li>
                
                <?php echo $this->element('song_selection_ui/song_select_upload_choice', array('id'=> $d['id'].'_'.$data['parent_id'])); ?>


              </li>
              <li>
                  <a class = "file_uploader_btn" href="#<?php echo $d['id']; ?>_<?php echo $data['parent_id'] ?>"> File Uploader</a>
              </li>
                <?php if (count($d['Vote'])>1): ?>
                  <li> <div class="badge badge-danger">!</div> </li>
                <?php endif; ?>
              </ul>
          </div>
          <div id="comment_reply_div_<?php echo $d['id']; ?>"></div>
        </div>
        <div class="clear clearfix">&nbsp;</div>
    </div> <!-- end forum_title_row -->
  </div><!-- end entry container -->
<div class="clear clearfix tm_3_px">&nbsp;</div>

<div class="">
  <?php if (!empty($data['ChildForumentry'])): ?>
    <?php foreach ($data['ChildForumentry'] as $key => $value): ?>
          <div class="child_comment tm_2_em">
                <?php
                    echo $this->element('Forum.forum_discussion_row', array('badges'=>$badges,'data'=>$value, "root_id" => $root_id,'pathToResource'=>$pathToResource));
                ?>
          </div>
    <?php endforeach; ?>
  <?php endif; ?>
</div>

</div>
<div class="clear clearfix">&nbsp;</div>



