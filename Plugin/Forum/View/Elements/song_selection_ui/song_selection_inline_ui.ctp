<?php
    $root_uri = "/my/userassets/";
    $target_directory = "tmp/";
?>
    <!-- http://digg:8889/my/userassets/upload/collaborations/the_mississippi_burnouts/raw_jams/ -->
<?php

// $target_directory = './users/';
?>

		<div class = "upload_toolbar_link btn-group bm_1_em pull-right" >
            <a class = 'btn btn-sm btn-default fb_ajax_link' href = '<?php echo $root_uri.''.$target_directory; ?>' >
                <span class = "glyphicon glyphicon-remove" ></span>
            </a>
        </div>				



<div class="container-fluid">
	<div class="well">
		<div class="white_well centered">
			<div class = "clearfix  tm_1_em">&nbsp;</div>
			<div id="file-uploader-demo1">		
				<noscript>	</noscript>         
			</div>
		</div>
	</div>
</div>


<link href="/Userassets/css/fileuploader.css" rel="stylesheet" type="text/css">	
<script src="/Userassets/js/fileuploader.js" type="text/javascript"></script>
<?php 
if (empty($root_uri)): 
	 $root_uri = '/assets/';
?>	

<?php endif;
    // $root_uri = "/users/be0fbff5ded65f6524a26955bb72965cb6823249/jams/blergh";
    //   $action = "/my/userassets/upload/tmp/";
    $action = "/my/forum/upload/song/tmp";
?>
<script>        

    function createUploader(){     
        var uploader = new qq.FileUploader({
            element: document.getElementById('file-uploader-demo1'),
			<?php 
				$debug = true;
				if (!empty($debug)){
					echo 'debug: "true",'."\n";
				}
				if (!empty($action)){
					echo 'action: "'.$action.'",'."\n";				
				}else{
					echo 'action: "'.$this->Html->url().'",'."\n";
				}
				if (!empty($allowedExtensions)){
					echo "allowedExtensions: ".$allowedExtensions.","."\n";
				}
			?>
			failedUploadTextDisplay: {
				mode: 'custom',
				maxChars: 40,
				responseProperty: 'error',
				enableTooltip: true
			},
			onProgress: function(id, fileName, loaded, total){
				console.log("onprogress")
			},

			onComplete: function(id, fileName, responseJSON){
				<?php if (!empty($debug)): ?>
					console.log('responseJSON: '+responseJSON);
				<?php endif ?>
				$('#sliding_content_container-fluid').fadeOut('slow', function(){
					<?php if (!empty($redirect)){
						if($redirect=="self"){
							$redirect = $root_uri.$target_directory;//$this->Html->url();
						}
						echo 'window.location = "'.$redirect.'";';
					}?>
				});
			},
			onSubmit: function(){
				uploader._options.params = {
						<?php 						
						if(!empty($model)){
							echo $model.':{';
							if(!empty($form_params)){
								$g = 0;
								$t= count($form_params);
								foreach ($form_params as $key => $value){
									echo $key.':'.$value."\n";
									$g++;
									if($g<$t){
										echo ',';
									}
								}
							}
							echo '}';
						}
						?>
				};				
			}			
        });     
		var g;
    }
//    window.onload = createUploader;     
 createUploader();
</script>

