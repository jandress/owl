<div class="navbar navbar-inverse">
	<h2 class=" col-sm-4">Forum Name. Forum Top Level List. </h2>
	<ul class="nav navbar navbar-nav pull-right">
				<li class = "active">
					<a class="" href="/my/forum/<?php echo $data['Course']['id']; ?>">All My Forums</a>
				</li>
	</ul>
</div>


<div class="tm_1_em container-fluid">


	<?php $d = $data; ?>
	<div class="hero-unit">

		<h2>Adding a Post to Course: <?php echo $d['Course']['name']; ?> </h2>

				<!--  text input box-->
				<div class="col-sm-6">
					<?php echo $this->BootstrapForm->create('Forumentry', array('class' => ''));
							echo $this->BootstrapForm->hidden('course_id',array("value"=>$d['Course']['id']));
							echo $this->BootstrapForm->input('title',array("class"=>"col-sm-12 bm_1_em block"));
							echo $this->BootstrapForm->textarea('copy',array("class"=>"forum_text_entry_box col-sm-12 bm_1_em block"));
							echo $this->BootstrapForm->submit('Submit', array('action' => 'submit'));
							echo $this->BootstrapForm->end();
					?>
				</div>




	</div>
</div>
<div class = "well">
	remember that when adding a post for an existing song, like from the file brower song contextual menu,
	the Url is /my/forum/add/song/{ path/to/file.mp3 }<br/>

	When adding a post from the Collaboration page, it should be:<br/> /my/forum/community/add/<br/>
	/my/forum/community/add/song/{community_id}/{song_id}

	Add a post from:

	<ul>
		<li>the collaboration page</li>
		<li>the filebrowser<br/>http://digg:8889/my/forum/add/song/collaborations/the_mississippi_burnouts/raw_jams/ecoomic_systems.wav</li>
		<li>Edit: /my/forumentry/edit/198</li>
	</ul>
	
	
</div>