<div class="container-fluid tm_1_em">
 <div class="forum_listing">
   <?php
       $badges = array(
           "badge_small_general.png",
           "badge_small_grad.png",
           "badge_small_greek.png",
           "badge_small_hipster.png",
           "badge_small_judge.png",
           "badge_small_nerd.png",
           "badge_small_sherlock.png",
           "badge_small_twain.png",
           "badge_small_world.png",
           "badge_small_zombie.png"
       );

       foreach ($this->data as $key => $value) {
          //  debug($value);
          echo $this->element('forum/forum_title_row', array("data"=>$value, 'recursion'=>1,'badges'=>$badges[$key]));
       }
    ?>
 </div>
 <?php debug($this->data); ?>
</div>
