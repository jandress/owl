<?php
    $badges = array(
        "badge_small_general.png",
        "badge_small_grad.png",
        "badge_small_greek.png",
        "badge_small_hipster.png",
        "badge_small_judge.png",
        "badge_small_nerd.png",
        "badge_small_sherlock.png",
        "badge_small_twain.png",
        "badge_small_world.png",
        "badge_small_zombie.png"
    );
    $d = $data['Forumentry'];

    if($d['child_count'] == 1){
      $comment_string = "1 Comment";
    } else {
      $comment_string = $d['child_count'].' comments';
    }
    $fe = $d;
?>



<div class="forum_title_row" id="e_<?php echo($fe['id']); ?>">
    <div  style="width:120px">
      <!-- forum_voter_arrows -->
      <div class="forum_voter_arrows">
          <a href="#"><div class="arrow up">&nbsp;</div></a>
          <div class="rank_number">
              <?php
              echo($d['upvotes']-$d['downvotes']);
              ?>
          </div>
          <a href="#"><div class="arrow down">&nbsp;</div></a>
      </div><!-- end forum_voter_arrows -->
      <!-- avatar -->
      <div class="avatar pull-left">
        <img src="/owls/<?php echo $badges[rand(1,8)] ?>" alt="" />
      </div><!-- end avatar -->
    </div>

    <div class=" col-sm-10 ">
      <a class = "" href = "/my/forumentry/<?php echo $d['id']; ?>" ><h3><?php echo $d['id'].' '.$d['title']; ?></h3></a>
      <div class="row_links">
        <h6 class = "gt_15 submitted_by" >Submitted by</h6>
        <h6 class="gt_2 comment_username" ><a href ="/my/forum/user/<?php echo $data['User']['id']; ?>" ><?php echo $data['User']['username']; ?></a></h6>
        <?php if (!empty($full_listing)): ?>
            <h6 class="gt_15 comment_date" >to <a class = ""  href = "/my/forum/<?php echo $data['Forumentry']['course_id']; ?>" >Forum <?php echo $data['Forumentry']['course_id']; ?></a>, </h6>
        <?php endif; ?>
        <h6 class="gt_15 comment_date" ><?php echo $this->Time->timeAgoInWords($d['timestamp']); ?></h6>
        <ul>
          <li>
              <a class = ""  href = "/my/forumentry/<?php echo $d['id']; ?>" ><?php echo $comment_string; ?></a>
          </li>
          <?php if ($data['User']['id'] == $current_user['User']['id']): ?>
            <li>
                <a class = "" href="/my/forumentry/edit/<?php echo $fe['id']; ?>" >Edit</a>
            </li>
            <li>
                <a class = "" href="/my/forumentry/delete/<?php echo $fe['id']; ?>/<?php echo  $fe['id'] ?>" >Delete</a>
            </li>
        </ul>
        <?php endif; ?>
      </div>
    </div>
    <div class="clear clearfix">&nbsp;</div>
</div> <!-- end forum_title_row -->
