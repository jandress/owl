<?php
//      echo $this->element('Forum.song_selection_ui/song_selection_ui'); 

        $pathToResource = "/discussions/entry_7/";
		$badges = array(
				"badge_small_general.png",
				"badge_small_grad.png",
				"badge_small_greek.png",
				"badge_small_hipster.png",
				"badge_small_judge.png",
				"badge_small_nerd.png",
				"badge_small_sherlock.png",
				"badge_small_twain.png",
				"badge_small_world.png",
				"badge_small_zombie.png"
		);
		$fe = $data['Forumentry'];
		$d = $fe;
		$user = $data['User'];
		if($fe['child_count'] == 1){
			$comment_string = ", 1 Comment";
		} else {
			$comment_string = ", ".$fe['child_count'].' comments';
		}
?>




<div class="navbar navbar-inverse">
	<a class="" href="/my/forum/<?php echo ''; ?>" ><h2 class=" col-sm-4"><?php echo ''; ?></h2></a>
	<ul class="nav navbar navbar-nav pull-right">
		<li class = "active">
			<a class="" href="/my/forum/<?php echo ''; ?>">Forum Home</a>
		</li>
	</ul>
</div>








<?php
	// TOP MEDIA PLAYER
	$files = array($data['Forumentry']['outside_resource']);
?> 
<div class = "media_dest" id = "row_media_dest_<?php echo $data['Forumentry']['id']; ?>" style = "width:100%" class = "" data-guid = "<?php echo $data['Forumentry']['id']; ?>" data-initial-file = "<?php echo $files[0]; ?>" ></div>            
	<div class = "audio_player tm_1_em" id = "audio_player_<?php echo $data['Forumentry']['id']; ?>">
		<?php echo $this->element('Userassets.user_area/audio_player/audio_player_ui',array('initial_file'=>$files[0],'guid'=>$data['Forumentry']['id'])); ?>
		<?php echo $this->element('Userassets.user_area/audio_player/audio_visualizer',array('initial_file'=>$files[0],'guid'=>$data['Forumentry']['id'])); ?>
	</div>
	<?php //echo $this->element( 'Userassets.user_area/audio_player/AudioPlayerJs',array('initial_file'=>$files[0],'guid'=>$data['Forumentry']['id']));  ?>
</div>



 





<div class = "tm_1_em container-fluid">

		<div class=" forum_entry" id="e_<?php echo($fe['id']); ?>">
		<div>
			<div style="width:120px">
				<?php
					$downvoted = $upvoted = $hasVote = false;
					if (!empty($data['Vote'][0])) {
						$hasVote = true;
						$upvoted = $data['Vote'][0]['value'];
						$downvoted = !$upvoted;
					}
				 ?>
				<div class="forum_voter_arrows">
						<a href="#<?php echo $d['id']; ?>"><div class="arrow up <?php if($upvoted){ echo " upvoted "; } ?>">&nbsp;</div></a>
						<div class="rank_number">
								<?php
								echo($d['upvotes']-$d['downvotes']);
								?>
						</div>
						<a href="#<?php echo $d['id'] ?>"><div class="arrow down <?php if($downvoted){ echo " downvoted "; } ?>">&nbsp;</div></a>
				</div><!-- end forum_voter_arrows -->
			  <div class="avatar pull-left">
			    <img src="/owls/badge_small_zombie.png" alt="" />
			  </div>
			</div>
		</div>
		<!-- Title, copy and post action links -->
		<div class="col-sm-10 forum_entry_field">
		  <h3>
		    <?php echo $fe['title']; ?>
		  </h3>
		  <div class="row_links">
		    <h6 class="gt_15" style="display:inline">Submitted yesterday by</h6>
				<h6 class="gt_2" style="display:inline">
					<a href="/my/forum/user/<?php echo $user['id'];?>"><?php echo $user['username'];?></a>
				</h6><h6 class="gt_15" style="display:inline"><?php echo $comment_string; ?></h6>
		  </div>
		  <div class="tm_1_em col-sm-10 pl_0" >
		    <div class = "r5 comment_text" style="">
		        <p>
		          <?php echo $fe['copy']; ?>
		        </p>
		    </div>
			<div  class = "row_links">
				<ul>
					<li>1 Comment</li>
					<li><a class = " edit_btn" href="#<?php echo $fe['id']; ?>" >Share</a></li>
					<?php if ($user['id'] == $current_user['User']['id']): ?>
						<li>





							<a class = "edit_btn" href="#<?php echo $fe['id']; ?>" >Edit</a>





							
						</li>
						<li>
							<a class = "" href="/my/forumentry/delete/<?php echo $fe['id']; ?>/<?php echo  $fe['id'] ?>" >Delete</a>
						</li>
					<?php endif; ?>
				</ul>
			</div>
		  </div>
		</div><!-- End Title, copy and post action links -->
		<div class="clear clearfix">&nbsp;</div><hr/>
		<!--  text input box-->
		<div class="col-sm-6">
			<div id = "row_media_dest_0"></div>
			<?php echo $this->BootstrapForm->create('NewForumentry', array('class' => ''));?>
			<?php
			//background-color:#202020; border-color:#454545; color:#ececec; border-radius:15px;
					echo $this->BootstrapForm->hidden('id');
					echo $this->BootstrapForm->textarea('copy', array("style"=>array("background-color:#202020; color:#ececec;  border-color:#454545; border-radius:15px;")));
					echo $this->BootstrapForm->hidden('action',array("value"=>'new'));
					echo $this->BootstrapForm->hidden('parent_id',array("value"=>$fe['id']));
					echo $this->BootstrapForm->hidden('root_id',array("value"=>$fe['id']));
					echo $this->BootstrapForm->submit('Submit', array('action' => 'submit','class'=>"btn btn-default pull-left tm_1_em"));
					echo $this->BootstrapForm->end();
			?>
		</div>
		<div class="clear clearfix ">&nbsp;</div>


		<div class="tm_4_em">
			<?php  if (!empty($data['ChildForumentry'])): ?>
				<?php foreach ($data['ChildForumentry'] as $key => $value): ?>
					<div class="child_comment">
						<?php
								if(!empty($value['Forumentry'])){
									$d = $value['Forumentry'];
								}else{
									$d = $value;
								}
								$ext = pathinfo($d['outside_resource'], PATHINFO_EXTENSION);
								if( $d['outside_resource'] != "0"
									&& ( $ext == "wav" || $ext == "mp3" )){													
										// array_push($files,$pathToResource.$d['outside_resource']);
										// array_push($guids,$d['id']);
								}	
								 echo $this->element('Forum.forum_discussion_row', array('badges'=>$badges,'data'=>$value,'root_id' => $fe['id'],'pathToResource'=>$pathToResource));
								
					
								echo '<div class="clear clearfix bm_2_em">&nbsp;</div>';

						?>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
</div>
<?php // debug($data); ?>
<script src = "/forum/js/forum_ui.js" type="text/javascript"></script>
