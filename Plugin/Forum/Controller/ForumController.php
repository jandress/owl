<?php

class ForumController extends ForumAppController {

	var $uses = array('Forum.Forumentry',"Course","Forum.Vote",'Userassets.Fileasset');
	public $components = array('Paginator', 'Session','Userassets.Fileutil','Forum.Forum');
  	// public $uses = array('Forumentry','Time');
	public $layout = "Userassets.user_area_layout";

	public function beforeFilter(){
		parent::beforeFilter();
	}

	var $file_dir_root_relative = './users/'; // relative is needed by Fileutil. 
	var $file_dir_root_absolute = '/users/'; // i believe abs is needed by the view.

	// For /my/forum/id
	public function admin_index($id = 1){
		$this->data =  $this->Forumentry->getTopLevelListing($id);
    	$this->set('data',$this->data);
		$this->set('id', $id );
	}

	// For /my/forumentry/X
	public function admin_view($id = 0){
			// $this->doThing(); 
			// $this->Forumentry->contain();
			// $g = $this->Forumentry->find('all');
			// debug($g);
			// exit;
		// I do not like how you are using this for submitting edits as well.   
		// for editing comment or replying?
		if ($this->request->is('post') || $this->request->is('put') ) {
			$d = $this->request->data;
			if(!empty($this->request->data['NewForumentry']) ){
				// this is a new forum entry. create it.
				$this->Forumentry->create();
				$d = $this->request->data;
				$this->request->data['Forumentry'] = $this->request->data['NewForumentry'];
				$d['Forumentry'] = $d['NewForumentry'];
			}else{
				$d = $this->Forumentry->getForumentry($this->request->data['Forumentry']['id']);
				$d['Forumentry']['id'] = $this->request->data['Forumentry']['id'];
				// $this->addAdditionalScript('/Userassets/js/audio_layer');
			}
			$d['Forumentry']['copy'] = $this->request->data['Forumentry']['copy'];
			$d['Forumentry']['user_id'] = $this->current_user['User']['id'];
			$s = $this->getTimestamp();
			if(!isset($d['Forumentry']['outside_resource'])){
				$d['Forumentry']['outside_resource'] = "0";
			}
			$d['Forumentry']['timestamp']= $s;
			    // debug($d); exit;
			// debug($d['Forumentry']);
			// exit;
			// $this->Forumentry->save($d['Forumentry']);
			$p = $this->Forumentry->saveFileAndEntrysave($d);
			if($p){

			}
		}
		$this->addAdditionalScript('/Userassets/js/audio_player');
		$this->addAdditionalScript('/Userassets/js/forum_audio_player_instantiation');
		// exit;
		$t = $this->Forumentry->getForumentry($id);
		if(empty($t)){
			throw new Exception("Invalid Entry", 1);
		}

		$this->data=$t;
		$this->set('data',$t);
		// debug($this->layout); exit;
	}

	public function full_listing(){
		$this->data =  $this->Forumentry->getFullTopLevelListing();
    	$this->set('data',$this->data);
	}

	public function admin_add($id = 1) {
		if ($this->request->is('post')) {
			$d = $this->request->data;
			$d['Forumentry']['user_id'] = $this->current_user['User']['id'];
			$d['Forumentry']['course_id'] = $id;
			$d['Forumentry']['parent_id'] =	1;
			$s = $this->getTimestamp();
			$d['Forumentry']['timestamp'] =	$s;
			$b = $this->Forumentry->create($d);
			$p = $this->Forumentry->save();
			if($p){
				$this->redirect("/my/forumentry/".$p['Forumentry']['id']);
			}
		}
		$this->Course->contain();
		$this->Course->id = $id;
		$b = $this->Course->read();
		// debug($b);
		$this->set("data",$b);
	}


	public function admin_inline_add_song($url){



	}
	
	public function admin_add_song($url) {
		// $id = $url;
		if ($this->request->is('post')) {
			// if there is data, treat it for saving, 
			// create the forumEntry and then $this->Forumentry->saveFileAndEntrysave();
			// then redirect to the newyly created forumEntry.
			$d = $this->request->data;
			$d['Forumentry']['user_id'] = $this->current_user['User']['id'];
			if(isset($id)){
				$d['Forumentry']['course_id'] = $id;
			}
			
			// debug("it looks like this might have just been copypasta. there is no id in a created entry. ");
			$d['Forumentry']['parent_id'] =	1;
			$s = $this->getTimestamp();
			$d['Forumentry']['timestamp'] =	$s;
			$b = $this->Forumentry->create($d);
			$p = $this->Forumentry->saveFileAndEntrysave();
			if($p){
				$this->redirect("/my/forumentry/".$p['Forumentry']['id']);
			}	
		}

		
		$a = implode('/',$this->params['pass']);
		
		$this->Forumentry->contain();
		// $this->Course->id = $id;
		$b = $this->Forumentry->read();
		
		$this->set("file",$a);
		$this->set("file_path","/users/be0fbff5ded65f6524a26955bb72965cb6823249/");

		// debug($a); exit;
		$this->addAdditionalScript('/Userassets/js/audio_player');
		$this->addAdditionalScript('/Userassets/js/standalone_player_instantiation');

	}


	
	public function vote($id = 0,$val=0){
		$val = strtolower($val);
		if($val == "false" ){
			$val = 0;
		}
		$this->Vote->submitvote($id,$val,$this->current_user['User']['id']);
	}

	public function admin_edit($id = 0){
		
		if ($this->request->is('post') || $this->request->is('put')) {
			$b = $this->Forumentry->create();
			$d = $this->request->data;
			// exit;
			$d['Forumentry']['user_id'] = $this->current_user['User']['id'];
			// exit;
			$s = $this->getTimestamp();
			$d['Forumentry']['timestamp']= $s;
			// debug($d);
			if($this->Forumentry->save($d['Forumentry'])){
				$p = $this->Forumentry->saveFileAndEntrysave($d);
				//  debug($p); exit;
				 $this->redirect("/my/forumentry/".$d['Forumentry']['id']);
			}else{
				debug("failure");  exit;
			}
		}
		$t = $this->Forumentry->getForumentry($id);
	//	debug($t ); 
		$this->data=$t;
		$this->set('data',$t);
		$this->addAdditionalScript('/Userassets/js/audio_player');
		$this->addAdditionalScript('/Userassets/js/forum_audio_player_instantiation');
		$this->addAdditionalScript('/forum/js/forum_ui');

	}

	public function admin_add_collaboration($collaboration_id = 0){
		
		if ($this->request->is('post') || $this->request->is('put')) {
			
			$b = $this->Forumentry->create();
			$d = $this->request->data;
			
			// exit;
			$d['Forumentry']['user_id'] = $this->current_user['User']['id'];
			
			// exit;
			$s = $this->getTimestamp();
			$d['Forumentry']['timestamp']= $s;
			// debug($d);
			if($this->Forumentry->save($d['Forumentry'])){
				// debug(2);

				$p = $this->Forumentry->saveFileAndEntrysave();
				//  debug($p); exit;
				 $this->redirect("/my/forumentry/".$d['Forumentry']['id']);
			}
		}
		$t = $this->Forumentry->getForumentry($id);
		$this->data=$t;
		$this->set('data',$t);
	}


	public function admin_delete($id = 0, $rootId = 1) {
		$this->Forumentry->id = $id;
		$g = $this->Forumentry->read();
		if($id==0 || $id==1){
			throw new \Exception("Error Processing Request", 1);
		}
		if($this->Forumentry->delete()){
			  $this->redirect("/my/forumentry/".$rootId);
		}
	}

	private function getTimestamp(){
			$date = new DateTime();
			return $date->format('Y-m-d H:i:s');
	}

	public function user($id = 4){}
	public function index(){}


	public function admin_upload_song($url='') {
		// this is an ajax item. 


		// debug("chehoo");
		$this->set('debug', true );
		$this->set('redirect', 'self' );
		$this->setCommonVars();
		// this is wierd.   it doesn't ever seem to get populated.
		$this->directoryPathArray .=',""';
		$this->set('directoryPathArray', $this->directoryPathArray );	
		//--

		if ($this->request->is('post')) {
			$user_dir = $this->User->hashString(
				$this->current_user['User']['username'] 
			);
			echo '{success: '.$this->Fileasset->saveFileToName("tmp_file",$this->request->data, $this->file_dir_root_relative.'/'.$user_dir.'/'.$this->current_directory).'}';
			exit;
		}
	}








	       		
	function directoryArrayToString($ar,$trailingSlash = true){
		$g = '';
		foreach ($ar as $key => $value) {
			if($key == count($ar)-1){
				$g .= $value;
				if ($trailingSlash) {
					$g .= '/';						
				}
			}else{
				$g .= $value.'/';						
			}
		}
		return $g; 
	}
	

	 /// after you hit upload and the file uploads


	private function setCommonVars($isColabArea=false){
		$ar = $this->params->params['pass'];
		$this->current_directory = $this->directoryArrayToString($ar );


		if(file_exists($this->file_dir_root_relative.$this->current_directory)){
			
		}

		// on the file upload, this is accessing  /users/tmp/ because the user_hash isnt getting set. that directory is unused.
		// the 32asw	q

		$this->dir_contents = $this->Fileutil->getDirectoryContents($this->file_dir_root_relative.$this->current_directory);
		//--
		$this->set('file_dir_root_relative',$this->file_dir_root_relative);
		$this->set('file_dir_root_absolute',$this->file_dir_root_absolute);			
		//--
		$this->set('target_directory',$this->current_directory ); //
		
		$this->set('filepath',$this->file_dir_root_absolute.$this->current_directory);			
		//--
		$this->set('dir_contents',$this->dir_contents);
		$this->set('listing',$this->dir_contents);
		$this->set('root_uri',$this->root_uri);
		//--	
		$ar = $this->params->params['pass'];
		$directoryPathArray = $ar;			
	
	}
	





	


}