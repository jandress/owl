








//Runs through a multidensional array  of fret ids.    drill_name isnt used within the method. 

function runDrill(_drill_pattern, drill_name){
    // console.log(_drill_pattern, drill_name);
    guitar.visualizeSelectNotes("clear");
    guitar.hideAll();
    var scope = this;
    var _scope = this; //~ hack?   does sscope not work here?


    // an array fret ids for the caged section.   if you get guitar_model.neckData.scale_positions_flat instead, it runs up the neck then proceeds to the next string and runs up again. 
    var caged_array = this.caged_array = caged.getCagedPositionArrayForFret(4);


     this.drill = {

        name:drill_name,
        pattern:_drill_pattern[0],
        caged_array:caged_array,
        scope:scope,
        interval:400,
        drill_pattern_index:0,
        caged_scale_index:0,

        resetCaged(){
            for (var i = 0; i < this.caged_array.length; i++) {
                $(this.caged_array[i]).removeClass("not_shown")
                $(this.caged_array[i]).addClass("reduced")
            }
        },

        run(scope,stop){
            alert("interval_runner.js")
            var timeout;
            var drill_pattern = scope.pattern;
            ///loop throught drill_pattern then advance the
            function goThroughDrill(){
                scope.resetCaged()
                var a  = scope.caged_scale_index+parseInt(drill_pattern[scope.drill_pattern_index])-1;
                var tr = caged_array[a].replace("#fret_","").split("_");
                $(caged_array[a]).removeClass("reduced")

                if( scope.drill_pattern_index == 0 ){
                    $(".note").removeClass("red")
                    $(caged_array[a]).addClass("red")
                }
                guitar.playSoundForNote( parseFloat(tr[0])+1, tr[1]);
                scope.drill_pattern_index++;
                if( scope.drill_pattern_index < drill_pattern.length){
                    if(!stop){
                        scope.intervalHandle = setTimeout(goThroughDrill, scope.interval);
                    }
                }else{
                    scope.caged_scale_index++
                    if( scope.caged_scale_index < caged_array.length ){
                        scope.drill_pattern_index = 0;
                        if(!stop){
                            scope.intervalHandle = setTimeout(goThroughDrill, scope.interval);
                        }
                    }else{
                        $(".note").removeClass("red").removeClass("reduced")
                        drill_pattern_index:scope.drill_pattern_index;
                        caged_scale_index:scope.caged_scale_index;
                    }
                }
            }
            goThroughDrill();
        },
        next(){
            this.stop();
            this.run(this, true);
        },
        prev(){
            console.log("prev")
            this.stop();
            this.scope.drill_pattern_index = this.scope.drill_pattern_index-2
            // this.scope.caged_scale_index = this.scope.caged_scale_index-2
            this.run(this, true);
        },
        stop(){
            clearTimeout(this.intervalHandle)
        },
        play(){
            this.run(this)
        },
        foo(){
            this.run(this)

        },
    }
    this.drill.foo();
}



