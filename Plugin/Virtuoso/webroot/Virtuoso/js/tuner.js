alert("hi")
class GuitarTuner {
    constructor() {
        //    navigator.mediaDevices.getUserMedia(audioIN)

    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // WebRTC is supported, and you can use navigator.mediaDevices.getUserMedia
        // Your code for accessing the microphone and working with audio can go here
        } else {
        console.error('WebRTC is not supported in this browser');
        // Provide a fallback or alternative solution for browsers that do not support WebRTC
        }
      this.audioContext = new (window.AudioContext || window.webkitAudioContext)();
      this.analyser = this.audioContext.createAnalyser();
      this.microphoneStream = null;
      this.stringFrequencies = {
        'E4': 329.63, // High E
        'B3': 246.94, // B
        'G3': 196.00, // G
        'D3': 146.83, // D
        'A2': 110.00, // A
        'E2': 82.41,  // Low E
      };
  
      this.init();
    }
  
    async init() {
      try {
        var t = navigator.mediaSession;
        const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
        this.microphoneStream = this.audioContext.createMediaStreamSource(stream);
        this.microphoneStream.connect(this.analyser);
        this.analyser.fftSize = 2048;
        this.bufferLength = this.analyser.frequencyBinCount;
        this.frequencyData = new Float32Array(this.bufferLength);
      } catch (error) {
        console.error('Error accessing microphone:', error);
      }
    }
  
    getTuning() {
      this.analyser.getFloatTimeDomainData(this.frequencyData);
  
      const detectedFrequencies = this.detectStringFrequencies();
      if (detectedFrequencies) {
        console.log('Detected Frequencies:', detectedFrequencies);
        return detectedFrequencies;
      }
  
      return null;
    }
  
    detectStringFrequencies() {
      const detectedFrequencies = {};
  
      Object.keys(this.stringFrequencies).forEach((string) => {
        const targetFrequency = this.stringFrequencies[string];
        const detectedFrequency = this.detectPitch();
  
        if (detectedFrequency) {
          const difference = Math.abs(targetFrequency - detectedFrequency);
          const isTuned = difference < 5; // Adjust this threshold based on your preference
  
          detectedFrequencies[string] = {
            target: targetFrequency,
            detected: detectedFrequency,
            isTuned,
          };
        }
      });
  
      return detectedFrequencies;
    }
  
    detectPitch() {
      // Use a pitch detection library or algorithm here.
      // For simplicity, we'll assume you have a function called `detectPitch` that returns the detected pitch.
      // You may consider using a library like Pitch.js for more accurate pitch detection.
  
      // Example: const detectedPitch = detectPitch(this.frequencyData, this.audioContext.sampleRate);
      // For this example, let's assume the pitch is directly obtained from the frequency data.
      const peak = this.findPeak(this.frequencyData);
      const detectedPitch = peak * this.audioContext.sampleRate / this.bufferLength;
      return detectedPitch;
    }
  
    findPeak(data) {
      let maxIndex = 0;
      let maxValue = data[0];
  
      for (let i = 1; i < data.length; i++) {
        if (data[i] > maxValue) {
          maxValue = data[i];
          maxIndex = i;
        }
      }
  
      return maxIndex;
    }
  }
  
  // Example usage:
  const guitarTuner = new GuitarTuner();
  
  // You can continuously call getTuning to get the current tuning of all strings.
  setInterval(() => {
    const tuningResult = guitarTuner.getTuning();
    // Add your logic to display the tuning result or take action accordingly.
  }, 1000);
  