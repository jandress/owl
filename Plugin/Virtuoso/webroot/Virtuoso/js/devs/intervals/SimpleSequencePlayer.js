
/*
 Playable guitar makes calls on Guitar.js 

*/

class SimpleSequencePlayer extends EventsManager{

	timer_interval_length = 250; // rename this
    tick_counter = 0;
    intervalHandle = null;    
    sequence = []
    play_index = 0;
    intervalSequencer;

	constructor() {
		super();
        this.reset();    
	}

    start(){
        self = this;
        clearInterval(this.intervalHandle); 
        this.intervalHandle = setInterval(function() { self.tick() }, 1);   
        this.tick_counter = 0;
        this.showNote();
    }

    stop(){
        clearInterval(this.intervalHandle);
        this.reset();    
    }

    pause(){
        clearInterval(this.intervalHandle); 
        this.play_index++;
    }

    reset(){
        this.tick_counter = 0;
        this.play_index = 0;
        guitar.hideAll();
    }

    setString(v){
        this.string = v;
    }
    
    loadSequence(seqArr){
        this.sequence = seqArr;
    }

    complete(){

    }

    showNote(){
        if( this.play_index >= this.sequence.length){
            this.play_index = 0; 
            clearInterval(this.intervalHandle);
            this.complete()
            return;
        }else{    
            //second version is with fret effects
            //guitar.showFretById(b, true)   
            var b = this.sequence[this.play_index].replace("#","")
            guitar.showFretById(b, true,true,true)   
            this.play_index++;    
        }
    }

    tick(){
        $("#caption_holder").html(this.tick_counter);
        if(this.tick_counter >= this.timer_interval_length){
            this.tick_counter = 0;
            this.showNote();                        
        }
        this.tick_counter++;
    }
}








