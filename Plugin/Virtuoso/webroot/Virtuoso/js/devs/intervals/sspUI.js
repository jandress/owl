class sspUI extends SimpleSequencePlayer{

	_scope = this;
    intervalSequencer;
    string = 0;
    interval = 1;
    timer_interval_length = 25;

	constructor(intervalSequencer) {
		super();
		this._scope = this;
		this.init();
        this.intervalSequencer = intervalSequencer;
        var p = intervalSequencer.assembleBasicSequence(3,0,0);
        this.loadSequence( p ); 
        //// half_steps_to_increment = 1, string = 0, starting_fret = 0 
	}

    pause(){
        $(".glyphicon-play").removeClass("not_shown")
        $(".glyphicon-pause").addClass("not_shown")
        super.pause();
    }

    play(){
        $(".glyphicon-pause").removeClass("not_shown")
        $(".glyphicon-play").addClass("not_shown")
        super.start();        
    }

    stop(){
        this.pause();
        super.reset();
    }

    changeInterval(val){
        // half_steps_to_increment = 1, string = 0, starting_fret = 0 
        $(".interval_select_label").text(val)    
        $("#interval_select").removeClass("open");
        this.interval = val;
        this.loadSequence(
            intervalSequencer.assembleBasicSequence(this.interval,this.string,0)
        );
        // this.start();
    }

    changeString(int){
        var ar = [
            "E",
            "A",
            "D",
            "G",
            "B",
            "E"];
        $(".string_select_label").text(ar[int])
        $("#string_select").removeClass("open");
        this.string = int;
        this.loadSequence(
            intervalSequencer.assembleBasicSequence(this.interval,this.string,0)
        );
        // this.start();
    }
    

	init(){
		var _scope = this._scope;
        var handle = $( "#custom-handle" );
        $( "#slider" ).slider({
            min: 5,
            max: 500,
            value:_scope.timer_interval_length,
            step:10,
            create: function() {
                handle.text( $( this ).slider( "value" ) );
            },
            start:function( event, ui ) {
                _scope.stop();
            },
            slide: function( event, ui ) {
                handle.text( ui.value );
            },
            change: function( event, ui ) {
                //handle.text( ui.value );
                _scope.timer_interval_length = ui.value;
                _scope.reset();
                _scope.start();
            }
        });

        
        $(".string_select_btn").click(function(e){
            _scope.stop();
            var a = $(e.currentTarget).attr("href").replace("#","")
            _scope.changeString(a);
            return false;
        });
        

        $(".interval_select_btn").click(function(e){
            _scope.stop();
            var a = $(e.currentTarget).text().replace("#","")
            this.half_steps_to_increment = a;
            _scope.changeInterval($(e.currentTarget).text());
            _scope.reset();
            return false;
        });

        $(".play_pause").click(function(e){
            var a = $(".play_pause").children(".glyphicon-pause").hasClass("not_shown");
            if(a){
                _scope.play();
            }else{
                _scope.pause();
            }
            return false;
        });
        
        $(".stop_btn").click(function(e){
            _scope.stop();
            return false;
        });
        

        $(".forward").click(function(e){
            return false;
        });

        
        $(".back_btn").click(function(e){
            return false;
        });



  
	}

    




}










