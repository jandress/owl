class IntervalActivity extends Guitar{

    _scope = this;
	timer_interval_length = 250; // rename this
    tick_counter = 0;
    note_index = 0
    starting_note_index = 0
    intervalHandle = null;
    sequence = []
    string = 0;
    play_index = 0;
    half_steps_to_increment = 1;

	constructor() {
		super();
        this.reset();    
	}

    start(){
        self = this;
        clearInterval(this.intervalHandle); 
        this.intervalHandle = setInterval(function() { self.tick() }, 1);   
        this.tick_counter = 0;
        this.showNote();
    }
    stop(){
        clearInterval(this.intervalHandle);
        this.reset();    
    }
    pause(){
        clearInterval(this.intervalHandle); 
        this.play_index = parseFloat(this.half_steps_to_increment)+this.play_index
    }

    reset(){
        this.tick_counter = 0;
        this.note_index = 0; // what did note_index do again?
        this.play_index = 0;
        this.starting_note_index = 0
        guitar.hideAll();
        this.assembleSequence();
    }

    setString(v){
        this.string = v;
        this.assembleSequence();
    }
    setInterval(v){
        this.half_steps_to_increment = v;
        this.assembleSequence();
    }

    assembleSequence(){
        //~ rewrite this garbage
        var ar = [];
        this.note_index = 0;
        for(var i = 1 ; this.note_index < 21; i++){
            this.note_index += parseFloat(this.half_steps_to_increment)
            ar.push("fret_"+this.string+"_"+this.starting_note_index);
            var a = "fret_"+this.string+"_"+(parseFloat(this.starting_note_index)+this.note_index);
            ar.push(a);
        }
        this.sequence = ar;
    }

    showNote(){
        if(this.play_index >=this.sequence.length){
            // console.log("clear interval")
            clearInterval(this.intervalHandle);
            return;
        }
        console.log(this.play_index);
        var b = this.sequence[this.play_index];
        //	showFretById(fretId,playSound=false, highlight = false, fadeOut = false, fadeTime = 1){
        guitar.showFretById(b, true,true,true)   
    }


    tick(){
        $("#caption_holder").html(this.tick_counter);
        if(this.tick_counter >= this.timer_interval_length){
            this.play_index++;
            if(this.play_index >= this.sequence.length){
                this.tick_counter = 0;
                this.play_index = 0; 
                clearInterval(this.intervalHandle);
            }   
            this.showNote();            
            this.tick_counter = 0
        }
        this.tick_counter++;
    }
 
    foo(){
        this.reset()
        this.stop();
    }
}

var g = new IntervalActivity();


























$("#foo").click(function(e){
    g.foo();
    return false;
});

$(".string_select_btn").click(function(e){
    g.stop();
    var a = $(e.currentTarget).attr("href").replace("#","")
    g.string = a;
    g.reset();
    $(".string_select_label").text($(e.currentTarget).text())
    $("#string_select").removeClass("open");
    return false;
});

$(".interval_select_btn").click(function(e){
    g.stop();
    var a = $(e.currentTarget).text().replace("#","")
    g.half_steps_to_increment = a;
    g.reset();
    $(".interval_select_label").text($(e.currentTarget).text())    
    $("#interval_select").removeClass("open");
    return false;
});

$(".play_pause").click(function(e){
    var a = $(e.currentTarget).children(".glyphicon-pause").hasClass("not_shown");
    if(a){
        $(e.currentTarget).children(".glyphicon-pause").removeClass("not_shown")
        $(e.currentTarget).children(".glyphicon-play").addClass("not_shown")
        g.start();
    }else{
        $(e.currentTarget).children(".glyphicon-play").removeClass("not_shown")
        $(e.currentTarget).children(".glyphicon-pause").addClass("not_shown")
        g.pause();
    }
    return false;
});

$(".stop").click(function(e){
    g.stop();
    g.reset();
    return false;
});

$(".forward").click(function(e){
    return false;
});

$(".back_btn").click(function(e){
    return false;
});

$( function() {
    var handle = $( "#custom-handle" );
    $( "#slider" ).slider({ min: 5,
            max: 500,
            value:250,
            step:10,
            create: function() {
                handle.text( $( this ).slider( "value" ) );
            },
            start:function( event, ui ) {
                g.stop();
            },
            slide: function( event, ui ) {
                handle.text( ui.value );
            },
            change: function( event, ui ) {
                handle.text( ui.value );
                g.timer_interval_length = ui.value;
                g.reset();
                g.start();
            }
        });
  } );


  function startDemo(){
    var self = this;
    this.intervalHandle = setTimeout(function() { 
        g.setString(2);
        g.setInterval(4);
        self.g.start();
        
    }, 500);   
  }

  startDemo()