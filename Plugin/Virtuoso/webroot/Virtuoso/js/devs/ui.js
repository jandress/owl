

function capitalize(string){
    var z = string[0].toUpperCase() + string.substring(1).toLowerCase();
    return z
}

$(".key_link").click(function(e){
    guitar.setKey($(e.currentTarget).text().toUpperCase());
    return false;
});

$(".scale_link").click(function(e){
    guitar.setScale($   (e.currentTarget).text().toUpperCase(),$(e.currentTarget).attr("href").replace("#",'').split(","));
    return false;
});

$(".caged_link").click(function(e){
  guitar.showCaged($(e.currentTarget).html());
  return false;
});

$(".visualize_link").click(function(e){
    try {
        var b = $(e.currentTarget).attr("href").replace("#","");
        guitar.visualizeSelectNotes(b);
    } catch (error) {
      console.error(error);
    }
    return false;
});

$(".neck_btn").click(function(e){
    try {
        guitar.showCurrentScaleCagedPortionAtFret($(e.currentTarget).text())
    } catch (error) {
      console.error(error);
    }
    return false;
})

$("#compare").click(function(e){
    openDrawer();
    enableDrillControls(false)
    return false;
})

$("#play_pause_btn").click(function(e){
    try {
        // guitar.showCurrentScaleCagedPortionAtFret($(e.currentTarget).text())
        if(
            $("#pause_icon").hasClass("hidden")){
                guitar.play()

                $("#pause_icon").removeClass("hidden")
                $("#play_icon").addClass("hidden")
        }else{
                guitar.stop()
                $("#pause_icon").addClass("hidden")
                $("#play_icon").removeClass("hidden")
        }
    } catch (error) {
      console.error(error);
    }
    return false;
})

$("#prev_btn").click(function(e){
    try {
        // guitar.showCurrentScaleCagedPortionAtFret($(e.currentTarget).text())
         guitar.prev();
    } catch (error) {
      console.error(error);
    }
    return false;
})

$("#next_btn").click(function(e){
    try {
        // guitar.showCurrentScaleCagedPortionAtFret($(e.currentTarget).text())
        guitar.next();
    } catch (error) {
      console.error(error);
    }
    return false;
})

$(".interval_link").click(function(e){
    guitar.interval = $(e.currentTarget).attr("href").replace("#","")
    return false;
})





$(".mode_link").click(function(e){
    try {
        foo();
        alert(1);
    } catch (error) {
      console.error(error);
    }
    return false;
});

$(".drills_link").click(function(e){
    //openDrawer()
    var b = e.currentTarget;
    var ar = []
    //  $(b).text() = 1a-- 1234, 2345, 3456, etc.
    var h = $(b).text().replace($(b).attr("href").replace("#","")+"--","").replace(", etc.","").replaceAll(" ","").split(",")

    for (var i = 0; i < h.length; i++) {
        ar.push(h[i].replace(" ","").replace("\n","").split(""))
    }
    //console.table(ar)
    // ar is a multi dimensional array of indexes pulled from the ui href.    1234, 2345, 3456 etc.    
    // c is the string pulled from the ui href (actually html text value)
     var c = $(b).text().replace(", etc.","").replace(/\s/g, '');
    // console.log(c)
     guitar.runDrill(ar, c);
    //runDrill(ar, c)
    updateCaption(guitar.key+" "+guitar.scale['name'],c)
    return false;
});

$("#sliderW").on("change", function(e) {
    onSliderChange(e)
     return false;
});
