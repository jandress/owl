class PracticeSession  {

    // base = "/my/psa/view/";
    base= "/my/psa/session/";
    
    constructor(_base) {
        var scope = this;
        scope.base = _base
        $("window").ready(function(e){
            scope.printTimerGraphic()
            // scope.ping()
            
        })
        $("#get_started_button").click(function(e){
            return false
        });

        $(".activity_link").click(function(e){
            console.log(scope.base);
            //scope.ping(scope.base2+"w/"+$(e.currentTarget).attr("href").replace(scope.base,""))
            // window.open("/my/psa/view/"+$(e.currentTarget).attr("href").replace(scope.base,""));
            return false;
        });
    }

    printTimerGraphic(txt=''){
        $("body").append('<div id = "timer_box" style="color:#ececec; background-color:#333333; position:fixed; top:180px; width:200px; padding:15px; right:20px; z-index:1000">'+txt+'</div>');
    }

    startPracticeSession(){
        var scope = this;
        function go(){
            scope.ping(scope.base);
        }
        setInterval(go, 10000)
        scope.ping(scope.base);
    }

    ping(_url){
        $("#timer_box").text("practice_session.js inititated "+_url)
        var scope = this;
                 $.ajax({
				url: _url,
				success: function(data){
                    console.log(data)
				}
			});
    }



}


var psession = new PracticeSession(ping);
psession.startPracticeSession();














/*

    You can enter a practiceSession at any point, but hitting "get started"
    Establish completion of an activity by either completion of the activity or periodic pings to the server to check against time limit.  



    when you click on one of the activity links in the table at /my/practicesessions/view/{ID}   
        it starts or resumes the activity.




    page load, you do an ajax call to see if 


    // get started changes into "CONTINUE."
    click get started. 
        you check 












    array(
        'PracticeSessionActivity' => array(
            'id' => '2',
            'user_id' => '1',
            'activity_id' => '9',
            'completed' => '0',
            'description' => 'interval child test',
            'timestamp' => '2023-10-28 02:30:07.326090',
            'session_id' => '1'
        ),
        'Activity' => array(
            'id' => '9',
            'name' => 'interval child test',
            'data' => '{
                "application": "interval",			
                "command": "#octave_to_octave/1s",
                "playerParameters": {
                        "autoPlay":true,
                        "handHoldingMode":false,
                        "key":"e",
                        "scale":"major"
                 },
                "ui_instructions":{
                    "messages" : [ ]
                }
            }',
            'type' => 'caged',
            'parent_id' => '3',
            'lft' => '37',
            'rght' => '38',
            'taxonomy_only' => false
        )
    )











*/