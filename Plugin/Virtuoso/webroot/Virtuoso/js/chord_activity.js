class ChordActivity extends Guitar{

    _scope = this;
	timer_interval_length = 100; // rename this
    tick_counter = 0;
    note_index = 0
    starting_note_index = 0
    intervalHandle = null;
    sequence = []
    string = 0;
    play_index = 0;
    half_steps_to_increment = 1;
    replay = false;
    replay_interval = 1500


	constructor() {
		super();
        this.reset();    
	}

    start(replay){
        this.replay = replay;
        this.play_index = 0;
        self = this;
        this.hideAll();
        clearInterval(this.intervalHandle); 
        this.reset()
        this.intervalHandle = setInterval(function() { self.tick() }, 1);   
        this.showNote();
    }
    stop(){
        clearInterval(this.intervalHandle);
        this.reset();    
    }
    reset(){
        this.tick_counter = 0;
        this.note_index = 0; // what did note_index do again?
        this.play_index = 0;
        this.starting_note_index = 0
        guitar.hideAll();
    }

    showNote(){
        if(this.play_index >=this.sequence.length){
            clearInterval(this.intervalHandle);
            return;
        }
        var b = this.sequence[this.play_index];
        //	showFretById(fretId,playSound=false, highlight = false, fadeOut = false, fadeTime = 1){
        guitar.showFretById(b, true,true,true);
    }

    playChord(a, replay){
        var ar = [ ]
        for(var i = 0; i < a.length; i++){
            if(a[i]!="x"){                
                 ar.push("fret_"+i+"_"+a[i])
            }
        } 
        this.sequence = ar;
        this.start(replay);
    }

    onComplete(){
        if(this.replay){
            this.timer_interval_length = 10;
            this.start(true);
        }
        this.replay = false;
    }

    tick(){
        var scope = this;
        $("#caption_holder").html(this.tick_counter);
        if(this.tick_counter >= this.timer_interval_length){
            this.play_index++;
            if(this.play_index >= this.sequence.length){
                this.tick_counter = 0;
                this.play_index = 0; 
                clearInterval(this.intervalHandle);
                this.intervalHandle = setTimeout(function() { 
                    scope.onComplete()
                    scope.tick_counter = 0;
                    scope.note_index = 0;    // what did note_index do again?
                    scope.play_index = 0;
                    scope.starting_note_index = 0
                 }, scope.replay_interval);   
                return;
            }   
            this.showNote();            
            this.tick_counter = 0
        }
        this.tick_counter++;
    }
}




var g = new ChordActivity();


function audioReady(){

    var do_replay = false

    $("#fastReplay").change(function(e){
            do_replay = ($('#fastReplay').is(":checked"))
    })

    $(".chord_icon_link a").click(function(e){
        console.log("when you feel like fixing the flat symbol issue. here it is. chordsJS[i].name==u.  seems like maybe the thing to do is add to the chord object a key string")
        try {
            var t = $(e.currentTarget).attr("href");
            var u = t.substring(1, t.length);
            for(var i = 0; i < chordsJS.length; i++){
                // console.log(chordsJS[i].name,u)
                if(chordsJS[i].key==u){
                    g.reset();
                    g.timer_interval_length = $( "#slider" ).slider( "values", 0 );
                    g.playChord(chordsJS[i].structure, do_replay)
                }
            }
        } catch (error) {
            console.log(error)
        }
        return false
    })

    var handle = $( "#custom-handle" );
    $( "#slider" ).slider({
            min: 1,
            max: 200,
            value:100,
            step:10,
            create: function() {
                handle.text( $( this ).slider( "value" ) );
            },
            start:function( event, ui ) {
                g.stop();
            },
            slide: function( event, ui ) {
                handle.text( ui.value );
            },
            change: function( event, ui ) {
                handle.text( ui.value );
                g.timer_interval_length = ui.value;
                g.reset();
                g.start(do_replay);
            }
        });
  };





