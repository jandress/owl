class SoundPlayer extends Phaser.Scene{
    preload (){
		var loaded = 0;
		var scope = this;
		var numStrings = 6;
		var audio_set = "clean";
        for(var i = 1 ; i <= numStrings; i ++){
           let songLoader =  this.load.audioSprite(i.toString(), '/Virtuoso/Virtuoso/audio/'+audio_set+"/"+(i)+'.json', [  '/Virtuoso/Virtuoso/audio/'+audio_set+"/"+(i)+'.wav' ]);
				//    songLoader.on('filecomplete', () => function(){
					// alert(1)
				//    })
				this.load.on('complete', function(){
					loaded++;
					if(loaded == numStrings){
						try {
							audioReady();
						  } catch (error) {
							console.log(error);
							// Expected output: ReferenceError: nonExistentFunction is not defined
							// (Note: the exact output may be browser-dependent)
						  }
					}
				})
        }
    }
    create (){}
}
const game = new Phaser.Game(
	{
		type: Phaser.AUTO,
		parent: 'phaser-soundplayer',
		width: 1,
		height: 1,
		scene: SoundPlayer,
	}
);



class Guitar extends EventsManager {

	fullNeckNoteArray = [];
	flat_position_array =[];
	//--
	key = "c"; // this should not be here
	//string_tunings = [ "e","a","d","g","b","e" ];
	notes_in_chromatic_scale = [ "e","f","f#","g","g#","a","a#","b","c","c#","d","d#" ];
	caged; // only used once to get the scale by name
	//--
	strings_and_frets = [];
	eventListeners = [];
	colors = [ "red", "red_orange", "orange", "yellow_orange", "yellow", "yellow_green", "green", "blue-green", "blue", "purple", "violet", "magenta" ]		

	constructor(tuning) {
		super();
		
		this.scale = caged.getScaleByName("Major"); // contains no keyinfo
		// console.log("this.scale: ",this.scale)

		this.string_tunings = tuning || guitar_model.string_tunings;
		var string_tunings = this.string_tunings;
		var fullNeckNoteArray = this.fullNeckNoteArray;
		var notes_in_chromatic_scale = this.notes_in_chromatic_scale;
		var string_tunings = this.string_tunings;
		this.strings_and_frets = [];
		for (var i = this.string_tunings.length-1; i >= 0; i--) {
			fullNeckNoteArray[i]=[]
			var ar =[]
			for (var j = 0; j < 22; j++) {
				ar.push("fret_"+i+"_"+j)
				var h = i < string_tunings.length ? i : i - string_tunings.length;
				var index_of_root_note = notes_in_chromatic_scale.indexOf(string_tunings[i])
				var b = (index_of_root_note+j)%notes_in_chromatic_scale.length;
				var id = "#fret_"+h+"_"+j+" .note_text";
				this.fullNeckNoteArray[i].push(notes_in_chromatic_scale[b]);
				$(id).html( notes_in_chromatic_scale[b] );			
				this.flat_position_array.push(id)
			}
			this.strings_and_frets.push(ar);
		}

	}

	hideAll(){
		$(".note").addClass( "not_shown" );
	}

	showAllNotes(){
		//simply shows the notes of the entire neck.
		// var notes = this.notes_in_chromatic_scale;
		console.log("showAll()")
		$(".note").removeClass( "not_shown" );
	}
	
	showFretById(fretId, playSound=false, highlight = false, fadeOut = false, fadeTime = 1){
		// alert(highlight)
		// console.log(fretId)
		// console.log("showFretById:", fretId, playSound, highlight , fadeOut ,fadeTime)
		fretId = fretId.replace("#","");
		$("#"+fretId).removeClass('not_shown')
		// $("#"+fretId).removeClass("reduced")
		if(playSound){
			var a = this.getStringAndFretFromDivId(fretId)
			this.playSoundForNote(parseFloat(a.string)+1,a.fret)
		}
		if(highlight){
			// note_text 
			var sid = "#"+fretId+" .note_highlight ";
			var g = $(sid);
			if(g[0]){
				g[0].onanimationend=function(e){
					$(e.currentTarget).removeClass('red fadeOut')
				}
			}
			if($("#"+fretId+" .note_highlight ").hasClass('red fadeOut')){
				$("#"+fretId+" .note_highlight ").removeClass('red fadeOut')	
			}
			 $("#"+fretId+" .note_highlight ").addClass('red fadeOut')
			//
		}
		if(fadeOut){
			
		}
		// var intr = setInterval(function(e){
		// 	$("#"+fretId+" .note_highlight ").removeClass('red fadeOut')
		// }, fadeTime*4000);

	}

	getStringAndFretFromDivId(str){
		var a = str.replace("#","").replace("fret_","").split("_");
		return {
			string:a[0].toString(),
			fret:a[1].toString()
		};
	}

	playSoundForNote(string,fret){
		// console.log("playSoundForNote(): ",string,fret)
	//	console.log("NOTE!   PHASER_GAME.sound.playAudioSprite() string is 1-indexed, frets are zero indexed!  FIX ASAP!")
		// console.log(string.toString(),fret.toString());
		PHASER_GAME.sound.playAudioSprite((string).toString(), (fret).toString());
	}

	showNotesInArray(ar, hideOthers){
		if(hideOthers){
			 $(".note").addClass("not_shown");
		}
		for (var i = 0; i < ar.length; i++) {
			//console.log(ar[i]);
			 $(ar[i]).removeClass("not_shown");
		}
	}	
	
	getNoteAtIndex(string,fret){
		return this.fullNeckNoteArray[string][fret];
	}

	getIndexForNote(note){
		// returns the first occurance of the note in the first string. 		
		// var i = this.fullNeckNoteArray; 
		return this.fullNeckNoteArray[0].indexOf(note);
	}

	formatNoteForColoration(note, color){
		var ar = []
		ar['note']=note;
		ar['color']=color;
		return ar;
	}

	highlightNote(note,color){
		// note is actually a note string eg. "c", "d#", "a" etc // color is a css class.
		// applies all over the neck.
		//console.log("highlightNotes",note,color)
		var ar = [
			this.formatNoteForColoration(note,color)
		]
		this.highlightNoteArray(ar)
	}

	highlightNotesSameColor(noteArray,color){
		// note is actually a note string eg. "c", "d#", "a" etc // color is a css class.
		// applies all over the neck.
		//  console.log("highlightNotes",note,color)
		var ar = [
			this.formatNoteForColoration(note,color)
		]
		this.highlightNoteArray(ar)
	}

	removeColors(obj){
		for (var i = 0; i < this.colors.length; i++) {
			$(obj).removeClass(this.colors[i]);
		}
	}

	highlightNoteArray(ar){
			// same as highlight note, but can do a batch.
			//  console.log("highlightNotes",ar)
			//ar is an array of:
			//ar = [[ 'note'="c", "color"]]
		for (var i = 0; i < this.fullNeckNoteArray.length; i++) {
			for (var j = 0; j < this.fullNeckNoteArray[i].length; j++) {
				// console.log(this.fullNeckNoteArray[i][j])
				var id = "#fret_"+i+"_"+j;
				for (var b = 0; b < ar.length; b++) {
					if(this.fullNeckNoteArray[i][j]== ar[b]['note']){
						this.removeColors(id);
						$(id).addClass(ar[b]['color'])
					}
				}
			}
		}
 	}
	visualizeSelectNotes(str){
		console.log('visualizeSelectNotes: ',str)
		//this isn't really low level code, which is what I would prefer to keep in this class.   
		//the presence of "key" in this file breaks encapsulation. 
		try {
		   // make notes in the currently "loaded'set  visible
			this.showCurrentNotes();
		   //then set highlight color based on selection string.
		   switch(str){
			   case("tonics"):
				   var i = this.notes_in_chromatic_scale.indexOf(this.key)
				   this.highlightNote(this.key,this.colors[i])

			   break;
			   case("remove_highlights"):
					$(this.colors).each(function(i){
						var p = this;	
						$(".note").removeClass(this);
					})

			   break;
			   case("all"):
				   this.showAllNotes();
			   break;
			   case("clear"):
				   for (var i = 0; i < this.colors.length; i++) {
					   $("."+this.colors[i]).removeClass(this.colors[i])
				   }
				   $(".note").addClass("not_shown")
			   break;
			   case("degree_1"):
				   var c = this.getNotePositioninCurrentScale(this.key,1);  // 7
				   var p = guitar_model.neckData['scale_notes_flat'][c];// e
				   var i = this.notes_in_chromatic_scale.indexOf(p)
				   // console.log(c,p)
				   this.highlightNote(p,this.colors[i])
			   break;


			   case("degree_2"):
				   var c = this.getNotePositioninCurrentScale(this.key,2);  // 7
				   var p = guitar_model.neckData['scale_notes_flat'][c];// e
				   var i = this.notes_in_chromatic_scale.indexOf(p)
				   // console.log(c,p)
				   this.highlightNote(p,this.colors[i])
			   break;

			   case("degree_3"):
					var c = this.getNotePositioninCurrentScale(this.key,3);  // 7
					var p = guitar_model.neckData['scale_notes_flat'][c];// e
					var i = this.notes_in_chromatic_scale.indexOf(p)
					// console.log(c,p)
					this.highlightNote(p,this.colors[i])
		   		break;

			   case("degree_4"):
				   var c = this.getNotePositioninCurrentScale(this.key,4);  // 7
				   var p = guitar_model.neckData['scale_notes_flat'][c];// e
				   var i = this.notes_in_chromatic_scale.indexOf(p)
				   // console.log(c,p)
				   this.highlightNote(p,this.colors[i])
			   break;


			   case("degree_5"):
					var c = this.getNotePositioninCurrentScale(this.key,5);  // 7
					var p = guitar_model.neckData['scale_notes_flat'][c];// e
					var i = this.notes_in_chromatic_scale.indexOf(p)
					// console.log(c,p)
					this.highlightNote(p,this.colors[i])
		   		break;

				case("degree_6"):
				   var c = this.getNotePositioninCurrentScale(this.key,6);  // 7
				   var p = guitar_model.neckData['scale_notes_flat'][c];// e
				   var i = this.notes_in_chromatic_scale.indexOf(p)
				   // console.log(c,p)
				   this.highlightNote(p,this.colors[i])
			   break;


			   case("degree_7"):
					var c = this.getNotePositioninCurrentScale(this.key,7);  // 7
					var p = guitar_model.neckData['scale_notes_flat'][c];// e
					var i = this.notes_in_chromatic_scale.indexOf(p)
					// console.log(c,p)
					this.highlightNote(p,this.colors[i])
		   		break;







			//    case("fifths"):
			// 	   var c = this.getNotePositioninCurrentScale(this.key,4)
			// 	   var p = guitar_model.neckData['scale_notes_flat'][c]
			// 	   var i = this.notes_in_chromatic_scale.indexOf(p)
			// 	   this.highlightNote(p,this.colors[i])
			//    break;
		   }

	   } catch (error) {
		 console.error(error);
		 return false;
	   }
	   return false;
    }
	getDataForScaleFullNeck(key,scl){
		return guitar_model.getDataForScaleFullNeck(key,scl);
	}
}
		
/*


notes_in_chromatic_scale
{
	neck_note_matrix
	scale_notes_flat
	scale_notes_matrix
	scale_positions_flat
	scale_positions_matrix
}



{
	structure:[
	[0,1,3],
	[0,2,3],
	[0,2,3],
	[0,2],
	[0,1,3],
	[0,1,3]
	],
	tonics:[
		{
			string:1,
			fret:2
		},
		{
			string:4,
			fretIndex:1
		},
	]
},




*/
