


const canvas = document.getElementById("canvas");
canvas.height=200;
const ctx = canvas.getContext("2d");




function drawFretboard(fretStructure){


    var _w = 75;
    var _h = 90;
    var fret_radius = 5
    
    var _inc = _w/5;
    
    var num_frets = 4
    var _vinc = _w/num_frets;
    

    function drawMarker(string,fret){
        var radius = fret_radius
        var i_x = _inc;
        var i_y = _vinc
        var x = i_x * string+fret_radius
        var y = (i_y * fret )-_vinc/2+fret_radius
        ctx.moveTo( x, y );        
        ctx.beginPath();
        ctx.fillstyle = "black";
        ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
        ctx.closePath();
        ctx.fill();
    
    }

    ctx.beginPath();
    ctx.strokeStyle = "black";
    ctx.rect(fret_radius, fret_radius, _w, _h);

    for(var i = 1; i <5; i++ ){
        var g = _inc*(i)+fret_radius
        ctx.moveTo( g, fret_radius );
        ctx.lineTo( g, _h+fret_radius);
    }

    for(var i = 1; i <=num_frets; i++ ){
        var g = _vinc * (i)
        ctx.moveTo( fret_radius, g+fret_radius );
        ctx.lineTo( _w+fret_radius, g+fret_radius);
    }



    ctx.stroke();
    ctx.closePath();


    
    for(var i = 1; i <fretStructure.length; i++ ){
        if(fretStructure[i].toString().toLowerCase() == "x"){
            continue;
        }
        drawMarker(i,fretStructure[i] )
    }

}

drawFretboard(["x","x",0,2,3,2])











function audioReady(){}



