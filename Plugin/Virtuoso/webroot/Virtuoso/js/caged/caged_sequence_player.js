class CagedSequencePlayer extends EventsManager {



    constructor() {
        super();

        this.WALK_THEN_LOOP = "1"
        this.WALK_THEN_REVERSE = "2"
        this.WALK_THEN_STOP = "3"
        // this.load(multiDimensionalArray);
        this.player_mode = 1

        this.reset()
    }

    reset(){
        //--
        this.intervalDurationMs = 400;
        // this.startingTopIndex = 40
        this.currentArrayIndex = 0;
        this.currentElementIndex = 0;
        this.progressingTopIndex = this.startingTopIndex;
        this.progressingBottomIndex = 0;
        this.isPlaying = false;
        //--
        this.intervalId = null;
        this.isRepeating = true
        this.isReversed = false;
        this.announceEvent("reset",this);
    }





    load(val){
      this.stop()
      // var isMda = this.isMultiDimensionalArray(val);
      if(!val){

        var b = 0
      }

      this.multiDimensionalArray = val;

      this.startingTopIndex = this.multiDimensionalArray[0].length
      this.progressingTopIndex = this.multiDimensionalArray[0].length
      this.reset();
    }
  
    setMode(val){
      // this.pau();
      this.player_mode = val;
    }

    play() {
      if (!this.isPlaying) {
        this.isPlaying = true;
        this.intervalId = setInterval(() => {
            this.next();
        }, this.intervalDurationMs); // Adjust the interval as needed (1000 milliseconds = 1 second)
      }
      this.announceEvent("played",this);
    }
  
    pause() {
      if (this.isPlaying) {
        clearInterval(this.intervalId);
        this.isPlaying = false;
      }
      this.announceEvent("paused",this);
    }
  
    stop() {
     // guitar.hideAll();
      this.pause();
      this.currentArrayIndex = 0;
      this.currentElementIndex = 0;
      this.announceEvent("stopped",this);
      this.reset();
    }
    
    next() {
      this.printCurrentElement(); 
          // this.WALK_THEN_STOP = 1
          // this.WALK_THEN_REVERSE = 2
          // this.WALK_THEN_LOOP = 3  
          // console.log("mode: ",this.player_mode)
      var mode = this.player_mode.toString()
      switch(mode){
        case(this.WALK_THEN_REVERSE):
        case(this.WALK_THEN_STOP):
         // console.log("WALK_THEN_STOP || WALK_THEN_REVERSE")
          this.walkThenReverse();
        break;        
        case(this.WALK_THEN_LOOP):
        default:
         // console.log("DEFAULT || WALK_THEN_LOOP")
          this.basicRunThroughArrays();  
        break;
      }
    }
    previous() {
      this.isReversed = !this.isReversed;
      this.next();
      this.isReversed = !this.isReversed;
    }
  
    walkThenReverse(reverse){
      // console.log(this.progressingTopIndex);
        //direction of increment
      if (this.isReversed) {
        this.currentElementIndex--;
        if(this.currentElementIndex < 0){
          this.isReversed = false;
          this.currentElementIndex = 0
        }
      } else {
        this.currentElementIndex++;
      }
      // is index exceeds the parent array length or in case it is less than zero, or the index = the topPregressingindex. 
      if (
        this.currentElementIndex >= this.multiDimensionalArray[this.currentArrayIndex].length 
        || this.currentElementIndex < 0
        // || this.currentElementIndex === this.progressingTopIndex
        ) {
          if(this.player_mode.toString() == this.WALK_THEN_REVERSE.toString()){
            this.isReversed = true;
            this.currentElementIndex--
            this.announceEvent("reversed",this);
          }else{
            this.stop();
            if(this.player_mode.toString() == this.WALK_THEN_LOOP.toString()){
              this.play();
            }
          }
      }   
    }

    basicRunThroughArrays(){
        //direction of increment

      if (this.isReversed) {
        this.currentElementIndex--;
        if(this.currentElementIndex<0){
          //console.log("currentElementIndex: ",this.currentElementIndex, "this.currentArrayIndex: ",this.currentArrayIndex)        
          this.previousArray() 
          //go to end of previous array
          this.currentElementIndex = this.multiDimensionalArray[this.currentArrayIndex].length-1
        }
      } else {
        this.currentElementIndex++;
      }
      if (this.currentElementIndex >= this.multiDimensionalArray[this.currentArrayIndex].length || this.currentElementIndex < 0) {
        this.currentElementIndex = 0;
        this.announceEvent("sequence_complete",this)        
        this.announceEvent("progressing_top_index_exceeded",this)        
        this.nextArray();
        //   this.stop();
      }

      //  console.log(this.currentElementIndex , this.progressingTopIndex)
      if (this.currentElementIndex >= this.progressingTopIndex) {
        this.progressingBottomIndex = 0;
        this.currentElementIndex = this.progressingBottomIndex
        this.announceEvent("progressing_top_index_exceeded",this)        
        // alert(1111)
        //this.progressingBottomIndex++;
        // if (this.progressingBottomIndex >= this.multiDimensionalArray.length) {
        //   this.progressingBottomIndex = 0;
        //   this.progressingTopIndex++;
        // }
      }    
    }

    nextArray() {
        /// log these values out and see what they do.
      this.currentArrayIndex = (this.currentArrayIndex + 1) % this.multiDimensionalArray.length;
      this.currentElementIndex = 0;
    }
  
    previousArray() {
        /// log these values out and see what they do.
      this.currentArrayIndex = (this.currentArrayIndex - 1 + this.multiDimensionalArray.length) % this.multiDimensionalArray.length;
      this.currentElementIndex = 0;
    }

    printCurrentElement() {
        ///  console.log("printCurrentElement() currentElementIndex: ",this.currentElementIndex, "this.currentArrayIndex: ",this.currentArrayIndex)        
        var b = this.multiDimensionalArray;
        // console.log(this.multiDimensionalArray)
      
        if(!Array.isArray(b)){
            throw new Error("printCurrentElement() is looking for a multidimensional array")
        }
        if (Array.isArray(b)) {
          if(
            !( b.some(item => Array.isArray(item)))
          ){
            throw new Error("printCurrentElement() is looking for a multidimensional array")
          }
        }
        var fretId = this.multiDimensionalArray[this.currentArrayIndex][this.currentElementIndex];
        guitar.showFretById(fretId, true, true, true)
    }

    
  }
