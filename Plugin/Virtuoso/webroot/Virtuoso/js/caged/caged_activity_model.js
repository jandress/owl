class CagedActivityModel {

	
	static getSequence(array){
		/*

			[
				{
					position:1,
					scale:"major",
					key:"e"
					direction:1
				},
				{
					position:2,
					scale:"major",
					key:"e"
					direction:-1
				},		
			]
			}
		*/
		// var b = caged.getCagedPositionArrayByNumberScaleNameAndKey(1,"major", "e");
		//var c = caged.getCagedPositionArrayByNumberScaleNameAndKey(2,"major", "e");
		var ar =[]
		for (let i = 0; i < array.length; i++) {
			const element = array[i];
			var b = caged.getCagedPositionArrayByNumberScaleNameAndKey(element.position,element.scale, element.key);
			
			if(element.direction < 1){   b.reverse();   }
			ar.push( b )
		}
		return ar;
	}
	


	//goes through one caged position and then down the next one.   starts with "position 1" which might not be the first position on the neck
	static createNeckCrawl(scale, key){
		
		// var a;	
		// for(var i = 0 ; i <= 6; i++){
		// 	var y = i%2
		// 	if(y){
		// 		a = caged.getCagedPositionArrayByNumberScaleNameAndKey(2, key, scale).reverse();			
		// 	} else {
		// 		a = caged.getCagedPositionArrayByNumberScaleNameAndKey(3, key, scale)
		// 	}
		// 	if(a.length){
		// 		ar.push(a);
		// 	}
		// }
		var ar = [caged.getCagedPositionArrayByNumberScaleNameAndKey(2, "e", "pentatonic major")] 			
		return ar;
	}
	

	static assembleDrillArray(sequence_array,position,scale,key){
		// getCagedPositionArrayByNumberScaleNameAndKey(num, scale, key)
		var a = caged.getCagedPositionArrayByNumberScaleNameAndKey(position,scale,key)
		var b = drill_runner.assembleAndGetDrillAsFlatArray(
			a,	
			sequence_array
		)
		return  [b];
	}


	static getScaleAcrossNeckAtPosition(num,scale,key ) {
		return caged.getCagedPositionArrayByNumberScaleNameAndKey(num,scale,key)
	}


	static getScaleOctivetoOctiveOneString(foo, scale ) {
		var dataObj = guitar_model.setDataForScaleFullNeck(foo,scale)
		var notes_matrix = dataObj['scale_notes_matrix']
		var ar = []
		for (let fret = 0; fret < notes_matrix[0].length; fret++) {
			for (let string = 0; string < notes_matrix.length; string++) {
				var element = notes_matrix[string][fret];
				var g = dataObj['scale_positions_matrix'][string][fret]
				if(element == foo ){
					ar = []
					for (let index = fret; index < notes_matrix[string].length; index++) {
						element = notes_matrix[string][index];
						ar.push(dataObj['scale_positions_matrix'][string][index])
						if(index > fret && element== foo){
							return ar		
						}						
					}
					return ar
				}
			}
		}
		return ar
		// return `${foo} not found in the matrix`;
	  }
	  


	  static getNeightboringScales() {}
	  static getScaleOctivetoOctiveAtPosition(foo, scale ) {}
  
}



















	//   for (let j = 0; j < notes_matrix.length; j++) {
	// 	if (notes_matrix[j][i] === foo) {
	// 	  return `Found ${foo} at position [${j}][${i}]`;
	// 	}
	//   }


	//   static getScaleOnString searchMatrix(matrix, foo) {
	// 	for (let i = 0; i < matrix.length; i++) {
	// 	  for (let j = 0; j < matrix[0].length; j++) {
	// 		if (matrix[i][j] === foo) {
	// 		  return `Found ${foo} at position [${i}][${j}]`;
	// 		}
	// 	  }
	// 	}
	// 	return `${foo} not found in the matrix`;
	//   }
	  
	/* 
	this algorithm does what it says, but not what its supposed to.    
	this algorithm checks down the strings, not accross the fret.
	*/
	// static getScaleOnString(key,scale){
	// 	var strings = guitar_model.setDataForScaleFullNeck(key,scale)
	// 	var i = 0;
	// 	var ar;
	// 	for (let index = 0; index < strings['scale_notes_matrix'].length; index++) {
	// 		var element = strings['scale_notes_matrix'][index];
	// 		ar = [];
	// 		var running = false;
	// 		for (let j = 0; j < strings['scale_notes_matrix'][index].length; j++) {
	// 			var e = strings['scale_positions_matrix'][index][j];
	// 			if(strings['scale_notes_matrix'][index][j] == key){
	// 				if(!running){
	// 					running = true;
	// 				}else{
	// 					//push the last note in the sequence.   
	// 					ar.push(e+" "+strings['scale_notes_matrix'][index][j])	
	// 					console.table(ar)
	// 					return ar;
	// 				}					
	// 			}
	// 			if(running){
	// 				ar.push(e+" "+strings['scale_notes_matrix'][index][j])	
	// 			}
				
	// 		}
	// 	}
	// 	console.table(ar)		
	// 	return ar;
	// }
