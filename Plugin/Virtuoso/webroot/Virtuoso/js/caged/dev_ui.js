class CagedActivityDevUi extends EventsManager {
	
	constructor() {
		super();
		this.init()
	}
	
	// key, scale, drill, position, mode
	setKey(val, announce_event = true){
		if(announce_event){
			this.announceEvent("ui_event", { name:"key", value:val });	
		}
		$(".key_select_label").html(val)
	}

	setScale(val, announce_event = true){	
		if(announce_event){
			this.announceEvent("ui_event", { name:"scale", value:val });	
		}
		$(".scale_select_label").html(val)
	}

	setPosition(val, announce_event = true){
		if(announce_event){
			this.announceEvent("ui_event", { name:"position", value:val });	
		}
		$(".position_label").html(val+1)
	}

	setDrill(name_string,sequence_arrays, announce_event = true){
		if(announce_event){
			this.announceEvent("ui_event", { name:"drill", value:{  pattern:sequence_arrays, name:name_string }});	
		}
		$(".drills_label").html(name_string)
	}

	setMode(val, announce_event = true){
		if(announce_event){
			this.announceEvent("ui_event", { name:"mode", value:val });	
		}
		$(".player_mode_label").html(val)
		$(".caged_player_mode_link").each(function(e){
			if($($(".caged_player_mode_link")[e]).attr("href") == val){
				var text = $($(".caged_player_mode_link")[e]).text();
				$(".player_mode_label").text(text);
			}
			var b = 0
		})
	}
	isPlaying(){
		$(".glyphicon-pause").removeClass("not_shown")
		$(".glyphicon-play").addClass("not_shown")
	}
	isStopped(){
		$(".glyphicon-play").removeClass("not_shown")
		$(".glyphicon-pause").addClass("not_shown")
	}

	init(){
		var scope = this;
		// CagedActivityUi.setDrill("1234, 2345, 3456")
		//const player = new CagedActivity();  
		$(".play_pause").click(function(e){
			var a = $(e.currentTarget).children(".glyphicon-pause").hasClass("not_shown");
			if(a){
				scope.isPlaying();
				scope.announceEvent("ui_event",{name:"play"});        
			}else{
				scope.isStopped()
				scope.announceEvent("ui_event",{name:"pause"});        
			}
			return false;
		});










		$(".stop_btn").click(function(e){
			scope.announceEvent("ui_event",{name:"stop"});        
			scope.isStopped()
			return false;
		});

		$(".caged_player_mode_link").click(function(e){
			// CagedActivityUi.setMode($(e.currentTarget).attr("href").replace("#",""));
			// guitar.hideAll()
			scope.setMode(
				$(e.currentTarget).attr("href")
			);
				// scope.setMode(player.position);

			return false;
		});

		$(".forward").click(function(e){
			// player.next();
			scope.announceEvent("ui_event",{name:"forward"});  
			return false;
		});

		$(".back_btn").click(function(e){
			// player.previous();
			scope.announceEvent("ui_event",{name:"back"});  
			return false;
		});

		$(".step-forward").click(function(e){
			// player.position = ((player.position + 1) % 5)
			scope.announceEvent("ui_event",{name:"step-forward"});  
			// scope.setPosition(player.position);
			return false;
		});

		$(".step-backward").click(function(e){
			scope.announceEvent("ui_event",{name:"step-backward"});  
			// player.position = (player.position - 1 + 5) % 5;
			//scope.setPosition(player.position);
			return false;
		});		
		
		$(".repeat_btn").click(function(e){		
			scope.announceEvent("ui_event",{name:"repeat"});  
			// player.isRepeating = player.isRepeating ? false : true;
			return false;
		});
		$(".refresh_btn").click(function(e){	
			scope.announceEvent("ui_event",{name:"refresh"});  
			// player.isRepeating = player.isRepeating ? false : true;
			return false;
		});
		
		$(".key_link").click(function(e){
			scope.setKey($(e.currentTarget).text());
			return false;
		});

		$(".scale_link").click(function(e){
			scope.setScale($(e.currentTarget).text());
			return false;
		});

		$(".caged_link").click(function(e){
			scope.setPosition($(e.currentTarget).text()-1);
			return false;
		});		


		$(".drills_link2").click(function(e){
			var b = $(e.currentTarget).attr("href");
			scope.announceEvent("ui_event",{name:"drills_link2",value:b});  
		})


		$(".drills_link").click(function(e){
			var b = e.currentTarget;
			var ar = [ ];
			var h = $(b).text().replace($(b).attr("href").replace("#","")+"--","").replace(", etc.","").replaceAll(" ","").split(",")
			// break the pattern strings up into arrays, eg 1234 to 1,2,3,4
			for (var i = 0; i < h.length; i++) {
				ar.push(h[i].replace(" ","").replace("\n","").split(""))
			}
			var c = $(b).text().replace(", etc.","").replace(/\s/g, '');
			scope.setDrill(c,ar)
			return false;
		});

		$( "#slider" ).slider({
				min: 5,
				max: 500,
				value:400,
				step:10,
				create: function() {
					// the handle
					$( "#custom-handle" ).text( $( this ).slider( "value" ) );
				},
				start:function( event, ui ) {
					// player.stop();
				},
				slide: function( event, ui ) {
					$( "#custom-handle" ).text( ui.value );
				},
				change: function( event, ui ) {
					$( "#custom-handle" ).text( ui.value );
				//	player.intervalDurationMs = ui.value;
					scope.announceEvent("ui_event",{name:"slider",value:ui.value});  

					// player.reset();	
				//	player.play();
				}
		});









		

	
	}
}
