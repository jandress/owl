
class UiMessaging extends EventsManager {

	constructor() {
		super();		
		$("body").prepend("<div id = 'activity_messaging'></div>");
	}

	formatString(ar){
		var str = "";
		for (const key in ar) {
			str += "<"+key+">"+ar[key]+"</"+key+">";
		}
		return str
	}

	index = 0;


	/*
		init(ar) recieves an array from a json object.   See the bottom of the page for the current json object expectation.   
		maybe this should be abstracted into some model object.


	*/
	
	init(ar){
		var index = 0;
		function update(obj, scope){
			if(
					( !obj ) ||
					( !obj.text_elements )	
				){
				return;
			}
			
			try {
				var str = scope.formatString(obj.text_elements)
				// console.log(obj.timer);
				// set opacity to 0 and set text.
				$("#activity_messaging").css({"opacity":.1});
				$("#activity_messaging").html("<div class = 'caption'>"+str+"</div>");
				// fadeIn, pause then fade out, then 
				$("#activity_messaging").animate({"opacity":1})			
				.delay(obj.timer.duration*1000)
				.animate( {"opacity":0}, function() {
					// console.log(index +"  "+ar.length)
					if(index == ar.length){
						return;
					}			
					index++;					
					update(ar[index],scope)	
				});						
				} catch (error) {
				console.error(error);
				// Expected output: ReferenceError: nonExistentFunction is not defined
				// (Note: the exact output may be browser-dependent)
				}
		}
		var t = ar[index]
		update(t, this)	
		console.log("make this recursive, or at least iterative over the messages array.    ")
	}




	debug(){
		// console.log(
		// 	"key:", this.key,
		// 	", scale:", this.scale,
		// 	", drill:",this.drill, 
		// 	", position:", this.position
		// )
	}


}

var ui_messaging_interface = new UiMessaging();



// // var str = '<div class="caption"><h1>Hi There!</h1></div>';



/*


			{
				"application": "caged",			
				"command": "#octave_to_octave/1s",
				"playerParameters": {
							"autoPlay":true,
							"handHoldingMode":false,
							"key":"e",
							"scale":"pentatonic major"
				},
				"ui_instructions":{
					"messages" : [ 
						{
							"text_elements":{
								"h1":"Grab a guitar and play along!",
								"h2":"E Major"
							},
							"timer":{
								"duration":4,
								"delay":0,
								"fade":"out"
							}								
						},
						{
							"text_elements":{
								"h1":"This is message 2",
								"h2":"Which is the same as C# Minor"
							},
							"timer":{
								"duration":4,
								"delay":2				
							}								
						},
						{
							"text_elements":{
								"h4":"You Also need other ways of including other text.   maybe that is a diffenet function call, for different text boxes etc. ",
								"p":"Also a menu o r progress graphic showing where you are in your practice session.     Also a timer for each one, also  a progress bar for certain places. or just numbers, 1 - 6 etc that are color coded. "
							},
							"timer":{
								"duration":10,
								"delay":2				
							}								
						},
						{
							"text_elements":{
								"h3":"This is message 3",
								"h4":"It would be nice if you could implement different starting points etc.     a play button, start after text fadeout etc. "
							},
							"timer":{
								"duration":6,
								"delay":2				
							}								
						}
					]
				}
			}


*/