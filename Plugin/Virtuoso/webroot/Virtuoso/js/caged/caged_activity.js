class CagedActivity extends CagedSequencePlayer {

	constructor(a) {
		super(a);		
		this.key = "e"
		this.scale = "major"
		this.drill = {
			'name':"none"
		}
		this.position = 1
		this.mode = 1
	}


	update(){
		guitar.hideAll();
		// this.debug();
		var g = CagedActivityModel.assembleDrillArray(this.drill.pattern, this.position,this.scale,this.key)
		super.load(g)
	}

	debug(){
		console.log(
			"key:", this.key,
			", scale:", this.scale,
			", drill:",this.drill, 
			", position:", this.position
		)

	}
}

