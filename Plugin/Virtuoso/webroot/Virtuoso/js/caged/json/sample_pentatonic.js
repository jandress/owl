var p = {
	"application": "caged",			
	"command": "#octave_to_octave/1s",
	"playerParameters": {
				"autoPlay":true,
				"handHoldingMode":false,
				"key":"e",
				"scale":"pentatonic major"
	},
	"ui_instructions":{
		"messages" : [ 
			{
				"text_elements":{
					"h1":"Grab a guitar and play along!",
					"h2":"E Major"
				},
				"timer":{
					"duration":4,
					"delay":0,
					"fade":"out"
				}								
			},
			{
				"text_elements":{
					"h1":"This is message 2",
					"h2":"Which is the same as C# Minor"
				},
				"timer":{
					"duration":4,
					"delay":2				
				}								
			},



{
				"text_elements":{
					"h4":"You Also need other ways of including other text.   maybe that is a diffenet function call, for different text boxes etc. ",
					"p":"Also a menu o r progress graphic showing where you are in your practice session.     Also a timer for each one, also  a progress bar for certain places. or just numbers, 1 - 6 etc that are color coded. "
				},
				"timer":{
					"duration":10,
					"delay":2				
				}								
			},
				"text_elements":{
					"h3":"This is message 3",
					"h4":"It would be nice if you could implement different starting points etc.     a play button, start after text fadeout etc. "
				},
				"timer":{
					"duration":6,
					"delay":2				
				}								
			}
		]
	}
}