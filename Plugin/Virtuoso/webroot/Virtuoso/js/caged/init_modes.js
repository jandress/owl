var player;
var ar = [];
var mode_explorer = new ModeExplorer(activityJson);
var key = "c";
var scale = "pentatonic major";
var mode = "pentatonic major";
var position = 5;
var secondOctave = false;
var notesInChromaticScale = ["e", "f", "f#", "g", "g#", "a", "a#", "b", "c", "c#", "d", "d#"];

var modes = [
    "pentatonic major",
    "pentatonic minor",
    "lydian",
    "dorian",
    "ionian",
    "aeolian",
    "mixolydian",    
    "phrygian"
];

if(activityJson && activityJson.ui_instructions){
    if(activityJson.ui_instructions.messages){
        for (const k in activityJson.ui_instructions.messages) {
            ar.push(activityJson.ui_instructions.messages[k])
        }
        var p = ar;
        var l = ar[0];
        ui_messaging_interface.init(ar);
    }
}

$(".caged_link").click(function(e){
    var t = $(e.currentTarget).attr("href").replace("#","")
    var num = Number( t );
    if(t.includes("+")){
        num = t.replace("+","");
        secondOctave = true;
    }
    position = Number( num ) + 1
    update();
});


$(".key_link").click(function(e){
    key = $(e.currentTarget).attr("href").replace("#","")
    update()
    return false;
}) 
$(".modes_link").click(function(e){
    mode = $(e.currentTarget).text()
    update();
    return false;
})

$(".forward_btn").click(function(e){
    var v = $("#mode").text()
    var p = modes.indexOf(v);
    var j = p >= modes.length-1 ? 0 : p + 1
    mode = modes[j]
    console.log(mode)
    update();
    return false;
});







function nextMode(){
    var b = $("#mode").text();
    var v = modes.indexOf(b);
    console.log(modes, $("#mode").text())
    v = v <= 0 ? modes.length-1 : v-1
    mode = modes[v]
    console.log("b ",b," v ",v," mode ",mode)
    update();
}



$(".back_btn").click(function(e){
    nextMode()
    return false;
});




function start(){
    $(".glyphicon-play").hide()
    $(".glyphicon-pause").show()
    intervalHandle = setInterval(function() { nextMode() }, 1000);   

}
function stop(){
    $(".glyphicon-play").show()
    $(".glyphicon-pause").hide()
    clearInterval(intervalHandle); 
}




var intervalHandle;


$(".play_btn").click(function(e){
    console.log($("#modes_play_btn").attr("style"))
    if($("#modes_play_btn").attr("style").includes("display:inline") || $("#modes_play_btn").attr("style").includes("display: inline")  || !$("#modes_play_btn").attr("style")){
        console.log("start")
        start()
    }else{
        stop()    
    }
    
    return false;
});
var original_scale;

function update(){
    var i;
    k = key;
    switch (mode.toLowerCase()) {
        case "phrygian":
        case "aeolian":
        case "dorian":
        case  "pentatonic minor":
            // shift to get a different key.   
             i = notesInChromaticScale.indexOf(key)
            var p = ((i + 12) - 3)%12

            k = notesInChromaticScale[p];
        break;        
    }
    // var b = caged.getCagedPositionArrayByNumberScaleNameAndKey(position, scale, k , secondOctave);
    var c = caged.getCagedPositionArrayByNumberScaleNameAndKey(position, mode, k , secondOctave);
    // console.table(c)
    guitar.hideAll();
    //guitar.showNotesInArray(b)
    guitar.showNotesInArray(c)
    //--
    var c = caged.getScaleByName(scale)
    var d = caged.getScaleNotesByKeyAndStructure(k,c['structure']);
    //---
    var e = caged.getScaleByName(mode)
    var f = caged.getScaleNotesByKeyAndStructure(k,e['structure']);
    //color the mode first
    for (let i = 0; i < f.length; i++) {
        guitar.highlightNote(f[i],"green");        
    }
    // I generally use the typeof operator:
    if (typeof original_scale == 'undefined') {
        original_scale = d
    }
    for (let i = 0; i < original_scale.length; i++) {
        console.log(original_scale[i])
        guitar.highlightNote(original_scale[i],"blue");        
    }
    //---
    // for (let i = 0; i < d.length; i++) {
    //    guitar.highlightNote(d[i],"blue");        
    // }
    //---
    guitar.highlightNote(k,"orange");
    var str = "<div class = 'caption'><h2><span id = 'key'>"+key+"</span> "+scale+", position <span id = 'position'>"+position+"</span></h2><h3>Compared to <span id = 'mode'>"+mode+"</span></h3></div>"
    $("#activity_messaging").html(str)
    return {
        "scale":d,
        "mode":f
    }
}








function audioReady(){}


update();
