/*

 Playable guitar makes calls on Guitar.js 

*/

class CagedActivityDev extends CagedActivity{
	/*
		interaction point for the  UI and the json.  	
	*/

	objseq =  [
		{
			"position":1,
			"scale":"major",
			"key":"e",
			"direction":1
		},
		{
			"position":2,
			"scale":"major",
			"key":"e",
			"direction":-1
		},		
		{
			"position":3,
			"scale":"major",
			"key":"e",
			"direction":1
		},				
		{
			"position":4,
			"scale":"major",
			"key":"e",
			"direction":-1
		},		
		{
			"position":5,
			"scale":"major",
			"key":"e",
			"direction":1
		},				
		{
			"position":5,
			"scale":"major",
			"key":"e",
			"direction":-1
		},																										
	];
	





	constructor(obj) {
		super();
		//set the json playerParameters vars locally.
		var object = obj.playerParameters;
		for (const key in object) {
			
			if (Object.hasOwnProperty.call(object, key)) {
				const element = object[key];
				this[key] = element;
				//console.log(key, element)
			}
		}
		this.init();	
		if(this.autoPlay){
			this.parseDrillLink(obj.command);
		}
	}

	init(){	
		// create UI, add listener, set initial default values.
		var ui = new CagedActivityDevUi();
		ui.setKey(this.key, false)
		ui.setScale(this.scale, false)
		ui.setPosition(this.position, false)
		var scope = this;
		this.drill = {
			pattern:[ ['1', '2', '3','4'], ['2', '3', '4','5']],
			name: "beginning default"
		}
		this.update();
		ui.listenForEvent( "ui_event", function(e){
			this.parseUiEvent(e[0].name,e[0].value)
		}, this)	
	};



	startHandHolding(){
		// this is the "hand holding algorithm"
		this.progressingTopIndex = this.startingTopIndex = 1
		var originalCount = 0
		var count = originalCount
		var threshold = 8;
		var scope = this;
		var sequence_ended = false
		console.log("this.clearEventListeners() is a nuclear option, and could intefere with future functionality. It only exists right now to deal with the situation of ")
		this.clearEventListeners()
		var l1 = function(e){
			count++
			console.log("count: ", count, "sequence_ended: ",sequence_ended)
			if(count >= threshold){
				threshold = 4;
				count = originalCount;
				this.progressingTopIndex++
				if(sequence_ended){
					scope.removeEventListener("progressing_top_index_exceeded",l1)
					scope.removeEventListener("sequence_complete",l2)
					sequence_ended = false;
					scope.stop()		
				}
		}}
		var l2 = function(e){
			console.log("sequence_complete")
			sequence_ended = true;
		}
		this.listenForEvent( "progressing_top_index_exceeded", l1, this)	
		this.listenForEvent( "sequence_complete", l2, this);
	}

	

	parseDrillLink(value){
		var ar = value.split("/")
		var data;
		guitar.hideAll();
		switch (value) {
			// octave to octave 1 string
			case "#octave_to_octave/1s":
				// console.log(value,this.key,this.scale)
				data = [  CagedActivityModel.getScaleOctivetoOctiveOneString(this.key,caged.getScaleByName(this.scale))];
				// this.load(data)
				// this.startHandHolding()
				// this.play();
				// return;
			break;
			// octave to octave position 1
			case "#octave_to_octave/P1":
			case "#p1/an":
			case "#octave_to_octave/P2":
			case "#p2/an":				
				data =	[  CagedActivityModel.getScaleAcrossNeckAtPosition(this.position, this.scale, this.key )  ];
				var b = 0;
			break;					
			// crawl through caged positions. 
			case("#crawl"):
				data =	  CagedActivityModel.createNeckCrawl(this.scale, this.key )  ;		
			break;	

			case("#test"):
				data = CagedActivityModel.getSequence(this.objseq);		
			break;				

			default:
				throw new Error("CagedActivityDev.parseDrillLink(value) switch statement mot matched.")
			break;
		}

		if(this.reversed || this.direction == -1 || this.direction == "-1"){
			data.reverse();
			for (let index = 0; index < data.length; index++) {
				data[index].reverse();
				for (let j = 0; j < data[index].length; j++) {
					if(Array.isArray(data[index][j])){
						data[index][j].reverse(); // this may cause a bug.  if you come here bug hunting.   This is probably it. 
					}
				}
				
			}
		}
		this.load( data );		
		if(this.handHoldingMode || this.handHolding){
			this.startHandHolding()
		}
		this.play();
	}


	parseUiEvent(name,value){
		console.log("parseUiEvent: ",name,value)
		//  step-forward  back_btn
		switch (name) {
			case "key":
				this.key = value;
				this.update()
			break;
			case "scale"     :
				this.scale = value;
				this.update()
			break;
			case "drills_link2":
				this.parseDrillLink(value)
			break;
			case "drill":
				// alert("drill not yet written.")
				// console.log(value)
				this.stop();
				this.drill = value
				this.update()
				this.play();
			break;
			case "position":
				this.position = value;
				this.update()
				this.play();
			break;
			case "stop":
				this.stop();
				guitar.hideAll();
			break;		
			case "play":
				this.play();
			break;			
			case "pause":
				this.pause();
			break;	
			case "mode":
				this.pause();
				this.player_mode = value.replace("#","").replace(" ","");
				// this.intervalDurationMs = value;;
				this.play();
			break;						
			case "slider":
				this.pause();
				this.intervalDurationMs = value;;
				this.play();
			break;			
			case "repeat_btn":
			break;																
			case "step-backward":
			break;																			
			case "step-forward":
			break;																	
			case "forward":
			break;			
			case "back":
			break;						
			case "refresh":
				//CagedActivityModel.assembleAndGetDrillAsArray(stringArray,sequenceArray)
				var g = CagedActivityModel.getScaleOctivetoOctiveOneString("a",caged.getScaleByName('major'))
				// var g =	CagedActivityModel.getScaleAcrossNeckAtPosition(2,"major","e" )
				this.load([g])
				this.play();
				console.table(g)
				// this.debug();
			break;									
			
			default:
				break;
		}
		var i = 0;
		// announceEvent(string, parameters){
		this.announceEvent("ui_event", { name:name, value:value });	
	}

}

