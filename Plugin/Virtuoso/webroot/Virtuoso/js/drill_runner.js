class DrillRunner extends EventsManager {

    constructor() {
      super();
    }


	getCagedPositionArrayForFret(position){
		// ugh.  dont like this.  it relies on external state. 
		return caged.getCagedPositionArrayForFret(position);
	}


	assembleAndGetDrillAsFlatArray(source_position_array, drill_pattern){
		var finalArray = []
		var indexIncrement = parseFloat(drill_pattern[1]) - parseFloat(drill_pattern[0]);
		var iterations = 0
		finalArray.push()
		for (let source_array_index = 0; source_array_index < source_position_array.length; source_array_index++) {
			var thisIndex = 0
			for (let drill_pattern_index = 0; drill_pattern_index < drill_pattern[0].length; drill_pattern_index++) {
				var a = drill_pattern[0];
				var b = parseFloat(a[drill_pattern_index])
				thisIndex = b+iterations-1
				if(thisIndex >= source_position_array.length){
					return finalArray;
				}
				 finalArray.push(source_position_array[thisIndex])
			}
			iterations+=indexIncrement
		}
		return finalArray;
	}


	assembleAndGetDrillAsMatrix(source_position_array, drill_pattern){
		var finalArray = []
		var indexIncrement = parseFloat(drill_pattern[1]) - parseFloat(drill_pattern[0]);
		var iterations = 0
		finalArray.push()
		for (let source_array_index = 0; source_array_index < source_position_array.length; source_array_index++) {
			var thisIndex = 0
			var thisArray=[]
			for (let drill_pattern_index = 0; drill_pattern_index < drill_pattern[0].length; drill_pattern_index++) {
				var a = drill_pattern[0];
				var b = parseFloat(a[drill_pattern_index])
				thisIndex = b+iterations-1
				if(thisIndex >= source_position_array.length){
					finalArray.push(thisArray)
					return finalArray;
				}
				thisArray.push(source_position_array[thisIndex])
			}
			finalArray.push(thisArray)
			iterations+=indexIncrement
		}
		return finalArray;
	}


  
	runDrill(_drill_pattern, position = 5){
		/*
		// _drill_pattern is a matrix of indexes.  ie 321, 432, 543 etc.   
		// drill_name is the string the comes from the href in ui
		*/
		guitar.visualizeSelectNotes("clear");
	 	guitar.hideAll();
		var scope = this;
		/// CODE SMELL
	    var caged_array = this.caged_array = this.getCagedPositionArrayForFret(position);
		
		this.drill = {

			pattern:_drill_pattern[0],
			caged_array:caged_array,
			scope:scope,
			interval:250,
			drill_pattern_index:0,
			caged_scale_index:0,

			resetCaged(){
				// shoow
				/// this needs to interact with the guitar.js and call there.     
				for (var i = 0; i < this.caged_array.length; i++) {
				    $(this.caged_array[i]).removeClass("not_shown")
				    $(this.caged_array[i]).addClass("reduced")
				}
			},
			run(scope,stop){
			    var timeout;
			    var drill_pattern = scope.pattern;
			    ///loop throught drill_pattern then advance the caged_scale_index 
			    function goThroughDrill(){
			        scope.resetCaged()
			        var a  = scope.caged_scale_index+parseInt(drill_pattern[scope.drill_pattern_index])-1; 
					if(a >= caged_array.length){
						console.log("time to stop!")
						var i = 0;
						return;
					}
					var tr = caged_array[a].replace("#fret_","").split("_");
			        $(caged_array[a]).removeClass("reduced")
			        if( scope.drill_pattern_index == 0 ){
						// $(".note").removeClass("red")
			            $(".note_highlight").removeClass("red").removeClass("fadeOut f10s")
						$(caged_array[a]+" .note_highlight").addClass("red").addClass("fadeOut f10s")
			        }
					guitar.playSoundForNote( parseFloat(tr[0])+1, tr[1]);
			        scope.drill_pattern_index++;
			        if( scope.drill_pattern_index < drill_pattern.length){
			            if(!stop){
							scope.intervalHandle = setTimeout(goThroughDrill, scope.interval);
						}
			        }else{
			            scope.caged_scale_index++
			            if( scope.caged_scale_index < caged_array.length ){
			                scope.drill_pattern_index = 0;
							if(!stop){
								scope.intervalHandle = setTimeout(goThroughDrill, scope.interval);
							}
			            }else{
							console.log('removeClass("reduced")')
			            	$(".note").removeClass("red").removeClass("reduced")
							drill_pattern_index:scope.drill_pattern_index;
							caged_scale_index:scope.caged_scale_index;
			            }
			        }
			    }
			    goThroughDrill();
			}
		}
		this.drill.run(this.drill);
	}






	

  }

  var drill_runner = new DrillRunner();