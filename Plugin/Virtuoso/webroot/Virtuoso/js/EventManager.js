class EventsManager {

	// eventListeners = [];

	constructor() {
		this.eventListeners = [];
	}


	announceEvent(string, parameters, caller = null){
		try {
			var K = this.eventListeners;
			// console.log(this.eventListeners)
			this.eventListeners.forEach(function (item) {
				// item.call(scope, o);
				var i = item;
				if (item['name'].toLowerCase() == string.toLowerCase()) {
					// alert(item['name'])
								item['func'].call(item['scope'],[parameters]);
					// item['name']
				}
				var n = 0
			});
		} catch (error) {
			console.log(error)
			var p = 10
		}
	}

	clearEventListeners(){
		this.eventListeners = [];
	}

	listenForEvent(name, func,scope, caller = null){
		if( name == undefined ||
			func == undefined ||
			scope == undefined
		){
			console.log("EventsManager.listenForEvent error: ",name, func,scope)
		}
		this.eventListeners.push(
		{
			name:name,
			func:func,
			scope:scope,
			params:[]
		}
			)
	}


	removeEventListener(name, func){
		for (let i = 0; i < this.eventListeners.length; i++) {
			var _name = this.eventListeners[i].name;
			if(_name == name ){
				this.eventListeners.splice(i, 1);
			}
		}
	}




}
