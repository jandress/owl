class Scales {

	major_scale = {
		'name':"major",
		'structure':[ 2 , 2 , 1 , 2 , 2 , 2 , 1 ]
	}

	minor_pentatonic = {
		'name':"minor pentatonic",
		'structure':[ 3,2,2,3,2 ]
	}

	chromatic = {
		'name':"chromatic",
		'structure':[ 1,1,1,1,1,1,1,1,1,1,1,1 ]
	}
	
	lydian_mode = {
		'name':"lydian",
		'structure':[ 2 , 2 ,2, 1 , 2 , 2 , 1  ]
	}


	ionian_mode = {
		'name':"ionian", // also major
		'structure':[ 2 , 2 , 1 , 2 , 2 , 2 , 1 ]
	}

	mixolydian_mode = {
		'name':"mixolydian",
		'structure':[2 , 2 , 1 , 2, 2, 1, 2 ]
	}

	dorian_mode = {
		'name':"dorian",
		'structure':[ 2 , 1 , 2 , 2 , 2 , 1, 2]
	}
	aeolian_mode = {
		'name':"aeolian",
		'structure':[2,1,2,2,1,2,2 ]
	}
	phrygian_mode = {
		'name':"phrygian",
		'structure':[1,2,2,2,1,2,2 ]
	}
	locrian_mode = {
		'name':"locrian",
		'structure':[1,2,2,1,2,2,2 ]
	}


	modes = [
		this.lydian_mode
		,this.ionian_mode
		,this.mixolydian_mode
		,this.dorian_mode
		,this.aeolian_mode
		,this.phrygian_mode
		,this.locrian_mode
	]

	scales = [
		this.major_scale,
		this.minor_pentatonic,
		this.lydian_mode
		,this.ionian_mode
		,this.mixolydian_mode
		,this.dorian_mode
		,this.aeolian_mode
		,this.phrygian_mode
		,this.locrian_mode
	]
	scale = this.major_scale;

	getScaleByName(name){
		for (var i = this.scales.length - 1; i >= 0; i--) {
			if(this.scales[i].name.toLowerCase().replace(" ","") == name.toLowerCase().replace(" ","")){
				return this.scales[i]
			}

		}
		return this.chromatic
	}
}



var scales = new Scales();
