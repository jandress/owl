class GuitarUi extends EventsManager{

	_scope = this;

	constructor() {
		super();
		this._scope = this;
		this.init();
	}

	init(){
		var _scope = this._scope
		$("#toggleVis").click(function(e){
			_scope.announceEvent("toggle_vis")
			return false;
		});

		$(".key_link").click(function(e){
			_scope.announceEvent("key_link",e)
		    return false;
		});

		$(".scale_link").click(function(e){
			_scope.announceEvent("scale_link",e)
			return false;
		});

		$(".caged_link").click(function(e){
			_scope.announceEvent("caged_link",e)
			return false;
		});

		$(".visualize_link").click(function(e){
			_scope.announceEvent("visualize_link")
		    return false;
		});

		$(".neck_btn").click(function(e){
			_scope.announceEvent("neck_btn")
		    return false;
		});

		$("#fooButton").click(function(e){
			showModal()
			_scope.announceEvent("fooButton")
		    return false;
		});

		$("#play_pause_btn").click(function(e){
			_scope.announceEvent("play_pause_btn")
		    return false;
		});
		$("#prev_btn").click(function(e){
			_scope.announceEvent("prev_btn")
		    return false;
		});
		$("#next_btn").click(function(e){
			_scope.announceEvent("next_btn")
		    return false;
		});
		$(".interval_link").click(function(e){
			_scope.announceEvent("interval_link")
		    return false;
		});

		$(".drills_link a").click(function(e){
			_scope.announceEvent("drills_links")
		    return false;
		});
		$("#sliderW").on("change", function(e) {
			_scope.announceEvent("sliderW")
		    return false;
		});
	}
}
