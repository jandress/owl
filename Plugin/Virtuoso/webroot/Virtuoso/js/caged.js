class Caged {

	scales = [
		{
			name:"Major",
			structure:[2,2,1,2,2,2,1], //2,2,1,2,2,2,1
			caged_positions:[7,9,0,2,4] 
		}, // END MAJOR SCALE

		/*
			2 2 2 1 2 2 1
			name:"dorian",
			structure:[ 2 , 1 , 2 , 2 , 2 , 1, 2]
			name:"lydian",
			structure:[ 2 , 2 ,2, 1 , 2 , 2 , 1  ]
		*/

		{
			name:"Pentatonic Minor",
			structure:[3,2,2,3,2]
			,caged_positions:[10,12,3,5,8,10]

		},// END SCALE
	
		{
			name:"Pentatonic Major",
			structure:[2,2,3,2,3],
			caged_positions:[7,9,0,2,4,7,9,0,2,4] 		
		},// END SCALE
		{	
			name:"lydian",
			structure:[ 2 , 2 ,2, 1 , 2 , 2 , 1  ],
			caged_positions:[7,9,0,2,4,7,9,0,2,4] 		
		},	

		{
			name:"Pentatonic Neutral",
			structure:[2,3,2,3,2],
		},// END SCALE
		{
			name:"Harmonic Minor",
			structure:[2,1,2,2,1,3,1],
		},// END SCALE
		{
			name:"Melodic Minor",
			structure:[2,1,2,2,2,2,1],
		},// END SCALE
		{
			name:"Natural Minor",
			structure:[2,1,2,2,1,2,2],
		},
		{
			name:"chromatic",
			structure:[1,1,1,1,1,1,1,1,1,1,1,1],
		},		

		{
			name:"ionian", // also major
			structure:[ 2 , 2 , 1 , 2 , 2 , 2 , 1 ],
			caged_positions:[7,9,0,2,4] 
		},
		{
			name:"mixolydian",
			structure:[2 , 2 , 1 , 2, 2, 1, 2 ],
			caged_positions:[7,9,0,2,4] 
		},

		{
			name:"dorian",
			structure:[ 2 , 1 , 2 , 2 , 2 , 1, 2]
			,caged_positions:[10,12,3,4,7,10]
		},

		{
			name:"aeolian",
			structure:[2,1,2,2,1,2,2 ]
			,caged_positions:[10,12,3,4,7,10]
		},
		{
			name:"phrygian",
			structure:[1,2,2,2,1,2,2 ]
			,caged_positions:[10,12,3,5,8,10]
		},







		{
			name:"locrian",
			structure:[1,2,2,1,2,2,2 ]
		}
	];


	notesInChromaticScale = ["e", "f", "f#", "g", "g#", "a", "a#", "b", "c", "c#", "d", "d#"];
	
    constructor() {
        
    }

    randomNumber(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }

    getRandomScale() {
        const randomIndex = this.randomNumber(0, this.scales.length - 1);
        return this.scales[randomIndex];
    }

	getScaleNotesByKeyAndStructure(key, structureArray){
		//console.log("getScaleNotesByKeyAndStructure() key: ",key, ", structureArray: ", structureArray)
		var notes = this.notesInChromaticScale;
		var index_of_root_note = notes.indexOf( key )
		var noteArray = [notes[index_of_root_note]];   // note of the index_of_root_note in the chromatic scale
		var ind = 0;
		var note;
		for (var i = 0; i < structureArray.length; i++) {
			// index_of_root_note
			ind += parseInt(structureArray[i]);
			//console.log(ind)
			note = notes[this.getNotePositionInChromaticScale(key,ind)];
			noteArray.push(note)
		}
		return noteArray; 
	}

    getNotePositionInChromaticScale(key, steps = 0) {
        const rootIndex = this.notesInChromaticScale.indexOf(key.toLowerCase());
        return (rootIndex + steps) % this.notesInChromaticScale.length;
    }

    getScaleByName(name) {
        return this.scales.find(scale => scale.name.toLowerCase().replace(/\s/g, "") === name.toLowerCase().replace(/\s/g, ""));
    }

    getFretForPosition(position, scaleName = "major", key = "c") {
        const scale = this.getScaleByName(scaleName);
        if (!scale) {
            throw new Error(`Scale "${scaleName}" not found.`);
        }

        const keyIndex = this.notesInChromaticScale.indexOf(key.toLowerCase());
        const cIndex = this.notesInChromaticScale.indexOf('c');
        const diff = (keyIndex - cIndex + 12);
		console.log("key",key, " key.toLowerCase(): ",key.toLowerCase(), "c  cIndex: ",cIndex ," diff: ",diff)

        const cagedPosition = scale.caged_positions[position - 1];
//		console.log("diff: ",diff, "cagedPosition : ",cagedPosition )
        let fret = (cagedPosition + diff) % 12;		
		let fret2 = fret > 12 ? fret % 12 : fret;
		// console.log("cagedPosition: ",cagedPosition," scale.caged_positions[position - 1]: ",scale.caged_positions[position - 1], "scale.caged_positions: ",scale.caged_positions)
		// console.log("position ",position,"scaleName ",scaleName, "key ", key);
         console.log("keyIndex: ",keyIndex,"cIndex: ",cIndex ,"diff: ",diff,"cagedPosition: ",cagedPosition,"fret: ",fret,"fret2: ",fret2)
		return fret2;
    }

    getCagedPositionArrayByNumberScaleNameAndKey(num, scaleName, key, secondOctave=false) {
        const scale = this.getScaleByName(scaleName);
        if (!scale) {
            throw new Error(`Scale "${scaleName}" not found.`);
        }

        guitar_model.setDataForScaleFullNeck(key, scale);
        let fretNumber = this.getFretForPosition(num, scaleName, key);
		
		
		// fretNumber = 4;


		console.log("num ",num, "fretNumber ", fretNumber)
		if(secondOctave){
			// 21 - 5 - 16
			if(fretNumber  <= 4 ){
				fretNumber+=12
			}
		}
        return this.getCagedPositionArrayForFret(fretNumber, scale);
    }

	
	getCagedPositionArrayForFret(fretNumber){
		// this guy works off of getting the actual note text from each fret div.    Works, but is less than Ideal. 
		// //console.log("getCagedPositionArrayForFret", fretNumber, this.key)
		// consider whether or not this this should come from a direct call to guitar_model.getDataForScaleFullNeck(key,scl).  that is currently happening in getCagedPositionArrayByNumberScaleNameAndKey(num, scale, key)
		var nData = guitar_model.neckData; // this breaks encapsulation.   neckData contains the current scale_notes_flat array
		var string = 0;
		var fretNumber = parseInt(fretNumber);
		var locationIdArray = []		
		function _crawlString(str){
			// populates locationIdArray
			var id;
			var thisFretsNote;
			for (var i = fretNumber; i < fretNumber+5; i++) {
				id = string+"_"+i;
				thisFretsNote = $("#fret_"+id).text();
				var noteIsInThisScale = nData['scale_notes_flat'].indexOf(thisFretsNote) > -1;
				if(noteIsInThisScale  && thisFretsNote.trim() != $("#fret_"+(string+1)+"_"+fretNumber).text().trim()){
				locationIdArray.push("#fret_"+id)
				}
			}
			if(string<5){
				string++
				_crawlString(str)
			}
		}
		_crawlString(0); // subroutine of this method.
		return locationIdArray;
	}



    createFretId(string, fret) {
        return `#fret_${string}_${fret}`;
    }
}

// Usage Example
const caged = new Caged();




/*
	Intended to basically be a big data model


class Caged {
			
		notes_in_chromatic_scale = [ "e","f","f#","g","g#","a","a#","b","c","c#","d","d#" ];

		/* 

		2 Main Functions: 
		
		fretNumber = getFretForPosition(position,scaleName="major",key="c")
			find out the lowest fret that occurs in the CagedPosition at the given key.    
			The problem with this is that the CagedPosition locations are different depending on the key  
			and they also repeat after the 12th fret.  So you dont have a good mechanism for selecting 


		getCagedPositionArrayForFret(fretNumber)
			crawls accross (as opposed to down) the neck and assembles a flat array representing the fretboard locations 


		




Help me design a javascript class with a rather complicated algorithm.      Consider the caged system for the pentatonic scale in guitar.  
Ultimately I want to create the following function: 

var b = caged.getCagedPositionArrayByNumberScaleNameAndKey("1","pentatonic major", "c");

It would be used in the following way:

guitar.showNotesInArray(b)



		getCagedPositionArrayByNumberScaleNameAndKey(num, scale, key)   sets the model data then calls getCagedPositionArrayForFret(fretNumber)

		
				ultimate use case you are working on: 

					var b = caged.getCagedPositionArrayByNumberScaleNameAndKey("1","pentatonic major", "c");
					guitar.showNotesInArray(b)

				
				1.  You have 3 parameters, the scale name, the key and the position number: getCagedPositionArrayByNumberScaleNameAndKey("1","pentatonic major", "c");   

				2.  The "CagedPosition" is a flat array with appx 12 fret ids whose notes exist within the scale at at a certain key.  
					The CagedPosition is identified by an index of the caged_positions array.  This number must be provied to:
						fretNumber = getFretForPostition(num, scale, key)

						fretNumber = getFretForPostition(position_index, scale, key) is intended to examine all strings and return the 
						lowest fret that occurs in the CagedPosition and key.   The string does not matter. 
						It retrieves notes directly from the fretboard note divs so that it is not duplicating state 
				
				3. 	The algo that returns the actual "CagedPosition" array:  getCagedPositionArrayForFret(fretNumber) 
					crawls across (as opposed to down) the neck and returns the notes within a 5 fret span of the 
					initial fret parameter  as a flat array.





			Caged position logic centers around the intervals contained in the scales[x][structure] array.  
			Those intervals define the intervals in the scale.  For example if you  want C major,  the intervals are 2,2,1,2,2,2,1 
			which produce the notes C,d,e,f,g,a,b 
			
			The caged_positions values (scales[x]['caged_positions']) are values for the fret that is the lowest fret of the lowest occurance in the position array
			in the key of C on the E string.  getCagedPositionArrayForFret(fretNumber)  crawls across the neck and populates an array 
			with the notes that exist on each string within a range of 5 frets from the initital given fret within the scale starting at   

		
			

			
			
			





			// Caged positions are actually exact for the key of c but for the key of d, the values increase by 2 frets.

			// so it is interval = caged_positions[x] + distance_from_c
			// if it exceeds 12 it is N - 12
			// it doesnt reset until it reaches 12.
			// c = 0,2,4,7,9
			// a = 9,11,2,4,6
			// d = 2,4,6,9,11
			// e = 1, 3, 6, 8, 11




		
		scales = [
			{
				name:"Major",
				structure:[2,2,1,2,2,2,1], //2,2,1,2,2,2,1
				caged_positions:[7,9,0,2,4] 
			}, // END MAJOR SCALE
			{
				name:"Pentatonic Minor",
				structure:[3,2,2,3,2],
				caged_positions:[7,9,0,2,4] 				
			},// END SCALE
			{
				name:"Pentatonic Major",
				structure:[2,2,3,2,3],
				caged_positions:[7,9,0,2,4,7,9,0,2,4] 		
			},// END SCALE
			{
				name:"Pentatonic Neutral",
				structure:[2,3,2,3,2],
			},// END SCALE
			{
				name:"Harmonic Minor",
				structure:[2,1,2,2,1,3,1],
			},// END SCALE
			{
				name:"Melodic Minor",
				structure:[2,1,2,2,2,2,1],
			},// END SCALE
			{
				name:"Natural Minor",
				structure:[2,1,2,2,1,2,2],
			},
			{
				name:"chromatic",
				structure:[1,1,1,1,1,1,1,1,1,1,1,1],
			},		
			{
				name:"chromatic",
				structure:[ 1,1,1,1,1,1,1,1,1,1,1,1 ]
			},
			
			{
				name:"lydian",
				structure:[ 2 , 2 ,2, 1 , 2 , 2 , 1  ]
			},	
			{
				name:"ionian", // also major
				structure:[ 2 , 2 , 1 , 2 , 2 , 2 , 1 ]
			},
		
			{
				name:"mixolydian",
				structure:[2 , 2 , 1 , 2, 2, 1, 2 ]
			},
		
			{
				name:"dorian",
				structure:[ 2 , 1 , 2 , 2 , 2 , 1, 2]
			},
			{
				name:"aeolian",
				structure:[2,1,2,2,1,2,2 ]
			},
			{
				name:"phrygian",
				structure:[1,2,2,2,1,2,2 ]
			},
			{
				name:"locrian",
				structure:[1,2,2,1,2,2,2 ]
			},







		];
		randomNumber(min, max) {
			return Math.round(Math.random() * (max - min) + min);
		}
			
		getRandomScale(){
			var min = 0;
			var max = this.scales.length
			var i = this.randomNumber(min,max)
			return this.scales[i];
		}
		getNotePositionInChromaticScale(key,steps){
			// maybe relocate this to caged.

			// there are 12 tones in the chromatic scale. this returns an index (0 - 11) of the note x steps from a given note.
			// key is a string.
			// notes is a flat array from e to d#
			// strings is a flat array of strings "e a d g b e"
			var strings = this.string_tunings;
			var notes = this.notes_in_chromatic_scale;
			var index_of_root_note = notes.indexOf(key)
			if(!steps){
				steps = 0
			}
			var b = (index_of_root_note+steps)%notes.length;
			////console.log("getNotePositionInChromaticScale: ", key, steps, b)
			return b;
		}

		getScaleByName(name){
			for (var i = this.scales.length - 1; i >= 0; i--) {
				if(this.scales[i].name.toLowerCase().replace(" ","") == name.toLowerCase().replace(" ","")){
					this.scales[i].length = function(){
						var b = this.structure.length;
						return b
					}
					return this.scales[i]
				}
			}
			return this.scales[0]
		}


		
		getCagedPositionArrayByNumberScaleNameAndKey(num, scale, key){
			//console.log(num, scale, key)
			guitar_model.setDataForScaleFullNeck(key,this.getScaleByName(scale))
			var b = caged.getFretForPosition(num, scale, key);    
			console.log("getFretForPosition: ",b)
			return this.getCagedPositionArrayForFret(b)
		}

		
		getCagedPositionArrayForFret(fretNumber){
			// this guy works off of getting the actual note text from each fret div.    Works, but is less than Ideal. 
			// //console.log("getCagedPositionArrayForFret", fretNumber, this.key)
	
			/*
			
				recieves a number for the fret
				returns an array of fret IDs. 
				it retrieves the notes in the 
				search returns 5 results in 3 files.   3 uses in this file.   also used in Interval Runner and modes_intro.js
				and intervalRunner.js may be abandoned.    so may only really be used here			

			// consider whether or not this this should come from a direct call to guitar_model.getDataForScaleFullNeck(key,scl).  that is currently happening in getCagedPositionArrayByNumberScaleNameAndKey(num, scale, key)
			var nData = guitar_model.neckData; // this breaks encapsulation.   neckData contains the current scale_notes_flat array
			var string = 0;
			var fretNumber = parseInt(fretNumber);
			var locationIdArray = []
			
			function _crawlString(str){
				// populates locationIdArray
				var id;
				var thisFretsNote;
				for (var i = fretNumber; i < fretNumber+5; i++) {
					id = string+"_"+i;
					thisFretsNote = $("#fret_"+id).text();
					var noteIsInThisScale = nData['scale_notes_flat'].indexOf(thisFretsNote) > -1;
					if(noteIsInThisScale  && thisFretsNote.trim() != $("#fret_"+(string+1)+"_"+fretNumber).text().trim()){
					locationIdArray.push("#fret_"+id)
					}
				}
				if(string<5){
					string++
					_crawlString(str)
				}
			}
			_crawlString(0); // subroutine of this method.
			return locationIdArray;
		}



		getFretForPosition(position,scaleName="major",key="c"){			
			//key = "c#"
			// position = Number(9)
			// 7 should be the same as 2 but isnt.   
			// 6 should be the same as 1 but isnt.   
			// 5 is 
			var inc = 0;
			if(position > 7){
				inc = 12;
				// inc = position-5  
				console.log(position, position%5);
				position = position % 5;
			}
			
			console.log("getFretForPosition position:",position,"scaleName: "+scaleName,"key: "+key, " inc: ",inc)

			/*
				Determines the fret to start on a caged position.   feed it the caged position number
			 	Returns a number  from 0 - 21 (not a fret id) for the lowest fret used on the e string.
				Caged positions are simply hard coded in the scales array objects for the key of C
				//
				Get the difference between the index of C and the index of the key in the chromatic scale.   This is the number to add to each  index in the scale array.  


			var index_of_c = this.notes_in_chromatic_scale.indexOf("c");  // is 8
			var index_of_key = this.notes_in_chromatic_scale.indexOf(key);
			var diff = index_of_key - index_of_c;
			diff = 0;

			//	7, 9, 12, 14, 17
			/// caged_positions are intervals, but the intervals are equal to fret numbers in the chromatic scale when the key is zero to zero.
			// console.log("index_of_key: ", index_of_key, " index_of_c: ",index_of_c, " diff: ", diff)
		 	// console.log("DIFF: ",diff, key)


			// var c = ( index_of_key - index_of_c )+12
			// return; 
			//		notes_in_chromatic_scale = [ "e","f","f#","g","g#","a","a#","b","c","c#","d","d#" ];
			// if(a == b){
			// 	c = a
			// 	console.log(1)
			// }
			//caged_positions:[7,9,0,2,4] 		

			// var frets = [ ]; 
			// //chromatic scale positions of c and the key.
			// get correct scale. 
			var correct_scale_index = 0
			for (var i = 0; i < this.scales.length; i++) {
				//	//console.log(this.scales[i]["name"].toLowerCase().toLowerCase().replace(" ","") , scaleName.toLowerCase().replace(" ","") );
				if( this.scales[i]["name"].toLowerCase().replace(" ","")  ==  scaleName.toLowerCase().replace(" ","")  ){
						//console.log(this.scales[i]["name"].toLowerCase().toLowerCase())
						correct_scale_index = i;
				}
			}
			var correctScale = this.scales[correct_scale_index];
			// console.log("diff: ", diff ,  correctScale['caged_positions'])
			var fret = diff + correctScale['caged_positions'][position-1];
			console.log("fret ",fret," inc: "+inc+",  fret+inc: ", fret+inc);
			console.log("correctScale['caged_positions']",correctScale['caged_positions'].length, correctScale['caged_positions'])

			 
			return fret+inc; 		




			//loop through the caged positions of the c scale and add an increment that is equal to the difference between the 


			// var d;
			// //console.log(this.scales[correct_scale_index])
			// var a = this.scales;
			// var b = this.scales[correct_scale_index];

			// // var c = this.scales[correct_scale_index].caged_positions; // ok this is kinda problematic.
			// var n;
			// var thisScale = this.scales[correct_scale_index];

			// for (var i = 0; i < thisScale.caged_positions.length; i++) {

			// 	d = c + thisScale.caged_positions[i];  // thisScale.caged_positions = [7,9,0,2,4] 				
			// 	//console.log("d: ", d);
			// 	n = d < 12 ? d : d-12
			// 	frets.push(d)
			// }
			// 	// frets.push(d)
			// return frets[position];
			// return this.getScaleByName(scale).caged_positions[position]
		}
}




var caged = new Caged();

*/