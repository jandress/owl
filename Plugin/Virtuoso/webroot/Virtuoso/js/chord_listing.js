


function drawFretboard(canvas, fretStructure){
    
    canvas.height=120;
    canvas.width=100;
    var ctx = canvas.getContext("2d");
    var _w = 75;
    var _h = 90;
    var fret_radius = 5
    var _inc = _w/5;
    var num_frets = 4
    var _vinc = _w/num_frets;
    

    function drawMarker(string,fret){
        var radius = fret_radius
        var i_x = _inc;
        var i_y = _vinc
        var x = i_x * string+fret_radius
        var y = (i_y * fret )+fret_radius
        ctx.moveTo( x, y );        
        ctx.beginPath();
        ctx.fillstyle = "black";
        ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
        ctx.closePath();
        ctx.fill();
    
    }

    function drawBoard(){
        ctx.beginPath();
        ctx.strokeStyle = "black";
        var b = fret_radius*2.75;
        ctx.rect(fret_radius, b, _w, _h);
        for(var i = 1; i <5; i++ ){
            var g = _inc*(i)+fret_radius
            ctx.moveTo( g, b );
            ctx.lineTo( g, _h+b);
        }
        for(var i = 1; i <=num_frets; i++ ){
            var g = _vinc * (i)
            ctx.moveTo( fret_radius, g+b );
            ctx.lineTo( _w+fret_radius, g+b);
        }    

        ctx.moveTo( fret_radius, g+b );

        ctx.stroke();
        ctx.closePath();
    }

    drawBoard()

    for(var i = 0; i <fretStructure.length; i++ ){
        if(fretStructure[i].toString().toLowerCase() == "x"){
            continue;
        }
        drawMarker(i,fretStructure[i] )
    }

}









var cnvs = $("canvas");
for(var i = 0; i <cnvs.length; i++ ){
    drawFretboard(cnvs[i],chordsJS[i].structure);
}

