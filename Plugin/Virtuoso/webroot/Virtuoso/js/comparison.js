 

var key_1 = scale1Key.replace("~", "#");
var key_2 = scale2Key.replace("~", "#"); 
var scale_1_name = scale1Name.replace("_", " ");
var scale_2_name = scale2Name.replace("_", " ");
var scale_1_structure = caged.getScaleByName(scale_1_name).structure;
var scale_2_structure = caged.getScaleByName(scale_2_name).structure;


function returnUniques(ar1, ar2){
  var arr1 = []
  var arr2 = []
  for(var i = 0; i < ar1.length; i++){
    for(var p = 0; p < ar2.length; p++){
      if(ar1.indexOf(ar2[p])==-1){
        console.log("ar2[p]: ",ar2[p])
        if(arr2.indexOf(ar2[p])==-1){
          arr2.push(ar2[p])
        }
      }
    }
    if(ar2.indexOf(ar1[i])==-1){
        console.log("ari[i]: ",ar1[i])
        arr1.push(ar1[i])
      }
  }
  var a = [arr1, arr2]
  return a;
}





function assembleUniquesFormatting(ar, uniques){
  var str = "";
  for(var i = 0; i < ar.length; i++){
    // console.table(uniques)
    if(uniques.indexOf(ar[i]) >= 0 ){
      str +="<span class = 'underlined'>"+ar[i].toUpperCase()+"</span>&nbsp;";
    }else{
      str +=""+ar[i].toUpperCase()+"&nbsp;";
    }
  }
  return str
}


function executeComparison() {
    // console.log( key_2, scale_1, scale_2)
    var scale_1 = guitar.createScaleObject(key_1,scale_1_name,scale_1_structure)
    var scale_2 = guitar.createScaleObject(key_2,scale_2_name,scale_2_structure)
  
    // the name of this method is confusing because its not getting uniques in this context.
    var scale_1_notes =  guitar.getuniqueNotesForScale(scale_1);
    var scale_2_notes =  guitar.getuniqueNotesForScale(scale_2);      

    var p = returnUniques(scale_1_notes, scale_2_notes)
    console.log(scale_1_notes,scale_2_notes,p)

    scale_1_notes = assembleUniquesFormatting(scale_1_notes, p[0])
    scale_2_notes = assembleUniquesFormatting(scale_2_notes, p[1]);

    console.log(key_1, key_2)
    var str1 = "<h1 class='yellow_text'>"+key_1.toUpperCase()+" "+scale_1_name+" </h1><h2> compared to </h2>&nbsp;<h1  class='blue_text' >"+key_2.toUpperCase()+" "+scale_2_name+"</h1><h2> with </h2><h1  class='green_text'> common notes</h1>";
    var str2 = "<h3 style = 'margin-top:0px;'><span class='yellow_text'> "+scale_1_notes+"&nbsp; </span><span class='blue_text'>"+scale_2_notes+"</span></h3>"
    var str = str1 + str2;
    $("#caption_holder").html(str);
    //  console.log("structure: ", key_1,  scale_1.structure)
    guitar.compareScales([ scale_1, scale_2 ])
    hideModal()
};


$(window).ready(function(){
    executeComparison();
});