class IntervalTrainer extends sspUI{
	
    key = "e";
    scaleName = "pentatonic minor";

    string_tunings;
    notePromptDelay = 1000;
    timer_interval_length = 70;
    thisString = 0;
    fret_array;
    intervalSequencer
    runs = 0;
    run_limit = 1;
    
    min =  0; 
    max =  8; 
    current_guess = 0;

    randomizeScale = true;
    randomizeKey = true;

	constructor(is) {
		super(is);
        this.intervalSequencer = is;
        var scope = this;
        this.string_tunings = guitar.string_tunings;
        this.initExercise()

        $(".note").click(function(e){
            var b = $(e.currentTarget).attr("id");
            var c= scope.fret_array;
            var a = scope.fret_array.indexOf("#"+b)
            if(a == scope.current_guess){
                guitar.hideAll()
                scope.runs++
                site_flash.displaySuccess("Correct!",'',2000,function(){
                    if(scope.runs >= scope.run_limit){
                        scope.runs = 0
                        scope.initExercise()
                    }
                    ssp.play();    
                })
            }else{
                site_flash.displayFailure("Incorrect!","",2000,function(){
                })
                return false;
            }
        });

        $(".key_link").click(function(e){
            scope.key = $(e.currentTarget).text().toLowerCase();
            scope.string = scope.string_tunings.indexOf(scope.key)
            scope.initExercise()
            scope.setKeySelectLabel(scope.key);            
            return false;
        });
        $(".scale_link").click(function(e){
            scope.scaleName = $(e.currentTarget).text().toLowerCase();
            scope.initExercise()
            scope.setScaleSelectLabel(scope.scaleName);            
            return false;
        });
        $("#randomScale").click(function(e){})

        $("#randomKey").click(function(e){})
                
        scope.setKeySelectLabel(scope.key);            
        scope.setScaleSelectLabel(scope.scaleName);            
	}

    setKeySelectLabel(str){
        $(".key_select_label").html(str);
       // $("key_select").removeClass("open");
    }
    setScaleSelectLabel(str){
        $(".scale_select_label").html(str);      
        
    }





    initExercise(){
        
        console.log('initExercise(): ', caged.getRandomScale())
        guitar.hideAll()
        var scope = this;

        scope.randomizeKey = $("#randomKey").is(":checked");
        scope.randomizeScale = $("#randomScale").is(":checked");

        if(scope.randomizeKey){
            scope.key = scope.string_tunings[scope.randomNumber(0, 5)];
            scope.string = scope.string_tunings.indexOf(scope.key)
            scope.setKeySelectLabel(scope.key)
        }
        if(scope.randomizeScale){
            scope.scaleName = caged.getRandomScale().name
            scope.setScaleSelectLabel(scope.scaleName)
                        
        }

        var y = caged.getScaleByName(this.scaleName);
        console.log(y)
        this.max = y.length()+1;
        var data = guitar.getDataForScaleFullNeck( this.key, y)
        this.fret_array = this.intervalSequencer.assembleScaleSequenceInKeyOnString(data,this.string).slice(this.min,this.max);
        super.loadSequence(this.fret_array)
    }

    
    randomNumber(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }

    complete(){
        var scope = this;
        this.current_guess = scope.randomNumber(scope.min, scope.max-1);
        this.intervalHandle = setTimeout(function() {             
            console.log(scope.current_guess);
            var ar =  scope.fret_array[scope.current_guess].replace("#","").replace("fret_","").split("_");
            guitar.playSoundForNote(parseFloat(ar[0])+1,ar[1]);
        }, this.notePromptDelay);           
    }
}



var ssp = new IntervalTrainer(IntervalSequencer());



// $("#foo").click(function(e){
//     guitar.hideAll();
//     ssp.play();    
// });
