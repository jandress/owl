 IntervalSequencer = function(){
    
    var o = {

        notes_in_chromatic_scale : [ "e","f","f#","g","g#","a","a#","b","c","c#","d","d#" ],


        assembleBasicSequence(half_steps_to_increment = 1, string = 0, starting_fret = 0   ){
             // this one plays an advancing note in the sequence on a single string. 
            var ar = [];
            half_steps_to_increment = parseFloat(half_steps_to_increment)    
            for(var i = 0; i <=(21-half_steps_to_increment); i+=half_steps_to_increment ){
                ar.push("fret_"+string+"_"+i)
            }
            return ar;
        },


        assembleBasicSequence2(half_steps_to_increment = 1, string = 0, starting_fret = 0   ){
            // this one plays the first note in the sequnce then plays an advancing note in the sequence on a single string. 
            var ar = [];
            half_steps_to_increment = parseFloat(half_steps_to_increment)    
            for(var i = half_steps_to_increment; i <= (22); i+=half_steps_to_increment ){
                ar.push("fret_"+string+"_"+starting_fret)
                ar.push("fret_"+string+"_"+i)      
            }
            return ar;
        },

    
        assembleScaleSequenceInKeyOnString(scale, whatString){
            return scale['scale_positions_matrix'][whatString]
        }





    }
    return o;
}


