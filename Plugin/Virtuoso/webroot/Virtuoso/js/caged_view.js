
var player;

function audioReady(){	
     player = new CagedActivityDev( activityJson );  
	 //listenForEvent(name, func,scope){
	 player.listenForEvent( "ui_event", function(e){

		if(e[0].name == "step-forward"){
			window.location = next;
		}
		if(e[0].name == "step-backward"){
			window.location = prev;
		}
	},this);
}





/*
	Right Now this is the player that creates


	in javascript create a class named CagedSequncePlayer. 
	The purpose of the class is to iterate over an encapsulated multidimensional array via 2 functions that call each other via an interval or timeout.


	It should have the following public methods:

	Play
	Pause
	Stop
	Previous
	Next
	PreviousArray
	NextArray 

	It should have the ability to reverse the direction of iteration.    
	It should also have the ability to iterate to an index named progressingTopIndex (with an initial value of 3) and then reverse. 
	It should also have the ability to iterate to progressingTopIndex and then restart at an index named progressingBottomIndex in which case progressingTopIndex would then increment by 1.
*/
