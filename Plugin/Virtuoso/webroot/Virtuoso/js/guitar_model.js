
class GuitarModel extends EventsManager {

    notes_in_chromatic_scale = [ "e","f","f#","g","g#","a","a#","b","c","c#","d","d#" ];
    string_tunings = [ "e","a","d","g","b","e" ];


    constructor() {
      super();
    }

    //not sure why this is a getter setter situation
	setDataForScaleFullNeck(key,scl){
		this.neckData = this.getDataForScaleFullNeck(key,scl);
		return this.neckData;
	}

	//~  is this in the right place?
	getDataForScaleFullNeck(key,scl){
		// console.log(key,scl)
		// key is a string coresponding to the chromatcic scale e-d#
		// scale is a string of an array of tones and semitones "2,2,1,2,2,2,1",
		// returns the neckdata doesnt actually return notes, returns fretIDs
		//console.log(scl["structure"])
		if(scl["structure"] == undefined){
			throw new Error('cl[structure] is undefined.   Scale Object is required.   Not structure array')
		}
		var scale = scl["structure"]; // see below
		/*
				scale = {
					'name':"major",
					'structure':[ 2 , 2 , 1 , 2 , 2 , 2 , 1 ]
				}
		*/
		var strings = this.string_tunings;
		var notes = this.notes_in_chromatic_scale;
		var index_of_root_note = notes.indexOf( key )
		var t = [notes[index_of_root_note]];   // note of the index_of_root_note in the chromatic scale
		// console.log(t)
		var ind = 0;
		var scale_positions_flat = []
		var scale_positions_matrix = []
		var scale_notes_flat = []
		var scale_notes_matrix = [];
		//--
		// create an array of notes that exist in sthe scale.  this logic I think should be in caged.     
		for (var i = 0; i < scale.length; i++) {
			// index_of_root_note
			ind += parseInt(scale[i]);
			//console.log(ind)
			note = notes[caged.getNotePositionInChromaticScale(key,ind)];
			t.push(note)
		}
		//--
		var id;
		var h;
		var note;
		// 	iterate over strings and create the scale_positions_matrix for the entire neck.  This is ok to belong here.   
		for (var i = 0; i < strings.length; i++) {
			scale_positions_matrix[i] = []
			scale_notes_matrix[i] = []
			//iterate over frets
			for (var j = 0; j < 22; j++) {
				//set id = string_fret
				h = i < strings.length ? i : i - strings.length; //
				id = h+"_"+j;
				note = $("#fret_"+id).text();
				//t is a multidimensional array of notes.
				if(t.indexOf(note) >= 0){
					scale_positions_flat.push("#fret_"+id);
					scale_positions_matrix[i].push("#fret_"+id)
					scale_notes_flat.push(note)
					scale_notes_matrix[i].push(note)
				}
			}
		}
		// end "create the scale_positions_matrix"
		// return a flat array of fret positions from fret_5_0 to fret_0_20
		var obj = []
		obj['scale_positions_flat'] = scale_positions_flat;
		obj['scale_positions_matrix'] = scale_positions_matrix;
		obj['scale_notes_flat'] = scale_notes_flat;
		obj['scale_notes_matrix']= scale_notes_matrix;
		obj['neck_note_matrix'] = this.fullNeckNoteArray;
		// console.log(obj)
		return obj
	}

	isNoteInScale(thisFretsNote){
		// console.log(thisFretsNote)
		return this.neckData['scale_notes_flat'].includes(thisFretsNote)
	}

	getNoteAt(string, effectiveFret){
		return this.neckData['scale_notes_matrix'][string][effectiveFret]
	}
  
    
  }
  
  guitar_model = new GuitarModel() 