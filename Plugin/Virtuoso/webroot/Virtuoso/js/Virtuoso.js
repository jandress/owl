var gui = new GuitarUi()


gui.listenForEvent("key_link",function(ev){
    
	var e = ev[0];
	var scale = $( e.currentTarget ).attr("href").replace("#","")
	var scaleName = $( e.currentTarget ).html();
	// console.log(scale,scaleName)
	var id = $($( e.currentTarget ).parent().parent().parent()).attr("id")
	// console.log(id)
	var l = $("#"+id).children("button")
	$("#"+id).children("button").children(".label_span").html(scaleName)
},this)




// gui.listenForEvent("fooButton",function(){test()},this)
// gui.listenForEvent("toggleVis",function(){toggleVis()},this)


// function toggleVis(){
// 	switch($(e.currentTarget).text()){
// 		case("Hide Notes"):
// 		    guitar.hideAll();
// 		    $("#toggleVis").text("Show Notes")
// 		    guitar.hideAll();
// 		break;
// 		case("Show Notes"):
// 		    guitar.showCurrentNotes();
// 		    $("#toggleVis").text("Hide Notes")
// 		break;
// 		default:
// 	}
// }

function update(){
    guitar.showCurrentNotes()
    var b = guitar.scale;
    updateCaption(guitar.scale[''],guitar.scale['name'])
}
function updateCaption(h1,h2){
    var b = "";
    var c = "";
    if(guitar.key){
        b +=" "+capitalize(guitar.key);
    }
    if(guitar.scale){
         b += " "+capitalize(guitar.scale['name']);
    }
    var str = "<h1>"+b+"</h1>"
    if(guitar.drill){
        c = capitalize(guitar.drill['name'].toString());
        str += " &nbsp;&nbsp; <h2>&nbsp;"+c+"&nbsp;</h2>"
    }

    if(h2){
         str += " &nbsp;&nbsp; <h2>"+c+"</h2>"
    }
    $("#caption_holder").html(str)
}
function onSliderChange(e){
    var t = $(e.currentTarget).val() / 100;
    var min = 300;
    var max = 1000;
    var diff = max-min;
    var b = ((diff*t))+min;
    $("#sliderLabel").text( (t*100)+" "+b )
    guitar.interval = b;
}
function openDrawer(){
    $("#undernav_drawer").css({
        "top":"48px"
    });
    enableDrillControlls(true);
}
function enableDrillControls(bool){
    if(bool){
        $("#prev_btn").removeClass("disabled");
        $("#next_btn").removeClass("disabled");
        $("#play_pause_btn").removeClass("disabled");
    } else {
        $("#prev_btn").addClass("disabled");
        $("#next_btn").addClass("disabled");
        $("#play_pause_btn").addClass("disabled");
    }
    // $(".interval_dropdown").addClass("disabled");
    // $(".interval_link").addClass("disabled");
}
function setPlayPause(str){
    if(str != "play"){
        $("#pause_icon").removeClass("hidden")
        $("#play_icon").addClass("hidden")
    }else{
        $("#pause_icon").addClass("hidden")
        $("#play_icon").removeClass("hidden")
    }
}
