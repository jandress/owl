/*    

 Playable guitar makes calls on Guitar.js 


 OK, this is starting to make more sense.   This is an application layer simlar to a controller that sits between the UI and the model 
 to provide  popular functionality to different pages with different UI'.
 Trying to keep algorithmic logic out of this class. Interacting directly with the guitar elements preferably should be done in Guitar.js  

*/

class PlayableGuitar extends Guitar{

	interval = 5;
	intervalHandle = null;
	timeOutHandle;
	key_changed = "key_changed"; //used only in PlayableGuitar
	scale_changed = "scale_changed";


	constructor() {
		super();
		guitar_model.setDataForScaleFullNeck(this.key,this.scale)
	}

	showCurrentScaleCagedPortionAtFret(fret,hideOthers = false){
		if(hideOthers){
			this.hideAll();
		}
	    // guitar.showCagedPortionAtCurrentFretAndScale(val, guitar.major_scale)
	    var fret_id_array = caged.getCagedPositionArrayForFret(fret);
	    
		// this.playSequence(fret_id_array,this.interval);
		this.showSequence(fret_id_array);
		
	}

	playSequence( array,  time , clear, playSound = false){
		// alert("playSequence")
		// this.interval = setInterval(advance, time);
		this.interval = time;
		var scope = this;
		// console.log("time",time,scope.interval)
		var index = 0;
		//  this part here prevents multipliple caged portions from showing at the same time. 
		// also line 24 in this file, function: showCurrentScaleCagedPortionAtFret()   	
		if(!clear){
			// $(".note").removeClass("not_shown")
			// 
		}
		function advance(interval){
			//show this note
			var r = array[index];

			$(r).removeClass('reduced')

			// $(r).removeClass("not_shown")
			//  $(r).removeClass("reduced")


			//	showFretById(fretId, playSound=false, highlight = false, fadeOut = false, fadeTime = 1){
			scope.showFretById(r,playSound,true)
			//scope.showFretById(r,false,true)
			index++;
			if(index < array.length){
				scope.timeOutHandle = setTimeout(function(){advance(scope.interval)}, time);
			}
		}
		// setTimeout(advance, this.interval);
		advance()
	}




	showSequence( array, clear, playSound = false){
		// this.interval = setInterval(advance, time);
		var scope = this;
		for (let index = 0; index < array.length; index++) {
			var r = array[index];

			$(r).removeClass('reduced')
			scope.showFretById(r,playSound,false)
		}
	}




	
	
	playSoundForNote(string,fret){
		super.playSoundForNote(string,fret);
	}
	
	showCaged(position){
		console.log("showCaged: ",position)
		// alert("showCaged: "+position)
		if(position.toString().toLowerCase()=="all"){
			this.showCurrentNotes()
			return;
		}
		//convert to zero index.   not sure why 
		var pos = position-1;
		// returns a number  from 0 - 21 (not a fret id) for the lowest fret used on the e string. 
		var b = caged.getFretForPosition(pos,this.scale.name,this.key);

		this.showCurrentScaleCagedPortionAtFret(
			b, true
		)		
	}

	reduceAll(){
		$(".note").addClass( "reduced" );
		// // var ar = $(".note")[]
		// for(var i =0 ; i < ar.length; i++){
		// 	if($(ar[i]).hasClass("not_shown")
		// }
	}

	//~  is this in the right place? yes
	showScaleFullNeck(key,scaleObject){
		var ar = this.setDataForScaleFullNeck(key,scaleObject);
		this.showNotesInArray(ar['scale_positions_flat']);
	}
	//~  is this in the right place?
	getNotePositioninCurrentScale(key,steps){
		// there are 12 tones in the chromatic scale. this returns an index (0 - 11) of the note x steps from a given note in the chromatic scale.next_note_matrix
		// key is a string.
		// notes is a flat array from e to d#
		// strings is a flat array of strings "e a d g b e";
		var notes = guitar_model.neckData['scale_notes_flat'];
		var index_of_root_note = notes.indexOf(key)
		if(!steps){
			steps = 0
		}
		var b = (index_of_root_note+steps)%notes.length;
		//console.log("getNotePositionInChromaticScale: ", key, steps, b)
		return b;
	}
	//~  is this in the right place?
	getuniqueNotesForScale(scale){
		var ind = 0;
		var notes = this.notes_in_chromatic_scale;
		var key = scale.key;
		var note;
		console.log(": ",scale)
		var t = []
		for (var i = 0; i < scale.structure.length; i++) {
			// index_of_root_note
			ind += parseInt(scale.structure[i]);
			//console.log(ind)
			note = notes[caged.getNotePositionInChromaticScale(key,ind)];
			t.push(note)
		}		
		t.sort(function(x,y){ return x == key ? -1 : y == key ? 1 : 0; });
		return t;
	}
	//~  is this in the right place?
	setKey(str){
		this.visualizeSelectNotes("clear")
		var b = caged
		this.key = str.toLowerCase();

		this.visualizeSelectNotes("tonics")
		guitar_model.setDataForScaleFullNeck(this.key, this.scale)
		this.announceEvent(this.key_changed)
	}
	//~  is this in the right place?
	setScale(){
		var ar = arguments
		this.scale = caged.getScaleByName(arguments[0]);
		var scale = this.scale;
		///not sure why i did this with anonymous params
		var name, structure;
		if(typeof arguments[0] == "string"){
			name = arguments[0];
			structure = arguments[1];
			structure = scale.structure;
		} else {
			name = arguments[0]['name'];
			structure = arguments[0]['structure'];
		}
		guitar_model.setDataForScaleFullNeck(this.key, this.scale)
		this.announceEvent(this.key_changed)
	}
	//~  is this in the right place?
	compareScales(scaleObjectArray){
		var comparison_array = [ ];
		////convert and extract the scale_positions_flat from the array of scaleObjects
		for (var i = 0; i < scaleObjectArray.length; i++) {
				var b = this.getDataForScaleFullNeck(scaleObjectArray[i].key, scaleObjectArray[i])
				comparison_array.push(b.scale_positions_flat)
		}
		//refactor all this so that it is iterative or recursive to handle as many scales as possible.
		var ar1 = comparison_array[0]
		var ar2 = comparison_array[1]
		var uniques1 = []
		var uniques2 = []		
		// populate uniques1 array
		for (var i = 0; i < ar2.length; i++) {
		//	console.log(i, ar2[i], ar1.indexOf(ar2[i]))
			if(ar1.indexOf(ar2[i])==-1){
				uniques1.push(ar2[i])
			}
		}
		// populate uniques2 array
		for (var i = 0; i < ar1.length; i++) {
		//	console.log(i, ar1[i], ar2.indexOf(ar1[i]))
			if(ar2.indexOf(ar1[i])==-1){
				uniques2.push(ar1[i])
			}
		}
		//show common notes and mark as green
		this.hideAll();
		for (var i = 0; i < comparison_array[0].length; i++) {
			$(comparison_array[0][i]).removeClass("not_shown");
			$(comparison_array[0][i]).addClass("green");
		}
		//show common notes and mark as green
		for (var i = 0; i < comparison_array[1].length; i++) {
			$(comparison_array[1][i]).removeClass("not_shown");
			$(comparison_array[1][i]).addClass("green");
		}
		//
		for (var i = 0; i < uniques1.length; i++) {
			$(uniques1[i]).removeClass("green")
			$(uniques1[i]).removeClass("not_shown")
			$(uniques1[i]).addClass("blue")
		}
		for (var i = 0; i < uniques2.length; i++) {
			$(uniques2[i]).removeClass("green")
			$(uniques2[i]).removeClass("not_shown")
			$(uniques2[i]).addClass("yellow")
		}

	}
	//~  Should this be in Scale.js? It's only in comparison_selects. 
	createScaleObject(key,name,structure){
		var obj = {};
		obj.key = key;
		obj.structure = structure;
		var ind = 0;
		var scl = structure.length
		var index_of_root_note = this.notes_in_chromatic_scale.indexOf( key )
		var notes = [this.notes_in_chromatic_scale[index_of_root_note]];   // note of the index_of_root_note in the chromatic scale
		// obj.notes = 

		for (var i = 0; i < structure.length-1; i++) {
			// index_of_root_note
			ind += parseInt(structure[i]);
			//console.log(ind)
			var note = this.notes_in_chromatic_scale[caged.getNotePositionInChromaticScale(key,ind)];
			notes.push(note)
		}		
		obj.notes = notes;
		obj.hasNote = function (note){
			var n = 0;
			return obj.notes.includes(note);
		}
		return obj;
	}
	//~  is this in the right place?
	formatScaleObject(key, name, structure){
		this.scale = {
			'key' : key,
			'name' : name,
			'structure' : structure
		}
		return this.scale;
	}	
	
	
	
	showCurrentNotes(){
		this.hideAll();
		var nData = guitar_model.neckData['scale_positions_flat'];
		if(!nData){
			this.showAllNotes();
		}
		for (var m = 0; m < nData.length; m++) {
			$(nData[m]).removeClass("not_shown")
			$(nData[m]).removeClass("reduced")
		}
	}
}



var  guitar = new PlayableGuitar([1,2,3]);
guitar.listenForEvent(guitar.key_changed,function(){update()},this)
guitar.listenForEvent(guitar.scale_changed,function(){update()},this)
//guitar.listenForEvent(guitar.drill_changed,function(){updateCaption()},this)




