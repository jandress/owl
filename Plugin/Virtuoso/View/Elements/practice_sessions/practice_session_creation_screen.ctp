<div class = "container">
    <h2 style = " font-size:3em; letter-spacing:1px">Assemble Practice Session</h2>
    <table class = "tm_2_em table table-striped table-border table-hover">
      <thead>
        <tr style = "font-weight:bold; font-size:1.25em; color:#ececec">
            
            <td>Area</td>            
            <td >Name</td>
            <td style = "" class="text_right col-sm-4">Estimated Time</td>
            <td class="col-sm-2 text_right ">Actions</td>
        </tr>
      </thead>            
        <?php
		//	 debug($activities);
          foreach ($activities as $key => $val): 
            $value = $val['Practiceplan'];
            
          ?>
            <tr>
                <td>
                    <a class = "activity_link" href = "/my/activities/view/<?php echo $value['id']  ?>">
                        <?php
                        // debug($value);
                        echo $val['ParentPracticeplan']['name'];  ?>
                    </a>  
                </td>
                <td>
                    <a href = "/my/activities/view/<?php echo $value['id'] ?>">
                        <?php echo $value['name']; ?>
                    </a>
                </td>
                <td class="col-sm-4 text_right">
                  5:00
                </td>
                <td style = "text-align:right " class= "pr_0"> 






                <div class="col-sm-3 pull-right text_right pr_0">
                    <div class="dropdown pull-right lm_10_px rm_10_px">
                        <a class="dropdown-toggle btn btn-default btn-sm" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
                            Edit &nbsp;<b class="caret">&nbsp;</b>
                        </a>
                        <ul class="dropdown-menu " role="menu" aria-labelledby="dLabel">
                            <li>
                                <a href="/my/activities/edit/2/" class="  ">Swap For Another Activity</a>
                            </li>
                            <li>
                                <a href="/my/activities/edit/2/" class="  ">Remove Activity</a>
                            </li>              
                            <li>
                                <a href="/my/activities/edit/2/" class="  ">Change Time Alotment</a>
                            </li>                            
                        </ul>
                    </div>
                </div>



                </td>
          </tr>
          <?php endforeach; ?>
    </table>

    <div style = "padding:60px;" class = "col-sm-12">
        <a id = "get_started_button" class = "btn btn-block btn-success pull-right " style = "font-size:2em; font-weight:bold; letter-spacing:2px" >Get Started</a>
    </div>


    <div>

    </div>
</div>



