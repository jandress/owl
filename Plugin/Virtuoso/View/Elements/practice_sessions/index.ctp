<div class = "container  tm_2_em">
	<h2 class = "col-sm-6">My Practice Sessions</h2>
	
	<div class = " pull-right col-sm-6">
		<a href = "/my/caged/add" class = "pull-right btn btn-default">New Session Activity</a>
	</div>
	<div class = "bm_1_em clear clearfix" >&nbsp;</div>
</div>

<div class = "container  well">


<table class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<td>id</td>	
				<td>date</td>
				<td style = "text-align:right">completed</td>
			</tr>
		</thead>
		<?php  foreach ($data as $key => $value): ?>
			
			<tr>
				<td><a href = "/my/practicesessions/view/<?php echo $value['PracticeSession']['id'];?>"><?php echo $value['PracticeSession']['id'];?></a></td>		
				<td><a href = "/my/practicesessions/view/<?php echo $value['PracticeSession']['id'];?>"><?php echo $value['PracticeSession']['started'];?></a></td>		
				<td  style = "text-align:right"> 
					<?php if($value['PracticeSession']['completed'] == "1"): ?>
						<span class = "glyphicon glyphicon-ok">&nbsp;</span>
					<?php else: ?>
						<span class = "glyphicon glyphicon-remove">&nbsp;</span>
					<?php endif; ?>
				</td>
			</tr>
							
		<?php  endforeach; ?>
</table>

</div>



<?php
/*

'PracticeSession' => array(
		'id' => '1',
		'user_id' => '1',
		'started' => 'Sunday, December 10, 2023 18:17:33',
		'completed' => ''
	),


'PracticeSessionActivity' => array(
		'id' => '1',
		'user_id' => '1',
		'activity_id' => '5',
		'completed' => '0',
		'description' => 'Position 1.  Play Then reverse',
		'timestamp' => '2023-10-28 02:30:07.325714',
		'session_id' => '1'




	),



'id' => '1',
		'user_id' => '1',
		'started' => 'Sunday, December 10, 2023 18:17:33',
		'completed' => ''

<tr>
			<td><?php 	echo $value['PracticeSession']['id'];  ?></td>	
			<td><?php 	echo $value['PracticeSession']['description'];  ?></td>

			<td style = "text-align:center" ><?php 	echo $value['PracticeSession']['completed'];  ?></td>	
			<td style = "text-align:center" ><?php 	echo $value['PracticeSession']['session_id'];  ?></td>	
			<td  style = "text-align:right" ><?php 	echo $value['PracticeSession']['timestamp'];  ?></td>
		</tr>



*/
?>