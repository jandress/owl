<h1>Practice Session activities</h1>
<table class="tm_2_em table table-striped table-border table-hover">
      <thead>
        <tr style="font-weight:bold; font-size:1.25em; color:#ececec">            
            <td class="text_right col-sm-1">&nbsp;</td>            
            <td>Name</td>
            <td style="" class="text_right col-sm-2">Time Required</td>
            <td style="" class="text_right col-sm-2">Time Begun</td>
            <td style="" class="text_right col-sm-2">Time Last</td>
            <td class="col-sm-1 text_right ">Completed</td>
        </tr>   
      </thead>            
      <tbody>
        <?php
            $i = 0;
        foreach ($data as $key => $value):
                $i++;
                $psa = $value['PracticeSessionActivity'];
                $act = $value['Activity'];
                    //---
                $psaId = $psa['id'];
                $psaCompleted = $psa['completed'];
                $actId = $act['id'];
                $actName = $act['name'];        
                $style = $i < 3 ? "table-active" : "table-primary";
            ?>
            <tr class = "<?php echo $style; ?>">
                <td><?php echo $psaId; ?></td>
                <td> <a class = "activity_link" target = "_blank" href = "/my/psa/view/<?php echo $psaId; ?>/<?php echo $psId; ?>"><?php echo $actName; ?></a></td>
                <td class="text_right"><?php echo $value['Activity']['time_estimated'];//$date->format('H:i');?> minutes</td> 
                <td class="text_right"><?php echo $value['PracticeSessionActivity']['time_begun'];//$date->format('H:i');?></td> 
                <td class="text_right"><?php  echo $value['PracticeSessionActivity']['time_last']; ?></td>                                        
                <td class="text_center">
                    <?php if($psaCompleted): ?>
						<span class = "glyphicon glyphicon-ok">&nbsp;</span>
					<?php else: ?>
						<span class = "glyphicon glyphicon-remove">&nbsp;</span>
					<?php endif; ?>
                </td>        
            </tr>

        <?php   endforeach; ?>                        
      </tbody>
</table>





<div style = "padding:60px;" class = "col-sm-12">
    <a id = "get_started_button" class = "btn btn-block btn-success pull-right " style = "font-size:2em; font-weight:bold; letter-spacing:2px" >Get Started</a>
</div>








<?php


/*



    array(
        'PracticeSessionActivity' => array(
            'id' => '2',
            'user_id' => '1',
            'activity_id' => '9',
            'completed' => '0',
            'description' => 'interval child test',
            'timestamp' => '2023-10-28 02:30:07.326090',
            'session_id' => '1'
        ),
        'Activity' => array(
            'id' => '9',
            'name' => 'interval child test',
            'data' => '{
                "application": "interval",			
                "command": "#octave_to_octave/1s",
                "playerParameters": {
                        "autoPlay":true,
                        "handHoldingMode":false,
                        "key":"e",
                        "scale":"major"
                },
                


        "ui_instructions":{
            "messages" : [ 
                {
                    "text_elements":{
                        "h1":"Grab a guitar and play along!",
                        "h2":"E Major Position 1",
                        "h3":" Normal play through"


                    },
                    "timer":{
                        "duration":4,
                        "delay":0,
                        "fade":"out"
                    }								
                },
                {
                    "text_elements":{
                        "h1":"This is message 2",
                        "h2":"Which is the same as C# Minor"
                    },
                    "timer":{
                        "duration":4,
                        "delay":2				
                    }								
                }
            ]
        }
            }',
            'type' => 'caged',
            'parent_id' => '3',
            'lft' => '37',
            'rght' => '38',
            'taxonomy_only' => false
        )
    )



*/


?>