<div class="neck">
            <?php $margins = array(0,65,170,270,360,447, 530,609,682,750,813,875,933,985,1036,1084,1128,1168,1204,1240,1273,1305);?>
            <?php for ($i=5; $i >= 0; $i--) {
              ?>
              <div id = "string_<?php echo $i; ?>" class="string">
                <?php
                  for ($j=0; $j < count($margins) ; $j++) {
                              // for ($j=0; $j < 3 ; $j++) {
                    $id = $i."_".$j;
                    ?><div id ="fret_<?php echo $id; ?>" class="note not_shown  fret_position_<?php echo $j; ?>"><div class="note_text "></div><div class="note_highlight "></div></div>
                    <?php
                  } ?>
              </div>
              <?php
            } ?>
            <div class = "neck_buttons">
                <?php
                  for ($j=0; $j < count($margins) ; $j++) {
                    // for ($j=0; $j < 3 ; $j++) {
                        ?>
                        <a  class= "neck_btn fret_position_<?php echo $j; ?>" id ="neck_btn_<?php echo $j; ?>" href="">
                            <div class="neck_btn_graphic"><?php echo $j; ?></div>
                        </a>
                        <?php
                    }
                  ?>
            </div>
  </div>
