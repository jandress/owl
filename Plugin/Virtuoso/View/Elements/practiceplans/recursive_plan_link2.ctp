<?
	$nl = $data['nl'];                    
	$id = $data['id'];                    
	$level = $data['level'];
	$value = $data['Practiceplan'];
?>


<tr id = "SM<?php echo $id; ?>" class = "parent<?php echo $address; ?> level_<?php echo $level ?> sitemap_row">
	<td>
		<div class = "col-sm-6">
			

			<div class = "pt_6_px">
				<?php  echo $this->element('practiceplans/practiceplans_sitemap_ui/expand_btn',array('id'=>$id, 'nl'=>$nl,'value'=>$value,'level'=>$level)); ?>
				<h5 style = "float:left; margin-left:1em; margin-top:3px;">  
					<?php 
					
					// if(empty($value['taxonomy_only'])){
					 //	debug($value);
					// }
					if(!empty($value["Practiceplan"]['taxonomy_only'])): ?>
							<span class="gt_20"><?php echo $value["Practiceplan"]["id"] .'.&nbsp;&nbsp;'.$value["Practiceplan"]["name"] ?></span>
					<?php else: ?>
						<a href = "/my/activities/view/<?php echo $value['Practiceplan']["activity_id"] ?>/" target = "_blank">
							<?php echo $value["Practiceplan"]["id"] .'.&nbsp;&nbsp;'.$value["Practiceplan"]["name"] ?>
						</a>
					<?php endif; ?>
				</h5>
			</div>

			
		</div>

		<div class = "col-sm-3 pull-right">
			<?php echo $this->element('practiceplans/practiceplans_sitemap_ui/practiceplans_sitemap_edit_ui',array('id'=>$id, 'nl'=>$nl,'value'=>$value)); ?>			
		</div>
	</td>
</tr>

