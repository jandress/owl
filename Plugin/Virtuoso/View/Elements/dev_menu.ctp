<div class="tm_8_px ">


       <div class="btn-group">

          <div class="btn-group closeonclick" id="key_select_0">
            <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="label_span">Key</span>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                                <li><a class="key_link" href="#e">e</a></li>
                                <li><a class="key_link" href="#f">f</a></li>
                                <li><a class="key_link" href="#f#">f#</a></li>
                                <li><a class="key_link" href="#g">g</a></li>
                                <li><a class="key_link" href="#g#">g#</a></li>
                                <li><a class="key_link" href="#a">a</a></li>
                                <li><a class="key_link" href="#a#">a#</a></li>
                                <li><a class="key_link" href="#b">b</a></li>
                                <li><a class="key_link" href="#c">c</a></li>
                                <li><a class="key_link" href="#c#">c#</a></li>
                                <li><a class="key_link" href="#d">d</a></li>
                                <li><a class="key_link" href="#d#">d#</a></li>
                          </ul>
          </div> <!-- btn-group -->


          <div class="btn-group" id="scale_select_0">
            <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="label_span">Scale</span>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li><a class="scale_link" href="#2,2,1,2,2,2,1">Major</a></li>
              <li><a class="scale_link" href="#3,2,2,3,2">Pentatonic Minor</a></li>
              <li><a class="scale_link" href="#2,2,3,2,3">Pentatonic Major</a></li>
              <li><a class="scale_link" href="#2,3,2,3,2">Pentatonic Neutral</a></li>
              <li><a class="scale_link" href="#2,1,2,2,1,3,1">Harmonic Minor</a></li>
              <li><a class="scale_link" href="#2,1,2,2,2,2,1">Melodic Minor</a></li>
              <li><a class="scale_link" href="#2,1,2,2,1,2,2">Natural Minor</a></li>
            </ul> <!-- dropdown menu -->
          </div>
          <div class="btn-group"> <!-- btn-group2 -->
            <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
              CAGED <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li><a class="caged_link" href="#0">1</a></li>
              <li><a class="caged_link" href="#1">2</a></li>
              <li><a class="caged_link" href="#2">3</a></li>
              <li><a class="caged_link" href="#3">4</a></li>
              <li><a class="caged_link" href="#4">5</a></li>
              <li><a class="caged_link" href="#all">All</a></li>
            </ul>
          </div> <!-- btn-group 2 -->


          <div class="btn-group">
            <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
              Modes<span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li><a class="mode_link" href="Ionian">Ionian</a></li>
              <li><a class="mode_link" href="Dorian">Dorian</a></li>
              <li><a class="mode_link" href="Phrygian">Phrygian</a></li>
              <li><a class="mode_link" href="Lydian">Lydian</a></li>
              <li><a class="mode_link" href="Mixolydian">Mixolydian</a></li>
              <li><a class="mode_link" href="Aeolian">Aeolian</a></li>
              <li><a class="mode_link" href="Locrian">Locrian</a></li>
            </ul>
          </div><!-- btn-group -->




        </div><!-- btn-group -->





          <div class="btn-group">
            <a class="btn btn-default btn-sm" id="compare">Compare</a>
          </div><!-- btn-group -->






          <div class="btn-group">
            <a class="btn btn-danger btn-sm" id="foo">Foo</a>
          </div><!-- btn-group -->






          <div class="btn-group">
            <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
              Visualize<span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li><a class="visualize_link" href="all">All</a></li>
              <li><a class="visualize_link" href="tonics">Tonics</a></li>
              <li><a class="visualize_link" href="thirds">Thirds</a></li>
              <li><a class="visualize_link" href="fifths">Fifths</a></li>
              <li><a class="visualize_link" href="remove_highlights">Remove Highlights</a></li>
              <li><a class="visualize_link" href="clear">Clear</a></li>

            </ul>
          </div><!-- btn-group -->



          <div class="btn-group">
          <div class="dropdown">
              <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-default btn-sm" data-target="#" href="#" aria-expanded="false">
                        Drills <span class="caret"></span>
              </a>

              
              <ul id="drills_links" class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">                                       
                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Groups of four</a>
                  <ul class="dropdown-menu">
                    <li><a tabindex="-1" href="#1a">1a-- 1234, 2345, 3456, etc.</a></li>
                    <li><a href="#1b">1b-- 4321, 5432, 6543, etc.</a></li>
                    <li><a href="#1c">1c-- 1243, 2354, 3465, etc.</a></li>
                    <li><a href="#1d">1d-- 1321, 2432, 3543, etc.</a></li>
                  </ul>
                </li>

                <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Groups of three</a>
                  <ul class="dropdown-menu">
                    <li>
                      <a tabindex="-1" href="#2a">
                        2a-- 123, 234, 345, etc.
                      </a>
                  </li>
                    <li>
                      <a tabindex="-1" href="#2b">
                        2b-- 321, 432, 543, etc.
                      </a>
                  </li>
                    <li>
                      <a tabindex="-1" href="#2c">
                        2c-- 132, 243, 354, etc.
                      </a>
                  </li>
                    <li>
                      <a tabindex="-1" href="#2d">
                        2d-- 312, 423, 534, etc.
                      </a>
                  </li>                                                                  

                  </ul>
                </li>





                 <li class="dropdown-submenu">
                  <a tabindex="-1" href="#">Intervallic</a>
                  <ul class="dropdown-menu">
                    <li class="dropdown-submenu">
                      <a href="#">Thirds</a>
                      <ul class="dropdown-menu">  
                        <li><a href="#3a">3a-- 13, 24, 35, 46, etc.</a></li>
                        <li><a href="#3b">3b-- 31, 42, 53, 64, etc.</a></li>
                        <li><a href="#3c">3c-- 13, 42, 35, 64, etc.</a></li>
                      </ul>
                    </li>

                    <li class="dropdown-submenu">
                      <a href="#">Fourths</a>
                      <ul class="dropdown-menu">  
                        <li><a href="#4a">4a-- 14, 25, 36, 47, etc.</a></li>
                        <li><a href="#4b">4b-- 41, 52, 63, 74, etc.</a></li>
                        <li><a href="#4c">4c-- 14, 52, 36, 74, etc.</a></li>
                      </ul>
                    </li>
                    <li class="dropdown-submenu">
                      <a href="#">Fifths</a>
                      <ul class="dropdown-menu">  
                        <li><a href="#5a">5a-- 15, 26, 37, 48, etc.</a></li>
                        <li><a href="#5b">5b-- 51, 62, 73, 84, etc.</a></li>
                        <li><a href="#5c">5c-- 15, 62, 37, 84, etc.</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
              </ul>
          </div>    
        </div>

     











      