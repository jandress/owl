<div class = "clear clearfix">&nbsp;</div>
    <div class = " tm_1_em">
        <div class = "btn-group">
            <?php 
                echo $this->element("nav/individual_ui/key_select"); 
                // echo $this->element("nav/individual_ui/scale_select" );  
                echo $this->element("nav/individual_ui/caged_select2"); 
                echo $this->element("nav/individual_ui/modes_select"); 
    ?> </div>
    <div class = "btn-group pull-right col-sm-4 tm_h_em">
        <div style = "" class = "pull-right btn-group col- tm_h_em">
            <a class="back_btn btn btn-default btn-sm" >
                <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
            </a>
            <a class="play_btn btn btn-default btn-sm" >
                <span id = "modes_play_btn" class="glyphicon glyphicon-play" style = "display:inline" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-pause" style = "display:none" aria-hidden="true"></span>
            </a>
            <a class="forward_btn btn btn-default btn-sm" >
                <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
            </a>
        </div>        
    </div>        
</div>

