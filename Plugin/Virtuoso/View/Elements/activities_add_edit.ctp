<div class = "admin_padding">
	<h2>
		Editing Activity
	</h2>
	<?php 
			
		echo $this->BootstrapForm->create('Activity', array('class' => 'form-horizontal'));
		echo $this->BootstrapForm->input('taxonomy_only');
		echo $this->BootstrapForm->select('Activity.parent_id',array('options'=>$activity_options)   );

		// echo $this->Form->input('parent_id');

		echo $this->BootstrapForm->hidden('id');
		echo $this->BootstrapForm->input('name', array(
			'required' => 'required'	
			,'class'=>'tm_2_em col-sm-8'
			)
		);


	



?> 

<div class="clear clearfix">&nbsp;</div>



<div class="control-group">
    <label for="ActivityParentId" class="control-label">Template</label>
    <div class="controls">
		<?php echo $this->BootstrapForm->select('json_template', array('options'=>$json_template_options)); ?>
    </div>
</div>



<?php
		if(empty($this->data['Activity']['data'])){
			// debug($this->data['Activity']['data']); 
			$this->data = array();
			echo $this->BootstrapForm->TextArea('data', array(
				'required' => 'required'	
				,'class'=>'tm_2_em col-sm-8'
				,'style'=>"height:400px;"
				,'value'=>$default
				)
			);
		 }else{
			echo $this->BootstrapForm->TextArea('data', array(
				'required' => 'required'	
				,'class'=>'tm_2_em col-sm-8'
				,'style'=>"height:400px;"
				)
			);
		 }
		
		?>
		<div class ="clear clearfix tm_2_em">&nbsp;</div>
		<div class ="clear clearfix tm_2_em">&nbsp;</div>
		<?php
		echo $this->Form->end(__('Submit'));	
	?>
</div>




<?php  echo $this->Html->script(
            '/Virtuoso/Virtuoso/js/activity_edit',
            array('block'=>"bottom_script")
         ); 
?>
    
    