<div style="margin-top:-5px" class="relocation_buttons btn-group btn-group-vertical pull-right rm_4_px">
	<a href="/my/activities/relocate/<?php echo $nl["id"] ?>/up/" class="btn btn-xs btn-default  relocation_arrows up-btn">
		<span class="glyphicon glyphicon-arrow-up"></span>
	</a>

	<a href="/my/activities/relocate/<?php echo $nl["id"] ?>/down/" class=" btn btn-xs  btn-default relocation_arrows glyphicon down-button">
		<span class="glyphicon glyphicon-arrow-down"></span>
	</a>
</div>


<div class="dropdown pull-right lm_10_px rm_10_px">

		  <a class="dropdown-toggle btn btn-default btn-sm" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
		   Edit
		    <b class="caret"></b>
		  </a>
		<?php			$childrenCount = count($value["ChildActivity"]);				?>
		  <ul class="dropdown-menu " role="menu" aria-labelledby="dLabel">
			<li>
				<?php if (strtolower($nl["class"])!="url"): ?>
						<a href="/my/<?php echo strtolower(Inflector::pluralize($nl["class"])) ?>/edit/<?php echo $nl["foreign_id"] ?>/" class = "">Edit Content</a>
				<?php endif ?>
			</li>
			<li>
				<a href="/my/activities/edit/<?php echo $nl["id"] ?>/" class = "  ">Edit Link</a>
			</li>
			<li>
<?php echo $this->Form->postLink(__("Delete This Link Only"),"/my/activities/remove/".$nl["id"],array("data"=>array("id"=>$nl["id"])),__("Are you sure you want to delete this link? ".  $childrenCount." children will be moved up one level. This could make your sitemap messy if you don\"t know what to expect.")); ?>
			</li>
			<li>
					<?php
						$childrenCount= count($value["ChildActivity"]);
						echo $this->Form->postLink(__("Delete Link and Children"),"/my/activities/delete/".$nl["id"],array("data"=>array("id"=>$nl["id"])),__("Are you sure you want to delete this link? It has ".$childrenCount." children that will also be deleted."));
					?>
			</li>
		  </ul>
</div>
