<div style="margin-top:-5px" class="relocation_buttons btn-group btn-group-vertical pull-right rm_4_px">
	<a href="/my/activities/relocate/<?php echo $nl["id"] ?>/up/" class="btn btn-xs btn-default  relocation_arrows up-btn">
		<span class="glyphicon glyphicon-arrow-up"></span>
	</a>

	<a href="/my/activities/relocate/<?php echo $nl["id"] ?>/down/" class=" btn btn-xs  btn-default relocation_arrows glyphicon down-button">
		<span class="glyphicon glyphicon-arrow-down"></span>
	</a>
</div>


<div class="dropdown pull-right lm_10_px rm_10_px">

		  <a class="dropdown-toggle btn btn-default btn-sm" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
		   Edit
		    <b class="caret"></b>
		  </a>
		<?php
		if(!empty($value["ChildActivity"])){
			
			$childrenCount = count($value["ChildActivity"]);							
		}else{
			$childrenCount = 0;							
		}	
		?>
		  <ul class="dropdown-menu " role="menu" aria-labelledby="dLabel">
			<li>
				<a href="/my/activities/edit/<?php echo $nl["id"] ?>/" class = "  ">Edit Activity</a>
			</li>
			<li>
				<?php echo $this->Form->postLink(__("Delete Activity"),"/my/activities/delete/".$nl["id"],array("data"=>array("id"=>$nl["id"])),__("Are you sure you want to delete this Activty?")); ?>
			</li>
		  </ul>
</div>
