<div class = "clear clearfix">&nbsp;</div>
    <div class = " tm_1_em">
        <div class = "btn-group">
            <?php
                // echo $this->element("nav/nc"); 
                echo $this->element("nav/individual_ui/key_select"); 

                echo $this->element("nav/individual_ui/scale_select" ); 
                echo $this->element("nav/individual_ui/drills_select"); 
                echo $this->element("nav/individual_ui/caged_select"); 
                echo $this->element("nav/individual_ui/caged_player_mode_select"); 
    ?> </div>
    <div class = "btn-group">
        <?php
            // echo $this->element("nav/nc"); 
            echo $this->element("nav/individual_ui/vcr_controls"); 
            // echo $this->element("nav/individual_ui/timer_slider"); 
        ?>
    </div>
    <div class = "btn-group">
            <a class=" btn btn-default btn-sm" href = "<?php echo $prev; ?>">
                <span class="glyphicon glyphicon glyphicon-step-backward" aria-hidden="true"></span>
            </a>
            <a class=" btn btn-default btn-sm"  href = "<?php echo $next; ?>">
                <span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span>
            </a>
    </div>    
    <div style = "" class = "pull-right btn-group col-sm-4 tm_h_em">
        <?php echo $this->element("nav/individual_ui/timer_slider"); ?>
    </div>        
    <div class = "btn-group pull-right col-sm-4 tm_h_em">
    </div>        
</div>

