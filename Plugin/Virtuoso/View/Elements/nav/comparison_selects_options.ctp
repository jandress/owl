<li><a class = "compare_scale_link" href="#2,2,1,2,2,2,1">Major</a></li>
<li><a class = "compare_scale_link"  href="#3,2,2,3,2">Pentatonic Minor</a></li>
<li><a class = "compare_scale_link"  href="#2,2,3,2,3">Pentatonic Major</a></li>
<li><a class = "compare_scale_link"  href="#2,3,2,3,2">Pentatonic Neutral</a></li>
<li><a class = "compare_scale_link"  href="#2,1,2,2,1,3,1">Harmonic Minor</a></li>
<li><a class = "compare_scale_link"  href="#2,1,2,2,2,2,1">Melodic Minor</a></li>
<li><a class = "compare_scale_link"  href="#2,1,2,2,1,2,2">Natural Minor</a></li>



<?php foreach ($scaleList as $key => $value): ?>
    <li><a class="compare_scale_link" href="#<?php echo implode($value['structure'],",") ?>"><?php echo $value['name']; ?></a></li>
<?php endforeach; ?>




