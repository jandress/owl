<div class="container-fluid tm_5_em">
    <div class="col-md-4">
      <div class="btn-group">
        <div class="btn-group closeonclick"   id="key_select_0">
          <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <span class="key_span" >Key</span>


            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
              <li><a class="compare_key_link" href="#e">e</a></li>
              <li><a class="compare_key_link" href="#f">f</a></li>
              <li><a class="compare_key_link" href="#f#">f#</a></li>
              <li><a class="compare_key_link" href="#g">g</a></li>
              <li><a class="compare_key_link" href="#g#">g#</a></li>
              <li><a class="compare_key_link" href="#a">a</a></li>
              <li><a class="compare_key_link" href="#a#">a#</a></li>
              <li><a class="compare_key_link" href="#b">b</a></li>
              <li><a class="compare_key_link" href="#c">c</a></li>
              <li><a class="compare_key_link" href="#c#">c#</a></li>
              <li><a class="compare_key_link" href="#d">d</a></li>
              <li><a class="compare_key_link" href="#d#">d#</a></li>
          </ul>
        </div> <!-- btn-group -->
        <div class="btn-group" id="scale_select_0">
          <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
            <span class="scale_span" >Scale</span>
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <?php echo $this->element('nav/comparison_selects_options'); ?>
          </ul> <!-- dropdown menu -->
        </div>
      </div>
    </div>



    <div class="col-md-4">
      <h2>Compared to</h2>
    </div>





    <div class="col-md-4">
      <div class="btn-group">
        <div id="key_select_1" class="btn-group closeonclick">
          <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <span class="key_span" >Key</span>
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
              <li><a class="compare_key_link" href="#e">e</a></li>
              <li><a class="compare_key_link" href="#f">f</a></li>
              <li><a class="compare_key_link" href="#f#">f#</a></li>
              <li><a class="compare_key_link" href="#g">g</a></li>
              <li><a class="compare_key_link" href="#g#">g#</a></li>
              <li><a class="compare_key_link" href="#a">a</a></li>
              <li><a class="compare_key_link" href="#a#">a#</a></li>
              <li><a class="compare_key_link" href="#b">b</a></li>
              <li><a class="compare_key_link" href="#c">c</a></li>
              <li><a class="compare_key_link" href="#c#">c#</a></li>
              <li><a class="compare_key_link" href="#d">d</a></li>
              <li><a class="compare_key_link" href="#d#">d#</a></li>
          </ul>
        </div> <!-- btn-group -->
        <div class="btn-group"  id="scale_select_1" >
          <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
            <span class="scale_span" >Scale</span>
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <?php echo $this->element('nav/comparison_selects_options'); ?>
          </ul> <!-- dropdown menu -->
        </div>
      </div>
    </div>



<div class="clear clearfix">&nbsp;</div>

<div class="tm_3_em" style="padding-left:80px; padding-right:80px">
  <a id="compare_submit" class="btn btn-success btn-block" href="#">Submit</a>
</div>




</div>



<script type="text/javascript">

  function hideModal(){
    $("#modal_container").addClass("hidden");
  }
  function showModal(){
    $("#modal_container").removeClass("hidden");
  }
  $("#modal_close_btn").click(function(e){
    // $($( e.currentTarget ).parent().parent()).remove();
    hideModal()
  });

  
  
  
  
  
  
  
  
  
  var key_1;
  var key_2;
  var scale_1_name;
  var scale_2_name;
  var scale_1_structure;
  var scale_2_structure;  

  $(".compare_key_link").click(function(e){
    var key = $( e.currentTarget ).html()
    var id = $($( e.currentTarget ).parent().parent().parent()).attr("id")
    $("#"+id).children("button").children(".key_span").html(key)
    if(id.replace("key_select_","")==0){
      key_1 = key;
    } else {
      key_2 = key;
    }
  })


  $(".compare_scale_link").click(function(e){
    var scaleStructure = $( e.currentTarget ).attr("href").replace("#","").split(",");
    var scaleName = $( e.currentTarget ).html();

    /// make the value of the scale name visible in the select
    var id = $($( e.currentTarget ).parent().parent().parent()).attr("id")
    $("#"+id).children("button").children(".scale_span").html(scaleName)
    ///
  
    //this part should be a loop / array situation.
    if(id.replace("scale_select_","") == 0){
      scale_1_structure = scaleStructure;
      scale_1_name = scaleName;  
    } else {
      scale_2_structure = scaleStructure;
      scale_2_name = scaleName;
    }
  });


  function returnUniques(ar1, ar2){
    var arr1 = []
    var arr2 = []
    for(var i = 0; i < ar1.length; i++){
      for(var p = 0; p < ar2.length; p++){
        if(ar1.indexOf(ar2[p])==-1){
          console.log("ar2[p]: ",ar2[p])
          if(arr2.indexOf(ar2[p])==-1){
            arr2.push(ar2[p])
          }
        }
      }
      if(ar2.indexOf(ar1[i])==-1){
          console.log("ari[i]: ",ar1[i])
          arr1.push(ar1[i])
        }
    }
    var a = [arr1, arr2]
    return a;
  }





  function assembleUniquesFormatting(ar, uniques){
    var str = "";
    for(var i = 0; i < ar.length; i++){
      // console.table(uniques)
      if(uniques.indexOf(ar[i]) >= 0 ){
        str +="<span class = 'underlined'>"+ar[i].toUpperCase()+"</span>&nbsp;";
      }else{
        str +=""+ar[i].toUpperCase()+"&nbsp;";
      }
    }
    return str
  }


  $("#compare_submit").click(function(e){
      // console.log( key_2, scale_1, scale_2)
      
      // key_select_
      
      // var scale_1 = guitar.createScaleObject(key_1,scale_1_name,scale_1_structure)
      // var scale_2 = guitar.createScaleObject(key_2,scale_2_name,scale_2_structure)
    
      // // the name of this method is confusing because its not getting uniques in this context.
      // var scale_1_notes =  guitar.getuniqueNotesForScale(scale_1);
      // var scale_2_notes =  guitar.getuniqueNotesForScale(scale_2);      

      // var p = returnUniques(scale_1_notes, scale_2_notes)
      // console.log(scale_1_notes,scale_2_notes,p)

      // scale_1_notes = assembleUniquesFormatting(scale_1_notes, p[0])
      // scale_2_notes = assembleUniquesFormatting(scale_2_notes, p[1]);

      // var str1 = "<h1 class='yellow_text'>"+key_1.toUpperCase()+" "+scale_1_name.toUpperCase()+" </h1><h2> compared to </h2>&nbsp;<h1  class='blue_text' >"+key_2+" "+scale_2_name+"</h1><h2> with </h2><h1  class='green_text'> common notes</h1>";
      // var str2 = "<h3 style = 'margin-top:0px;'><span class='yellow_text'> "+scale_1_notes+"&nbsp; </span><span class='blue_text'>"+scale_2_notes+"</span></h3>"
      // var str = str1 + str2;
      // $("#caption_holder").html(str);
      // //  console.log("structure: ", key_1,  scale_1.structure)
      // guitar.compareScales([ scale_1, scale_2 ])
      // hideModal()
      window.location = "/tools/comparison/"+key_1+"/"+scale_1_name+"/"+key_2+"/"+scale_2_name+"/";
  });
</script>
