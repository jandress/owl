

<div class="btn-group">
          <div class="dropdown">
              <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-default btn-sm" data-target="#" href="#" aria-expanded="false">
                        Drills <span class="caret"></span>
              </a>
              <ul id="drills_links" class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">                                       
                <li class="dropdown-submenu">
                  <a class = "drills_link"  tabindex="-1" href="#">Groups of four</a>
                  <ul class="dropdown-menu">
                    <li><a class = "drills_link" tabindex="-1" href="#1a">1a-- 1234, 2345, 3456, etc.</a></li>
                    <li><a class = "drills_link"  href="#1b">1b-- 4321, 5432, 6543, etc.</a></li>
                    <li><a class = "drills_link"  href="#1c">1c-- 1243, 2354, 3465, etc.</a></li>
                    <li><a class = "drills_link"  href="#1d">1d-- 1321, 2432, 3543, etc.</a></li>
                  </ul>
                </li>

                <li class="dropdown-submenu">
                  <a class = "drills_link"  tabindex="-1" href="#">Groups of three</a>
                  <ul class="dropdown-menu">
                    <li>
                      <a class = "drills_link"  tabindex="-1" href="#2a">
                        2a-- 123, 234, 345, etc.
                      </a>
                  </li>
                    <li>
                      <a class = "drills_link"  tabindex="-1" href="#2b">
                        2b-- 321, 432, 543, etc.
                      </a>
                  </li>
                    <li>
                      <a class = "drills_link"  tabindex="-1" href="#2c">
                        2c-- 132, 243, 354, etc.
                      </a>
                  </li>
                    <li>
                      <a class = "drills_link"  tabindex="-1" href="#2d">
                        2d-- 312, 423, 534, etc.
                      </a>
                  </li>                                                                  

                  </ul>
                </li>

                 <li class="dropdown-submenu">
                  <a class = "drills_link"  tabindex="-1" href="#">Intervallic</a>
                  <ul class="dropdown-menu">
                    <li class="dropdown-submenu">
                      <a class = "drills_link"  href="#">Thirds</a>
                      <ul class="dropdown-menu">  
                        <li><a class = "drills_link"  href="#3a">3a-- 13, 24, 35, 46, etc.</a></li>
                        <li><a class = "drills_link"  href="#3b">3b-- 31, 42, 53, 64, etc.</a></li>
                        <li><a class = "drills_link"  href="#3c">3c-- 13, 42, 35, 64, etc.</a></li>
                      </ul>
                    </li>

                    <li class="dropdown-submenu">
                      <a class = "drills_link"  href="#">Fourths</a>
                      <ul class="dropdown-menu">  
                        <li><a class = "drills_link"  href="#4a">4a-- 14, 25, 36, 47, etc.</a></li>
                        <li><a class = "drills_link"  href="#4b">4b-- 41, 52, 63, 74, etc.</a></li>
                        <li><a class = "drills_link"  href="#4c">4c-- 14, 52, 36, 74, etc.</a></li>
                      </ul>
                    </li>
                    <li class="dropdown-submenu">
                      <a class = "drills_link"  href="#">Fifths</a>
                      <ul class="dropdown-menu">  
                        <li><a class = "drills_link"  href="#5a">5a-- 15, 26, 37, 48, etc.</a></li>
                        <li><a class = "drills_link"  href="#5b">5b-- 51, 62, 73, 84, etc.</a></li>
                        <li><a class = "drills_link"  href="#5c">5c-- 15, 62, 37, 84, etc.</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
              </ul>
          </div>    
        </div>