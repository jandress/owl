
<div class="btn-group">
    <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
        Modes<span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><a class = "mode_link" href="Ionian">Ionian</a></li>
        <li><a class = "mode_link" href="Dorian">Dorian</a></li>
        <li><a class = "mode_link" href="Phrygian">Phrygian</a></li>
        <li><a class = "mode_link" href="Lydian">Lydian</a></li>
        <li><a class = "mode_link" href="Mixolydian">Mixolydian</a></li>
        <li><a class = "mode_link" href="Aeolian">Aeolian</a></li>
        <li><a class = "mode_link" href="Locrian">Locrian</a></li>
    </ul>
</div><!-- btn-group -->
