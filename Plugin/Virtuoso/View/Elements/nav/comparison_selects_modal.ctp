<div id="modal_container" class="hidden" style="position: fixed; z-index: 2000; padding: 20px; width:100%; height:100%; background-color:rgba(0, 0, 0, 0.75);">
  <div style="width:1000px; height:400px; border-width: 1px;  border-color:#555555; border-style: solid; border-radius: 15px; margin:auto; background-color:#000000; ">
    <a id="modal_close_btn" class="btn-danger btn pull-right">
       X
    </a>
    <div style="text-align:center" class="tm_2_em">
      <h1>Scale Comparison</h1>

      <?php echo $this->element('nav/comparison_selects'); ?>

    </div>
  </div>
</div>
