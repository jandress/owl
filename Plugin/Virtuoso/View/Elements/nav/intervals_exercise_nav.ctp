<div class = "col-sm-6 tm_1_em">
    <div class = "btn-group ">
            <?php 
                echo $this->element('nav/individual_ui/key_select');
                echo $this->element('nav/individual_ui/scale_select');   
                echo $this->element('nav/individual_ui/play_btn');
                // echo $this->element('nav/individual_ui/foo');            
            ?>
        </div>

        <div class="btn-group">
            <input type="checkbox" name="randomKey" id="randomKey" value="0">
            <label for="randomKey" class="control-label">Randomize Key</label>
        </div>
        <div class="btn-group">
            <input type="checkbox" name="randomScale" id="randomScale" value="0">
            <label for="randomScale" class="control-label">Randomize Scale</label>
        </div>
</div>

<div class = "col-sm-6 tm_1_em">

<div class = "btn-group pull-right">
    <a class=" btn btn-default btn-sm" href = "<?php echo $prev; ?>">
        <span class="glyphicon glyphicon glyphicon-step-backward" aria-hidden="true"></span>
    </a>
    <a class="btn btn-default btn-sm"  href = "<?php echo $next; ?>">
        <span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span>
    </a>
</div>    

</div>
<?php 
//debug($prev);
// debug($next); 
?>