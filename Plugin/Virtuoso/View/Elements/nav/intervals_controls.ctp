<div class="tm_2_em">
        <div class=" col-sm-6">
                <?php echo $this->element('nav/individual_ui/string_select'); ?>
                <?php echo $this->element('nav/individual_ui/interval_select'); ?>
                <?php echo $this->element('nav/individual_ui/key_select'); ?>
                <a class="btn btn-danger btn-sm foo" >Foo</a>
                <?php echo $this->element('nav/individual_ui/vcr_controls'); ?>

        </div>
        <div class="col-sm-6">
                <?php echo $this->element('nav/individual_ui/timer_slider'); ?>
        </div>

</div>
<!-- https://getbootstrap.com/docs/3.3/components/#top -->