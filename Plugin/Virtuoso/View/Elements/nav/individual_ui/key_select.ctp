<div class="btn-group">
    <div class="btn-group closeonclick" id="key_select_0">
    <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
        <span class="key_select_label label_span">Key</span>
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><a class="key_link" href="#e">e</a></li>
        <li><a class="key_link" href="#f">f</a></li>
        <li><a class="key_link" href="#f#">f#</a></li>
        <li><a class="key_link" href="#g">g</a></li>
        <li><a class="key_link" href="#g#">g#</a></li>
        <li><a class="key_link" href="#a">a</a></li>
        <li><a class="key_link" href="#a#">a#</a></li>
        <li><a class="key_link" href="#b">b</a></li>
        <li><a class="key_link" href="#c">c</a></li>
        <li><a class="key_link" href="#c#">c#</a></li>
        <li><a class="key_link" href="#d">d</a></li>
        <li><a class="key_link" href="#d#">d#</a></li>
    </ul>
    </div>
</div> <!-- btn-group -->

