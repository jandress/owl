<div class="btn-group"> <!-- btn-group2 -->
    <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
            <span class="player_mode_label">Player Mode</span>
             <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><a class="caged_player_mode_link" href="#1">Play then Repeat (Loop)</a></li>
        <li><a class="caged_player_mode_link" href="#2">Play Then Reverse</a></li>
        <li><a class="caged_player_mode_link" href="#3">Play then Stop</a></li>    
    </ul>
</div>

