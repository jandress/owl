<div class="btn-group" id="scale_select_0">
    <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
        <span class="label_span">Mode</span>
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><a class="modes_link primary" href="#null">pentatonic major</a></li>
        <li><a class="modes_link primary" href="#null">pentatonic minor</a></li>
        <li><a class="modes_link primary" href="#2,2,2,1,2,2,1">lydian</a></li>
        <li><a class="modes_link" href="#2,2,1,2,2,2,1">ionian</a></li>
        <li><a class="modes_link" href="#2,2,1,2,2,1,2">mixolydian</a></li>
        <li><a class="modes_link" href="#2,1,2,2,2,1,2">dorian</a></li>
        <li><a class="modes_link" href="#2,1,2,2,1,2,2">aeolian</a></li>
        <li><a class="modes_link" href="#1,2,2,2,1,2,2">phrygian</a></li>
        <!-- <a class="modes_link" href="#1,2,2,1,2,2,2">locrian</a></li> -->
    </ul> <!-- dropdown menu -->
</div>