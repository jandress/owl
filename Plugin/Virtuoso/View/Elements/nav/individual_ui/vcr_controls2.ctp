        <div class = "btn-group vcr_controls">

            <!-- <a class="back_btn btn btn-default btn-sm" >
                <span class="glyphicon glyphicon glyphicon-fast-backward" aria-hidden="true"></span>
            </a> -->
            <a class="step-backward btn btn-default btn-sm" >
                <span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span>
            </a>

            <a class="back_btn btn btn-default btn-sm" >
                <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
            </a>

            <?php 
                echo $this->element('nav/individual_ui/play_btn');
            ?>
            <a class="stop_btn btn btn-default btn-sm" >
                <span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
            </a>
            <a class="forward btn btn-default btn-sm" >
                <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
            </a>

            <a class="step-forward btn btn-default btn-sm" >
                <span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span>
            </a>

            <!-- <a class="forward btn btn-default btn-sm" >
                <span class="glyphicon glyphicon-fast-forward" aria-hidden="true"></span>
            </a> -->
        </div>