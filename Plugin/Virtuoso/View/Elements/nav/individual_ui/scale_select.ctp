<div class="btn-group" id="scale_select_0">
    <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
        <span class="scale_select_label label_span">Scale</span>
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <?php
            foreach ($scaleList as $key => $value): ?>
                <li>
                    <a class="scale_link" href="#<?php echo implode($value['structure']) ?>">
                        <?php echo $value['name']; ?>
                    </a>
                </li>
        <?php endforeach; ?>
        <li><a class="scale_link" href="#2,2,1,2,2,2,1">Major</a></li>
        <li><a class="scale_link" href="#3,2,2,3,2">Pentatonic Minor</a></li>
        <li><a class="scale_link" href="#2,2,3,2,3">Pentatonic Major</a></li>
        <li><a class="scale_link" href="#2,3,2,3,2">Pentatonic Neutral</a></li>
        <li><a class="scale_link" href="#2,1,2,2,1,3,1">Harmonic Minor</a></li>
        <li><a class="scale_link" href="#2,1,2,2,2,2,1">Melodic Minor</a></li>
        <li><a class="scale_link" href="#2,1,2,2,1,2,2">Natural Minor</a></li>
        <li><a class="scale_link" href="#1,1,1,1,1,1,1,1,1,1,1,1,1,1,1">Chromatic</a></li>
    </ul> <!-- dropdown menu -->
</div> <!-- btn-group 2 -->