<a class="play_pause btn btn-default btn-sm" <?php 
if(isset($tooltip)){
    echo 'data-toggle="tooltip" data-placement="top" title="'.$tooltip.'"';
}
?> >
    <span class="glyphicon glyphicon-play" aria-hidden="true"></span>
    <span class="glyphicon glyphicon-pause not_shown" aria-hidden="true"></span>
</a>