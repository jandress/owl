<div class="btn-group"> <!-- btn-group2 -->
    <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
            <span class="position_label">Position</span>
             <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">

        <li><a class="caged_link" href="#0">1</a></li>
        <li><a class="caged_link" href="#1">2</a></li>
        <li><a class="caged_link" href="#2">3</a></li>
        <li><a class="caged_link" href="#3">4</a></li>
        <li><a class="caged_link" href="#4">5</a></li>
        <li><a class="caged_link" href="#0+">1 (+ 1 octave)</a></li>
        <li><a class="caged_link" href="#1+">2 (+ 1 octave)</a></li>
        <li><a class="caged_link" href="#2+">3 (+ 1 octave)</a></li>
        <li><a class="caged_link" href="#3+">4 (+ 1 octave)</a></li>
        <li><a class="caged_link" href="#4+">5 (+ 1 octave)</a></li>        
        <li><a class="caged_link" href="#all">All</a></li>
    </ul>
</div>