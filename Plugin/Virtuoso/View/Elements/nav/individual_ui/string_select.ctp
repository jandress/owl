<div class="btn-group ">
    <div class="btn-group closeonclick" id="string_select">
        <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
            <span class="string_select_label label_span">String</span>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li><a class="string_select_btn" href="#0">e</a></li>
            <li><a class="string_select_btn" href="#1">a</a></li>
            <li><a class="string_select_btn" href="#2">d</a></li>
            <li><a class="string_select_btn" href="#3">g</a></li>
            <li><a class="string_select_btn" href="#4">b</a></li>
            <li><a class="string_select_btn" href="#5">e</a></li>
        </ul>
    </div> 
</div><!-- btn-group -->

