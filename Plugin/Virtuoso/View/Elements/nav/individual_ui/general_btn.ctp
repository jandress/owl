<a class="<?php echo $type;?>_btn btn btn-default btn-sm" <?php 
if(isset($tooltip)){
    echo 'data-toggle="tooltip" data-placement="top" title="'.$tooltip.'"';
}
?> >
    <span class="glyphicon glyphicon-<?php echo $type;?>" aria-hidden="true"></span>
</a>