<div class ="tm_1_em pl_1_em pr_1_em">
    <form>
       <div  class="btn-group">
          <div class="btn-group closeonclick" id="key_select_0">
            <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="label_span" >Key</span>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <?php
              $keys = array("e","f","f#","g","g#","a","a#","b","c","c#","d","d#" );
              foreach ($keys as $key => $value): ?>
                  <li><a class = "key_link" href="#<?php echo $value; ?>"><?php echo $value; ?></a></li>
              <?php endforeach ?>
            </ul>
          </div> <!-- btn-group -->


          <div class="btn-group"  id="scale_select_0" >
            <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="label_span" >Scale</span>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li><a class = "scale_link" href="#2,2,1,2,2,2,1">Major</a></li>
              <li><a class = "scale_link"  href="#3,2,2,3,2">Pentatonic Minor</a></li>
              <li><a class = "scale_link"  href="#2,2,3,2,3">Pentatonic Major</a></li>
              <li><a class = "scale_link"  href="#2,3,2,3,2">Pentatonic Neutral</a></li>
              <li><a class = "scale_link"  href="#2,1,2,2,1,3,1">Harmonic Minor</a></li>
              <li><a class = "scale_link"  href="#2,1,2,2,2,2,1">Melodic Minor</a></li>
              <li><a class = "scale_link"  href="#2,1,2,2,1,2,2">Natural Minor</a></li>
            </ul> <!-- dropdown menu -->
          </div>
          <div class="btn-group"> <!-- btn-group2 -->
            <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
              CAGED <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li><a class = "caged_link" href="#0">1</a></li>
              <li><a class = "caged_link"  href="#1">2</a></li>
              <li><a class = "caged_link"  href="#2">3</a></li>
              <li><a class = "caged_link"  href="#3">4</a></li>
              <li><a class = "caged_link"  href="#4">5</a></li>
              <li><a class = "caged_link"  href="#all">All</a></li>
            </ul>
          </div> <!-- btn-group 2 -->
        </div><!-- btn-group -->


          <div class = "btn-group">
            <a class = "btn btn-default btn-sm" id="fooButton">Compare</a>
          </div><!-- btn-group -->


          <div class="btn-group">
            <button type="button" class="btn  btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
              Visualize<span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li><a class = "visualize_link" href="all">All</a></li>
              <li><a class = "visualize_link" href="tonics">Tonics</a></li>
              <li><a class = "visualize_link" href="thirds">Thirds</a></li>
              <li><a class = "visualize_link" href="fifths">Fifths</a></li>
              <li><a class = "visualize_link" href="remove_highlights">Remove Highlights</a></li>
              <li><a class = "visualize_link" href="clear">Clear</a></li>

            </ul>
          </div><!-- btn-group -->

          

          <?php echo $this->element('Virtuoso.nav/drills_dropdown'); ?>
          <?php echo $this->element('Virtuoso.nav/modes_dropdown'); ?>
       <!-- btn-group -->
    </form>
</div>