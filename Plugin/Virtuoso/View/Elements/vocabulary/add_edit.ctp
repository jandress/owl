<div class="container">    
        <h2 style="text-align:center; font-size:3em; letter-spacing:1px">Vocabulary</h2>

        <div style="padding:60px;" class="col-sm-12">
            <?php 
                echo $this->BootstrapForm->create('Vocabulary');
				///echo $this->BootstrapForm->hidden('id', array('class' => ''));
                echo $this->BootstrapForm->input('Vocabulary.name',array('class'=>'col-md-12'));

                echo $this->BootstrapForm->input('format',array('options' => $vocabDirectoryListing, 'class'=>'pull-right'));
            ?><div id = "form_ajax_target" ><?php
                echo $this->BootstrapForm->input('Vocabulary.url',array('class'=>'col-md-12'));
				echo '<div class = "clearfix bm_2_em">&nbsp;</div>';
                echo $this->BootstrapForm->submit(__('Submit'),array('class'=>'btn-success  btn-large btn-block pull-right' ,'style'=>"font-size:2em; font-weight:bold; letter-spacing:2px"));
            ?>
            <?php echo $this->BootstrapForm->end();?>

        </div>  
</div>


<?php 
    echo $this->Html->script(
        [
            '/Virtuoso/Virtuoso/js/vocab_admin',
        ]
        , array('block'=>"bottom_script")
    ); 
?>




<p>
    <ul>
<li>
1.  file listing of /users/{userdir}/private/vocab as links.
</li>
<li>
2.  ability to upload to that directory and that directory only. (implement a modal with uploader)
</li>
<li>
3.  /vocab/view/ needs to implement UI that displays the advanced player.   Or else a Simple stylized link.  
</li>
<li>

</li>



    </ul>
</p>




    
    
