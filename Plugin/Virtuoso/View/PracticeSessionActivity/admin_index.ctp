<div class = "container  tm_2_em">
	<h2 class = "col-sm-6">My Practice Sessions</h2>
	
	<div class = " pull-right col-sm-6">
		<a href = "/my/caged/add" class = "pull-right btn btn-default">New Session Activity</a>
	</div>
	<div class = "bm_1_em clear clearfix" >&nbsp;</div>
</div>

<div class = "container  well">


<table class="table table-striped table-bordered table-condensed">
		<tr>
			<td>id</td>	
			<td>description</td>
			<td  style = "text-align:center" >completed</td>	
			<td  style = "text-align:center" >session id</td>	
			<td  style = "text-align:right" >timestamp</td>
		</tr>
<?php  foreach ($data as $key => $value): ?>

		<tr>
			<td><?php 	echo $value['PracticeSessionActivity']['id'];  ?></td>	
			<td><?php 	echo $value['PracticeSessionActivity']['description'];  ?></td>

			<td style = "text-align:center" ><?php 	echo $value['PracticeSessionActivity']['completed'];  ?></td>	
			<td style = "text-align:center" ><?php 	echo $value['PracticeSessionActivity']['session_id'];  ?></td>	
			<td  style = "text-align:right" ><?php 	echo $value['PracticeSessionActivity']['timestamp'];  ?></td>
		</tr>
	<?php  endforeach; ?>
</table>

</div>

