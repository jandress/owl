<div class = "container tm_2_em" >
        <div class= "pull-left">
            <h1>Vocabulary</h1>
        </div>
        
        <div class = "btn-group pull-right">
            <a class = "btn btn-primary " href="/my/vocab/add/"> Add New Url</a>
            <a class = "btn btn-primary " href="/my/vocab/add/"> Upload File</a>
        </div>


        <table class="table table-striped">
        <thead>
            <tr>
            <th scope="col">&nbsp;</th>
            <th scope="col">&nbsp;</th>
            <th scope="col">&nbsp;</th>    
        </tr>
        </thead>
        <tbody>
   

        <?php  
            $i=0;
            
            foreach ($files as $key => $value):
                $i++;
                ?>
                <tr>
                    <th scope="row"><?php echo $i; ?></th>
                    <td>
                    <a target = "_blank" href = "/playtrack/private/vocab/<?php echo $key; ?>"><?php echo $key; ?></a>

                    </td>
                    <td>
                        <div class="dropdown " style="text-align:center">
                            <a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown" aria-expanded="true">
                                <span class="glyphicon glyphicon-plus-sign"></span>
                            </a>
                            <ul class="dropdown-menu " role="menu" style="text-decoration:none;">
                                <li>	
                                    <a target="_blank" href="/my/vocabulary/edit/<?php echo $key; ?>">Edit</a>
                                </li>			           		
                                <li>	
                                    <a onclick="return confirm('Are you sure you want to delete \&quot;collaborations\&quot;?');" class="" href="/assets/rmdir/collaborations">
                                        Delete 
                                    </a>		
                                </li>			
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php endforeach;  ?>




            <?php  
            $i=0;
            
            foreach ($links as $key => $value):
                $i++;
                ?>
                <tr>
                    <th scope="row"><?php echo $i; ?></th>
                    <td>
                        <a target = "_blank" href = "<?php echo  $value['Vocabulary']['url']; ?>"><?php echo $value['Vocabulary']['name']; ?></a>
                    </td>
                    <td>
                        <div class="dropdown " style="text-align:center">
                            <a href="#" class="dropdown-toggle btn btn-default" style="text-align:center" data-toggle="dropdown" aria-expanded="true">
                                <span class="glyphicon glyphicon-plus-sign"></span>
                            </a>
                            <ul class="dropdown-menu " role="menu" style="text-decoration:none;">
                                <li>	
                                    <a target="_blank" href="/playtrack/collaborations">Edit</a>
                                </li>			           		
                                <li>	
                                    <a onclick="return confirm('Are you sure you want to delete \&quot;collaborations\&quot;?');" class="" href="/assets/rmdir/collaborations">
                                        Delete 
                                    </a>		
                                </li>			
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php endforeach;  ?>
        </tbody>
        </table>


</div>




