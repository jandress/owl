<div class = "container admin_padding tm_1_em">
    <h2 class = "col-sm-6"><?php  echo($data['Activity']['name']); ?></h2>
    
    <div class = "col-sm-6 tm_1_em">
        <a class = "pull-right btn btn-sm btn-default" href = "/my/activities/add/">New Activity</a>
    </div>
    <table class="table table-striped table-bordered table-hover">
        <tbody>    
            <?php
                foreach ($activities as $key => $value):
                $data = array();
                $value["Activity"] = $value;
                $nl = $value["Activity"];
                $nid = $nl["id"];
                $k = "row_".$key % 2;
                $new_address = $address.'_'.$key;
                $id = $new_address.'n'.$nid;
                $level = count(explode('_',$address));
                $data['nl'] = $nl;                    
                $data['id'] = $id;                    
                $data['level'] = $level;
                $data['Activity'] = $value;
                echo $this->element('activities/recursive_activity_link2',array('data'=>$data)); 
            ?>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
<?php  echo $this->Html->script( [ '/js/courses_sitemap' ] , array('block'=>"bottom_script") ); ?>
