<div class= "container">
	
	<?php
		$data = '{
			"application": "caged",			
			"command": "#octave_to_octave/1s",
			"playerParameters": {
					"autoPlay":true,
					 "handHoldingMode":false,
					 "key":"e",
					 "scale":"major"
			},
			"ui_instructions":{
					"timer":true,
					"text_elements":{
							  "h1":"Grab a guitar and play along!",
							  "h2":"E Major"
					}
			}
		}';


		echo $this->element('activities_add_edit',array('default'=>$data)); 
	?>
</div>