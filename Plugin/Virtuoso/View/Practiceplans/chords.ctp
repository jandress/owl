<div class = "col-sm-12 tm_1_em">
    <?php // echo $this->element('nav/intervals_Controls'); ?>    
    
    <h2>Chords</h2>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod eos asperiores reprehenderit enim ipsam placeat animi reiciendis ex aperiam, quidem velit, earum mollitia? Excepturi facilis ducimus voluptatibus natus quaerat atque.
    <div class = "clear clearfix tm_2_em">&nbsp;</div>
    
    <div class="col-sm-6">
        <?php echo $this->element('nav/individual_ui/timer_slider'); ?>
    </div>        
    <div class="col-sm-6">
        <div class="control-group">
            <label for="fastReplay" class="control-label">Fast Replay</label>
            <div class="controls">
                <input type="checkbox"  name="fastReplay" id="fastReplay" value="0">
            </div>
        </div>
    </div>            
</div>
<div class = "clear clearfix tm_2_em">&nbsp;</div>


<div class = "tm_4_em ">
    <?php  foreach($chords as $b => $i ): ?>
        <div class = "chord_icon_link col-sm-2 bm_4_em">
            <a href = "#<?php echo $i['name']; ?>" > 
                <canvas id="canvas_<?php echo $b?>"></canvas>
                <span><?php echo $i['name']; ?></span>
            </a>
        </div>
    <?php endforeach; ?>
</div>



<script>
    var chordsJS = <?php echo $chordsjs; ?>
</script>


<?php $this->Js->buffer("var chordsJS = ".$chordsjs); ?>