

<?php echo $this->element("Virtuoso.caged_menu"); ?>

<?php 
    echo $this->Html->script(
        [
            '/Virtuoso/Virtuoso/js/drill_runner',
            '/Virtuoso/Virtuoso/js/caged/dev_ui',   
            '/Virtuoso/Virtuoso/js/caged/caged_sequence_player',
            '/Virtuoso/Virtuoso/js/caged/caged_activity_model',
            '/Virtuoso/Virtuoso/js/caged/caged_activity',    
            '/Virtuoso/Virtuoso/js/caged/caged_activity_dev',
            '/Virtuoso/Virtuoso/js/modes_intro'
        ]
        , array('block'=>"bottom_script")
    ); 
?>
