<div class = "container admin_padding tm_1_em">
    <h2 class = "col-sm-6"><?php  
    
    // debug($data);
    echo($data['Practiceplan']['name']); ?></h2>
    
    <div class = "col-sm-6 tm_1_em">
        <a class = "pull-right btn btn-sm btn-default" href = "/my/practiceplans/add/">New Practiceplans</a>
    </div>
    <div class = "clear clearfix">&nbsp;</div>
    <table class="table table-striped table-bordered table-hover">
        <tbody>    
            <?php
                foreach ($practiceplans as $key => $value):
                    // debug($value); continue;
                $data = array();
                $value["Practiceplan"] = $value;
                $nl = $value["Practiceplan"];
                $nid = $nl["id"];
                $k = "row_".$key % 2;
                $new_address = $address.'_'.$key;
                $id = $new_address.'n'.$nid;
                $level = count(explode('_',$address));
                $data['nl'] = $nl;                    
                $data['id'] = $id;                    
                $data['level'] = $level;
                $data['Practiceplan'] = $value;
                echo $this->element('practiceplans/recursive_plan_link2',array('data'=>$data)); 
            ?>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
<?php  echo $this->Html->script( [ '/js/courses_sitemap' ] , array('block'=>"bottom_script") ); ?>
