<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="./favicon.ico">
    <title>Horizontal Guitar</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/layout_aids.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/style.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/guitar_horizontal.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/neck.css" rel="stylesheet">
    
    
    <link href="/css/jquery-ui.css" rel="stylesheet">

    
    <style>
      #bottom_drawer{
        position:fixed;
        width:100%;
        height:300px;
        background-color:#ffffff;
        bottom:0;
      }
      #bottom_drawer_content{
          margin-top:10px;
          height:240px;
          overflow-y:auto;
      }


      #custom-handle {
              width: 3em;
              height: 1.6em;
              top: 50%;
              margin-top: -.8em;
              text-align: center;
              line-height: 1.6em;
        }



    </style>

    
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src=".//js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/js/jquery.js"></script>


    <script src="/js/jquery-ui.js"></script>

    <script src="/js/bootstrap.min.js"></script>
    <!--wyswyg editor -->
    <!--end wyswyg editor -->
    <!-- <link rel="stylesheet" type="text/css" href="./css/freelancer.css"></link> -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <?php echo $this->element('nav/comparison_selects_modal'); ?>
  
  
    <nav id = "top_nav" class="navbar navbar   navbar-inverse navbar-fixed-top" role="navigation">
	  <div class="container-fluid">
      <?php echo $this->element('navigation/top_nav_content',array('admin'=>  !empty($admin)   ));?>
	  </div>
	</nav>

    
    
    
    <div class="container-fluid">
        <div class="guitar_container" >
          <div class="guitar">
            <?php echo $this->element('neck'); ?>
            <img class= "guitar_img" src="/Virtuoso/Virtuoso/img/guitar_left.jpg">
          </div>
      </div>
	  </div>

    
    <div id = "bottom_drawer">
          <?php echo $content_for_layout; ?>
    </div>



    
  </body>

  
  <!-- here -->
  <script src="/Virtuoso/phaser/dev.js"></script>
  <script src="/Virtuoso/Virtuoso/js/caged.js"></script>
  <!-- here -->
  <script src="/Virtuoso/Virtuoso/js/EventManager.js"></script>
  <script src="/Virtuoso/Virtuoso/js/guitar.js"></script>  
  <script src="/Virtuoso/Virtuoso/js/PlayableGuitar.js"></script>
  <!-- here -->    
  <script src="/Virtuoso/Virtuoso/js/GuitarUi.js"></script>
  <script src="/Virtuoso/Virtuoso/js/Virtuoso.js"></script>
  <script src="/Virtuoso/Virtuoso/js/ui.js"></script>
  <!-- here -->
  <script src="/Virtuoso/phaser/versions.js"></script>
  <script src="/Virtuoso/phaser/getQueryString.js"></script>
  <!-- here -->
  <script src="/Virtuoso/Virtuoso/js/makeNotesClickable.js"></script>


  <?php
    /// debug($additional_scripts);
    echo $this->Js->writeBuffer();
    echo $this->Session->flash('flash', array(
        'element' => 'flash',
        'params' => array('plugin' => 'PagekwikTools')
    ));
    // this flash should probably be included in the sites, rather than an a plugin.
    // also the site flash currently resides in the quiz plugin. that needs to be relocated to somewhere universal.
?>


<?php foreach ($additional_scripts as $key => $value): ?>
  <script type="text/javascript" src='<?php echo $value ?>.js'></script>
<?php endforeach; ?>


  
</html>