
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title><?php echo $title_for_layout; ?></title>

    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.js"></script>
    <link href="/css/bootstrap.css" rel="stylesheet">

    <link href="/css/layout_aids.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/style.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/guitar_horizontal.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/neck.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">

    <link href="/css/jquery-ui.css" rel="stylesheet">    
    <link href="/Quizmodules/css/pnotify.custom.min.css" rel="stylesheet">
    <script src="/Quizmodules/js/pnotify.custom.min.js"></script>    
    <script src="/PagekwikTools/js/notifications.js"></script>

    <script src="/js/bootstrap.min.js"></script>
      
    <style>
      /* #bottom_drawer{
        position:fixed;
        width:100%;
        height:500px;
        background-color:#ffffff;
        bottom:0;
      }
      #bottom_drawer_content{
          margin-top:10px;
          height:300px;
          overflow-y:auto;
      }
      .guitar_container {
          top: -240px;
      } */

      #caption_holder {
          position: absolute;
          top: 240px;
          z-index: 400;
          margin-left: 120px;
      }


      #caption_holder h1,
      #caption_holder h2 {
        display: inline;
        color: #ffffff;
      }

      .underline, .underlined{
        text-decoration:underline;
      }


    </style>

    <!-- Bootstrap core CSS -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/js/ie-emulation-modes-warning.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
  <style>

/* GLOBAL STYLES
 * Padding below the footer and lighter body text
* -------------------------------------------------- */

body {
  padding-bottom: 40px;
  color: #5a5a5a;
}


/* CUSTOMIZE THE NAVBAR
-------------------------------------------------- */

/* Special class on .container surrounding .navbar, used for positioning it into place. */
.navbar-wrapper {
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  z-index: 20;
}

/* Flip around the padding for proper display in narrow viewports */
.navbar-wrapper > .container {
  padding-right: 0;
  padding-left: 0;
}
.navbar-wrapper .navbar {
  padding-right: 15px;
  padding-left: 15px;
}
.navbar-wrapper .navbar .container {
  width: auto;
}


/* RESPONSIVE CSS
-------------------------------------------------- */

@media (min-width: 768px) {
  /* Navbar positioning foo */
  .navbar-wrapper {
    margin-top: 20px;
  }
  .navbar-wrapper .container {
    padding-right: 15px;
    padding-left: 15px;
  }
  .navbar-wrapper .navbar {
    padding-right: 0;
    padding-left: 0;
    border-radius: 4px; /* The navbar becomes detached from the top, so we round the corners */
  }
}

.guitar_container{
  height:500px;
  overflow-y:hidden;
}
.guitar{
  margin-top:-220px;
}

.neck{
  top:188px !important;
}
  </style>
      <link href="/Virtuoso/Virtuoso/css/style.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/guitar_horizontal.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/neck.css" rel="stylesheet">
  </head>

  <body>

  <?php echo $this->element('Virtuoso.nav/comparison_selects_modal'); ?>  
  
  <nav id = "top_nav" class="navbar navbar navbar-inverse navbar-fixed-top" role="navigation">
	  <div class="container-fluid">
      <?php echo $this->element('Virtuoso.navigation/top_nav_content', array('admin'=>!empty($admin)   ));?>
	  </div>
	</nav>

  <div id ="undernav_drawer" class="nav_underbar navbar-default " >
          <div class = "pl_1_em">
          <?php //
            if(isset($underbar_content)){
              echo $this->element($underbar_content); 
            }else{
              echo $this->element('Virtuoso.nav/nc'); 
            }
            ?>
          </div>
          <div class="clear clearfix bm_1_em">&nbsp;</div>
          <div class="clear clearfix tm_1_em">&nbsp;</div>
      </div>
      

    <div class="">
      <div class="container-fluid">
      <div class="guitar_container" >
          <div class="guitar">
            <?php echo $this->element('neck'); ?>
            <img class= "guitar_img" src="/Virtuoso/Virtuoso/img/guitar_left.jpg">
          </div>
      </div>
      </div>
    </div>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div style = "margin-top:400px;" class="container marketing" >
      <?php echo $content_for_layout; ?>
      <hr class="featurette-divider">
    </div><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  </body>
  <script src="/Virtuoso/phaser/dev.js"></script>
  <script src="/Virtuoso/phaser/versions.js"></script>
  <script src="/Virtuoso/phaser/getQueryString.js"></script>
  <!-- here -->
  <script src="/Virtuoso/Virtuoso/js/caged.js"></script>
  <!-- here -->
  <script src="/Virtuoso/Virtuoso/js/EventManager.js"></script>
  <script src="/Virtuoso/Virtuoso/js/guitar.js"></script>  
  <script src="/Virtuoso/Virtuoso/js/PlayableGuitar.js"></script>
  <!-- <script src="/Virtuoso/Virtuoso/js/ui.js"></script>  
  <script src="/Virtuoso/Virtuoso/js/GuitarUI.js"></script> -->
  <script src="/Virtuoso/Virtuoso/js/makeNotesClickable.js"></script>
  <!-- here -->    
  <?php
    /// debug($additional_scripts);
    echo $this->Js->writeBuffer();
    echo $this->Session->flash('flash', array(
        'element' => 'flash',
        'params' => array('plugin' => 'PagekwikTools')
    ));
    // this flash should probably be included in the sites, rather than an a plugin.
    // also the site flash currently resides in the quiz plugin. that needs to be relocated to somewhere universal.
  ?>
  <?php foreach ($additional_scripts as $key => $value): ?>
    <script type="text/javascript" src='<?php echo $value ?>.js'></script>
  <?php endforeach; ?>
</html>
