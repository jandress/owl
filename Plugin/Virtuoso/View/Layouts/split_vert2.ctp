<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="./favicon.ico">
    <title>Vert</title>
    <!-- split_vert2.ctp -->
  	<link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/layout_aids.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/neck.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/guitar_vertical_compact.css?nc=<?php echo rand() ?>" rel="stylesheet">
    <!-- <link href="/Virtuoso/Virtuoso/css/style.css" rel="stylesheet"> -->
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/jquery-ui.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="/Virtuoso/Virtuoso/.//js/ie8-responsive-file-warning.js"></script><![endif]-->
  
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.js"></script>
    <script src="/js/bootstrap.min.js"></script>








    <style>


      html, body,.right_side{
          height:100%
      }
      .left_side_content{
        background:white;
        color:#000000;
        min-height:100% !important;
        position:fixed;
        width:inherit;
        padding:40px;
        padding-top:0px;
        overflow-y:scroll;
      }
      .left_side_p{
        overflow-y:scroll;
      }
      .left_side{
        padding:0px !important;
        overflow-y:scroll;
      }
      
      .guitar_container{
        width:100%;
      }

      .right_side{
        min-height:100%        
      }
      .neck{
        margin-top:660px;
      }
      #left_side_content{
        height:100px;
      }


    </style>
    <!--wyswyg editor -->
    <!--end wyswyg editor -->

    <?php  /* <link rel="stylesheet" type="text/css" href="./css/freelancer.css"></link> */ ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
   


    </style>
  </head>
  <body>



  <?php echo $this->element('Virtuoso.nav/comparison_selects_modal'); ?>
   	<nav id = "top_nav" class="navbar navbar   navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
          <?php echo $this->element('navigation/top_nav_content',array('admin'=>  !empty($admin)   ));?>
      </div>
      <div class = "clear clearfix">&nbsp;</div>
  	</nav>
  <div class = "clear clearfix">&nbsp;</div>




<div class="container-fluid tm_4_em">
      <div class = "left_side col-sm-6">
        
      
        <div id = "left_side_content" class = "left_side_content col-sm-12">
      
           <div style = "width:1200px;  margin-left:-200px !important;">
                <div class = "vert_neck_container" style = "margin-left:600px;">
                  <?php echo $this->element('Virtuoso.neck'); ?>
                </div>
              <img style = "width:100%;" src="/Virtuoso/Virtuoso/img/guitar_vert_compact.jpg" alt="">
            </div>
      
        </div>


      </div>      
      <div style = "background:#ffffff; overflow-x:hidden;"  class = "right_side col-sm-6">        
          
        <?php echo $content_for_layout; ?>

      </div>
</div>

 



  
  </body>

  


  <script src="/Virtuoso/phaser/dev.js"></script>
  <script src="/Virtuoso/phaser/versions.js"></script>
  <script src="/Virtuoso/phaser/getQueryString.js"></script>
  <!-- here -->
  <script src="/Virtuoso/Virtuoso/js/EventManager.js"></script>
  <script src="/Virtuoso/Virtuoso/js/guitar_model.js"></script>
  <script src="/Virtuoso/Virtuoso/js/caged.js"></script>
  <!-- here -->
  
  <script src="/Virtuoso/Virtuoso/js/guitar.js"></script>  
  <script src="/Virtuoso/Virtuoso/js/PlayableGuitar.js"></script>

  <?php
    /// debug($additional_scripts);
    echo $this->Js->writeBuffer();
    echo $this->Session->flash('flash', array(
        'element' => 'flash',
        'params' => array('plugin' => 'PagekwikTools')
    ));
    // this flash should probably be included in the sites, rather than an a plugin.
    // also the site flash currently resides in the quiz plugin. that needs to be relocated to somewhere universal.
?>


<?php foreach ($additional_scripts as $key => $value): ?>
  <script type="text/javascript" src='<?php echo $value ?>.js'></script>
<?php endforeach; ?>





  <script>
      $(".note").click(function(e){
          var n = $(e.target).attr("id").replace("fret_","").split("_");
          guitar.playSoundForNote( parseInt(n[0])+1, parseInt(n[1]));
      });
      $(window).resize(function(e){
        $("#left_side_content").resize(function(e){

        });

        
      });
  </script>
  <?php echo $this->fetch('bottom_script') ?>

</html>
