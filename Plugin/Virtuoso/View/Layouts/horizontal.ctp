<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="./favicon.ico">
    <title>Horizontal Guitar</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/icons.css" rel="stylesheet">
    <link href="/css/layout_aids.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/style.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/guitar_horizontal.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/neck.css" rel="stylesheet">
    <link href="/Virtuoso/Virtuoso/css/style.css" rel="stylesheet">
    
    
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <link href="/css/jquery-ui.css" rel="stylesheet">
    
    
    <link href="/Quizmodules/css/pnotify.custom.min.css" rel="stylesheet">
    <script src="/Quizmodules/js/pnotify.custom.min.js"></script>    
    <script src="/PagekwikTools/js/notifications.js"></script>
    
    <style>
      /* #bottom_drawer{
        position:fixed;
        width:100%;
        height:500px;
        background-color:#ffffff;
        bottom:0;
      }
      #bottom_drawer_content{
          margin-top:10px;
          height:300px;
          overflow-y:auto;
      }


*/

      .guitar_container {
          top: 0px;
      } 

      #caption_holder {
          position: absolute;
          top: 240px;
          z-index: 4;
          margin-left: 120px;
      }


      #caption_holder h1,
      #caption_holder h2 {
        display: inline;
        color: #ffffff;
      }

      .underline, .underlined{
        text-decoration:underline;
      }
      body,html{
        background-color:#171717;
      }



      


    </style>

    
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src=".//js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!--wyswyg editor -->
    <!--end wyswyg editor -->
    <!-- <link rel="stylesheet" type="text/css" href="./css/freelancer.css"></link> -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>


    <?php echo $this->element('nav/comparison_selects_modal'); ?>
    <div id="caption_holder"></div>

      <nav id = "top_nav" class="navbar navbar   navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
          <?php echo $this->element('navigation/top_nav_content',array('admin'=>  !empty($admin)   ));?>
        </div>
      </nav>

      <div id ="undernav_drawer" class="nav_underbar navbar-default " >
          <div class = "pl_1_em">
          <?php //
            if(isset($underbar_content)){
              echo $this->element($underbar_content); 
            }else{
              echo $this->element('Virtuoso.nav/nc',array('scaleList'=>$scaleList)); 
            }
            ?>
          </div>
          <div class="clear clearfix bm_1_em">&nbsp;</div>
          <div class="clear clearfix tm_1_em">&nbsp;</div>
      </div>
      
      <div class="container-fluid">
      <?php
//  debug($left_nav_array); 
?>
          <div class="guitar_container" >
            <div class="guitar">
              <?php echo $this->element('neck'); ?>
              <img class= "guitar_img" src="/Virtuoso/Virtuoso/img/guitar_left.jpg">
            </div>
        </div>
      </div>

    
    <div id = "bottom_drawer">
        <?php  echo $content_for_layout; ?>
    </div>


    
  </body>

  <script src="/Virtuoso/phaser/dev.js"></script>
  <script src="/Virtuoso/phaser/versions.js"></script>
  <script src="/Virtuoso/phaser/getQueryString.js"></script>
  <!-- here -->
  <script src="/Virtuoso/Virtuoso/js/EventManager.js"></script>
  <script src="/Virtuoso/Virtuoso/js/guitar_model.js"></script>
  <script src="/Virtuoso/Virtuoso/js/caged.js"></script>
  <!-- here -->
  
  <script src="/Virtuoso/Virtuoso/js/guitar.js"></script>  
  <script src="/Virtuoso/Virtuoso/js/PlayableGuitar.js"></script>
  <!-- <script src="/Virtuoso/Virtuoso/js/ui.js"></script>  
  <script src="/Virtuoso/Virtuoso/js/GuitarUI.js"></script> -->
  <script src="/Virtuoso/Virtuoso/js/makeNotesClickable.js"></script>
  <!-- here -->    
  <?php
    /// debug($additional_scripts);
    echo $this->Js->writeBuffer();
    echo $this->Session->flash('flash', array(
        'element' => 'flash',
        'params' => array('plugin' => 'PagekwikTools')
    ));
    // this flash should probably be included in the sites, rather than an a plugin.
    // also the site flash currently resides in the quiz plugin. that needs to be relocated to somewhere universal.
?>

<script>
  //  function audioReady(){}
</script>  



<?php foreach ($additional_scripts as $key => $value): ?>
    <script type="text/javascript" src='<?php echo $value ?>.js'></script>
<?php endforeach; ?>

<?php echo $this->fetch('bottom_script') ?>

</html>
