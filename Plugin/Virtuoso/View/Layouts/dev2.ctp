<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="./favicon.ico">
    <title>Vert</title>


  	<link href="/css/bootstrap.css" rel="stylesheet">
    
    <link href="/css/layout_aids.css" rel="stylesheet">

    <link href="/Virtuoso/Virtuoso/css/neck.css" rel="stylesheet">

    <link href="/Virtuoso/Virtuoso/css/style.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="/Virtuoso/Virtuoso/.//js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <style>


html, body, .container-fluid, .right_side{
      height:100%
    }
      .left_side {
        background:white;
        color:#000000;
        padding-right:40px;
        padding-left:40px;
        min-height:100% !important;
      }

      .guitar_img2{}
/* 
        .guitar_container{
          width:100%;
          overflow-x:auto;
          overflow-y:auto;
        } */

        .right_side{
          min-height:100%
          background-color:red;
          color:white;
        }






a{
  color:#ffffff;
}
.guitar{
  width:1249px;
}
.guitar_container_container{
  
  width:1249px;  
  border-color: #1771bd;
  margin-left: auto;
  margin-right: auto;
}
.guitar_container{
  margin-left:625px;            
}
.guitar_img{
  width:1249px;
  position: absolute;
  margin-left:-625px; 
  z-index: -10;;
}
.vert_neck_container {
  width:100%;
}
.neck{
  position:absolute;  
  margin-left:-130px;
  margin-top:690px;
  border-color: #00ff00;
}
.neck_buttons, .string{ 
  width:25px;
  height:100%;
  margin-bottom:5px;

  float:right;
}
.neck_buttons{
  margin-right: -4px;
}
.string{ 
  margin-left: 10.35px;
}


.string{
  display:block;
  border-color: #00ff00;
}


.neck_btn{
  position:absolute;
  border-width: 1px;
  border-color: #00ff00;
  background-color: #666666;
  border-radius: 20px;
  text-align:center;
  width:20px;
}


.note { 
    width:25px;
    height:25px;
    border-style: solid;
    border-width: 1px;
    border-color: #000000;
    background-color: #ffffff;
    border-radius: 20px;
    text-align:center;
    position:absolute;
}


#caption_holder {
  left:0px;
  z-index: 400;
  margin-left: 20px;
  margin-top:7px;
}







#caption_holder h1, #caption_holder h2{
  font-size: 24px;
}






.fret_position_0{
  top:0px
}
.fret_position_1{
  top:60px
}
.fret_position_2{
  top:150px;
}
.fret_position_3{
  top:223px;
}
.fret_position_4{
  top:300px;
}
.fret_position_5{
  top:375px;
}
.fret_position_6{
  top:455px;
}
.fret_position_7{
  top:530px;
}
.fret_position_8{
  top:610px;
}
.fret_position_9{
  top:684px;
}
.fret_position_10{
  top:763px;
}
.fret_position_11{
  top:836px;
}
.fret_position_12{
  top:911px;
}
.fret_position_13{
  top:972px;
}
.fret_position_14{
  top:1037px;
}
.fret_position_15{
  top:1097px;
}
.fret_position_16{
  top:1150px;
}
.fret_position_17{
  top:1200px;
}
.fret_position_18{
  top:1248px;
}
.fret_position_19{
  top:1292px;
}
.fret_position_20{
  top:1333px;
}
.fret_position_21{
  top:1373px;
}

.red {
  background-color:#ff0000 !important;
}
.red_orange {
  background-color:#ff4800 !important;
}
.orange {
  background-color:#ff7700 !important;
}
.yellow_orange {
  background-color:#ffab00 !important;
}
.yellow {
  background-color:#fff800 !important;
}
.yellow_green {
  background-color:#cefc07 !important;
}
.green {
  background-color:#3bab2d !important;
}
.blue-green {
  background-color:#00d193 !important;
}
.blue {
  background-color:#1771bd !important;
}
.blue_text {
  color:#1771bd !important;
}
.yellow_text {
  color:#fff800 !important;
}
.green_text {
  color:#3bab2d !important;
}
.purple{
  background-color:#6718c6 !important;
}
.violet{
  background-color:#9226e3 !important;
}
.magenta{
  background-color:#ff0090 !important;
}

.note{
  cursor: pointer;
}





    </style>
    <!--wyswyg editor -->
    <!--end wyswyg editor -->

    <?php  /* <link rel="stylesheet" type="text/css" href="./css/freelancer.css"></link> */ ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
   


    </style>
  </head>
  <body>



  <?php echo $this->element('nav/comparison_selects_modal'); ?>
    <div id="caption_holder"></div>
   	<nav id = "top_nav" class="navbar navbar   navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
          <?php echo $this->element('navigation/top_nav_content',array('admin'=>  !empty($admin)   ));?>
      </div>
      <div class = "clear clearfix">&nbsp;</div>
  	</nav>
  <div class = "clear clearfix">&nbsp;</div>




<div class="container-fluid tm_4_em">
      <div class = "left_side col-sm-6">
          <?php echo $this->element('fpo_text'); ?>
      </div>      
      <div class = "right_side col-sm-6">
          <div class = "guitar_container_container" >
            <div class="guitar_container" >
                <div class="guitar">
                  <div class = "vert_neck_container">
                        <?php echo $this->element('neck'); ?>
                  </div>
                  <img class= "guitar_img" src="/Virtuoso/Virtuoso/img/guitar_vert.jpg">
                </div>
            </div>
          </div>    
      </div>
</div>

 



  
  </body>

  <script src="/Virtuoso/phaser/dev.js"></script>
  <script src="/Virtuoso/Virtuoso/js/caged.js"></script>
  <!-- here -->
  <script src="/Virtuoso/Virtuoso/js/EventManager.js"></script>
  <script src="/Virtuoso/Virtuoso/js/guitar.js"></script>  
  <script src="/Virtuoso/Virtuoso/js/PlayableGuitar.js"></script>
  <!-- here -->    
  <script src="/Virtuoso/Virtuoso/js/GuitarUi.js"></script>
  <script src="/Virtuoso/Virtuoso/js/Virtuoso.js"></script>
  <script src="/Virtuoso/Virtuoso/js/ui.js"></script>
  <!-- here -->
  <script src="/Virtuoso/phaser/versions.js"></script>
  <script src="/Virtuoso/phaser/getQueryString.js"></script>
  <!-- here -->
 
 
 
  <?php
    /// debug($additional_scripts);
    echo $this->Js->writeBuffer();
    echo $this->Session->flash('flash', array(
        'element' => 'flash',
        'params' => array('plugin' => 'PagekwikTools')
    ));
    // this flash should probably be included in the sites, rather than an a plugin.
    // also the site flash currently resides in the quiz plugin. that needs to be relocated to somewhere universal.
?>






<?php foreach ($additional_scripts as $key => $value): ?>
  <script type="text/javascript" src='<?php echo $value ?>.js'></script>
<?php endforeach; ?>


 
 
 
 <script>
      $(".note").click(function(e){
          var n = $(e.target).attr("id").replace("fret_","").split("_");
          guitar.playSoundForNote( parseInt(n[0])+1, parseInt(n[1]));
      });
  </script>


</html>
