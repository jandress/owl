<div class = "admin_padding">
	<h2>
		Editing CagedActivity
	</h2>
	<?php 
		echo $this->BootstrapForm->create('CagedActivity', array('class' => 'form-horizontal'));
		echo $this->BootstrapForm->hidden('id');
		echo $this->BootstrapForm->input('name', array(
			'required' => 'required'	
			,'class'=>'tm_2_em col-sm-8'
			)
		);
		echo $this->BootstrapForm->TextArea('data', array(
			'helpInline' => '<span class="label label-important">' . __('Json') . '</span>&nbsp;'
			,'class'=>'tm_2_em col-sm-8'
			)
		);
		?>
		<div class ="clear clearfix tm_2_em">&nbsp;</div>
		<div class ="clear clearfix tm_2_em">&nbsp;</div>
		<?php
		echo $this->BootstrapForm->submit(__('Submit'));	
	?>
</div>