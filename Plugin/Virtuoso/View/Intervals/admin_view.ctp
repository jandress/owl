<div class="row-fluid">
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('')), array('controller' => '', 'action' => 'add'));?> </li>
		</ul>
	</div>
	<div class="span9">
		<h2><?php  echo __('Contentmodule');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($contentmodule['Contentmodule']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Class'); ?></dt>
			<dd>
				<?php echo h($contentmodule['Contentmodule']['class']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Foreign Id'); ?></dt>
			<dd>
				<?php echo h($contentmodule['Contentmodule']['foreign_id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Created'); ?></dt>
			<dd>
				<?php echo h($contentmodule['Contentmodule']['created']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Modified'); ?></dt>
			<dd>
				<?php echo h($contentmodule['Contentmodule']['modified']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Contentmodule')), array('action' => 'edit', $contentmodule['Contentmodule']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Contentmodule')), array('action' => 'delete', $contentmodule['Contentmodule']['id']), null, __('Are you sure you want to delete # %s?', $contentmodule['Contentmodule']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__( __('Contentmodules')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Contentmodule')), array('action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

