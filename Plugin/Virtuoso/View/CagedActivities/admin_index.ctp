<div class = "admin_padding tm_2_em">
	<h2 class = "col-sm-6">CagedActivities</h2>
	<div class = " pull-right col-sm-6">
		<a href = "/my/caged/add" class = "pull-right btn btn-default">New CagedActivity</a>
	</div>
	<?php if (!empty($cagedActivities)): ?>	
	<table class="table table-striped table-bordered table-hover tm_1_em">
			<tbody>
				<tr>
					<th class="span1" style="text-align:center">id</th>
					<th class="span4"><a href="/my/landingpages/sort:title/direction:asc">Title</a></th>
					<th class="actions span3" style="text-align:center">Actions</th>
				</tr>
				<?php foreach ($cagedActivities as $key => $value): ?>
					<tr>
						<td style="text-align:center">
							<?php echo $value['CagedActivity']['id']; ?>&nbsp;
						</td>
						<td>
							<a href = "/my/caged/view/<?php echo $value['CagedActivity']['id']; ?>" target = "_blank"><?php echo $value['CagedActivity']['name']; ?></a>
							&nbsp;
						</td>
						<td class="actions" style="text-align:center">
							<ul style="list-style:none; padding:0">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
									Actions
								</a>
								<ul class="dropdown-menu  dropdown-menu-right" role="menu" style="text-decoration:none;">
									<li>	
										<a href = "/my/caged/view/<?php echo $value['CagedActivity']['id']; ?>" target = "_blank">View</a>
									</li>
									<li>	
										<a href="/my/caged/edit/<?php echo $value['CagedActivity']['id'] ?>" target="_blank">Edit Activity</a>
									</li>
									<li>	
										<?php echo $this->Form->postLink( __('Delete'), '/my/cagedActivities/delete/'.$value['CagedActivity']['id'] , array($value['CagedActivity']['id']) ,__('Are you sure you want to delete this tier and all associated (child) content?', $value['CagedActivity']['id']) ); ?>
									</li>
								</ul>
							</li>
						</ul>	
						</td>
					</tr>					
				<?php endforeach ?>

			</tbody>
	</table>
		<?php echo $this->BootstrapPaginator->pagination(); ?>
	<?php 
	else:
		debug("empty");
	endif ?>
	
	<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
	<?php echo $this->BootstrapPaginator->pagination(); ?>
</div>
