<?php
    function findPreviousAndNext($array, $searchIndex) {
        $keys = array_keys($array);
        $currentIndex = array_search($searchIndex, $keys);
        if ($currentIndex === false) {
            return null; // If searchIndex is not found, return null
        }
        $prevKey = ($currentIndex > 0) ? $keys[$currentIndex - 1] : null;
        $nextKey = ($currentIndex < count($keys) - 1) ? $keys[$currentIndex + 1] : null;
        return array('prv' => $prevKey, 'nxt' => $nextKey);
    }
    $t = findPreviousAndNext($data['list'], $data['CagedActivity']['id']);
?>
<script>
    var prev ="/my/caged/view/<?php echo $t['prv']; ?>"
    var next = "/my/caged/view/<?php echo $t['nxt']; ?>"
    var activityJson = <?php echo($this->data['CagedActivity']['data']); ?>
</script>

<?php 
    echo $this->Html->script(
        [
            '/Virtuoso/Virtuoso/js/drill_runner',
            '/Virtuoso/Virtuoso/js/caged/dev_ui',   
            '/Virtuoso/Virtuoso/js/caged/caged_sequence_player',
            '/Virtuoso/Virtuoso/js/caged/caged_activity_model',
            '/Virtuoso/Virtuoso/js/caged/caged_activity',    
            '/Virtuoso/Virtuoso/js/caged/caged_activity_dev',
            '/Virtuoso/Virtuoso/js/caged_view'
        ]
        , array('block'=>"bottom_script")
    ); 
?>

