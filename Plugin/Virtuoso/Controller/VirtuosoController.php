<?php

App::uses('AppController', 'Controller');
//App::uses('Sanitize', 'Utility');
//App::uses('Security', 'Utility');
//App::uses('Auth', 'Component');


class VirtuosoController extends VirtuosoAppController {

	 var $uses = array('Virtuoso.Scale');
		// 'UserManagement.User','MessagingSystem.Message','UserManagement.Ticket','UserManagement.Group','Aro','Aco');
	// var $helpers = array('Html',"Session" , "Js",'TwitterBootstrap.BootstrapHtml',  'TwitterBootstrap.BootstrapForm','UserManagement.UserManagementForm' );
	// public $components = array('UserManagement.UserManagement','Acl', 'Session','Security','Auth','RequestHandler','Email');
	
    function beforeFilter() {
		$this->Auth->allow('index');
		parent::beforeFilter();
    }


	function index(){
		
		$this->layout = "horizontal";
		// $this->layout = "split_horiz";
		$this->set('scaleList', $this->Scale->scales());
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/drill_runner');
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/GuitarUi');
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/Virtuoso');
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/ui');
		//$this->addAdditionalScript('/Virtuoso/Virtuoso/js/makeNotesClickable');
		//$this->layout = "vertical";
	}

	function comparison( $scale1Key, $scale1Name,  $scale2Key, $scale2Name ){
		// debug($scale1Key.' '.$scale1Name.' '.  $scale2Key.' '. $scale2Name);


		//   $scale1Name, $scale1Key, $scale2Name, $scale2Key
		$this->layout = "horizontal";
		// $this->layout = "split_horiz";
		$this->set('scaleList', $this->Scale->scales());
		$this->set(
			'scale1Key' , $scale1Key
		);
		$this->set(
			'scale1Name',$scale1Name
		);

		$this->set(
			'scale2Key', $scale2Key
		);
		$this->set(
			 'scale2Name', $scale2Name
		);
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/drill_runner');
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/GuitarUi');
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/Virtuoso');
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/ui');
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/comparison');
		//$this->addAdditionalScript('/Virtuoso/Virtuoso/js/makeNotesClickable');
		//$this->layout = "vertical";
	}




	

}
