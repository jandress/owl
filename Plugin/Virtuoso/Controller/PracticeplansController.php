<?php


class PracticeplansController extends VirtuosoAppController {

    public $uses = array('Virtuoso.Practiceplan','Virtuoso.PracticeSessionPracticeplan',"Chord","Virtuoso.Scale",'Virtuoso.Activity');
    public $helpers = array('Navigations.TreeNav');
    public $components = array('TreeNav');
	
    public $intervals_includes = array(
		'/Virtuoso/Virtuoso/js/GuitarUi',
		'/Virtuoso/Virtuoso/js/intervals/SimpleSequencePlayer',
		'/Virtuoso/Virtuoso/js/intervals/IntervalSequencer',
		'/Virtuoso/Virtuoso/js/intervals/sspUI',
		'/Virtuoso/Virtuoso/js/intervals/IntervalTrainer',
	);

    public $caged_includes = array(
        
        "/Virtuoso/Virtuoso/js/drill_runner",
        "/Virtuoso/Virtuoso/js/caged/ui_messaging_interface",
        "/Virtuoso/Virtuoso/js/caged/dev_ui",
        "/Virtuoso/Virtuoso/js/caged/caged_sequence_player",
        "/Virtuoso/Virtuoso/js/caged/caged_activity_model", 
        "/Virtuoso/Virtuoso/js/caged/caged_activity",
        "/Virtuoso/Virtuoso/js/caged/caged_activity_dev", 
        "/Virtuoso/Virtuoso/js/caged_view",
        '/Virtuoso/Virtuoso/js/caged/init_caged'
    );

    function beforeFilter() {		
		parent::beforeFilter();
        $this->Auth->allow('index',"admin_children",'admin_index','view',"dev","chords","dev2","modes");
    }

    public function index(){
        $this->title("Activities Index");
        //$this->layout = "standalone_styled";
        $this->layout = "standalone_styled";
        //$this->layout = "background_only";
        
        $this->createPracticeSession();
    }

    private function createPracticeSession(){
        // $g = $this->Practiceplan->find("all", array('conditions'=>array('Practiceplan.parent_id'=>1)));
        // $ar = array();
        // foreach ($g as $key => $value) {
        //     //  debug($value);
        //     $i = rand(0,count($value['ChildPracticeplan'])-1);
        //     // debug($i);
        //     array_push($ar,$value['ChildPracticeplan'][$i]);
        // }
        // debug($ar); exit;
        // $this
        $this->set('session_practiceplans',$ar);
    }


    public function admin_index($id=1){
        // debug($id); exit;
        $this->Practiceplan->recursive = 2;
        $g = $this->Practiceplan->find("first",array('conditions'=>array("Practiceplan.id"=>$id),'order' => array('Practiceplan.lft')));
        $this->set('data',$g);
        $this->set('practiceplans',$g['ChildPracticeplan']);
        $this->layout = "dashboard";
        $address = 0;    
        $this->set('address',$address);        
    }

    public function admin_children($id=2, $address = 0){
        $this->layout = "ajax";
        $id = $this->params->params['pass'][0];
        $g = $this->Practiceplan->find("all",array('conditions'=>array("Practiceplan.parent_id"=>$id),'order' => array('Practiceplan.lft')));
        // $address = 0;    

		$g = $this->Practiceplan->find('all', array(
                'order' => array('Practiceplan.lft')
                ,'conditions'=>array('Practiceplan.parent_id' => $id)
            )
        );
        $this->set('practiceplans',$g);
        $this->set('address',$address);
        // debug($g); exit;
    }

    public function view($id){
        $this->title("Activities view");
        // $this->admin_view($id);
        //$data = $this->Navlink->generateTreeList(null, null, null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
        // $this->addAdditionalScript('/Virtuoso/Virtuoso/js/GuitarUi');
        // $this->addAdditionalScript('/Virtuoso/Virtuoso/js/ui');
        // $this->addAdditionalScript('/Virtuoso/Virtuoso/js/caged_activity');
    }

    public function admin_view($id){         
        // $this->Practiceplan->reorder();        
        $this->layout = "horizontal";
        $this->Practiceplan->id = $id;
        $this_Practiceplan = $this->Practiceplan->find("first",array('conditions'=>array('Practiceplan.id'=>$id) ));;
        $json_data = json_decode(($this_Practiceplan['Practiceplan']['data']));
        // debug($this_Practiceplan['Practiceplan']['data']); exit;
        $full_command = explode("/",$json_data->command);
        $params = $full_command;
        $c = array_splice($params,0,1);
        $command = str_replace("#","",$c[0]);       
        // debug($full_command);
            // $this_Practiceplan['Practiceplan']['data']); exit
        $this->set("activityJson", $this_Practiceplan['Practiceplan']['data']);
        $this->set("command",$command);
        $this->set("parameters", $params);        
        //--  ui_messaging_interface


        switch ($json_data->application) {
            case 'caged':
                    # code...
                    $this->addAdditionalScripts($this->caged_includes );
                    $this->set('underbar_content',"caged_menu");
                    $this->set('scaleList', $this->Scale->scales());
                break;
            case 'interval':
                    # code...
                    $this->addAdditionalScripts($this->intervals_includes );
                    $this->set("underbar_content",'nav/intervals_exercise_nav');
                break;                
                case 'chords':
                    # code...
                    // $this->addAdditionalScripts($this->intervals_includes );
                    // $this->set("underbar_content",'nav/intervals_exercise_nav');
                break;                                
            default:
                # code...
                break;
        }
        $this->setPreviousNextActions($id);
    }
    
    private function setPreviousNextActions($id=0){
        $vv = $this->Practiceplan->generateTreeList(null, null, null, ' ');
        $i = 0; 
        $prev = null;
        $tr = false;
        foreach ($vv as $key => $value) {
            $i++;
            if($tr){
                $this->set("activity_next",$key);
                $next = $key;
                $activity_next = $key;
                break;                
            }
            if($key==$id){
                if(isset($prev)){
                    $this->set("activity_prev",$prev);
                }
                $activity_prev = $prev;
                $tr = true;
            }
            $prev = $key;
        }
        $practice_session = $this->PracticeSessionPracticeplan->find("list",array('conditions'=>array('PracticeSessionPracticeplan.user_id'=>$this->current_user['User']['id'], 'PracticeSessionPracticeplan.completed' => 0)));;
        $tr = false;
        
        foreach ($practice_session as $key => $value) {
            $i++;
            if($tr){
                $this->set("practiceSessionPracticeplan_next",$key);
                $practiceSessionPracticeplan_next = $key;
                break;                
            }
            $this->set("practiceSessionPracticeplan_next",$key);
            $this->set("practiceSessionPracticeplan_prev",$prev);
            if($key==$id){
                if(isset($prev)){
                    $this->set("practiceSessionPracticeplan_prev",$prev);
                    $practiceSessionPracticeplan_prev = $prev;
                }
                $tr = true;
            }
            $prev = $key;
        }
    }

	public function admin_relocate($id,$direction){
        // debug($this->Practiceplan->veritfy()); exit;
        $this->layout="ajax";        
        if($direction=='up'){
            // debug($this->Practiceplan->moveUp($id)); exit;
            if($this->Practiceplan->moveUp($id)){
                $this->redirect('/my/practiceplans/');
            }
        }
        if($direction=='down'){
            //$this->Practiceplan->recover();
            // debug($this->Practiceplan->moveDown($id)); exit;
            if($this->Practiceplan->moveDown($id)){
                $this->redirect('/my/practiceplans/');
            }
        }
        
        $g =  $this->Practiceplan->generateTreeList(null, null, null, '.');
    }

    public function admin_edit($id = null) {


        // $this->Practiceplan->contain("Activity");
        // $g = $this->Practiceplan->find('all');
        // debug( $g );
        // foreach ($g as $key => $value) {
        //     $b = $value['Practiceplan'];
        //     $b['activity_id'] = $b['id'];
        //     debug($this->Practiceplan->save($b));
        // }





        $this->set('activities_options',$this->Activity->find('list'));    

        $this->set('parent_id',$this->Practiceplan->find('list'));

        $this->layout = "dashboard";
        $this->Practiceplan->id = $id;
        if (!$this->Practiceplan->exists()) {
            throw new NotFoundException(__('Invalid %s', __('Practiceplan')));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $g = $this->Practiceplan->save($this->request->data);
            // debug($this->request->data);
            // debug($g); exit;
            if ($g) {
                $this->Session->setFlash(
                    __('The %s has been saved', __('Practiceplan')),
                    'alert',
                    array(
                        'plugin' => 'TwitterBootstrap',
                        'class' => 'alert-success'
                    )
                );
                $this->redirect(array('action' => 'admin_index'));
            } else {
                $this->Session->setFlash(
                    __('The %s could not be saved. Please, try again.', __('Practiceplan')),
                    'alert',
                    array(
                        'plugin' => 'TwitterBootstrap',
                        'class' => 'alert-error'
                    )
                );
            }
        } else {
            $this->request->data = $this->Practiceplan->read(null, $id);
        }
        $this->title($this->request->data['Practiceplan']['name']);
        // $posts = $this->Practiceplan->Post->find('list');
        // $contentmodules = $this->Practiceplan->Contentmodule->find('list');
        $this->set('layouts_array',$this->layouts_array);
        //$this->set(compact('posts', 'contentmodules','layouts_array'));
    }

    public function admin_add($id = null){

        $this->set('parent_id',$this->Practiceplan->find('list'));
        $this->set('activities_options',$this->Activity->find('list'));    

        $this->layout = "dashboard";
        $this->title('New Practiceplan');




            // debug($data); exit;
            if(isset($id)){
                // $data['Practiceplan']['parent_id']= $id;
                $this->Practiceplan->id = $id;
                $this->Practiceplan->contain();
                $g = $this->Practiceplan->find('list',array(
                    'fields'=>array('id','name'),
                    'conditions'=>array('Practiceplan.id'=> $id)
                ));
                // debug($g); exit;
                $this->set("parent_id",$g);
             ///   exit;
            }

         //   debug($this->request->data); exit;

		if ($this->request->is('post')) {
			$this->Practiceplan->create();
            $data = $this->request->data;
			if ($this->Practiceplan->save($data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('page')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('/my/practiceplans');
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('page')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
    }
    
  
    public function admin_delete($id = null) {
		// deletes this link and its children.
		$this->Practiceplan->id = $id;
		if (!$this->Practiceplan->exists()) {
			throw new NotFoundException(__('Invalid %s', __('navlink')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			$this->Practiceplan->id = $this->request->data['id'];
			if ($this->Practiceplan->delete()) {
				$this->Session->setFlash(
					__('The %s has been saved', __('navlink')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect($this->defaultRedirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be deleted. Please, try again.', __('Practice Plan')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {

		}
        debug("success?");
		// $children = $this->getChildrenList($id);
		// $this->set(compact('parentActivitys','children'));
	}


}

?>