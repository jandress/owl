<?php


class ActivitiesController extends VirtuosoAppController {

    // sub class this out 
    // var $uses = array(
    //     // "Chord"
    // );
    public $uses = array('Virtuoso.Activity','Virtuoso.PracticeSessionActivity',"Chord","Virtuoso.Scale");
    public $helpers = array('Navigations.TreeNav');
    public $components = array('TreeNav','Userassets.FileUtil');	

    function beforeFilter() {		
		parent::beforeFilter();
        $this->Auth->allow('index',"admin_children",'admin_index','view',"dev","chords","dev2","modes");
    }

    public function index(){
        $this->title("Activities Index");
        //$this->layout = "standalone_styled";
        $this->layout = "standalone_styled";
        //$this->layout = "background_only";
        
        $this->createPracticeSession();
    }

    private function createPracticeSession(){
        // $g = $this->Activity->find("all", array('conditions'=>array('Activity.parent_id'=>1)));
        // $ar = array();
        // foreach ($g as $key => $value) {
        //     //  debug($value);
        //     $i = rand(0,count($value['ChildActivity'])-1);
        //     // debug($i);
        //     array_push($ar,$value['ChildActivity'][$i]);
        // }
        // debug($ar); exit;
        // $this
        $this->set('session_activities',$ar);
    }

    public function modes(){
        //modes_intro
         $this->title("Activities modes");
         $this->set('underbar_content',"caged_menu");
        //  $this->addAdditionalScript('/Virtuoso/Virtuoso/js/caged_sequence_player');
        // $this->addAdditionalScript('/Virtuoso/Virtuoso/js/ui');
        // $this->addAdditionalScript('/Virtuoso/Virtuoso/js/modes_intro');
        $this->layout = "horizontal";        
    }

    public function chords($id=0){
        $this->title("Chords");
        $this->layout = "split_vert";
        $this->addAdditionalScript('/Virtuoso/Virtuoso/js/GuitarUi');
        $this->addAdditionalScript('/Virtuoso/Virtuoso/js/ui');
        $this->addAdditionalScript('/Virtuoso/Virtuoso/js/intervals/SimpleSequencePlayer');
        $this->addAdditionalScript('/Virtuoso/Virtuoso/js/chord_listing');
        $this->addAdditionalScript('/Virtuoso/Virtuoso/js/chord_activity');
        // debug($this->Chord); exit;
        $this->set('chords',$this->Chord->chord_list);
        $this->set('chordsjs', json_encode($this->Chord->chord_list ));        

    }

    public function admin_index($id=1){
        $this->Activity->recursive = 2;
        $g = $this->Activity->find("first",array('conditions'=>array("Activity.id"=>$id),'order' => array('Activity.lft')));
        $this->set('data',$g);
        $this->set('activities',$g['ChildActivity']);

        $this->layout = "dashboard";
        $address = 0;    
        $this->set('address',$address);        
    }

    public function admin_children($id=2, $address = 0){
        $id = $this->params->params['pass'][0];
        $g = $this->Activity->find("all",array('conditions'=>array("Activity.parent_id"=>$id),'order' => array('Activity.lft')));
        // $address = 0;    
		$g = $this->Activity->find('all', array(
                'order' => array('Activity.lft')
                ,'conditions'=>array('Activity.parent_id' => $id)
            )
        );
        $this->set('activities',$g);
        $this->set('address',$address);
    }

    public function view($id){

    }
    
    public function admin_view($id){         
        // $this->Activity->reorder();
        
        $this->layout = "horizontal";
        $this->Activity->id = $id;
        $this_Activity = $this->Activity->find("first",array('conditions'=>array('Activity.id'=>$id) ));;

        $json_data = json_decode(($this_Activity['Activity']['data']));
        if(!isset($json_data)){
            debug("malformed or missing json data"); exit;
        }
        
        $full_command = explode("/",$json_data->command);

        $params = $full_command;
        $c = array_splice($params,0,1);
        $command = str_replace("#","",$c[0]);       
        // debug($full_command);
            // $this_Activity['Activity']['data']); exit
        $this->set("activityJson", $this_Activity['Activity']['data']);
        $this->set("command",$command);
        $this->set("parameters", $params);        
        //--  ui_messaging_interface
        switch ($json_data->application) {
            case 'caged':
                # code...
                $this->addAdditionalScripts($this->Activity->caged_includes );
                $this->set('underbar_content',"caged_menu");
                $this->set('scaleList', $this->Scale->scales());
            break;
            case 'modes':
                # code...
                $this->addAdditionalScripts($this->Activity->modes_includes );
                $this->set('underbar_content',"modes_menu");
                $this->set('scaleList', $this->Scale->scales());
            break;            
            case 'interval':
                    # code...
                    $this->addAdditionalScripts($this->Activity->intervals_includes );
                    $this->set("underbar_content",'nav/intervals_exercise_nav');
                break;                
                case 'chords':
                    # code...
                    // $this->addAdditionalScripts($this->intervals_includes );
                    // $this->set("underbar_content",'nav/intervals_exercise_nav');
                break;                                
            default:
                # code...
                break;
        }
        // $this->setPreviousNextActions($id);
       $k = $this->Activity->generateTreeList(null, null, null, ' ');
       $m = $this->findElementsByIdWithKeys($k, $id);
       if($m['next'] == null || $m['next'] =="null"){
        $next = '/my/activities/';
       }else{
        $next = '/my/activities/view/'.$m['next']['id'];
       }
       if($m['previous'] == null || $m['previous'] =="null"){
        $previous = $prev = '/my/activities/';
       }else{
        $previous = $prev = '/my/activities/view/'.$m['previous']['id'];
       }
       $this->set(compact('current','prev','previous','next','practiceSessionId'));
    }
    
    public function dolist(){
        $this->set('list', $this->Activity->generateTreeList(null, null, null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'));
        //$this->Activity->recover();
        //debug($this->Activity->generateTreeList()); 
    }

	public function admin_relocate($id,$direction){
        // debug($this->Activity->veritfy()); exit;
        $this->layout="ajax";        
        if($direction=='up'){
         //   debug($this->Activity->moveUp($id)); exit;
            if($this->Activity->moveUp($id)){
                $this->redirect('/my/activities/');
            }
        }
        if($direction=='down'){
            //$this->Activity->recover();
          //  debug($this->Activity->moveDown($id)); exit;
            if($this->Activity->moveDown($id)){
                $this->redirect('/my/activities/');
            }
        }
        
        $g =  $this->Activity->generateTreeList(null, null, null, '.');
    }

    public function dev(){
        $this->title("Activities dev");
        $this->addAdditionalScript('/Virtuoso/Virtuoso/js/dev');
        $this->layout = "dev";
    }

    public function dev2(){
        $this->title("Activities dev");
        $this->addAdditionalScript('/Virtuoso/Virtuoso/js/dev2');
        // $this->layout = "dev";
    }

	public function devs($file = ''){
		//$y = $this->Courseevent->getCourseEventsForUserWithinDates(4,  '2000-08-08', '2999-08-08');
		//$this->addAdditionalScript('/Virtuoso/Virtuoso/js/devs/'.$file);
		
		//$this->set('events',$y);
		
		if (!empty($this->data)) {
			$this->User->save($this->data);
		}
		$g = $this->Fileutil->getDirectoryContents('../Plugin/Virtuoso/View/Elements/devs');
		$this->set('listing', $g);
		$this->set('include', $file);
		$this->set('userId', $this->current_user['User']['id']);
		switch ($file) {
			case '':
				$this->set('listing', $g);
				break;
			case('course_selects'):
				$this->course_selects($g, $file);
			break;
			default:
				// $this->doCSV();
			break;
		}
	}

    public function edit($id = null) {
        $this->admin_edit($id);
    }

    public function admin_edit($id = null) {
        $this->set('activity_options',$this->Activity->find('list'));
        $this->layout = "dashboard";
        $this->Activity->id = $id;
        if (!$this->Activity->exists()) {
            throw new NotFoundException(__('Invalid %s', __('Activity')));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            // debug($this->request->data); exit;
            $g = $this->Activity->save($this->request->data);
            // debug($this->request->data);
            // debug($g); exit;
            if ($g) {
                $this->Session->setFlash(
                    __('The %s has been saved', __('Activity')),
                    'alert',
                    array(
                        'plugin' => 'TwitterBootstrap',
                        'class' => 'alert-success'
                    )
                );
                $this->redirect(array('action' => 'admin_index'));
            } else {
                $this->Session->setFlash(
                    __('The %s could not be saved. Please, try again.', __('Activity')),
                    'alert',
                    array(
                        'plugin' => 'TwitterBootstrap',
                        'class' => 'alert-error'
                    )
                );
            }
        } else {
            $this->request->data = $this->Activity->read(null, $id);
        }
        $this->title($this->request->data['Activity']['name']);
        $this->set('layouts_array',$this->layouts_array);
        ///--
        $json_template_options = $this->FileUtil->getFileNamesAsKeys("./json_templates");
        $this->set('json_template_options',$json_template_options);
    }

    public function admin_add($id = null){

        $json_template_options = $this->FileUtil->getFileNamesAsKeys("./json_templates");
        $this->set('json_template_options',$json_template_options);

        $this->set('activity_options',$this->Activity->find('list'));

        $this->set('parent_id',$this->Activity->find('list'));

        $this->layout = "dashboard";
        $this->title('New Activity');




            // debug($data); exit;
            if(isset($id)){
                // $data['Activity']['parent_id']= $id;
                $this->Activity->id = $id;
                $this->Activity->contain();
                $g = $this->Activity->find('list',array(
                    'fields'=>array('id','name'),
                    'conditions'=>array('Activity.id'=> $id)
                ));
                // debug($g); exit;
                $this->set("parent_id",$g);
             ///   exit;
            }



		if ($this->request->is('post')) {
			$this->Activity->create();
            $data = $this->request->data;
			if ($this->Activity->save($data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('page')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('/my/activities');
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('page')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
    }
    
    public function admin_delete($id = null) {
		// deletes this link and its children.
		$this->Activity->id = $id;
		if (!$this->Activity->exists()) {
			throw new NotFoundException(__('Invalid %s', __('navlink')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			$this->Activity->id = $this->request->data['id'];
			if ($this->Activity->delete()) {
				$this->Session->setFlash(
					__('The %s has been saved', __('navlink')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect($this->defaultRedirect);
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('navlink')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {

		}
		$children = $this->getChildrenList($id);
		$this->set(compact('parentActivitys','children'));
	}

}

?>