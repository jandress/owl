<?php

App::uses('AppController', 'Controller');
//App::uses('Sanitize', 'Utility');
//App::uses('Security', 'Utility');
//App::uses('Auth', 'Component');
App::uses('Model', 'CakeSession');


class PracticesessionsController extends VirtuosoAppController {

	//'UserManagement.User','MessagingSystem.Message','UserManagement.Ticket','UserManagement.Group','Aro','Aco');
	// var $helpers = array('Html',"Session" , "Js",'TwitterBootstrap.BootstrapHtml',  'TwitterBootstrap.BootstrapForm','UserManagement.UserManagementForm' );
	// public $components = array('UserManagement.UserManagement','Acl', 'Session','Security','Auth','RequestHandler','Email');
	
	public $uses = array(
		'Virtuoso.PracticeSessionActivity'
		,'Virtuoso.PracticeSession'
		,'Virtuoso.PracticePlan'
		,'Virtuoso.Activity'
		,"Virtuoso.Scale"
	);
	
    function beforeFilter() {
		$this->Auth->allow('view');
		parent::beforeFilter();
		$this->layout = "dashboard";
    }
	
	function session(){

		$current_user = $this->current_user['User'];
		$this->layout= "ajax";
		$g = $this->params->pass;
		if(!isset($g[1])){
			print( "&nbsp; : )");
			return false;	
		}

		$this->PracticeSessionActivity->contain("Activity");
		$activities = $this->PracticeSessionActivity->find("first",array('conditions'=>array(
			"PracticeSessionActivity.user_id"=>$current_user['id'],
			"PracticeSessionActivity.id"=>$g[1]
		)));
	



		switch($g[0]){
			case 'r':
				// print( "r");
				//	print CakeSession::read( "ps" );
				// debug($activities);				
			break;
			case 'w':
				//Pinged By Javascript. 
				// check to see if this user has a PracticeSessionActivity with the passed in ID.   
				//if they do, check 
				// if time_begun is empty mark time_begun.  
        		// if time_begun is not empty, mark as time_last.
				// debug($activities);				
				if(empty($activities['PracticeSessionActivity']['time_begun'])){
					// echo "empty";
					$activities['PracticeSessionActivity']['time_begun'] = date('Y-m-d H:i:s');	
				}else{
					$activities['PracticeSessionActivity']['time_last'] = date('Y-m-d H:i:s');
				}

				$datetime1 =  new DateTime($activities['PracticeSessionActivity']['time_last']);
				$datetime2 =  new DateTime($activities['PracticeSessionActivity']['time_begun']);
				// Calculate the difference
				$interval = $datetime1->diff($datetime2);
				// Convert the interval to total minutes
				$totalMinutes = $interval->days * 24 * 60;
				$totalMinutes += $interval->h * 60;
				$totalMinutes += $interval->i;
				// Threshold in minutes (1 hour and 5 minutes)
				$thresholdMinutes = $activities['Activity']['time_estimated'];
				// Check if the difference exceeds the threshold
				if ($totalMinutes > $thresholdMinutes) {
					echo "Difference exceeds the threshold of ".$thresholdMinutes.'  Marking as completed.   ';
					$activities['PracticeSessionActivity']['completed'] = 1;
				} else {
					// echo "Difference does not exceed the threshold of ".$thresholdMinutes;
				}
				debug($this->PracticeSessionActivity->save($activities));	
				// $activities['PracticeSessionActivity']['time_last']
				// $activities['PracticeSessionActivity']['time_est']
				//debug($activities);

			break;		
		}
	}
	
	public function view_psa($id,$sid){
        // $this->Activity->reorder();
		$this->set('sid',$sid);
		$this->set('id',$id);
        $this->layout = "horizontal";
        $this->Activity->id = $id;
        $this_Activity = $this->PracticeSessionActivity->find("first",array('conditions'=>array('PracticeSessionActivity.id'=>$id) ));;

		// debug("OK. Problem. You;re having a hard time figuring out how to get access to the siblings for the previous  / next ");
		 //  debug($this_Activity); exit; 
		//  debug($this_Activity['Activity']); exit;
        $json_data = json_decode(($this_Activity['Activity']['data']));
			//----------
        if(!isset($json_data)){
            debug("malformed or missing json data");// exit;
        }
		//------
        $full_command = explode("/",$json_data->command);

        $params = $full_command;
        $c = array_splice($params,0,1);
        $command = str_replace("#","",$c[0]);       
        // debug($full_command);
            // $this_Activity['Activity']['data']); exit
        $this->set("activityJson", $this_Activity['Activity']['data']);
        $this->set("command",$command);
        $this->set("parameters", $params);        
        //--  ui_messaging_interface


		// this needs to be relocated to Activity,  Here and ActivtiesController.
        switch ($json_data->application) {
            case 'caged':
                # code...$this->Activity->intervals_includes 
                $this->addAdditionalScripts($this->Activity->caged_includes );
                $this->set('underbar_content',"caged_menu");
                $this->set('scaleList', $this->Scale->scales());
                break;
            case 'interval':
                    # code...
                    $this->addAdditionalScripts($this->Activity->intervals_includes );
                    $this->set("underbar_content",'nav/intervals_exercise_nav');
                break;                
                case 'chords':
                    # code...
                    // $this->addAdditionalScripts($this->intervals_includes );
                    // $this->set("underbar_content",'nav/intervals_exercise_nav');
                break;                                
            default:
                # code...
                break;
        }
        $this->setPreviousNextActions($id,$sid);
	}

	private function setPreviousNextActions($id,$sid){
		$this->PracticeSessionActivity->contain("Activity");
		$a = $this->PracticeSessionActivity->find('all',array('conditions'=>array('PracticeSessionActivity.session_id'=>$sid),
		'fields'=>array(
			'id','description'
		)
		));
		$b = $this->findSessionActivityById($a, $id);
		//  debug($b); exit;
		// /my/psa/view/2/1
		$current = $b['current']['PracticeSessionActivity']['id'].'/'.$sid;
		$prev = "/my/psa/view/".$b['previous']['PracticeSessionActivity']['id'].'/'.$sid;
		if($b['next']==null||$b['next']=="null"){
			$next = "/my/practicesessions/complete";
		}else{
			$next = "/my/psa/view/".$b['next']['PracticeSessionActivity']['id'].'/'.$sid;
		}
		
		$practiceSessionId = $sid;
		$this->set(compact('current','prev','next','practiceSessionId'));
    }

	public function complete(){

	}


	public function create(){

		// $activities = $this->PracticeSession->getCurrentSessionActivities();
		$activities = $this->PracticeSession->createPracticeSession();
		$this->set('activities',$activities);
		return;
	}



	function findSessionActivityById($array, $id) {
		$result = [
			'current' => null,
			'previous' => null,
			'next' => null
		];
	
		$foundIndex = -1;
	
		// Iterate through the array to find the matching ID
		foreach ($array as $index => $item) {
			if ($item['PracticeSessionActivity']['id'] == $id) {
				$result['current'] = $item;
				$foundIndex = $index;
				break;
			}
		}
	
		// If found, get the previous and next items if they exist
		if ($foundIndex != -1) {
			if ($foundIndex > 0) {
				$result['previous'] = $array[$foundIndex - 1];
			}
	
			if ($foundIndex < count($array) - 1) {
				$result['next'] = $array[$foundIndex + 1];
			}
		}
	
		return $result;
	}
	

	function admin_view($id = null ){
		//  debug($id); exit;
		if($id==null){
			// $activities = $this->PracticeSession->getCurrentSessionActivities();
			// $this->set('activities',$activities);
			return;
		}
		$this->set('psId',$id);
		//if you search PracticeSession instead of PracticeSessionActivity, PracticeSessionActivityloses its association with Activity 
		$b = $this->PracticeSessionActivity->find('all',array('conditions'=>array('PracticeSessionActivity.session_id'=>$id)));

		$this->set('data', $b );
	}
	
	function index(){
		// $this->layout = "horizontal";


        // Set previous and null 
        // debug($this->Activity->generateTreeList(null, null, null, ' ')); exit;
		$ar = $this->PracticeSessionActivity->find("all", array('conditions'=>array('PracticeSessionActivity.user_id'=>1)));
		$this->createPracticeSession();
		$this->set('activities',$ar);

	}

	private function createPracticeSession(){
        $g = $this->Activity->find("all", array('conditions'=>array('Activity.parent_id'=>1),'fields'=>array('name','id')));
        $ar = array();
		$ar2 = array();
        $this->set('activities',$ar);
    }

	function admin_index(){
		
		// echo date('l, F j, Y H:i:s');
		// exit;


		// $this->index();	
		// debug("domain-->kingdom-->phylum-->class-->order-->family-->Genus-->Species");
		// debug("Domain...Subject...Direction...");
		// debug("L0...L1...L2...L3...");
		// L1 i 
		// musical kingdoms...		
		// tonalia,
			// intervallia	
			// texture
			// harmony
			// melody

		// Narrative

		// genus: (plural: genera) A group of closely related species
		// species is a type of genus, a genus associated with other members if a family, an dorde3r
		


		// $ar = $this->PracticePlan->generateTreeList(null,null,null,"	");
		// debug($ar);
		// $this->PracticePlan->contain(); 
		 $ar2 = $this->PracticeSession->find("all", array("conditions"=>array('user_id'=>$this->current_user['User']['id'])));
		//  debug($ar2); exit;
		// foreach ($ar2 as $key => $value) {
		// 	// debug($value);
		// 	# code...
		// }
		
		//exit;
		  $this->set('data',$ar2);
	}

	function admin_add(){
	
	}

	function edit_psa($id){
		$this_Activity = $this->PracticeSessionActivity->find("first",array('conditions'=>array('PracticeSessionActivity.id'=>$id) ));;
		// debug($this_Activity); 
		$this->redirect("/my/activities/edit/".$this_Activity['PracticeSessionActivity']['activity_id']);
		// exit; 
		// debug($id); 
		// exit;
		// /my/psa/edit/
	}

	function admin_delete(){
	
	}




	function create_from_plan(){



	}


	function create_from_random_activity(){
		// $this->Activity->contain();
		$g = $this->Activity->find("all", array('conditions'=>array("Activity.parent_id"=>1)));
		$b = $this->Activity->selectValidActivities($g,15);
		// debug($b);
		// exit;
		foreach ($b as $key => $value) {
			  debug($value);
			// debug($value['Activity']["name"].' '.count($value['ChildActivity']));
			// debug($value['ChildActivity']);
			// debug();
		}
		
		
	}


}
