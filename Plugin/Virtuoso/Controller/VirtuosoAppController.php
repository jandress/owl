<?php

class VirtuosoAppController extends AppController {




	var $components = array( 'Session','Auth');



    public function beforeFilter(){
        parent::beforeFilter();
        // $this->layout = "Virtuoso.dev";
        $this->layout = "split_horiz";
    }







	function findElementsByIdWithKeys($array, $id) {
		$keys = array_keys($array); // Get all keys of the array
		$foundIndex = array_search($id, $keys); // Find the index of the ID in keys array
		$result = [];
		// Get the element with the matching ID
		if (array_key_exists($id, $array)) {
			$result['current'] = ['id' => $id, 'value' => $array[$id]];
		} else {
			$result['current'] = null;
		}
	
		// Get the preceding element if it exists
		if ($foundIndex !== false && $foundIndex > 0) {
			$precedingKey = $keys[$foundIndex - 1];
			$result['previous'] = ['id' => $precedingKey, 'value' => $array[$precedingKey]];
		} else {
			$result['previous'] = null;
		}
	
		// Get the next element if it exists
		if ($foundIndex !== false && $foundIndex < count($keys) - 1) {
			$nextKey = $keys[$foundIndex + 1];
			$result['next'] = ['id' => $nextKey, 'value' => $array[$nextKey]];
		} else {
			$result['next'] = null;
		}
	
		return $result;
	}








    

}
?>