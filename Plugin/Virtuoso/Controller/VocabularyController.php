<?php


class VocabularyController extends VirtuosoAppController {

    // public $uses = array('Virtuoso.Practiceplan','Virtuoso.PracticeSessionPracticeplan',"Chord","Virtuoso.Scale",'Virtuoso.Activity');
    // public $helpers = array('Navigations.TreeNav');
    // public $components = array('TreeNav');
    public $components = array('Paginator', 'Session','Userassets.Fileutil');

	
    function beforeFilter() {		
		parent::beforeFilter();
        $this->layout = "default";
        // $this->Auth->allow('index',"admin_children",'admin_index','view',"dev","chords","dev2","modes");
    }

    public function index(){

    }
    public function view($id){
        $t = $this->Vocabulary->find("all",array('conditions'=>array("Vocabulary.user_id"=>$this->current_user['User']['id'])));
        debug($t);
    }



    


    public function admin_view($id){
        $t = $this->Vocabulary->find("first",array('conditions'=>array( "Vocabulary.user_id" => $this->current_user['User']['id'], "Vocabulary.id" => $id   )));
        $this->set('data',$t);
        // debug($this->current_user['User']['id']);
        // debug($t);

    }



    public function admin_edit($id){
        $vocabDirectory = '/users/'.$this->current_user['User']['user_dir'].'/private/vocab';
        $b = $this->Fileutil->getFileListing( '.'.$vocabDirectory );
        array_unshift($b,"Url");
        $this->set('vocabDirectoryListing',$b);




        if ($this->request->is('post')) {
			$this->Vocabulary->create();
			if ($this->Vocabulary->save($this->request->data)) {
				$this->Session->setFlash(__('The avatar has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The avatar could not be saved. Please, try again.'));
			}
		}else{

        }



    }
    
    public function admin_index(){

        $vocabDirectory = '/users/'.$this->current_user['User']['user_dir'].'/private/vocab';
        $this->set('files',$this->Fileutil->getFileListing( '.'.$vocabDirectory ));
        $t = $this->Vocabulary->find("all",array('conditions'=>array( "Vocabulary.user_id" => $this->current_user['User']['id']  )));
        $this->set('links',$t);
    }
    public function admin_delete($id){

    }

    public function admin_add() {
		// $this->set('fileOptions',
        // $this->getimagesAsOptions($this->Fileutil->getFileListing( '.'.$this->Vocabulary->avatarDirectory )));
        //$this->Fileutil->getFileListing( '.'.$this->Avatar->avatarDirectory )));
        // debug($this->current_user['User']['user_dir']);
        $vocabDirectory = '/users/'.$this->current_user['User']['user_dir'].'/private/vocab';
        $this->set('vocabDirectoryListing',$this->Fileutil->getFileListing( '.'.$vocabDirectory ));

        $this->request->data['Vocabulary']['user_id'] = $this->current_user['User']['id'];
        // debug( $this->Vocabulary->save($this->request->data) );
		if ($this->request->is('post')) {
			$this->Vocabulary->create();
			if ($this->Vocabulary->save($this->request->data)) {
				$this->Session->setFlash(__('The avatar has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The avatar could not be saved. Please, try again.'));
			}
		}
	}

}

?>