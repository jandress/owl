<?php

App::uses('AppController', 'Controller');
//App::uses('Sanitize', 'Utility');
//App::uses('Security', 'Utility');
//App::uses('Auth', 'Component');


class IntervalsController extends VirtuosoAppController {

	// var $uses = array('UserManagement.User','MessagingSystem.Message','UserManagement.Ticket','UserManagement.Group','Aro','Aco');
	// var $helpers = array('Html',"Session" , "Js",'TwitterBootstrap.BootstrapHtml',  'TwitterBootstrap.BootstrapForm','UserManagement.UserManagementForm' );
	// public $components = array('UserManagement.UserManagement','Acl', 'Session','Security','Auth','RequestHandler','Email');
	
    function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow("admin_exercise",'lesson');
    }

	function index(){	
		$this->title("Intervals Index");
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/intervals/SimpleSequencePlayer');
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/intervals/IntervalSequencer');
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/intervals/sspUI');
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/intervals/intervals');
		//$this->layout = "dev";
		$this->layout = "split_horiz2";
		// $this->layout = "split_vert";
	}

	function exercise(){}
	

	public $intervals_include = array(
		'/Virtuoso/Virtuoso/js/GuitarUi',
		'/Virtuoso/Virtuoso/js/intervals/SimpleSequencePlayer',
		'/Virtuoso/Virtuoso/js/intervals/IntervalSequencer',
		'/Virtuoso/Virtuoso/js/intervals/sspUI',
		'/Virtuoso/Virtuoso/js/intervals/IntervalTrainer',
	);


	function includeJsfiles($g){}
	
	
	function admin_exercise(){
		$this->title("Intervals Exercise");
		$this->addAdditionalScripts($this->intervals_include );
		$this->layout = "horizontal";		
		$this->set("underbar_content",'nav/intervals_exercise_nav');
	}
	


	function lesson(){
		$this->title("Intervals Lesson");
		$this->addAdditionalScript('/Virtuoso/Virtuoso/js/intervals/interval_activity');
	}
	
	public function admin_index(){
		$this->layout = "dashboard";
		$this->set('activitites',$this->paginate());
	}

}
