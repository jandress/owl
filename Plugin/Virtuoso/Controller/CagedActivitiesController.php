<?php

    class CagedActivitiesController extends VirtuosoAppController {


        function beforeFilter() {		
                parent::beforeFilter();  
                $this->Auth->allow('routing','crawling','index','view','admin_index','admin_view');
        }

        public function crawling(){
            $this->addAdditionalScript('/Virtuoso/Virtuoso/js/GuitarUi');
            $this->addAdditionalScript('/Virtuoso/Virtuoso/js/ui');
        }

        public function admin_view($id){
            
            $this->set('underbar_content',"caged_menu");
            $this->layout = "horizontal";
            // debug($this->layout); exit;
            $this->CagedActivity->id = $id;
            if (!$this->CagedActivity->exists()) {
                throw new NotFoundException(__('Invalid %s', __('page')));
            }
            $list = $this->CagedActivity->find("list");


            //	pr($this->Page->read(null, $id));
            $data = $this->CagedActivity->read(null, $id);

            $data['list'] = $list;

            $this->data = $data;
            $this->set('data', $data);          

        }
        
        public function routing(){

        }
        
        public function view($id){
            

            $this->CagedActivity->id = $id;
            if (!$this->CagedActivity->exists()) {
                throw new NotFoundException(__('Invalid %s', __('page')));
            }
        //	pr($this->Page->read(null, $id));
            
            $this->data = $this->CagedActivity->read(null, $id);
            $this->set('data', $this->data);                
        }

        public function admin_index(){
            $this->layout = "dashboard";
            $this->set('cagedActivities',$this->paginate());
        }

        public function index(){
            $this->layout = "dashboard";
            $this->setDashboardLayout();
            $this->CagedActivity->contain();
            $this->set('cagedActivities',$this->paginate());
        }

        public function admin_edit($id = null) {
            $this->layout = "dashboard";

            $this->CagedActivity->id = $id;
            if (!$this->CagedActivity->exists()) {
                throw new NotFoundException(__('Invalid %s', __('CagedActivity')));
            }
            if ($this->request->is('post') || $this->request->is('put')) {
                if ($this->CagedActivity->save($this->request->data)) {
                    $this->Session->setFlash(
                        __('The %s has been saved', __('CagedActivity')),
                        'alert',
                        array(
                            'plugin' => 'TwitterBootstrap',
                            'class' => 'alert-success'
                        )
                    );
                    $this->redirect(array('action' => 'admin_index'));
                } else {
                    $this->Session->setFlash(
                        __('The %s could not be saved. Please, try again.', __('CagedActivity')),
                        'alert',
                        array(
                            'plugin' => 'TwitterBootstrap',
                            'class' => 'alert-error'
                        )
                    );
                }
            } else {
                $this->request->data = $this->CagedActivity->read(null, $id);
            }
            
            $this->title($this->request->data['CagedActivity']['name']);
            // $posts = $this->CagedActivity->Post->find('list');
            // $contentmodules = $this->CagedActivity->Contentmodule->find('list');
            $this->set('layouts_array',$this->layouts_array);
            //$this->set(compact('posts', 'contentmodules','layouts_array'));
        }
    
        public function admin_delete($id = null) {
            if (!$this->request->is('post')) {
                throw new MethodNotAllowedException();
            }
            $this->CagedActivity->id = $id;
            if (!$this->CagedActivity->exists()) {
                throw new NotFoundException(__('Invalid %s', __('CagedActivity')));
            }
            if ($this->CagedActivity->delete()) {
                $this->Session->setFlash(
                    __('The %s deleted', __('CagedActivity')),
                    'alert',
                    array(
                        'plugin' => 'TwitterBootstrap',
                        'class' => 'alert-success'
                    )
                );
                $this->redirect(array('action' => 'admin_index'));
            }
            $this->Session->setFlash(
                __('The %s was not deleted', __('CagedActivity')),
                'alert',
                array(
                    'plugin' => 'TwitterBootstrap',
                    'class' => 'alert-error'
                )
            );
            $this->redirect(array('action' => 'index'));
        }
    
        public function admin_add() {
            $this->setDashboardLayout();
            if ($this->request->is('post')) {
               /// debug($this->request->data); exit;
                if ($this->CagedActivity->save($this->request->data)) {
                    $this->Session->setFlash(
                        __('The %s has been saved', __('CagedActivity')),
                        'alert',
                        array(
                            'plugin' => 'TwitterBootstrap',
                            'class' => 'alert-success'
                        )
                    );
                    $this->redirect($this->defaultRedirect);
                } else {
                    $this->Session->setFlash(
                        __('The %s could not be saved. Please, try again.', __('CagedActivity')),
                        'alert',
                        array(
                            'plugin' => 'TwitterBootstrap',
                            'class' => 'alert-error'
                        )
                    );
                }
            }
        }

        
    }

?>