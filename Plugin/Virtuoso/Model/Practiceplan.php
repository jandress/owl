<?php
App::uses('AppModel', 'Model');
// App::uses('AuthComponent', 'Model');

class Practiceplan extends AppModel {

	var $actsAs=array('Containable','Tree');


	public $belongsTo = array(
		'ParentPracticeplan' => array(
			'className' => 'Practiceplan',
			'foreignKey' => 'parent_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Activity' => array(
			'className' => 'Activity',
			'foreignKey' => 'activity_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ChildPracticeplan' => array(
			'className' => 'Practiceplan',
			'foreignKey' => 'parent_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	/*
	public function create($data=array() , $filterKey = false){
		$t = parent::create($data, $filterKey);
		if($t){
			return $t;
		}
		$ar = array(
			'CagedActivity'=>array(
				'id'=>'',
				'name'=>'',
				'data'=>''
			)
		);
		return $this->save($ar);
	}
*/

}
