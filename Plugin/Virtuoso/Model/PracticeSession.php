
<?php
App::uses('AppModel', 'Model');
/**
 * Avatar Model
 *
 */
class PracticeSession extends AppModel {

    public $testers = 12345;
    
	public $hasMany = array(
		'PracticeSessionActivity' => array(
            'class'=>'PracticeSessionActivity',
            'foreignKey' => 'session_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);



    /*
    questionaire
    How comfortable are you with the guitar.      
    Presented with rate your skills page.  each row has a rate your kills and rate your interest.   how many chords do you know.    

    Do you know cowboy chords,  
    scripted courses vs algorithmic.   
*/




    public $variable = array(
        array('subject'=>'Scales','title'=>"E Major Position 2, Play Then Reverse"),
        array('subject'=>'Scales / Chords','title'=>"Diads"),
        array('subject'=>'Scales / Chords','title'=>"Triads"),
        array('subject'=>'Intervals','title'=>"Intervals"),
        array('subject'=>'Arpeggios','title'=>"Arpeggios")
    );



    function getObj($subject, $title, $id){
        return array(
            'subject'=>$subject,
            'title'=>$title,
            'id'=>$id
        );  
    }

    private function getLink($i){}
    

    private $link_array; 
    private function getRandomChildren($ar, $parentName = null){
        // debug($ar); 
         foreach ($ar as $key => $value) {
             if(empty($ar[$key]['Activity'])){
                 $thisItem = $ar[$key];                    
                 // debug($thisItem);
                 $b = $this->getObj($thisItem['name'],$parentName ,$thisItem['id']);
                 // debug($b);
                 array_push($this->link_array,$b);                  
                 continue;  
             }else{
                 $thisItem = $ar[$key]['Activity'];
             }
             if(!$thisItem['taxonomy_only']){        
                 $b = $this->getObj($ar[$key]['ParentActivity']['name'],$thisItem['name'],$thisItem['id']);
                 array_push($this->link_array,$b);                
             }
             if(!empty($value['ChildActivity'])){
                 $this->getRandomChildren($value['ChildActivity'],$thisItem['name']);
             }        
         }
         return $this->link_array;
     }
 
 

     public function createPracticeSession(){
        $my_model = ClassRegistry::init('Virtuoso.Practiceplan');

         $g = $my_model->find('all', array('conditions'=>array("Practiceplan.parent_id"=>23)));         
         $g2 = $my_model->find('all', array('conditions'=>array("Practiceplan.level >= 1")));    

         $g3 = array_merge($g2,$g);
         return $g3;
        //  debug($g); exit;
     }



      public function getCurrentSessionActivities(){
        $this->link_array = array();
        $my_model = ClassRegistry::init('Virtuoso.Activity');
        $g = $my_model->find('all', array('conditions'=>array('Activity.parent_id'=>1)));
        
        $t = array(
            $this->getObj("Intervals","random",1), 
            $this->getObj("Scales","random",1),
            $this->getObj("Rhythm","random",1),
            $this->getObj("Vocabulary","random",1),
            $this->getObj("Chords","random",1),
            $this->getObj("Arpeggios","random",1),
            $this->getObj("Theory","random",1)                
        );
        
        // $t = $this->getRandomChildren($g);
        return $t; //$this->link_array;
        // return $this->variable;
      }


      function getNextActivity(){}  
      function getActivity($i){}

}



    
/*
    
*/  