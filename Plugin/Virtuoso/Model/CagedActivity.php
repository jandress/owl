<?php
App::uses('AppModel', 'Model');
// App::uses('AuthComponent', 'Model');

class CagedActivity extends Model {

	
	var $actsAs = array('Polymorphic','Containable'); 	
    public $displayField = 'name';
	public $display_field = 'name';
	// var $actsAs = array('Containable');
    // public $useDbConfig = 'users';
	// public $user_role = 'visitor';

	// public $STUDENT = array('id'=>1,'label'=>'students');
	// public $STAFF= array('id'=>2,'label'=>'staff');
	// public $ADMINISTRATOR = array('id'=> 3,'label'=>'administrators');
	// public $DEVELOPER = array('id'=> 4,'label'=>'developers');
	// public $VISITOR =  array('id'=>5,'label'=>'visitors');


	public $belongsTo = array(
		// 'Group' => array(
		// 	'className' => 'group',
		// 	'foreignKey' => 'group_id',
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => ''
		// )
	);


	public function create($data=array() , $filterKey = false){
		$t = parent::create($data, $filterKey);
		if($t){
			return $t;
		}
		$ar = array(
			'CagedActivity'=>array(
				'id'=>'',
				'name'=>'',
				'data'=>''
			)
		);
		return $this->save($ar);
	}


}
