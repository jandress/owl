<?php
App::uses('AppModel', 'Model');
// App::uses('AuthComponent', 'Model');

class Scale extends AppModel {


    public $major_scale = array(
        'name' => "Major",
        'structure' => array(2, 2, 1, 2, 2, 2, 1)
    );
    
    public $minor_pentatonic = array(
        'name' => "Pentatonic Minor",
        'structure' => array(3, 2, 2, 3, 2)
    );
    
    public $chromatic = array(
        'name' => "chromatic",
        'structure' => array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
    );
    
    public $lydian_mode = array(
        'name' => "lydian",
        'structure' => array(2, 2, 2, 1, 2, 2, 1)
    );
    
    public $ionian_mode = array(
        'name' => "ionian", // also major
        'structure' => array(2, 2, 1, 2, 2, 2, 1)
    );
    
    public $mixolydian_mode = array(
        'name' => "mixolydian",
        'structure' => array(2, 2, 1, 2, 2, 1, 2)
    );
    
    public $dorian_mode = array(
        'name' => "dorian",
        'structure' => array(2, 1, 2, 2, 2, 1, 2)
    );
    
    public $aeolian_mode = array(
        'name' => "aeolian",
        'structure' => array(2, 1, 2, 2, 1, 2, 2)
    );
    
    public $phrygian_mode = array(
        'name' => "phrygian",
        'structure' => array(1, 2, 2, 2, 1, 2, 2)
    );
    
    public $locrian_mode = array(
        'name' => "locrian",
        'structure' => array(1, 2, 2, 1, 2, 2, 2)
    );
    
    public function modes(){
        $t = array(
            $this->lydian_mode,
            $this->ionian_mode,
            $this->mixolydian_mode,
            $this->dorian_mode,
            $this->aeolian_mode,
            $this->phrygian_mode,
            $this->locrian_mode
        );        
        return $t;        
    }


    public function scales(){
        $t =  array(
            $this->major_scale,
            $this->minor_pentatonic,
            $this->lydian_mode,
            $this->ionian_mode,
            $this->mixolydian_mode,
            $this->dorian_mode,
            $this->aeolian_mode,
            $this->phrygian_mode,
            $this->locrian_mode
        );
        return $t;        
    }
    




















}
