<?php
App::uses('AppModel', 'Model');
// App::uses('AuthComponent', 'Model');

class Activity extends AppModel {

	var $actsAs=array('Containable','Tree');


	public $belongsTo = array(
		'ParentActivity' => array(
			'className' => 'Activity',
			'foreignKey' => 'parent_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ChildActivity' => array(
			'className' => 'Activity',
			'foreignKey' => 'parent_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	public $intervals_includes = array(
		'/Virtuoso/Virtuoso/js/GuitarUi',
		'/Virtuoso/Virtuoso/js/intervals/SimpleSequencePlayer',
		'/Virtuoso/Virtuoso/js/intervals/IntervalSequencer',
		'/Virtuoso/Virtuoso/js/intervals/sspUI',
		'/Virtuoso/Virtuoso/js/intervals/IntervalTrainer',
	);

    public $caged_includes = array(
        
        "/Virtuoso/Virtuoso/js/drill_runner",
        "/Virtuoso/Virtuoso/js/caged/ui_messaging_interface",
        "/Virtuoso/Virtuoso/js/caged/dev_ui",
        "/Virtuoso/Virtuoso/js/caged/caged_sequence_player",
        "/Virtuoso/Virtuoso/js/caged/caged_activity_model", 
        "/Virtuoso/Virtuoso/js/caged/caged_activity",
        "/Virtuoso/Virtuoso/js/caged/caged_activity_dev", 
        "/Virtuoso/Virtuoso/js/caged_view",
        '/Virtuoso/Virtuoso/js/caged/init_caged'
    );




    public $modes_includes = array(
        
        "/Virtuoso/Virtuoso/js/drill_runner", // for when a drill is chosen in the ui.    probably not neccessary  
        "/Virtuoso/Virtuoso/js/caged/ui_messaging_interface", //the text that shows up in an activity
        "/Virtuoso/Virtuoso/js/caged/dev_ui", 
        "/Virtuoso/Virtuoso/js/caged/caged_sequence_player",
        "/Virtuoso/Virtuoso/js/caged/caged_activity_model", 
        "/Virtuoso/Virtuoso/js/caged/caged_activity",
        "/Virtuoso/Virtuoso/js/caged/caged_activity_dev", 
        "/Virtuoso/Virtuoso/js/caged_view",
        '/Virtuoso/Virtuoso/js/caged/modes',
		'/Virtuoso/Virtuoso/js/caged/init_modes'
    );

	function selectValidActivities($activityObjectArray, $minimumTotalNumber, $selectionInfo = null) {
		$selectedActivities = [];
	
		function countValidActivities($actv, &$selectedActivities, $selectionInfo) {
		}
	
		foreach ($activityObjectArray as $activity) {
			$tempSelectedActivities = [];
		}
	
		// Ensure the minimum total number is met
		if (count($selectedActivities) < $minimumTotalNumber) {
			// Handle the case where the minimum requirement is not met
			// This could involve selecting more Activities or adjusting the criteria
		}
	
		return $selectedActivities;
	}
	





	function isSelectionInfoTarget($activityId, $selectionInfo) {
		if ($selectionInfo) {
			foreach ($selectionInfo as $info) {
				if ($info["id"] == $activityId) {
					return true;
				}
			}
		}
		return false;
	}

	function getSelectionInfoTarget($activityId, $selectionInfo) {
		foreach ($selectionInfo as $info) {
			if ($info["id"] == $activityId) {
				return $info;
			}
		}
		return null;
	}



}
